﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Unloading
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Unloading))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.EDLocation = New System.Windows.Forms.ComboBox()
        Me.TLOCATIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Quantity = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.PackingId = New System.Windows.Forms.ComboBox()
        Me.TPACKINGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EDPacking = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Cbn10 = New System.Windows.Forms.TextBox()
        Me.EDProduct = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.P_Weight = New System.Windows.Forms.ComboBox()
        Me.Cbn9 = New System.Windows.Forms.TextBox()
        Me.Updatedate = New System.Windows.Forms.TextBox()
        Me.Cbn6 = New System.Windows.Forms.TextBox()
        Me.StatusId = New System.Windows.Forms.ComboBox()
        Me.TSTATUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DriverID = New System.Windows.Forms.ComboBox()
        Me.TDRIVERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TruckCompanyId = New System.Windows.Forms.ComboBox()
        Me.TCOMPANYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductId = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CustomerID = New System.Windows.Forms.ComboBox()
        Me.TCustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TruckId = New System.Windows.Forms.ComboBox()
        Me.TTRUCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ShipmentNo = New System.Windows.Forms.TextBox()
        Me.ShipperId = New System.Windows.Forms.ComboBox()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GiDate = New System.Windows.Forms.DateTimePicker()
        Me.Coa = New System.Windows.Forms.TextBox()
        Me.EDCustomer = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.V_UNLOADINGNOTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.DatetimeDBGrid1 = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DBGrid1 = New System.Windows.Forms.DataGridView()
        Me.LOAD_DID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_DOfull = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Product_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Customer_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_DRIVER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_PRESET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_VEHICLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LC_COMPARTMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_TANK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LCBAYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReferenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BTWeightOut = New System.Windows.Forms.Button()
        Me.BTWeightIn = New System.Windows.Forms.Button()
        Me.BTupdate = New System.Windows.Forms.Button()
        Me.BTcancel = New System.Windows.Forms.Button()
        Me.BTsap = New System.Windows.Forms.Button()
        Me.BTsave = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Btprint = New System.Windows.Forms.Button()
        Me.Btedit = New System.Windows.Forms.Button()
        Me.Btadd = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BtVehicleEdit = New System.Windows.Forms.Button()
        Me.CBCardNO = New System.Windows.Forms.ComboBox()
        Me.VCARDFREEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label69 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.WeightScal = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Invoice = New System.Windows.Forms.TextBox()
        Me.PO_No = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Prono = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.DateeditTemp = New System.Windows.Forms.TextBox()
        Me.Dateedit = New System.Windows.Forms.DateTimePicker()
        Me.Container = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Batchlot = New System.Windows.Forms.TextBox()
        Me.Cbn11 = New System.Windows.Forms.TextBox()
        Me.Cbn8 = New System.Windows.Forms.TextBox()
        Me.Cbn7 = New System.Windows.Forms.TextBox()
        Me.EDShipper = New System.Windows.Forms.ComboBox()
        Me.EDDriver = New System.Windows.Forms.ComboBox()
        Me.EDTransNO = New System.Windows.Forms.ComboBox()
        Me.EDVehicle = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Cbn5 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Weightintime = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Weighttol = New System.Windows.Forms.TextBox()
        Me.weightouttime = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Netweight = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.PackingWeight = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.WeightTotal = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Status = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.LawWeightIn = New System.Windows.Forms.TextBox()
        Me.UpdateWeightIn = New System.Windows.Forms.TextBox()
        Me.UpdateWeightOut = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LawWeightout = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.V_LoadingnoteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CrystalReport1 = New CrystalDecisions.CrystalReports.Engine.ReportDocument()
        Me.T_DRIVERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.T_COMPANYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter()
        Me.T_TRUCKTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTableAdapter()
        Me.V_LOADINGNOTETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_LOADINGNOTETableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.V_UNLOADINGNOTETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_UNLOADINGNOTETableAdapter()
        Me.T_LOCATIONTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_LOCATIONTableAdapter()
        Me.T_STATUSTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_STATUSTableAdapter()
        Me.T_COMPANYTableAdapter1 = New TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_DRIVERTableAdapter1 = New TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter()
        Me.BindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.T_CustomerTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_CustomerTableAdapter()
        Me.T_PACKINGTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_PACKINGTableAdapter()
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.BTweightoutprint = New System.Windows.Forms.Button()
        Me.V_CARDFREETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_CARDFREETableAdapter()
        CType(Me.TLOCATIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TPACKINGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.TSTATUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TTRUCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        CType(Me.V_UNLOADINGNOTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.VCARDFREEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.V_LoadingnoteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TextBox9
        '
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(636, 282)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(180, 26)
        Me.TextBox9.TabIndex = 20
        Me.TextBox9.Visible = False
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label45.Location = New System.Drawing.Point(526, 289)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(96, 20)
        Me.Label45.TabIndex = 188
        Me.Label45.Text = "Station No. :"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label45.Visible = False
        '
        'EDLocation
        '
        Me.EDLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.EDLocation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDLocation.DataSource = Me.TLOCATIONBindingSource
        Me.EDLocation.DisplayMember = "CODE"
        Me.EDLocation.DropDownWidth = 300
        Me.EDLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDLocation.FormattingEnabled = True
        Me.EDLocation.Location = New System.Drawing.Point(629, 190)
        Me.EDLocation.Name = "EDLocation"
        Me.EDLocation.Size = New System.Drawing.Size(180, 28)
        Me.EDLocation.TabIndex = 19
        '
        'TLOCATIONBindingSource
        '
        Me.TLOCATIONBindingSource.DataMember = "T_LOCATION"
        Me.TLOCATIONBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label46.Location = New System.Drawing.Point(476, 196)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(139, 20)
        Me.Label46.TabIndex = 185
        Me.Label46.Text = "Storage Location :"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label47.Location = New System.Drawing.Point(543, 286)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(79, 20)
        Me.Label47.TabIndex = 184
        Me.Label47.Text = "Host No. :"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label47.Visible = False
        '
        'TextBox10
        '
        Me.TextBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox10.Location = New System.Drawing.Point(637, 283)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(180, 26)
        Me.TextBox10.TabIndex = 183
        Me.TextBox10.Visible = False
        '
        'Quantity
        '
        Me.Quantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Quantity.Location = New System.Drawing.Point(156, 353)
        Me.Quantity.Name = "Quantity"
        Me.Quantity.Size = New System.Drawing.Size(180, 26)
        Me.Quantity.TabIndex = 12
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label33.Location = New System.Drawing.Point(70, 362)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(80, 20)
        Me.Label33.TabIndex = 182
        Me.Label33.Text = "Quantity  :"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PackingId
        '
        Me.PackingId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.PackingId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.PackingId.DataSource = Me.TPACKINGBindingSource
        Me.PackingId.DisplayMember = "P_ID"
        Me.PackingId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.PackingId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PackingId.FormattingEnabled = True
        Me.PackingId.Location = New System.Drawing.Point(2, 202)
        Me.PackingId.Name = "PackingId"
        Me.PackingId.Size = New System.Drawing.Size(59, 21)
        Me.PackingId.TabIndex = 180
        Me.PackingId.ValueMember = "P_ID"
        '
        'TPACKINGBindingSource
        '
        Me.TPACKINGBindingSource.DataMember = "T_PACKING"
        Me.TPACKINGBindingSource.DataSource = Me.FPTDataSet
        '
        'EDPacking
        '
        Me.EDPacking.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.EDPacking.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDPacking.DataSource = Me.TPACKINGBindingSource
        Me.EDPacking.DisplayMember = "P_CODE"
        Me.EDPacking.DropDownWidth = 300
        Me.EDPacking.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDPacking.FormattingEnabled = True
        Me.EDPacking.Location = New System.Drawing.Point(156, 320)
        Me.EDPacking.Name = "EDPacking"
        Me.EDPacking.Size = New System.Drawing.Size(180, 28)
        Me.EDPacking.TabIndex = 11
        Me.EDPacking.ValueMember = "P_ID"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label32.Location = New System.Drawing.Point(39, 329)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(111, 20)
        Me.Label32.TabIndex = 178
        Me.Label32.Text = "Packing Type :"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Cbn10
        '
        Me.Cbn10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Cbn10.Location = New System.Drawing.Point(156, 384)
        Me.Cbn10.Name = "Cbn10"
        Me.Cbn10.Size = New System.Drawing.Size(180, 26)
        Me.Cbn10.TabIndex = 13
        '
        'EDProduct
        '
        Me.EDProduct.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDProduct.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDProduct.DataSource = Me.TProductBindingSource
        Me.EDProduct.DisplayMember = "Product_code"
        Me.EDProduct.DropDownWidth = 300
        Me.EDProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDProduct.FormattingEnabled = True
        Me.EDProduct.Location = New System.Drawing.Point(156, 194)
        Me.EDProduct.Name = "EDProduct"
        Me.EDProduct.Size = New System.Drawing.Size(180, 28)
        Me.EDProduct.TabIndex = 7
        Me.EDProduct.ValueMember = "Product_code"
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label34.Location = New System.Drawing.Point(78, 197)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(72, 20)
        Me.Label34.TabIndex = 174
        Me.Label34.Text = "Product :"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.P_Weight)
        Me.GroupBox6.Controls.Add(Me.Cbn9)
        Me.GroupBox6.Controls.Add(Me.Updatedate)
        Me.GroupBox6.Controls.Add(Me.Cbn6)
        Me.GroupBox6.Controls.Add(Me.StatusId)
        Me.GroupBox6.Controls.Add(Me.DriverID)
        Me.GroupBox6.Controls.Add(Me.TruckCompanyId)
        Me.GroupBox6.Controls.Add(Me.PackingId)
        Me.GroupBox6.Controls.Add(Me.ProductId)
        Me.GroupBox6.Controls.Add(Me.TextBox1)
        Me.GroupBox6.Controls.Add(Me.CustomerID)
        Me.GroupBox6.Controls.Add(Me.TruckId)
        Me.GroupBox6.Controls.Add(Me.Label11)
        Me.GroupBox6.Controls.Add(Me.ShipmentNo)
        Me.GroupBox6.Controls.Add(Me.ShipperId)
        Me.GroupBox6.Controls.Add(Me.GiDate)
        Me.GroupBox6.Controls.Add(Me.Coa)
        Me.GroupBox6.Location = New System.Drawing.Point(348, 85)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(133, 29)
        Me.GroupBox6.TabIndex = 173
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Hidden"
        Me.GroupBox6.Visible = False
        '
        'P_Weight
        '
        Me.P_Weight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.P_Weight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.P_Weight.DataSource = Me.TPACKINGBindingSource
        Me.P_Weight.DisplayMember = "P_WEIGHT"
        Me.P_Weight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.P_Weight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.P_Weight.FormattingEnabled = True
        Me.P_Weight.Location = New System.Drawing.Point(12, 329)
        Me.P_Weight.Name = "P_Weight"
        Me.P_Weight.Size = New System.Drawing.Size(53, 21)
        Me.P_Weight.TabIndex = 193
        Me.P_Weight.ValueMember = "P_WEIGHT"
        '
        'Cbn9
        '
        Me.Cbn9.Location = New System.Drawing.Point(97, 14)
        Me.Cbn9.Name = "Cbn9"
        Me.Cbn9.Size = New System.Drawing.Size(53, 20)
        Me.Cbn9.TabIndex = 158
        Me.Cbn9.Visible = False
        '
        'Updatedate
        '
        Me.Updatedate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Updatedate.Location = New System.Drawing.Point(84, 36)
        Me.Updatedate.Name = "Updatedate"
        Me.Updatedate.Size = New System.Drawing.Size(53, 26)
        Me.Updatedate.TabIndex = 111
        Me.Updatedate.Visible = False
        '
        'Cbn6
        '
        Me.Cbn6.Location = New System.Drawing.Point(10, 29)
        Me.Cbn6.Name = "Cbn6"
        Me.Cbn6.Size = New System.Drawing.Size(55, 20)
        Me.Cbn6.TabIndex = 149
        Me.Cbn6.Visible = False
        '
        'StatusId
        '
        Me.StatusId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.StatusId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.StatusId.DataSource = Me.TSTATUSBindingSource
        Me.StatusId.DisplayMember = "ID"
        Me.StatusId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.StatusId.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.StatusId.FormattingEnabled = True
        Me.StatusId.Location = New System.Drawing.Point(6, 245)
        Me.StatusId.Name = "StatusId"
        Me.StatusId.Size = New System.Drawing.Size(59, 28)
        Me.StatusId.TabIndex = 177
        Me.StatusId.ValueMember = "STATUS_ID"
        '
        'TSTATUSBindingSource
        '
        Me.TSTATUSBindingSource.DataMember = "T_STATUS"
        Me.TSTATUSBindingSource.DataSource = Me.FPTDataSet
        '
        'DriverID
        '
        Me.DriverID.DataSource = Me.TDRIVERBindingSource
        Me.DriverID.DisplayMember = "ID"
        Me.DriverID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DriverID.FormattingEnabled = True
        Me.DriverID.Location = New System.Drawing.Point(12, 226)
        Me.DriverID.Name = "DriverID"
        Me.DriverID.Size = New System.Drawing.Size(55, 21)
        Me.DriverID.TabIndex = 157
        Me.DriverID.ValueMember = "ID"
        '
        'TDRIVERBindingSource
        '
        Me.TDRIVERBindingSource.DataMember = "T_DRIVER"
        Me.TDRIVERBindingSource.DataSource = Me.FPTDataSet
        '
        'TruckCompanyId
        '
        Me.TruckCompanyId.DataSource = Me.TCOMPANYBindingSource
        Me.TruckCompanyId.DisplayMember = "COMPANY_ID"
        Me.TruckCompanyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TruckCompanyId.FormattingEnabled = True
        Me.TruckCompanyId.Location = New System.Drawing.Point(67, 215)
        Me.TruckCompanyId.Name = "TruckCompanyId"
        Me.TruckCompanyId.Size = New System.Drawing.Size(55, 21)
        Me.TruckCompanyId.TabIndex = 156
        Me.TruckCompanyId.ValueMember = "COMPANY_ID"
        '
        'TCOMPANYBindingSource
        '
        Me.TCOMPANYBindingSource.DataMember = "T_COMPANY"
        Me.TCOMPANYBindingSource.DataSource = Me.FPTDataSet
        '
        'ProductId
        '
        Me.ProductId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ProductId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ProductId.DataSource = Me.TProductBindingSource
        Me.ProductId.DisplayMember = "ID"
        Me.ProductId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ProductId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ProductId.FormattingEnabled = True
        Me.ProductId.Location = New System.Drawing.Point(16, 279)
        Me.ProductId.Name = "ProductId"
        Me.ProductId.Size = New System.Drawing.Size(53, 21)
        Me.ProductId.TabIndex = 175
        Me.ProductId.ValueMember = "ID"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(12, 77)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(86, 26)
        Me.TextBox1.TabIndex = 174
        '
        'CustomerID
        '
        Me.CustomerID.DataSource = Me.TCustomerBindingSource
        Me.CustomerID.DisplayMember = "ID"
        Me.CustomerID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CustomerID.FormattingEnabled = True
        Me.CustomerID.Location = New System.Drawing.Point(73, 235)
        Me.CustomerID.Name = "CustomerID"
        Me.CustomerID.Size = New System.Drawing.Size(55, 21)
        Me.CustomerID.TabIndex = 170
        Me.CustomerID.ValueMember = "ID"
        '
        'TCustomerBindingSource
        '
        Me.TCustomerBindingSource.DataMember = "T_Customer"
        Me.TCustomerBindingSource.DataSource = Me.FPTDataSet
        '
        'TruckId
        '
        Me.TruckId.DataSource = Me.TTRUCKBindingSource
        Me.TruckId.DisplayMember = "ID"
        Me.TruckId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TruckId.FormattingEnabled = True
        Me.TruckId.Location = New System.Drawing.Point(6, 147)
        Me.TruckId.Name = "TruckId"
        Me.TruckId.Size = New System.Drawing.Size(55, 21)
        Me.TruckId.TabIndex = 154
        Me.TruckId.ValueMember = "ID"
        '
        'TTRUCKBindingSource
        '
        Me.TTRUCKBindingSource.DataMember = "T_TRUCK"
        Me.TTRUCKBindingSource.DataSource = Me.FPTDataSet
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(67, 192)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 20)
        Me.Label11.TabIndex = 167
        Me.Label11.Text = "Shipment No.:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label11.Visible = False
        '
        'ShipmentNo
        '
        Me.ShipmentNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ShipmentNo.Location = New System.Drawing.Point(17, 8)
        Me.ShipmentNo.Name = "ShipmentNo"
        Me.ShipmentNo.Size = New System.Drawing.Size(69, 26)
        Me.ShipmentNo.TabIndex = 166
        Me.ShipmentNo.Visible = False
        '
        'ShipperId
        '
        Me.ShipperId.DataSource = Me.TSHIPPERBindingSource
        Me.ShipperId.DisplayMember = "ID"
        Me.ShipperId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ShipperId.FormattingEnabled = True
        Me.ShipperId.Location = New System.Drawing.Point(56, 138)
        Me.ShipperId.Name = "ShipperId"
        Me.ShipperId.Size = New System.Drawing.Size(55, 21)
        Me.ShipperId.TabIndex = 155
        Me.ShipperId.ValueMember = "ID"
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'GiDate
        '
        Me.GiDate.CustomFormat = ""
        Me.GiDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GiDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.GiDate.Location = New System.Drawing.Point(16, 102)
        Me.GiDate.Name = "GiDate"
        Me.GiDate.Size = New System.Drawing.Size(112, 26)
        Me.GiDate.TabIndex = 12
        '
        'Coa
        '
        Me.Coa.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Coa.Location = New System.Drawing.Point(2, 55)
        Me.Coa.Name = "Coa"
        Me.Coa.Size = New System.Drawing.Size(102, 26)
        Me.Coa.TabIndex = 10
        '
        'EDCustomer
        '
        Me.EDCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDCustomer.DataSource = Me.TCustomerBindingSource
        Me.EDCustomer.DisplayMember = "Customer_code"
        Me.EDCustomer.DropDownWidth = 300
        Me.EDCustomer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDCustomer.FormattingEnabled = True
        Me.EDCustomer.Location = New System.Drawing.Point(156, 127)
        Me.EDCustomer.Name = "EDCustomer"
        Me.EDCustomer.Size = New System.Drawing.Size(180, 28)
        Me.EDCustomer.TabIndex = 5
        Me.EDCustomer.ValueMember = "ID"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label29.Location = New System.Drawing.Point(81, 131)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(69, 20)
        Me.Label29.TabIndex = 172
        Me.Label29.Text = "Vender :"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox17)
        Me.GroupBox4.Controls.Add(Me.DBGrid1)
        Me.GroupBox4.Controls.Add(Me.GroupBox2)
        Me.GroupBox4.Controls.Add(Me.GroupBox1)
        Me.GroupBox4.Controls.Add(Me.GroupBox3)
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1285, 692)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.BindingNavigator2)
        Me.GroupBox17.Controls.Add(Me.DatetimeDBGrid1)
        Me.GroupBox17.Controls.Add(Me.Label5)
        Me.GroupBox17.Location = New System.Drawing.Point(6, 450)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(539, 66)
        Me.GroupBox17.TabIndex = 4
        Me.GroupBox17.TabStop = False
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.BindingSource = Me.V_UNLOADINGNOTEBindingSource
        Me.BindingNavigator2.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.EDFillter})
        Me.BindingNavigator2.Location = New System.Drawing.Point(205, 23)
        Me.BindingNavigator2.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator2.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator2.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator2.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator2.Size = New System.Drawing.Size(311, 25)
        Me.BindingNavigator2.TabIndex = 212
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'V_UNLOADINGNOTEBindingSource
        '
        Me.V_UNLOADINGNOTEBindingSource.AllowNew = True
        Me.V_UNLOADINGNOTEBindingSource.DataMember = "V_UNLOADINGNOTE"
        Me.V_UNLOADINGNOTEBindingSource.DataSource = Me.FPTDataSet
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 25)
        '
        'DatetimeDBGrid1
        '
        Me.DatetimeDBGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DatetimeDBGrid1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DatetimeDBGrid1.Location = New System.Drawing.Point(65, 23)
        Me.DatetimeDBGrid1.Name = "DatetimeDBGrid1"
        Me.DatetimeDBGrid1.Size = New System.Drawing.Size(137, 26)
        Me.DatetimeDBGrid1.TabIndex = 205
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(7, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 20)
        Me.Label5.TabIndex = 211
        Me.Label5.Text = "Date :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'DBGrid1
        '
        Me.DBGrid1.AllowUserToAddRows = False
        Me.DBGrid1.AllowUserToDeleteRows = False
        Me.DBGrid1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.DBGrid1.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LOAD_DID, Me.LOAD_DOfull, Me.Product_name, Me.STATUS_NAME, Me.Customer_name, Me.SP_Code, Me.LOAD_DRIVER, Me.LOAD_PRESET, Me.LOAD_VEHICLE, Me.LC_COMPARTMENT, Me.LOAD_TANK, Me.LOADDATEDataGridViewTextBoxColumn, Me.LCBAYDataGridViewTextBoxColumn, Me.ReferenceDataGridViewTextBoxColumn})
        Me.DBGrid1.DataSource = Me.V_UNLOADINGNOTEBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DBGrid1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DBGrid1.Location = New System.Drawing.Point(6, 522)
        Me.DBGrid1.Name = "DBGrid1"
        Me.DBGrid1.ReadOnly = True
        Me.DBGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DBGrid1.Size = New System.Drawing.Size(1272, 163)
        Me.DBGrid1.TabIndex = 7
        '
        'LOAD_DID
        '
        Me.LOAD_DID.DataPropertyName = "LOAD_DID"
        Me.LOAD_DID.HeaderText = "Load NO."
        Me.LOAD_DID.Name = "LOAD_DID"
        Me.LOAD_DID.ReadOnly = True
        '
        'LOAD_DOfull
        '
        Me.LOAD_DOfull.DataPropertyName = "LOAD_DOfull"
        Me.LOAD_DOfull.HeaderText = "DO No."
        Me.LOAD_DOfull.Name = "LOAD_DOfull"
        Me.LOAD_DOfull.ReadOnly = True
        '
        'Product_name
        '
        Me.Product_name.DataPropertyName = "Product_name"
        Me.Product_name.HeaderText = "Product"
        Me.Product_name.Name = "Product_name"
        Me.Product_name.ReadOnly = True
        '
        'STATUS_NAME
        '
        Me.STATUS_NAME.DataPropertyName = "STATUS_NAME"
        Me.STATUS_NAME.HeaderText = "Status"
        Me.STATUS_NAME.Name = "STATUS_NAME"
        Me.STATUS_NAME.ReadOnly = True
        '
        'Customer_name
        '
        Me.Customer_name.DataPropertyName = "Customer_name"
        Me.Customer_name.HeaderText = "Vender"
        Me.Customer_name.Name = "Customer_name"
        Me.Customer_name.ReadOnly = True
        '
        'SP_Code
        '
        Me.SP_Code.DataPropertyName = "SP_Code"
        Me.SP_Code.HeaderText = "Company"
        Me.SP_Code.Name = "SP_Code"
        Me.SP_Code.ReadOnly = True
        '
        'LOAD_DRIVER
        '
        Me.LOAD_DRIVER.DataPropertyName = "LOAD_DRIVER"
        Me.LOAD_DRIVER.HeaderText = "Driver"
        Me.LOAD_DRIVER.Name = "LOAD_DRIVER"
        Me.LOAD_DRIVER.ReadOnly = True
        '
        'LOAD_PRESET
        '
        Me.LOAD_PRESET.DataPropertyName = "LOAD_PRESET"
        Me.LOAD_PRESET.FillWeight = 120.0!
        Me.LOAD_PRESET.HeaderText = "Preset"
        Me.LOAD_PRESET.Name = "LOAD_PRESET"
        Me.LOAD_PRESET.ReadOnly = True
        '
        'LOAD_VEHICLE
        '
        Me.LOAD_VEHICLE.DataPropertyName = "LOAD_VEHICLE"
        Me.LOAD_VEHICLE.HeaderText = "Vehicle"
        Me.LOAD_VEHICLE.Name = "LOAD_VEHICLE"
        Me.LOAD_VEHICLE.ReadOnly = True
        '
        'LC_COMPARTMENT
        '
        Me.LC_COMPARTMENT.DataPropertyName = "LC_COMPARTMENT"
        Me.LC_COMPARTMENT.HeaderText = "Compartment"
        Me.LC_COMPARTMENT.Name = "LC_COMPARTMENT"
        Me.LC_COMPARTMENT.ReadOnly = True
        '
        'LOAD_TANK
        '
        Me.LOAD_TANK.DataPropertyName = "LOCATION_CODE"
        Me.LOAD_TANK.HeaderText = "Storage Location"
        Me.LOAD_TANK.Name = "LOAD_TANK"
        Me.LOAD_TANK.ReadOnly = True
        Me.LOAD_TANK.Width = 150
        '
        'LOADDATEDataGridViewTextBoxColumn
        '
        Me.LOADDATEDataGridViewTextBoxColumn.DataPropertyName = "LOAD_DATE"
        Me.LOADDATEDataGridViewTextBoxColumn.HeaderText = "Load date"
        Me.LOADDATEDataGridViewTextBoxColumn.Name = "LOADDATEDataGridViewTextBoxColumn"
        Me.LOADDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LCBAYDataGridViewTextBoxColumn
        '
        Me.LCBAYDataGridViewTextBoxColumn.DataPropertyName = "LC_BAY"
        Me.LCBAYDataGridViewTextBoxColumn.HeaderText = "bay"
        Me.LCBAYDataGridViewTextBoxColumn.Name = "LCBAYDataGridViewTextBoxColumn"
        Me.LCBAYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ReferenceDataGridViewTextBoxColumn
        '
        Me.ReferenceDataGridViewTextBoxColumn.DataPropertyName = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.HeaderText = "Ref."
        Me.ReferenceDataGridViewTextBoxColumn.Name = "ReferenceDataGridViewTextBoxColumn"
        Me.ReferenceDataGridViewTextBoxColumn.ReadOnly = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.BTWeightOut)
        Me.GroupBox2.Controls.Add(Me.BTWeightIn)
        Me.GroupBox2.Controls.Add(Me.BTupdate)
        Me.GroupBox2.Controls.Add(Me.BTcancel)
        Me.GroupBox2.Controls.Add(Me.BTsap)
        Me.GroupBox2.Controls.Add(Me.BTsave)
        Me.GroupBox2.Location = New System.Drawing.Point(855, 450)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(424, 67)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Visible = False
        '
        'BTWeightOut
        '
        Me.BTWeightOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTWeightOut.Image = Global.TAS_TOL.My.Resources.Resources.weight
        Me.BTWeightOut.Location = New System.Drawing.Point(103, 14)
        Me.BTWeightOut.Name = "BTWeightOut"
        Me.BTWeightOut.Size = New System.Drawing.Size(120, 47)
        Me.BTWeightOut.TabIndex = 145
        Me.BTWeightOut.Text = "Check Out weight"
        Me.BTWeightOut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTWeightOut.UseVisualStyleBackColor = True
        '
        'BTWeightIn
        '
        Me.BTWeightIn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTWeightIn.Image = Global.TAS_TOL.My.Resources.Resources.weight
        Me.BTWeightIn.Location = New System.Drawing.Point(103, 14)
        Me.BTWeightIn.Name = "BTWeightIn"
        Me.BTWeightIn.Size = New System.Drawing.Size(120, 47)
        Me.BTWeightIn.TabIndex = 143
        Me.BTWeightIn.Text = "Check in weight"
        Me.BTWeightIn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTWeightIn.UseVisualStyleBackColor = True
        '
        'BTupdate
        '
        Me.BTupdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTupdate.Image = Global.TAS_TOL.My.Resources.Resources.gtk_refresh
        Me.BTupdate.Location = New System.Drawing.Point(229, 14)
        Me.BTupdate.Name = "BTupdate"
        Me.BTupdate.Size = New System.Drawing.Size(91, 47)
        Me.BTupdate.TabIndex = 15
        Me.BTupdate.Text = "&Update"
        Me.BTupdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTupdate.UseVisualStyleBackColor = True
        '
        'BTcancel
        '
        Me.BTcancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BTcancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTcancel.Image = Global.TAS_TOL.My.Resources.Resources.Button_Cancel
        Me.BTcancel.Location = New System.Drawing.Point(326, 14)
        Me.BTcancel.Name = "BTcancel"
        Me.BTcancel.Size = New System.Drawing.Size(91, 47)
        Me.BTcancel.TabIndex = 16
        Me.BTcancel.Tag = "21"
        Me.BTcancel.Text = "&Cancel"
        Me.BTcancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTcancel.UseVisualStyleBackColor = True
        '
        'BTsap
        '
        Me.BTsap.Enabled = False
        Me.BTsap.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTsap.Image = Global.TAS_TOL.My.Resources.Resources.sap
        Me.BTsap.Location = New System.Drawing.Point(6, 14)
        Me.BTsap.Name = "BTsap"
        Me.BTsap.Size = New System.Drawing.Size(91, 47)
        Me.BTsap.TabIndex = 144
        Me.BTsap.Text = "&Data"
        Me.BTsap.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTsap.UseVisualStyleBackColor = True
        '
        'BTsave
        '
        Me.BTsave.Image = Global.TAS_TOL.My.Resources.Resources.document_save
        Me.BTsave.Location = New System.Drawing.Point(229, 14)
        Me.BTsave.Name = "BTsave"
        Me.BTsave.Size = New System.Drawing.Size(91, 47)
        Me.BTsave.TabIndex = 14
        Me.BTsave.Text = "&Save"
        Me.BTsave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTsave.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Btprint)
        Me.GroupBox1.Controls.Add(Me.Btedit)
        Me.GroupBox1.Controls.Add(Me.Btadd)
        Me.GroupBox1.Location = New System.Drawing.Point(551, 450)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(298, 67)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'Btprint
        '
        Me.Btprint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btprint.Image = Global.TAS_TOL.My.Resources.Resources.Printer_2
        Me.Btprint.Location = New System.Drawing.Point(201, 14)
        Me.Btprint.Name = "Btprint"
        Me.Btprint.Size = New System.Drawing.Size(91, 47)
        Me.Btprint.TabIndex = 3
        Me.Btprint.Text = "&Print"
        Me.Btprint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Btprint.UseVisualStyleBackColor = True
        '
        'Btedit
        '
        Me.Btedit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btedit.Image = Global.TAS_TOL.My.Resources.Resources.edit_32
        Me.Btedit.Location = New System.Drawing.Point(104, 14)
        Me.Btedit.Name = "Btedit"
        Me.Btedit.Size = New System.Drawing.Size(91, 47)
        Me.Btedit.TabIndex = 2
        Me.Btedit.Text = "&Edit"
        Me.Btedit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Btedit.UseVisualStyleBackColor = True
        '
        'Btadd
        '
        Me.Btadd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Btadd.Image = Global.TAS_TOL.My.Resources.Resources.add_1
        Me.Btadd.Location = New System.Drawing.Point(7, 14)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.Size = New System.Drawing.Size(91, 47)
        Me.Btadd.TabIndex = 1
        Me.Btadd.Text = "&Add"
        Me.Btadd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Btadd.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BtVehicleEdit)
        Me.GroupBox3.Controls.Add(Me.CBCardNO)
        Me.GroupBox3.Controls.Add(Me.Label69)
        Me.GroupBox3.Controls.Add(Me.GroupBox6)
        Me.GroupBox3.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox3.Controls.Add(Me.Label50)
        Me.GroupBox3.Controls.Add(Me.WeightScal)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.TextBox9)
        Me.GroupBox3.Controls.Add(Me.Label45)
        Me.GroupBox3.Controls.Add(Me.EDLocation)
        Me.GroupBox3.Controls.Add(Me.Label46)
        Me.GroupBox3.Controls.Add(Me.Label47)
        Me.GroupBox3.Controls.Add(Me.TextBox10)
        Me.GroupBox3.Controls.Add(Me.Quantity)
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.EDPacking)
        Me.GroupBox3.Controls.Add(Me.Label32)
        Me.GroupBox3.Controls.Add(Me.Cbn10)
        Me.GroupBox3.Controls.Add(Me.EDProduct)
        Me.GroupBox3.Controls.Add(Me.Label34)
        Me.GroupBox3.Controls.Add(Me.EDCustomer)
        Me.GroupBox3.Controls.Add(Me.Label29)
        Me.GroupBox3.Controls.Add(Me.Invoice)
        Me.GroupBox3.Controls.Add(Me.PO_No)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Prono)
        Me.GroupBox3.Controls.Add(Me.ComboBox1)
        Me.GroupBox3.Controls.Add(Me.DateeditTemp)
        Me.GroupBox3.Controls.Add(Me.Dateedit)
        Me.GroupBox3.Controls.Add(Me.Container)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.Label27)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.Batchlot)
        Me.GroupBox3.Controls.Add(Me.Cbn11)
        Me.GroupBox3.Controls.Add(Me.Cbn8)
        Me.GroupBox3.Controls.Add(Me.Cbn7)
        Me.GroupBox3.Controls.Add(Me.EDShipper)
        Me.GroupBox3.Controls.Add(Me.EDDriver)
        Me.GroupBox3.Controls.Add(Me.EDTransNO)
        Me.GroupBox3.Controls.Add(Me.EDVehicle)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Cbn5)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(6, 15)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(816, 432)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Unload Detail"
        '
        'BtVehicleEdit
        '
        Me.BtVehicleEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtVehicleEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BtVehicleEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtVehicleEdit.Location = New System.Drawing.Point(269, 8)
        Me.BtVehicleEdit.Name = "BtVehicleEdit"
        Me.BtVehicleEdit.Size = New System.Drawing.Size(67, 20)
        Me.BtVehicleEdit.TabIndex = 2106
        Me.BtVehicleEdit.Text = "Vehicle Edit"
        Me.BtVehicleEdit.UseVisualStyleBackColor = True
        '
        'CBCardNO
        '
        Me.CBCardNO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBCardNO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBCardNO.DataSource = Me.VCARDFREEBindingSource
        Me.CBCardNO.DisplayMember = "CARD_NUMBER"
        Me.CBCardNO.DropDownWidth = 100
        Me.CBCardNO.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CBCardNO.FormattingEnabled = True
        Me.CBCardNO.Location = New System.Drawing.Point(629, 225)
        Me.CBCardNO.Name = "CBCardNO"
        Me.CBCardNO.Size = New System.Drawing.Size(180, 28)
        Me.CBCardNO.TabIndex = 2105
        Me.CBCardNO.ValueMember = "CARD_NUMBER"
        '
        'VCARDFREEBindingSource
        '
        Me.VCARDFREEBindingSource.DataMember = "V_CARDFREE"
        Me.VCARDFREEBindingSource.DataSource = Me.FPTDataSet
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label69.Location = New System.Drawing.Point(534, 228)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(79, 20)
        Me.Label69.TabIndex = 2104
        Me.Label69.Text = "Card No. :"
        Me.Label69.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = ""
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(629, 126)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(180, 26)
        Me.DateTimePicker1.TabIndex = 17
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(718, 371)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(87, 52)
        Me.Label50.TabIndex = 191
        Me.Label50.Text = "Kg."
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'WeightScal
        '
        Me.WeightScal.BackColor = System.Drawing.SystemColors.MenuText
        Me.WeightScal.Font = New System.Drawing.Font("DS-Digital", 33.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WeightScal.ForeColor = System.Drawing.Color.Lime
        Me.WeightScal.Location = New System.Drawing.Point(533, 368)
        Me.WeightScal.Name = "WeightScal"
        Me.WeightScal.Size = New System.Drawing.Size(179, 52)
        Me.WeightScal.TabIndex = 22
        Me.WeightScal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(487, 161)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 20)
        Me.Label4.TabIndex = 90
        Me.Label4.Text = "Unload Of Date :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(536, 288)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 20)
        Me.Label10.TabIndex = 96
        Me.Label10.Text = "Batch Lot :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.Label10.Visible = False
        '
        'Invoice
        '
        Me.Invoice.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Invoice.Location = New System.Drawing.Point(156, 289)
        Me.Invoice.Name = "Invoice"
        Me.Invoice.Size = New System.Drawing.Size(180, 26)
        Me.Invoice.TabIndex = 10
        '
        'PO_No
        '
        Me.PO_No.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PO_No.Location = New System.Drawing.Point(156, 227)
        Me.PO_No.Name = "PO_No"
        Me.PO_No.Size = New System.Drawing.Size(180, 26)
        Me.PO_No.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(77, 263)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 20)
        Me.Label6.TabIndex = 161
        Me.Label6.Text = "D/O No. :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Prono
        '
        Me.Prono.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Prono.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Prono.FormattingEnabled = True
        Me.Prono.Location = New System.Drawing.Point(322, 474)
        Me.Prono.Name = "Prono"
        Me.Prono.Size = New System.Drawing.Size(450, 21)
        Me.Prono.TabIndex = 160
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"a", "b", "c"})
        Me.ComboBox1.Location = New System.Drawing.Point(336, 550)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 153
        '
        'DateeditTemp
        '
        Me.DateeditTemp.Location = New System.Drawing.Point(322, 501)
        Me.DateeditTemp.Name = "DateeditTemp"
        Me.DateeditTemp.Size = New System.Drawing.Size(156, 20)
        Me.DateeditTemp.TabIndex = 139
        Me.DateeditTemp.Visible = False
        '
        'Dateedit
        '
        Me.Dateedit.Enabled = False
        Me.Dateedit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Dateedit.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateedit.Location = New System.Drawing.Point(629, 91)
        Me.Dateedit.Name = "Dateedit"
        Me.Dateedit.Size = New System.Drawing.Size(180, 26)
        Me.Dateedit.TabIndex = 16
        '
        'Container
        '
        Me.Container.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Container.Location = New System.Drawing.Point(342, 30)
        Me.Container.Name = "Container"
        Me.Container.Size = New System.Drawing.Size(133, 26)
        Me.Container.TabIndex = 2
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label31.Location = New System.Drawing.Point(343, 8)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(106, 20)
        Me.Label31.TabIndex = 147
        Me.Label31.Text = "Container No."
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.Location = New System.Drawing.Point(83, 296)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(67, 20)
        Me.Label27.TabIndex = 140
        Me.Label27.Text = "Invoice :"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.Location = New System.Drawing.Point(73, 395)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(77, 20)
        Me.Label26.TabIndex = 138
        Me.Label26.Text = "D/O Qty. :"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.Location = New System.Drawing.Point(513, 131)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(102, 20)
        Me.Label25.TabIndex = 136
        Me.Label25.Text = "Modify Date :"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Batchlot
        '
        Me.Batchlot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Batchlot.Location = New System.Drawing.Point(636, 283)
        Me.Batchlot.Name = "Batchlot"
        Me.Batchlot.Size = New System.Drawing.Size(180, 26)
        Me.Batchlot.TabIndex = 9
        Me.Batchlot.Visible = False
        '
        'Cbn11
        '
        Me.Cbn11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Cbn11.Location = New System.Drawing.Point(156, 258)
        Me.Cbn11.Name = "Cbn11"
        Me.Cbn11.Size = New System.Drawing.Size(180, 26)
        Me.Cbn11.TabIndex = 9
        '
        'Cbn8
        '
        Me.Cbn8.Enabled = False
        Me.Cbn8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Cbn8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Cbn8.Location = New System.Drawing.Point(629, 27)
        Me.Cbn8.Name = "Cbn8"
        Me.Cbn8.ReadOnly = True
        Me.Cbn8.Size = New System.Drawing.Size(180, 26)
        Me.Cbn8.TabIndex = 14
        '
        'Cbn7
        '
        Me.Cbn7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Cbn7.Location = New System.Drawing.Point(629, 158)
        Me.Cbn7.Name = "Cbn7"
        Me.Cbn7.ReadOnly = True
        Me.Cbn7.Size = New System.Drawing.Size(180, 26)
        Me.Cbn7.TabIndex = 18
        '
        'EDShipper
        '
        Me.EDShipper.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDShipper.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDShipper.DataSource = Me.TSHIPPERBindingSource
        Me.EDShipper.DisplayMember = "SP_NameEN"
        Me.EDShipper.DropDownWidth = 300
        Me.EDShipper.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDShipper.FormattingEnabled = True
        Me.EDShipper.Location = New System.Drawing.Point(156, 95)
        Me.EDShipper.Name = "EDShipper"
        Me.EDShipper.Size = New System.Drawing.Size(180, 28)
        Me.EDShipper.TabIndex = 4
        Me.EDShipper.ValueMember = "ID"
        '
        'EDDriver
        '
        Me.EDDriver.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDDriver.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDDriver.DataSource = Me.TDRIVERBindingSource
        Me.EDDriver.DisplayMember = "Driver_NAME"
        Me.EDDriver.DropDownWidth = 300
        Me.EDDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDDriver.FormattingEnabled = True
        Me.EDDriver.Location = New System.Drawing.Point(156, 62)
        Me.EDDriver.Name = "EDDriver"
        Me.EDDriver.Size = New System.Drawing.Size(180, 28)
        Me.EDDriver.TabIndex = 3
        Me.EDDriver.ValueMember = "ID"
        '
        'EDTransNO
        '
        Me.EDTransNO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDTransNO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDTransNO.DataSource = Me.TCOMPANYBindingSource
        Me.EDTransNO.DisplayMember = "COMPANY_NAME"
        Me.EDTransNO.DropDownWidth = 300
        Me.EDTransNO.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDTransNO.FormattingEnabled = True
        Me.EDTransNO.Location = New System.Drawing.Point(156, 161)
        Me.EDTransNO.Name = "EDTransNO"
        Me.EDTransNO.Size = New System.Drawing.Size(180, 28)
        Me.EDTransNO.TabIndex = 6
        Me.EDTransNO.ValueMember = "COMPANY_ID"
        '
        'EDVehicle
        '
        Me.EDVehicle.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDVehicle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDVehicle.DataSource = Me.TTRUCKBindingSource
        Me.EDVehicle.DisplayMember = "TRUCK_NUMBER"
        Me.EDVehicle.DropDownWidth = 300
        Me.EDVehicle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDVehicle.FormattingEnabled = True
        Me.EDVehicle.Location = New System.Drawing.Point(156, 29)
        Me.EDVehicle.Name = "EDVehicle"
        Me.EDVehicle.Size = New System.Drawing.Size(180, 28)
        Me.EDVehicle.TabIndex = 1
        Me.EDVehicle.ValueMember = "ID"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(79, 230)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(71, 20)
        Me.Label18.TabIndex = 114
        Me.Label18.Text = "P/O No. :"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(481, 62)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(134, 20)
        Me.Label15.TabIndex = 111
        Me.Label15.Text = "Vehicle Capacity :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(52, 98)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(98, 20)
        Me.Label14.TabIndex = 110
        Me.Label14.Text = "Sales ORG :"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(500, 94)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(115, 20)
        Me.Label12.TabIndex = 97
        Me.Label12.Text = "Account Date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(523, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(92, 20)
        Me.Label9.TabIndex = 93
        Me.Label9.Text = "Reference :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Cbn5
        '
        Me.Cbn5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Cbn5.Location = New System.Drawing.Point(629, 59)
        Me.Cbn5.Name = "Cbn5"
        Me.Cbn5.ReadOnly = True
        Me.Cbn5.Size = New System.Drawing.Size(180, 26)
        Me.Cbn5.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(53, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 20)
        Me.Label1.TabIndex = 83
        Me.Label1.Text = "Vehicle No. :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 164)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 20)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "Forwarding Agent :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(46, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 20)
        Me.Label3.TabIndex = 89
        Me.Label3.Text = "Driver Name :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label44)
        Me.GroupBox5.Controls.Add(Me.Label52)
        Me.GroupBox5.Controls.Add(Me.Label51)
        Me.GroupBox5.Controls.Add(Me.Weightintime)
        Me.GroupBox5.Controls.Add(Me.Label43)
        Me.GroupBox5.Controls.Add(Me.Weighttol)
        Me.GroupBox5.Controls.Add(Me.weightouttime)
        Me.GroupBox5.Controls.Add(Me.Label42)
        Me.GroupBox5.Controls.Add(Me.Netweight)
        Me.GroupBox5.Controls.Add(Me.Label41)
        Me.GroupBox5.Controls.Add(Me.PackingWeight)
        Me.GroupBox5.Controls.Add(Me.Label40)
        Me.GroupBox5.Controls.Add(Me.WeightTotal)
        Me.GroupBox5.Controls.Add(Me.Button6)
        Me.GroupBox5.Controls.Add(Me.Button5)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Controls.Add(Me.Status)
        Me.GroupBox5.Controls.Add(Me.Label30)
        Me.GroupBox5.Controls.Add(Me.LawWeightIn)
        Me.GroupBox5.Controls.Add(Me.UpdateWeightIn)
        Me.GroupBox5.Controls.Add(Me.UpdateWeightOut)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.LawWeightout)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GroupBox5.Location = New System.Drawing.Point(828, 15)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(450, 432)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Weight"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label44.Location = New System.Drawing.Point(370, 199)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(32, 20)
        Me.Label44.TabIndex = 188
        Me.Label44.Text = "Kg."
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label52.Location = New System.Drawing.Point(47, 68)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(128, 20)
        Me.Label52.TabIndex = 202
        Me.Label52.Text = "Weight-In Time  :"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label51.Location = New System.Drawing.Point(35, 164)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(140, 20)
        Me.Label51.TabIndex = 203
        Me.Label51.Text = "Weight-Out Time  :"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Weightintime
        '
        Me.Weightintime.Enabled = False
        Me.Weightintime.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Weightintime.Location = New System.Drawing.Point(181, 65)
        Me.Weightintime.Name = "Weightintime"
        Me.Weightintime.Size = New System.Drawing.Size(179, 26)
        Me.Weightintime.TabIndex = 1
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label43.Location = New System.Drawing.Point(38, 353)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(137, 20)
        Me.Label43.TabIndex = 187
        Me.Label43.Text = "Weight Tolerance:"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Weighttol
        '
        Me.Weighttol.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Weighttol.Location = New System.Drawing.Point(181, 353)
        Me.Weighttol.Name = "Weighttol"
        Me.Weighttol.Size = New System.Drawing.Size(179, 26)
        Me.Weighttol.TabIndex = 12
        '
        'weightouttime
        '
        Me.weightouttime.Enabled = False
        Me.weightouttime.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.weightouttime.Location = New System.Drawing.Point(181, 161)
        Me.weightouttime.Name = "weightouttime"
        Me.weightouttime.Size = New System.Drawing.Size(179, 26)
        Me.weightouttime.TabIndex = 5
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label42.Location = New System.Drawing.Point(79, 321)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(96, 20)
        Me.Label42.TabIndex = 185
        Me.Label42.Text = "Net Weight :"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Netweight
        '
        Me.Netweight.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Netweight.Location = New System.Drawing.Point(181, 321)
        Me.Netweight.Name = "Netweight"
        Me.Netweight.Size = New System.Drawing.Size(179, 26)
        Me.Netweight.TabIndex = 11
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label41.Location = New System.Drawing.Point(48, 289)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(127, 20)
        Me.Label41.TabIndex = 183
        Me.Label41.Text = "Packing Weight :"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PackingWeight
        '
        Me.PackingWeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PackingWeight.Location = New System.Drawing.Point(181, 290)
        Me.PackingWeight.Name = "PackingWeight"
        Me.PackingWeight.Size = New System.Drawing.Size(179, 26)
        Me.PackingWeight.TabIndex = 10
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label40.Location = New System.Drawing.Point(69, 257)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(106, 20)
        Me.Label40.TabIndex = 181
        Me.Label40.Text = "Total Weight :"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'WeightTotal
        '
        Me.WeightTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.WeightTotal.Location = New System.Drawing.Point(181, 257)
        Me.WeightTotal.Name = "WeightTotal"
        Me.WeightTotal.Size = New System.Drawing.Size(179, 26)
        Me.WeightTotal.TabIndex = 9
        '
        'Button6
        '
        Me.Button6.BackgroundImage = CType(resources.GetObject("Button6.BackgroundImage"), System.Drawing.Image)
        Me.Button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button6.Location = New System.Drawing.Point(367, 225)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(45, 26)
        Me.Button6.TabIndex = 7
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.BackgroundImage = CType(resources.GetObject("Button5.BackgroundImage"), System.Drawing.Image)
        Me.Button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button5.Location = New System.Drawing.Point(367, 124)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(45, 26)
        Me.Button5.TabIndex = 3
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.Location = New System.Drawing.Point(370, 97)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 20)
        Me.Label17.TabIndex = 166
        Me.Label17.Text = "Kg."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(54, 100)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(121, 20)
        Me.Label13.TabIndex = 95
        Me.Label13.Text = "Raw Weight In :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label24.Location = New System.Drawing.Point(33, 131)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(142, 20)
        Me.Label24.TabIndex = 95
        Me.Label24.Text = "Update Weight In :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Status
        '
        Me.Status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Status.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Status.DataSource = Me.TSTATUSBindingSource
        Me.Status.DisplayMember = "STATUS_NAME"
        Me.Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Status.FormattingEnabled = True
        Me.Status.Location = New System.Drawing.Point(181, 29)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(180, 28)
        Me.Status.TabIndex = 0
        Me.Status.ValueMember = "STATUS_ID"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(105, 31)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(70, 24)
        Me.Label30.TabIndex = 175
        Me.Label30.Text = "Status :"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LawWeightIn
        '
        Me.LawWeightIn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LawWeightIn.Location = New System.Drawing.Point(181, 97)
        Me.LawWeightIn.Name = "LawWeightIn"
        Me.LawWeightIn.ReadOnly = True
        Me.LawWeightIn.Size = New System.Drawing.Size(179, 26)
        Me.LawWeightIn.TabIndex = 2
        '
        'UpdateWeightIn
        '
        Me.UpdateWeightIn.Enabled = False
        Me.UpdateWeightIn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UpdateWeightIn.Location = New System.Drawing.Point(181, 128)
        Me.UpdateWeightIn.Name = "UpdateWeightIn"
        Me.UpdateWeightIn.Size = New System.Drawing.Size(179, 26)
        Me.UpdateWeightIn.TabIndex = 4
        '
        'UpdateWeightOut
        '
        Me.UpdateWeightOut.Enabled = False
        Me.UpdateWeightOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UpdateWeightOut.Location = New System.Drawing.Point(181, 225)
        Me.UpdateWeightOut.Name = "UpdateWeightOut"
        Me.UpdateWeightOut.Size = New System.Drawing.Size(179, 26)
        Me.UpdateWeightOut.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(42, 196)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(133, 20)
        Me.Label8.TabIndex = 164
        Me.Label8.Text = "Raw Weight Out :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LawWeightout
        '
        Me.LawWeightout.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LawWeightout.Location = New System.Drawing.Point(181, 193)
        Me.LawWeightout.Name = "LawWeightout"
        Me.LawWeightout.ReadOnly = True
        Me.LawWeightout.Size = New System.Drawing.Size(179, 26)
        Me.LawWeightout.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(21, 228)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(154, 20)
        Me.Label7.TabIndex = 163
        Me.Label7.Text = "Update Weight Out :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'V_LoadingnoteBindingSource
        '
        Me.V_LoadingnoteBindingSource.DataMember = "V_LOADINGNOTE"
        Me.V_LoadingnoteBindingSource.DataSource = Me.FPTDataSet
        '
        'T_DRIVERTableAdapter
        '
        Me.T_DRIVERTableAdapter.ClearBeforeFill = True
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem1.Text = "ToolStripMenuItem1"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(183, 26)
        '
        'T_COMPANYTableAdapter
        '
        Me.T_COMPANYTableAdapter.ClearBeforeFill = True
        '
        'T_TRUCKTableAdapter
        '
        Me.T_TRUCKTableAdapter.ClearBeforeFill = True
        '
        'V_LOADINGNOTETableAdapter
        '
        Me.V_LOADINGNOTETableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'V_UNLOADINGNOTETableAdapter
        '
        Me.V_UNLOADINGNOTETableAdapter.ClearBeforeFill = True
        '
        'T_LOCATIONTableAdapter
        '
        Me.T_LOCATIONTableAdapter.ClearBeforeFill = True
        '
        'T_STATUSTableAdapter
        '
        Me.T_STATUSTableAdapter.ClearBeforeFill = True
        '
        'T_COMPANYTableAdapter1
        '
        Me.T_COMPANYTableAdapter1.ClearBeforeFill = True
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "T_COMPANY"
        Me.BindingSource1.DataSource = Me.FPTDataSet
        '
        'T_DRIVERTableAdapter1
        '
        Me.T_DRIVERTableAdapter1.ClearBeforeFill = True
        '
        'BindingSource2
        '
        Me.BindingSource2.DataMember = "T_DRIVER"
        Me.BindingSource2.DataSource = Me.FPTDataSet
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'T_CustomerTableAdapter
        '
        Me.T_CustomerTableAdapter.ClearBeforeFill = True
        '
        'T_PACKINGTableAdapter
        '
        Me.T_PACKINGTableAdapter.ClearBeforeFill = True
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'BTweightoutprint
        '
        Me.BTweightoutprint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTweightoutprint.Image = Global.TAS_TOL.My.Resources.Resources.Printer_21
        Me.BTweightoutprint.Location = New System.Drawing.Point(424, 383)
        Me.BTweightoutprint.Name = "BTweightoutprint"
        Me.BTweightoutprint.Size = New System.Drawing.Size(107, 50)
        Me.BTweightoutprint.TabIndex = 23
        Me.BTweightoutprint.Text = "Weight Out Print"
        Me.BTweightoutprint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTweightoutprint.UseVisualStyleBackColor = True
        '
        'V_CARDFREETableAdapter
        '
        Me.V_CARDFREETableAdapter.ClearBeforeFill = True
        '
        'Unloading
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.CancelButton = Me.BTcancel
        Me.ClientSize = New System.Drawing.Size(1285, 692)
        Me.Controls.Add(Me.BTweightoutprint)
        Me.Controls.Add(Me.GroupBox4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Unloading"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "21"
        Me.Text = "Unloading Data Entry"
        CType(Me.TLOCATIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TPACKINGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.TSTATUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TTRUCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        CType(Me.V_UNLOADINGNOTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.VCARDFREEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.V_LoadingnoteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents EDLocation As System.Windows.Forms.ComboBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents Quantity As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents PackingId As System.Windows.Forms.ComboBox
    Friend WithEvents EDPacking As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Cbn10 As System.Windows.Forms.TextBox
    Friend WithEvents EDProduct As System.Windows.Forms.ComboBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Cbn9 As System.Windows.Forms.TextBox
    Friend WithEvents Updatedate As System.Windows.Forms.TextBox
    Friend WithEvents Cbn6 As System.Windows.Forms.TextBox
    Friend WithEvents TruckId As System.Windows.Forms.ComboBox
    Friend WithEvents ProductId As System.Windows.Forms.ComboBox
    Friend WithEvents TruckCompanyId As System.Windows.Forms.ComboBox
    Friend WithEvents StatusId As System.Windows.Forms.ComboBox
    Friend WithEvents DriverID As System.Windows.Forms.ComboBox
    Friend WithEvents ShipperId As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents EDCustomer As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents LOADCUSTOMERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCCAPACITYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCPRESETDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCHNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCISLANDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADSTATUSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddnoteDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents V_LoadingnoteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BTWeightOut As System.Windows.Forms.Button
    Friend WithEvents BTWeightIn As System.Windows.Forms.Button
    Friend WithEvents BTcancel As System.Windows.Forms.Button
    Friend WithEvents BTsap As System.Windows.Forms.Button
    Friend WithEvents BTupdate As System.Windows.Forms.Button
    Friend WithEvents BTsave As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Btprint As System.Windows.Forms.Button
    Friend WithEvents Btedit As System.Windows.Forms.Button
    Friend WithEvents Btadd As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents Invoice As System.Windows.Forms.TextBox
    Friend WithEvents ShipmentNo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PO_No As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Prono As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents GiDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateeditTemp As System.Windows.Forms.TextBox
    Friend WithEvents Dateedit As System.Windows.Forms.DateTimePicker
    Friend WithEvents Container As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Coa As System.Windows.Forms.TextBox
    Friend WithEvents Batchlot As System.Windows.Forms.TextBox
    Friend WithEvents Cbn11 As System.Windows.Forms.TextBox
    Friend WithEvents Cbn8 As System.Windows.Forms.TextBox
    Friend WithEvents Cbn7 As System.Windows.Forms.TextBox
    Friend WithEvents EDShipper As System.Windows.Forms.ComboBox
    Friend WithEvents EDDriver As System.Windows.Forms.ComboBox
    Friend WithEvents EDTransNO As System.Windows.Forms.ComboBox
    Friend WithEvents EDVehicle As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Cbn5 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Weighttol As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Netweight As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents PackingWeight As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents WeightTotal As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents LawWeightIn As System.Windows.Forms.TextBox
    Friend WithEvents UpdateWeightIn As System.Windows.Forms.TextBox
    Friend WithEvents UpdateWeightOut As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents LawWeightout As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CrystalReport1 As CrystalDecisions.CrystalReports.Engine.ReportDocument
    Friend WithEvents T_DRIVERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TTRUCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TDRIVERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_COMPANYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter
    Friend WithEvents T_TRUCKTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTableAdapter
    Friend WithEvents TCOMPANYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_LOADINGNOTETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_LOADINGNOTETableAdapter
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents WeightScal As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents DBGrid1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Weightintime As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents weightouttime As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents V_UNLOADINGNOTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents P_Weight As System.Windows.Forms.ComboBox
    Friend WithEvents TLOCATIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_LOCATIONTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_LOCATIONTableAdapter
    Friend WithEvents V_UNLOADINGNOTETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_UNLOADINGNOTETableAdapter
    Friend WithEvents DatetimeDBGrid1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TSTATUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_STATUSTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_STATUSTableAdapter
    Friend WithEvents T_COMPANYTableAdapter1 As TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TCustomerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_DRIVERTableAdapter1 As TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter
    Friend WithEvents BindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents T_CustomerTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_CustomerTableAdapter
    Friend WithEvents TPACKINGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_PACKINGTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_PACKINGTableAdapter
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents BTweightoutprint As System.Windows.Forms.Button
    Friend WithEvents CBCardNO As System.Windows.Forms.ComboBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents VCARDFREEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_CARDFREETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_CARDFREETableAdapter
    Friend WithEvents BtVehicleEdit As System.Windows.Forms.Button
    Friend WithEvents LOAD_DID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_DOfull As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Product_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Customer_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SP_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_DRIVER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_PRESET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_VEHICLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LC_COMPARTMENT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_TANK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCBAYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReferenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
