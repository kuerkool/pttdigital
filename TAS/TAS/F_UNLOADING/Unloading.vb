﻿Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Threading
Imports ExtendedErrorProvider
Imports System.Runtime.InteropServices
Imports TAS_TOL.C01_Permission

Public Class Unloading
    Public Const MOD_ALT As Integer = &H1 'Either ALT key must be held down.
    Public Const MOD_CONTROL As Integer = &H2 'Either CTRL key must be held down.
    Public Const MOD_NOREPEAT As Integer = &H4000 'Changes the hotkey behavior so that the keyboard auto-repeat does not yield multiple hotkey notifications. 
    Public Const MOD_SHIFT As Integer = &H4 'Either SHIFT key must be held down.
    Public Const MOD_WIN As Integer = &H8 'Either WINDOWS key was held down. These keys are labeled with the Windows logo. Keyboard shortcuts that involve the WINDOWS key are reserved for use by the operating system.
    Public Const WM_HOTKEY As Integer = &H312

    <DllImport("User32.dll")> _
    Public Shared Function RegisterHotKey(ByVal hwnd As IntPtr, _
                        ByVal id As Integer, ByVal fsModifiers As Integer, _
                        ByVal vk As Integer) As Integer
    End Function

    <DllImport("User32.dll")> _
    Public Shared Function UnregisterHotKey(ByVal hwnd As IntPtr, _
                        ByVal id As Integer) As Integer
    End Function


    Private MyErrorProvider As New ErrorProviderExtended
    Dim conn As SqlClient.SqlConnection = GetConnection()
    'conn.Open()
    Dim da As New SqlClient.SqlDataAdapter
    Dim ds As DataSet
    Dim dt As DataTable
    Dim ProductNo() As ComboBox
    Dim CapacityNo() As TextBox
    Dim PRESETNO() As TextBox
    Dim COMPNO() As TextBox
    Dim BayNo() As ComboBox
    Dim Meterno() As ComboBox
    Dim test() As Array
    Dim TRUCK_COMP_NUM, sum As Integer

    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_HOTKEY Then
            Dim id As IntPtr = m.WParam
            Select Case (id.ToString)
                Case "100" 'Add
                    If Btadd.Enabled = True Then
                        Btadd.PerformClick()
                    End If

                Case "101" 'Edit
                    If Btedit.Enabled = True Then
                        Btedit.PerformClick()
                    End If
                Case "102" 'Print
                    If Btprint.Enabled = True Then
                        Btprint.PerformClick()
                    End If
                Case "200" 'Sap
                    If BTsap.Enabled = True Then
                        BTsap.PerformClick()
                    End If
                Case "201" 'w int
                    If BTWeightIn.Enabled = True Then
                        BTWeightIn.PerformClick()
                    End If
                Case "202" 'w out
                    If BTWeightOut.Enabled = True Then
                        BTWeightOut.PerformClick()
                    End If
                Case "203" 'Save
                    If BTsave.Enabled = True Then
                        BTsave.PerformClick()
                    End If
                Case "204" 'Update
                    If BTupdate.Enabled = True Then
                        BTupdate.PerformClick()
                    End If
            End Select
        End If
        MyBase.WndProc(m)
    End Sub


    Private Sub SelectVUNLoadingNote()
        Dim q As String
        q = "SELECT        MAX(LOAD_CAPACITY) AS LOAD_CAPACITY, MAX(LOAD_PRESET) AS LOAD_PRESET, MAX(LOAD_CARD) AS LOAD_CARD, MAX(LC_COMPARTMENT) " _
                       & " AS LC_COMPARTMENT, MAX(LC_BAY) AS LC_BAY, Reference, MAX(LOAD_DATE) AS LOAD_DATE, MAX(LOAD_DID) AS LOAD_DID, MAX(SP_Code) AS SP_Code, " _
                       & "   MAX(LOAD_TANK) AS LOAD_TANK, MAX(STATUS_NAME) AS STATUS_NAME, MAX(LOAD_DOfull) AS LOAD_DOfull, MAX(LOAD_VEHICLE) AS LOAD_VEHICLE, " _
                       & "   MAX(Product_name) AS Product_name, MAX(Customer_name) AS Customer_name, MAX(LOAD_DRIVER) AS LOAD_DRIVER, MAX(LOAD_ID) AS Load_id, " _
                       & "   MAX(LOADDATE) AS Loaddate ,max(LOCATION_CODE) as LOCATION_CODE" _
                       & "   FROM V_UNLOADINGNOTE" _
                       & "  where Loaddate='" + String.Format("{0:yyyy-M-d}", DatetimeDBGrid1.Value) + "'" _
                       & "  or LOAD_STATUS in(1,2) " _
                       & " GROUP BY Reference" _
                       & " ORDER BY LOAD_DATE DESC, Reference"
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim MyDataSet As New DataSet
        da.Fill(MyDataSet, "V_UNLOADINGNOTE")
        V_UNLOADINGNOTEBindingSource.DataSource = MyDataSet
        V_UNLOADINGNOTEBindingSource.DataMember = "V_UNLOADINGNOTE"

    End Sub

    Public Function RemoveElementFromArray(ByRef objArray As System.Array, ByVal IndexElement As Integer, ByVal objType As System.Type)
        Dim objArrayList As New ArrayList(objArray)
        objArrayList.RemoveAt(IndexElement)
        Return objArrayList.ToArray(objType)
    End Function

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function


    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTcancel.Click
        Try
            MyErrorProvider.ClearAllErrorMessages()
            Try
                'For i = ProductNo.Length To 0 Step -1
                For i = 0 To ProductNo.Length
                    ProductNo(i).Dispose()
                    CapacityNo(i).Dispose()
                    BayNo(i).Dispose()
                    Meterno(i).Dispose()
                    PRESETNO(i).Dispose()
                    COMPNO(i).Dispose()
                Next
            Catch ex As Exception

            End Try
        Finally
            ReDim ProductNo(0)
            ReDim CapacityNo(0)
            ReDim BayNo(0)
            ReDim Meterno(0)
            ReDim PRESETNO(0)
            ReDim COMPNO(0)
        End Try
        '  Unloading_Load(sender, e)
        cleardata()
        Btadd.Enabled = CheckAddPermisstion(Me.Tag)
        Btedit.Enabled = CheckEditPermisstion(Me.Tag)
        Btprint.Enabled = CheckPrintPermisstion(Me.Tag)
        ' BtWorkPrint.Enabled = CheckPrintPermisstion(Me.Tag)
        'unloading_Shown(sender, e)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        Dim q, s_day, s_month, s_year As String
        NoteAddNew()
        EDVehicle.Focus()
        GroupBox2.Visible = True
        GroupBox2.Enabled = True
        'GroupBox1.Visible = False
        GroupBox1.Enabled = False
        GroupBox3.Enabled = True
        GroupBox5.Enabled = True
        BTsave.Visible = True
        BTupdate.Visible = False

        BTWeightIn.Visible = True
        BTWeightOut.Visible = False

        s_day = Date.Now.Day
        s_month = Date.Now.Month
        s_year = Date.Now.Year

        GroupBox4.Enabled = True
        EDVehicle.SelectedItem = -1
        EDTransNO.SelectedItem = -1
        EDDriver.SelectedItem = -1
        EDShipper.SelectedItem = -1
        Cbn9.Text = Format(Date.Now.Date, "dd/MM/yyyy")
        GiDate.Value = Date.Now.Date
        EDShipper.SelectedIndex = 0
        Status.SelectedIndex = 0
        EDPacking.SelectedIndex = EDPacking.FindStringExact("BULK")


        q = ""
        q = "select isnull(max(Load_id),0)+1 as load_id ,isnull(max(Reference),0)+1 as Reference  from T_unloadingnote"
        Dim da6 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim ds6 As New DataSet()
        Dim dt6 As New DataTable
        da6.Fill(dt6)
        Cbn6.Text = dt6.Rows(0).Item("load_id").ToString()
        Cbn8.Text = dt6.Rows(0).Item("Reference").ToString()

        Dim yearthai As String
        yearthai = Str(Int(s_year + 543))

        q = ""
        q = "select isnull(max(LOAD_DID),0)+1 as LOAD_DID from T_unLOADINGNOTE "
        q &= " where LOAD_DAY=" + s_day + " and LOAD_MONTH = " + s_month + " and LOAD_YEAR=" + yearthai + ""
        Dim da7 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim ds7 As New DataSet()
        Dim dt7 As New DataTable
        da7.Fill(dt7)
        Cbn7.Text = dt7.Rows(0).Item("Load_did").ToString()

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btedit.Click
        If DBGrid1.SelectedRows.Count < 0 Then
            MessageBox.Show("Please Select Data, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        Try
            NoteAddNew()
            EDVehicle.Focus()
            BTWeightIn.Visible = False
            BTWeightOut.Visible = True
            BTsave.Visible = False
            BTupdate.Visible = True

            'GroupBox1.Visible = False
            GroupBox1.Enabled = False
            GroupBox2.Visible = True
            GroupBox2.Enabled = True
            GroupBox4.Visible = True
            GroupBox4.Enabled = True
            GroupBox3.Visible = True
            GroupBox3.Enabled = True
            GroupBox5.Enabled = True


            'TextBox1.Text = PRESETNO(1).Text

            Dim load_id As String
            'load_id = DBGrid1.DataSource.DataSet.FieldByName("LOAD_ID").asstring

            load_id = V_UNLOADINGNOTEBindingSource.Item(V_UNLOADINGNOTEBindingSource.Position)("LOAD_ID").ToString()
            Dim q As String
            q &= ""
            q &= "Select isnull(load_did,0) as Load_did ,"
            q &= "isnull(LOAD_TRUCKCOMPANY,0) as LOAD_TRUCKCOMPANY ,"
            q &= "isnull(load_Shipper,0) as load_shipper ,"
            q &= "isnull(SP_CODE,0) as SP_CODE ,"
            q &= "isnull(load_delivery,0) as load_delivery ,"
            q &= "isnull(load_vehicle,0) as load_vehicle ,"
            q &= "isnull(load_capacity,0) as load_capacity ,"
            q &= "isnull(load_driver,0) as load_driver ,"
            q &= "isnull(load_preset,0) as load_preset ,"
            q &= "isnull(Batch_Name,0) as Batch_Name ,"
            q &= "isnull(LC_SEAL,0) as LC_SEAL ,"
            q &= "isnull(load_card,0) as load_card ,"
            q &= "isnull(AddnoteDate,0) as AddnoteDate ,"
            q &= "isnull(Reference,0) as Reference ,"
            q &= "isnull(load_id,0) as load_id ,"
            q &= "isnull(load_Sealcount,0) as load_Sealcount ,"
            q &= "isnull(LOAD_WEIGHT_IN,0) as LOAD_WEIGHT_IN ,"
            q &= "isnull(Raw_Weight_in,0) as Raw_Weight_in ,"
            q &= "isnull(Update_Weight_in,0) as Update_Weight_in ,"
            q &= "isnull(GI_Date,0) as GI_Date ,"
            'q &= "isnull(BatchLot,0) as BatchLot ,"
            q &= "isnull(Container,0) as Container ,"
            q &= "isnull(COA,0) as COA ,"
            q &= "isnull(Remark,0) as Remark ,"
            q &= "isnull(Load_Customer,0) as Customer_name ,"
            q &= "isnull(status_name,0) as Status_name ,"
            q &= "isnull(packing_type,0) as packing_type ,"
            q &= "isnull(Packing_weight,0) as Packing_weight ,"
            q &= "isnull(Packing_Qty,0) as Packing_Qty ,"
            q &= "isnull(Load_status,0) as Load_status ,"
            q &= "isnull(status_name,0) as status_Name ,"
            q &= "isnull(P_code,0) as P_code ,"
            q &= "isnull(WeightTotal,0) as WeightTotal ,"
            q &= "isnull(MeterReader,0) as MeterReader ,"
            q &= "isnull(WeightCal,0) as WeightCal ,"
            q &= "isnull(Raw_Weight_Out,0) as Raw_Weight_Out ,"
            q &= "isnull(Update_Weight_Out,0) as Update_Weight_Out ,"
            q &= "isnull(AccessCode,0) as AccessCode ,"
            q &= "isnull(ShipmentNo,0) as ShipmentNo ,"
            q &= "Weightin_time ,"
            q &= "WeightOut_time ,"
            q &= "isnull(Invoice,0) as Invoice ,"
            q &= "isnull(PO_NO,0) as PO_NO ,"
            q &= "isnull(Netweight,0) as Netweight ,"
            q &= "isnull(Weighttol,0) as Weighttol ,"
            q &= "isnull(LOAD_PRODUCT,0) as LOAD_PRODUCT ,"
            q &= "isnull(Product_code,0) as Product_code ,"

            q &= " LOCATION_CODE ,"
            'q &= "isnull(LOCATION_CODE,0) as LOCATION_CODE ,"
            q &= "isnull(LOAD_DOfull,0) as LOAD_DOfull "
            q &= "From V_UNLOADINGNOTE "
            q &= "where Load_Id = "
            q &= "" + (load_id) + " "
            q &= "and load_status <> 99   "

            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim ds As New DataSet()
            Dim dt As New DataTable
            da.Fill(dt)
            'Cbn2.SelectedText = (dt.Rows(0).Item("load_vehicle").ToString)
            'Packing.SelectedIndex = Packing.Items.IndexOf(dt.Rows(0).Item("P_code").ToString())
            'Customer.SelectedIndex = Customer.Items.IndexOf(dt.Rows(0).Item("Customer_name").ToString())
            'Status.SelectedIndex = Status.Items.IndexOf(dt.Rows(0).Item("Status_name").ToString())
            'Product.SelectedIndex = Product.Items.IndexOf(dt.Rows(0).Item("Product_code").ToString())
            'Shipper.SelectedIndex = Shipper.Items.IndexOf(dt.Rows(0).Item("SP_CODE").ToString())

            EDVehicle.SelectedIndex = EDVehicle.FindStringExact(dt.Rows(0).Item("load_Vehicle").ToString())
            EDDriver.SelectedIndex = EDDriver.FindStringExact(dt.Rows(0).Item("load_driver").ToString())
            ' dt.Rows(0).Item("load_driver").ToString()
            TCOMPANYBindingSource.Position = TCOMPANYBindingSource.Find("COMPANY_CODE", dt.Rows(0).Item("Load_TRUCKCOMPANY").ToString())
            EDTransNO.SelectedIndex = TCOMPANYBindingSource.Position
            EDLocation.SelectedIndex = EDLocation.FindStringExact(dt.Rows(0).Item("LOCATION_CODE").ToString())
            '(dt.Rows(0).Item("LOCATION_CODE").ToString)
            ' dt.Rows(0).Item("Load_TRUCKCOMPANY").ToString()

            TSHIPPERBindingSource.Position = TSHIPPERBindingSource.Find("SP_CODE", dt.Rows(0).Item("SP_CODE").ToString())
            EDShipper.SelectedIndex = TSHIPPERBindingSource.Position
            EDProduct.SelectedIndex = EDProduct.FindStringExact(dt.Rows(0).Item("Product_code").ToString())
            Status.SelectedIndex = Status.FindStringExact(dt.Rows(0).Item("Status_name").ToString())
            EDCustomer.SelectedIndex = 1
            EDCustomer.SelectedIndex = EDCustomer.FindStringExact(dt.Rows(0).Item("Customer_name").ToString())

            EDPacking.SelectedIndex = EDPacking.FindStringExact(dt.Rows(0).Item("P_code").ToString())

            Quantity.Text = (dt.Rows(0).Item("Packing_Qty").ToString)
            PackingWeight.Text = (dt.Rows(0).Item("Packing_weight").ToString)


            'Cbn2.Text = dt.Rows(0).Item("load_Vehicle").ToString()
            'Shipper.Text = dt.Rows(0).Item("SP_CODE").ToString()
            'Product.Text = dt.Rows(0).Item("Product_code").ToString()
            'Status.Text = dt.Rows(0).Item("Status_name").ToString()
            'Customer.Text = dt.Rows(0).Item("Customer_name").ToString()
            'Packing.Text = dt.Rows(0).Item("P_code").ToString()



            'Cbn4.SelectedIndex = Cbn4.Items.IndexOf(dt.Rows(0).Item("load_driver").ToString())

            CBCardNO.SelectedIndex = 1
            CBCardNO.SelectedIndex = CBCardNO.FindStringExact(dt.Rows(0).Item("load_card").ToString())


            'Cbn4.SelectedText = (dt.Rows(0).Item("load_driver").ToString)
            Cbn5.Text = (dt.Rows(0).Item("load_capacity").ToString)
            'PO_No.Text = (dt.Rows(0).Item("PO_No").ToString)
            Cbn10.Text = (dt.Rows(0).Item("load_preset").ToString)
            Cbn6.Text = (dt.Rows(0).Item("load_id").ToString)
            Cbn7.Text = (dt.Rows(0).Item("Load_did").ToString)
            Cbn8.Text = (dt.Rows(0).Item("Reference").ToString)
            Cbn9.Text = Format(Date.Now.Date, "dd/MM/yyyy")
            GiDate.Text = (dt.Rows(0).Item("Gi_date").ToString)
            Cbn11.Text = (dt.Rows(0).Item("load_DOfull").ToString)
            Coa.Text = (dt.Rows(0).Item("coa").ToString)
            'Batchlot.Text = (dt.Rows(0).Item("BatchLot").ToString)
            Container.Text = (dt.Rows(0).Item("Container").ToString)
            UpdateWeightIn.Text = (dt.Rows(0).Item("Update_Weight_in").ToString)
            LawWeightIn.Text = (dt.Rows(0).Item("Raw_Weight_in").ToString)
            ShipmentNo.Text = (dt.Rows(0).Item("ShipmentNo").ToString)
            Cbn8.Text = (dt.Rows(0).Item("Reference").ToString)
            WeightTotal.Text = (dt.Rows(0).Item("WeightTotal").ToString)
            LawWeightout.Text = (dt.Rows(0).Item("Raw_Weight_Out").ToString)
            UpdateWeightOut.Text = (dt.Rows(0).Item("Update_Weight_Out").ToString)

            Weightintime.Text = (dt.Rows(0).Item("Weightin_time").ToString)
            weightouttime.Text = (dt.Rows(0).Item("WeightOut_time").ToString)


            Invoice.Text = (dt.Rows(0).Item("Invoice").ToString)
            PO_No.Text = (dt.Rows(0).Item("PO_NO").ToString)
            Netweight.Text = (dt.Rows(0).Item("Netweight").ToString)
            Weighttol.Text = (dt.Rows(0).Item("Weighttol").ToString)

            WeightTotal_TextChanged(sender, e)

            Cbn1Chang()

        Catch ex As Exception

        End Try
    End Sub

    Private Function ReportDefinition() As Object
        Throw New NotImplementedException
    End Function

    Sub Cbn1Chang()
        'TruckId.SelectedIndex = Cbn2.SelectedIndex
        'TruckCompanyId.SelectedIndex = Cbn3.SelectedIndex
        'DriverID.SelectedIndex = Cbn4.SelectedIndex
        'ShipperId.SelectedIndex = Shipper.SelectedIndex
        'CustomerID.SelectedIndex = Customer.SelectedIndex
        'StatusId.SelectedIndex = Status.SelectedIndex
        'P_Weight.SelectedIndex = Packing.SelectedIndex
        ProductId.SelectedIndex = EDProduct.SelectedIndex
    End Sub

    Private Sub WeightIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTWeightIn.Click
        LawWeightIn.Text = WeightScal.Text
        'LawWeightIn.Text = Main.weight.Text.Replace(",", "")
        Weightintime.Text = Date.Now.Hour.ToString + ":" + Date.Now.Minute.ToString + ":" + Date.Now.Second.ToString

    End Sub

    Private Sub Status_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Status.TextChanged
        StatusId.SelectedIndex = Status.SelectedIndex
    End Sub


    Private Sub Cbn2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim q As String
        Dim TRUCK_CAPASITY As Integer

        '  EDVehicle.Text = EDVehicle.Text
        TRUCK_COMP_NUM = 0
        If EDVehicle.SelectedIndex >= 0 Then
            q = "select * from V_TRUCK2 where TRUCK_NUMBER= '" + (EDVehicle.Text) + "'"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds As New DataSet()
            Dim dt As New DataTable
            da.Fill(dt)


            '  EDDriver.SelectedIndex = EDDriver.FindStringExact(dt.Rows(0).Item("TRUCK_DRIVER").ToString())
            TRUCK_CAPASITY = dt.Rows(0).Item("TRUCK_CAPASITY").ToString()
            TRUCK_COMP_NUM = dt.Rows(0).Item("TRUCK_COMP_NUM").ToString()
            '  EDTransNO.SelectedIndex = EDTransNO.FindStringExact(dt.Rows(0).Item("TRUCK_COMPANY").ToString())
            Cbn5.Text = TRUCK_CAPASITY.ToString
            Cbn10.Text = TRUCK_CAPASITY
            'RichTextBox1.Text = TRUCK_COMP_NUM

            Dim i As Integer
            Try
                Try
                    'For i = ProductNo.Length To 0 Step -1
                    For i = 0 To ProductNo.Length
                        ProductNo(i).Dispose()
                        CapacityNo(i).Dispose()
                        BayNo(i).Dispose()
                        Meterno(i).Dispose()
                        PRESETNO(i).Dispose()
                        COMPNO(i).Dispose()
                    Next
                Catch ex As Exception

                End Try
            Finally
                ReDim ProductNo(0)
                ReDim CapacityNo(0)
                ReDim BayNo(0)
                ReDim Meterno(0)
                ReDim PRESETNO(0)
                ReDim COMPNO(0)
            End Try
            Dim aa As Integer = 1


            'For i = TRUCK_COMP_NUM - 1 To 0 Step -1
            '    ReDim Preserve COMPNO(TRUCK_COMP_NUM)
            '    COMPNO(i) = New TextBox
            '    COMPNO(i).Text = (i - TRUCK_COMP_NUM) + TRUCK_COMP_NUM + 1 '(TRUCK_COMP_NUM - i) + 1
            '    COMPNO(i).TextAlign = HorizontalAlignment.Center
            '    COMPNO(i).Height = 21
            '    COMPNO(i).Width = GroupBox15.Width
            '    COMPNO(i).Parent = GroupBox15
            '    COMPNO(i).Dock = DockStyle.Top
            '    COMPNO(i).Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '    COMPNO(i).TabIndex = i


            '    ReDim Preserve ProductNo(TRUCK_COMP_NUM)
            '    ProductNo(i) = New ComboBox
            '    'ProductNo(i).DropDownStyle = ComboBoxStyle.DropDownList
            '    'ProductNo(i).DrawMode = DrawMode.OwnerDrawFixed

            '    ProductNo(i).Height = 21
            '    ProductNo(i).Width = GroupBox14.Width
            '    ProductNo(i).Parent = GroupBox14
            '    ProductNo(i).ItemHeight = 13
            '    ProductNo(i).Dock = DockStyle.Top
            '    ProductNo(i).Parent = GroupBox14
            '    ProductNo(i).SelectedValue = Text
            '    ProductNo(i).Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '    ProductNo(i).TabIndex = i


            '    ReDim Preserve CapacityNo(TRUCK_COMP_NUM)
            '    CapacityNo(i) = New TextBox
            '    CapacityNo(i).Text = ""
            '    CapacityNo(i).Height = 21
            '    CapacityNo(i).Width = GroupBox12.Width
            '    CapacityNo(i).Parent = GroupBox12
            '    CapacityNo(i).Dock = DockStyle.Top
            '    CapacityNo(i).TextAlign = HorizontalAlignment.Center
            '    CapacityNo(i).Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '    CapacityNo(i).ReadOnly = True
            '    CapacityNo(i).TabIndex = i

            '    ReDim Preserve PRESETNO(TRUCK_COMP_NUM)
            '    'Dim handler As TextChangedEventHandler

            '    'PRESETNO(i).txtMyTextBox.TextChanged = New System.EventHandler(TextChange)


            '    PRESETNO(i) = New TextBox
            '    PRESETNO(i).Text = ""
            '    PRESETNO(i).Height = 21
            '    PRESETNO(i).Width = GroupBox13.Width
            '    PRESETNO(i).TextAlign = HorizontalAlignment.Center
            '    PRESETNO(i).Parent = GroupBox13
            '    PRESETNO(i).Dock = DockStyle.Top
            '    PRESETNO(i).Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '    PRESETNO(i).TabIndex = i
            '    'PRESETNO(i).


            '    ReDim Preserve BayNo(TRUCK_COMP_NUM)
            '    BayNo(i) = New ComboBox
            '    BayNo(i).Height = 21
            '    BayNo(i).Width = GroupBox11.Width
            '    BayNo(i).Parent = GroupBox11
            '    BayNo(i).ItemHeight = 13
            '    BayNo(i).Dock = DockStyle.Top
            '    BayNo(i).Parent = GroupBox11
            '    BayNo(i).Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '    BayNo(i).TabIndex = i
            '    'BayNo(i).Items.Add("")


            '    ReDim Preserve Meterno(TRUCK_COMP_NUM)
            '    Meterno(i) = New ComboBox
            '    Meterno(i).Height = 21
            '    Meterno(i).Width = GroupBox10.Width
            '    Meterno(i).Parent = GroupBox10
            '    Meterno(i).ItemHeight = 13
            '    Meterno(i).Dock = DockStyle.Top
            '    Meterno(i).Parent = GroupBox10
            '    Meterno(i).Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            '    Meterno(i).TabIndex = i

            'Next i

            '''''''''''''CapacityNo''''''''''''''

            'For i = 0 To TRUCK_COMP_NUM - 1 'dt.Rows.Count - 1
            '    CapacityNo(i).Text = dt.Rows(i).Item("T_TRUCKCOMPCAP").ToString()

            'Next i


            ' '''''''ProductNo''''''''''''''

            'q = ""
            'q = "Select * from T_Product where product_type=1 Order by PRODUCT_CODE"
            'da.SelectCommand = New SqlClient.SqlCommand(q, conn)
            'For i = 0 To TRUCK_COMP_NUM - 1 'ProductNo.Length - 2
            '    Dim T_Product As New DataTable
            '    da.Fill(T_Product)
            '    With ProductNo(i)
            '        .DataSource = T_Product
            '        .DisplayMember = "PRODUCT_CODE" 'T_Product.Rows(i).Item("PRODUCT_CODE")
            '        .ValueMember = "PRODUCT_CODE" 'T_Product.Rows(i).Item("PRODUCT_CODE")
            '        .SelectedIndex = 0
            '    End With
            'Next



            ' ''''''''''Island/Bay''''''''''''''
            'q = ""
            'q = "select BATCH_ISLAND_NO,max(ISLAND_LEFT) as ISLAND_LEFT,max(ISLAND_RIGHT) as ISLAND_RIGHT from V_BATCHMETER"
            'q &= " GROUP BY BATCH_ISLAND_NO order by BATCH_ISLAND_NO"
            'da.SelectCommand = New SqlClient.SqlCommand(q, conn)

            ''BayNo(1).Items.Add("")
            'For i = 0 To TRUCK_COMP_NUM - 1 ' BayNo.Length - 2
            '    Dim BATCH_ISLAND_NO As New DataTable
            '    da.Fill(BATCH_ISLAND_NO)
            '    With BayNo(i)
            '        .Items.Add("")
            '        .DataSource = BATCH_ISLAND_NO
            '        .DisplayMember = "BATCH_ISLAND_NO"
            '        .ValueMember = "BATCH_ISLAND_NO"
            '        .SelectedIndex = -1
            '    End With
            'Next


            ' ''''''''''''''''''''Meter''''''''''''''
            'q = ""
            'q = "select * from V_BATCHMETER order by Batch_NAME"
            'da.SelectCommand = New SqlClient.SqlCommand(q, conn)
            'Try
            '    Meterno(1).Items.Add("")
            'Catch ex As Exception
            '    Meterno(0).Items.Add("")
            'End Try


            'For i = 0 To TRUCK_COMP_NUM - 1 ' BayNo.Length - 2
            '    Dim V_BATCHMETER As New DataTable
            '    da.Fill(V_BATCHMETER)
            '    With Meterno(i)
            '        '.Items.Add("")
            '        .DataSource = V_BATCHMETER
            '        .DisplayMember = "BATCH_NAME"
            '        .ValueMember = "BATCH_NAME"
            '        .SelectedIndex = 0
            '    End With
            'Next

        End If
        Cbn1Chang()
    End Sub





    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = False Then
            Exit Sub
        End If
        Dim q, id, Driver_Code As String
        'Dim TTemp1, TTemp2 As String
        Dim i, LC_Base, LC_Blend, Customer, Shipper1, topup1, SL, SC, ST, r, j As Integer

        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        'Cbn10.Text = "9000"

        If LawWeightIn.Text = "" Then
            MessageBox.Show("Please Check Lawweight", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If MsgBox("Are you sure to save?", vbYesNo + vbDefaultButton2, "Confirmation") = vbYes Then
            q = ""
            q = "select max(ST_ID)+1 as ST_ID"
            q &= " from T_ST"
            Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim ds1 As New DataSet()
            Dim dt1 As New DataTable
            da1.Fill(dt1)
            ST = dt1.Rows(0).Item("ST_ID").ToString
            dt1.Clear()
            ds1.Clear()

            q = ""
            q = "select count(load_id) as CLoad_id "
            q &= "from T_unloadingnote "
            q &= "where  (DATEPART(dd, LOAD_DATE) = (DATEPART(dd,GETDATE()))) "
            q &= "AND  (DATEPART(mm, LOAD_DATE) = (DATEPART(mm,GETDATE()))) "
            q &= "AND  (DATEPART(yy, LOAD_DATE) = (DATEPART(yy,GETDATE()))) "
            q &= "AND (ST_ID NOT IN (SELECT st_id FROM t_st))  "
            Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds2 As New DataSet()
            Dim dt2 As New DataTable

            da2.Fill(dt2)
            SL = dt2.Rows(0).Item("CLoad_id").ToString
            SC = ST + SL
            'PING
            q = ""
            q = "Insert into T_UnLoadingnote "
            q &= " (LOAD_ID,"
            q &= " LOAD_CARD,"
            q &= " LOAD_DID,"
            q &= " LOAD_VEHICLE,"
            q &= " LOAD_STATUS,"
            q &= " LOAD_CAPACITY,"
            q &= " ST_ID,"
            q &= " LOAD_Shipper,"
            q &= " LOAD_STARTTIME,"
            q &= " AddnoteDate,"
            q &= " LOAD_DRIVER,"
            q &= " LOAD_PRESET,"
            q &= " LOAD_DOfull, "
            q &= " Reference, "
            q &= " COA, "
            'q &= " Remark, "
            q &= " Container, "
            q &= " Gi_date, "
            'q &= " BatchLot, "
            q &= " Raw_Weight_in, "
            q &= " Update_Weight_in, "
            'q &= " Invoice, "
            q &= " ShipmentNo, "
            'q &= " LOAD_SEALCOUNT, "
            'q &= " load_Temp, "
            'q &= " Density, "
            'q &= " load_Tank, "
            q &= " Packing_Qty, "
            q &= " Packing_weight, "
            q &= " Packing_Type, "
            q &= " Load_customer, "
            q &= " weightIn_time, "
            q &= " Invoice, "
            q &= " PO_NO, "
            q &= " NetWeight, "
            q &= " WeightTol, "
            q &= " Load_product, "
            q &= " Load_Location, "
            q &= " LOAD_TRUCKCOMPANY) "
            q &= " Values ("
            q &= "" + (Cbn6.Text) + "" + ","
            q &= "'" + (CBCardNO.Text) + "'" + ","
            q &= "'" + (Cbn7.Text) + "'" + ","
            q &= "" + (TruckId.Text) + "" + ","
            'q &= "1" + ","
            q &= "" + (StatusId.Text) + "" + ","
            q &= "" + (Cbn5.Text) + "" + ","
            q &= "" + Str(SC) + "" + ","
            q &= "" + (ShipperId.Text) + "" + ","
            q &= "'" + (Cbn9.Text) + "'" + ","
            q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
            q &= "" + (DriverID.Text) + "" + ","
            q &= "" + (Cbn10.Text) + "" + ","
            q &= "'" + (Cbn11.Text) + "'" + ","
            q &= "" + (Cbn8.Text) + "" + ","
            q &= "'" + (Coa.Text) + "'" + ","
            'q &= "'" + (RichTextBox1.Text) + "'" + ","
            q &= "'" + (Container.Text) + "'" + ","
            q &= "'" + String.Format("{0:yyyy-MM-dd}", GiDate.Value) + "'" + ","
            'q &= "'" + (Batchlot.Text) + "'" + ","
            q &= "'" + (LawWeightIn.Text) + "'" + ","
            q &= "'" + (UpdateWeightIn.Text) + "'" + ","
            'q &= "'" + (Invoice.Text) + "'" + ","
            q &= "'" + (ShipmentNo.Text) + "'" + ","
            'q &= "'" + (SealCount.Text) + "'" + ","
            'q &= "'" + (Temp.Text) + "'" + ","
            'q &= "'" + (Density.Text) + "'" + ","
            'q &= "'" + (Tank.Text) + "'" + ","
            q &= "'" + (Quantity.Text) + "'" + ","
            q &= "'" + (PackingWeight.Text) + "'" + ","
            q &= "'" + (PackingId.Text) + "'" + ","
            q &= "'" + (CustomerID.Text) + "'" + ","
            q &= "'" + (Weightintime.Text) + "'" + ","
            q &= "'" + (Invoice.Text) + "'" + ","
            q &= "'" + (PO_No.Text) + "'" + ","
            q &= "'" + (Netweight.Text) + "'" + ","
            q &= "'" + (Weighttol.Text) + "'" + ","
            q &= "'" + (ProductId.Text) + "'" + ","
            q &= "'" + (TLOCATIONBindingSource.Item(TLOCATIONBindingSource.Position)("ID").ToString) + "'" + ","

            q &= "'" + (TruckCompanyId.Text) + "'" + ")"

            Try
                da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                da.InsertCommand.ExecuteNonQuery()

            Catch ex As Exception
                MessageBox.Show("Unable to add information, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            'Update T_truck
            Try
                q = "Update T_truck set truck_driver='" & DriverID.Text & "' truck_company='" & TruckCompanyId.Text _
                    & " where truck_number='" & EDVehicle.Text & "'"
                da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                da.InsertCommand.ExecuteNonQuery()

            Catch ex As Exception
                ' q = ""
                ' MessageBox.Show("Wrong information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '  Cursor = Cursors.Default
                '  Exit Sub
            End Try
            ''''''End Update T_truck''''''''''''''



            'MessageBox.Show("Update data Successfully", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Main.CEvent("USER :" + Main.U_NAME + " Created unloading Reference: " + Cbn8.Text)

            ''''Report  

            ' Dim ref As String
            'ref = V_UNLOADINGNOTEBindingSource.Item(V_UNLOADINGNOTEBindingSource.Position)("reference").ToString()
            ' ref = Cbn8.Text
            Bcancel_Click(sender, e)
            Btprint_Click(sender, e)

            'Dim Myreport As New ReportDocument
            'Myreport = New ReportDocument
            'Dim sql As String
            'sql = "Select * from V_unLoadingnote where reference =" + ref + ""
            'Dim da7 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            'Dim ds7 As New DataSet()
            'Dim dt7 As New DataTable
            'da7.Fill(ds7, "V_unLoadingnote")
            'Try
            '    Myreport.Load("TOCUNWorkOrder.rpt")
            '    Myreport.SetDataSource(ds7.Tables("V_unLoadingnote"))
            '    ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            '    ReportPrint.Show()

            'Catch ex As Exception

            'End Try

        End If

    End Sub

    Private Sub Unloading_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Dim q, s_day, s_month, s_year, s_hr, s_mn, s_sc, TTimes As String

        s_day = Date.Now.Day
        s_month = Date.Now.Month
        s_year = Date.Now.Year
        cleardata()

        'Dim numrow, indexx As Integer
        'q = ""
        'q = "Select * from T_product where product_type=1 order by product_code"
        'Dim da11 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds11 As New DataSet()
        'Dim dt11 As New DataTable
        'da11.Fill(dt11)
        'numrow = dt11.Rows.Count
        'EDProduct.Items.Add("")
        'ProductId.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    ProductId.Items.Add(dt11.Rows(indexx).Item("ID").ToString())
        '    EDProduct.Items.Add(dt11.Rows(indexx).Item("product_code").ToString())
        'Next indexx

        'q = ""
        'q = "select * from v_unLoadingnote order by id desc"
        'Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds As New DataSet()
        'Dim dt As New DataTable

        '''''''''''''''''''''''''''''

        'q = ""
        'q = "Select max(id) as ID,Max(TRUCK_NUMBER) as TRUCK_NUMBER   from V_TRUCK2 group by TRUCK_NUMBER order by TRUCK_NUMBER"
        'Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds1 As New DataSet()
        'Dim dt1 As New DataTable

        'da1.Fill(dt1)
        'numrow = dt1.Rows.Count
        'Cbn2.Items.Add("")
        'TruckId.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    TruckId.Items.Add(dt1.Rows(indexx).Item("ID").ToString())
        '    Cbn2.Items.Add(dt1.Rows(indexx).Item("TRUCK_NUMBER").ToString())
        'Next indexx



        'q = ""
        'q = "select * from T_company order by COMPANY_Code"
        'Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds2 As New DataSet()
        'Dim dt2 As New DataTable
        'da2.Fill(dt2)
        'numrow = dt2.Rows.Count
        'TruckCompanyId.Items.Add("")
        'Cbn3.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    TruckCompanyId.Items.Add(dt2.Rows(indexx).Item("COMPANY_ID").ToString())
        '    Cbn3.Items.Add(dt2.Rows(indexx).Item("Company_code").ToString())
        'Next indexx

        'q = ""
        'q = "Select * from V_DRIVER Where Driver_BLACK_LIST <>'Yes'"
        'q &= " order by Driver_name"

        'Dim da3 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds3 As New DataSet()
        'Dim dt3 As New DataTable
        'da3.Fill(dt3)
        'numrow = dt3.Rows.Count
        'Cbn4.Items.Add("")
        'DriverID.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    DriverID.Items.Add(dt3.Rows(indexx).Item("ID").ToString())
        '    Cbn4.Items.Add(dt3.Rows(indexx).Item("Driver_Name").ToString())
        'Next indexx

        'q = ""
        'q = "Select * from T_Shipper order by SP_CODE"
        'Dim da4 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds4 As New DataSet()
        'Dim dt4 As New DataTable
        'da4.Fill(dt4)
        'numrow = dt4.Rows.Count
        'Shipper.Items.Clear()
        'ShipperId.Items.Clear()

        'Shipper.Items.Add("")
        'ShipperId.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    ShipperId.Items.Add(dt4.Rows(indexx).Item("ID").ToString())
        '    Shipper.Items.Add(dt4.Rows(indexx).Item("SP_CODE").ToString())
        'Next indexx



        'q = ""
        'q = "Select * from T_Customer"
        'Dim da8 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds8 As New DataSet()
        'Dim dt8 As New DataTable
        'da8.Fill(dt8)
        'numrow = dt8.Rows.Count
        'Customer.Items.Clear()
        'CustomerID.Items.Clear()
        'Customer.Items.Add("")
        'CustomerID.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    CustomerID.Items.Add(dt8.Rows(indexx).Item("ID").ToString())
        '    Customer.Items.Add(dt8.Rows(indexx).Item("Customer_code").ToString())
        'Next indexx

        'q = ""
        'q = "Select * from T_status order by status_id"
        'Dim da9 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds9 As New DataSet()
        'Dim dt9 As New DataTable
        'da9.Fill(dt9)
        'numrow = dt9.Rows.Count
        'Status.Items.Add("")
        'StatusId.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    StatusId.Items.Add(dt9.Rows(indexx).Item("Status_id").ToString())
        '    Status.Items.Add(dt9.Rows(indexx).Item("Status_name").ToString())
        'Next indexx

        'q = ""
        'q = "Select * from T_packing"
        'Dim da10 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds10 As New DataSet()
        'Dim dt10 As New DataTable
        'da10.Fill(dt10)
        'numrow = dt10.Rows.Count
        'Packing.Items.Add("")
        'PackingId.Items.Add("")
        'P_Weight.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    PackingId.Items.Add(dt10.Rows(indexx).Item("P_ID").ToString())
        '    Packing.Items.Add(dt10.Rows(indexx).Item("P_Code").ToString())
        '    P_Weight.Items.Add(dt10.Rows(indexx).Item("P_Weight").ToString())
        'Next indexx



        'q = ""
        'q = "select BATCH_ISLAND_NO,max(ISLAND_LEFT) as ISLAND_LEFT,max(ISLAND_RIGHT) as ISLAND_RIGHT from V_BATCHMETER"
        'q &= " GROUP BY BATCH_ISLAND_NO order by BATCH_ISLAND_NO"
        'Dim da12 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds12 As New DataSet()
        'Dim dt12 As New DataTable
        'da12.Fill(dt12)
        'numrow = dt12.Rows.Count
        'Bay.Items.Add("")
        '' ProductId.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    'ProductId.Items.Add(dt12.Rows(indexx).Item("ID").ToString())
        '    Bay.Items.Add(dt12.Rows(indexx).Item("BATCH_ISLAND_NO").ToString())
        'Next indexx

        'q = ""
        'q = "select * from V_BATCHMETER order by Batch_NAME"
        'Dim da13 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds13 As New DataSet()
        'Dim dt13 As New DataTable
        'da13.Fill(dt13)
        'numrow = dt13.Rows.Count
        'Meter.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    Meter.Items.Add(dt13.Rows(indexx).Item("Batch_NAME").ToString())
        'Next indexx
    End Sub

    Private Sub Btprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btprint.Click
        Try


            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()

            Dim ref As String
            ref = V_UNLOADINGNOTEBindingSource.Item(V_UNLOADINGNOTEBindingSource.Position)("reference").ToString()

            Dim Myreport As New ReportDocument
            Myreport = New ReportDocument
            Dim sql As String
            sql = "Select * from V_unLoadingnote where reference =" + ref + ""
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim ds As New DataSet()
            Dim dt As New DataTable
            da.Fill(ds, "V_unLoadingnote")

            Myreport.Load(TSHIPPERBindingSource(0)("UNLOAD_Workoder").ToString())

            'Select Case Main.SALE_ORG
            '    Case 1100
            '            Myreport.Load("TOCUNWorkOrder.rpt")
            '    Case 1200
            '        Myreport.Load("TEAunloadingreport.rpt")
            '    Case 1500
            '        Myreport.Load("TOLunloadingreport.rpt")
            '    Case 1600
            '            Myreport.Load("TOLunloadingreport.rpt")
            '        Case Else
            '            Myreport.Load("TOCUNWorkOrder.rpt")
            '    End Select
            ' Myreport.Load("unloadingreport.rpt")

            Try
                Myreport.PrintOptions.PrinterName = My.Settings.WORK_P
            Catch ex As Exception

            End Try

            Try
                Myreport.PrintOptions.PaperOrientation = My.Settings.WORK_PaperOrientation
                Myreport.PrintOptions.PaperSize = My.Settings.WORK_PaperSize
            Catch ex As Exception
            End Try

            'Try
            '    Myreport.PrintOptions.PrinterName = "GCCOA"
            'Catch ex As Exception

            'End Try

            Myreport.SetDataSource(ds.Tables("V_unLoadingnote"))
            ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            ReportPrint.Show()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub WeightOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTWeightOut.Click
        Status.Text = "Finished"
        LawWeightout.Text = WeightScal.Text
        Dateedit.Value = Now
        '   LawWeightout.Text = Main.weight.Text.Replace(",", "")
        weightouttime.Text = Date.Now.Hour.ToString + ":" + Date.Now.Minute.ToString + ":" + Date.Now.Second.ToString
        LawWeightout_TextChanged(sender, e)

    End Sub

    Private Sub Product_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDProduct.TextChanged
        If EDProduct.Text = "" Then
            ProductId.SelectedIndex = -1
        Else
            ProductId.SelectedIndex = EDProduct.SelectedIndex
        End If
    End Sub

    Sub cleardata()
        EDVehicle.SelectedIndex = -1
        EDTransNO.SelectedIndex = -1
        EDDriver.SelectedIndex = -1
        TruckCompanyId.SelectedIndex = -1
        DriverID.SelectedIndex = -1
        CBCardNO.SelectedIndex = -1

        EDShipper.SelectedIndex = -1
        ShipperId.SelectedIndex = -1
        EDCustomer.SelectedIndex = -1
        CustomerID.SelectedIndex = -1
        EDProduct.SelectedIndex = -1
        ProductId.SelectedIndex = -1
        Status.SelectedIndex = -1
        StatusId.SelectedIndex = -1

        EDPacking.SelectedIndex = -1
        PackingId.SelectedIndex = -1

        TruckId.SelectedIndex = -1
        EDLocation.SelectedIndex = -1

        GroupBox3.Enabled = False
        GroupBox4.Enabled = True
        GroupBox3.Visible = True
        GroupBox5.Enabled = False
        'GroupBox2.Visible = False
        GroupBox2.Enabled = False
        GroupBox1.Visible = True
        GroupBox1.Enabled = True
        UpdateWeightIn.Enabled = False
        UpdateWeightOut.Enabled = False

        Cbn11.Text = ""
        Coa.Text = ""
        Container.Text = ""
        'Batchlot.Text = ""
        PO_No.Text = ""
        Cbn8.Text = ""
        Cbn5.Text = ""
        Cbn10.Text = ""
        Cbn7.Text = ""
        LawWeightIn.Text = ""
        UpdateWeightIn.Text = ""
        LawWeightout.Text = ""
        UpdateWeightOut.Text = ""
        Invoice.Text = ""
        ShipmentNo.Text = ""
        Invoice.Text = ""
        PO_No.Text = ""
        Netweight.Text = ""
        Weighttol.Text = ""
        WeightTotal.Text = ""
        'Order.Text = ""
        Weightintime.Text = ""
        weightouttime.Text = ""
        PackingWeight.Text = ""
        Quantity.Text = ""

        Try
            SelectVUNLoadingNote()
            '     Me.V_UNLOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_UNLOADINGNOTE)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub NoteAddNew()
        Try
            TTRUCKBindingSource.Position = -1
            TSHIPPERBindingSource.Position = -1
            TSTATUSBindingSource.Position = -1
            TCOMPANYBindingSource.Position = -1
            TCustomerBindingSource.Position = -1
            TPACKINGBindingSource.Position = -1
            TDRIVERBindingSource.Position = -1
            TProductBindingSource.Position = -1
            VCARDFREEBindingSource.Position = -1
            EDVehicle.SelectedIndex = -1
            EDTransNO.SelectedIndex = -1
            EDDriver.SelectedIndex = -1
            TruckCompanyId.SelectedIndex = -1
            DriverID.SelectedIndex = -1
            CBCardNO.SelectedIndex = -1

            EDShipper.SelectedIndex = -1
            ShipperId.SelectedIndex = -1
            EDCustomer.SelectedIndex = -1
            CustomerID.SelectedIndex = -1
            EDProduct.SelectedIndex = -1
            ProductId.SelectedIndex = -1
            Status.SelectedIndex = -1
            StatusId.SelectedIndex = -1

            EDPacking.SelectedIndex = -1
            PackingId.SelectedIndex = -1

            TruckId.SelectedIndex = -1
            EDLocation.SelectedIndex = -1
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Packing_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDPacking.TextChanged

        PackingId.SelectedIndex = EDPacking.SelectedIndex
        P_Weight.SelectedIndex = EDPacking.SelectedIndex
        Quantity.Text = 1
        Quantity_TextChanged(sender, e)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        f05_01_Advisenote.loginId = 1
        f02_Login.ShowDialog()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        f05_01_Advisenote.loginId = 1
        f02_Login.ShowDialog()

        If f05_01_Advisenote.loginId = 2 Then
            UpdateWeightOut.Text = Convert.ToDouble(LawWeightIn.Text) - Convert.ToDouble(Cbn10.Text) - Convert.ToDouble(PackingWeight.Text)
            If UpdateWeightIn.Text <> "" And UpdateWeightIn.Text <> "0" Then
                UpdateWeightOut.Text = Convert.ToDouble(UpdateWeightIn.Text) - Convert.ToDouble(Cbn10.Text) - Convert.ToDouble(PackingWeight.Text)
            End If

            Status.Text = "Finished"
        End If
    End Sub

    Private Sub Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTupdate.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = False Then
            Exit Sub
        End If
        EDCustomer_TextChanged(sender, e)
        EDTransNO_TextChanged(sender, e)
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        'If (LawWeightout.Text = "" Or LawWeightout.Text = "0") And (UpdateWeightOut.Text = "" Or UpdateWeightOut.Text = "0") Then
        '    MessageBox.Show("Please Check weight-Out", "Check", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Exit Sub
        'End If

        If weightouttime.Text = "" Then
            weightouttime.Text = Date.Now.Hour.ToString + ":" + Date.Now.Minute.ToString + ":" + Date.Now.Second.ToString
        End If

        If MsgBox("Are you sure to update?", vbYesNo + vbDefaultButton2, "Confirmation") = vbYes Then
            'TruckId.SelectedIndex = Cbn2.SelectedIndex
            'TruckCompanyId.SelectedIndex = Cbn3.SelectedIndex
            'DriverID.SelectedIndex = Cbn4.SelectedIndex
            'ShipperId.SelectedIndex = Shipper.SelectedIndex
            'CustomerID.SelectedIndex = Customer.SelectedIndex
            'StatusId.SelectedIndex = Status.SelectedIndex
            ProductId.SelectedIndex = EDProduct.SelectedIndex
            Dim q As String
            q = ""
            q = "UPDATE T_UNLOADINGNOTE "
            q &= "SET LOAD_SHIPPER = "
            q &= "" + (ShipperId.Text) + ","
            q &= "LOAD_DOfull = "
            q &= "'" + (Cbn11.Text) + "'" + ","
            q &= "LOAD_DID = "
            q &= "" + (Cbn7.Text) + ","
            q &= "LOAD_STARTTIME = "
            q &= "" + (Cbn9.Text) + ","
            q &= " AddnoteDate = "
            q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
            q &= "LOAD_DRIVER = "
            q &= "'" + (DriverID.Text) + "'" + ","
            q &= "LOAD_PRESET = "
            q &= "'" + (Cbn10.Text) + "'" + ","
            q &= "LOAD_VEHICLE = "
            q &= "'" + (TruckId.Text) + "'" + ","
            q &= "LOAD_CAPACITY = "
            q &= "'" + (Cbn5.Text) + "'" + ","
            q &= "Container = "
            q &= "'" + (Container.Text) + "'" + ","
            q &= "Load_customer = "
            q &= "'" + (CustomerID.Text) + "'" + ","
            q &= "COA = "
            q &= "'" + (Coa.Text) + "'" + ","
            'q &= "BatchLot = "
            'q &= "'" + (Batchlot.Text) + "'" + ","
            q &= "GI_Date = "
            q &= "'" + String.Format("{0:yyyy-MM-dd}", GiDate.Value) + "'" + ","
            q &= " Raw_Weight_in = "
            q &= "'" + (LawWeightIn.Text) + "'" + ","
            q &= " update_Weight_in = "
            q &= "'" + (UpdateWeightIn.Text) + "'" + ","
            q &= " Raw_Weight_Out = "
            q &= "'" + (LawWeightout.Text) + "'" + ","
            q &= " update_Weight_Out = "
            q &= "'" + (UpdateWeightOut.Text) + "'" + ","
            'q &= " Remark = "
            'q &= "'" + (RichTextBox1.Text) + "'" + ","
            q &= " AccessCode = "
            q &= "'" + (Invoice.Text) + "'" + ","
            q &= " ShipmentNo = "
            q &= "'" + (ShipmentNo.Text) + "'" + ","
            q &= " Packing_Type = "
            q &= "'" + (PackingId.Text) + "'" + ","
            q &= " Packing_Qty = "
            q &= "'" + (Quantity.Text) + "'" + ","
            q &= " Packing_weight = "
            q &= "'" + (PackingWeight.Text) + "'" + ","
            q &= " WeightTotal = "
            q &= "'" + (WeightTotal.Text) + "'" + ","
            q &= " LOAD_Product = "
            q &= "'" + (ProductId.Text) + "'" + ","
            'q &= " Density = "
            'q &= "'" + (Density.Text) + "'" + ","
            'q &= " LOAD_TANK = "
            'q &= "'" + (Tank.Text) + "'" + ","
            'q &= " WeightCal = "
            'q &= "'" + (Calculate.Text) + "'" + ","
            q &= " weightout_time = "
            q &= "'" + (weightouttime.Text) + "'" + ","
            q &= " load_status = "
            q &= "'" + (StatusId.Text) + "'" + ","
            q &= " Invoice = "
            q &= "'" + (Invoice.Text) + "'" + ","
            q &= " PO_No = "
            q &= "'" + (PO_No.Text) + "'" + ","
            q &= " Netweight = "
            q &= "'" + (Netweight.Text) + "'" + ","
            q &= " Weighttol = "
            q &= "'" + (Weighttol.Text) + "'" + ","

            q &= " Load_Location = "
            q &= "'" + (TLOCATIONBindingSource.Item(TLOCATIONBindingSource.Position)("ID").ToString) + "'" + ","

            q &= " Load_CARD = "
            q &= "'" + (CBCardNO.Text) + "'" + ","


            q &= "LOAD_TRUCKCOMPANY = "
            q &= "'" + (TruckCompanyId.Text) + "'" + " "
            q &= "WHERE LOAD_ID= "
            q &= "" + (Cbn6.Text) + "  "

            Try
                da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                da.UpdateCommand.ExecuteNonQuery()

            Catch ex As Exception
                MessageBox.Show("Unable to add information, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            'Update T_truck
            Try
                q = "Update T_truck set truck_driver='" & DriverID.Text & "' truck_company='" & TruckCompanyId.Text _
                    & " where truck_number='" & EDVehicle.Text & "'"
                da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                da.InsertCommand.ExecuteNonQuery()

            Catch ex As Exception
                ' q = ""
                ' MessageBox.Show("Wrong information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '  Cursor = Cursors.Default
                '  Exit Sub
            End Try
            ''''''End Update T_truck''''''''''''''

            'weightoutprint_Click(Nothing, Nothing)
            'MessageBox.Show("Update data Successfully", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Main.CEvent("USER :" + Main.U_NAME + " Update Advisenote Reference: " + Cbn8.Text)
            Bcancel_Click(sender, e)
            conn.Close()
        End If
    End Sub

    Private Sub Unloading_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'TODO: This line of code loads data into the 'FPTDataSet.V_CARDFREE' table. You can move, or remove it, as needed.
        Me.V_CARDFREETableAdapter.Fill(Me.FPTDataSet.V_CARDFREE)
        'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
        Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOCATION' table. You can move, or remove it, as needed.
        Me.T_LOCATIONTableAdapter.Fill(Me.FPTDataSet.T_LOCATION)
        'TODO: This line of code loads data into the 'FPTDataSet.V_UNLOADINGNOTE' table. You can move, or remove it, as needed.
        '   Me.V_UNLOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_UNLOADINGNOTE)

        Me.T_PACKINGTableAdapter.Fill(Me.FPTDataSet.T_PACKING)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STATUS' table. You can move, or remove it, as needed.
        Me.T_STATUSTableAdapter.Fill(Me.FPTDataSet.T_STATUS)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPMENT_TYPE' table. You can move, or remove it, as needed.

        Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'Me.FPTDataSet.V_LOADINGNOTE
        'TODO: This line of code loads data into the 'FPTDataSet.V_LOADINGNOTE' table. You can move, or remove it, as needed.

        'Me.V_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_LOADINGNOTE)

        'Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
        'Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo("en-us")
        'Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Control Panel\International", "sShortDate", "dd/MM/yyyy")

        'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
        'TODO: This line of code loads data into the 'FPTDataSet.T_TRUCK' table. You can move, or remove it, as needed.
        Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
        Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)

        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(EDVehicle, "Vehicle No")
        MyErrorProvider.Controls.Add(EDDriver, "Driver Name")
        MyErrorProvider.Controls.Add(EDShipper, "Sales ORG")
        MyErrorProvider.Controls.Add(EDProduct, "Product")
        '   MyErrorProvider.Controls.Add(EDLocation, "Location")
        MyErrorProvider.Controls.Add(f04_01_Product, "Product Code or Mat Code")
        MyErrorProvider.ClearAllErrorMessages()




        DatetimeDBGrid1.Value = Date.Now.Date

        Btadd.Enabled = CheckAddPermisstion(Me.Tag)
        Btedit.Enabled = CheckEditPermisstion(Me.Tag)
        Btprint.Enabled = CheckPrintPermisstion(Me.Tag)
        'BtWorkPrint.Enabled = CheckPrintPermisstion(Me.Tag)

    End Sub

    Private Sub Quantity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Quantity.TextChanged
        If Quantity.Text <> "" Or Quantity.Text <> "0" Then
            Try
                PackingWeight.Text = Convert.ToDouble(Quantity.Text * P_Weight.Text)
            Catch ex As Exception
                PackingWeight.Text = 0
            End Try
        End If
    End Sub

    Private Sub UpdateWeightOut_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateWeightOut.TextChanged
        If UpdateWeightOut.Text = "" Or UpdateWeightOut.Text = "0" Then
            LawWeightout_TextChanged(sender, e)
            Exit Sub
        End If

        If PackingWeight.Text = "" Then
            PackingWeight.Text = "0"
        End If

        If UpdateWeightIn.Text = "" Or UpdateWeightIn.Text = "0" Then
            Try

                WeightTotal.Text = Convert.ToDouble(LawWeightIn.Text - UpdateWeightOut.Text)
                Netweight.Text = Convert.ToDouble(LawWeightIn.Text - UpdateWeightOut.Text)
            Catch ex As Exception
            End Try
        Else
            Try
                WeightTotal.Text = Convert.ToDouble(UpdateWeightIn.Text - UpdateWeightOut.Text)
                Netweight.Text = Convert.ToDouble(UpdateWeightIn.Text - UpdateWeightOut.Text)
            Catch ex As Exception
            End Try
        End If
        WeightTotal_TextChanged(sender, e)
    End Sub

    Private Sub LawWeightout_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LawWeightout.TextChanged
        If UpdateWeightOut.Text <> "" And UpdateWeightOut.Text <> "0" Then
            UpdateWeightOut_TextChanged(sender, e)
            Exit Sub
        End If
        If PackingWeight.Text = "" Then
            PackingWeight.Text = "0"
        End If

        If UpdateWeightIn.Text = "" Or UpdateWeightIn.Text = "0" Then
            Try

                WeightTotal.Text = Convert.ToDouble(LawWeightIn.Text - LawWeightout.Text)
            Catch ex As Exception
            End Try
        Else
            Try
                WeightTotal.Text = Convert.ToDouble(UpdateWeightIn.Text - LawWeightout.Text)
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub PackingWeight_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PackingWeight.TextChanged
        WeightTotal_TextChanged(sender, e)
        If UpdateWeightOut.Text = "" Or UpdateWeightOut.Text = "0" Then
            LawWeightout_TextChanged(sender, e)
            Exit Sub
        Else
            If LawWeightout.Text = "" Or LawWeightout.Text = "0" Then
                Exit Sub
            End If
            UpdateWeightOut_TextChanged(sender, e)
        End If

    End Sub

    Private Sub WeightTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WeightTotal.TextChanged
        Try
            Netweight.Text = Convert.ToDouble(WeightTotal.Text - PackingWeight.Text)
            Weighttol.Text = Convert.ToDouble(Netweight.Text - Cbn10.Text).ToString("#.00")

            'Weighttol.Text = String.Format("{0:0.0}", Weighttol.Text)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub weightoutprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTweightoutprint.Click
        Try


            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()

            Dim ref As String
            ref = V_UNLOADINGNOTEBindingSource.Item(V_UNLOADINGNOTEBindingSource.Position)("reference").ToString()
            'ref = Cbn8.Text

            Dim Myreport As New ReportDocument
            Myreport = New ReportDocument
            Dim sql As String
            sql = "Select * from V_unLoadingnote where reference =" + ref + " and load_status=3"
            Dim da7 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim ds7 As New DataSet()
            Dim dt7 As New DataTable
            da7.Fill(dt7)

            Dim q As String
            q = "Select * from T_Picture"
            Dim da8 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim ds8 As New DataSet()
            Dim dt8 As New DataTable

            If dt7.Rows.Count = 0 Then
                MessageBox.Show("Load Id Status Connot Weight-out", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                Try
                    da7.Fill(ds7, "V_unLoadingnote")
                    'da8.Fill(ds7, "T_Picture")
                    Myreport.Load(TSHIPPERBindingSource(0)("UNLOAD_WeightTicket").ToString())
                    'Select Case Main.SALE_ORG
                    '    Case 1100
                    '        Myreport.Load("TOCUNWorkOrder.rpt")
                    '    Case 1200
                    '        Myreport.Load("TEAUnloadWeightTicket.rpt")
                    '    Case 1500
                    '        Myreport.Load("TOLUnloadWeightTicket.rpt")
                    '    Case 1600
                    '        Myreport.Load("TOLUnloadWeightTicket.rpt")
                    '    Case Else
                    '        Myreport.Load("UnloadWeightTicket.rpt")
                    'End Select
                    ' Myreport.Load("UnloadWeightTicket.rpt")
                    Try
                        Myreport.PrintOptions.PrinterName = My.Settings.WORK_P
                    Catch ex As Exception

                    End Try

                    Try
                        Myreport.PrintOptions.PaperOrientation = My.Settings.WORK_PaperOrientation
                        Myreport.PrintOptions.PaperSize = My.Settings.WORK_PaperSize
                    Catch ex As Exception
                    End Try
                    Myreport.SetDataSource(ds7.Tables("V_unLoadingnote"))
                    ReportPrint.CrystalReportViewer1.ReportSource = Myreport
                    ReportPrint.Show()

                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateWeightIn_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateWeightIn.TextChanged
        UpdateWeightOut_TextChanged(sender, e)
    End Sub

    Private Sub FillByToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    Me.V_UNLOADINGNOTETableAdapter.FillBy(Me.FPTDataSet.V_UNLOADINGNOTE)
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try

    End Sub

    Private Sub DBGrid1_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DBGrid1.CellFormatting
        Try

            With sender.Rows(e.RowIndex)
                Select Case UCase(.Cells(3).Value)
                    Case UCase("Waiting")
                        .DefaultCellStyle.BackColor = Color.White
                    Case UCase("Finished")
                        If Me.DBGrid1.Columns(e.ColumnIndex).Name = "LOAD_DID" Then
                            If e.Value IsNot Nothing Then
                                e.CellStyle.BackColor = Color.Green
                            End If
                        End If
                        ' .DefaultCellStyle.BackColor = Color.Yellow

                End Select
            End With

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Cbn10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cbn10.TextChanged
        WeightTotal_TextChanged(sender, e)
    End Sub

    Private Sub DatetimeDBGrid1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatetimeDBGrid1.ValueChanged

        SelectVUNLoadingNote()
        'Dim DD As String
        'DD = "Loaddate='" + String.Format("{0:yyyy-M-d}", DatetimeDBGrid1.Value) + "'"
        'V_UNLOADINGNOTEBindingSource.Filter = DD
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        f05_01_Advisenote.Show()
    End Sub

    Private Sub Weighttol_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Weighttol.TextChanged
        If Weighttol.Text = ".00" Then Weighttol.Text = "0"
    End Sub


    Private Sub EDVehicle_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles EDVehicle.KeyUp, EDTransNO.KeyUp, EDShipper.KeyUp, EDProduct.KeyUp, EDDriver.KeyUp, EDCustomer.KeyUp
        If e.KeyCode = 13 Then
            EDVehicle_Leave(sender, Nothing)
        End If
    End Sub

    Private Sub EDVehicle_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDVehicle.Leave, EDTransNO.Leave, EDShipper.Leave, EDProduct.Leave, EDDriver.Leave, EDCustomer.Leave
        Dim SelectText As String
        Dim Name As String
        Try
            SelectText = sender.text
            Name = UCase(sender.name)

            sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (sender.SelectedIndex < 0) And (SelectText <> "") Then
                sender.SelectedIndex = -1
                If Name = UCase("EDVEHICLE") Then
                    f04_01_Truck.Close()
                    Me.AddOwnedForm(f04_01_Truck)
                    f04_01_Truck.ShowType = 1
                    f04_01_Truck.TRUCK_NO = SelectText
                    f04_01_Truck.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_Truck)
                    Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If f04_01_Truck.TRUCK_COMPANY <> "" Then
                        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)

                        EDTransNO.SelectedIndex = EDTransNO.FindStringExact(f04_01_Truck.TRUCK_COMPANY)
                        If EDTransNO.SelectedIndex <> -1 Then
                            EDProduct.Focus()
                        End If
                        f04_01_Truck.TRUCK_COMPANY = ""
                    End If

                    ''''''''''''''driver'''''''''''''''''''''''''''''''''''''
                    If f04_01_Truck.TRUCK_DRIVER <> "" Then
                        ' Dim q As String
                        Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
                        EDDriver.SelectedIndex = EDDriver.FindStringExact(f04_01_Truck.TRUCK_DRIVER)
                        If EDDriver.SelectedIndex <> -1 Then
                            EDShipper.Focus()
                        End If
                        f04_01_Truck.TRUCK_DRIVER = ""
                    End If

                ElseIf Name = UCase("EDTransNO") Then
                    f04_01_TransportationCompany.Close()
                    Me.AddOwnedForm(f04_01_TransportationCompany)
                    f04_01_TransportationCompany.ShowType = 1
                    f04_01_TransportationCompany.CompanyCode = SelectText
                    f04_01_TransportationCompany.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_TransportationCompany)
                    Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)

                    sender.SelectedIndex = sender.FindStringExact(SelectText)

                ElseIf Name = UCase("EDDRIVER") Then
                    Driver.Close()
                    Me.AddOwnedForm(Driver)
                    Driver.ShowType = 1
                    Driver.DRIVER_NAME = SelectText
                    Driver.ShowDialog()
                    Me.RemoveOwnedForm(Driver)
                    Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)



                ElseIf Name = UCase("EDSHIPPER") Then
                    f04_01_Shipper.Close()
                    Me.AddOwnedForm(f04_01_Shipper)
                    f04_01_Shipper.ShowType = 1
                    f04_01_Shipper.Shipper_code = SelectText
                    f04_01_Shipper.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_Shipper)
                    Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)


                ElseIf Name = UCase("EDCUSTOMER") Then
                    f04_01_CustomerCompany.Close()
                    Me.AddOwnedForm(f04_01_CustomerCompany)
                    f04_01_CustomerCompany.ShowType = 1
                    f04_01_CustomerCompany.CompanyCode = SelectText
                    f04_01_CustomerCompany.CompanyName = SelectText
                    f04_01_CustomerCompany.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_CustomerCompany)
                    Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)




                ElseIf Name = UCase("EDProduct") Then
                    f04_01_Product.Close()
                    Me.AddOwnedForm(f04_01_Product)
                    f04_01_Product.ShowType = 1
                    f04_01_Product.SpCode = SelectText
                    f04_01_Product.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_Product)
                    Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)

                    sender.SelectedIndex = sender.FindStringExact(SelectText)

                End If



            End If
            If Name = UCase("EDVEHICLE") Then Cbn2_TextChanged(sender, e)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub EDVehicle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDVehicle.TextChanged
        If EDVehicle.Text = "" Then
            TruckId.SelectedIndex = -1
        Else
            TruckId.SelectedIndex = EDVehicle.SelectedIndex
            Try


                TDRIVERBindingSource.Position = TDRIVERBindingSource.Find("ID", TTRUCKBindingSource.Item(TruckId.SelectedIndex)("TRUCK_DRIVER").ToString)

            Catch ex As Exception
                TDRIVERBindingSource.Position = -1
                EDDriver.SelectedIndex = -1
            End Try

            Try
                TCOMPANYBindingSource.Position = TCOMPANYBindingSource.Find("COMPANY_ID", TTRUCKBindingSource.Item(TruckId.SelectedIndex)("TRUCK_COMPANY").ToString)
            Catch ex As Exception
                TCOMPANYBindingSource.Position = -1
                EDTransNO.SelectedIndex = -1
            End Try
        End If
    End Sub

    Private Sub EDDriver_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If EDDriver.Text = "" Then
                DriverID.SelectedIndex = -1
            Else
                DriverID.SelectedIndex = EDDriver.SelectedIndex
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub EDShipper_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDShipper.TextChanged
        If EDShipper.Text = "" Then
            ShipperId.SelectedIndex = -1
            TCustomerBindingSource.RemoveFilter()
        Else
            ShipperId.SelectedIndex = EDShipper.SelectedIndex
            TCustomerBindingSource.Filter = "SORG_CODE='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "'" _
                & " or SORG_CODE=''" & " or SORG_CODE IS NULL"
            EDCustomer.SelectedIndex = -1

        End If
    End Sub

    Private Sub EDCustomer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDCustomer.TextChanged
        If EDCustomer.Text = "" Then
            CustomerID.SelectedIndex = -1
        Else
            CustomerID.SelectedIndex = EDCustomer.SelectedIndex
        End If
    End Sub

    Private Sub EDTransNO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDTransNO.TextChanged
        If EDTransNO.Text = "" Then
            TruckCompanyId.SelectedIndex = -1
        Else
            TruckCompanyId.SelectedIndex = EDTransNO.SelectedIndex
        End If
    End Sub

    Private Sub Location_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDLocation.TextChanged
        'If EDTransNO.Text = "" Then
        '    TruckCompanyId.SelectedIndex = -1
        'Else
        '    TruckCompanyId.SelectedIndex = EDTransNO.SelectedIndex
        'End If
    End Sub

    Private Sub Location_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDLocation.Leave
        Dim SelectText As String
        Dim Name As String
        Try
            SelectText = sender.text
            Name = UCase(sender.name)

            sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (sender.SelectedIndex < 0) And (SelectText <> "") Then
                sender.SelectedIndex = -1
                FRLocation.Close()
                Me.AddOwnedForm(FRLocation)
                FRLocation.ShowType = 1
                FRLocation.locationCode = SelectText
                FRLocation.ShowDialog()
                Me.RemoveOwnedForm(FRLocation)
                Me.T_LOCATIONTableAdapter.Fill(Me.FPTDataSet.T_LOCATION)
                'Dim q As String
                'q = ""
                'q = "select * from T_company order by COMPANY_Code"
                'Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                'Dim ds2 As New DataSet()
                'Dim dt2 As New DataTable
                'Dim numrow, indexx As Integer
                'da2.Fill(dt2)
                'numrow = dt2.Rows.Count
                'TruckCompanyId.Items.Clear()
                'Cbn3.Items.Clear()
                'TruckCompanyId.Items.Add("")
                'Cbn3.Items.Add("")
                'For indexx = 0 To numrow - 1
                '    TruckCompanyId.Items.Add(dt2.Rows(indexx).Item("COMPANY_ID").ToString())
                '    Cbn3.Items.Add(dt2.Rows(indexx).Item("Company_code").ToString())
                'Next indexx
                sender.SelectedIndex = sender.FindStringExact(SelectText)
                FRLocation.locationCode = ""
            End If

        Catch ex As Exception

        End Try

    End Sub



    Private Sub EDLocation_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles EDLocation.KeyUp
        If e.KeyCode = 13 Then
            Location_Leave(sender, Nothing)
        End If
    End Sub

    Private Sub BtVehicleEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtVehicleEdit.Click
        Dim SelectText As String
        '  Dim Name As String
        Try
            SelectText = EDVehicle.Text


            '     sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (EDVehicle.SelectedIndex > 0) And (EDVehicle.Text <> "") Then

                f04_01_Truck.Close()
                Me.AddOwnedForm(f04_01_Truck)
                f04_01_Truck.TRUCK_NO = SelectText
                f04_01_Truck.ShowType = 3
                f04_01_Truck.ShowDialog()
                Me.RemoveOwnedForm(f04_01_Truck)
                Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
                EDVehicle.SelectedIndex = EDVehicle.FindStringExact(SelectText)
                EDVehicle_TextChanged(sender, e)
                EDVehicle_Leave(sender, Nothing)
                Cbn2_TextChanged(sender, e)
                ' EDVehicle_Leave(sender, e)

                ' Order_Click(sender, e)

            End If

        Catch ex As Exception

        End Try
    End Sub


    Private Sub EDPacking_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDPacking.Leave
        Dim SelectText As String
        '  Dim Name As String
        Try
            SelectText = Me.EDPacking.Text


            '     sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (Me.EDPacking.SelectedIndex < 0) And (Me.EDPacking.Text <> "") Then
                Fpacking.Close()
                Me.AddOwnedForm(Fpacking)
                Fpacking.ShowType = 1
                Fpacking.PCode = SelectText
                Fpacking.ShowDialog()
                Me.RemoveOwnedForm(Fpacking)
                Me.T_PACKINGTableAdapter.Fill(Me.FPTDataSet.T_PACKING)
                Me.EDPacking.SelectedIndex = Me.EDPacking.FindStringExact(SelectText)

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub EDPacking_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles EDPacking.KeyUp
        If e.KeyCode = 13 Then
            EDPacking_Leave(sender, Nothing)
        End If
    End Sub


    Private Sub DBGrid1_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles DBGrid1.CellPainting
        Try

            If e.ColumnIndex = 0 AndAlso e.RowIndex <> -1 Then

                Using gridBrush As Brush = New SolidBrush(sender.GridColor), backColorBrush As Brush = New SolidBrush(e.CellStyle.BackColor)

                    Using gridLinePen As Pen = New Pen(gridBrush)
                        ' Clear cell  
                        e.Graphics.FillRectangle(backColorBrush, e.CellBounds)
                        ' Draw line (bottom border and right border of current cell)  
                        'If next row cell has different content, only draw bottom border line of current cell  
                        If e.RowIndex < sender.Rows.Count - 2 AndAlso sender.Rows(e.RowIndex + 1).Cells(e.ColumnIndex).Value.ToString() <> e.Value.ToString() Then
                            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
                        End If


                        ' Draw right border line of current cell  

                        e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1, e.CellBounds.Top, e.CellBounds.Right - 1, e.CellBounds.Bottom)

                        ' draw/fill content in current cell, and fill only one cell of multiple same cells  

                        If Not e.Value Is Nothing Then

                            If e.RowIndex > 0 AndAlso sender.Rows(e.RowIndex - 1).Cells(e.ColumnIndex).Value.ToString() = e.Value.ToString() Then

                            Else

                                e.Graphics.DrawString(CType(e.Value, String), e.CellStyle.Font, Brushes.Black, e.CellBounds.X + 2, e.CellBounds.Y + 5, StringFormat.GenericDefault)

                            End If

                        End If

                        e.Handled = True
                    End Using

                End Using
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub DBGrid1_CellErrorTextNeeded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellErrorTextNeededEventArgs) Handles DBGrid1.CellErrorTextNeeded

    End Sub



    Private Sub Unloading_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        RegisterHotKey(Me.Handle, 100, MOD_CONTROL, Keys.N)
        RegisterHotKey(Me.Handle, 101, MOD_CONTROL, Keys.E)
        RegisterHotKey(Me.Handle, 102, MOD_CONTROL, Keys.P)
        RegisterHotKey(Me.Handle, 200, MOD_ALT, Keys.S)
        RegisterHotKey(Me.Handle, 201, MOD_CONTROL, Keys.I)
        RegisterHotKey(Me.Handle, 202, MOD_CONTROL, Keys.O)
        RegisterHotKey(Me.Handle, 203, MOD_CONTROL, Keys.S)
        RegisterHotKey(Me.Handle, 204, MOD_CONTROL, Keys.U)
    End Sub

    Private Sub Unloading_Deactivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Deactivate
        UnregisterHotKey(Me.Handle, 100)
        UnregisterHotKey(Me.Handle, 101)
        UnregisterHotKey(Me.Handle, 102)
        UnregisterHotKey(Me.Handle, 200)
        UnregisterHotKey(Me.Handle, 201)
        UnregisterHotKey(Me.Handle, 202)
        UnregisterHotKey(Me.Handle, 203)
        UnregisterHotKey(Me.Handle, 204)
    End Sub

End Class