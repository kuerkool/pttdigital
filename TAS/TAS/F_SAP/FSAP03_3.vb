﻿Imports TAS_TOL.SAP03

Public Class FSAP03_3
    Public DO_NO As String
    Dim conn As SqlClient.SqlConnection = GetConnection()

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Private Sub TDOBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TDOBindingSource.PositionChanged
        'TBATCHLOSTTASBindingSource.Filter = "Higher_Level=" & TDOBindingSource.Item(TDOBindingSource.Position)("DO_ITEM")
    End Sub

    Private Sub SelectBatch(ByVal DO_NO As String)
        Dim q As String
        q = "select * from  T_BATCH_LOT_TAS Where DO_NO='" & DO_NO + "' and STATUS <> 12"
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim MyDataSet As New DataSet
        da.Fill(MyDataSet, "T_BATCH_LOT_TAS")
        TBATCHLOSTTASBindingSource.DataSource = MyDataSet
        TBATCHLOSTTASBindingSource.DataMember = "T_BATCH_LOT_TAS"
        BatchGrid.DataSource = TBATCHLOSTTASBindingSource
        BindingNavigator2.BindingSource = TBATCHLOSTTASBindingSource
    End Sub

    Private Sub SelectDO(ByVal DO_NO As String)
        Dim q As String
        q = "select * from  T_DO Where DO_NO='" & DO_NO + "'  and STATUS <> 12"
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim MyDataSet As New DataSet
        da.Fill(MyDataSet, "T_DO")
        TDOBindingSource.DataSource = MyDataSet
        TDOBindingSource.DataMember = "T_DO"
        DoGrid.DataSource = TDOBindingSource
        BindingNavigator1.BindingSource = TDOBindingSource
    End Sub

    Private Sub SelectShipment(ByVal DO_NO As String)
        Dim q As String
        q = "select  MAX(sHIPPdO_NO)as sHIPPdO_NO,MAX(sHIP_NO) as sHIP_NO,MAX(sHIP_TYPE) as sHIP_TYPE,MAX(tRN_PLAN_POINT) as tRN_PLAN_POINT,MAX(cREATE_DATE)as cREATE_DATE," _
            & "MAX(cREATE_TIME)as cREATE_TIME,MAX(cONTAINER) as cONTAINER,MAX(dRIVER_NAME) as dRIVER_NAME,MAX(cAR_LINCENSE)as cAR_LINCENSE,MAX(fORWD_AGENT)as fORWD_AGENT,(cast(MIN(cast(wEIGHT_IN as int)) as Varchar(20))+ ' / ' +cast(min(aCT_LOAD_STIME) as varchar(5))) as wEIGHT_IN," _
          & "(cast(MAX(cast(wEIGHT_OUT as int)) as varchar(20))+' / '+ cast(max(aCT_LOAD_ETIME) as varchar(5)))   as  wEIGHT_OUT,MAX(sEAL_NO)as sEAL_NO,MAX(sHIPP_TYPE) as sHIPP_TYPE,MAX(sHIPP_COND) as sHIPP_COND,MAX(CHK_DAT)as CHK_DAT ,max(CHK_TIM) as CHK_TIM,max(LOAD_E_DAT) as LOAD_E_DAT,Max(LOAD_E_TIM) as LOAD_E_TIM  from  T_DO Where DO_NO='" & DO_NO + "' and   (STATUS) <> 12  GROUP BY DO_NO"
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim MyDataSet As New DataSet
        da.Fill(MyDataSet, "T_DO")
        TDOShipmentBindingSource.DataSource = MyDataSet
        TDOShipmentBindingSource.DataMember = "T_DO"
        ShipmentGrid.DataSource = TDOShipmentBindingSource
        ShipmentNav.BindingSource = TDOShipmentBindingSource
    End Sub

    Private Sub FSAP03_3_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Try
            SelectDO(DO_NO)
            SelectBatch(DO_NO)
            SelectShipment(DO_NO)
            BtPost.Enabled = True
            '  TBATCHLOSTTASBindingSource.Filter = "Higher_Level=" & TDOBindingSource.Item(TDOBindingSource.Position)("DO_ITEM")
        Catch ex As Exception

        End Try
    End Sub


    Private Sub FSAP03_3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_DO' table. You can move, or remove it, as needed.
        '  Me.T_DOTableAdapter.Fill(Me.FPTDataSet.T_DO)

    End Sub

    Private Sub SaveDO(ByVal ds As DataSet, ByVal DO_NO As String)
        TDOBindingSource.EndEdit()
        BindingContext(ds, "T_DO").EndCurrentEdit()

        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try

  
        Try
                Dim sql As String = "select * from  T_DO Where DO_NO='" & DO_NO + "'  and STATUS <> 12"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                If ds.HasChanges Then
                    da.Update(ds, "T_DO")
                    ds.AcceptChanges()
                End If
            Finally

                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Savebatch(ByVal ds As DataSet, ByVal DO_NO As String)
        TBATCHLOSTTASBindingSource.Filter = ""
        TBATCHLOSTTASBindingSource.EndEdit()
        BindingContext(ds, "T_BATCH_LOT_TAS").EndCurrentEdit()

        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "select * from  T_BATCH_LOT_TAS Where DO_NO='" & DO_NO + "'  and STATUS <> 12"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                If ds.HasChanges Then
                    da.Update(ds, "T_BATCH_LOT_TAS")
                    ds.AcceptChanges()
                End If
            Finally

                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub SaveShipment(ByVal ds As DataSet, ByVal DO_NO As String)
        TDOShipmentBindingSource.EndEdit()
        BindingContext(ds, "T_DO").EndCurrentEdit()

        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "select top 1 * from  T_DO Where DO_NO='" & DO_NO + "' and  WHERE STATUS <> 12"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                If ds.HasChanges Then
                    da.Update(ds, "T_DO")
                    ds.AcceptChanges()
                End If
            Finally

                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SaveDO(TDOBindingSource.DataSource, DO_NO)
        Savebatch(TBATCHLOSTTASBindingSource.DataSource, DO_NO)
        FSAP03_3_Shown(sender, e)
        'SaveShipment(TDOShipmentBindingSource.DataSource, DO_NO)
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        Close()
    End Sub

    Private Sub BtPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtPost.Click

        Cursor = Cursors.WaitCursor

        Me.BtPost.Enabled = False
        ' Bsave.PerformClick()
        Dim _BAG As String
        Dim _LIQUID As String
        If f05_02_Advisenote2.RadioBig.Checked Then
            _BAG = "X"
            _LIQUID = ""
        ElseIf f05_02_Advisenote2.RadioLiquid.Checked Then
            _BAG = ""
            _LIQUID = "X"
        End If

        Try

            Dim _PostDOData(TDOBindingSource.Count - 1) As Create_Shipment_DO03_Out_DTITEM

            Dim _PostShipData(0) As Create_Shipment_SHIP03_Out_DTITEM
            TBATCHLOSTTASBindingSource.RemoveFilter()
            Dim _PostBATCHData(TBATCHLOSTTASBindingSource.Count - 1) As Create_Shipment_BATCH_Out_DTITEM

            For i = 0 To _PostDOData.Length - 1

                _PostDOData(i) = New Create_Shipment_DO03_Out_DTITEM
                _PostDOData(i).DO_NO = TDOBindingSource.Item(i)("DO_NO").ToString
                _PostDOData(i).DO_ITEM = TDOBindingSource.Item(i)("DO_ITEM").ToString
                _PostDOData(i).SALES_ORG = TDOBindingSource.Item(i)("SALES_ORG").ToString
                _PostDOData(i).SD_DOC_CAT = TDOBindingSource.Item(i)("SD_DOC_CAT").ToString
                _PostDOData(i).ACT_GI_DATE = Convert.ToDateTime(TDOBindingSource.Item(i)("ACT_GI_DATE").ToString).ToString("dd/MM/yyyy")  ' Convert.ToDateTime("10/07/2011").ToString("dd/MM/yyyy") ''''''''''''''มั่ว''''''''''''''''''''
                _PostDOData(i).ACT_GI_TIME = Convert.ToDateTime(TDOBindingSource.Item(i)("ACT_GI_TIME").ToString).ToString("HH:mm:ss")  ' Convert.ToDateTime("10/07/2011").ToString("dd/MM/yyyy") ''''''''''''''มั่ว''''''''''''''''''''

                _PostDOData(i).MAT_CODE = TDOBindingSource.Item(i)("MAT_CODE").ToString
                _PostDOData(i).PLANT = TDOBindingSource.Item(i)("PLANT").ToString
                _PostDOData(i).STORAGE_LOC = TDOBindingSource.Item(i)("STORAGE_LOC").ToString
                _PostDOData(i).BATCH_NO = TDOBindingSource.Item(i)("BATCH_NO").ToString
                'If _LIQUID = "X" Then
                '    _PostDOData(i).ACT_QTY_DELV_SU = Int(TDOBindingSource.Item(i)("ACT_QTY_DELV_SU").ToString).ToString
                '    _PostDOData(i).ACT_QTY_DELV_SKU = Int(TDOBindingSource.Item(i)("ACT_QTY_DELV_SKU").ToString).ToString     ''''''''
                '    _PostDOData(i).PICK_QTY_SU = Int(TDOBindingSource.Item(i)("PICK_QTY_SU").ToString).ToString     ''''
                'Else
                _PostDOData(i).ACT_QTY_DELV_SU = (TDOBindingSource.Item(i)("ACT_QTY_DELV_SU").ToString)
                _PostDOData(i).ACT_QTY_DELV_SKU = (TDOBindingSource.Item(i)("ACT_QTY_DELV_SKU").ToString)     ''''''''
                _PostDOData(i).PICK_QTY_SU = (TDOBindingSource.Item(i)("PICK_QTY_SU").ToString)     ''''
                '  End If

                _PostDOData(i).BASE_UOM = TDOBindingSource.Item(i)("BASE_UOM").ToString
                _PostDOData(i).SALES_UNIT = TDOBindingSource.Item(i)("SALES_UNIT").ToString
                _PostDOData(i).NET_WEIGHT = TDOBindingSource.Item(i)("NET_WEIGHT").ToString
                _PostDOData(i).GROSS_WEIGHT = TDOBindingSource.Item(i)("GROSS_WEIGHT").ToString
                _PostDOData(i).WEIGHT_UNIT = TDOBindingSource.Item(i)("WEIGHT_UNIT").ToString
                ''''''''''''''''''''''''''
                _PostDOData(i).DOC_CATEGORY = TDOBindingSource.Item(i)("DOC_CATEGORY").ToString    ''''

                _PostDOData(i).HIGHER_LEVEL_ITEM = TDOBindingSource.Item(i)("HIGHER_LEVEL_ITEM").ToString

                _PostDOData(i).COA_NO = TDOBindingSource.Item(i)("COA").ToString
                _PostDOData(i).DO_INS_TXT = TDOBindingSource.Item(i)("dELIVERY_INSTRUCTION_TEXT").ToString & "(" & TDOBindingSource.Item(i)("DO_INS_TXT").ToString & ")"
                _PostDOData(i).LOAD_PT = TDOBindingSource.Item(i)("LOAD_PT").ToString

                ''''
                If i = 0 Then
                    _PostShipData(i) = New Create_Shipment_SHIP03_Out_DTITEM
                    _PostShipData(i).SHIP_NO = TDOShipmentBindingSource.Item(i)("SHIP_NO").ToString
                    _PostShipData(i).SHIP_TYPE = TDOShipmentBindingSource.Item(i)("SHIP_TYPE").ToString  '''''''''''''''''''''''''''
                    _PostShipData(i).TRN_PLAN_POINT = TDOShipmentBindingSource.Item(i)("tRN_PLAN_POINT").ToString  ''''''''''''''''''''
                    _PostShipData(i).CREATE_DATE = Convert.ToDateTime(TDOShipmentBindingSource.Item(i)("CREATE_DATE").ToString).ToString("dd/MM/yyyy") 'Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
                    _PostShipData(i).CREATE_TIME = Convert.ToDateTime(TDOShipmentBindingSource.Item(i)("CREATE_TIME").ToString).ToString("HH:mm:ss") ' Convert.ToDateTime("11:11:11").ToString("HH:mm:ss")
                    _PostShipData(i).CHK_DAT = Convert.ToDateTime(TDOShipmentBindingSource.Item(i)("CHK_DAT").ToString).ToString("dd/MM/yyyy")
                    _PostShipData(i).CHK_TIM = Convert.ToDateTime(TDOShipmentBindingSource.Item(i)("CHK_TIM").ToString).ToString("HH:mm:ss")
                    _PostShipData(i).LOAD_E_DAT = Convert.ToDateTime(TDOShipmentBindingSource.Item(i)("LOAD_E_DAT").ToString).ToString("dd/MM/yyyy")
                    _PostShipData(i).LOAD_E_TIM = Convert.ToDateTime(TDOShipmentBindingSource.Item(i)("LOAD_E_TIM").ToString).ToString("HH:mm:ss")

                    _PostShipData(i).CONTAINER = TDOShipmentBindingSource.Item(i)("CONTAINER").ToString
                    _PostShipData(i).DRIVER_NAME = TDOShipmentBindingSource.Item(i)("DRIVER_NAME").ToString
                    _PostShipData(i).CAR_LINCENSE = TDOShipmentBindingSource.Item(i)("CAR_LINCENSE").ToString
                    _PostShipData(i).FORWD_AGENT = TDOShipmentBindingSource.Item(i)("FORWD_AGENT").ToString ' บริษัทขนส่ง
                    _PostShipData(i).WEIGHT_IN = TDOShipmentBindingSource.Item(i)("WEIGHT_IN").ToString
                    _PostShipData(i).WEIGHT_OUT = TDOShipmentBindingSource.Item(i)("WEIGHT_OUT").ToString
                    _PostShipData(i).SEAL_NO = TDOShipmentBindingSource.Item(i)("SEAL_NO").ToString
                    If TDOShipmentBindingSource.Item(i)("CONTAINER").ToString <> "" Then
                        _PostShipData(i).SEAL_NO = _PostShipData(i).SEAL_NO + "/Con No." + TDOShipmentBindingSource.Item(i)("CONTAINER").ToString
                    End If
                    _PostShipData(i).SHIPP_TYPE = TDOShipmentBindingSource.Item(i)("SHIPP_TYPE").ToString
                    _PostShipData(i).SHIPP_COND = TDOShipmentBindingSource.Item(i)("SHIPP_COND").ToString
                    _PostShipData(i).DO_NO = TDOShipmentBindingSource.Item(i)("sHIPPdO_NO").ToString
                    '  _PostShipData(i).ACT_LOAD_SDATE = Convert.ToDateTime(TDOBindingSource.Item(i)("ACT_LOAD_SDATE").ToString).ToString("dd/MM/yyyy")  ' Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
                    '  _PostShipData(i).ACT_LOAD_STIME = Convert.ToDateTime(TDOBindingSource.Item(i)("ACT_LOAD_STIME").ToString).ToString("HH:mm:ss") ' Convert.ToDateTime("11:11:11").ToString("HH:mm:ss")
                    '   _PostShipData(i).ACT_LOAD_EDATE = Convert.ToDateTime(TDOBindingSource.Item(i)("ACT_LOAD_EDATE").ToString).ToString("dd/MM/yyyy")  ' Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
                    ' _PostShipData(i).ACT_LOAD_ETIME = Convert.ToDateTime(TDOBindingSource.Item(i)("ACT_LOAD_ETIME").ToString).ToString("HH:mm:ss") ' Convert.ToDateTime("11:11:11").ToString("HH:mm:ss")
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If

            Next
            TBATCHLOSTTASBindingSource.RemoveFilter()
            'If _PostBATCHData.Length > 0 Then
            For i = 0 To _PostBATCHData.Length - 1
                _PostBATCHData(i) = New Create_Shipment_BATCH_Out_DTITEM
                _PostBATCHData(i).BATCH_NO = TBATCHLOSTTASBindingSource.Item(i)("BATCH_NO").ToString
                _PostBATCHData(i).DO_ITEM = TBATCHLOSTTASBindingSource.Item(i)("DO_ITEM").ToString
                _PostBATCHData(i).DO_NO = TBATCHLOSTTASBindingSource.Item(i)("DO_NO").ToString
                _PostBATCHData(i).HIGHER_LEVEL_ITEM = TBATCHLOSTTASBindingSource.Item(i)("Higher_Level").ToString

                'If _LIQUID = "X" Then
                '    _PostBATCHData(i).PICK_QTY_SU = Int(TBATCHLOSTTASBindingSource.Item(i)("stock_quantity").ToString).ToString
                'Else
                _PostBATCHData(i).PICK_QTY_SU = (TBATCHLOSTTASBindingSource.Item(i)("stock_quantity").ToString)
                ' End If
                _PostBATCHData(i).PLANT = TBATCHLOSTTASBindingSource.Item(i)("PLANT").ToString
                _PostBATCHData(i).STORAGE_LOC = TBATCHLOSTTASBindingSource.Item(i)("STOR").ToString
            Next
            'Else
            '    ' Not batch
            '    _PostBATCHData(0) = New DT_BATCHITEM
            '    _PostBATCHData(0).BATCH_NO = TDOBindingSource.Item(0)("BATCH_NO").ToString
            '    _PostBATCHData(0).DO_ITEM = TDOBindingSource.Item(0)("DO_ITEM").ToString
            '    _PostBATCHData(0).DO_NO = TBATCHLOSTTASBindingSource.Item(0)("DO_NO").ToString
            '    _PostBATCHData(0).HIGHER_LEVEL_ITEM = TDOBindingSource.Item(0)("HIGHER_LEVEL_ITEM").ToString
            '    _PostBATCHData(0).PICK_QTY_SU = TDOBindingSource.Item(0)("ACT_QTY_DELV_SU").ToString
            '    _PostBATCHData(0).PLANT = TDOBindingSource.Item(0)("PLANT").ToString
            '    _PostBATCHData(0).STORAGE_LOC = TDOBindingSource.Item(0)("STORAGE_LOC").ToString
            '  End If
            FSAP03_1.POST_DO_SHIP(_PostDOData, _PostShipData, _PostBATCHData, _BAG, _LIQUID)
        Catch ex As Exception
            Cursor = Cursors.Default
            BtPost.Enabled = True
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error message")
        End Try
    End Sub

    Private Sub FSAP03_3_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

        f05_02_Advisenote2.Bcancel.PerformClick()
    End Sub
End Class