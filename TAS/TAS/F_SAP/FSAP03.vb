﻿Imports TAS_TOL.SAP03
Imports System.Net
Public Class FSAP03
#Region " Post GI"
    Private Sub SEND_to_SAP(ByVal DTRequest As Create_Shipment_DATA03_Out_DT)
        Try
            Cursor = Cursors.WaitCursor
            FSAP03_2.BtPost.Enabled = False
            Dim myproxy As New Create_Shipment_Sync_Out_SIService
            Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
            Dim netCred As NetworkCredential = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            Dim uri As Uri = New Uri(objIniFile.GetString("Webservice", "URL03", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING03"))

            'Dim netCred As NetworkCredential = New NetworkCredential("TAS01", "initial1")
            'Dim uri As Uri = New Uri("http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING03")
            Dim credentials As ICredentials = netCred.GetCredential(uri, "Basic")
            myproxy.Url = uri.AbsoluteUri
            myproxy.Credentials = credentials
            myproxy.PreAuthenticate = True
            myproxy.Timeout = 30000
            ' myproxy.Credentials = credentials
            Dim DTReturn() As Create_Shipment_RETURN03_DTITEM
            DTReturn = myproxy.Create_Shipment_Sync_Out_SI(DTRequest)
            AddHandler myproxy.Create_Shipment_Sync_Out_SICompleted, AddressOf POST_GI
            myproxy.Create_Shipment_Sync_Out_SIAsync(DTRequest)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Cursor = Cursors.Default
        End Try
        FSAP03_2.BtPost.Enabled = True
        FSAP03_2.Cursor = Cursors.Default
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Private Sub POST_GI(ByVal sender As Object, ByVal e As Create_Shipment_Sync_Out_SICompletedEventArgs)


        Try
            If (Not e.Cancelled) Then
                Dim DTReturn() As Create_Shipment_RETURN03_DTITEM
                '     DTReturn = New DT_RETURN03ITEM
                DTReturn = e.Result
                'DODATA = MResponse.DO02
                'AddItems(lvRefresh, DODATA)
                'GroupBox1.Text = "Last Get " + CbShipper.Text + " DO from SAP on " + Now.ToString

                Dim conn As SqlClient.SqlConnection = GetConnection()

                conn.Open()
                Dim q As String = "Select DO_STATUS from  T_DO WHERE DO_STATUS=0 and DO_NO='" & f05_01_Advisenote.EDDONo.Text & "'"
                Dim da As New SqlClient.SqlDataAdapter(q, conn)
                Dim dt As New DataTable
                Dim ShipmentNo As String
                Dim MESSAGEReturn() As String
                da.Fill(dt)


                For i = 0 To DTReturn.Length - 1
                    If DTReturn(i).TYPE = "E" Then
                        MsgBox(" MESSAGE (TYPE E) : " & DTReturn(i).MESSAGE &
                          " " & DTReturn(i).MESSAGE_V1 &
                          " " & DTReturn(i).MESSAGE_V2 &
                          " " & DTReturn(i).MESSAGE_V3 &
                          " " & DTReturn(i).MESSAGE_V4 &
                          " " & DTReturn(i)._SYSTEM, MsgBoxStyle.Critical, "Error message")


                        If dt.Rows.Count > 0 Then
                            q = "UPDATE T_DO SET DO_STATUS=0 , DO_POSTDATE=getdate() where DO_NO='" & f05_01_Advisenote.EDDONo.Text & "'"
                            da = New SqlClient.SqlDataAdapter

                            da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                            da.UpdateCommand.ExecuteNonQuery()
                            f05_01_Advisenote.load_id = 0
                        End If

                    Else
                        MsgBox("MESSAGE (TYPE S) : " & DTReturn(i).MESSAGE &
                        " " & DTReturn(i).MESSAGE_V1 &
                        " " & DTReturn(i).MESSAGE_V2 &
                        " " & DTReturn(i).MESSAGE_V3 &
                        " " & DTReturn(i).MESSAGE_V4 &
                        " " & DTReturn(i)._SYSTEM, MsgBoxStyle.ApplicationModal, "Summary message")

                        MESSAGEReturn = Split(DTReturn(i).MESSAGE, " ")
                        Try
                            If UCase(MESSAGEReturn(3)) = UCase("Shipment") Then
                                ShipmentNo = Trim(MESSAGEReturn(4))

                            End If
                        Catch ex As Exception

                        End Try

                        q = "UPDATE T_DO SET DO_STATUS=1 , DO_POSTDATE=getdate(),sHIP_NO='" & ShipmentNo & "' where DO_NO='" & f05_01_Advisenote.EDDONo.Text & "'"
                        da = New SqlClient.SqlDataAdapter

                        da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                        da.UpdateCommand.ExecuteNonQuery()
                        f05_01_Advisenote.load_id = 0



                    End If
                Next
                Cursor = Cursors.Default
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Cursor = Cursors.Default
        End Try
        FSAP03_2.BtPost.Enabled = True
        FSAP03_2.Cursor = Cursors.Default
        'DoDetail.Enabled = True
    End Sub

    Public Sub POST_DO_SHIP(ByVal _DO03() As Create_Shipment_DO03_Out_DTITEM, ByVal _SHIP03() As Create_Shipment_SHIP03_Out_DTITEM,
                            ByVal _BATCH03() As Create_Shipment_BATCH_Out_DTITEM, ByVal _BAG As String, ByVal _LIQUID As String)
        Dim DTRequest As New Create_Shipment_DATA03_Out_DT
        ''''X or ""''''''''
        DTRequest.BAG = _BAG
        DTRequest.LIGUID = _LIQUID
        DTRequest.DO03 = _DO03
        DTRequest.SHIP03 = _SHIP03
        DTRequest.BATCH = _BATCH03
        SEND_to_SAP(DTRequest)
    End Sub
#End Region

#Region " Gen GI data"
    Function Gen_do(ByVal _DO03 As Create_Shipment_DO03_Out_DTITEM) As Create_Shipment_DO03_Out_DTITEM
        Gen_do = New Create_Shipment_DO03_Out_DTITEM
        Gen_do.DO_NO = _DO03.DO_NO
        Gen_do.DO_ITEM = _DO03.DO_ITEM
        Gen_do.SALES_ORG = _DO03.SALES_ORG
        Gen_do.SD_DOC_CAT = _DO03.SD_DOC_CAT
        Gen_do.ACT_GI_DATE = _DO03.ACT_GI_DATE
        Gen_do.MAT_CODE = _DO03.MAT_CODE
        Gen_do.PLANT = _DO03.PLANT
        Gen_do.STORAGE_LOC = _DO03.STORAGE_LOC
        Gen_do.BATCH_NO = _DO03.BATCH_NO
        Gen_do.ACT_QTY_DELV_SU = _DO03.ACT_QTY_DELV_SU
        Gen_do.BASE_UOM = _DO03.BASE_UOM
        Gen_do.SALES_UNIT = _DO03.SALES_UNIT
        Gen_do.NET_WEIGHT = _DO03.NET_WEIGHT
        Gen_do.GROSS_WEIGHT = _DO03.GROSS_WEIGHT
        Gen_do.WEIGHT_UNIT = _DO03.WEIGHT_UNIT
        Gen_do.ACT_QTY_DELV_SKU = _DO03.ACT_QTY_DELV_SKU

        Return Gen_do
    End Function
    Function Gen_ship(ByVal _SHIP03 As Create_Shipment_SHIP03_Out_DTITEM) As Create_Shipment_SHIP03_Out_DTITEM
        Gen_ship = New Create_Shipment_SHIP03_Out_DTITEM
        Gen_ship.SHIP_NO = _SHIP03.SHIP_NO
        Gen_ship.SHIP_TYPE = _SHIP03.SHIP_TYPE
        Gen_ship.TRN_PLAN_POINT = _SHIP03.TRN_PLAN_POINT
        Gen_ship.CREATE_DATE = _SHIP03.CREATE_DATE
        Gen_ship.CREATE_TIME = _SHIP03.CREATE_TIME
        Gen_ship.CONTAINER = _SHIP03.CONTAINER
        Gen_ship.DRIVER_NAME = _SHIP03.DRIVER_NAME
        Gen_ship.CAR_LINCENSE = _SHIP03.CAR_LINCENSE
        Gen_ship.FORWD_AGENT = _SHIP03.FORWD_AGENT
        Gen_ship.WEIGHT_IN = _SHIP03.WEIGHT_IN
        Gen_ship.WEIGHT_OUT = _SHIP03.WEIGHT_OUT
        Gen_ship.SEAL_NO = _SHIP03.SEAL_NO
        Gen_ship.SHIPP_TYPE = _SHIP03.SHIPP_TYPE
        Gen_ship.SHIPP_COND = _SHIP03.SHIPP_COND
        Gen_ship.DO_NO = _SHIP03.DO_NO
        '   Gen_ship.ACT_LOAD_SDATE = _SHIP03.ACT_LOAD_SDATE
        '   Gen_ship.ACT_LOAD_STIME = _SHIP03.ACT_LOAD_STIME
        '   Gen_ship.ACT_LOAD_EDATE = _SHIP03.ACT_LOAD_EDATE
        '   Gen_ship.ACT_LOAD_ETIME = _SHIP03.ACT_LOAD_ETIME
        Return Gen_ship
    End Function

#End Region


    Private Sub BtPostGi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtPostGi.Click
        Dim DTRequest As New Create_Shipment_DATA03_Out_DT
        Dim _DO03(0) As Create_Shipment_DO03_Out_DTITEM
        Dim _SHIP03(0) As Create_Shipment_SHIP03_Out_DTITEM
        _DO03(0) = New Create_Shipment_DO03_Out_DTITEM
        _SHIP03(0) = New Create_Shipment_SHIP03_Out_DTITEM
        '_DO03(0).DO_NO = "a"
        '_DO03(0).ACT_GI_DATE = "20110101"
        '_SHIP03(0).CREATE_DATE = "20111010"
        '_SHIP03(0).CREATE_TIME = "111111"
        '_SHIP03(0).ACT_LOAD_EDATE = "20111010"
        '_SHIP03(0).ACT_LOAD_ETIME = "111111"
        '_SHIP03(0).ACT_LOAD_SDATE = "20111010"
        '_SHIP03(0).ACT_LOAD_STIME = "111111"


        _DO03(0).DO_NO = "a"
        _DO03(0).ACT_GI_DATE = "01/01/2011"
        _SHIP03(0).CREATE_DATE = "01/01/2011"
        _SHIP03(0).CREATE_TIME = "11:11:11"
        ' _SHIP03(0).ACT_LOAD_EDATE = "01/01/2011"
        ' _SHIP03(0).ACT_LOAD_ETIME = "11:11:11"
        ' _SHIP03(0).ACT_LOAD_SDATE = "01/01/2011"
        ' _SHIP03(0).ACT_LOAD_STIME = "11:11:11"



        DTRequest.DO03 = _DO03
        DTRequest.SHIP03 = _SHIP03
        SEND_to_SAP(DTRequest)
    End Sub

    Private Sub FSAP03_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class