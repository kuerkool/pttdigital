﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FSAP02
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BtGetdata = New System.Windows.Forms.Button()
        Me.DpStart = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DpEnd = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.BtPost = New System.Windows.Forms.Button()
        Me.LbShipcode = New System.Windows.Forms.Label()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.BtCancel = New System.Windows.Forms.Button()
        Me.BtImport = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CbShipper = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.lvRefresh = New System.Windows.Forms.ListView()
        Me.GroupBox1.SuspendLayout()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtGetdata
        '
        Me.BtGetdata.Image = Global.TAS_TOL.My.Resources.Resources.get_info
        Me.BtGetdata.Location = New System.Drawing.Point(999, 21)
        Me.BtGetdata.Name = "BtGetdata"
        Me.BtGetdata.Size = New System.Drawing.Size(110, 42)
        Me.BtGetdata.TabIndex = 8
        Me.BtGetdata.Text = "&Get Data"
        Me.BtGetdata.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtGetdata.UseVisualStyleBackColor = True
        '
        'DpStart
        '
        Me.DpStart.CustomFormat = ""
        Me.DpStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DpStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DpStart.Location = New System.Drawing.Point(445, 29)
        Me.DpStart.Name = "DpStart"
        Me.DpStart.Size = New System.Drawing.Size(130, 26)
        Me.DpStart.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(348, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Start Date :"
        '
        'DpEnd
        '
        Me.DpEnd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DpEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DpEnd.Location = New System.Drawing.Point(672, 29)
        Me.DpEnd.Name = "DpEnd"
        Me.DpEnd.Size = New System.Drawing.Size(130, 26)
        Me.DpEnd.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.BtPost)
        Me.GroupBox1.Controls.Add(Me.LbShipcode)
        Me.GroupBox1.Controls.Add(Me.BtCancel)
        Me.GroupBox1.Controls.Add(Me.BtImport)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.CbShipper)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.BtGetdata)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.DpEnd)
        Me.GroupBox1.Controls.Add(Me.DpStart)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(4, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1363, 87)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Last Get Data from SAP on "
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1177, -12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 27)
        Me.Button2.TabIndex = 33
        Me.Button2.Text = "&Form3"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'BtPost
        '
        Me.BtPost.Location = New System.Drawing.Point(1276, 0)
        Me.BtPost.Name = "BtPost"
        Me.BtPost.Size = New System.Drawing.Size(75, 27)
        Me.BtPost.TabIndex = 32
        Me.BtPost.Text = "&Post"
        Me.BtPost.UseVisualStyleBackColor = True
        Me.BtPost.Visible = False
        '
        'LbShipcode
        '
        Me.LbShipcode.AutoSize = True
        Me.LbShipcode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_NameEN", True))
        Me.LbShipcode.Location = New System.Drawing.Point(107, 59)
        Me.LbShipcode.Name = "LbShipcode"
        Me.LbShipcode.Size = New System.Drawing.Size(42, 16)
        Me.LbShipcode.TabIndex = 3
        Me.LbShipcode.Text = "name"
        Me.LbShipcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BtCancel
        '
        Me.BtCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BtCancel.Image = Global.TAS_TOL.My.Resources.Resources.Button_Cancel
        Me.BtCancel.Location = New System.Drawing.Point(1230, 21)
        Me.BtCancel.Name = "BtCancel"
        Me.BtCancel.Size = New System.Drawing.Size(109, 42)
        Me.BtCancel.TabIndex = 10
        Me.BtCancel.Text = "&Cancel"
        Me.BtCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtCancel.UseVisualStyleBackColor = True
        '
        'BtImport
        '
        Me.BtImport.Image = Global.TAS_TOL.My.Resources.Resources.import
        Me.BtImport.Location = New System.Drawing.Point(1115, 21)
        Me.BtImport.Name = "BtImport"
        Me.BtImport.Size = New System.Drawing.Size(109, 42)
        Me.BtImport.TabIndex = 9
        Me.BtImport.Text = "&Import"
        Me.BtImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtImport.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(1119, 65)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(0, 16)
        Me.Label4.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(581, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "End Date :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Sales ORG :"
        '
        'CbShipper
        '
        Me.CbShipper.DataSource = Me.TSHIPPERBindingSource
        Me.CbShipper.DisplayMember = "SP_Code"
        Me.CbShipper.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CbShipper.FormattingEnabled = True
        Me.CbShipper.Location = New System.Drawing.Point(110, 28)
        Me.CbShipper.Name = "CbShipper"
        Me.CbShipper.Size = New System.Drawing.Size(85, 28)
        Me.CbShipper.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(970, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 27)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&Get Data"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'lvRefresh
        '
        Me.lvRefresh.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvRefresh.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvRefresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvRefresh.FullRowSelect = True
        Me.lvRefresh.GridLines = True
        Me.lvRefresh.Location = New System.Drawing.Point(-4, 105)
        Me.lvRefresh.MultiSelect = False
        Me.lvRefresh.Name = "lvRefresh"
        Me.lvRefresh.ShowItemToolTips = True
        Me.lvRefresh.Size = New System.Drawing.Size(1373, 370)
        Me.lvRefresh.TabIndex = 11
        Me.lvRefresh.UseCompatibleStateImageBehavior = False
        Me.lvRefresh.View = System.Windows.Forms.View.Details
        '
        'FSAP02
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BtCancel
        Me.ClientSize = New System.Drawing.Size(1372, 477)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lvRefresh)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FSAP02"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "SAP02 DATA"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtGetdata As System.Windows.Forms.Button
    Friend WithEvents DpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CbShipper As System.Windows.Forms.ComboBox
    Friend WithEvents BtCancel As System.Windows.Forms.Button
    Friend WithEvents BtImport As System.Windows.Forms.Button
    Friend WithEvents LbShipcode As System.Windows.Forms.Label
    Friend WithEvents BtPost As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lvRefresh As System.Windows.Forms.ListView

End Class
