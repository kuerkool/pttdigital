﻿Imports System.Net
Imports TAS_TOL.SAP02
Imports TAS_TOL.SAP03
Imports TAS_TOL.FSAP03
Imports Telerik.WinControls.UI
Imports System.Data.SqlClient

Public Class FSAP02_3
    Public Shared SAP_ITEM As DO_ITEM
    Public Shared SAP_ALLITEM() As DO_ITEM
    Public Shared SAP_BATCH_ITEM() As BATCH_ITEM
    Private DODATA() As Extract_DO_DO02_DTITEM
    Private SHIndex As Int16
    Private DoTable, DoTable2 As New DataTable
    Private SALEORGFilter, PLANTFilter As String
    Private Const SC_CLOSE As Integer = &HF060
    Private Const MF_GRAYED As Integer = &H1

    Private Declare Function GetSystemMenu Lib "user32" _
       (ByVal hwnd As Long, _
        ByVal bRevert As Long) As Long

    Private Declare Function RemoveMenu Lib "user32" _
        (ByVal hMenu As Long, _
         ByVal nPosition As Long, _
         ByVal wFlags As Long) As Long

    Private Const MF_BYPOSITION = &H400&



    Public Structure BATCH_ITEM
        Public BT_RETURN As Integer
        Public DO_NO As String ''''
        Public DO_ITEM As String
        Public PLANT As String
        Public STORAGE_LOC As String
        Public BATCH_NO As String ''
        Public QTY_DELV As String ''
        Public HIGHER_LEVEL_ITEM As String
        Public MAT_CODE As String
    End Structure

    Public Structure DO_ITEM
        Public BT_RETURN As Integer
        Public dO_NO As String

        Public dO_ITEM As String

        Public sO_NO As String

        Public sO_ITEM As String

        Public dO_CDATE As String

        Public dO_CTIME As String

        Public sD_DOC_CAT As String

        Public sALES_ORG As String

        Public dIS_CHAN As String

        Public dIVISION As String

        Public sHIP_POINT As String

        Public sHIP_TYPE As String

        Public sHIP_TYPE_DESC As String

        Public dOC_DATE As String

        Public pLAN_GI_DATE As String

        Public lOAD_DATE As String

        Public lOAD_TIME As String

        Public tRN_PLAN_DATE As String

        Public dELV_DATE As String

        Public pICK_DATE As String

        Public sHIP_TO_PARTY As String

        Public sOLD_TO_PARTY As String

        Public tOTAL_WEIGHT As String

        Public mAT_CODE As String

        Public mAT_DES As String

        Public pLANT As String

        Public sTORAGE_LOC As String

        Public bATCH_NO As String

        Public aCT_QTY_DELV_SU As String

        Public bASE_UOM As String

        Public sALES_UNIT As String

        Public nET_WEIGHT As String

        Public gROSS_WEIGHT As String

        Public wEIGHT_UNIT As String

        Public vOLUME As String

        Public vOLUME_UNIT As String

        Public oVR_DELV_TLIMIT As String

        Public uND_DELV_TLIMIT As String

        Public hIGHER_LEVEL_ITEM As String

        Public bATCH_INDICATOR As String

        Public dELIVERY_INSTRUCTION_TEXT As String

        Public pUR_ORDER_TYPE As String

        Public cUST_PO_NUMBER As String

        Public lOADING_POINT As String

        Public oVERALL_STAT As String

        Public sTAT_WAREHOUSE As String

    End Structure

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Private Sub AddTableColumns(ByRef GridView As RadGridView)

        If lvRefresh.Columns.Count > 0 Then Exit Sub
        DoTable.Columns.Add("Delivery order")
        DoTable.Columns.Add("D/O")
        DoTable.Columns.Add("Delivery item")
        DoTable.Columns.Add("Sales order No.")
        DoTable.Columns.Add("Sales order Item.")
        DoTable.Columns.Add("D/O Create Date")
        DoTable.Columns.Add("D/O Create Time")
        DoTable.Columns.Add("SD document category")
        DoTable.Columns.Add("Sales organization")
        DoTable.Columns.Add("Distribution channel")
        DoTable.Columns.Add("Division")
        DoTable.Columns.Add("Shipping Point")
        DoTable.Columns.Add("Shipping Type")
        DoTable.Columns.Add("Shipping Type desc")
        DoTable.Columns.Add("Document date")
        DoTable.Columns.Add("Planned goods issue date")
        DoTable.Columns.Add("Loading Date")
        DoTable.Columns.Add("Loading time")
        DoTable.Columns.Add("Transportation Planning Date")
        DoTable.Columns.Add("Delivery Date")
        DoTable.Columns.Add("Picking Date")
        DoTable.Columns.Add("Ship-to-Party")
        DoTable.Columns.Add("Sold-to-Party")
        DoTable.Columns.Add("Total Weight")
        DoTable.Columns.Add("Material code")
        DoTable.Columns.Add("Short text for sales order item")
        DoTable.Columns.Add("Plant")
        DoTable.Columns.Add("Storage Location")
        DoTable.Columns.Add("Batch Number")
        DoTable.Columns.Add("Actual quantity delivered (in sales units)")
        DoTable.Columns.Add("Base Unit of Measure")
        DoTable.Columns.Add("Sales unit")
        DoTable.Columns.Add("Net weight")
        DoTable.Columns.Add("Gross weight")
        DoTable.Columns.Add("Weight Unit")
        DoTable.Columns.Add("Volume")
        DoTable.Columns.Add("Volume unit")
        DoTable.Columns.Add("Over delivery Tolerance Limit")
        DoTable.Columns.Add("Under delivery Tolerance Limit")
        DoTable.Columns.Add("Higher-Level Item of Batch Split Item")
        DoTable.Columns.Add("Batch management requirement indicator")
        DoTable.Columns.Add("Delivery instruction text (Z016)")
        DoTable.Columns.Add("Purchase order type")
        DoTable.Columns.Add("Customer PO Number")
        DoTable.Columns.Add("loading point (Key for define actual printed location)")
        DoTable.Columns.Add("Status of warehouse management (Header)")
        DoTable.Columns.Add("Status of warehouse management(Item)")
        DoTable.Columns.Add("INDEX")

        GridView.DataSource = DoTable
        ' GridView.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
        For i = 0 To GridView.Columns.Count - 1
            GridView.Columns(i).Width = 120
            '   GridView.Columns(i).AutoSizeMode = BestFitColumnMode.HeaderCells
        Next
        DpStart.Value = Now
        DpEnd.Value = Now
    End Sub

    Private Sub AddTableItems(ByRef GridView As RadGridView, ByVal DODATA() As Extract_DO_DO02_DTITEM)
        Dim l As Int16 = 0
        Dim workRow As DataRow
        Dim i As Integer
        DoTable.Rows.Clear()
        For i = 0 To DODATA.Length - 1
            workRow = DoTable.NewRow()
            workRow(0) = DODATA(i).DO_NO
            workRow(1) = DODATA(i).DO_NO
            workRow(2) = DODATA(i).DO_ITEM
            workRow(3) = DODATA(i).SO_NO
            workRow(4) = DODATA(i).SO_ITEM
            workRow(5) = DODATA(i).DO_CDATE
            workRow(6) = DODATA(i).DO_CTIME
            workRow(7) = DODATA(i).SD_DOC_CAT
            workRow(8) = DODATA(i).SALES_ORG
            workRow(9) = DODATA(i).DIS_CHAN
            workRow(10) = DODATA(i).DIVISION
            workRow(11) = DODATA(i).SHIP_POINT
            workRow(12) = DODATA(i).SHIP_TYPE
            workRow(13) = DODATA(i).SHIP_TYPE_DESC
            workRow(14) = DODATA(i).DOC_DATE
            workRow(15) = DODATA(i).PLAN_GI_DATE
            workRow(16) = DODATA(i).LOAD_DATE
            workRow(17) = DODATA(i).LOAD_TIME
            workRow(18) = DODATA(i).TRN_PLAN_DATE
            workRow(19) = DODATA(i).DELV_DATE
            workRow(20) = DODATA(i).PICK_DATE
            workRow(21) = DODATA(i).SHIP_TO_PARTY
            workRow(22) = DODATA(i).SOLD_TO_PARTY
            workRow(23) = DODATA(i).TOTAL_WEIGHT
            workRow(24) = DODATA(i).MAT_CODE
            workRow(25) = DODATA(i).MAT_DES
            workRow(26) = DODATA(i).PLANT
            workRow(27) = DODATA(i).STORAGE_LOC
            workRow(28) = DODATA(i).BATCH_NO
            workRow(29) = DODATA(i).ACT_QTY_DELV_SU
            workRow(30) = DODATA(i).BASE_UOM
            workRow(31) = DODATA(i).SALES_UNIT
            workRow(32) = DODATA(i).NET_WEIGHT
            workRow(33) = DODATA(i).GROSS_WEIGHT
            workRow(34) = DODATA(i).WEIGHT_UNIT
            workRow(35) = DODATA(i).VOLUME
            workRow(36) = DODATA(i).VOLUME_UNIT
            workRow(37) = DODATA(i).OVR_DELV_TLIMIT
            workRow(38) = DODATA(i).UND_DELV_TLIMIT
            workRow(39) = DODATA(i).HIGHER_LEVEL_ITEM
            workRow(40) = DODATA(i).BATCH_INDICATOR
            workRow(41) = DODATA(i).DELIVERY_INSTRUCTION_TEXT
            workRow(42) = DODATA(i).PUR_ORDER_TYPE
            workRow(43) = DODATA(i).CUST_PO_NUMBER
            workRow(44) = DODATA(i).LOADING_POINT
            workRow(45) = DODATA(i).OVERALL_STAT
            workRow(46) = DODATA(i).STAT_WAREHOUSE


            workRow(47) = i
            DoTable.Rows.Add(workRow)
        Next


        TSTOBindingSource_PositionChanged(Nothing, Nothing)

    End Sub

    Private Sub FSAP02_3_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        lvRefresh.FilterDescriptors.Clear()
        SHIndex = CBSalesORG.SelectedIndex
    End Sub

    Private Sub RadForm3_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_SETTING' table. You can move, or remove it, as needed.
        Me.T_SETTINGTableAdapter.Fill(Me.FPTDataSet.T_SETTING)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO1' table. You can move, or remove it, as needed.
        Me.T_STO1TableAdapter.Fill(Me.FPTDataSet.T_STO1)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO' table. You can move, or remove it, as needed.
        Me.T_STOTableAdapter.Fill(Me.FPTDataSet.T_STO)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
        Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        AddTableColumns(lvRefresh)

  Dim _SALEORGFilter, _PLANTFilter As String

        Me.SALEORGFilter = "(SP_Code<>'" & TSETTINGBindingSource(0)("SALESORG") & "')"
        Me.PLANTFilter = "(PLANT<>'" & TSETTINGBindingSource(0)("PLANT") & "')"

        For i = 0 To TSETTINGPLANTBindingSource.Count - 1
            If _SALEORGFilter = "" Then
                _SALEORGFilter &= "('" & TSETTINGPLANTBindingSource(i)("SALESORG") & "'"
            Else
                _SALEORGFilter &= ", '" & TSETTINGPLANTBindingSource(i)("SALESORG") & "'"
            End If

            If _PLANTFilter = "" Then
                _PLANTFilter += "('" & TSETTINGPLANTBindingSource(i)("PLANT") & "'"
            Else
                _PLANTFilter += ", '" & TSETTINGPLANTBindingSource(i)("PLANT") & "'"
            End If

        Next
        _SALEORGFilter &= ")"
        _PLANTFilter &= ")"
        Me.SALEORGFilter &= " and (SP_Code in" & _SALEORGFilter & ")"
        Me.PLANTFilter &= " and (PLANT in" & _PLANTFilter & ")"
        TSHIPPERBindingSource.Filter = Me.SALEORGFilter
        TSTOBindingSource.Filter = Me.PLANTFilter
        CBPlant.SelectedIndex = 0
        TSHIPPERBindingSource_PositionChanged(sender, e)
    End Sub

    Private Sub RadButton1_Click(sender As System.Object, e As System.EventArgs) Handles BtGetdata.Click
        Dim Dateformat As String
        Try
            RadWaitingBar1.Visible = True
            RadWaitingBar1.StartWaiting()
            Dim SqlDataAdapter1 As SqlDataAdapter
            Dim SqlConnection1 As New SqlConnection(My.Settings.FPTConnectionString)
            Dim cmd As New SqlCommand
            Dim ds As New DataSet
            cmd.Connection = SqlConnection1
            cmd.CommandText = "select * from DateFormat"
            SqlDataAdapter1 = New SqlDataAdapter(cmd)
            SqlDataAdapter1.Fill(ds, "DateFormat")
            Dateformat = ds.Tables("DateFormat").Rows(0)("ITAS02ImportDate").ToString
            SqlConnection1.Close()
            SqlConnection1.Dispose()
            SqlDataAdapter1.Dispose()
            cmd.Dispose()
            ds.Dispose()
        Catch ex As Exception
            Dateformat = "{0:yyyy-M-d}"

        End Try


        Try

            Cursor = Cursors.WaitCursor
            Dim MRequest As New SAP02.Extract_DO_DT
            MRequest.SORG = TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString
            MRequest.CRDATE_S = String.Format(Dateformat, DpStart.Value.Date)
            MRequest.CRDATE_E = String.Format(Dateformat, DpEnd.Value.Date)
            'MRequest.CRDATE_S = DpStart.Value.Date.Day & DateFirstseparator & DpStart.Value.Date.Month & DateSecondseparator & DpStart.Value.Date.Year
            'MRequest.CRDATE_E = DpEnd.Value.Date.Day & DateFirstseparator & DpEnd.Value.Date.Month & DateSecondseparator & DpEnd.Value.Date.Year
            Dim myproxy As New Extract_DO_Sync_Out_SIService
            Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
            Dim netCred As NetworkCredential = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            Dim uri As Uri = New Uri(objIniFile.GetString("Webservice", "URL02", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING02"))

            ' Dim netCred As NetworkCredential = New NetworkCredential("TAS01", "initial1")
            ' Dim uri As Uri = New Uri("http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING02")
            Dim credentials As ICredentials = netCred.GetCredential(uri, "Basic")
            myproxy.Url = uri.AbsoluteUri
            myproxy.Credentials = credentials
            myproxy.PreAuthenticate = True
            ' myproxy.Credentials = credentials
            AddHandler myproxy.Extract_DO_Sync_Out_SICompleted, AddressOf GetDO
            '   TSLWS.Enabled = False
            myproxy.Extract_DO_Sync_Out_SIAsync(MRequest)
        Catch ex As Exception
            RadWaitingBar1.StopWaiting()
            RadWaitingBar1.Visible = False
            Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub GetDO(ByVal sender As Object, ByVal e As Extract_DO_Sync_Out_SICompletedEventArgs)
        Try
            If (Not e.Cancelled) Then
                Dim MResponse As New SAP02.Extract_DO_Resp_DT
                Dim aa As New SAP02.Extract_DO_DO02_DTITEM
                '  aa.PLANT = "1004"
                MResponse = e.Result
                DODATA = MResponse.DO02

                ' DODATA.Where(DODATA.Se, True)
                AddTableItems(lvRefresh, DODATA)
                ' AddItems(lvRefresh, DODATA)


                RadGroupBox1.Text = "Last Get " + CBSalesORG.Text + " DO from SAP on " + Now.ToString
                RadWaitingBar1.StopWaiting()
                RadWaitingBar1.Visible = False

                Cursor = Cursors.Default

            End If

        Catch ex As Exception
            MsgBox(ex.Message + Chr(13) + e.Error.Message, MsgBoxStyle.Critical, "Error message")
            RadWaitingBar1.StopWaiting()
            RadWaitingBar1.Visible = False
            Cursor = Cursors.Default


        End Try
    End Sub



    Private Sub TSHIPPERBindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSHIPPERBindingSource.PositionChanged

        Try

            TSTOBindingSource.Filter = Me.PLANTFilter & " and ( SALEORG='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "')"
            'TSTOBindingSource.Position = 0
            CBPlant.SelectedIndex = 0
            TSTOBindingSource_PositionChanged(sender, e)
        Catch ex As Exception

        End Try

        'Try
        '    TSTOBindingSource.Filter = "SALEORG='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "'"

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub TSTOBindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSTOBindingSource.PositionChanged
        DoTable2.Clear()
        Try
            TLOBindingSource.Filter = "PLANT_LOCATION='" & TSTOBindingSource(TSTOBindingSource.Position)("PLANT").ToString & "'"

            TSTOBindingSource.Item(TSTOBindingSource.Position)("PLANT").ToString()
            DoTable2 = DoTable.Clone
            'DoTable.Select()
            Dim dr2 As DataRow() = DoTable.Select("ShippingPoint='" &
                                                   TSTOBindingSource.Item(TSTOBindingSource.Position)("PLANT").ToString() & "'", "")
            For Each row As DataRow In dr2
                DoTable2.ImportRow(row)
            Next
            lvRefresh.DataSource = DoTable2
        Catch ex As Exception
        End Try
        ' '' Dim DoTable2 As DataTable = DoTable.Clone
        ''DoTable2.Clear()
        ''Try
        ''    'TLOBindingSource.Filter = "PLANT_LOCATION='" & TSTOBindingSource(TSTOBindingSource.Position)("PLANT").ToString & "'"

        ''    ' TSTOBindingSource.Item(TSTOBindingSource.Position)("PLANT").ToString()
        ''    DoTable2 = DoTable.Clone
        ''    'DoTable.Select()
        ''    Dim dr2 As DataRow() = DoTable.Select("Plant='" &
        ''                                           TSTOBindingSource(TSTOBindingSource.Position)("PLANT").ToString & "'", "")
        ''    For Each row As DataRow In dr2
        ''        DoTable2.ImportRow(row)
        ''    Next
        ''    lvRefresh.DataSource = DoTable2
        ''Catch ex As Exception
        ''End Try
    End Sub
    Public Function DisableCloseButton(ByVal frm As Form) As Boolean

        'PURPOSE: Removes X button from a form
        'EXAMPLE: DisableCloseButton Me
        'RETURNS: True if successful, false otherwise
        'NOTES:   Also removes Exit Item from
        '         Control Box Menu

        Dim lHndSysMenu As Long
        Dim lAns1 As Long, lAns2 As Long


        lHndSysMenu = GetSystemMenu(frm.Handle, 0)

        'remove close button
        lAns1 = RemoveMenu(lHndSysMenu, 6, MF_BYPOSITION)

        'Remove seperator bar
        lAns2 = RemoveMenu(lHndSysMenu, 5, MF_BYPOSITION)

        'Return True if both calls were successful
        DisableCloseButton = (lAns1 <> 0 And lAns2 <> 0)

    End Function

    Private Sub BtImport_Click(sender As System.Object, e As System.EventArgs) Handles BtImport.Click
        Try
            '  Check DO NO DO ITEM


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()
            Dim q As String
            Dim MsgString As String
            Dim INDEX As Integer
            ' DoTable2.Rows.
            ' MessageBox.Show(lvRefre)
            ' MsgBox(lvRefresh.Groups(lvRefresh.CurrentRow.Group.HeaderRow.Index).Item(0).Cells("INDEX").Value)
            ' MsgBox(lvRefresh.CurrentRow.Group.Item(0).Cells(0).Value)
            ' lvRefresh.Groups(lvRefresh.CurrentRow.Group.HeaderRow.Index)
            'MsgBox(lvRefresh.CurrentRow.Cells("INDEX").Value)
            ' MsgBox(lvRefresh.Groups(lvRefresh.CurrentRow.Group.HeaderRow.Index).)
            'MsgBox(lvRefresh.CurrentCellAddress.ToString)
            INDEX = lvRefresh.Groups(lvRefresh.CurrentRow.Group.HeaderRow.Index).Item(0).Cells("INDEX").Value
            MsgString = "Delivery order: " & DODATA(INDEX).DO_NO & " D/O Item: " & DODATA(INDEX).HIGHER_LEVEL_ITEM _
                                     & " มีขอมูลอยู่ในระบบ TAS แล้ว !"
            q = "select LOAD_ID from V_LOADINGNOTE WHERE do_NO='" & DODATA(INDEX).DO_NO & "' and do_item='" & DODATA(INDEX).HIGHER_LEVEL_ITEM & "'"
            If DODATA(INDEX).HIGHER_LEVEL_ITEM = 0 Then
                q = "select LOAD_ID from V_LOADINGNOTE WHERE do_NO='" & DODATA(INDEX).DO_NO & "' and do_item='" & DODATA(INDEX).DO_ITEM & "'"
                MsgString = "Delivery order: " & DODATA(INDEX).DO_NO & " D/O Item: " & DODATA(INDEX).DO_ITEM _
                                    & " มีขอมูลอยู่ในระบบ TAS แล้ว !"
            End If



            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim dt As New DataTable
            da.Fill(dt)

            If dt.Rows.Count > 0 And f05_01_Advisenote.AddNew = 1 Then
                MessageBox.Show(MsgString, "Error")
                Exit Sub
            ElseIf dt.Rows.Count > 0 Then
                If MsgBox(MsgString, vbYesNo + vbDefaultButton2, "Confirmation") = vbNo Then
                    Exit Sub
                End If
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'test()

            SAP_BATCH_ITEM = Nothing
            SAP_ALLITEM = Nothing
            SAP_ITEM = Nothing
            Dim IndexDO As Integer = 0
            Dim IndexBatch As Integer = 0
            For i = 0 To DODATA.Length - 1
                If (DODATA(INDEX).DO_NO = DODATA(i).DO_NO) And
                (DODATA(i).HIGHER_LEVEL_ITEM = 0) Then
                    ReDim Preserve SAP_ALLITEM(IndexDO)
                    SAP_ALLITEM(IndexDO).BT_RETURN = 1
                    SAP_ALLITEM(IndexDO).dO_NO = DODATA(i).DO_NO
                    SAP_ALLITEM(IndexDO).dO_ITEM = DODATA(i).DO_ITEM
                    SAP_ALLITEM(IndexDO).sO_NO = DODATA(i).SO_NO
                    SAP_ALLITEM(IndexDO).sO_ITEM = DODATA(i).SO_ITEM
                    SAP_ALLITEM(IndexDO).dO_CDATE = DODATA(i).DO_CDATE
                    SAP_ALLITEM(IndexDO).dO_CTIME = DODATA(i).DO_CTIME
                    SAP_ALLITEM(IndexDO).sD_DOC_CAT = DODATA(i).SD_DOC_CAT
                    SAP_ALLITEM(IndexDO).sALES_ORG = DODATA(i).SALES_ORG
                    SAP_ALLITEM(IndexDO).dIS_CHAN = DODATA(i).DIS_CHAN
                    SAP_ALLITEM(IndexDO).dIVISION = DODATA(i).DIVISION
                    SAP_ALLITEM(IndexDO).sHIP_POINT = DODATA(i).SHIP_POINT
                    SAP_ALLITEM(IndexDO).sHIP_TYPE = DODATA(i).SHIP_TYPE
                    SAP_ALLITEM(IndexDO).sHIP_TYPE_DESC = DODATA(i).SHIP_TYPE_DESC
                    SAP_ALLITEM(IndexDO).dOC_DATE = DODATA(i).DOC_DATE
                    SAP_ALLITEM(IndexDO).pLAN_GI_DATE = DODATA(i).PLAN_GI_DATE
                    SAP_ALLITEM(IndexDO).lOAD_DATE = DODATA(i).LOAD_DATE
                    SAP_ALLITEM(IndexDO).lOAD_TIME = DODATA(i).LOAD_TIME
                    SAP_ALLITEM(IndexDO).tRN_PLAN_DATE = DODATA(i).TRN_PLAN_DATE
                    SAP_ALLITEM(IndexDO).dELV_DATE = DODATA(i).DELV_DATE
                    SAP_ALLITEM(IndexDO).pICK_DATE = DODATA(i).PICK_DATE
                    SAP_ALLITEM(IndexDO).sHIP_TO_PARTY = DODATA(i).SHIP_TO_PARTY
                    SAP_ALLITEM(IndexDO).sOLD_TO_PARTY = DODATA(i).SOLD_TO_PARTY
                    SAP_ALLITEM(IndexDO).tOTAL_WEIGHT = DODATA(i).TOTAL_WEIGHT
                    SAP_ALLITEM(IndexDO).mAT_CODE = DODATA(i).MAT_CODE
                    SAP_ALLITEM(IndexDO).mAT_DES = DODATA(i).MAT_DES
                    SAP_ALLITEM(IndexDO).pLANT = DODATA(i).PLANT
                    SAP_ALLITEM(IndexDO).sTORAGE_LOC = DODATA(i).STORAGE_LOC
                    SAP_ALLITEM(IndexDO).bATCH_NO = DODATA(i).BATCH_NO
                    SAP_ALLITEM(IndexDO).aCT_QTY_DELV_SU = DODATA(i).ACT_QTY_DELV_SU
                    SAP_ALLITEM(IndexDO).bASE_UOM = DODATA(i).BASE_UOM
                    SAP_ALLITEM(IndexDO).sALES_UNIT = DODATA(i).SALES_UNIT
                    SAP_ALLITEM(IndexDO).nET_WEIGHT = DODATA(i).NET_WEIGHT
                    SAP_ALLITEM(IndexDO).gROSS_WEIGHT = DODATA(i).GROSS_WEIGHT
                    SAP_ALLITEM(IndexDO).wEIGHT_UNIT = DODATA(i).WEIGHT_UNIT
                    SAP_ALLITEM(IndexDO).vOLUME = DODATA(i).VOLUME
                    SAP_ALLITEM(IndexDO).vOLUME_UNIT = DODATA(i).VOLUME_UNIT
                    SAP_ALLITEM(IndexDO).oVR_DELV_TLIMIT = DODATA(i).OVR_DELV_TLIMIT
                    SAP_ALLITEM(IndexDO).uND_DELV_TLIMIT = DODATA(i).UND_DELV_TLIMIT
                    SAP_ALLITEM(IndexDO).hIGHER_LEVEL_ITEM = DODATA(i).HIGHER_LEVEL_ITEM
                    SAP_ALLITEM(IndexDO).bATCH_INDICATOR = DODATA(i).BATCH_INDICATOR
                    SAP_ALLITEM(IndexDO).dELIVERY_INSTRUCTION_TEXT = DODATA(i).DELIVERY_INSTRUCTION_TEXT
                    SAP_ALLITEM(IndexDO).pUR_ORDER_TYPE = DODATA(i).PUR_ORDER_TYPE
                    SAP_ALLITEM(IndexDO).cUST_PO_NUMBER = DODATA(i).CUST_PO_NUMBER
                    SAP_ALLITEM(IndexDO).lOADING_POINT = DODATA(i).LOADING_POINT
                    SAP_ALLITEM(IndexDO).OVERALL_STAT = DODATA(i).OVERALL_STAT
                    SAP_ALLITEM(IndexDO).sTAT_WAREHOUSE = DODATA(i).STAT_WAREHOUSE


                    Dim Batch_count As Int16 = 0
                    For l = 0 To DODATA.Length - 1
                        If (DODATA(i).DO_NO = DODATA(l).DO_NO) And
                            (DODATA(i).DO_ITEM = DODATA(l).HIGHER_LEVEL_ITEM) Then

                            Batch_count += 1
                        End If
                    Next
                    If (UCase(SAP_ALLITEM(IndexDO).bATCH_INDICATOR)) = "X" And (Batch_count = 0) Or
                    (UCase(SAP_ALLITEM(IndexDO).bATCH_INDICATOR)) = "X" And (SAP_ALLITEM(IndexDO).aCT_QTY_DELV_SU > 0) Then
                        ReDim Preserve SAP_BATCH_ITEM(IndexBatch)
                        SAP_BATCH_ITEM(IndexBatch).BT_RETURN = 1
                        SAP_BATCH_ITEM(IndexBatch).DO_NO = DODATA(i).DO_NO
                        SAP_BATCH_ITEM(IndexBatch).DO_ITEM = DODATA(i).DO_ITEM
                        SAP_BATCH_ITEM(IndexBatch).BATCH_NO = DODATA(i).BATCH_NO
                        SAP_BATCH_ITEM(IndexBatch).PLANT = DODATA(i).PLANT
                        SAP_BATCH_ITEM(IndexBatch).QTY_DELV = DODATA(i).ACT_QTY_DELV_SU
                        SAP_BATCH_ITEM(IndexBatch).STORAGE_LOC = DODATA(i).STORAGE_LOC
                        SAP_BATCH_ITEM(IndexBatch).HIGHER_LEVEL_ITEM = DODATA(i).DO_ITEM
                        SAP_BATCH_ITEM(IndexBatch).MAT_CODE = DODATA(i).MAT_CODE
                        IndexBatch += 1
                    End If

                    IndexDO += 1
                ElseIf (DODATA(INDEX).DO_NO = DODATA(i).DO_NO) And
                       (DODATA(i).HIGHER_LEVEL_ITEM <> 0) Then
                    ReDim Preserve SAP_BATCH_ITEM(IndexBatch)
                    SAP_BATCH_ITEM(IndexBatch).BT_RETURN = 1
                    SAP_BATCH_ITEM(IndexBatch).DO_NO = DODATA(i).DO_NO
                    SAP_BATCH_ITEM(IndexBatch).DO_ITEM = DODATA(i).HIGHER_LEVEL_ITEM
                    SAP_BATCH_ITEM(IndexBatch).BATCH_NO = DODATA(i).BATCH_NO
                    SAP_BATCH_ITEM(IndexBatch).PLANT = DODATA(i).PLANT
                    SAP_BATCH_ITEM(IndexBatch).QTY_DELV = DODATA(i).ACT_QTY_DELV_SU
                    SAP_BATCH_ITEM(IndexBatch).STORAGE_LOC = DODATA(i).STORAGE_LOC
                    SAP_BATCH_ITEM(IndexBatch).HIGHER_LEVEL_ITEM = DODATA(i).HIGHER_LEVEL_ITEM
                    Dim Mat_code As String
                    For l = 0 To DODATA.Length - 1
                        If (DODATA(l).DO_NO = DODATA(i).DO_NO) And (DODATA(l).DO_ITEM = DODATA(i).HIGHER_LEVEL_ITEM) Then
                            Mat_code = DODATA(l).MAT_CODE
                            Exit For
                        End If
                    Next l
                    SAP_BATCH_ITEM(IndexBatch).MAT_CODE = Mat_code
                    IndexBatch += 1
                End If
            Next

            If Int(DODATA(INDEX).HIGHER_LEVEL_ITEM) = 0 Then
                SAP_ITEM.BT_RETURN = 1
                SAP_ITEM.dO_NO = DODATA(INDEX).DO_NO
                SAP_ITEM.dO_ITEM = DODATA(INDEX).DO_ITEM
                SAP_ITEM.sO_NO = DODATA(INDEX).SO_NO
                SAP_ITEM.sO_ITEM = DODATA(INDEX).SO_ITEM
                SAP_ITEM.dO_CDATE = DODATA(INDEX).DO_CDATE
                SAP_ITEM.dO_CTIME = DODATA(INDEX).DO_CTIME
                SAP_ITEM.sD_DOC_CAT = DODATA(INDEX).SD_DOC_CAT
                SAP_ITEM.sALES_ORG = DODATA(INDEX).SALES_ORG
                SAP_ITEM.dIS_CHAN = DODATA(INDEX).DIS_CHAN
                SAP_ITEM.dIVISION = DODATA(INDEX).DIVISION
                SAP_ITEM.sHIP_POINT = DODATA(INDEX).SHIP_POINT
                SAP_ITEM.sHIP_TYPE = DODATA(INDEX).SHIP_TYPE
                SAP_ITEM.sHIP_TYPE_DESC = DODATA(INDEX).SHIP_TYPE_DESC
                SAP_ITEM.dOC_DATE = DODATA(INDEX).DOC_DATE
                SAP_ITEM.pLAN_GI_DATE = DODATA(INDEX).PLAN_GI_DATE
                SAP_ITEM.lOAD_DATE = DODATA(INDEX).LOAD_DATE
                SAP_ITEM.lOAD_TIME = DODATA(INDEX).LOAD_TIME
                SAP_ITEM.tRN_PLAN_DATE = DODATA(INDEX).TRN_PLAN_DATE
                SAP_ITEM.dELV_DATE = DODATA(INDEX).DELV_DATE
                SAP_ITEM.pICK_DATE = DODATA(INDEX).PICK_DATE
                SAP_ITEM.sHIP_TO_PARTY = DODATA(INDEX).SHIP_TO_PARTY
                SAP_ITEM.sOLD_TO_PARTY = DODATA(INDEX).SOLD_TO_PARTY
                SAP_ITEM.tOTAL_WEIGHT = DODATA(INDEX).TOTAL_WEIGHT
                SAP_ITEM.mAT_CODE = DODATA(INDEX).MAT_CODE
                SAP_ITEM.mAT_DES = DODATA(INDEX).MAT_DES
                SAP_ITEM.pLANT = DODATA(INDEX).PLANT
                SAP_ITEM.sTORAGE_LOC = DODATA(INDEX).STORAGE_LOC
                SAP_ITEM.bATCH_NO = DODATA(INDEX).BATCH_NO
                SAP_ITEM.aCT_QTY_DELV_SU = DODATA(INDEX).ACT_QTY_DELV_SU
                SAP_ITEM.bASE_UOM = DODATA(INDEX).BASE_UOM
                SAP_ITEM.sALES_UNIT = DODATA(INDEX).SALES_UNIT
                SAP_ITEM.nET_WEIGHT = DODATA(INDEX).NET_WEIGHT
                SAP_ITEM.gROSS_WEIGHT = DODATA(INDEX).GROSS_WEIGHT
                SAP_ITEM.wEIGHT_UNIT = DODATA(INDEX).WEIGHT_UNIT
                SAP_ITEM.vOLUME = DODATA(INDEX).VOLUME
                SAP_ITEM.vOLUME_UNIT = DODATA(INDEX).VOLUME_UNIT
                SAP_ITEM.oVR_DELV_TLIMIT = DODATA(INDEX).OVR_DELV_TLIMIT
                SAP_ITEM.uND_DELV_TLIMIT = DODATA(INDEX).UND_DELV_TLIMIT
                SAP_ITEM.hIGHER_LEVEL_ITEM = DODATA(INDEX).HIGHER_LEVEL_ITEM
                SAP_ITEM.bATCH_INDICATOR = DODATA(INDEX).BATCH_INDICATOR
                SAP_ITEM.dELIVERY_INSTRUCTION_TEXT = DODATA(INDEX).DELIVERY_INSTRUCTION_TEXT
                SAP_ITEM.pUR_ORDER_TYPE = DODATA(INDEX).PUR_ORDER_TYPE
                SAP_ITEM.cUST_PO_NUMBER = DODATA(INDEX).CUST_PO_NUMBER
                SAP_ITEM.lOADING_POINT = DODATA(INDEX).LOADING_POINT
                SAP_ITEM.OVERALL_STAT = DODATA(INDEX).OVERALL_STAT
                SAP_ITEM.sTAT_WAREHOUSE = DODATA(INDEX).STAT_WAREHOUSE
                If SAP_ITEM.oVERALL_STAT <> "" And (SAP_ITEM.oVERALL_STAT <> "B" And SAP_ITEM.oVERALL_STAT <> "C") Then
                    MsgBox("ข้อมูลการสร้าง D/O ยังไม่สมบรูณ์ " + Chr(13) + "กรุณาตรวจสอบ WM STATUS บนระบบ SAP", vbOKOnly, "Confirmation")
                    Exit Sub

                End If

            Else
                IndexDO = 0
                For i = 0 To DODATA.Length - 1
                    If DoTable2.Rows(INDEX)("Delivery order") And
                    (DODATA(i).DO_ITEM = DODATA(INDEX).HIGHER_LEVEL_ITEM) Then
                        IndexDO = i
                        Exit For
                    End If
                Next

                SAP_ITEM.BT_RETURN = 1
                SAP_ITEM.dO_NO = DODATA(IndexDO).DO_NO
                SAP_ITEM.dO_ITEM = DODATA(IndexDO).DO_ITEM
                SAP_ITEM.sO_NO = DODATA(IndexDO).SO_NO
                SAP_ITEM.sO_ITEM = DODATA(IndexDO).SO_ITEM
                SAP_ITEM.dO_CDATE = DODATA(IndexDO).DO_CDATE
                SAP_ITEM.dO_CTIME = DODATA(IndexDO).DO_CTIME
                SAP_ITEM.sD_DOC_CAT = DODATA(IndexDO).SD_DOC_CAT
                SAP_ITEM.sALES_ORG = DODATA(IndexDO).SALES_ORG
                SAP_ITEM.dIS_CHAN = DODATA(IndexDO).DIS_CHAN
                SAP_ITEM.dIVISION = DODATA(IndexDO).DIVISION
                SAP_ITEM.sHIP_POINT = DODATA(IndexDO).SHIP_POINT
                SAP_ITEM.sHIP_TYPE = DODATA(IndexDO).SHIP_TYPE
                SAP_ITEM.sHIP_TYPE_DESC = DODATA(IndexDO).SHIP_TYPE_DESC
                SAP_ITEM.dOC_DATE = DODATA(IndexDO).DOC_DATE
                SAP_ITEM.pLAN_GI_DATE = DODATA(IndexDO).PLAN_GI_DATE
                SAP_ITEM.lOAD_DATE = DODATA(IndexDO).LOAD_DATE
                SAP_ITEM.lOAD_TIME = DODATA(IndexDO).LOAD_TIME
                SAP_ITEM.tRN_PLAN_DATE = DODATA(IndexDO).TRN_PLAN_DATE
                SAP_ITEM.dELV_DATE = DODATA(IndexDO).DELV_DATE
                SAP_ITEM.pICK_DATE = DODATA(IndexDO).PICK_DATE
                SAP_ITEM.sHIP_TO_PARTY = DODATA(IndexDO).SHIP_TO_PARTY
                SAP_ITEM.sOLD_TO_PARTY = DODATA(IndexDO).SOLD_TO_PARTY
                SAP_ITEM.tOTAL_WEIGHT = DODATA(IndexDO).TOTAL_WEIGHT
                SAP_ITEM.mAT_CODE = DODATA(IndexDO).MAT_CODE
                SAP_ITEM.mAT_DES = DODATA(IndexDO).MAT_DES
                SAP_ITEM.pLANT = DODATA(IndexDO).PLANT
                SAP_ITEM.sTORAGE_LOC = DODATA(IndexDO).STORAGE_LOC
                SAP_ITEM.bATCH_NO = DODATA(IndexDO).BATCH_NO
                SAP_ITEM.aCT_QTY_DELV_SU = DODATA(IndexDO).ACT_QTY_DELV_SU
                SAP_ITEM.bASE_UOM = DODATA(IndexDO).BASE_UOM
                SAP_ITEM.sALES_UNIT = DODATA(IndexDO).SALES_UNIT
                SAP_ITEM.nET_WEIGHT = DODATA(IndexDO).NET_WEIGHT
                SAP_ITEM.gROSS_WEIGHT = DODATA(IndexDO).GROSS_WEIGHT
                SAP_ITEM.wEIGHT_UNIT = DODATA(IndexDO).WEIGHT_UNIT
                SAP_ITEM.vOLUME = DODATA(IndexDO).VOLUME
                SAP_ITEM.vOLUME_UNIT = DODATA(IndexDO).VOLUME_UNIT
                SAP_ITEM.oVR_DELV_TLIMIT = DODATA(IndexDO).OVR_DELV_TLIMIT
                SAP_ITEM.uND_DELV_TLIMIT = DODATA(IndexDO).UND_DELV_TLIMIT
                SAP_ITEM.hIGHER_LEVEL_ITEM = DODATA(IndexDO).HIGHER_LEVEL_ITEM
                SAP_ITEM.bATCH_INDICATOR = DODATA(IndexDO).BATCH_INDICATOR
                SAP_ITEM.dELIVERY_INSTRUCTION_TEXT = DODATA(IndexDO).DELIVERY_INSTRUCTION_TEXT
                SAP_ITEM.pUR_ORDER_TYPE = DODATA(IndexDO).PUR_ORDER_TYPE
                SAP_ITEM.cUST_PO_NUMBER = DODATA(IndexDO).CUST_PO_NUMBER
                SAP_ITEM.lOADING_POINT = DODATA(IndexDO).LOADING_POINT
                SAP_ITEM.oVERALL_STAT = DODATA(IndexDO).OVERALL_STAT
                SAP_ITEM.sTAT_WAREHOUSE = DODATA(IndexDO).STAT_WAREHOUSE
                If SAP_ITEM.oVERALL_STAT <> "" And (SAP_ITEM.oVERALL_STAT <> "B" And SAP_ITEM.oVERALL_STAT <> "C") Then
                    MsgBox("ข้อมูลการสร้าง D/O ยังไม่สมบรูณ์ " + Chr(13) + "กรุณาตรวจสอบ WM STATUS บนระบบ SAP", vbOKOnly, "Confirmation")
                    Exit Sub

                End If


            End If


            For i = 0 To SAP_ALLITEM.Length - 1
                If SAP_ALLITEM(i).aCT_QTY_DELV_SU = 0 Then
                    For l = 0 To SAP_BATCH_ITEM.Length - 1
                        If SAP_BATCH_ITEM(l).HIGHER_LEVEL_ITEM = SAP_ALLITEM(i).dO_ITEM Then
                            SAP_ALLITEM(i).aCT_QTY_DELV_SU += Convert.ToDouble(SAP_BATCH_ITEM(l).QTY_DELV)
                        End If
                    Next
                End If
            Next



            'lvRefresh.SelectedIndices.Clear()
            ' Label4.Text = SAP_ITEM.dO_NO


        Catch ex As Exception
            SAP_ITEM.BT_RETURN = 0
            'If lvRefresh.SelectedIndices.Count <= 0 Then
            '    MsgBox("Please select one record", MsgBoxStyle.Critical, "Error")
            'Else
            '    MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
            'End If
            Exit Sub
            'For delete
            ' SAP_ITEM.BT_RETURN = 1
            '  SAP_ITEM.DO_NO = "123"
            '  SAP_ITEM.BATCH_NO = "444"
            '  SAP_ITEM.DIVISION = "D1"
            ' MessageBox.Show(ex.Message, )
        End Try
        Close()
    End Sub

    Private Sub BtCancel_Click(sender As System.Object, e As System.EventArgs) Handles BtCancel.Click
        Try
            SAP_ITEM = Nothing
            SAP_ALLITEM = Nothing
            SAP_BATCH_ITEM = Nothing
            SAP_ITEM.BT_RETURN = 0
            For i = 0 To SAP_ALLITEM.Length - 1
                SAP_ALLITEM(i).BT_RETURN = 0
            Next
            'lvRefresh.SelectedIndices.Clear()
        Catch ex As Exception

        End Try
        Close()
    End Sub

    Private Sub FSAP02_3_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        CBSalesORG.SelectedIndex = SHIndex
        CBSalesORG.Focus()
    End Sub

    Private Sub lvRefresh_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvRefresh.MouseDoubleClick
        BtImport.PerformClick()
    End Sub
End Class
