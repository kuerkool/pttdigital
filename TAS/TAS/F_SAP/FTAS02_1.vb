﻿Imports System.Data.SqlClient
Imports System.ComponentModel
Imports System.Threading
Imports System
Imports System.ServiceProcess
Public Class FTAS02_1
    Private SALEORGFilter, PLANTFilter As String

    Private Sub RadForm2_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_BATCH_LOT_TAS' table. You can move, or remove it, as needed.
        Me.T_BATCH_LOT_TASTableAdapter.Fill(Me.FPTDataSet.T_BATCH_LOT_TAS)
        'TODO: This line of code loads data into the 'FPTDataSet.T_DO1' table. You can move, or remove it, as needed.
        Me.T_DO1TableAdapter.Fill(Me.FPTDataSet.T_DO1)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADINGNOTECOMPARTMENT' table. You can move, or remove it, as needed.
        Me.T_LOADINGNOTECOMPARTMENTTableAdapter.Fill(Me.FPTDataSet.T_LOADINGNOTECOMPARTMENT)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADINGNOTE' table. You can move, or remove it, as needed.
        Me.T_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.T_LOADINGNOTE)
        'TODO: This line of code loads data into the 'FPTDataSet.T_BATCH_LOST_TAS' table. You can move, or remove it, as needed.
        'Me.T_BATCH_LOST_TASTableAdapter.Fill(Me.FPTDataSet.T_BATCH_LOST_TAS)
        'TODO: This line of code loads data into the 'FPTDataSet.T_DO' table. You can move, or remove it, as needed.
        'Me.T_DOTableAdapter.Fill(Me.FPTDataSet.T_DO)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADINGNOTE' table. You can move, or remove it, as needed.
        'Me.T_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.T_LOADINGNOTE)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADINGNOTECOMPARTMENT' table. You can move, or remove it, as needed.
        'Me.T_LOADINGNOTECOMPARTMENTTableAdapter.Fill(Me.FPTDataSet.T_LOADINGNOTECOMPARTMENT)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADINGNOTECOMPARTMENT' table. You can move, or remove it, as needed.
        ' Me.T_LOADINGNOTECOMPARTMENTTableAdapter.Fill(Me.FPTDataSet.T_LOADINGNOTECOMPARTMENT)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADINGNOTE' table. You can move, or remove it, as needed.
        '  Me.T_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.T_LOADINGNOTE)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO1' table. You can move, or remove it, as needed.
        Me.T_STO1TableAdapter.Fill(Me.FPTDataSet.T_STO1)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SETTING' table. You can move, or remove it, as needed.
        Me.T_SETTINGTableAdapter.Fill(Me.FPTDataSet.T_SETTING)
        'TODO: This line of code loads data into the 'FPTDataSet.TSETTINGPLANT' table. You can move, or remove it, as needed.
        Me.TSETTINGPLANTTableAdapter.Fill(Me.FPTDataSet.TSETTINGPLANT)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO' table. You can move, or remove it, as needed.
        Me.T_STOTableAdapter.Fill(Me.FPTDataSet.T_STO)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
        Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
        'TODO: This line of code loads data into the 'FPTDataSet.V_LOADINGNOTE' table. You can move, or remove it, as needed.
        ' Me.V_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_LOADINGNOTE)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)

        Dim _SALEORGFilter, _PLANTFilter As String

        Me.SALEORGFilter = "(SP_Code<>'" & TSETTINGBindingSource(0)("SALESORG") & "')"
        Me.PLANTFilter = "(PLANT<>'" & TSETTINGBindingSource(0)("PLANT") & "')"

        For i = 0 To TSETTINGPLANTBindingSource.Count - 1
            If _SALEORGFilter = "" Then
                _SALEORGFilter &= "('" & TSETTINGPLANTBindingSource(i)("SALESORG") & "'"
            Else
                _SALEORGFilter &= ", '" & TSETTINGPLANTBindingSource(i)("SALESORG") & "'"
            End If

            If _PLANTFilter = "" Then
                _PLANTFilter += "('" & TSETTINGPLANTBindingSource(i)("PLANT") & "'"
            Else
                _PLANTFilter += ", '" & TSETTINGPLANTBindingSource(i)("PLANT") & "'"
            End If

        Next
        _SALEORGFilter &= ")"
        _PLANTFilter &= ")"
        Me.SALEORGFilter &= " and (SP_Code in" & _SALEORGFilter & ")"
        Me.PLANTFilter &= " and (PLANT in" & _PLANTFilter & ")"
        Try
            TSHIPPERBindingSource.Filter = Me.SALEORGFilter
            TSTOBindingSource.Filter = Me.PLANTFilter
            CORG.SelectedIndex = 0
            TSHIPPERBindingSource_PositionChanged(sender, e)

        Catch ex As Exception

        End Try

    End Sub




    Private Sub TSHIPPERBindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSHIPPERBindingSource.PositionChanged
        Try

            TSTOBindingSource.Filter = Me.PLANTFilter & " and ( SALEORG='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "')"
            'TSTOBindingSource.Position = 0
            CPlant.SelectedIndex = 0
            TSTOBindingSource_PositionChanged(sender, e)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TSTOBindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSTOBindingSource.PositionChanged
        ' Dim DoTable2 As DataTable = DoTable.Clone

        Try
            TLOBindingSource.Filter = "PLANT_LOCATION='" & TSTOBindingSource(TSTOBindingSource.Position)("PLANT").ToString & "'"
            TSTOBindingSource.Item(TSTOBindingSource.Position)("PLANT").ToString()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub RadButton1_Click(sender As System.Object, e As System.EventArgs) Handles RadButton1.Click
        Try
            Cursor = Cursors.WaitCursor
            Enabled = False
            TSETTINGPLANTBindingSource.Filter = "PLANT='" & TSTOBindingSource(TSTOBindingSource.Position)("PLANT") & "'"
            Dim SqlCon As New SqlConnection(TSETTINGPLANTBindingSource(TSETTINGPLANTBindingSource.Position)("DBConStr").ToString)
            SqlCon.Open()
            Dim cmd As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            cmd.Connection = SqlCon
            cmd.CommandText = "select * from V_LOADINGNOTE Where LOAD_DOFULL like '" & Trim(EDONO.Text) & "'" &
                " and PLANT='" & CPlant.Text & "'" & " and SP_CODE='" & CORG.Text & "'"
            da.SelectCommand = cmd
            da.Fill(ds, "V_LOADINGNOTE")
            VLOADINGNOTEBindingSource.DataSource = ds
            VLOADINGNOTEBindingSource.DataMember = "V_LOADINGNOTE"
            SqlCon.Close()
            SqlCon.Dispose()
            ds.Dispose()
            da.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Enabled = True
        Cursor = Cursors.Default
    End Sub

    Private Sub RadButton2_Click(sender As System.Object, e As System.EventArgs) Handles RadButton2.Click
        Try
            Cursor = Cursors.WaitCursor
            Enabled = False
            Dim SqlCon As New SqlConnection(TSETTINGPLANTBindingSource(TSETTINGPLANTBindingSource.Position)("DBConStr").ToString)
            SqlCon.Open()
            Dim SqlCon2 As New SqlConnection(My.Settings.FPTConnectionString)
            SqlCon2.Open()
            Dim cmd As New SqlCommand
            Dim cmd2 As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim da2 As New SqlDataAdapter
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim LOAD_DOFULL As String
            ' LOAD_DOFULL = RadGridView1.CurrentRow.Group.GroupRow.Cells(0).Value
            LOAD_DOFULL = RadGridView1.CurrentRow.Group.Header
            ' LOAD_DOFULL = RadGridView1.CurrentRow.Group.HeaderRow.HeaderText
            'INDEX = lvRefresh.Groups(lvRefresh.CurrentRow.Group.HeaderRow.Index).Item(0).Cells("INDEX").Value
            ' LOAD_DOFULL = RadGridView1.Groups(RadGridView1.CurrentRow.Group.HeaderRow.Index).HeaderRow.Cells(0).Value
            ' LOAD_DOFULL = Trim(VLOADINGNOTEBindingSource(VLOADINGNOTEBindingSource.Position)("LOAD_DOFULL").ToString())


            cmd2.Connection = SqlCon2
            cmd2.CommandText = "select * from T_DO Where DO_NO='" & LOAD_DOFULL & "'"
            da2.SelectCommand = cmd2
            da2.Fill(ds2, "T_DO")
            If ds2.Tables("T_DO").Rows.Count > 0 Then
                MessageBox.Show("D/O " & LOAD_DOFULL & " มีข้อมูลอยู่ในระบบแล้ว ไม่สามารถบันทึกข้อมูลซ้ำกันได้ !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                '''
                FPTDataSet.T_DO1.RejectChanges()
                FPTDataSet.T_LOADINGNOTE.RejectChanges()
                FPTDataSet.T_LOADINGNOTECOMPARTMENT.RejectChanges()
                FPTDataSet.T_BATCH_LOT_TAS.RejectChanges()

                cmd.Connection = SqlCon
                cmd.CommandText = "select * from T_DO Where DO_NO='" & LOAD_DOFULL & "'"
                da.SelectCommand = cmd
                da.Fill(ds, "T_DO")

                cmd.CommandText = "select * from T_BATCH_LOT_TAS Where DO_NO='" & LOAD_DOFULL & "'"
                da.SelectCommand = cmd
                da.Fill(ds, "T_BATCH_LOT_TAS")

                cmd.CommandText = "select * from T_LOADINGNOTE Where LOAD_DOFULL='" & LOAD_DOFULL & "'"
                da.SelectCommand = cmd
                da.Fill(ds, "T_LOADINGNOTE")
                cmd.CommandText = "select * from T_LOADINGNOTECOMPARTMENT Where LC_LOAD in(Select LOAD_ID from  T_LOADINGNOTE where LOAD_DOFULL='" & LOAD_DOFULL & "')"
                da.SelectCommand = cmd
                da.Fill(ds, "T_LOADINGNOTECOMPARTMENT")
                Dim IDIndex As Integer

                For Each dr As DataRow In ds.Tables("T_LOADINGNOTECOMPARTMENT").Rows
                    IDIndex += -1
                    dr.Item("LC_ID") = IDIndex
                    FPTDataSet.T_LOADINGNOTECOMPARTMENT.Rows.Add(dr.ItemArray)
                Next

                For Each dr As DataRow In ds.Tables("T_LOADINGNOTE").Rows
                    For i = 0 To FPTDataSet.T_LOADINGNOTECOMPARTMENT.Rows.Count - 1
                        If FPTDataSet.T_LOADINGNOTECOMPARTMENT.Rows(i)("LC_LOAD") = dr.Item("LOAD_ID") Then
                            FPTDataSet.T_LOADINGNOTECOMPARTMENT.Rows(i)("LC_LOAD") = FPTDataSet.T_LOADINGNOTE.Compute("Max(LOAD_ID)", "") + 1
                        End If
                    Next
                    dr.Item("LOAD_ID") = FPTDataSet.T_LOADINGNOTE.Compute("Max(LOAD_ID)", "") + 1
                    FPTDataSet.T_LOADINGNOTE.Rows.Add(dr.ItemArray)
                Next




                For Each dr As DataRow In ds.Tables("T_BATCH_LOT_TAS").Rows
                    IDIndex += -1
                    dr.Item("ID") = IDIndex
                    FPTDataSet.T_BATCH_LOT_TAS.Rows.Add(dr.ItemArray)
                Next

                For Each dr As DataRow In ds.Tables("T_DO").Rows
                    IDIndex += -1
                    dr.Item("ID") = IDIndex
                    FPTDataSet.T_DO1.Rows.Add(dr.ItemArray)
                Next


                '  TDO1BindingSource.AddNew()
                ' ds.Tables("T_LOADINGNOTE") = ds.Tables("T_LOADINGNOTE").Copy
                ' T_BATCH_LOST_TASTableAdapter.Update(dr)
                '  FPTDataSet.T_LOADINGNOTE.ImportRow(dr)
                'FPTDataSet.T_LOADINGNOTE.Copy(
                ' MessageBox.Show(FPTDataSet.Tables("T_LOADINGNOTE").Rows(0)(0).ToString)
                BindingContext(FPTDataSet, "T_BATCH_LOT_TAS").EndCurrentEdit()
                BindingContext(FPTDataSet, "T_LOADINGNOTE").EndCurrentEdit()
                BindingContext(FPTDataSet, "T_LOADINGNOTECOMPARTMENT").EndCurrentEdit()
                BindingContext(FPTDataSet, "T_DO1").EndCurrentEdit()
                T_LOADINGNOTETableAdapter.Update(FPTDataSet.T_LOADINGNOTE)
                T_LOADINGNOTECOMPARTMENTTableAdapter.Update(FPTDataSet.T_LOADINGNOTECOMPARTMENT)
                T_DO1TableAdapter.Update(FPTDataSet.T_DO1)
                T_BATCH_LOT_TASTableAdapter.Update(FPTDataSet.T_BATCH_LOT_TAS)
                '  T_LOADINGNOTECOMPARTMENTTableAdapter.Update(ds.Tables("T_LOADINGNOTECOMPARTMENT"))
                '  T_DOTableAdapter.Update(ds.Tables("T_DO"))
                MessageBox.Show("บันทึกข้อมูลเรียบร้อย")
                Close()
            End If

            SqlCon.Close()
            SqlCon.Dispose()
            ds.Dispose()
            da.Dispose()
            SqlCon2.Close()
            SqlCon2.Dispose()
            ds2.Dispose()
            da2.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Enabled = True
        Cursor = Cursors.Default
    End Sub


    Private Sub RadButton3_Click(sender As System.Object, e As System.EventArgs) Handles RadButton3.Click
        Close()
    End Sub
End Class
