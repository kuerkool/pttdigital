﻿Imports System.Net
Imports TAS_TOL.SAP02
Imports TAS_TOL.SAP03
Imports TAS_TOL.FSAP03
Imports Telerik.WinControls.UI

Public Class FSAP02
    Public Shared SAP_ITEM As DO_ITEM
    Public Shared SAP_ALLITEM() As DO_ITEM
    Public Shared SAP_BATCH_ITEM() As BATCH_ITEM
    Private DODATA() As Extract_DO_DO02_DTITEM
    Private SHIndex As Int16
    Private DoTalbe As New DataTable

    Private Const SC_CLOSE As Integer = &HF060
    Private Const MF_GRAYED As Integer = &H1

    Private Declare Function GetSystemMenu Lib "user32" _
       (ByVal hwnd As Long, _
        ByVal bRevert As Long) As Long

    Private Declare Function RemoveMenu Lib "user32" _
        (ByVal hMenu As Long, _
         ByVal nPosition As Long, _
         ByVal wFlags As Long) As Long

    Private Const MF_BYPOSITION = &H400&

    Public Structure BATCH_ITEM
        Public BT_RETURN As Integer
        Public DO_NO As String ''''
        Public DO_ITEM As String
        Public PLANT As String
        Public STORAGE_LOC As String
        Public BATCH_NO As String ''
        Public QTY_DELV As String ''
        Public HIGHER_LEVEL_ITEM As String
        Public MAT_CODE As String
    End Structure

    Public Structure DO_ITEM
        Public BT_RETURN As Integer
        Public dO_NO As String
        Public dO_ITEM As String
        Public sO_NO As String
        Public sO_ITEM As String
        Public dO_CDATE As String
        Public dO_CTIME As String
        Public sD_DOC_CAT As String
        Public sALES_ORG As String
        Public dIS_CHAN As String
        Public dIVISION As String
        Public sHIP_POINT As String
        Public dOC_DATE As String
        Public pLAN_GI_DATE As String
        Public lOAD_DATE As String
        Public lOAD_TIME As String
        Public tRN_PLAN_DATE As String
        Public dELV_DATE As String
        Public pICK_DATE As String
        Public sHIP_TO_PARTY As String
        Public sOLD_TO_PARTY As String
        Public tOTAL_WEIGHT As String
        Public mAT_CODE As String
        Public mAT_DES As String
        Public pLANT As String
        Public sTORAGE_LOC As String
        Public bATCH_NO As String
        Public aCT_QTY_DELV_SU As String
        Public bASE_UOM As String
        Public sALES_UNIT As String
        Public nET_WEIGHT As String
        Public gROSS_WEIGHT As String
        Public wEIGHT_UNIT As String
        Public vOLUME As String
        Public vOLUME_UNIT As String
        Public oVR_DELV_TLIMIT As String
        Public uND_DELV_TLIMIT As String
        Public hIGHER_LEVEL_ITEM As String
        Public bATCH_INDICATOR As String
        Public dELIVERY_INSTRUCTION_TEXT As String
        Public pUR_ORDER_TYPE As String
        Public lOADING_POINT As String
        Public sTAT_WAREHOUSE As String

    End Structure


#Region " Gen ListView Columns"
    Private Sub AddTableColumns(ByRef GridView As RadGridView)
        DoTalbe.Columns.Clear()
        DoTalbe.Columns.Add("Delivery order")
        DoTalbe.Columns.Add("Delivery item")
        DoTalbe.Columns.Add("Sales order No.")
        DoTalbe.Columns.Add("Sales order Item.")
        DoTalbe.Columns.Add("D/O Create Date")
        DoTalbe.Columns.Add("D/O Create Time")
        DoTalbe.Columns.Add("SD document category")
        DoTalbe.Columns.Add("Sales organization")
        DoTalbe.Columns.Add("Distribution channel")
        DoTalbe.Columns.Add("Division")
        DoTalbe.Columns.Add("Shipping Point")
        DoTalbe.Columns.Add("Document date")
        DoTalbe.Columns.Add("Planned goods issue date")
        DoTalbe.Columns.Add("Loading Date")
        DoTalbe.Columns.Add("Loading time")
        DoTalbe.Columns.Add("Transportation")
        DoTalbe.Columns.Add("Planning Date")
        DoTalbe.Columns.Add("Delivery Date")
        DoTalbe.Columns.Add("Picking Date")
        DoTalbe.Columns.Add("Ship-to-Party")
        DoTalbe.Columns.Add("Sold-to-Party")
        DoTalbe.Columns.Add("Total Weight")
        DoTalbe.Columns.Add("Material code")
        DoTalbe.Columns.Add("Short text for sales order item")
        DoTalbe.Columns.Add("Plant")
        DoTalbe.Columns.Add("Storage Location")
        DoTalbe.Columns.Add("Batch Number")
        DoTalbe.Columns.Add("Actual quantity delivered (in sales units)")
        DoTalbe.Columns.Add("Base Unit of Measure")
        DoTalbe.Columns.Add("Sales unit")
        DoTalbe.Columns.Add("Net weight")
        DoTalbe.Columns.Add("Gross weight")
        DoTalbe.Columns.Add("Weight Unit")
        DoTalbe.Columns.Add("Volume")
        DoTalbe.Columns.Add("Volume unit")
        DoTalbe.Columns.Add("Over delivery Tolerance Limit")
        DoTalbe.Columns.Add("Under delivery Tolerance Limit")
        DoTalbe.Columns.Add("Higher-Level Item of Batch Split Item")
        DoTalbe.Columns.Add("Batch management requirement indicator")
        DoTalbe.Columns.Add("Delivery instruction text (Z016)")
        DoTalbe.Columns.Add("Purchase order type")
        DoTalbe.Columns.Add("loading point (Key for define actual printed location)")
        DoTalbe.Columns.Add("Status of warehouse management")


        GridView.DataSource = DoTalbe
        GridView.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
        For i = 0 To GridView.Columns.Count - 1
            GridView.Columns(i).Width = 120
            GridView.Columns(i).AutoSizeMode = BestFitColumnMode.HeaderCells
        Next

    End Sub
    Private Sub AddColumns(ByRef _ListView As ListView)
        _ListView.Columns.Clear()
        _ListView.Columns.Add(UCase("Delivery Order"))
        _ListView.Columns.Add(UCase("D/O Item"))
        _ListView.Columns.Add(UCase("Sales order No."))
        _ListView.Columns.Add(UCase("Sales order Item"))
        _ListView.Columns.Add(UCase("hIGHER_LEVEL_ITEM"))
        _ListView.Columns.Add(UCase("Actual quantity delivered (in sales units)"))
        _ListView.Columns.Add(UCase("sALES UNIT"))
        _ListView.Columns.Add(UCase("Customer Name"))
        _ListView.Columns.Add(UCase("Product Name"))
        _ListView.Columns.Add(UCase("sHIP_TO_PARTY"))
        _ListView.Columns.Add(UCase("mAT_CODE"))
        _ListView.Columns.Add(UCase("dO_CDATE"))
        _ListView.Columns.Add(UCase("dO_CTIME"))
        _ListView.Columns.Add(UCase("sD_DOC_CAT"))
        _ListView.Columns.Add(UCase("sALES_ORG"))
        _ListView.Columns.Add(UCase("dIS_CHAN"))
        _ListView.Columns.Add(UCase("dIVISION"))
        _ListView.Columns.Add(UCase("sHIP_POINT"))
        _ListView.Columns.Add(UCase("dOC_DATE"))
        _ListView.Columns.Add(UCase("pLAN_GI_DATE"))
        _ListView.Columns.Add(UCase("lOAD_DATE"))
        _ListView.Columns.Add(UCase("tRN_PLAN_DATE"))
        _ListView.Columns.Add(UCase("dELV_DATE"))
        _ListView.Columns.Add(UCase("pICK_DATE"))

        _ListView.Columns.Add(UCase("sOLD_TO_PARTY"))
        _ListView.Columns.Add(UCase("tOTAL_WEIGHT"))

        _ListView.Columns.Add(UCase("pLANT"))
        _ListView.Columns.Add(UCase("sTORAGE_LOC"))
        _ListView.Columns.Add(UCase("bATCH_NO"))

        _ListView.Columns.Add(UCase("bASE_UOM"))

        _ListView.Columns.Add(UCase("nET_WEIGHT"))
        _ListView.Columns.Add(UCase("gROSS_WEIGHT"))
        _ListView.Columns.Add(UCase("wEIGHT_UNIT"))
        _ListView.Columns.Add(UCase("vOLUME"))
        _ListView.Columns.Add(UCase("vOLUME_UNIT"))
        _ListView.Columns.Add(UCase("oVR_DELV_TLIMIT"))
        _ListView.Columns.Add(UCase("uND_DELV_TLIMIT"))
        _ListView.Columns.Add(UCase("BATCH_INDICATOR"))
        _ListView.Columns.Add(UCase("DELIVERY_INSTRUCTION_TEXT"))

  

        '  _ListView.Items(2).SubItems(2).ForeColor = Color.Blue

        For i = 0 To _ListView.Columns.Count - 1

            _ListView.Columns(i).Width = (_ListView.Columns(i).Text.Length + 3) * 10

        Next
    End Sub

    Private Sub AddTableItems(ByRef GridView As RadGridView, ByVal DODATA() As Extract_DO_DO02_DTITEM)
        Dim l As Int16 = 0
        Dim workRow As DataRow
        Dim i As Integer
        DoTalbe.Rows.Clear()
        For i = 0 To DODATA.Length - 1
            workRow = DoTalbe.NewRow()
            workRow(0) = DODATA(i).DO_NO
            workRow(1) = DODATA(i).DO_ITEM
            workRow(2) = DODATA(i).SO_NO
            workRow(3) = DODATA(i).SO_ITEM
            workRow(4) = DODATA(i).DO_CDATE
            workRow(5) = DODATA(i).DO_CTIME
            workRow(6) = DODATA(i).SD_DOC_CAT
            workRow(7) = DODATA(i).SALES_ORG
            workRow(8) = DODATA(i).DIS_CHAN
            workRow(9) = DODATA(i).DIVISION
            workRow(10) = DODATA(i).SHIP_POINT
            workRow(11) = DODATA(i).DOC_DATE
            workRow(12) = DODATA(i).PLAN_GI_DATE
            workRow(13) = DODATA(i).LOAD_DATE
            workRow(14) = DODATA(i).LOAD_TIME
            workRow(15) = DODATA(i).TRN_PLAN_DATE
            workRow(16) = DODATA(i).DELV_DATE
            workRow(17) = DODATA(i).PICK_DATE
            workRow(18) = DODATA(i).SHIP_TO_PARTY
            workRow(19) = DODATA(i).SOLD_TO_PARTY
            workRow(20) = DODATA(i).TOTAL_WEIGHT
            workRow(21) = DODATA(i).MAT_CODE
            workRow(22) = DODATA(i).MAT_DES
            workRow(23) = DODATA(i).PLANT
            workRow(24) = DODATA(i).STORAGE_LOC
            workRow(25) = DODATA(i).BATCH_NO
            workRow(26) = DODATA(i).ACT_QTY_DELV_SU
            workRow(27) = DODATA(i).BASE_UOM
            workRow(28) = DODATA(i).SALES_UNIT
            workRow(29) = DODATA(i).NET_WEIGHT
            workRow(30) = DODATA(i).GROSS_WEIGHT
            workRow(31) = DODATA(i).WEIGHT_UNIT
            workRow(32) = DODATA(i).VOLUME
            workRow(33) = DODATA(i).VOLUME_UNIT
            workRow(34) = DODATA(i).OVR_DELV_TLIMIT
            workRow(35) = DODATA(i).UND_DELV_TLIMIT
            workRow(36) = DODATA(i).HIGHER_LEVEL_ITEM
            workRow(37) = DODATA(i).BATCH_INDICATOR
            workRow(38) = DODATA(i).DELIVERY_INSTRUCTION_TEXT
            workRow(39) = DODATA(i).PUR_ORDER_TYPE
            workRow(40) = DODATA(i).LOADING_POINT
            workRow(41) = DODATA(i).STAT_WAREHOUSE
            DoTalbe.Rows.Add(workRow)
        Next




        '   drarray.
        '   If drarray.Length > 0 Then

        'For Each row As DataRow In drarray
        'Dt.ImportRow(row)
        'Next

        'End If
        'DoTalbe.ImportRow()
        'While l < DODATA.Length
        '    DoTalbe.NewRow()
        '    DoTalb()

        '    DOdt.NewRow()
        '    'DoTalbe()

        'End While
        '_Listview.Items.Add(DODATA(l).DO_NO)


    End Sub

    Private Sub AddItems(ByRef _Listview As ListView, ByVal DODATA() As Extract_DO_DO02_DTITEM)
        Dim l As Int16 = 0
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Dim q As String
        _Listview.Items.Clear()
        While l < DODATA.Length
            _Listview.Items.Add(DODATA(l).DO_NO)
            '_Listview.Items(l).SubItems(0).ForeColor = Color.Blue

            '_Listview.Items(l).SubItems(0).Font = New Font(_Listview.Items(l).SubItems(0).Font, _
            '_Listview.Items(l).SubItems(0).Font.Style Or FontStyle.Bold)

            _Listview.Items(l).SubItems.Add(DODATA(l).DO_ITEM)
            _Listview.Items(l).SubItems.Add(DODATA(l).SO_NO)
            _Listview.Items(l).SubItems.Add(DODATA(l).SO_ITEM)
            _Listview.Items(l).SubItems.Add(DODATA(l).HIGHER_LEVEL_ITEM)
            _Listview.Items(l).SubItems.Add(DODATA(l).ACT_QTY_DELV_SU)
            _Listview.Items(l).SubItems.Add(DODATA(l).SALES_UNIT)
            Try
                q = "select * from T_Customer where CUS_CODE='" & DODATA(l).SHIP_TO_PARTY & "' and SORG_CODE='" & DODATA(l).SALES_ORG & "'"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                Dim dt As New DataTable
                da.Fill(dt)
                _Listview.Items(l).SubItems.Add(dt.Rows(0)("Customer_code").ToString)
            Catch ex As Exception
            End Try

            Try
                q = "select * from T_product where Product_number='" & DODATA(l).MAT_CODE & "'"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                Dim dt As New DataTable
                da.Fill(dt)
                _Listview.Items(l).SubItems.Add(dt.Rows(0)("product_name").ToString)
            Catch ex As Exception
            End Try
            _Listview.Items(l).SubItems.Add(DODATA(l).SHIP_TO_PARTY)
            _Listview.Items(l).SubItems.Add(DODATA(l).MAT_CODE)


            _Listview.Items(l).SubItems.Add(DODATA(l).DO_CDATE)
            _Listview.Items(l).SubItems.Add(DODATA(l).DO_CTIME)
            _Listview.Items(l).SubItems.Add(DODATA(l).SD_DOC_CAT)
            _Listview.Items(l).SubItems.Add(DODATA(l).SALES_ORG)
            _Listview.Items(l).SubItems.Add(DODATA(l).DIS_CHAN)
            _Listview.Items(l).SubItems.Add(DODATA(l).DIVISION)
            _Listview.Items(l).SubItems.Add(DODATA(l).SHIP_POINT)
            _Listview.Items(l).SubItems.Add(DODATA(l).DOC_DATE)
            _Listview.Items(l).SubItems.Add(DODATA(l).PLAN_GI_DATE)
            _Listview.Items(l).SubItems.Add(DODATA(l).LOAD_DATE)
            _Listview.Items(l).SubItems.Add(DODATA(l).TRN_PLAN_DATE)
            _Listview.Items(l).SubItems.Add(DODATA(l).DELV_DATE)
            _Listview.Items(l).SubItems.Add(DODATA(l).PICK_DATE)

            _Listview.Items(l).SubItems.Add(DODATA(l).SOLD_TO_PARTY)
            _Listview.Items(l).SubItems.Add(DODATA(l).TOTAL_WEIGHT)

            _Listview.Items(l).SubItems.Add(DODATA(l).PLANT)
            _Listview.Items(l).SubItems.Add(DODATA(l).STORAGE_LOC)
            _Listview.Items(l).SubItems.Add(DODATA(l).BATCH_NO)

            _Listview.Items(l).SubItems.Add(DODATA(l).BASE_UOM)

            _Listview.Items(l).SubItems.Add(DODATA(l).NET_WEIGHT)
            _Listview.Items(l).SubItems.Add(DODATA(l).GROSS_WEIGHT)
            _Listview.Items(l).SubItems.Add(DODATA(l).WEIGHT_UNIT)
            _Listview.Items(l).SubItems.Add(DODATA(l).VOLUME)
            _Listview.Items(l).SubItems.Add(DODATA(l).VOLUME_UNIT)
            _Listview.Items(l).SubItems.Add(DODATA(l).OVR_DELV_TLIMIT)
            _Listview.Items(l).SubItems.Add(DODATA(l).UND_DELV_TLIMIT)
            _Listview.Items(l).SubItems.Add(DODATA(l).BATCH_INDICATOR)
            _Listview.Items(l).SubItems.Add(DODATA(l).DELIVERY_INSTRUCTION_TEXT)
            l = l + 1
        End While
    End Sub

#End Region


    Public Function DisableCloseButton(ByVal frm As Form) As Boolean

        'PURPOSE: Removes X button from a form
        'EXAMPLE: DisableCloseButton Me
        'RETURNS: True if successful, false otherwise
        'NOTES:   Also removes Exit Item from
        '         Control Box Menu

        Dim lHndSysMenu As Long
        Dim lAns1 As Long, lAns2 As Long


        lHndSysMenu = GetSystemMenu(frm.Handle, 0)

        'remove close button
        lAns1 = RemoveMenu(lHndSysMenu, 6, MF_BYPOSITION)

        'Remove seperator bar
        lAns2 = RemoveMenu(lHndSysMenu, 5, MF_BYPOSITION)

        'Return True if both calls were successful
        DisableCloseButton = (lAns1 <> 0 And lAns2 <> 0)

    End Function

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        AddColumns(lvRefresh)

        'AddTableColumns(RadGridView2)
        '  AddTableItems(RadGridView2)
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Private Sub BtGetdata_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtGetdata.Click
        Try
            Cursor = Cursors.WaitCursor
            Dim MRequest As New SAP02.Extract_DO_DT
            MRequest.SORG = TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString
            MRequest.CRDATE_S = DpStart.Value.Date.Day & "/" & DpStart.Value.Date.Month & "/" & DpStart.Value.Date.Year
            MRequest.CRDATE_E = DpEnd.Value.Date.Day & "/" & DpEnd.Value.Date.Month & "/" & DpEnd.Value.Date.Year
            Dim myproxy As New Extract_DO_Sync_Out_SIService
            Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
            Dim netCred As NetworkCredential = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            Dim uri As Uri = New Uri(objIniFile.GetString("Webservice", "URL02", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING02"))

            ' Dim netCred As NetworkCredential = New NetworkCredential("TAS01", "initial1")
            ' Dim uri As Uri = New Uri("http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING02")
            Dim credentials As ICredentials = netCred.GetCredential(uri, "Basic")
            myproxy.Url = uri.AbsoluteUri
            myproxy.Credentials = credentials
            myproxy.PreAuthenticate = True
            ' myproxy.Credentials = credentials
            AddHandler myproxy.Extract_DO_Sync_Out_SICompleted, AddressOf GetDO
            '   TSLWS.Enabled = False
            myproxy.Extract_DO_Sync_Out_SIAsync(MRequest)
        Catch ex As Exception
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub GetDO(ByVal sender As Object, ByVal e As Extract_DO_Sync_Out_SICompletedEventArgs)
        Try
            If (Not e.Cancelled) Then
                Dim MResponse As New SAP02.Extract_DO_Resp_DT
                MResponse = e.Result
                DODATA = MResponse.DO02
                'AddTableItems(RadGridView2, DODATA)
                AddItems(lvRefresh, DODATA)


                GroupBox1.Text = "Last Get " + CbShipper.Text + " DO from SAP on " + Now.ToString
                Cursor = Cursors.Default
            End If

        Catch ex As Exception
            MsgBox(ex.Message + Chr(13) + e.Error.Message, MsgBoxStyle.Critical, "Error message")
            Cursor = Cursors.Default


        End Try
    End Sub


    Private Sub BtImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtImport.Click
        Try
            '  Check DO NO DO ITEM
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()
            Dim q As String
            Dim MsgString As String
            MsgString = "Delivery order: " & DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO & " D/O Item: " & DODATA(lvRefresh.SelectedIndices.Item(0)).HIGHER_LEVEL_ITEM _
                                     & " มีขอมูลอยู่ในระบบ TAS แล้ว !"
            q = "select LOAD_ID from V_LOADINGNOTE WHERE do_NO='" & DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO & "' and do_item='" & DODATA(lvRefresh.SelectedIndices.Item(0)).HIGHER_LEVEL_ITEM & "'"
            If DODATA(lvRefresh.SelectedIndices.Item(0)).HIGHER_LEVEL_ITEM = 0 Then
                q = "select LOAD_ID from V_LOADINGNOTE WHERE do_NO='" & DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO & "' and do_item='" & DODATA(lvRefresh.SelectedIndices.Item(0)).DO_ITEM & "'"
                MsgString = "Delivery order: " & DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO & " D/O Item: " & DODATA(lvRefresh.SelectedIndices.Item(0)).DO_ITEM _
                                    & " มีขอมูลอยู่ในระบบ TAS แล้ว !"
            End If
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim dt As New DataTable
            da.Fill(dt)

            If dt.Rows.Count > 0 Then
                If MsgBox(MsgString, vbYesNo + vbDefaultButton2, "Confirmation") = vbNo Then
                    Exit Sub
                End If
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' test

            SAP_BATCH_ITEM = Nothing
            SAP_ALLITEM = Nothing
            SAP_ITEM = Nothing
            Dim IndexDO As Integer = 0
            Dim IndexBatch As Integer = 0
            For i = 0 To DODATA.Length - 1
                If (DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO = DODATA(i).DO_NO) And
                (DODATA(i).HIGHER_LEVEL_ITEM = 0) Then
                    ReDim Preserve SAP_ALLITEM(IndexDO)
                    SAP_ALLITEM(IndexDO).BT_RETURN = 1
                    SAP_ALLITEM(IndexDO).dO_NO = DODATA(i).DO_NO
                    SAP_ALLITEM(IndexDO).dO_ITEM = DODATA(i).DO_ITEM
                    SAP_ALLITEM(IndexDO).sO_NO = DODATA(i).SO_NO
                    SAP_ALLITEM(IndexDO).sO_ITEM = DODATA(i).SO_ITEM
                    SAP_ALLITEM(IndexDO).hIGHER_LEVEL_ITEM = DODATA(i).HIGHER_LEVEL_ITEM
                    SAP_ALLITEM(IndexDO).dO_CDATE = DODATA(i).DO_CDATE
                    SAP_ALLITEM(IndexDO).dO_CTIME = DODATA(i).DO_CTIME
                    SAP_ALLITEM(IndexDO).sD_DOC_CAT = DODATA(i).SD_DOC_CAT
                    SAP_ALLITEM(IndexDO).sALES_ORG = DODATA(i).SALES_ORG
                    SAP_ALLITEM(IndexDO).dIS_CHAN = DODATA(i).DIS_CHAN
                    SAP_ALLITEM(IndexDO).dIVISION = DODATA(i).DIVISION
                    SAP_ALLITEM(IndexDO).sHIP_POINT = DODATA(i).SHIP_POINT
                    SAP_ALLITEM(IndexDO).dOC_DATE = DODATA(i).DOC_DATE
                    SAP_ALLITEM(IndexDO).pLAN_GI_DATE = DODATA(i).PLAN_GI_DATE
                    SAP_ALLITEM(IndexDO).lOAD_DATE = DODATA(i).LOAD_DATE
                    SAP_ALLITEM(IndexDO).tRN_PLAN_DATE = DODATA(i).TRN_PLAN_DATE
                    SAP_ALLITEM(IndexDO).dELV_DATE = DODATA(i).DELV_DATE
                    SAP_ALLITEM(IndexDO).pICK_DATE = DODATA(i).PICK_DATE
                    SAP_ALLITEM(IndexDO).sHIP_TO_PARTY = DODATA(i).SHIP_TO_PARTY
                    SAP_ALLITEM(IndexDO).sOLD_TO_PARTY = DODATA(i).SOLD_TO_PARTY
                    SAP_ALLITEM(IndexDO).tOTAL_WEIGHT = DODATA(i).TOTAL_WEIGHT
                    SAP_ALLITEM(IndexDO).mAT_CODE = DODATA(i).MAT_CODE
                    SAP_ALLITEM(IndexDO).pLANT = DODATA(i).PLANT
                    SAP_ALLITEM(IndexDO).sTORAGE_LOC = DODATA(i).STORAGE_LOC
                    ' SAP_ALLITEM(IndexDO).BATCH_NO = DODATA(i).BATCH_NO
                    SAP_ALLITEM(IndexDO).aCT_QTY_DELV_SU = DODATA(i).ACT_QTY_DELV_SU
                    SAP_ALLITEM(IndexDO).bASE_UOM = DODATA(i).BASE_UOM
                    SAP_ALLITEM(IndexDO).sALES_UNIT = DODATA(i).SALES_UNIT
                    SAP_ALLITEM(IndexDO).nET_WEIGHT = DODATA(i).NET_WEIGHT
                    SAP_ALLITEM(IndexDO).gROSS_WEIGHT = DODATA(i).GROSS_WEIGHT
                    SAP_ALLITEM(IndexDO).wEIGHT_UNIT = DODATA(i).WEIGHT_UNIT
                    SAP_ALLITEM(IndexDO).vOLUME = DODATA(i).VOLUME
                    SAP_ALLITEM(IndexDO).vOLUME_UNIT = DODATA(i).VOLUME_UNIT
                    SAP_ALLITEM(IndexDO).oVR_DELV_TLIMIT = DODATA(i).OVR_DELV_TLIMIT
                    SAP_ALLITEM(IndexDO).uND_DELV_TLIMIT = DODATA(i).UND_DELV_TLIMIT
                    SAP_ALLITEM(IndexDO).bATCH_INDICATOR = DODATA(i).BATCH_INDICATOR
                    SAP_ALLITEM(IndexDO).dELIVERY_INSTRUCTION_TEXT = DODATA(i).DELIVERY_INSTRUCTION_TEXT
                    Dim Batch_count As Int16 = 0
                    For l = 0 To DODATA.Length - 1
                        If (DODATA(i).DO_NO = DODATA(l).DO_NO) And
                            (DODATA(i).DO_ITEM = DODATA(l).HIGHER_LEVEL_ITEM) Then

                            Batch_count += 1
                        End If
                    Next
                    If (UCase(SAP_ALLITEM(IndexDO).bATCH_INDICATOR)) = "X" And (Batch_count = 0) Then
                        ReDim Preserve SAP_BATCH_ITEM(IndexBatch)
                        SAP_BATCH_ITEM(IndexBatch).BT_RETURN = 1
                        SAP_BATCH_ITEM(IndexBatch).DO_NO = DODATA(i).DO_NO
                        SAP_BATCH_ITEM(IndexBatch).DO_ITEM = DODATA(i).DO_ITEM
                        SAP_BATCH_ITEM(IndexBatch).BATCH_NO = DODATA(i).BATCH_NO
                        SAP_BATCH_ITEM(IndexBatch).PLANT = DODATA(i).PLANT
                        SAP_BATCH_ITEM(IndexBatch).QTY_DELV = DODATA(i).ACT_QTY_DELV_SU
                        SAP_BATCH_ITEM(IndexBatch).STORAGE_LOC = DODATA(i).STORAGE_LOC
                        SAP_BATCH_ITEM(IndexBatch).HIGHER_LEVEL_ITEM = DODATA(i).DO_ITEM
                        SAP_BATCH_ITEM(IndexBatch).MAT_CODE = DODATA(i).MAT_CODE
                        IndexBatch += 1
                    End If

                    IndexDO += 1
                ElseIf (DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO = DODATA(i).DO_NO) And
                       (DODATA(i).HIGHER_LEVEL_ITEM <> 0) Then
                    ReDim Preserve SAP_BATCH_ITEM(IndexBatch)
                    SAP_BATCH_ITEM(IndexBatch).BT_RETURN = 1
                    SAP_BATCH_ITEM(IndexBatch).DO_NO = DODATA(i).DO_NO
                    SAP_BATCH_ITEM(IndexBatch).DO_ITEM = DODATA(i).HIGHER_LEVEL_ITEM
                    SAP_BATCH_ITEM(IndexBatch).BATCH_NO = DODATA(i).BATCH_NO
                    SAP_BATCH_ITEM(IndexBatch).PLANT = DODATA(i).PLANT
                    SAP_BATCH_ITEM(IndexBatch).QTY_DELV = DODATA(i).ACT_QTY_DELV_SU
                    SAP_BATCH_ITEM(IndexBatch).STORAGE_LOC = DODATA(i).STORAGE_LOC
                    SAP_BATCH_ITEM(IndexBatch).HIGHER_LEVEL_ITEM = DODATA(i).HIGHER_LEVEL_ITEM
                    Dim Mat_code As String
                    For l = 0 To DODATA.Length - 1
                        If (DODATA(l).DO_NO = DODATA(i).DO_NO) And (DODATA(l).DO_ITEM = DODATA(i).HIGHER_LEVEL_ITEM) Then
                            Mat_code = DODATA(l).MAT_CODE
                            Exit For
                        End If
                    Next l
                    SAP_BATCH_ITEM(IndexBatch).MAT_CODE = Mat_code
                    IndexBatch += 1
                End If
            Next


            If Int(DODATA(lvRefresh.SelectedIndices.Item(0)).HIGHER_LEVEL_ITEM) = 0 Then
                SAP_ITEM.BT_RETURN = 1
                SAP_ITEM.dO_NO = DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO
                SAP_ITEM.dO_ITEM = DODATA(lvRefresh.SelectedIndices.Item(0)).DO_ITEM
                SAP_ITEM.sO_NO = DODATA(lvRefresh.SelectedIndices.Item(0)).SO_NO
                SAP_ITEM.sO_ITEM = DODATA(lvRefresh.SelectedIndices.Item(0)).SO_ITEM
                SAP_ITEM.hIGHER_LEVEL_ITEM = DODATA(lvRefresh.SelectedIndices.Item(0)).HIGHER_LEVEL_ITEM
                SAP_ITEM.dO_CDATE = DODATA(lvRefresh.SelectedIndices.Item(0)).DO_CDATE
                SAP_ITEM.dO_CTIME = DODATA(lvRefresh.SelectedIndices.Item(0)).DO_CTIME
                SAP_ITEM.sD_DOC_CAT = DODATA(lvRefresh.SelectedIndices.Item(0)).SD_DOC_CAT
                SAP_ITEM.sALES_ORG = DODATA(lvRefresh.SelectedIndices.Item(0)).SALES_ORG
                SAP_ITEM.dIS_CHAN = DODATA(lvRefresh.SelectedIndices.Item(0)).DIS_CHAN
                SAP_ITEM.dIVISION = DODATA(lvRefresh.SelectedIndices.Item(0)).DIVISION
                SAP_ITEM.sHIP_POINT = DODATA(lvRefresh.SelectedIndices.Item(0)).SHIP_POINT
                SAP_ITEM.dOC_DATE = DODATA(lvRefresh.SelectedIndices.Item(0)).DOC_DATE
                SAP_ITEM.pLAN_GI_DATE = DODATA(lvRefresh.SelectedIndices.Item(0)).PLAN_GI_DATE
                SAP_ITEM.lOAD_DATE = DODATA(lvRefresh.SelectedIndices.Item(0)).LOAD_DATE
                SAP_ITEM.tRN_PLAN_DATE = DODATA(lvRefresh.SelectedIndices.Item(0)).TRN_PLAN_DATE
                SAP_ITEM.dELV_DATE = DODATA(lvRefresh.SelectedIndices.Item(0)).DELV_DATE
                SAP_ITEM.pICK_DATE = DODATA(lvRefresh.SelectedIndices.Item(0)).PICK_DATE
                SAP_ITEM.sHIP_TO_PARTY = DODATA(lvRefresh.SelectedIndices.Item(0)).SHIP_TO_PARTY
                SAP_ITEM.sOLD_TO_PARTY = DODATA(lvRefresh.SelectedIndices.Item(0)).SOLD_TO_PARTY
                SAP_ITEM.tOTAL_WEIGHT = DODATA(lvRefresh.SelectedIndices.Item(0)).TOTAL_WEIGHT
                SAP_ITEM.mAT_CODE = DODATA(lvRefresh.SelectedIndices.Item(0)).MAT_CODE
                SAP_ITEM.pLANT = DODATA(lvRefresh.SelectedIndices.Item(0)).PLANT
                SAP_ITEM.sTORAGE_LOC = DODATA(lvRefresh.SelectedIndices.Item(0)).STORAGE_LOC
                '  SAP_ITEM.BATCH_NO = DODATA(lvRefresh.SelectedIndices.Item(0)).BATCH_NO
                SAP_ITEM.aCT_QTY_DELV_SU = DODATA(lvRefresh.SelectedIndices.Item(0)).ACT_QTY_DELV_SU
                SAP_ITEM.bASE_UOM = DODATA(lvRefresh.SelectedIndices.Item(0)).BASE_UOM
                SAP_ITEM.sALES_UNIT = DODATA(lvRefresh.SelectedIndices.Item(0)).SALES_UNIT
                SAP_ITEM.nET_WEIGHT = DODATA(lvRefresh.SelectedIndices.Item(0)).NET_WEIGHT
                SAP_ITEM.gROSS_WEIGHT = DODATA(lvRefresh.SelectedIndices.Item(0)).GROSS_WEIGHT
                SAP_ITEM.wEIGHT_UNIT = DODATA(lvRefresh.SelectedIndices.Item(0)).WEIGHT_UNIT
                SAP_ITEM.vOLUME = DODATA(lvRefresh.SelectedIndices.Item(0)).VOLUME
                SAP_ITEM.vOLUME_UNIT = DODATA(lvRefresh.SelectedIndices.Item(0)).VOLUME_UNIT
                SAP_ITEM.oVR_DELV_TLIMIT = DODATA(lvRefresh.SelectedIndices.Item(0)).OVR_DELV_TLIMIT
                SAP_ITEM.uND_DELV_TLIMIT = DODATA(lvRefresh.SelectedIndices.Item(0)).UND_DELV_TLIMIT
                SAP_ITEM.bATCH_INDICATOR = DODATA(lvRefresh.SelectedIndices.Item(0)).BATCH_INDICATOR
                SAP_ITEM.dELIVERY_INSTRUCTION_TEXT = DODATA(lvRefresh.SelectedIndices.Item(0)).DELIVERY_INSTRUCTION_TEXT
            Else
                IndexDO = 0
                For i = 0 To DODATA.Length - 1
                    If (DODATA(lvRefresh.SelectedIndices.Item(0)).DO_NO = DODATA(i).DO_NO) And
                    (DODATA(i).DO_ITEM = DODATA(lvRefresh.SelectedIndices.Item(0)).HIGHER_LEVEL_ITEM) Then
                        IndexDO = i
                        Exit For
                    End If
                Next

                SAP_ITEM.BT_RETURN = 1
                SAP_ITEM.dO_NO = DODATA(IndexDO).DO_NO
                SAP_ITEM.dO_ITEM = DODATA(IndexDO).DO_ITEM
                SAP_ITEM.sO_NO = DODATA(IndexDO).SO_NO
                SAP_ITEM.sO_ITEM = DODATA(IndexDO).SO_ITEM
                SAP_ITEM.hIGHER_LEVEL_ITEM = DODATA(IndexDO).HIGHER_LEVEL_ITEM
                SAP_ITEM.dO_CDATE = DODATA(IndexDO).DO_CDATE
                SAP_ITEM.dO_CTIME = DODATA(IndexDO).DO_CTIME
                SAP_ITEM.sD_DOC_CAT = DODATA(IndexDO).SD_DOC_CAT
                SAP_ITEM.sALES_ORG = DODATA(IndexDO).SALES_ORG
                SAP_ITEM.dIS_CHAN = DODATA(IndexDO).DIS_CHAN
                SAP_ITEM.dIVISION = DODATA(IndexDO).DIVISION
                SAP_ITEM.sHIP_POINT = DODATA(IndexDO).SHIP_POINT
                SAP_ITEM.dOC_DATE = DODATA(IndexDO).DOC_DATE
                SAP_ITEM.pLAN_GI_DATE = DODATA(IndexDO).PLAN_GI_DATE
                SAP_ITEM.lOAD_DATE = DODATA(IndexDO).LOAD_DATE
                SAP_ITEM.tRN_PLAN_DATE = DODATA(IndexDO).TRN_PLAN_DATE
                SAP_ITEM.dELV_DATE = DODATA(IndexDO).DELV_DATE
                SAP_ITEM.pICK_DATE = DODATA(IndexDO).PICK_DATE
                SAP_ITEM.sHIP_TO_PARTY = DODATA(IndexDO).SHIP_TO_PARTY
                SAP_ITEM.sOLD_TO_PARTY = DODATA(IndexDO).SOLD_TO_PARTY
                SAP_ITEM.tOTAL_WEIGHT = DODATA(IndexDO).TOTAL_WEIGHT
                SAP_ITEM.mAT_CODE = DODATA(IndexDO).MAT_CODE
                SAP_ITEM.pLANT = DODATA(IndexDO).PLANT
                SAP_ITEM.sTORAGE_LOC = DODATA(IndexDO).STORAGE_LOC
                SAP_ITEM.bATCH_NO = DODATA(IndexDO).BATCH_NO
                SAP_ITEM.aCT_QTY_DELV_SU = DODATA(IndexDO).ACT_QTY_DELV_SU
                SAP_ITEM.bASE_UOM = DODATA(IndexDO).BASE_UOM
                SAP_ITEM.sALES_UNIT = DODATA(IndexDO).SALES_UNIT
                SAP_ITEM.nET_WEIGHT = DODATA(IndexDO).NET_WEIGHT
                SAP_ITEM.gROSS_WEIGHT = DODATA(IndexDO).GROSS_WEIGHT
                SAP_ITEM.wEIGHT_UNIT = DODATA(IndexDO).WEIGHT_UNIT
                SAP_ITEM.vOLUME = DODATA(IndexDO).VOLUME
                SAP_ITEM.vOLUME_UNIT = DODATA(IndexDO).VOLUME_UNIT
                SAP_ITEM.oVR_DELV_TLIMIT = DODATA(IndexDO).OVR_DELV_TLIMIT
                SAP_ITEM.uND_DELV_TLIMIT = DODATA(IndexDO).UND_DELV_TLIMIT
                SAP_ITEM.bATCH_INDICATOR = DODATA(IndexDO).BATCH_INDICATOR
                SAP_ITEM.dELIVERY_INSTRUCTION_TEXT = DODATA(IndexDO).DELIVERY_INSTRUCTION_TEXT

            End If


            For i = 0 To SAP_ALLITEM.Length - 1
                If SAP_ALLITEM(i).aCT_QTY_DELV_SU = 0 Then
                    For l = 0 To SAP_BATCH_ITEM.Length - 1
                        If SAP_BATCH_ITEM(l).HIGHER_LEVEL_ITEM = SAP_ALLITEM(i).dO_ITEM Then
                            SAP_ALLITEM(i).aCT_QTY_DELV_SU += Convert.ToDouble(SAP_BATCH_ITEM(l).QTY_DELV)
                        End If
                    Next
                End If
            Next



            lvRefresh.SelectedIndices.Clear()
            Label4.Text = SAP_ITEM.dO_NO


        Catch ex As Exception
            SAP_ITEM.BT_RETURN = 0
            If lvRefresh.SelectedIndices.Count <= 0 Then
                MsgBox("Please select one record", MsgBoxStyle.Critical, "Error")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
            End If
            Exit Sub
            'For delete
            ' SAP_ITEM.BT_RETURN = 1
            '  SAP_ITEM.DO_NO = "123"
            '  SAP_ITEM.BATCH_NO = "444"
            '  SAP_ITEM.DIVISION = "D1"
            ' MessageBox.Show(ex.Message, )
        End Try
        Close()
    End Sub

    Private Sub BtCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtCancel.Click
        Try
            SAP_ITEM = Nothing
            SAP_ALLITEM = Nothing
            SAP_BATCH_ITEM = Nothing
            SAP_ITEM.BT_RETURN = 0
            For i = 0 To SAP_ALLITEM.Length - 1
                SAP_ALLITEM(i).BT_RETURN = 0
            Next
            lvRefresh.SelectedIndices.Clear()
        Catch ex As Exception

        End Try
        Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtPost.Click
        Dim _PostDOData(0) As Create_Shipment_DO03_Out_DTITEM
        Dim _PostShipData(0) As Create_Shipment_SHIP03_Out_DTITEM

        _PostDOData(0) = New Create_Shipment_DO03_Out_DTITEM
        _PostShipData(0) = New Create_Shipment_SHIP03_Out_DTITEM

        _PostDOData(0).DO_NO = SAP_ITEM.dO_NO
        _PostDOData(0).DO_ITEM = SAP_ITEM.dO_ITEM
        _PostDOData(0).SALES_ORG = SAP_ITEM.sALES_ORG
        _PostDOData(0).SD_DOC_CAT = SAP_ITEM.sD_DOC_CAT
        _PostDOData(0).ACT_GI_DATE = Convert.ToDateTime("10/07/2011").ToString("dd/MM/yyyy") ''''''''''''''มั่ว''''''''''''''''''''
        _PostDOData(0).MAT_CODE = SAP_ITEM.mAT_CODE
        _PostDOData(0).PLANT = SAP_ITEM.pLANT
        _PostDOData(0).STORAGE_LOC = SAP_ITEM.sTORAGE_LOC
        _PostDOData(0).BATCH_NO = SAP_ITEM.bATCH_NO
        _PostDOData(0).ACT_QTY_DELV_SU = SAP_ITEM.aCT_QTY_DELV_SU
        _PostDOData(0).BASE_UOM = SAP_ITEM.bASE_UOM
        _PostDOData(0).SALES_UNIT = SAP_ITEM.sALES_UNIT
        _PostDOData(0).NET_WEIGHT = SAP_ITEM.nET_WEIGHT
        _PostDOData(0).GROSS_WEIGHT = SAP_ITEM.gROSS_WEIGHT
        _PostDOData(0).WEIGHT_UNIT = SAP_ITEM.wEIGHT_UNIT
        _PostDOData(0).ACT_QTY_DELV_SKU = SAP_ITEM.aCT_QTY_DELV_SU     '''''''''''''''''มั่ว'''''''''''''''''


        _PostShipData(0).SHIP_NO = "" '''''''''''''''''''''''''''
        _PostShipData(0).SHIP_TYPE = "ZT02" '''''''''''''''''''''''''''
        _PostShipData(0).TRN_PLAN_POINT = SAP_ITEM.sALES_ORG ''''''''''''''''''''
        _PostShipData(0).CREATE_DATE = Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
        _PostShipData(0).CREATE_TIME = Convert.ToDateTime("11:11:11").ToString("HH:mm:ss")
        _PostShipData(0).CONTAINER = "MEDU3173390"
        _PostShipData(0).DRIVER_NAME = "BEE"
        _PostShipData(0).CAR_LINCENSE = "22442091"
        _PostShipData(0).FORWD_AGENT = "0004000009"
        _PostShipData(0).WEIGHT_IN = "10000"
        _PostShipData(0).WEIGHT_OUT = "50000"
        _PostShipData(0).SEAL_NO = "19888"
        _PostShipData(0).SHIPP_TYPE = ""
        _PostShipData(0).SHIPP_COND = ""
        _PostShipData(0).DO_NO = SAP_ITEM.dO_NO
        '   _PostShipData(0).ACT_LOAD_SDATE = Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
        '  _PostShipData(0).ACT_LOAD_STIME = Convert.ToDateTime("11:11:11").ToString("HH:mm:ss")
        '   _PostShipData(0).ACT_LOAD_EDATE = Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
        '   _PostShipData(0).ACT_LOAD_ETIME = Convert.ToDateTime("11:11:11").ToString("HH:mm:ss")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '  FSAP03.POST_DO_SHIP(_PostDOData, _PostShipData)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FSAP03.Show()
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ReDim Preserve DODATA(4)
        DODATA(0) = New Extract_DO_DO02_DTITEM
        DODATA(1) = New Extract_DO_DO02_DTITEM
        DODATA(2) = New Extract_DO_DO02_DTITEM
        DODATA(3) = New Extract_DO_DO02_DTITEM
        DODATA(4) = New Extract_DO_DO02_DTITEM

        DODATA(0).DO_NO = "162000010"
        DODATA(1).DO_NO = "162000010"
        DODATA(2).DO_NO = "162000010"
        DODATA(3).DO_NO = "162000010"
        DODATA(4).DO_NO = "162000010"

        DODATA(0).DO_ITEM = "000010"
        DODATA(1).DO_ITEM = "900001"
        DODATA(2).DO_ITEM = "900002"
        DODATA(3).DO_ITEM = "000020"
        DODATA(4).DO_ITEM = "900003"

        DODATA(0).HIGHER_LEVEL_ITEM = "000000"
        DODATA(1).HIGHER_LEVEL_ITEM = "000010"
        DODATA(2).HIGHER_LEVEL_ITEM = "000010"
        DODATA(3).HIGHER_LEVEL_ITEM = "000000"
        DODATA(4).HIGHER_LEVEL_ITEM = "000020"
        AddItems(lvRefresh, DODATA)
    End Sub

    Private Sub FSAP02_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        SHIndex = CbShipper.SelectedIndex
    End Sub

    Private Sub FSAP02_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        CbShipper.SelectedIndex = SHIndex
        CbShipper.Focus()
    End Sub
End Class
