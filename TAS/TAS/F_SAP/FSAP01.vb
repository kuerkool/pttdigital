﻿Imports System.Net
Imports TAS_TOL.SAP01
Imports System.ComponentModel
Imports System.Threading
Imports TAS_TOL.C01_Permission

Public Class FSAP01
    Friend WithEvents ThrRefresh As BackgroundWorker
    Dim MResponse As New Material_Customer_Master_Return_DT
    Private Sub FRefresh()
        ThrRefresh = New BackgroundWorker
        ThrRefresh.WorkerSupportsCancellation = True
        AddHandler ThrRefresh.DoWork, AddressOf RefreshData
        ThrRefresh.RunWorkerAsync()
    End Sub
    Public Sub StartMaster()
        Try
            treeManu.BtUPMaster.Enabled = False
            MResponse = Nothing
            Me.FPTDataSet.T_STO.RejectChanges()
            Me.FPTDataSet.T_LO.RejectChanges()
            Me.FPTDataSet.T_SHIPPER.RejectChanges()
            Me.FPTDataSet.T_Product.RejectChanges()
            Me.FPTDataSet.T_Customer.RejectChanges()
            Me.FPTDataSet.T_COMPANY.RejectChanges()
            TCOMPANYBindingSource.CancelEdit()
            TCustomerBindingSource.CancelEdit()
            TProductBindingSource.CancelEdit()
            TSHIPPERBindingSource.CancelEdit()
            TLOBindingSource.CancelEdit()
            TSTOBindingSource.CancelEdit()

            'TODO: This line of code loads data into the 'FPTDataSet.T_STO' table. You can move, or remove it, as needed.
            Me.T_STOTableAdapter.Fill(Me.FPTDataSet.T_STO)
            'TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
            Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)

            'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
            Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
            'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
            Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
            'TODO: This line of code loads data into the 'FPTDataSet.T_Customer' table. You can move, or remove it, as needed.
            Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
            'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
            Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
            ' Cursor = Cursors.WaitCursor
            Dim MRequest As String = ""
            Dim myproxy As New SAP01.Material_Customer_Master_Sync_Out_SIService

            Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
            Dim netCred As NetworkCredential = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            Dim uri As Uri = New Uri(objIniFile.GetString("Webservice", "URL01", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING01"))
            Dim credentials As ICredentials = netCred.GetCredential(uri, "Basic")
            myproxy.Url = uri.AbsoluteUri
            myproxy.Credentials = credentials
            myproxy.PreAuthenticate = True
            '   myproxy.Credentials = credentials
            AddHandler myproxy.Material_Customer_Master_Sync_Out_SICompleted, AddressOf GetMasterData
            '   TSLWS.Enabled = False
            myproxy.Material_Customer_Master_Sync_Out_SIAsync(MRequest)
        Catch ex As Exception
            ' Cursor = Cursors.Default
        End Try
        treeManu.BtUPMaster.Enabled = True
    End Sub


    Private Sub RefreshData()
        Try
            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()
            Dim cmd As New SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandText = "Update T_PRODUCT Set Update_date = null where Product_number is not null"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "Update T_COMPANY Set Update_date = null where COMPANY_NUMBER is not null"
            cmd.ExecuteNonQuery()

            Dim Max_loop As Integer = Math.Max(MResponse.FWDAGENT.Length, MResponse.CUSTOMER.Length)
            Max_loop = Math.Max(Max_loop, MResponse.MATERIAL.Length)
            Max_loop = Math.Max(Max_loop, MResponse.PLNT.Length)
            Max_loop = Math.Max(Max_loop, MResponse.PLNT_MAP.Length)
            Max_loop = Math.Max(Max_loop, MResponse.SALESORG.Length)
            Max_loop = Math.Max(Max_loop, MResponse.LOADPT.Length)

            For i = 0 To Max_loop
                RadWaitingBar1.Text = (i / Max_loop * 100).ToString("0.00") & " %"
                ' RadWaitingBar1.Text = Math.Round(i / Max_loop * 100)
                'Application.DoEvents()

                ' Update Forword agent 
                Try
                    If i < MResponse.FWDAGENT.Length And cbforw.Checked Then
                        If TCOMPANYBindingSource.Find("COMPANY_CODE", MResponse.FWDAGENT(i).VEND_CODE) >= 0 Then
                            Dim Position As Integer = TCOMPANYBindingSource.Find("COMPANY_CODE", MResponse.FWDAGENT(i).VEND_CODE)
                            '   TCOMPANYBindingSource.Item(Position)("COMPANY_DATE") = Now
                            TCOMPANYBindingSource.Item(Position)("COMPANY_number") = MResponse.FWDAGENT(i).VEND_CODE
                            TCOMPANYBindingSource.Item(Position)("COMPANY_CODE") = MResponse.FWDAGENT(i).VEND_CODE
                            TCOMPANYBindingSource.Item(Position)("COMPANY_NAME") = MResponse.FWDAGENT(i).NAME1
                            TCOMPANYBindingSource.Item(Position)("COMPANY_ADDRESS") = MResponse.FWDAGENT(i).STREET & " " & MResponse.FWDAGENT(i).STREET2 & " " & MResponse.FWDAGENT(i).STREET3 & " " & MResponse.FWDAGENT(i).STREET4
                            TCOMPANYBindingSource.Item(Position)("COMPANY_province") = MResponse.FWDAGENT(i).CITY
                            TCOMPANYBindingSource.Item(Position)("COMPANY_Zipcode") = MResponse.FWDAGENT(i).POST_CODE
                            TCOMPANYBindingSource.Item(Position)("COMPANY_Tel") = MResponse.FWDAGENT(i).TEL_NO
                            TCOMPANYBindingSource.Item(Position)("COMPANY_fax") = MResponse.FWDAGENT(i).FAX_NO
                            TCOMPANYBindingSource.Item(Position)("COMPANY_BLACK_LIST") = "NO"
                            TCOMPANYBindingSource.Item(Position)("Update_date") = Now
                            TCOMPANYBindingSource.Item(Position)("ACC_GROUP") = MResponse.FWDAGENT(i).ACC_GROUP
                            TCOMPANYBindingSource.Item(Position)("CITY") = MResponse.FWDAGENT(i).CITY
                            TCOMPANYBindingSource.Item(Position)("COUNTRY") = MResponse.FWDAGENT(i).COUNTRY
                            TCOMPANYBindingSource.Item(Position)("FAX_NO") = MResponse.FWDAGENT(i).FAX_NO
                            TCOMPANYBindingSource.Item(Position)("HOUSE_NO1") = MResponse.FWDAGENT(i).HOUSE_NO1
                            TCOMPANYBindingSource.Item(Position)("HOUSE_NO2") = MResponse.FWDAGENT(i).HOUSE_NO2
                            TCOMPANYBindingSource.Item(Position)("LANGUAGE") = MResponse.FWDAGENT(i).LANGUAGE
                            TCOMPANYBindingSource.Item(Position)("LOCATION") = MResponse.FWDAGENT(i).LOCATION
                            TCOMPANYBindingSource.Item(Position)("NAME1") = MResponse.FWDAGENT(i).NAME1
                            TCOMPANYBindingSource.Item(Position)("NAME2") = MResponse.FWDAGENT(i).NAME2
                            TCOMPANYBindingSource.Item(Position)("NAME3") = MResponse.FWDAGENT(i).NAME3
                            TCOMPANYBindingSource.Item(Position)("NAME4") = MResponse.FWDAGENT(i).NAME4
                            TCOMPANYBindingSource.Item(Position)("POST_CODE") = MResponse.FWDAGENT(i).POST_CODE
                            TCOMPANYBindingSource.Item(Position)("REGION") = MResponse.FWDAGENT(i).REGION
                            TCOMPANYBindingSource.Item(Position)("SEARCH_TERM") = MResponse.FWDAGENT(i).SEARCH_TERM
                            TCOMPANYBindingSource.Item(Position)("STREET") = MResponse.FWDAGENT(i).STREET
                            TCOMPANYBindingSource.Item(Position)("STREET2") = MResponse.FWDAGENT(i).STREET2
                            TCOMPANYBindingSource.Item(Position)("STREET3") = MResponse.FWDAGENT(i).STREET3
                            TCOMPANYBindingSource.Item(Position)("STREET4") = MResponse.FWDAGENT(i).STREET4
                            TCOMPANYBindingSource.Item(Position)("TEL_NO") = MResponse.FWDAGENT(i).TEL_NO
                            TCOMPANYBindingSource.Item(Position)("TIME_ZONE") = MResponse.FWDAGENT(i).TIME_ZONE
                            TCOMPANYBindingSource.Item(Position)("TRANSP_ZONE") = MResponse.FWDAGENT(i).TRANSP_ZONE
                            TCOMPANYBindingSource.Item(Position)("VEND_CODE") = MResponse.FWDAGENT(i).VEND_CODE

                        Else
                            TCOMPANYBindingSource.AddNew()
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_DATE") = Now
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_number") = MResponse.FWDAGENT(i).VEND_CODE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_CODE") = MResponse.FWDAGENT(i).VEND_CODE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_NAME") = MResponse.FWDAGENT(i).NAME1
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_ADDRESS") = MResponse.FWDAGENT(i).STREET & " " & MResponse.FWDAGENT(i).STREET2 & " " & MResponse.FWDAGENT(i).STREET3 & " " & MResponse.FWDAGENT(i).STREET4
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_province") = MResponse.FWDAGENT(i).CITY
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_Zipcode") = MResponse.FWDAGENT(i).POST_CODE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_Tel") = MResponse.FWDAGENT(i).TEL_NO
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_fax") = MResponse.FWDAGENT(i).FAX_NO
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_BLACK_LIST") = "NO"
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("Update_date") = Now
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("ACC_GROUP") = MResponse.FWDAGENT(i).ACC_GROUP
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("CITY") = MResponse.FWDAGENT(i).CITY
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COUNTRY") = MResponse.FWDAGENT(i).COUNTRY
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("FAX_NO") = MResponse.FWDAGENT(i).FAX_NO
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("HOUSE_NO1") = MResponse.FWDAGENT(i).HOUSE_NO1
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("HOUSE_NO2") = MResponse.FWDAGENT(i).HOUSE_NO2
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("LANGUAGE") = MResponse.FWDAGENT(i).LANGUAGE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("LOCATION") = MResponse.FWDAGENT(i).LOCATION
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("NAME1") = MResponse.FWDAGENT(i).NAME1
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("NAME2") = MResponse.FWDAGENT(i).NAME2
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("NAME3") = MResponse.FWDAGENT(i).NAME3
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("NAME4") = MResponse.FWDAGENT(i).NAME4
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("POST_CODE") = MResponse.FWDAGENT(i).POST_CODE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("REGION") = MResponse.FWDAGENT(i).REGION
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("SEARCH_TERM") = MResponse.FWDAGENT(i).SEARCH_TERM
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("STREET") = MResponse.FWDAGENT(i).STREET
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("STREET2") = MResponse.FWDAGENT(i).STREET2
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("STREET3") = MResponse.FWDAGENT(i).STREET3
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("STREET4") = MResponse.FWDAGENT(i).STREET4
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("TEL_NO") = MResponse.FWDAGENT(i).TEL_NO
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("TIME_ZONE") = MResponse.FWDAGENT(i).TIME_ZONE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("TRANSP_ZONE") = MResponse.FWDAGENT(i).TRANSP_ZONE
                            TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("VEND_CODE") = MResponse.FWDAGENT(i).VEND_CODE




                        End If
                        TCOMPANYBindingSource.EndEdit()
                        T_COMPANYTableAdapter.Update(FPTDataSet.Tables("T_COMPANY"))
                        FPTDataSet.Tables("T_COMPANY").AcceptChanges()
                        FPTDataSet.Tables("T_COMPANY").RejectChanges()
                    End If

                Catch ex As Exception
                    TCOMPANYBindingSource.CancelEdit()
                    FPTDataSet.Tables("T_COMPANY").RejectChanges()
                End Try
                '''''
                '   Update Customer
                Try

                    If i < MResponse.CUSTOMER.Length And cbcus.Checked Then


                        'If MResponse.CUSTOMER(i).CUS_CODE = "0001000010" Then
                        '    MessageBox.Show(MResponse.CUSTOMER(i).CUS_CODE)
                        'End If
                        If TCustomerBindingSource.Find("Customer_number", MResponse.CUSTOMER(i).CUS_CODE & " | " & MResponse.CUSTOMER(i).SALES_ORG) >= 0 Then
                            Dim Position As Integer = TCustomerBindingSource.Find("Customer_number", MResponse.CUSTOMER(i).CUS_CODE & " | " & MResponse.CUSTOMER(i).SALES_ORG)
                            '   TCustomerBindingSource.Item(Position)("Customer_Date") = Now
                            TCustomerBindingSource.Item(Position)("Customer_number") = MResponse.CUSTOMER(i).CUS_CODE & " | " & MResponse.CUSTOMER(i).SALES_ORG
                            TCustomerBindingSource.Item(Position)("Customer_code") = MResponse.CUSTOMER(i).NAME1
                            TCustomerBindingSource.Item(Position)("Customer_name") = MResponse.CUSTOMER(i).NAME1 & " " & MResponse.CUSTOMER(i).NAME2 & " "
                            ' & MResponse.CUSTOMER(i).NAME3 & " " & MResponse.CUSTOMER(i).NAME4
                            TCustomerBindingSource.Item(Position)("Customer_address") = MResponse.CUSTOMER(i).STREET & " " & MResponse.CUSTOMER(i).STREET2 & " " & MResponse.CUSTOMER(i).STREET3 & " " & MResponse.CUSTOMER(i).STREET4
                            TCustomerBindingSource.Item(Position)("Customer_province") = MResponse.CUSTOMER(i).CITY
                            TCustomerBindingSource.Item(Position)("Customer_Zipcode") = MResponse.CUSTOMER(i).POST_CODE
                            TCustomerBindingSource.Item(Position)("Customer_tel") = MResponse.CUSTOMER(i).TEL_NO
                            TCustomerBindingSource.Item(Position)("Customer_fax") = MResponse.CUSTOMER(i).FAX_NO
                            TCustomerBindingSource.Item(Position)("Customer_black_list") = "NO"
                            TCustomerBindingSource.Item(Position)("Update_date") = Now
                            TCustomerBindingSource.Item(Position)("SORG_CODE") = MResponse.CUSTOMER(i).SALES_ORG
                            TCustomerBindingSource.Item(Position)("CUS_CODE") = MResponse.CUSTOMER(i).CUS_CODE

                            TCustomerBindingSource.Item(Position)("ACC_GROUP") = MResponse.CUSTOMER(i).ACC_GROUP
                            TCustomerBindingSource.Item(Position)("CITY") = MResponse.CUSTOMER(i).CITY
                            TCustomerBindingSource.Item(Position)("COUNTRY") = MResponse.CUSTOMER(i).COUNTRY
                            TCustomerBindingSource.Item(Position)("CUS_CODE") = MResponse.CUSTOMER(i).CUS_CODE
                            TCustomerBindingSource.Item(Position)("FAX_NO") = MResponse.CUSTOMER(i).FAX_NO
                            TCustomerBindingSource.Item(Position)("HOUSE_NO1") = MResponse.CUSTOMER(i).HOUSE_NO1
                            TCustomerBindingSource.Item(Position)("HOUSE_NO2") = MResponse.CUSTOMER(i).HOUSE_NO2
                            TCustomerBindingSource.Item(Position)("LANGUAGE") = MResponse.CUSTOMER(i).LANGUAGE
                            TCustomerBindingSource.Item(Position)("LOCATION") = MResponse.CUSTOMER(i).LOCATION
                            TCustomerBindingSource.Item(Position)("NAME1") = MResponse.CUSTOMER(i).NAME1
                            TCustomerBindingSource.Item(Position)("NAME2") = MResponse.CUSTOMER(i).NAME2
                            TCustomerBindingSource.Item(Position)("NAME3") = MResponse.CUSTOMER(i).NAME3
                            TCustomerBindingSource.Item(Position)("NAME4") = MResponse.CUSTOMER(i).NAME4
                            TCustomerBindingSource.Item(Position)("POST_CODE") = MResponse.CUSTOMER(i).POST_CODE
                            TCustomerBindingSource.Item(Position)("REGION") = MResponse.CUSTOMER(i).REGION
                            TCustomerBindingSource.Item(Position)("SALES_ORG") = MResponse.CUSTOMER(i).SALES_ORG
                            TCustomerBindingSource.Item(Position)("SEARCH_TERM") = MResponse.CUSTOMER(i).SEARCH_TERM
                            TCustomerBindingSource.Item(Position)("STREET") = MResponse.CUSTOMER(i).STREET
                            TCustomerBindingSource.Item(Position)("STREET2") = MResponse.CUSTOMER(i).STREET2
                            TCustomerBindingSource.Item(Position)("STREET3") = MResponse.CUSTOMER(i).STREET3
                            TCustomerBindingSource.Item(Position)("STREET4") = MResponse.CUSTOMER(i).STREET4
                            TCustomerBindingSource.Item(Position)("TEL_NO") = MResponse.CUSTOMER(i).TEL_NO
                            TCustomerBindingSource.Item(Position)("TIME_ZONE") = MResponse.CUSTOMER(i).TIME_ZONE
                            TCustomerBindingSource.Item(Position)("TRANSP_ZONE") = MResponse.CUSTOMER(i).TRANSP_ZONE

                        Else

                            TCustomerBindingSource.AddNew()
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_Date") = Now
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_number") = MResponse.CUSTOMER(i).CUS_CODE & " | " & MResponse.CUSTOMER(i).SALES_ORG
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_code") = MResponse.CUSTOMER(i).NAME1
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_name") = MResponse.CUSTOMER(i).NAME1 & " " & MResponse.CUSTOMER(i).NAME2 & " " & MResponse.CUSTOMER(i).NAME3 & " " & MResponse.CUSTOMER(i).NAME4
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_address") = MResponse.CUSTOMER(i).STREET & " " & MResponse.CUSTOMER(i).STREET2 & " " & MResponse.CUSTOMER(i).STREET3 & " " & MResponse.CUSTOMER(i).STREET4
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_province") = MResponse.CUSTOMER(i).CITY
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_Zipcode") = MResponse.CUSTOMER(i).POST_CODE
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_tel") = MResponse.CUSTOMER(i).TEL_NO
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_fax") = MResponse.CUSTOMER(i).FAX_NO
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_black_list") = "NO"
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Update_date") = Now
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("SORG_CODE") = MResponse.CUSTOMER(i).SALES_ORG
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("CUS_CODE") = MResponse.CUSTOMER(i).CUS_CODE

                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("ACC_GROUP") = MResponse.CUSTOMER(i).ACC_GROUP
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("CITY") = MResponse.CUSTOMER(i).CITY
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("COUNTRY") = MResponse.CUSTOMER(i).COUNTRY
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("CUS_CODE") = MResponse.CUSTOMER(i).CUS_CODE
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("FAX_NO") = MResponse.CUSTOMER(i).FAX_NO
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("HOUSE_NO1") = MResponse.CUSTOMER(i).HOUSE_NO1
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("HOUSE_NO2") = MResponse.CUSTOMER(i).HOUSE_NO2
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("LANGUAGE") = MResponse.CUSTOMER(i).LANGUAGE
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("LOCATION") = MResponse.CUSTOMER(i).LOCATION
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("NAME1") = MResponse.CUSTOMER(i).NAME1
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("NAME2") = MResponse.CUSTOMER(i).NAME2
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("NAME3") = MResponse.CUSTOMER(i).NAME3
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("NAME4") = MResponse.CUSTOMER(i).NAME4
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("POST_CODE") = MResponse.CUSTOMER(i).POST_CODE
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("REGION") = MResponse.CUSTOMER(i).REGION
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("SALES_ORG") = MResponse.CUSTOMER(i).SALES_ORG
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("SEARCH_TERM") = MResponse.CUSTOMER(i).SEARCH_TERM
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("STREET") = MResponse.CUSTOMER(i).STREET
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("STREET2") = MResponse.CUSTOMER(i).STREET2
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("STREET3") = MResponse.CUSTOMER(i).STREET3
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("STREET4") = MResponse.CUSTOMER(i).STREET4
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("TEL_NO") = MResponse.CUSTOMER(i).TEL_NO
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("TIME_ZONE") = MResponse.CUSTOMER(i).TIME_ZONE
                            TCustomerBindingSource.Item(TCustomerBindingSource.Position)("TRANSP_ZONE") = MResponse.CUSTOMER(i).TRANSP_ZONE


                        End If
                        TCustomerBindingSource.EndEdit()
                        T_CustomerTableAdapter.Update(FPTDataSet.Tables("T_CUSTOMER"))
                        FPTDataSet.Tables("T_CUSTOMER").AcceptChanges()
                        FPTDataSet.Tables("T_CUSTOMER").RejectChanges()
                    End If

                Catch ex As Exception
                    TCustomerBindingSource.CancelEdit()
                    FPTDataSet.Tables("T_CUSTOMER").RejectChanges()
                End Try

                '''''
                ' Update mat code Product code
                Try

                    If i < MResponse.MATERIAL.Length And cbmat.Checked Then
                        If TProductBindingSource.Find("Product_code", MResponse.MATERIAL(i).MAT_CODE) >= 0 Then
                            Dim Position As Integer = TProductBindingSource.Find("Product_code", MResponse.MATERIAL(i).MAT_CODE)
                            '   TProductBindingSource.Item(Position)("Product_Date") = Now
                            TProductBindingSource.Item(Position)("Product_code") = MResponse.MATERIAL(i).MAT_CODE
                            TProductBindingSource.Item(Position)("Product_number") = MResponse.MATERIAL(i).MAT_CODE
                            TProductBindingSource.Item(Position)("Product_name") = MResponse.MATERIAL(i).MAT_DESC
                            TProductBindingSource.Item(Position)("SALES_ORG") = MResponse.MATERIAL(i).SALES_ORG
                            TProductBindingSource.Item(Position)("MAT_TYPE") = MResponse.MATERIAL(i).MAT_TYPE
                            TProductBindingSource.Item(Position)("Product_Blendingpercen") = 0
                            TProductBindingSource.Item(Position)("Product_Type") = 1
                            TProductBindingSource.Item(Position)("Product_Type") = 1
                            TProductBindingSource.Item(Position)("Update_date") = Now
                        Else
                            TProductBindingSource.AddNew()
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_Date") = Now
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_code") = MResponse.MATERIAL(i).MAT_CODE
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_number") = MResponse.MATERIAL(i).MAT_CODE
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_name") = MResponse.MATERIAL(i).MAT_DESC
                            TProductBindingSource.Item(TProductBindingSource.Position)("SALES_ORG") = MResponse.MATERIAL(i).SALES_ORG
                            TProductBindingSource.Item(TProductBindingSource.Position)("MAT_TYPE") = MResponse.MATERIAL(i).MAT_TYPE
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_Blendingpercen") = 0
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = 1
                            TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = 1
                            TProductBindingSource.Item(TProductBindingSource.Position)("Update_date") = Now
                        End If
                        TProductBindingSource.EndEdit()
                        T_ProductTableAdapter.Update(FPTDataSet.Tables("T_Product"))
                        FPTDataSet.Tables("T_Product").AcceptChanges()
                        FPTDataSet.Tables("T_Product").RejectChanges()

                    End If

                Catch ex As Exception
                    TProductBindingSource.CancelEdit()
                    FPTDataSet.Tables("T_Product").RejectChanges()
                End Try
                '''''
                ' Update sales org
                Try

                    If i < MResponse.SALESORG.Length And cborg.Checked Then
                        If TSHIPPERBindingSource.Find("SP_Code", MResponse.SALESORG(i).SORG_CODE) >= 0 Then
                            Dim Position As Integer = TSHIPPERBindingSource.Find("SP_Code", MResponse.SALESORG(i).SORG_CODE)
                            '   TSHIPPERBindingSource.Item(Position)("SP_Date") = Now
                            TSHIPPERBindingSource.Item(Position)("SP_Code") = MResponse.SALESORG(i).SORG_CODE
                            TSHIPPERBindingSource.Item(Position)("SP_SAPCODE") = MResponse.SALESORG(i).SORG_CODE
                            TSHIPPERBindingSource.Item(Position)("SP_NameEN") = MResponse.SALESORG(i).SORG_NAME
                            TSHIPPERBindingSource.Item(Position)("Update_date") = Now

                        Else
                            TSHIPPERBindingSource.AddNew()
                            TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_Date") = Now
                            TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_Code") = MResponse.SALESORG(i).SORG_CODE
                            TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_SAPCODE") = MResponse.SALESORG(i).SORG_CODE
                            TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_NameEN") = MResponse.SALESORG(i).SORG_NAME
                            TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("Update_date") = Now
                        End If

                        TSHIPPERBindingSource.EndEdit()
                        T_SHIPPERTableAdapter.Update(FPTDataSet.Tables("T_SHIPPER"))
                        FPTDataSet.Tables("T_SHIPPER").AcceptChanges()
                        FPTDataSet.Tables("T_SHIPPER").RejectChanges()

                    End If

                Catch ex As Exception
                    FPTDataSet.Tables("T_SHIPPER").RejectChanges()
                End Try
                ''''''
                Try

                    If i < MResponse.LOADPT.Length And cbpoint.Checked Then
                        If TLOBindingSource.Find("LOAD_LOCATION", MResponse.LOADPT(i).LOAD_LOCATION) >= 0 Then
                            Dim Position As Integer = TLOBindingSource.Find("LOAD_LOCATION", MResponse.LOADPT(i).LOAD_LOCATION)
                            'TLOBindingSource.Item(Position)("LOAD_LOCATION") = MResponse.LOADPT(i).LOAD_LOCATION
                            TLOBindingSource.Item(Position)("PLANT_LOCATION") = MResponse.LOADPT(i).PLNT_LOCATION
                            TLOBindingSource.Item(Position)("PRINT_LOC_DES") = MResponse.LOADPT(i).PRINT_LOC_DES
                            'TLOBindingSource.Item(Position)("CreateBy") = Main.M_NAME.Text
                            TLOBindingSource.Item(Position)("LastUpdateBy") = Main.M_NAME.Text
                            'TLOBindingSource.Item(Position)("CreateDate") = Now
                            TLOBindingSource.Item(Position)("LastUpdateDate") = Now

                        Else
                            TLOBindingSource.AddNew()
                            TLOBindingSource.Item(TLOBindingSource.Position)("LOAD_LOCATION") = MResponse.LOADPT(i).LOAD_LOCATION
                            TLOBindingSource.Item(TLOBindingSource.Position)("PLANT_LOCATION") = MResponse.LOADPT(i).PLNT_LOCATION
                            TLOBindingSource.Item(TLOBindingSource.Position)("PRINT_LOC_DES") = MResponse.LOADPT(i).PRINT_LOC_DES
                            TLOBindingSource.Item(TLOBindingSource.Position)("CreateBy") = Main.M_NAME.Text
                            TLOBindingSource.Item(TLOBindingSource.Position)("LastUpdateBy") = Main.M_NAME.Text
                            TLOBindingSource.Item(TLOBindingSource.Position)("CreateDate") = Now
                            TLOBindingSource.Item(TLOBindingSource.Position)("LastUpdateDate") = Now
                        End If

                        TLOBindingSource.EndEdit()
                        T_LOTableAdapter.Update(FPTDataSet.Tables("T_LO"))
                        FPTDataSet.Tables("T_LO").AcceptChanges()
                        FPTDataSet.Tables("T_LO").RejectChanges()

                    End If
                Catch ex As Exception
                    FPTDataSet.Tables("T_LO").RejectChanges()
                End Try
                Try
                    If i < MResponse.PLNT_MAP.Length And cbplant.Checked Then
                        TSTOBindingSource.Filter = ""
                        TSTOBindingSource.Filter = "COMPANY='" & MResponse.PLNT_MAP(i).COMPANY & "' "
                        TSTOBindingSource.Filter += " and PLANT='" & MResponse.PLNT_MAP(i).PLANT & "' "
                        TSTOBindingSource.Filter += " and STORAGE_LOC='" & MResponse.PLNT_MAP(i).STORAGE_LOC & "' "

                        If TSTOBindingSource.Count > 0 Then

                            Dim Position As Integer = TSTOBindingSource.Find("COMPANY", MResponse.PLNT_MAP(i).COMPANY)
                            'TSTOBindingSource.Item(Position)("COMPANY") = MResponse.PLNT_MAP(i).COMPANY
                            TSTOBindingSource.Item(Position)("PLANT") = MResponse.PLNT_MAP(i).PLANT
                            TSTOBindingSource.Item(Position)("SALEORG") = MResponse.PLNT_MAP(i).SALEORG
                            TSTOBindingSource.Item(Position)("STORAGE_DES") = MResponse.PLNT_MAP(i).STORAGE_DES
                            TSTOBindingSource.Item(Position)("STORAGE_LOC") = MResponse.PLNT_MAP(i).STORAGE_LOC
                            'TSTOBindingSource.Item(Position)("CreateBy") = Main.M_NAME.Text
                            TSTOBindingSource.Item(Position)("LastUpdateBy") = Main.M_NAME.Text
                            'TSTOBindingSource.Item(Position)("CreateDate") = Now
                            TSTOBindingSource.Item(Position)("LastUpdateDate") = Now

                        Else
                            TSTOBindingSource.Filter = ""
                            TSTOBindingSource.AddNew()
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("COMPANY") = MResponse.PLNT_MAP(i).COMPANY
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("PLANT") = MResponse.PLNT_MAP(i).PLANT
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("SALEORG") = MResponse.PLNT_MAP(i).SALEORG
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("STORAGE_DES") = MResponse.PLNT_MAP(i).STORAGE_DES
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("STORAGE_LOC") = MResponse.PLNT_MAP(i).STORAGE_LOC
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("CreateBy") = Main.M_NAME.Text
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("LastUpdateBy") = Main.M_NAME.Text
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("CreateDate") = Now
                            TSTOBindingSource.Item(TSTOBindingSource.Position)("LastUpdateDate") = Now
                        End If
                        TSTOBindingSource.EndEdit()
                        T_STOTableAdapter.Update(FPTDataSet.Tables("T_STO"))
                        FPTDataSet.Tables("T_STO").AcceptChanges()
                        FPTDataSet.Tables("T_STO").RejectChanges()


                    End If
                Catch ex As Exception
                    FPTDataSet.Tables("T_STO").RejectChanges()
                End Try

                ''Save(FPTDataSet)
                '    End If

            Next


            treeManu.BtUPMaster.Enabled = True
          
            MResponse = Nothing
            treeManu.BtUPMaster.Enabled = True

        Catch ex As Exception
            MResponse = Nothing
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error message")
            treeManu.BtUPMaster.Enabled = True
        End Try
        'Cursor = Cursors.Default
        GMaster.Visible = True
        BtCancel.Visible = False
        RadWaitingBar1.StopWaiting()
        RadWaitingBar1.Visible = False
        MessageBox.Show("Please Close Application", "Check Update MasterData", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Application.Exit()
    End Sub


    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Private Sub BtUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtUpdate.Click
        Try

            GMaster.Enabled = False
            BtCancel.Visible = True
            RadWaitingBar1.Visible = True
            RadWaitingBar1.StartWaiting()
            RadWaitingBar1.Text = "0.0 %"
            MResponse = Nothing
            TCOMPANYBindingSource.CancelEdit()
            TCustomerBindingSource.CancelEdit()
            TProductBindingSource.CancelEdit()
            TSHIPPERBindingSource.CancelEdit()
            FSAP01_Load(sender, e)

            '  Cursor = Cursors.WaitCursor
            Dim MRequest As String = ""
            Dim myproxy As New Material_Customer_Master_Sync_Out_SIService
            Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
            Dim netCred As NetworkCredential = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            Dim uri As Uri = New Uri(objIniFile.GetString("Webservice", "URL01", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING01"))
            Dim credentials As ICredentials = netCred.GetCredential(uri, "Basic")
            myproxy.Url = uri.AbsoluteUri
            myproxy.Credentials = credentials
            myproxy.PreAuthenticate = True
            '   myproxy.Credentials = credentials
            AddHandler myproxy.Material_Customer_Master_Sync_Out_SICompleted, AddressOf GetMasterData
            '   TSLWS.Enabled = False
            myproxy.Material_Customer_Master_Sync_Out_SIAsync(MRequest)
        Catch ex As Exception
            ' Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub GetMasterData(ByVal sender As Object, ByVal e As Material_Customer_Master_Sync_Out_SICompletedEventArgs)
        Try
            If (Not e.Cancelled) Then
                MResponse = e.Result
                FRefresh()
                ' Cursor = Cursors.Default
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error message")
            ' Cursor = Cursors.Default
        End Try
    End Sub

    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try

            Dim sql As String = "Select * from T_COMPANY order by company_id"
            Dim sql2 As String = "Select * from T_CUSTOMER order by ID"
            Dim sql3 As String = "Select * from T_Product order by ID"
            Dim sql4 As String = "Select * from T_SHIPPER order by ID"

            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql2, conn)
            Dim da3 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql3, conn)
            Dim da4 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql4, conn)


            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                Dim cb2 As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da2)
                Dim cb3 As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da3)
                Dim cb4 As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da4)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_COMPANY")
                    da2.Update(ds, "T_CUSTOMER")
                    da3.Update(ds, "T_Product")
                    da4.Update(ds, "T_SHIPPER")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
                da2.Dispose()
                da3.Dispose()
                da4.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub


    Private Sub FSAP01_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO' table. You can move, or remove it, as needed.
        Me.T_STOTableAdapter.Fill(Me.FPTDataSet.T_STO)
        'TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
        Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
        Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
        'TODO: This line of code loads data into the 'FPTDataSet.T_Customer' table. You can move, or remove it, as needed.
        Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
        'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
        ' Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtUpdate.Enabled = CheckEditPermisstion(sender.tag)
        ' BtDelete.Enabled = CheckDelPermisstion(sender.tag)
        ' BtUserRights.Enabled = CheckAddPermisstion(sender.tag)
    End Sub

  
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles BtCancel.Click
        Try
            ThrRefresh.CancelAsync()
            GMaster.Visible = True
            BtCancel.Visible = False
            RadWaitingBar1.Visible = False
            RadWaitingBar1.StopWaiting()
            Close()
        Catch ex As Exception

        End Try
    End Sub
End Class