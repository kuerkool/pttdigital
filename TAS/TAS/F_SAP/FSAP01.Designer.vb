﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FSAP01
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BtUpdate = New System.Windows.Forms.Button()
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.TCustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TCOMPANYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_COMPANYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter()
        Me.T_CustomerTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_CustomerTableAdapter()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.TLOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_LOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_LOTableAdapter()
        Me.TSTOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_STOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_STOTableAdapter()
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        Me.BtCancel = New System.Windows.Forms.Button()
        Me.RadWaitingBar1 = New Telerik.WinControls.UI.RadWaitingBar()
        Me.cbcus = New System.Windows.Forms.CheckBox()
        Me.cbmat = New System.Windows.Forms.CheckBox()
        Me.cbforw = New System.Windows.Forms.CheckBox()
        Me.cbpoint = New System.Windows.Forms.CheckBox()
        Me.GMaster = New Telerik.WinControls.UI.RadGroupBox()
        Me.cbplant = New System.Windows.Forms.CheckBox()
        Me.cbloca = New System.Windows.Forms.CheckBox()
        Me.cborg = New System.Windows.Forms.CheckBox()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TLOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSTOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadWaitingBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GMaster.SuspendLayout()
        Me.SuspendLayout()
        '
        'BtUpdate
        '
        Me.BtUpdate.Location = New System.Drawing.Point(481, 36)
        Me.BtUpdate.Name = "BtUpdate"
        Me.BtUpdate.Size = New System.Drawing.Size(108, 43)
        Me.BtUpdate.TabIndex = 0
        Me.BtUpdate.Text = "Update"
        Me.BtUpdate.UseVisualStyleBackColor = True
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TCustomerBindingSource
        '
        Me.TCustomerBindingSource.DataMember = "T_Customer"
        Me.TCustomerBindingSource.DataSource = Me.FPTDataSet
        '
        'TCOMPANYBindingSource
        '
        Me.TCOMPANYBindingSource.DataMember = "T_COMPANY"
        Me.TCOMPANYBindingSource.DataSource = Me.FPTDataSet
        '
        'T_COMPANYTableAdapter
        '
        Me.T_COMPANYTableAdapter.ClearBeforeFill = True
        '
        'T_CustomerTableAdapter
        '
        Me.T_CustomerTableAdapter.ClearBeforeFill = True
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'TLOBindingSource
        '
        Me.TLOBindingSource.DataMember = "T_LO"
        Me.TLOBindingSource.DataSource = Me.FPTDataSet
        '
        'T_LOTableAdapter
        '
        Me.T_LOTableAdapter.ClearBeforeFill = True
        '
        'TSTOBindingSource
        '
        Me.TSTOBindingSource.DataMember = "T_STO"
        Me.TSTOBindingSource.DataSource = Me.FPTDataSet
        '
        'T_STOTableAdapter
        '
        Me.T_STOTableAdapter.ClearBeforeFill = True
        '
        'BtCancel
        '
        Me.BtCancel.Location = New System.Drawing.Point(493, 128)
        Me.BtCancel.Name = "BtCancel"
        Me.BtCancel.Size = New System.Drawing.Size(108, 41)
        Me.BtCancel.TabIndex = 2
        Me.BtCancel.Text = "Cancel"
        Me.BtCancel.UseVisualStyleBackColor = True
        Me.BtCancel.Visible = False
        '
        'RadWaitingBar1
        '
        Me.RadWaitingBar1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadWaitingBar1.Location = New System.Drawing.Point(12, 128)
        Me.RadWaitingBar1.Name = "RadWaitingBar1"
        Me.RadWaitingBar1.ShowText = True
        Me.RadWaitingBar1.Size = New System.Drawing.Size(436, 41)
        Me.RadWaitingBar1.TabIndex = 3
        Me.RadWaitingBar1.Text = "0%"
        Me.RadWaitingBar1.ThemeName = "Office2010Blue"
        Me.RadWaitingBar1.Visible = False
        '
        'cbcus
        '
        Me.cbcus.AutoSize = True
        Me.cbcus.Checked = True
        Me.cbcus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbcus.Location = New System.Drawing.Point(17, 48)
        Me.cbcus.Name = "cbcus"
        Me.cbcus.Size = New System.Drawing.Size(82, 20)
        Me.cbcus.TabIndex = 4
        Me.cbcus.Text = "Customer"
        Me.cbcus.UseVisualStyleBackColor = True
        '
        'cbmat
        '
        Me.cbmat.AutoSize = True
        Me.cbmat.Checked = True
        Me.cbmat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbmat.Location = New System.Drawing.Point(184, 22)
        Me.cbmat.Name = "cbmat"
        Me.cbmat.Size = New System.Drawing.Size(73, 20)
        Me.cbmat.TabIndex = 5
        Me.cbmat.Text = "Material"
        Me.cbmat.UseVisualStyleBackColor = True
        '
        'cbforw
        '
        Me.cbforw.AutoSize = True
        Me.cbforw.Checked = True
        Me.cbforw.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbforw.Location = New System.Drawing.Point(17, 74)
        Me.cbforw.Name = "cbforw"
        Me.cbforw.Size = New System.Drawing.Size(129, 20)
        Me.cbforw.TabIndex = 6
        Me.cbforw.Text = "Forwarding Agent"
        Me.cbforw.UseVisualStyleBackColor = True
        '
        'cbpoint
        '
        Me.cbpoint.AutoSize = True
        Me.cbpoint.Checked = True
        Me.cbpoint.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbpoint.Location = New System.Drawing.Point(184, 74)
        Me.cbpoint.Name = "cbpoint"
        Me.cbpoint.Size = New System.Drawing.Size(103, 20)
        Me.cbpoint.TabIndex = 7
        Me.cbpoint.Text = "Loading point"
        Me.cbpoint.UseVisualStyleBackColor = True
        '
        'GMaster
        '
        Me.GMaster.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.GMaster.Controls.Add(Me.cbplant)
        Me.GMaster.Controls.Add(Me.cbloca)
        Me.GMaster.Controls.Add(Me.cborg)
        Me.GMaster.Controls.Add(Me.cbcus)
        Me.GMaster.Controls.Add(Me.cbpoint)
        Me.GMaster.Controls.Add(Me.BtUpdate)
        Me.GMaster.Controls.Add(Me.cbmat)
        Me.GMaster.Controls.Add(Me.cbforw)
        Me.GMaster.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GMaster.HeaderText = "Master Data"
        Me.GMaster.Location = New System.Drawing.Point(12, 12)
        Me.GMaster.Name = "GMaster"
        Me.GMaster.Size = New System.Drawing.Size(599, 110)
        Me.GMaster.TabIndex = 8
        Me.GMaster.Text = "Master Data"
        '
        'cbplant
        '
        Me.cbplant.AutoSize = True
        Me.cbplant.Checked = True
        Me.cbplant.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbplant.Location = New System.Drawing.Point(337, 22)
        Me.cbplant.Name = "cbplant"
        Me.cbplant.Size = New System.Drawing.Size(99, 20)
        Me.cbplant.TabIndex = 10
        Me.cbplant.Text = "Plant master"
        Me.cbplant.UseVisualStyleBackColor = True
        '
        'cbloca
        '
        Me.cbloca.AutoSize = True
        Me.cbloca.Checked = True
        Me.cbloca.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbloca.Location = New System.Drawing.Point(184, 48)
        Me.cbloca.Name = "cbloca"
        Me.cbloca.Size = New System.Drawing.Size(120, 20)
        Me.cbloca.TabIndex = 9
        Me.cbloca.Text = "Storage location"
        Me.cbloca.UseVisualStyleBackColor = True
        '
        'cborg
        '
        Me.cborg.AutoSize = True
        Me.cborg.Checked = True
        Me.cborg.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cborg.Location = New System.Drawing.Point(17, 22)
        Me.cborg.Name = "cborg"
        Me.cborg.Size = New System.Drawing.Size(132, 20)
        Me.cborg.TabIndex = 8
        Me.cborg.Text = "Sales organization"
        Me.cborg.UseVisualStyleBackColor = True
        '
        'FSAP01
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(619, 177)
        Me.Controls.Add(Me.GMaster)
        Me.Controls.Add(Me.RadWaitingBar1)
        Me.Controls.Add(Me.BtCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FSAP01"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "11"
        Me.Text = "Update MasterData"
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TLOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSTOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadWaitingBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GMaster.ResumeLayout(False)
        Me.GMaster.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtUpdate As System.Windows.Forms.Button
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TCOMPANYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_COMPANYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter
    Friend WithEvents TCustomerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_CustomerTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_CustomerTableAdapter
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents TLOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_LOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_LOTableAdapter
    Friend WithEvents TSTOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_STOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_STOTableAdapter
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents BtCancel As System.Windows.Forms.Button
    Friend WithEvents RadWaitingBar1 As Telerik.WinControls.UI.RadWaitingBar
    Friend WithEvents cbcus As System.Windows.Forms.CheckBox
    Friend WithEvents cbmat As System.Windows.Forms.CheckBox
    Friend WithEvents cbforw As System.Windows.Forms.CheckBox
    Friend WithEvents cbpoint As System.Windows.Forms.CheckBox
    Friend WithEvents GMaster As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents cbloca As System.Windows.Forms.CheckBox
    Friend WithEvents cborg As System.Windows.Forms.CheckBox
    Friend WithEvents cbplant As System.Windows.Forms.CheckBox
End Class
