﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace My
    
    <Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"),  _
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
    Partial Friend NotInheritable Class MySettings
        Inherits Global.System.Configuration.ApplicationSettingsBase
        
        Private Shared defaultInstance As MySettings = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New MySettings()),MySettings)
        
#Region "My.Settings Auto-Save Functionality"
#If _MyType = "WindowsForms" Then
    Private Shared addedHandler As Boolean

    Private Shared addedHandlerLockObject As New Object

    <Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)> _
    Private Shared Sub AutoSaveSettings(ByVal sender As Global.System.Object, ByVal e As Global.System.EventArgs)
        If My.Application.SaveMySettingsOnExit Then
            My.Settings.Save()
        End If
    End Sub
#End If
#End Region
        
        Public Shared ReadOnly Property [Default]() As MySettings
            Get
                
#If _MyType = "WindowsForms" Then
               If Not addedHandler Then
                    SyncLock addedHandlerLockObject
                        If Not addedHandler Then
                            AddHandler My.Application.Shutdown, AddressOf AutoSaveSettings
                            addedHandler = True
                        End If
                    End SyncLock
                End If
#End If
                Return defaultInstance
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=12;Initial Catalog=TAS;Persist Security Info=True;User ID=sa;Password"& _ 
            "=P@$$w0rd")>  _
        Public ReadOnly Property FPTConnectionString() As String
            Get
                Return CType(Me("FPTConnectionString"),String)
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.WebServiceUrl)>  _
        Public ReadOnly Property TAS_WebReference_MI_TASI03Service() As String
            Get
                Return CType(Me("TAS_WebReference_MI_TASI03Service"),String)
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.WebServiceUrl),  _
         Global.System.Configuration.DefaultSettingValueAttribute("http://sgci1wd01.pttgc.corp:8100/XISOAPAdapter/MessageServlet?senderParty=&sender"& _ 
            "Service=Truck_Loading&receiverParty=&receiverService=&interface=Material_Custome"& _ 
            "r_Master_Sync_Out_SI&interfaceNamespace=urn%3Apttgc.com%3ASD%3ATruck_Loading")>  _
        Public ReadOnly Property TAS_TOL_SAP01_MI_TASI01Service() As String
            Get
                Return CType(Me("TAS_TOL_SAP01_MI_TASI01Service"),String)
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.WebServiceUrl),  _
         Global.System.Configuration.DefaultSettingValueAttribute("http://localhost/TASService/Service.asmx")>  _
        Public ReadOnly Property TAS_TOL_TASService_TASService() As String
            Get
                Return CType(Me("TAS_TOL_TASService_TASService"),String)
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.WebServiceUrl),  _
         Global.System.Configuration.DefaultSettingValueAttribute("http://sgci1wd01.pttgc.corp:8100/XISOAPAdapter/MessageServlet?senderParty=&sender"& _ 
            "Service=Truck_Loading&receiverParty=&receiverService=&interface=Extract_DO_Sync_"& _ 
            "Out_SI&interfaceNamespace=urn%3Apttgc.com%3ASD%3ATruck_Loading")>  _
        Public ReadOnly Property TAS_TOL_SAP02_MI_TASI02Service() As String
            Get
                Return CType(Me("TAS_TOL_SAP02_MI_TASI02Service"),String)
            End Get
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("GCCOA")>  _
        Public Property WORK_P() As String
            Get
                Return CType(Me("WORK_P"),String)
            End Get
            Set
                Me("WORK_P") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("GCCOA")>  _
        Public Property WIGHT_P() As String
            Get
                Return CType(Me("WIGHT_P"),String)
            End Get
            Set
                Me("WIGHT_P") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("1")>  _
        Public Property WORK_COPY() As String
            Get
                Return CType(Me("WORK_COPY"),String)
            End Get
            Set
                Me("WORK_COPY") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("1")>  _
        Public Property WIGHT_COPY() As String
            Get
                Return CType(Me("WIGHT_COPY"),String)
            End Get
            Set
                Me("WIGHT_COPY") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("D:\Application\Weight Scale interface for WMS\R3\Weight Scale interface for WMS\W"& _ 
            "eight Scale interface for WMS.exe")>  _
        Public Property WMSApp() As String
            Get
                Return CType(Me("WMSApp"),String)
            End Get
            Set
                Me("WMSApp") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("DefaultPaperOrientation")>  _
        Public Property WORK_PaperOrientation() As Global.CrystalDecisions.[Shared].PaperOrientation
            Get
                Return CType(Me("WORK_PaperOrientation"),Global.CrystalDecisions.[Shared].PaperOrientation)
            End Get
            Set
                Me("WORK_PaperOrientation") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Portrait")>  _
        Public Property WIGHT_PaperOrientation() As Global.CrystalDecisions.[Shared].PaperOrientation
            Get
                Return CType(Me("WIGHT_PaperOrientation"),Global.CrystalDecisions.[Shared].PaperOrientation)
            End Get
            Set
                Me("WIGHT_PaperOrientation") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("0")>  _
        Public Property WORK_PaperSize() As Integer
            Get
                Return CType(Me("WORK_PaperSize"),Integer)
            End Get
            Set
                Me("WORK_PaperSize") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("2")>  _
        Public Property WIGHT_PaperSize() As Integer
            Get
                Return CType(Me("WIGHT_PaperSize"),Integer)
            End Get
            Set
                Me("WIGHT_PaperSize") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("1")>  _
        Public Property WIGHTidx() As Integer
            Get
                Return CType(Me("WIGHTidx"),Integer)
            End Get
            Set
                Me("WIGHTidx") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("piserver")>  _
        Public Property PI_SERVER() As String
            Get
                Return CType(Me("PI_SERVER"),String)
            End Get
            Set
                Me("PI_SERVER") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("ictviewer")>  _
        Public Property PI_USER() As String
            Get
                Return CType(Me("PI_USER"),String)
            End Get
            Set
                Me("PI_USER") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("ictviewer")>  _
        Public Property PI_PASSWORD() As String
            Get
                Return CType(Me("PI_PASSWORD"),String)
            End Get
            Set
                Me("PI_PASSWORD") = value
            End Set
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.WebServiceUrl),  _
         Global.System.Configuration.DefaultSettingValueAttribute("http://schwebpart.pttgc.corp:82/PIGETdata.asmx")>  _
        Public ReadOnly Property TAS_TOL_corp_pttgc_schwebpart_PIGetData() As String
            Get
                Return CType(Me("TAS_TOL_corp_pttgc_schwebpart_PIGetData"),String)
            End Get
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("YME For Esso Update.xlsx")>  _
        Public Property CAL_FILENAME() As String
            Get
                Return CType(Me("CAL_FILENAME"),String)
            End Get
            Set
                Me("CAL_FILENAME") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("21")>  _
        Public Property CAL_TEMP_ROW() As Integer
            Get
                Return CType(Me("CAL_TEMP_ROW"),Integer)
            End Get
            Set
                Me("CAL_TEMP_ROW") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("3")>  _
        Public Property CAL_TEMP_COLU() As Integer
            Get
                Return CType(Me("CAL_TEMP_COLU"),Integer)
            End Get
            Set
                Me("CAL_TEMP_COLU") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("21")>  _
        Public Property CAL_WEIGHT_ROW() As Integer
            Get
                Return CType(Me("CAL_WEIGHT_ROW"),Integer)
            End Get
            Set
                Me("CAL_WEIGHT_ROW") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("5")>  _
        Public Property CAL_WEIGHT_COLU() As Integer
            Get
                Return CType(Me("CAL_WEIGHT_COLU"),Integer)
            End Get
            Set
                Me("CAL_WEIGHT_COLU") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("21")>  _
        Public Property CAL_DEN_ROW() As Integer
            Get
                Return CType(Me("CAL_DEN_ROW"),Integer)
            End Get
            Set
                Me("CAL_DEN_ROW") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("7")>  _
        Public Property CAL_DEN_COLU() As Integer
            Get
                Return CType(Me("CAL_DEN_COLU"),Integer)
            End Get
            Set
                Me("CAL_DEN_COLU") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("21")>  _
        Public Property CAL_VOLUME_ROW() As Integer
            Get
                Return CType(Me("CAL_VOLUME_ROW"),Integer)
            End Get
            Set
                Me("CAL_VOLUME_ROW") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("9")>  _
        Public Property CAL_VOLUME_COLU() As Integer
            Get
                Return CType(Me("CAL_VOLUME_COLU"),Integer)
            End Get
            Set
                Me("CAL_VOLUME_COLU") = value
            End Set
        End Property
    End Class
End Namespace

Namespace My
    
    <Global.Microsoft.VisualBasic.HideModuleNameAttribute(),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Module MySettingsProperty
        
        <Global.System.ComponentModel.Design.HelpKeywordAttribute("My.Settings")>  _
        Friend ReadOnly Property Settings() As Global.TAS_TOL.My.MySettings
            Get
                Return Global.TAS_TOL.My.MySettings.Default
            End Get
        End Property
    End Module
End Namespace
