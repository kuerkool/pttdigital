﻿Imports TAS_TOL.C01_Permission
Imports ExtendedErrorProvider
Public Class Driver
    Dim MyErrorProvider As New ErrorProviderExtended
    Dim Max_ID As Integer
    Dim NewData As Boolean
    Public DRIVER_NAME As String
    Public Shared ShowType As Integer
    Private Sub ClearStatus()
        MyErrorProvider.ClearAllErrorMessages()
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.Controls.Add(d_IdenNO, "Identification Number :")
        MyErrorProvider.Controls.Add(d_Name, "Name - last name : ")
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        NewData = False
        MeterGrid.ResumeLayout()
        Me.BringToFront()
    End Sub
    Private Sub Driver_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MeterGrid.SuspendLayout()
        'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
        'TODO: This line of code loads data into the 'FPTDataSet.T_DRIVER' table. You can move, or remove it, as needed.
        Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
        'Add controls one by one in error provider.
        MyErrorProvider.ClearAllErrorMessages()
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(d_IdenNO, "Identification Number :")
        MyErrorProvider.Controls.Add(d_Name, "Name - last name : ")
        '  MyErrorProvider.Controls.Add(D_LICENSE_NO, "driving license Number :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        NewData = False
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub SaveDriver(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_DRIVER"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()
                If ds.HasChanges Then
                    da.Update(ds, "T_DRIVER")
                    ds.AcceptChanges()
                End If
            Finally

                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TDRIVERBindingSource.CancelEdit()
    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        d_IdenNO.Focus()
    End Sub

    Private Sub D_trans_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles D_trans.Leave
        Dim TransText As String
        TransText = sender.text
        sender.SelectedIndex = sender.FindStringExact(TransText)
        If (sender.SelectedIndex < 0) And (TransText <> "") Then
            sender.SelectedIndex = -1
            f04_01_TransportationCompany.Close()
            Me.AddOwnedForm(f04_01_TransportationCompany)
            f04_01_TransportationCompany.ShowType = 1
            f04_01_TransportationCompany.CompanyCode = TransText
            f04_01_TransportationCompany.ShowDialog()
            Me.RemoveOwnedForm(f04_01_TransportationCompany)
            Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
            sender.SelectedIndex = sender.FindStringExact(TransText)
        End If
    End Sub

    Private Function RandomNumber(ByVal min As Integer, ByVal max As Integer) As Integer
        Dim random As New Random()
        Randomize()
        Return random.Next(min, max)
    End Function 'RandomNumber 


    Private Sub Bsave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Dim Driver_Code As Integer
        conn.Open()
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    '''''''''''''''''''''''''''''''''''''''
                    'If d_backDis.Checked = True Then
                    '    TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST") = "No"
                    'Else
                    '    TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST") = "Yes"
                    'End If

                    If NewData = True Then
                        TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("ID") = Max_ID + 1
                        Dim dt As New DataTable
                        Do
                            Driver_Code = RandomNumber(100000, 999999)
                            Dim sql As String = "Select Driver_Code from T_DRIVER where Driver_Code=" + "'" + Driver_Code.ToString + "'"
                            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                            da.Fill(dt)
                        Loop Until dt.Rows.Count = 0
                        TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_Code") = Driver_Code
                    End If
                    TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("update_Date") = Now
                    TDRIVERBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_DRIVER").EndCurrentEdit()
                    T_DRIVERTableAdapter.Update(FPTDataSet.T_DRIVER)
                    '  SaveDriver(FPTDataSet)
                    'Driver_Load(sender, e)
                    ClearStatus()
                    Me.BringToFront()
                Finally
                    conn.Close()
                End Try
                If ShowType > 0 Then
                    Me.Close()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)

            End Try
        End If
    End Sub

    Private Sub Bcancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TDRIVERBindingSource.CancelEdit()
        FPTDataSet.T_DRIVER.RejectChanges()
        ClearStatus()
        ''Driver_Load(sender, e)
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False

    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        '  MeterEn.Checked = True
        d_backDis.Checked = True
        '  d_backEn.Enabled = False
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        'BtDelete.Enabled = False
        TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("driver_Date") = Now
        TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST") = "NO"
        d_backDis.Checked = True
        d_IdenNO.Focus()
        'MeterNoBox.Text = max + 1
    End Sub

    Private Sub TDRIVERBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TDRIVERBindingSource.PositionChanged
        Try
            If UCase(TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST").ToString()) = "YES" Then
                d_backEn.Checked = True
            Else
                d_backDis.Checked = True
            End If
            If BtEdit.Enabled = False Then TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("update_Date") = Now
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        NewData = True
        Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
     Where Not r.IsNewRow _
    Select CInt(r.Cells(0).Value)
        Try
            Max_ID = cells.Max
        Catch ex As Exception

        End Try

        d_backDis.Checked = True
        '  d_backEn.Enabled = False
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        d_IdenNO.Focus()

        '  MeterNoBox.Text() = cells.Max + 1
        'Dim min As Integer = cells.Min
        'Dim minAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, min)}
        'Dim max As Integer = cells.Max
        'Dim maxAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, max)}
        'MeterNoBox.Text = max + 1
        'MeterNoBox.Focus()
    End Sub

    Private Sub MeterGrid_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyDown
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub

    Private Sub D_trans_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles D_trans.KeyUp

        If e.KeyCode = 13 Then
            D_trans_Leave(sender, Nothing)
        End If
    End Sub

    Private Sub d_IdenNO_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles d_poscode.KeyPress, d_Phone.KeyPress, d_Fax.KeyPress
        If e.KeyChar <> Chr(8) Then
            Dim strAllowableChars As String
            strAllowableChars = "0123456789-:/ "
            If InStr(strAllowableChars, e.KeyChar.ToString) = 0 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub MeterGrid_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles MeterGrid.CellFormatting
        'change background color of entire row based on value of 4th cell
        Try

            With sender.Rows(e.RowIndex)
                Select Case UCase(.Cells(4).Value)
                    Case "NO"
                        .DefaultCellStyle.BackColor = Color.White
                    Case "YES"
                        .DefaultCellStyle.BackColor = Color.Red
                End Select
            End With

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Driver_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()

            Me.d_Name.Text = DRIVER_NAME

            Me.TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_Date") = Now
            TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("driver_Date") = Now
            TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST") = "NO"

            Me.d_IdenNO.Focus()
        End If
    End Sub

    Private Sub Driver_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
    End Sub

    Private Sub d_backEn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles d_backEn.Click
        If sender.Checked = True Then TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST") = "Yes"
    End Sub

    Private Sub d_backDis_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles d_backDis.Click
        If sender.Checked = True Then TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_BLACK_LIST") = "No"
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_DRIVER.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_DRIVER.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_DRIVER.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '     Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_DRIVER.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TDRIVERBindingSource.Filter = StrFilter
        Else : TDRIVERBindingSource.RemoveFilter()
        End If

    End Sub

   
    Private Sub BTLoadImage_Click(sender As System.Object, e As System.EventArgs) Handles BTLoadImage.Click
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(OpenFileDialog1.FileName)
        End If
    End Sub
End Class
