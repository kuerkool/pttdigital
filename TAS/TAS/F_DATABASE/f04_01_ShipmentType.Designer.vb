﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_ShipmentType
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_ShipmentType))
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CODEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPTIONDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TSHIPMENTTYPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.P_CRDATE = New System.Windows.Forms.TextBox()
        Me.P_DEC = New System.Windows.Forms.TextBox()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.P_UPDATE = New System.Windows.Forms.TextBox()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.P_NAME = New System.Windows.Forms.TextBox()
        Me.P_CODE = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.T_SHIPMENT_TYPETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPMENT_TYPETableAdapter()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPMENTTYPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.DetailGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.CODEDataGridViewTextBoxColumn, Me.NAMEDataGridViewTextBoxColumn, Me.DESCRIPTIONDataGridViewTextBoxColumn})
        Me.MeterGrid.DataSource = Me.TSHIPMENTTYPEBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.MeterGrid.Location = New System.Drawing.Point(0, 34)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(504, 394)
        Me.MeterGrid.TabIndex = 4
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CODEDataGridViewTextBoxColumn
        '
        Me.CODEDataGridViewTextBoxColumn.DataPropertyName = "CODE"
        Me.CODEDataGridViewTextBoxColumn.HeaderText = "CODE"
        Me.CODEDataGridViewTextBoxColumn.Name = "CODEDataGridViewTextBoxColumn"
        Me.CODEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NAMEDataGridViewTextBoxColumn
        '
        Me.NAMEDataGridViewTextBoxColumn.DataPropertyName = "NAME"
        Me.NAMEDataGridViewTextBoxColumn.HeaderText = "NAME"
        Me.NAMEDataGridViewTextBoxColumn.Name = "NAMEDataGridViewTextBoxColumn"
        Me.NAMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DESCRIPTIONDataGridViewTextBoxColumn
        '
        Me.DESCRIPTIONDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPTION"
        Me.DESCRIPTIONDataGridViewTextBoxColumn.HeaderText = "DESCRIPTION"
        Me.DESCRIPTIONDataGridViewTextBoxColumn.Name = "DESCRIPTIONDataGridViewTextBoxColumn"
        Me.DESCRIPTIONDataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCRIPTIONDataGridViewTextBoxColumn.Width = 150
        '
        'TSHIPMENTTYPEBindingSource
        '
        Me.TSHIPMENTTYPEBindingSource.DataMember = "T_SHIPMENT_TYPE"
        Me.TSHIPMENTTYPEBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'Bcancel
        '
        Me.Bcancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bcancel.Location = New System.Drawing.Point(210, 365)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 24
        Me.Bcancel.Tag = "14"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Bsave
        '
        Me.Bsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bsave.Location = New System.Drawing.Point(129, 365)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 23
        Me.Bsave.Tag = "14"
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(291, 21)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(20, 25)
        Me.Label15.TabIndex = 134
        Me.Label15.Text = "*"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(291, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 25)
        Me.Label3.TabIndex = 135
        Me.Label3.Text = "*"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'P_CRDATE
        '
        Me.P_CRDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPMENTTYPEBindingSource, "Cre_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.P_CRDATE.Enabled = False
        Me.P_CRDATE.Location = New System.Drawing.Point(129, 103)
        Me.P_CRDATE.Name = "P_CRDATE"
        Me.P_CRDATE.Size = New System.Drawing.Size(156, 22)
        Me.P_CRDATE.TabIndex = 7
        '
        'P_DEC
        '
        Me.P_DEC.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPMENTTYPEBindingSource, "DESCRIPTION", True))
        Me.P_DEC.Location = New System.Drawing.Point(129, 75)
        Me.P_DEC.Name = "P_DEC"
        Me.P_DEC.Size = New System.Drawing.Size(156, 22)
        Me.P_DEC.TabIndex = 6
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'P_UPDATE
        '
        Me.P_UPDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPMENTTYPEBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.P_UPDATE.Enabled = False
        Me.P_UPDATE.Location = New System.Drawing.Point(129, 131)
        Me.P_UPDATE.Name = "P_UPDATE"
        Me.P_UPDATE.Size = New System.Drawing.Size(156, 22)
        Me.P_UPDATE.TabIndex = 8
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(34, 134)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Update Date :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(33, 106)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Created date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(47, 78)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 16)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Desciption :"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TSHIPMENTTYPEBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(839, 27)
        Me.BindingNavigator1.TabIndex = 3
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.P_UPDATE)
        Me.DetailGroup.Controls.Add(Me.P_CRDATE)
        Me.DetailGroup.Controls.Add(Me.P_DEC)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.P_NAME)
        Me.DetailGroup.Controls.Add(Me.P_CODE)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Enabled = False
        Me.DetailGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DetailGroup.Location = New System.Drawing.Point(510, 27)
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.Size = New System.Drawing.Size(320, 401)
        Me.DetailGroup.TabIndex = 5
        Me.DetailGroup.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Shipment Name :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_NAME
        '
        Me.P_NAME.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPMENTTYPEBindingSource, "NAME", True))
        Me.P_NAME.Location = New System.Drawing.Point(129, 47)
        Me.P_NAME.Name = "P_NAME"
        Me.P_NAME.Size = New System.Drawing.Size(156, 22)
        Me.P_NAME.TabIndex = 4
        '
        'P_CODE
        '
        Me.P_CODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPMENTTYPEBindingSource, "CODE", True))
        Me.P_CODE.Location = New System.Drawing.Point(129, 19)
        Me.P_CODE.Name = "P_CODE"
        Me.P_CODE.Size = New System.Drawing.Size(156, 22)
        Me.P_CODE.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Shipment Code :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'T_SHIPMENT_TYPETableAdapter
        '
        Me.T_SHIPMENT_TYPETableAdapter.ClearBeforeFill = True
        '
        'f04_01_ShipmentType
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(839, 435)
        Me.Controls.Add(Me.MeterGrid)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.DetailGroup)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "f04_01_ShipmentType"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "14"
        Me.Text = "ShipmentType"
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPMENTTYPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents P_CRDATE As System.Windows.Forms.TextBox
    Friend WithEvents P_DEC As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents P_UPDATE As System.Windows.Forms.TextBox
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents P_NAME As System.Windows.Forms.TextBox
    Friend WithEvents P_CODE As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TSHIPMENTTYPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPMENT_TYPETableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPMENT_TYPETableAdapter
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CODEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPTIONDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
End Class
