﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Fpacking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Fpacking))
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.PIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PCODEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PWEIGHTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PCAPASITYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PDECDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TPACKINGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.P_UPDATE = New System.Windows.Forms.TextBox()
        Me.P_CRDATE = New System.Windows.Forms.TextBox()
        Me.P_CAPASITY = New System.Windows.Forms.TextBox()
        Me.P_DEC = New System.Windows.Forms.TextBox()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.P_WEIGHT = New System.Windows.Forms.TextBox()
        Me.P_CODE = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.T_PACKINGTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_PACKINGTableAdapter()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TPACKINGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DetailGroup.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PIDDataGridViewTextBoxColumn, Me.PCODEDataGridViewTextBoxColumn, Me.PWEIGHTDataGridViewTextBoxColumn, Me.PCAPASITYDataGridViewTextBoxColumn, Me.PDECDataGridViewTextBoxColumn})
        Me.MeterGrid.DataSource = Me.TPACKINGBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.MeterGrid.Location = New System.Drawing.Point(0, 31)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(504, 393)
        Me.MeterGrid.TabIndex = 1
        '
        'PIDDataGridViewTextBoxColumn
        '
        Me.PIDDataGridViewTextBoxColumn.DataPropertyName = "P_ID"
        Me.PIDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.PIDDataGridViewTextBoxColumn.Name = "PIDDataGridViewTextBoxColumn"
        Me.PIDDataGridViewTextBoxColumn.ReadOnly = True
        Me.PIDDataGridViewTextBoxColumn.Width = 50
        '
        'PCODEDataGridViewTextBoxColumn
        '
        Me.PCODEDataGridViewTextBoxColumn.DataPropertyName = "P_CODE"
        Me.PCODEDataGridViewTextBoxColumn.HeaderText = "Packing Code"
        Me.PCODEDataGridViewTextBoxColumn.Name = "PCODEDataGridViewTextBoxColumn"
        Me.PCODEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PWEIGHTDataGridViewTextBoxColumn
        '
        Me.PWEIGHTDataGridViewTextBoxColumn.DataPropertyName = "P_WEIGHT"
        Me.PWEIGHTDataGridViewTextBoxColumn.HeaderText = "Packing Weight"
        Me.PWEIGHTDataGridViewTextBoxColumn.Name = "PWEIGHTDataGridViewTextBoxColumn"
        Me.PWEIGHTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PCAPASITYDataGridViewTextBoxColumn
        '
        Me.PCAPASITYDataGridViewTextBoxColumn.DataPropertyName = "P_CAPASITY"
        Me.PCAPASITYDataGridViewTextBoxColumn.HeaderText = "Packing Capasity"
        Me.PCAPASITYDataGridViewTextBoxColumn.Name = "PCAPASITYDataGridViewTextBoxColumn"
        Me.PCAPASITYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PDECDataGridViewTextBoxColumn
        '
        Me.PDECDataGridViewTextBoxColumn.DataPropertyName = "P_DEC"
        Me.PDECDataGridViewTextBoxColumn.HeaderText = "Packing Decription"
        Me.PDECDataGridViewTextBoxColumn.Name = "PDECDataGridViewTextBoxColumn"
        Me.PDECDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TPACKINGBindingSource
        '
        Me.TPACKINGBindingSource.DataMember = "T_PACKING"
        Me.TPACKINGBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'P_UPDATE
        '
        Me.P_UPDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TPACKINGBindingSource, "P_UPDATE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.P_UPDATE.Enabled = False
        Me.P_UPDATE.Location = New System.Drawing.Point(129, 159)
        Me.P_UPDATE.Name = "P_UPDATE"
        Me.P_UPDATE.Size = New System.Drawing.Size(144, 22)
        Me.P_UPDATE.TabIndex = 8
        '
        'P_CRDATE
        '
        Me.P_CRDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TPACKINGBindingSource, "P_CRDATE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.P_CRDATE.Enabled = False
        Me.P_CRDATE.Location = New System.Drawing.Point(129, 131)
        Me.P_CRDATE.Name = "P_CRDATE"
        Me.P_CRDATE.Size = New System.Drawing.Size(144, 22)
        Me.P_CRDATE.TabIndex = 7
        '
        'P_CAPASITY
        '
        Me.P_CAPASITY.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TPACKINGBindingSource, "P_CAPASITY", True))
        Me.P_CAPASITY.Location = New System.Drawing.Point(129, 75)
        Me.P_CAPASITY.Name = "P_CAPASITY"
        Me.P_CAPASITY.Size = New System.Drawing.Size(144, 22)
        Me.P_CAPASITY.TabIndex = 5
        '
        'P_DEC
        '
        Me.P_DEC.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TPACKINGBindingSource, "P_DEC", True))
        Me.P_DEC.Location = New System.Drawing.Point(129, 103)
        Me.P_DEC.Name = "P_DEC"
        Me.P_DEC.Size = New System.Drawing.Size(144, 22)
        Me.P_DEC.TabIndex = 6
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.Label5)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.Label4)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.P_UPDATE)
        Me.DetailGroup.Controls.Add(Me.P_CRDATE)
        Me.DetailGroup.Controls.Add(Me.P_DEC)
        Me.DetailGroup.Controls.Add(Me.P_CAPASITY)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.Label9)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.P_WEIGHT)
        Me.DetailGroup.Controls.Add(Me.P_CODE)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Enabled = False
        Me.DetailGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DetailGroup.Location = New System.Drawing.Point(510, 24)
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.Size = New System.Drawing.Size(320, 401)
        Me.DetailGroup.TabIndex = 2
        Me.DetailGroup.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(274, 78)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 16)
        Me.Label6.TabIndex = 138
        Me.Label6.Text = "Kg"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(274, 50)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 16)
        Me.Label5.TabIndex = 137
        Me.Label5.Text = "Kg"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Bcancel
        '
        Me.Bcancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bcancel.Location = New System.Drawing.Point(210, 365)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 24
        Me.Bcancel.Tag = "6"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bsave.Location = New System.Drawing.Point(129, 365)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 23
        Me.Bsave.Tag = "6"
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(295, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 25)
        Me.Label4.TabIndex = 136
        Me.Label4.Text = "*"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(295, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 25)
        Me.Label3.TabIndex = 135
        Me.Label3.Text = "*"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(295, 21)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(20, 25)
        Me.Label15.TabIndex = 134
        Me.Label15.Text = "*"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(32, 162)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Update Date :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(31, 134)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Created date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(45, 106)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 16)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Desciption :"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(4, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(119, 16)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Packing Capasity :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Packing Weight :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_WEIGHT
        '
        Me.P_WEIGHT.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TPACKINGBindingSource, "P_WEIGHT", True))
        Me.P_WEIGHT.Location = New System.Drawing.Point(129, 47)
        Me.P_WEIGHT.Name = "P_WEIGHT"
        Me.P_WEIGHT.Size = New System.Drawing.Size(144, 22)
        Me.P_WEIGHT.TabIndex = 4
        '
        'P_CODE
        '
        Me.P_CODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TPACKINGBindingSource, "P_CODE", True))
        Me.P_CODE.Location = New System.Drawing.Point(129, 19)
        Me.P_CODE.Name = "P_CODE"
        Me.P_CODE.Size = New System.Drawing.Size(144, 22)
        Me.P_CODE.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Packing Code :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TPACKINGBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(839, 27)
        Me.BindingNavigator1.TabIndex = 0
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'T_PACKINGTableAdapter
        '
        Me.T_PACKINGTableAdapter.ClearBeforeFill = True
        '
        'Fpacking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(839, 430)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.MeterGrid)
        Me.Controls.Add(Me.DetailGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Fpacking"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "6"
        Me.Text = "packing"
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TPACKINGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents TPACKINGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents P_UPDATE As System.Windows.Forms.TextBox
    Friend WithEvents P_CRDATE As System.Windows.Forms.TextBox
    Friend WithEvents P_CAPASITY As System.Windows.Forms.TextBox
    Friend WithEvents P_DEC As System.Windows.Forms.TextBox
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents P_WEIGHT As System.Windows.Forms.TextBox
    Friend WithEvents P_CODE As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents T_PACKINGTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_PACKINGTableAdapter
    Friend WithEvents PIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PCODEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PWEIGHTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PCAPASITYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PDECDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
