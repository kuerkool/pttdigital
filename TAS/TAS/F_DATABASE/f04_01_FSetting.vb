﻿Imports TAS_TOL.C01_Permission
Public Class f04_01_FSetting

    Private Sub FSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO1' table. You can move, or remove it, as needed.
        Me.T_STO1TableAdapter.Fill(Me.FPTDataSet.T_STO1)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SETTING' table. You can move, or remove it, as needed.
        Me.T_SETTINGTableAdapter.Fill(Me.FPTDataSet.T_SETTING)
        'Btadd.Enabled = CheckAddPermisstion(sender.tag)
        RadButton1.Enabled = CheckEditPermisstion(sender.tag)
        'BtDelete.Enabled = CheckDelPermisstion(sender.tag)

    End Sub

    Private Sub RadButton1_Click(sender As System.Object, e As System.EventArgs) Handles RadButton1.Click
        TSETTINGBindingSource.EndEdit()
        T_SETTINGTableAdapter.Update(FPTDataSet.T_SETTING)
        MessageBox.Show("บันทึกข้อมูลเรียบร้อย")

    End Sub

    Private Sub RadButton2_Click(sender As System.Object, e As System.EventArgs) Handles RadButton2.Click
        TSETTINGBindingSource.CancelEdit()
        FPTDataSet.T_SETTING.RejectChanges()
        Close()
    End Sub

    Private Sub TSHIPPERBindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSHIPPERBindingSource.PositionChanged
        Try
            TSTO1BindingSource.Filter = "SALEORG='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "'"

        Catch ex As Exception

        End Try
    End Sub
End Class
