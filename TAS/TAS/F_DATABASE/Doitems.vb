﻿Imports System
Imports System.Data.SqlClient
Imports TAS_TOL.C01_Permission

Public Class Doitems
    Dim ds As New DataSet

    Private Sub SelectDO()
        Dim SqlCon As New SqlClient.SqlConnection
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter

        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        cmd.Connection = SqlCon
        SqlCon.Open()
        cmd.CommandText = "select * from t_do where DO_NO<>'' and DO_NO is not null and LOAD_DATE='" & SelectDate.Value.Year.ToString("0000") & "-" & SelectDate.Value.Month.ToString("00") & "-" &
        SelectDate.Value.Day.ToString("00") & "'"
        da.SelectCommand = cmd
        ds = New DataSet
        da.Fill(ds, "t_do")
        TDOBindingSource.DataSource = ds
        TDOBindingSource.DataMember = "t_do"
        SqlCon.Close()
        SqlCon.Dispose()
        cmd.Dispose()
        da.Dispose()
    End Sub

    Private Sub Doitems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SelectDate.Value = Now
        '  Btadd.Enabled = CheckAddPermisstion(sender.tag)
        '  BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
        '  BtUserRights.Enabled = CheckAddPermisstion(sender.tag)
        'TODO: This line of code loads data into the 'FPTDataSet.T_DO' table. You can move, or remove it, as needed.

        'Me.T_DOTableAdapter.Fill(Me.FPTDataSet.T_DO)

    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        Try
            If EDFillter.Text <> "" Then
                Dim StrFilter As String = ""
                For i = 0 To ds.Tables("T_DO").Columns.Count - 1
                    Dim _DataType As String = ds.Tables("T_DO").Columns(i).DataType.ToString

                    If _DataType = "System.String" Then
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += ds.Tables("T_DO").Columns(i).ColumnName & " like '%" & EDFillter.Text & "%' "
                    ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                        Try
                            '   Int(EDFillter.Text)
                            Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                            If StrFilter <> "" Then StrFilter += " or "
                            StrFilter += ds.Tables("T_DO").Columns(i).ColumnName & " = '" & FilterText & "'"
                        Catch ex As Exception
                        End Try
                    End If
                Next
                TDOBindingSource.Filter = StrFilter
            Else : TDOBindingSource.RemoveFilter()
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub SelectDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectDate.ValueChanged
        SelectDO()
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        If MsgBox("Are you sure to Delete?", vbYesNo + vbDefaultButton2, "Confirmation") = vbYes Then
            Dim SqlCon As New SqlClient.SqlConnection
            Dim cmd As New SqlCommand
            Dim DO_NO As String = TDOBindingSource.Item(TDOBindingSource.Position)("DO_NO").ToString

            If DO_NO <> "" Then
                SqlCon.ConnectionString = My.Settings.FPTConnectionString
                cmd.Connection = SqlCon
                SqlCon.Open()
                cmd.CommandText = " Delete T_LOADINGNOTECOMPARTMENT WHERE LC_LOAD in(select LOAD_ID from T_LOADINGNOTE where LOAD_DOfull ='" & DO_NO & "') ; " &
                                  " Delete T_BATCH_LOT_TAS WHERE DO_NO ='" & DO_NO & "' ; " &
                                  " Delete T_DO WHERE DO_NO ='" & DO_NO & "' ; " &
                                  " Delete T_LOADINGNOTE WHERE LOAD_DOfull ='" & DO_NO & "' ; "
                cmd.ExecuteNonQuery()

                SqlCon.Close()
            End If
           


            SqlCon.Dispose()
            cmd.Dispose()


        End If
    End Sub


    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        SelectDO()
    End Sub
End Class