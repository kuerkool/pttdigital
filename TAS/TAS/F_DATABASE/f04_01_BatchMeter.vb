﻿'Imports TASData
Imports TAS_TOL.C01_Permission
Imports ExtendedErrorProvider
Public Class f04_01_BatchMeter
    Dim MyErrorProvider As New ErrorProviderExtended
    Dim MaxBatchNo As Integer
    Dim PotitionChg As Boolean

    Private Sub BatchMeter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOCATION' table. You can move, or remove it, as needed.
        Me.T_LOCATIONTableAdapter.Fill(Me.FPTDataSet.T_LOCATION)
        MeterGrid.SuspendLayout()


        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
        Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
        'TODO: This line of code loads data into the 'FPTDataSet.T_ISLAND' table. You can move, or remove it, as needed.
        Me.T_ISLANDTableAdapter.Fill(Me.FPTDataSet.T_ISLAND)
        'TODO: This line of code loads data into the 'FPTDataSet.T_BATCHMETER' table. You can move, or remove it, as needed.
        Me.T_BATCHMETERTableAdapter.Fill(Me.FPTDataSet.T_BATCHMETER)


        'Add controls one by one in error provider.
        MyErrorProvider.ClearAllErrorMessages()
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(MeterNameBox, "Meter name:")
        MyErrorProvider.Controls.Add(IslandNoComboBox, "Isand No")
        MyErrorProvider.Controls.Add(Batch_bay, "BAY :")
        MyErrorProvider.Controls.Add(ProMainComboBox, "Product main :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub SaveBatchMeter(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_BATCHMETER"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()
                If ds.HasChanges Then
                    da.Update(ds, "T_BATCHMETER")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    MeterGrid.SuspendLayout()
                    '''''''''''''''''''''''''''''''''''''''
                    'If MeterButton.Checked = True Then
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Top"
                    'Else
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Button"
                    'End If


                    'If MarkerEn.Checked = True Then
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "1"
                    'Else
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "0"
                    'End If


                    'If MeterEn.Checked = True Then
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = 10
                    'Else
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = 11
                    'End If
                    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Update_date") = Now

                    TBATCHMETERBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_BATCHMETER").EndCurrentEdit()
                    T_BATCHMETERTableAdapter.Update(FPTDataSet.T_BATCHMETER)
                    'SaveBatchMeter(FPTDataSet)
                    BatchMeter_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
            Catch ex As Exception

                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)

            End Try
        End If

    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TBATCHMETERBindingSource.CancelEdit()
        BatchMeter_Load(sender, e)

    End Sub

    Private Sub MeterNoBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not IsNumeric(sender.Text) Then
            ErrorProvider1.SetError(sender, "Not a numeric value.")
        Else
            ' Clear the error.
            ErrorProvider1.SetError(sender, "")
        End If

    End Sub

    Private Structure cell
        Dim rowIndex As Integer
        Dim columnIndex As Integer
    End Structure



    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.SuspendLayout()
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False

    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        MeterGrid.SuspendLayout()
        DetailGroup.Enabled = True
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterEn.Checked = True
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        Try

            Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
         Where Not r.IsNewRow _
         Select CInt(r.Cells(0).Value)
            'MaxBatchNo = cells.Max
            MeterNoBox.Text() = cells.Max + 1

        Catch ex As Exception

        End Try
        'Dim min As Integer = cells.Min
        'Dim minAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, min)}
        'Dim max As Integer = cells.Max
        'Dim maxAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, max)}
        'MeterNoBox.Text = max + 1
        'MeterNoBox.Focus()
        TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Top"
        TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "1"
        TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = "10"
        TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_DATE") = Now
        MeterTop.Checked = True
        MarkerEn.Checked = True
        MeterEn.Checked = True
        MeterNameBox.Focus()
        'MeterNoBox.Text = max + 1
    End Sub

    Private Sub TBATCHMETERBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TBATCHMETERBindingSource.PositionChanged
        PotitionChg = True
        Try

            ' Set Status Type Meter Top/Button
            If TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type").ToString() = "Top" Then
                MeterTop.Checked = True
            Else
                MeterButton.Checked = True
            End If
            ' Set Status Marker En/Dis
            If TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER").ToString() = "1" Then
                MarkerEn.Checked = True
            Else
                MarkerDis.Checked = True
            End If
            ' Set Meter Status  En/Dis
            If TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS").ToString() = "10" Then
                MeterEn.Checked = True
            Else
                MeterDis.Checked = True
            End If

            If BtEdit.Enabled = False Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Update_date") = Now
        Catch ex As Exception

        End Try


        Batch_bay.Items.Clear()
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        'Dim BAY_NUMBER As Integer = 0
        'Try
        '    Dim sql As String = "Select * from T_ISLAND order by ISLAND_NUMBER "
        '    Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        '    Dim dt As New DataTable
        '    da.Fill(dt)
        '    For i As Integer = 0 To IslandNoComboBox.Text - 2
        '        If UCase(dt.Rows(i).Item("ISLAND_BAYRIGHT_STATUS").ToString) = "EN" Then
        '            '     BAY_NUMBER += 1
        '        End If
        '        If UCase(dt.Rows(i).Item("ISLAND_BAYLEFT_STATUS").ToString) = "EN" Then
        '            '   BAY_NUMBER += 1
        '        End If
        '    Next

        'Catch ex As Exception

        'End Try
        Try
            Dim sql As String = "Select * from T_ISLAND where island_number = " & IslandNoComboBox.Text
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim dt As New DataTable
            da.Fill(dt)
            If UCase(dt.Rows(0).Item("ISLAND_BAYRIGHT_STATUS").ToString) = "EN" Then
                '   BAY_NUMBER += 1
                Batch_bay.Items.Add(dt.Rows(0).Item("ISLAND_RIGHT").ToString & " (Right Bay)")
            End If
            If UCase(dt.Rows(0).Item("ISLAND_BAYLEFT_STATUS").ToString) = "EN" Then
                '   BAY_NUMBER += 1
                Batch_bay.Items.Add(dt.Rows(0).Item("ISLAND_LEFT").ToString & " (Left Bay)")
            End If
        Catch ex As Exception
        End Try
        Try

            If Batch_bay.Items.Count > 1 Then
                Batch_bay.SelectedIndex = 0
                '  MessageBox.Show(UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString()))
                '  MessageBox.Show(InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())))
                If InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())) >= 1 Then
                    Batch_bay.SelectedIndex = 0
                End If

                Batch_bay.SelectedIndex = 1
                If InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())) >= 1 Then
                    Batch_bay.SelectedIndex = 1
                Else
                    Batch_bay.SelectedIndex = 0
                End If

            Else
                Batch_bay.SelectedIndex = 0

            End If


        Catch ex As Exception

        End Try


        PotitionChg = False
    End Sub


    Private Sub IslandNoComboBox_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IslandNoComboBox.Leave, ShipperComboBox.Leave, ProMainComboBox.Leave, ProBlendComboBox.Leave, ProBaseComboBox.Leave
        Dim ISLANDText As String
        ISLANDText = sender.text
        sender.SelectedIndex = sender.FindStringExact(sender.text)
        If sender.SelectedIndex < 0 Then
            sender.SelectedIndex = -1
            sender.Text = ""
        End If
        If (sender.SelectedIndex < 0) And (ISLANDText <> "") Then
            sender.SelectedIndex = -1
            f04_01_Island.Close()
            Me.AddOwnedForm(f04_01_Island)
            f04_01_Island.ShowType = 1
            f04_01_Island.ShowDialog()
            Me.RemoveOwnedForm(f04_01_Island)
            Me.T_ISLANDTableAdapter.Fill(Me.FPTDataSet.T_ISLAND)
            sender.SelectedIndex = sender.FindStringExact(ISLANDText)
        End If
    End Sub



    Private Sub MeterGrid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub


    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        MeterGrid.SuspendLayout()
    End Sub

    Private Sub MeterTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MeterTop.Click
        If sender.Checked = True Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Top"
    End Sub

    Private Sub MeterButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MeterButton.Click
        If sender.Checked = True Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Button"
    End Sub

    Private Sub MarkerEn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MarkerEn.Click
        If sender.Checked = True Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "1"
    End Sub

    Private Sub MarkerDis_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MarkerDis.Click
        If sender.Checked = True Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "0"
    End Sub

    Private Sub MeterEn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MeterEn.Click
        If sender.Checked = True Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = "10"
    End Sub

    Private Sub MeterDis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MeterDis.Click
        If sender.Checked = True Then TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = "11"
    End Sub


    Private Sub IslandNoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IslandNoComboBox.SelectedIndexChanged
        Batch_bay.Items.Clear()
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        'Dim BAY_NUMBER As Integer = 0
        'Try
        '    Dim sql As String = "Select * from T_ISLAND order by ISLAND_NUMBER "
        '    Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        '    Dim dt As New DataTable
        '    da.Fill(dt)
        '    For i As Integer = 0 To IslandNoComboBox.Text - 2
        '        If UCase(dt.Rows(i).Item("ISLAND_BAYRIGHT_STATUS").ToString) = "EN" Then
        '            BAY_NUMBER += 1
        '        End If
        '        If UCase(dt.Rows(i).Item("ISLAND_BAYLEFT_STATUS").ToString) = "EN" Then
        '            BAY_NUMBER += 1
        '        End If
        '    Next

        'Catch ex As Exception

        'End Try
        Try
            Dim sql As String = "Select * from T_ISLAND where island_number = " & IslandNoComboBox.Text
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim dt As New DataTable
            da.Fill(dt)
            If UCase(dt.Rows(0).Item("ISLAND_BAYRIGHT_STATUS").ToString) = "EN" Then
                '     BAY_NUMBER += 1
                Batch_bay.Items.Add(dt.Rows(0).Item("ISLAND_RIGHT").ToString & " (Right Bay)")
            End If
            If UCase(dt.Rows(0).Item("ISLAND_BAYLEFT_STATUS").ToString) = "EN" Then
                '   BAY_NUMBER += 1
                Batch_bay.Items.Add(dt.Rows(0).Item("ISLAND_LEFT").ToString & " (Left Bay)")
            End If
        Catch ex As Exception
        End Try
        Try

            If Batch_bay.Items.Count > 1 Then
                Batch_bay.SelectedIndex = 0
                '  MessageBox.Show(UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString()))
                '  MessageBox.Show(InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())))
                If InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())) >= 1 Then
                    Batch_bay.SelectedIndex = 0
                End If

                Batch_bay.SelectedIndex = 1
                If InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())) >= 1 Then
                    Batch_bay.SelectedIndex = 1
                Else
                    Batch_bay.SelectedIndex = 0
                End If

            Else
                Batch_bay.SelectedIndex = 0

            End If


        Catch ex As Exception

        End Try


    End Sub

    Private Sub Batch_bay_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Batch_bay.Leave
        sender.SelectedIndex = sender.FindStringExact(sender.text)
        If sender.SelectedIndex < 0 Then
            sender.SelectedIndex = -1
            sender.Text = ""
        End If
    End Sub

    Private Sub Batch_bay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Batch_bay.SelectedIndexChanged
        If ((BtEdit.Enabled = False) Or (Btadd.Enabled = False)) And PotitionChg = False Then
            Dim Batchbay() As String
            Batchbay = Split(Batch_bay.Text, " ")
            TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY") = Batchbay(0)

        End If
    End Sub



    Private Sub BatchMeter_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Batch_bay.Items.Clear()
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Dim BAY_NUMBER As Integer = 0
        Try
            Dim sql As String = "Select * from T_ISLAND order by ISLAND_NUMBER "
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim dt As New DataTable
            da.Fill(dt)
            For i As Integer = 0 To IslandNoComboBox.Text - 2
                If UCase(dt.Rows(i).Item("ISLAND_BAYRIGHT_STATUS").ToString) = "EN" Then
                    BAY_NUMBER += 1
                End If
                If UCase(dt.Rows(i).Item("ISLAND_BAYLEFT_STATUS").ToString) = "EN" Then
                    BAY_NUMBER += 1
                End If
            Next

        Catch ex As Exception

        End Try
        Try
            Dim sql As String = "Select * from T_ISLAND where island_number = " & IslandNoComboBox.Text
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim dt As New DataTable
            da.Fill(dt)
            If UCase(dt.Rows(0).Item("ISLAND_BAYRIGHT_STATUS").ToString) = "EN" Then
                BAY_NUMBER += 1
                Batch_bay.Items.Add(BAY_NUMBER.ToString & " (Right Bay)")
            End If
            If UCase(dt.Rows(0).Item("ISLAND_BAYLEFT_STATUS").ToString) = "EN" Then
                BAY_NUMBER += 1
                Batch_bay.Items.Add(BAY_NUMBER.ToString & " (Left Bay)")
            End If
        Catch ex As Exception
        End Try
        Try

            If Batch_bay.Items.Count > 1 Then
                Batch_bay.SelectedIndex = 0
                '  MessageBox.Show(UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString()))
                '  MessageBox.Show(InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())))
                If InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())) >= 1 Then
                    Batch_bay.SelectedIndex = 0
                End If

                Batch_bay.SelectedIndex = 1
                If InStr(UCase(Batch_bay.Text), UCase(TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_BAY").ToString())) >= 1 Then
                    Batch_bay.SelectedIndex = 1
                Else
                    Batch_bay.SelectedIndex = 0
                End If

            Else
                Batch_bay.SelectedIndex = 0

            End If


        Catch ex As Exception

        End Try

    End Sub


    Private Sub IslandNoComboBox_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles IslandNoComboBox.KeyUp
        If e.KeyCode = 13 Then
            IslandNoComboBox_Leave(sender, Nothing)
        End If
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_BATCHMETER.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_BATCHMETER.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_BATCHMETER.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_BATCHMETER.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TBATCHMETERBindingSource.Filter = StrFilter
        Else : TBATCHMETERBindingSource.RemoveFilter()
        End If

    End Sub

    Private Sub CheckBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.Click
        TBATCHMETERBindingSource.EndEdit()
    End Sub
End Class