﻿'Imports TASData
Imports TAS_TOL.C01_Permission
Imports ExtendedErrorProvider
Public Class f04_01_BADGE
    Dim MyErrorProvider As New ErrorProviderExtended
    Dim MaxBatchNo As Integer

    Private Sub BatchMeter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_BADGE' table. You can move, or remove it, as needed.
        Me.T_BADGETableAdapter.Fill(Me.FPTDataSet.T_BADGE)
        MeterGrid.SuspendLayout()

        'Add controls one by one in error provider.
        MyErrorProvider.ClearAllErrorMessages()
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        MyErrorProvider.Controls.Add(MeterNoBox, "Card No:")
        MyErrorProvider.Controls.Add(MeterNameBox, "Card Code:")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub SaveBatchMeter(ByVal ds As DataSet)
        'Update a dataset representing T_BADGE
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_BADGE"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()
                If ds.HasChanges Then
                    da.Update(ds, "T_BADGE")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    MeterGrid.SuspendLayout()
                    '''''''''''''''''''''''''''''''''''''''
                    'If MeterButton.Checked = True Then
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Top"
                    'Else
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Button"
                    'End If


                    'If MarkerEn.Checked = True Then
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "1"
                    'Else
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "0"
                    'End If


                    'If MeterEn.Checked = True Then
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = 10
                    'Else
                    '    TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = 11
                    'End If
                    ' TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Update_date") = Now
                    TBADGEBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_BADGE").EndCurrentEdit()
                    T_BADGETableAdapter.Update(FPTDataSet.T_BADGE)
                    'SaveBatchMeter(FPTDataSet)
                    BatchMeter_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
            Catch ex As Exception

                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)

            End Try
        End If

    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TBADGEBindingSource.CancelEdit()
        FPTDataSet.RejectChanges()
        BatchMeter_Load(sender, e)
    End Sub

    Private Sub MeterNoBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not IsNumeric(sender.Text) Then
            ErrorProvider1.SetError(sender, "Not a numeric value.")
        Else
            ' Clear the error.
            ErrorProvider1.SetError(sender, "")
        End If

    End Sub

    Private Structure cell
        Dim rowIndex As Integer
        Dim columnIndex As Integer
    End Structure



    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.SuspendLayout()
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False

    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        MeterGrid.SuspendLayout()
        DetailGroup.Enabled = True
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        'MeterEn.Checked = True
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        Try

            Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
         Where Not r.IsNewRow _
         Select CInt(r.Cells(0).Value)
            'MaxBatchNo = cells.Max
            MeterNoBox.Text() = cells.Max + 1

        Catch ex As Exception

        End Try
        'Dim min As Integer = cells.Min
        'Dim minAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, min)}
        'Dim max As Integer = cells.Max
        'Dim maxAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, max)}
        'MeterNoBox.Text = max + 1
        'MeterNoBox.Focus()
        'TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("Arm_type") = "Top"
        'TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_MARKER") = "1"
        'TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_STATUS") = "10"
        'TBATCHMETERBindingSource.Item(TBATCHMETERBindingSource.Position)("BATCH_DATE") = Now
        'MeterTop.Checked = True
        'MarkerEn.Checked = True
        'MeterEn.Checked = True
        MeterNoBox.Focus()
        'MeterNoBox.Text = max + 1
    End Sub







    Private Sub MeterGrid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub


    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        MeterGrid.SuspendLayout()
    End Sub



    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_BADGE.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_BADGE.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_BADGE.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_BADGE.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TBADGEBindingSource.Filter = StrFilter
        Else : TBADGEBindingSource.RemoveFilter()
        End If

    End Sub

End Class