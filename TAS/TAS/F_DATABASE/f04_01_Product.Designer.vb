﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_Product
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_Product))
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductnameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.P_SAPCODE = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.P_typeF = New System.Windows.Forms.RadioButton()
        Me.P_typeN = New System.Windows.Forms.RadioButton()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.P_COLOR = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.P_DENSITY = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.P_TEMP = New System.Windows.Forms.TextBox()
        Me.P_API = New System.Windows.Forms.TextBox()
        Me.P_TANK = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.P_MARKERDis = New System.Windows.Forms.RadioButton()
        Me.P_MARKEREn = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.P_INJECTDis = New System.Windows.Forms.RadioButton()
        Me.P_INJECTEN = New System.Windows.Forms.RadioButton()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.P_CDATE = New System.Windows.Forms.TextBox()
        Me.P_ADDT_RATIO = New System.Windows.Forms.TextBox()
        Me.P_ADDT_NAME = New System.Windows.Forms.TextBox()
        Me.P_BLEND = New System.Windows.Forms.TextBox()
        Me.P_SAPTYPE = New System.Windows.Forms.TextBox()
        Me.P_SISCODE = New System.Windows.Forms.TextBox()
        Me.P_CODE_CARD = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.P_NAME = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.P_UPDATE = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.P_CODE_DBATCH = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.P_CODE = New System.Windows.Forms.TextBox()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DetailGroup.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.ProductcodeDataGridViewTextBoxColumn, Me.ProductnameDataGridViewTextBoxColumn})
        Me.MeterGrid.DataSource = Me.TProductBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.MeterGrid.Location = New System.Drawing.Point(6, 37)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(268, 353)
        Me.MeterGrid.TabIndex = 1
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        Me.IDDataGridViewTextBoxColumn.Width = 50
        '
        'ProductcodeDataGridViewTextBoxColumn
        '
        Me.ProductcodeDataGridViewTextBoxColumn.DataPropertyName = "Product_code"
        Me.ProductcodeDataGridViewTextBoxColumn.HeaderText = "Product code"
        Me.ProductcodeDataGridViewTextBoxColumn.Name = "ProductcodeDataGridViewTextBoxColumn"
        Me.ProductcodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProductcodeDataGridViewTextBoxColumn.Width = 130
        '
        'ProductnameDataGridViewTextBoxColumn
        '
        Me.ProductnameDataGridViewTextBoxColumn.DataPropertyName = "Product_name"
        Me.ProductnameDataGridViewTextBoxColumn.HeaderText = "Product name"
        Me.ProductnameDataGridViewTextBoxColumn.Name = "ProductnameDataGridViewTextBoxColumn"
        Me.ProductnameDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProductnameDataGridViewTextBoxColumn.Width = 130
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.Label22)
        Me.DetailGroup.Controls.Add(Me.Label24)
        Me.DetailGroup.Controls.Add(Me.P_SAPCODE)
        Me.DetailGroup.Controls.Add(Me.GroupBox4)
        Me.DetailGroup.Controls.Add(Me.Label23)
        Me.DetailGroup.Controls.Add(Me.P_COLOR)
        Me.DetailGroup.Controls.Add(Me.Label21)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.Label20)
        Me.DetailGroup.Controls.Add(Me.Label19)
        Me.DetailGroup.Controls.Add(Me.P_DENSITY)
        Me.DetailGroup.Controls.Add(Me.Label8)
        Me.DetailGroup.Controls.Add(Me.P_TEMP)
        Me.DetailGroup.Controls.Add(Me.P_API)
        Me.DetailGroup.Controls.Add(Me.P_TANK)
        Me.DetailGroup.Controls.Add(Me.GroupBox1)
        Me.DetailGroup.Controls.Add(Me.GroupBox2)
        Me.DetailGroup.Controls.Add(Me.Label18)
        Me.DetailGroup.Controls.Add(Me.Label17)
        Me.DetailGroup.Controls.Add(Me.Label16)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.Label14)
        Me.DetailGroup.Controls.Add(Me.P_CDATE)
        Me.DetailGroup.Controls.Add(Me.P_ADDT_RATIO)
        Me.DetailGroup.Controls.Add(Me.P_ADDT_NAME)
        Me.DetailGroup.Controls.Add(Me.P_BLEND)
        Me.DetailGroup.Controls.Add(Me.P_SAPTYPE)
        Me.DetailGroup.Controls.Add(Me.P_SISCODE)
        Me.DetailGroup.Controls.Add(Me.P_CODE_CARD)
        Me.DetailGroup.Controls.Add(Me.Label7)
        Me.DetailGroup.Controls.Add(Me.Label10)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.Label9)
        Me.DetailGroup.Controls.Add(Me.P_NAME)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Controls.Add(Me.P_UPDATE)
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.Label5)
        Me.DetailGroup.Controls.Add(Me.Label4)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.P_CODE_DBATCH)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.P_CODE)
        Me.DetailGroup.Enabled = False
        Me.DetailGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DetailGroup.Location = New System.Drawing.Point(280, 30)
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.Size = New System.Drawing.Size(744, 361)
        Me.DetailGroup.TabIndex = 2
        Me.DetailGroup.TabStop = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Red
        Me.Label22.Location = New System.Drawing.Point(409, 80)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(20, 25)
        Me.Label22.TabIndex = 145
        Me.Label22.Text = "*"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(164, 80)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(77, 16)
        Me.Label24.TabIndex = 144
        Me.Label24.Text = "SAP Code :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_SAPCODE
        '
        Me.P_SAPCODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_number", True))
        Me.P_SAPCODE.Location = New System.Drawing.Point(247, 77)
        Me.P_SAPCODE.Name = "P_SAPCODE"
        Me.P_SAPCODE.Size = New System.Drawing.Size(156, 22)
        Me.P_SAPCODE.TabIndex = 143
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.P_typeF)
        Me.GroupBox4.Controls.Add(Me.P_typeN)
        Me.GroupBox4.Location = New System.Drawing.Point(247, 271)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(156, 73)
        Me.GroupBox4.TabIndex = 11
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Tag = ""
        '
        'P_typeF
        '
        Me.P_typeF.AutoSize = True
        Me.P_typeF.Checked = True
        Me.P_typeF.Location = New System.Drawing.Point(12, 43)
        Me.P_typeF.Name = "P_typeF"
        Me.P_typeF.Size = New System.Drawing.Size(61, 20)
        Me.P_typeF.TabIndex = 1
        Me.P_typeF.TabStop = True
        Me.P_typeF.Text = "Finish"
        Me.P_typeF.UseVisualStyleBackColor = True
        '
        'P_typeN
        '
        Me.P_typeN.AutoSize = True
        Me.P_typeN.Location = New System.Drawing.Point(12, 17)
        Me.P_typeN.Name = "P_typeN"
        Me.P_typeN.Size = New System.Drawing.Size(70, 20)
        Me.P_typeN.TabIndex = 0
        Me.P_typeN.Text = "Normal"
        Me.P_typeN.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(146, 288)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(95, 16)
        Me.Label23.TabIndex = 142
        Me.Label23.Text = "Product Type :"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_COLOR
        '
        Me.P_COLOR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.P_COLOR.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_color", True))
        Me.P_COLOR.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.P_COLOR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.P_COLOR.FormattingEnabled = True
        Me.P_COLOR.Location = New System.Drawing.Point(247, 105)
        Me.P_COLOR.Name = "P_COLOR"
        Me.P_COLOR.Size = New System.Drawing.Size(156, 23)
        Me.P_COLOR.TabIndex = 5
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Red
        Me.Label21.Location = New System.Drawing.Point(409, 246)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(20, 25)
        Me.Label21.TabIndex = 138
        Me.Label21.Text = "*"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Bcancel
        '
        Me.Bcancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bcancel.Location = New System.Drawing.Point(640, 325)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 25
        Me.Bcancel.Tag = "5"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bsave.Location = New System.Drawing.Point(558, 325)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 24
        Me.Bsave.Tag = "5"
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Location = New System.Drawing.Point(409, 52)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(20, 25)
        Me.Label20.TabIndex = 135
        Me.Label20.Text = "*"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Red
        Me.Label19.Location = New System.Drawing.Point(409, 24)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(20, 25)
        Me.Label19.TabIndex = 134
        Me.Label19.Text = "*"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'P_DENSITY
        '
        Me.P_DENSITY.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_API", True))
        Me.P_DENSITY.Location = New System.Drawing.Point(559, 105)
        Me.P_DENSITY.Name = "P_DENSITY"
        Me.P_DENSITY.Size = New System.Drawing.Size(156, 22)
        Me.P_DENSITY.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(490, 108)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Density :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_TEMP
        '
        Me.P_TEMP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_API", True))
        Me.P_TEMP.Location = New System.Drawing.Point(559, 77)
        Me.P_TEMP.Name = "P_TEMP"
        Me.P_TEMP.Size = New System.Drawing.Size(156, 22)
        Me.P_TEMP.TabIndex = 15
        '
        'P_API
        '
        Me.P_API.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_API", True))
        Me.P_API.Location = New System.Drawing.Point(559, 49)
        Me.P_API.Name = "P_API"
        Me.P_API.Size = New System.Drawing.Size(156, 22)
        Me.P_API.TabIndex = 14
        '
        'P_TANK
        '
        Me.P_TANK.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_Tank", True))
        Me.P_TANK.Location = New System.Drawing.Point(559, 21)
        Me.P_TANK.Name = "P_TANK"
        Me.P_TANK.Size = New System.Drawing.Size(156, 22)
        Me.P_TANK.TabIndex = 13
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.P_MARKERDis)
        Me.GroupBox1.Controls.Add(Me.P_MARKEREn)
        Me.GroupBox1.Location = New System.Drawing.Point(559, 224)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(156, 36)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        '
        'P_MARKERDis
        '
        Me.P_MARKERDis.AutoSize = True
        Me.P_MARKERDis.Checked = True
        Me.P_MARKERDis.Location = New System.Drawing.Point(94, 12)
        Me.P_MARKERDis.Name = "P_MARKERDis"
        Me.P_MARKERDis.Size = New System.Drawing.Size(44, 20)
        Me.P_MARKERDis.TabIndex = 1
        Me.P_MARKERDis.TabStop = True
        Me.P_MARKERDis.Text = "No"
        Me.P_MARKERDis.UseVisualStyleBackColor = True
        '
        'P_MARKEREn
        '
        Me.P_MARKEREn.AutoSize = True
        Me.P_MARKEREn.Location = New System.Drawing.Point(24, 12)
        Me.P_MARKEREn.Name = "P_MARKEREn"
        Me.P_MARKEREn.Size = New System.Drawing.Size(50, 20)
        Me.P_MARKEREn.TabIndex = 0
        Me.P_MARKEREn.Text = "Yes"
        Me.P_MARKEREn.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.P_INJECTDis)
        Me.GroupBox2.Controls.Add(Me.P_INJECTEN)
        Me.GroupBox2.Location = New System.Drawing.Point(559, 185)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(156, 36)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        '
        'P_INJECTDis
        '
        Me.P_INJECTDis.AutoSize = True
        Me.P_INJECTDis.Checked = True
        Me.P_INJECTDis.Location = New System.Drawing.Point(94, 12)
        Me.P_INJECTDis.Name = "P_INJECTDis"
        Me.P_INJECTDis.Size = New System.Drawing.Size(44, 20)
        Me.P_INJECTDis.TabIndex = 1
        Me.P_INJECTDis.TabStop = True
        Me.P_INJECTDis.Text = "No"
        Me.P_INJECTDis.UseVisualStyleBackColor = True
        '
        'P_INJECTEN
        '
        Me.P_INJECTEN.AutoSize = True
        Me.P_INJECTEN.Location = New System.Drawing.Point(24, 12)
        Me.P_INJECTEN.Name = "P_INJECTEN"
        Me.P_INJECTEN.Size = New System.Drawing.Size(50, 20)
        Me.P_INJECTEN.TabIndex = 0
        Me.P_INJECTEN.Text = "Yes"
        Me.P_INJECTEN.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(493, 238)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "Marker :"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(496, 80)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(53, 16)
        Me.Label17.TabIndex = 35
        Me.Label17.Text = "Temp. :"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(116, 220)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(125, 16)
        Me.Label16.TabIndex = 29
        Me.Label16.Text = "SAP Product Type :"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(50, 164)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(191, 16)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "Product Code Display of Card :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(9, 136)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(232, 16)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Product Code Display of batch Meter :"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_CDATE
        '
        Me.P_CDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_Date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.P_CDATE.Enabled = False
        Me.P_CDATE.Location = New System.Drawing.Point(559, 269)
        Me.P_CDATE.Name = "P_CDATE"
        Me.P_CDATE.Size = New System.Drawing.Size(156, 22)
        Me.P_CDATE.TabIndex = 22
        '
        'P_ADDT_RATIO
        '
        Me.P_ADDT_RATIO.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_additive_Ratio", True))
        Me.P_ADDT_RATIO.Location = New System.Drawing.Point(559, 161)
        Me.P_ADDT_RATIO.Name = "P_ADDT_RATIO"
        Me.P_ADDT_RATIO.Size = New System.Drawing.Size(156, 22)
        Me.P_ADDT_RATIO.TabIndex = 18
        '
        'P_ADDT_NAME
        '
        Me.P_ADDT_NAME.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_additive_name", True))
        Me.P_ADDT_NAME.Location = New System.Drawing.Point(559, 133)
        Me.P_ADDT_NAME.Name = "P_ADDT_NAME"
        Me.P_ADDT_NAME.Size = New System.Drawing.Size(156, 22)
        Me.P_ADDT_NAME.TabIndex = 17
        '
        'P_BLEND
        '
        Me.P_BLEND.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_Blendingpercen", True))
        Me.P_BLEND.Location = New System.Drawing.Point(247, 246)
        Me.P_BLEND.Name = "P_BLEND"
        Me.P_BLEND.Size = New System.Drawing.Size(156, 22)
        Me.P_BLEND.TabIndex = 10
        '
        'P_SAPTYPE
        '
        Me.P_SAPTYPE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_SAPType", True))
        Me.P_SAPTYPE.Location = New System.Drawing.Point(247, 217)
        Me.P_SAPTYPE.Name = "P_SAPTYPE"
        Me.P_SAPTYPE.Size = New System.Drawing.Size(156, 22)
        Me.P_SAPTYPE.TabIndex = 9
        '
        'P_SISCODE
        '
        Me.P_SISCODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_SAPCode", True))
        Me.P_SISCODE.Location = New System.Drawing.Point(247, 189)
        Me.P_SISCODE.Name = "P_SISCODE"
        Me.P_SISCODE.Size = New System.Drawing.Size(156, 22)
        Me.P_SISCODE.TabIndex = 8
        '
        'P_CODE_CARD
        '
        Me.P_CODE_CARD.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_Card", True))
        Me.P_CODE_CARD.Location = New System.Drawing.Point(247, 161)
        Me.P_CODE_CARD.Name = "P_CODE_CARD"
        Me.P_CODE_CARD.Size = New System.Drawing.Size(156, 22)
        Me.P_CODE_CARD.TabIndex = 7
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(514, 52)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 16)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "API :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(446, 136)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(103, 16)
        Me.Label10.TabIndex = 37
        Me.Label10.Text = "Additive Name :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(462, 300)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Update Date :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(461, 272)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 16)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Created date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(451, 164)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(98, 16)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Additive Ratio :"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(154, 249)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(87, 16)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Blending(%) :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_NAME
        '
        Me.P_NAME.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_name", True))
        Me.P_NAME.Location = New System.Drawing.Point(247, 21)
        Me.P_NAME.Name = "P_NAME"
        Me.P_NAME.Size = New System.Drawing.Size(156, 22)
        Me.P_NAME.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(141, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Product Name :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_UPDATE
        '
        Me.P_UPDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.P_UPDATE.Enabled = False
        Me.P_UPDATE.Location = New System.Drawing.Point(559, 297)
        Me.P_UPDATE.Name = "P_UPDATE"
        Me.P_UPDATE.Size = New System.Drawing.Size(156, 22)
        Me.P_UPDATE.TabIndex = 23
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(504, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 16)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "Tank :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(452, 199)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(97, 16)
        Me.Label5.TabIndex = 39
        Me.Label5.Text = "Inject Additive :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(121, 194)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 16)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "SIS Product Code :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(145, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Product Code :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_CODE_DBATCH
        '
        Me.P_CODE_DBATCH.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_Batch", True))
        Me.P_CODE_DBATCH.Location = New System.Drawing.Point(247, 133)
        Me.P_CODE_DBATCH.Name = "P_CODE_DBATCH"
        Me.P_CODE_DBATCH.Size = New System.Drawing.Size(156, 22)
        Me.P_CODE_DBATCH.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(146, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 16)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Product Color :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'P_CODE
        '
        Me.P_CODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TProductBindingSource, "Product_code", True))
        Me.P_CODE.Location = New System.Drawing.Point(247, 49)
        Me.P_CODE.Name = "P_CODE"
        Me.P_CODE.Size = New System.Drawing.Size(156, 22)
        Me.P_CODE.TabIndex = 4
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TProductBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(1030, 27)
        Me.BindingNavigator1.TabIndex = 3
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'f04_01_Product
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1030, 396)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.MeterGrid)
        Me.Controls.Add(Me.DetailGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "f04_01_Product"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "5"
        Me.Text = "Material  or Product"
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents P_TEMP As System.Windows.Forms.TextBox
    Friend WithEvents P_API As System.Windows.Forms.TextBox
    Friend WithEvents P_TANK As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents P_MARKERDis As System.Windows.Forms.RadioButton
    Friend WithEvents P_MARKEREn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents P_INJECTDis As System.Windows.Forms.RadioButton
    Friend WithEvents P_INJECTEN As System.Windows.Forms.RadioButton
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents P_CDATE As System.Windows.Forms.TextBox
    Friend WithEvents P_ADDT_RATIO As System.Windows.Forms.TextBox
    Friend WithEvents P_ADDT_NAME As System.Windows.Forms.TextBox
    Friend WithEvents P_BLEND As System.Windows.Forms.TextBox
    Friend WithEvents P_SAPTYPE As System.Windows.Forms.TextBox
    Friend WithEvents P_SISCODE As System.Windows.Forms.TextBox
    Friend WithEvents P_CODE_CARD As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents P_NAME As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents P_UPDATE As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents P_CODE_DBATCH As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents P_DENSITY As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductnameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents P_COLOR As System.Windows.Forms.ComboBox
    Friend WithEvents P_CODE As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents P_typeF As System.Windows.Forms.RadioButton
    Friend WithEvents P_typeN As System.Windows.Forms.RadioButton
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents P_SAPCODE As System.Windows.Forms.TextBox
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
End Class
