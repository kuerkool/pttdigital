﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_CustomerCompany
    Public Shared ShowType As Integer
    Public Shared CompanyCode, CompanyName As String
    Dim MyErrorProvider As New ErrorProviderExtended
    '  Dim Max_ID As Integer
    Private Sub CustomerCompany_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_Customer' table. You can move, or remove it, as needed.
        MeterGrid.SuspendLayout()
        Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
        '   Max_ID = FPTDataSet.T_Customer.Compute("MAX(ID)", "")
        '   Dim sql As String = "Select * from T_Customer"
        '  da = New SqlClient.SqlDataAdapter(sql, conn)

        'Add controls one by one in error provider.
        MyErrorProvider.ClearAllErrorMessages()
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(TbCusname, "Customer name")
        MyErrorProvider.Controls.Add(TbCusCode, "Customer Code")
        '  MyErrorProvider.Controls.Add(ProMainComboBox, "Product main :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
        'If Not CheckViewPermisstion(sender) Then
        '    Exit Sub
        'End If
    End Sub


    Private Function GetConnection() As SqlClient.SqlConnection


        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub SaveCustomer(ByVal ds As DataSet)
        'Update a dataset representing T_Customer
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_Customer "
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_Customer")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            TCustomerBindingSource.EndEdit()
            BindingContext(FPTDataSet, "T_Customer").EndCurrentEdit()
            SaveCustomer(FPTDataSet)
            CustomerCompany_Load(sender, e)
            Me.BringToFront()

        Finally

        End Try
    End Sub





    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        'Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
        DetailGroup.Enabled = True
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Dim dr As DataRow
                ' dr = TCustomerBindingSource
                CompanyCode = TbCusname.Text
                TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Update_date") = Now
                TCustomerBindingSource.EndEdit()
                BindingContext(FPTDataSet, "T_Customer").EndCurrentEdit()
                T_CustomerTableAdapter.Update(FPTDataSet.T_Customer)
                'SaveCustomer(FPTDataSet)
                CustomerCompany_Load(sender, e)
                Me.BringToFront()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
                Exit Sub
            End Try
            If ShowType > 0 Then
                Me.Close()
            Else : CompanyCode = ""
            End If
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TCustomerBindingSource.CancelEdit()
        CustomerCompany_Load(sender, e)
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_Date") = Now
        TbCusname.Focus()
    End Sub

    Private Sub DataGridView1_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub

    Private Sub TCustomerBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TCustomerBindingSource.PositionChanged
        Try
            If BtEdit.Enabled = False Then TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Update_date") = Now

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click

        Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        TbCusname.Focus()
    End Sub

    Private Sub CustomerCompany_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.TbCusname.Text = CompanyName
            Me.TbCusCode.Text = CompanyCode
            Me.TCustomerBindingSource.Item(TCustomerBindingSource.Position)("Customer_Date") = Now
            Me.Bsave.Focus()
        End If
    End Sub

    Private Sub CustomerCompany_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
    End Sub


    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_Customer.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_Customer.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_Customer.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '  Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_Customer.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TCustomerBindingSource.Filter = StrFilter
        Else : TCustomerBindingSource.RemoveFilter()
        End If

    End Sub

    'Private Sub BtDelete_MouseUp(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles BtDelete.MouseUp
    '    If Not CheckDelPermisstion(sender.tag) Then
    '        Bcancel.PerformClick()
    '        Exit Sub
    '    End If
    'End Sub
End Class