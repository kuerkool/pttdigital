﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_Setting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PresentText = New System.Windows.Forms.TextBox()
        Me.TSETTINGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.weighttimeText = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.T_SETTINGTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SETTINGTableAdapter()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        CType(Me.TSETTINGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PresentText
        '
        Me.PresentText.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGBindingSource, "PERCENT_WEIGHT", True))
        Me.PresentText.Location = New System.Drawing.Point(82, 8)
        Me.PresentText.Name = "PresentText"
        Me.PresentText.Size = New System.Drawing.Size(100, 20)
        Me.PresentText.TabIndex = 0
        '
        'TSETTINGBindingSource
        '
        Me.TSETTINGBindingSource.DataMember = "T_SETTING"
        Me.TSETTINGBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'weighttimeText
        '
        Me.weighttimeText.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGBindingSource, "TIME_WIGHT", True))
        Me.weighttimeText.Location = New System.Drawing.Point(82, 34)
        Me.weighttimeText.Name = "weighttimeText"
        Me.weighttimeText.Size = New System.Drawing.Size(100, 20)
        Me.weighttimeText.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "% Weight"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Time Weight"
        '
        'T_SETTINGTableAdapter
        '
        Me.T_SETTINGTableAdapter.ClearBeforeFill = True
        '
        'Bcancel
        '
        Me.Bcancel.Location = New System.Drawing.Point(123, 72)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 24
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Location = New System.Drawing.Point(42, 72)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 23
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'Setting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(221, 109)
        Me.Controls.Add(Me.Bcancel)
        Me.Controls.Add(Me.Bsave)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.weighttimeText)
        Me.Controls.Add(Me.PresentText)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "Setting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Setting"
        CType(Me.TSETTINGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PresentText As System.Windows.Forms.TextBox
    Friend WithEvents weighttimeText As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TSETTINGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SETTINGTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SETTINGTableAdapter
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
End Class
