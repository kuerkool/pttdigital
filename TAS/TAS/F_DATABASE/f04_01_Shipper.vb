﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_Shipper
    Public Shared ShowType As Integer
    Public Shipper_code As String
    Dim MyErrorProvider As New ErrorProviderExtended
    Private Sub Shipper_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MeterGrid.SuspendLayout()
        'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
        Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)

        'Add controls one by one in error provider.
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.ClearAllErrorMessages()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(SP_CODE, "Shipper Code:")
        MyErrorProvider.Controls.Add(SP_NAMEEN, "Shipper Name:")
        MyErrorProvider.Controls.Add(SP_NAMETH, "ชื่อผู้ขนส่งสินค้า:")
        'MyErrorProvider.Controls.Add(IsNumofMeter, "Product main :")

        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub SaveShipper(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_SHIPPER"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_SHIPPER")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub
    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        '   Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
        'Where Not r.IsNewRow _
        'Select CInt(r.Cells(0).Value)
        '   'MaxBatchNo = cells.Max
        '   MeterNoBox.Text() = cells.Max + 1
        'Dim min As Integer = cells.Min
        'Dim minAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, min)}
        'Dim max As Integer = cells.Max
        'Dim maxAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, max)}
        'MeterNoBox.Text = max + 1
        'MeterNoBox.Focus()
        TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_Date") = Now
        SP_CODE.Focus()
        'MeterNoBox.Text = max + 1
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("update_date") = Now
                Try
                    TSHIPPERBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_SHIPPER").EndCurrentEdit()
                    T_SHIPPERTableAdapter.Update(FPTDataSet.T_SHIPPER)
                    ' SaveShipper(FPTDataSet)
                    Shipper_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
                If ShowType > 0 Then
                    Me.Close()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TSHIPPERBindingSource.CancelEdit()
        Shipper_Load(sender, e)
    End Sub

    Private Sub Shipper_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.SP_CODE.Text = Shipper_code
            ' Me.TbCusCode.Text = Driver.D_trans.Text
            TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_Date") = Now
            Me.SP_CONNAME.Focus()
        End If
    End Sub

    Private Sub Shipper_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
    End Sub

    Private Sub SP_PROD1_Leave_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SP_PROD6.Leave, SP_PROD5.Leave, SP_PROD4.Leave, SP_PROD3.Leave, SP_PROD2.Leave, SP_PROD1.Leave
        Dim TransText As String
        TransText = sender.text
        sender.SelectedIndex = sender.FindStringExact(TransText)
        If (sender.SelectedIndex < 0) And (TransText <> "") Then
            sender.SelectedIndex = -1
            f04_01_Product.Close()
            Me.AddOwnedForm(f04_01_Product)
            f04_01_Product.ShowType = 1
            f04_01_Product.SpCode = TransText
            f04_01_Product.ShowDialog()
            Me.RemoveOwnedForm(f04_01_Product)
            Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
            sender.SelectedIndex = sender.FindStringExact(TransText)
        End If
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        SP_CONNAME.Focus()
    End Sub

    Private Sub TSHIPPERBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSHIPPERBindingSource.PositionChanged
        Try
            If BtEdit.Enabled = False Then TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("update_date") = Now

        Catch ex As Exception

        End Try
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_SHIPPER.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_SHIPPER.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_SHIPPER.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '    Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_SHIPPER.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TSHIPPERBindingSource.Filter = StrFilter
        Else : TSHIPPERBindingSource.RemoveFilter()
        End If

    End Sub
End Class