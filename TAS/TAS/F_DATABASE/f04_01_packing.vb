﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class Fpacking
    Public Shared ShowType As Integer
    Public Shared PCode As String
    Dim MyErrorProvider As New ErrorProviderExtended


    Private Sub packing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MeterGrid.SuspendLayout()
        'TODO: This line of code loads data into the 'FPTDataSet.T_PACKING' table. You can move, or remove it, as needed.
        Me.T_PACKINGTableAdapter.Fill(Me.FPTDataSet.T_PACKING)
        'Add controls one by one in error provider.
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.ClearAllErrorMessages()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(P_CODE, "Packing Code :")
        MyErrorProvider.Controls.Add(P_WEIGHT, "IsanPacking Weight :")
        MyErrorProvider.Controls.Add(P_CAPASITY, "Packing Capasity :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        TPACKINGBindingSource.Item(TPACKINGBindingSource.Position)("P_CRDATE") = Now
        P_CODE.Focus()
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub MeterGrid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_PACKING"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_PACKING")
                    ds.AcceptChanges()
                End If
            Finally
                da.Dispose()
            End Try
        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub
    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    TPACKINGBindingSource.Item(TPACKINGBindingSource.Position)("P_UPDATE") = Now
                    TPACKINGBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_PACKING").EndCurrentEdit()
                    T_PACKINGTableAdapter.Update(FPTDataSet.T_PACKING)
                    ' Save(FPTDataSet)
                    packing_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
            If ShowType > 0 Then
                Me.Close()
            Else : PCode = ""
            End If
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TPACKINGBindingSource.CancelEdit()
        packing_Load(sender, e)
    End Sub

    Private Sub TPACKINGBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TPACKINGBindingSource.PositionChanged
        Try
            If BtEdit.Enabled = False Then TPACKINGBindingSource.Item(TPACKINGBindingSource.Position)("P_UPDATE") = Now

        Catch ex As Exception

        End Try
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_PACKING.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_PACKING.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_PACKING.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '  Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_PACKING.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TPACKINGBindingSource.Filter = StrFilter
        Else : TPACKINGBindingSource.RemoveFilter()
        End If

    End Sub

    Private Sub Fpacking_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.P_CODE.Text = PCode
            ' Me.TPACKINGBindingSource.Item(TPACKINGBindingSource.Position)("Customer_Date") = Now
            Me.TPACKINGBindingSource.Item(TPACKINGBindingSource.Position)("P_CRDATE") = Now
            Me.P_WEIGHT.Focus()
        End If
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub Fpacking_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
    End Sub
End Class