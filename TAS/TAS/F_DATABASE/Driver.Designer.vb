﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Driver
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Driver))
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.BTLoadImage = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.d_Exdate = New System.Windows.Forms.DateTimePicker()
        Me.TDRIVERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.d_backDis = New System.Windows.Forms.RadioButton()
        Me.d_backEn = New System.Windows.Forms.RadioButton()
        Me.d_update = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.D_LICENSE_NO = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.d_addr = New System.Windows.Forms.RichTextBox()
        Me.d_Fax = New System.Windows.Forms.TextBox()
        Me.d_Phone = New System.Windows.Forms.TextBox()
        Me.d_poscode = New System.Windows.Forms.TextBox()
        Me.d_prov = New System.Windows.Forms.TextBox()
        Me.d_Dist = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.D_trans = New System.Windows.Forms.ComboBox()
        Me.T_COMPANYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.d_IdenNO = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.d_Credate = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.d_Name = New System.Windows.Forms.TextBox()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Driver_NUMBER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DriverCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Driver_BLACK_LIST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T_DRIVERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter()
        Me.T_COMPANYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter()
        Me.TableAdapterManager = New TAS_TOL.FPTDataSetTableAdapters.TableAdapterManager()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.DetailGroup.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.T_COMPANYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.BTLoadImage)
        Me.DetailGroup.Controls.Add(Me.PictureBox1)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.Label14)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.d_Exdate)
        Me.DetailGroup.Controls.Add(Me.GroupBox2)
        Me.DetailGroup.Controls.Add(Me.d_update)
        Me.DetailGroup.Controls.Add(Me.Label10)
        Me.DetailGroup.Controls.Add(Me.Label9)
        Me.DetailGroup.Controls.Add(Me.D_LICENSE_NO)
        Me.DetailGroup.Controls.Add(Me.Label4)
        Me.DetailGroup.Controls.Add(Me.d_addr)
        Me.DetailGroup.Controls.Add(Me.d_Fax)
        Me.DetailGroup.Controls.Add(Me.d_Phone)
        Me.DetailGroup.Controls.Add(Me.d_poscode)
        Me.DetailGroup.Controls.Add(Me.d_prov)
        Me.DetailGroup.Controls.Add(Me.d_Dist)
        Me.DetailGroup.Controls.Add(Me.Label8)
        Me.DetailGroup.Controls.Add(Me.Label7)
        Me.DetailGroup.Controls.Add(Me.Label5)
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.Label19)
        Me.DetailGroup.Controls.Add(Me.D_trans)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.d_IdenNO)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Controls.Add(Me.d_Credate)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.d_Name)
        Me.DetailGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DetailGroup.Location = New System.Drawing.Point(567, 28)
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.Size = New System.Drawing.Size(368, 601)
        Me.DetailGroup.TabIndex = 2
        Me.DetailGroup.TabStop = False
        '
        'BTLoadImage
        '
        Me.BTLoadImage.Location = New System.Drawing.Point(67, 104)
        Me.BTLoadImage.Name = "BTLoadImage"
        Me.BTLoadImage.Size = New System.Drawing.Size(115, 25)
        Me.BTLoadImage.TabIndex = 139
        Me.BTLoadImage.Text = "Choose a photo"
        Me.BTLoadImage.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.TDRIVERBindingSource, "Driver_Image", True))
        Me.PictureBox1.Location = New System.Drawing.Point(189, 104)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(159, 89)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 138
        Me.PictureBox1.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(348, 47)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(20, 25)
        Me.Label15.TabIndex = 137
        Me.Label15.Text = "*"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(348, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(20, 25)
        Me.Label14.TabIndex = 137
        Me.Label14.Text = "*"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Bcancel
        '
        Me.Bcancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Bcancel.Location = New System.Drawing.Point(275, 562)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 18
        Me.Bcancel.Tag = "3"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Location = New System.Drawing.Point(191, 562)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 17
        Me.Bsave.Tag = "3"
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'd_Exdate
        '
        Me.d_Exdate.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TDRIVERBindingSource, "Driver_DATE_END", True))
        Me.d_Exdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.d_Exdate.Location = New System.Drawing.Point(189, 438)
        Me.d_Exdate.Name = "d_Exdate"
        Me.d_Exdate.Size = New System.Drawing.Size(159, 22)
        Me.d_Exdate.TabIndex = 13
        '
        'TDRIVERBindingSource
        '
        Me.TDRIVERBindingSource.DataMember = "T_DRIVER"
        Me.TDRIVERBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.d_backDis)
        Me.GroupBox2.Controls.Add(Me.d_backEn)
        Me.GroupBox2.Location = New System.Drawing.Point(189, 463)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(159, 36)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        '
        'd_backDis
        '
        Me.d_backDis.AutoSize = True
        Me.d_backDis.Checked = True
        Me.d_backDis.Location = New System.Drawing.Point(76, 12)
        Me.d_backDis.Name = "d_backDis"
        Me.d_backDis.Size = New System.Drawing.Size(44, 20)
        Me.d_backDis.TabIndex = 9
        Me.d_backDis.TabStop = True
        Me.d_backDis.Text = "No"
        Me.d_backDis.UseVisualStyleBackColor = True
        '
        'd_backEn
        '
        Me.d_backEn.AutoSize = True
        Me.d_backEn.Location = New System.Drawing.Point(6, 13)
        Me.d_backEn.Name = "d_backEn"
        Me.d_backEn.Size = New System.Drawing.Size(50, 20)
        Me.d_backEn.TabIndex = 8
        Me.d_backEn.Text = "Yes"
        Me.d_backEn.UseVisualStyleBackColor = True
        '
        'd_update
        '
        Me.d_update.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.d_update.Enabled = False
        Me.d_update.Location = New System.Drawing.Point(189, 534)
        Me.d_update.Name = "d_update"
        Me.d_update.Size = New System.Drawing.Size(159, 22)
        Me.d_update.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(115, 477)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 16)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Black List :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(99, 443)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(84, 16)
        Me.Label9.TabIndex = 29
        Me.Label9.Text = "Expire Date :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'D_LICENSE_NO
        '
        Me.D_LICENSE_NO.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_NO", True))
        Me.D_LICENSE_NO.Location = New System.Drawing.Point(189, 410)
        Me.D_LICENSE_NO.Name = "D_LICENSE_NO"
        Me.D_LICENSE_NO.Size = New System.Drawing.Size(159, 22)
        Me.D_LICENSE_NO.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 413)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(151, 16)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "driving license Number :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'd_addr
        '
        Me.d_addr.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_ADDRESS", True))
        Me.d_addr.Location = New System.Drawing.Point(189, 201)
        Me.d_addr.Name = "d_addr"
        Me.d_addr.Size = New System.Drawing.Size(159, 64)
        Me.d_addr.TabIndex = 6
        Me.d_addr.Text = ""
        '
        'd_Fax
        '
        Me.d_Fax.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_fax", True))
        Me.d_Fax.Location = New System.Drawing.Point(189, 382)
        Me.d_Fax.Name = "d_Fax"
        Me.d_Fax.Size = New System.Drawing.Size(159, 22)
        Me.d_Fax.TabIndex = 11
        '
        'd_Phone
        '
        Me.d_Phone.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_phone", True))
        Me.d_Phone.Location = New System.Drawing.Point(189, 354)
        Me.d_Phone.Name = "d_Phone"
        Me.d_Phone.Size = New System.Drawing.Size(159, 22)
        Me.d_Phone.TabIndex = 10
        '
        'd_poscode
        '
        Me.d_poscode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_Zipcode", True))
        Me.d_poscode.Location = New System.Drawing.Point(189, 326)
        Me.d_poscode.Name = "d_poscode"
        Me.d_poscode.Size = New System.Drawing.Size(159, 22)
        Me.d_poscode.TabIndex = 9
        '
        'd_prov
        '
        Me.d_prov.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_Province", True))
        Me.d_prov.Location = New System.Drawing.Point(189, 298)
        Me.d_prov.Name = "d_prov"
        Me.d_prov.Size = New System.Drawing.Size(159, 22)
        Me.d_prov.TabIndex = 8
        '
        'd_Dist
        '
        Me.d_Dist.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_amphur", True))
        Me.d_Dist.Location = New System.Drawing.Point(189, 270)
        Me.d_Dist.Name = "d_Dist"
        Me.d_Dist.Size = New System.Drawing.Size(159, 22)
        Me.d_Dist.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(116, 301)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 16)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Province :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(129, 273)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 16)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "District :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(147, 385)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 16)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Fax :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(130, 357)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 16)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Phone :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(97, 329)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 16)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Postal code :"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(118, 204)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 16)
        Me.Label19.TabIndex = 22
        Me.Label19.Text = "Address :"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'D_trans
        '
        Me.D_trans.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.D_trans.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.D_trans.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TDRIVERBindingSource, "Driver_Company", True))
        Me.D_trans.DataSource = Me.T_COMPANYBindingSource
        Me.D_trans.DisplayMember = "COMPANY_NAME"
        Me.D_trans.DropDownWidth = 300
        Me.D_trans.FormattingEnabled = True
        Me.D_trans.Location = New System.Drawing.Point(189, 75)
        Me.D_trans.Name = "D_trans"
        Me.D_trans.Size = New System.Drawing.Size(159, 24)
        Me.D_trans.TabIndex = 5
        Me.D_trans.ValueMember = "COMPANY_ID"
        '
        'T_COMPANYBindingSource
        '
        Me.T_COMPANYBindingSource.DataMember = "T_COMPANY"
        Me.T_COMPANYBindingSource.DataSource = Me.FPTDataSet
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(92, 537)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Update Date :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(91, 509)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 16)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Created date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'd_IdenNO
        '
        Me.d_IdenNO.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_NUMBER", True))
        Me.d_IdenNO.Location = New System.Drawing.Point(189, 22)
        Me.d_IdenNO.MaxLength = 13
        Me.d_IdenNO.Name = "d_IdenNO"
        Me.d_IdenNO.Size = New System.Drawing.Size(159, 22)
        Me.d_IdenNO.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(81, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 16)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Driver Number :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'd_Credate
        '
        Me.d_Credate.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_Date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.d_Credate.Enabled = False
        Me.d_Credate.Location = New System.Drawing.Point(189, 506)
        Me.d_Credate.Name = "d_Credate"
        Me.d_Credate.Size = New System.Drawing.Size(159, 22)
        Me.d_Credate.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(64, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 16)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Name - last name :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(63, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 16)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Forwarding Agent :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'd_Name
        '
        Me.d_Name.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDRIVERBindingSource, "Driver_NAME", True))
        Me.d_Name.Location = New System.Drawing.Point(189, 47)
        Me.d_Name.Name = "d_Name"
        Me.d_Name.Size = New System.Drawing.Size(159, 22)
        Me.d_Name.TabIndex = 4
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn3, Me.Driver_NUMBER, Me.DriverCodeDataGridViewTextBoxColumn, Me.Driver_BLACK_LIST})
        Me.MeterGrid.DataSource = Me.TDRIVERBindingSource
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle8
        Me.MeterGrid.Location = New System.Drawing.Point(5, 35)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(549, 593)
        Me.MeterGrid.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "ID"
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Driver_NAME"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'Driver_NUMBER
        '
        Me.Driver_NUMBER.DataPropertyName = "Driver_NUMBER"
        Me.Driver_NUMBER.HeaderText = "Identification NO"
        Me.Driver_NUMBER.Name = "Driver_NUMBER"
        Me.Driver_NUMBER.ReadOnly = True
        Me.Driver_NUMBER.Width = 150
        '
        'DriverCodeDataGridViewTextBoxColumn
        '
        Me.DriverCodeDataGridViewTextBoxColumn.DataPropertyName = "Driver_Code"
        Me.DriverCodeDataGridViewTextBoxColumn.HeaderText = "Pin code"
        Me.DriverCodeDataGridViewTextBoxColumn.Name = "DriverCodeDataGridViewTextBoxColumn"
        Me.DriverCodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Driver_BLACK_LIST
        '
        Me.Driver_BLACK_LIST.DataPropertyName = "Driver_BLACK_LIST"
        Me.Driver_BLACK_LIST.HeaderText = "Back List"
        Me.Driver_BLACK_LIST.Name = "Driver_BLACK_LIST"
        Me.Driver_BLACK_LIST.ReadOnly = True
        Me.Driver_BLACK_LIST.Width = 50
        '
        'T_DRIVERTableAdapter
        '
        Me.T_DRIVERTableAdapter.ClearBeforeFill = True
        '
        'T_COMPANYTableAdapter
        '
        Me.T_COMPANYTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DateFormatTableAdapter = Nothing
        Me.TableAdapterManager.MenusTableAdapter = Nothing
        Me.TableAdapterManager.ProgramsTableAdapter = Nothing
        Me.TableAdapterManager.T_BATCH_LOST_TASTableAdapter = Nothing
        Me.TableAdapterManager.T_BATCH_LOT_TASTableAdapter = Nothing
        Me.TableAdapterManager.T_BATCHMETERTableAdapter = Nothing
        Me.TableAdapterManager.T_COMPANYTableAdapter = Me.T_COMPANYTableAdapter
        Me.TableAdapterManager.T_CustomerTableAdapter = Nothing
        Me.TableAdapterManager.T_DO1TableAdapter = Nothing
        Me.TableAdapterManager.T_DOTableAdapter = Nothing
        Me.TableAdapterManager.T_DRIVERTableAdapter = Me.T_DRIVERTableAdapter
        Me.TableAdapterManager.T_ISLANDTableAdapter = Nothing
        Me.TableAdapterManager.T_LOADINGNOTECOMPARTMENTTableAdapter = Nothing
        Me.TableAdapterManager.T_LOADINGNOTETableAdapter = Nothing
        Me.TableAdapterManager.T_LOCATIONTableAdapter = Nothing
        Me.TableAdapterManager.T_LOTableAdapter = Nothing
        Me.TableAdapterManager.T_PACKINGTableAdapter = Nothing
        Me.TableAdapterManager.T_ProductTableAdapter = Nothing
        Me.TableAdapterManager.T_SETTINGTableAdapter = Nothing
        Me.TableAdapterManager.T_SHIPMENT_TYPETableAdapter = Nothing
        Me.TableAdapterManager.T_SHIPMENTTableAdapter = Nothing
        Me.TableAdapterManager.T_SHIPPERTableAdapter = Nothing
        Me.TableAdapterManager.T_STATUSTableAdapter = Nothing
        Me.TableAdapterManager.T_STOTableAdapter = Nothing
        Me.TableAdapterManager.T_TANKFARMTableAdapter = Nothing
        Me.TableAdapterManager.T_TRUCKCOMPARTMENTTableAdapter = Nothing
        Me.TableAdapterManager.T_TRUCKTableAdapter = Nothing
        Me.TableAdapterManager.T_TRUCKTYPETableAdapter = Nothing
        Me.TableAdapterManager.T_UGRPTableAdapter = Nothing
        Me.TableAdapterManager.T_USERLOGINTableAdapter = Nothing
        Me.TableAdapterManager.T_USERTableAdapter = Nothing
        Me.TableAdapterManager.T_WEIGHTTableAdapter = Nothing
        Me.TableAdapterManager.TSETTINGPLANTTableAdapter = Nothing
        Me.TableAdapterManager.TUNITTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = TAS_TOL.FPTDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserRightsTableAdapter = Nothing
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TDRIVERBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(937, 27)
        Me.BindingNavigator1.TabIndex = 3
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "JPEG Files: (*.JPG;*.JPEG;*.JPE;*.JFIF)|*.JPG;*.JPEG;*.JPE;*.JFIF|GIF Files: (*.G" & _
    "IF)|*.GIF|PNG Files: (*.PNG)|*.PNG"
        '
        'Driver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.Bcancel
        Me.ClientSize = New System.Drawing.Size(937, 635)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.DetailGroup)
        Me.Controls.Add(Me.MeterGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Driver"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "3"
        Me.Text = "Driver"
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.T_COMPANYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents D_trans As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents d_IdenNO As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents d_Credate As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents d_Name As System.Windows.Forms.TextBox
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents d_update As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents D_LICENSE_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents d_addr As System.Windows.Forms.RichTextBox
    Friend WithEvents d_Fax As System.Windows.Forms.TextBox
    Friend WithEvents d_Phone As System.Windows.Forms.TextBox
    Friend WithEvents d_poscode As System.Windows.Forms.TextBox
    Friend WithEvents d_prov As System.Windows.Forms.TextBox
    Friend WithEvents d_Dist As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents d_backDis As System.Windows.Forms.RadioButton
    Friend WithEvents d_backEn As System.Windows.Forms.RadioButton
    Friend WithEvents d_Exdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DriverNUMBERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DriverNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TDRIVERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_DRIVERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter
    Friend WithEvents T_COMPANYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_COMPANYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter
    Friend WithEvents TableAdapterManager As TAS_TOL.FPTDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Driver_NUMBER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DriverCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Driver_BLACK_LIST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BTLoadImage As System.Windows.Forms.Button
End Class
