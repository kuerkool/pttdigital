﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_Island
    Public Shared ShowType As Integer
    Dim MyErrorProvider As New ErrorProviderExtended
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub SaveIsland(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_ISLAND"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_ISLAND")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub



    Private Sub Island_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_ISLAND' table. You can move, or remove it, as needed.
        MeterGrid.SuspendLayout()

        Me.T_ISLANDTableAdapter.Fill(Me.FPTDataSet.T_ISLAND)

        'Add controls one by one in error provider.
        MyErrorProvider.ClearAllErrorMessages()
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(IsNo, "Island No:")
        MyErrorProvider.Controls.Add(IsNumofArm, "Number of Arm")
        MyErrorProvider.Controls.Add(IsNumofMeter, "Number of Meter:")
        'MyErrorProvider.Controls.Add(IsNumofMeter, "Product main :")

        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub


    Private Sub TISLANDBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TISLANDBindingSource.PositionChanged
        Try

            ' Set BAYLEFT_STATUS En/Dis

            If UCase(TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYLEFT_STATUS").ToString()) = "EN" Then
                EnLeftBay.Checked = True
            Else
                DisLeftBay.Checked = True
            End If
            ' Set BAYRIGHT_STATUS En/Dis
            If UCase(TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYRIGHT_STATUS").ToString()) = "EN" Then
                IsEnRightBay.Checked = True
            Else
                IsDisRightbay.Checked = True
            End If
            ' Set ISLAND Status  En/Dis
            If UCase(TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_STATUS").ToString()) = "EN" Then
                IsEn.Checked = True
            Else
                Isdis.Checked = True
            End If

            If UCase(TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_TYPE").ToString()) = "TOP" Then
                ISTop.Checked = True
            Else
                IsBottom.Checked = True
            End If
            If UCase(TISLANDBindingSource.Item(TISLANDBindingSource.Position)("PortNetwork").ToString()) = "SINGLE" Then
                IsComSingle.Checked = True
            Else
                IsCommulti.Checked = True
            End If
            If BtEdit.Enabled = False Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("update_date") = Now
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        EnLeftBay.Checked = True
        IsEnRightBay.Checked = True
        IsEn.Checked = True
        IsComSingle.Checked = True
        ISTop.Enabled = True
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        '   Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
        'Where Not r.IsNewRow _
        'Select CInt(r.Cells(0).Value)
        '   'MaxBatchNo = cells.Max
        '   MeterNoBox.Text() = cells.Max + 1
        'Dim min As Integer = cells.Min
        'Dim minAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, min)}
        'Dim max As Integer = cells.Max
        'Dim maxAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, max)}
        'MeterNoBox.Text = max + 1
        'MeterNoBox.Focus()
        TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYLEFT_STATUS") = "EN"
        TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYRIGHT_STATUS") = "EN"
        TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_STATUS") = "EN"
        TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_TYPE") = "TOP"
        TISLANDBindingSource.Item(TISLANDBindingSource.Position)("PortNetwork") = "SINGLE"
        EnLeftBay.Checked = True
        IsEnRightBay.Checked = True
        IsEn.Checked = True
        ISTop.Checked = True
        IsComSingle.Checked = True
        TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_DATE") = Now
        IsNo.Focus()
        'MeterNoBox.Text = max + 1
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub MeterGrid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    MeterGrid.SuspendLayout()
                    '''''''''''''''''''''''''''''''''''''''
                    'If EnLeftBay.Checked = True Then
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYLEFT_STATUS") = "En"

                    'Else
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYLEFT_STATUS") = "Dis"
                    'End If
                    '' Set BAYRIGHT_STATUS En/Dis
                    'If IsEnRightBay.Checked = True Then
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYRIGHT_STATUS") = "En"

                    'Else
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYRIGHT_STATUS") = "Dis"
                    'End If

                    '' Set ISLAND Status  En/Dis
                    'If IsEn.Checked = True Then
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_STATUS") = "EN"

                    'Else
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_STATUS") = "Dis"
                    'End If


                    'If ISTop.Checked = True Then
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_TYPE") = "Top"

                    'Else
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_TYPE") = "Bottom"
                    'End If

                    'If IsComSingle.Checked = True Then
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("PortNetwork") = "Single"
                    'Else
                    '    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("PortNetwork") = "Multi"
                    'End If
                    TISLANDBindingSource.Item(TISLANDBindingSource.Position)("update_date") = Now
                    TISLANDBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_ISLAND").EndCurrentEdit()
                    T_ISLANDTableAdapter.Update(FPTDataSet.T_ISLAND)
                    ' SaveIsland(FPTDataSet)
                    Island_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TISLANDBindingSource.CancelEdit()
        Island_Load(sender, e)
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        MeterGrid.SuspendLayout()
    End Sub

    Private Sub EnLeftBay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnLeftBay.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYLEFT_STATUS") = "En"
    End Sub

    Private Sub DisLeftBay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DisLeftBay.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYLEFT_STATUS") = "Dis"
    End Sub

    Private Sub IsEnRightBay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsEnRightBay.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYRIGHT_STATUS") = "En"
    End Sub

    Private Sub IsDisRightbay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsDisRightbay.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_BAYRIGHT_STATUS") = "Dis"
    End Sub

    Private Sub ISTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ISTop.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_TYPE") = "Top"
    End Sub

    Private Sub IsBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsBottom.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_TYPE") = "Button"
    End Sub

    Private Sub IsEn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsEn.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_STATUS") = "EN"
    End Sub

    Private Sub Isdis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Isdis.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_STATUS") = "Dis"
    End Sub

    Private Sub IsComSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsComSingle.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("PortNetwork") = "Single"
    End Sub

    Private Sub IsCommulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IsCommulti.Click
        If sender.Checked = True Then TISLANDBindingSource.Item(TISLANDBindingSource.Position)("PortNetwork") = "Multi"
    End Sub

    Private Sub Island_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.IsNo.Text = f04_01_BatchMeter.IslandNoComboBox.Text
            Me.TISLANDBindingSource.Item(TISLANDBindingSource.Position)("ISLAND_DATE") = Now
            Me.IsNumofArm.Focus()
        End If
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_ISLAND.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_ISLAND.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_ISLAND.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        ' Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_ISLAND.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TISLANDBindingSource.Filter = StrFilter
        Else : TISLANDBindingSource.RemoveFilter()
        End If

    End Sub
End Class