﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_UserAccount
    Dim MyErrorProvider As New ErrorProviderExtended
    Private Sub UserAccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_USER' table. You can move, or remove it, as needed.
        Me.T_USERTableAdapter.Fill(Me.FPTDataSet.T_USER)
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(U_NAME_S, "Name - last name")
        MyErrorProvider.Controls.Add(U_NAME, "User Name")
        MyErrorProvider.Controls.Add(U_PASSWD, " Password")

        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        U_PASSWD_COMFIRM_LABEL.Visible = False
        U_PASSWD_COMFIRM.Visible = False
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
        BtUserRights.Enabled = CheckAddPermisstion(sender.tag)

    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_User"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_User")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub


    Private Sub MeterGrid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click

        If U_PASSWD.Text <> U_PASSWD_COMFIRM.Text Then
            MessageBox.Show("The passwords you typed do not match", "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            Exit Sub
        End If

        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    TUSERBindingSource.Item(TUSERBindingSource.Position)("Update_date") = Now
                    TUSERBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_User").EndCurrentEdit()
                    T_USERTableAdapter.Update(FPTDataSet.T_USER)
                    ' Save(FPTDataSet)
                    UserAccount_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TUSERBindingSource.CancelEdit()
        UserAccount_Load(sender, e)
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        U_PASSWD_COMFIRM.Visible = True
        U_PASSWD_COMFIRM_LABEL.Visible = True
        TUSERBindingSource.Item(TUSERBindingSource.Position)("U_DATE") = Now
        TUSERBindingSource.Item(TUSERBindingSource.Position)("U_STATUS") = "10"
        U_NAME_S.Focus()
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub TUSERBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TUSERBindingSource.PositionChanged
        Try

            Select Case TUSERBindingSource.Item(TUSERBindingSource.Position)("U_GROUP").ToString()
                Case 1
                    U_level1.Checked = True
                Case 2
                    U_level2.Checked = True
                Case 3
                    U_level3.Checked = True
                Case 4
                    U_level4.Checked = True
                Case 5
                    U_level5.Checked = True

            End Select
            ' Set  Status  En/Dis
            If TUSERBindingSource.Item(TUSERBindingSource.Position)("U_STATUS").ToString() = "10" Then
                Status_En.Checked = True
            Else
                Status_Dis.Checked = True
            End If

            If BtEdit.Enabled = False Then TUSERBindingSource.Item(TUSERBindingSource.Position)("Update_date") = Now
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Status_En_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Status_En.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_STATUS") = "10"
    End Sub

    Private Sub Status_Dis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Status_Dis.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_STATUS") = "11"
    End Sub

    Private Sub U_level1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles U_level1.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_GROUP") = "1"
    End Sub

    Private Sub U_level2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles U_level2.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_GROUP") = "2"
    End Sub

    Private Sub U_level3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles U_level3.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_GROUP") = "3"
    End Sub

    Private Sub U_level4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles U_level4.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_GROUP") = "4"
    End Sub

    Private Sub U_level5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles U_level5.Click
        If sender.Checked = True Then TUSERBindingSource.Item(TUSERBindingSource.Position)("U_GROUP") = "5"
    End Sub

    Private Sub U_PASSWD_COMFIRM_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles U_PASSWD_COMFIRM.Leave
        If U_PASSWD.Text <> U_PASSWD_COMFIRM.Text Then
            MessageBox.Show("The passwords you typed do not match", "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
        End If
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_USER.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_USER.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_USER.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '         Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_USER.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TUSERBindingSource.Filter = StrFilter
        Else : TUSERBindingSource.RemoveFilter()
        End If

    End Sub

    Private Sub BtUserRights_Click(sender As System.Object, e As System.EventArgs) Handles BtUserRights.Click
        Me.RemoveOwnedForm(f04_01_UserRight)
        Me.AddOwnedForm(f04_01_UserRight)
        f04_01_UserRight.Show()
        f04_01_UserRight.BringToFront()

        'f04_01_UserRight.ShowDialog()
        'f04_01_UserRight.Dispose()
    End Sub
End Class
