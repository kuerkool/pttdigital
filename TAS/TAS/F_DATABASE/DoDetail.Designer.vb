﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DoDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TDOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.T_DOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_DOTableAdapter()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.BtPost = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TSHIPMENTTYPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShipCancel = New System.Windows.Forms.Button()
        Me.ShipSave = New System.Windows.Forms.Button()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.DateTimePicker13 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker14 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker11 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker12 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker9 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker10 = New System.Windows.Forms.DateTimePicker()
        Me.TextBox40 = New System.Windows.Forms.TextBox()
        Me.TextBox39 = New System.Windows.Forms.TextBox()
        Me.TextBox38 = New System.Windows.Forms.TextBox()
        Me.TextBox37 = New System.Windows.Forms.TextBox()
        Me.TextBox36 = New System.Windows.Forms.TextBox()
        Me.TextBox35 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.T_SHIPMENT_TYPETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPMENT_TYPETableAdapter()
        CType(Me.TDOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.TSHIPMENTTYPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TDOBindingSource
        '
        Me.TDOBindingSource.DataMember = "T_DO"
        Me.TDOBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_DOTableAdapter
        '
        Me.T_DOTableAdapter.ClearBeforeFill = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(690, 534)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.TextBox3)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label69)
        Me.TabPage1.Controls.Add(Me.Label68)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label67)
        Me.TabPage1.Controls.Add(Me.Label66)
        Me.TabPage1.Controls.Add(Me.Label65)
        Me.TabPage1.Controls.Add(Me.Label64)
        Me.TabPage1.Controls.Add(Me.Label63)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.Label70)
        Me.TabPage1.Controls.Add(Me.Label61)
        Me.TabPage1.Controls.Add(Me.Label60)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label57)
        Me.TabPage1.Controls.Add(Me.Label56)
        Me.TabPage1.Controls.Add(Me.Bcancel)
        Me.TabPage1.Controls.Add(Me.Bsave)
        Me.TabPage1.Controls.Add(Me.DateTimePicker4)
        Me.TabPage1.Controls.Add(Me.Label30)
        Me.TabPage1.Controls.Add(Me.Label29)
        Me.TabPage1.Controls.Add(Me.Label28)
        Me.TabPage1.Controls.Add(Me.Label27)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.Label25)
        Me.TabPage1.Controls.Add(Me.Label24)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.TextBox30)
        Me.TabPage1.Controls.Add(Me.TextBox29)
        Me.TabPage1.Controls.Add(Me.TextBox28)
        Me.TabPage1.Controls.Add(Me.TextBox27)
        Me.TabPage1.Controls.Add(Me.TextBox26)
        Me.TabPage1.Controls.Add(Me.TextBox25)
        Me.TabPage1.Controls.Add(Me.TextBox24)
        Me.TabPage1.Controls.Add(Me.TextBox23)
        Me.TabPage1.Controls.Add(Me.TextBox22)
        Me.TabPage1.Controls.Add(Me.TextBox21)
        Me.TabPage1.Controls.Add(Me.TextBox8)
        Me.TabPage1.Controls.Add(Me.TextBox7)
        Me.TabPage1.Controls.Add(Me.TextBox2)
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(682, 506)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Delivery order"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(465, 433)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 25)
        Me.Label3.TabIndex = 143
        Me.Label3.Text = "*"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "ACT_QTY_DELV_SKU", True))
        Me.TextBox3.Location = New System.Drawing.Point(338, 434)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(121, 22)
        Me.TextBox3.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(34, 437)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(298, 16)
        Me.Label4.TabIndex = 141
        Me.Label4.Text = "Actual quantity delivered (in stock keeping units) :"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label69.ForeColor = System.Drawing.Color.Red
        Me.Label69.Location = New System.Drawing.Point(465, 407)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(20, 25)
        Me.Label69.TabIndex = 140
        Me.Label69.Text = "*"
        Me.Label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.Red
        Me.Label68.Location = New System.Drawing.Point(465, 376)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(20, 25)
        Me.Label68.TabIndex = 140
        Me.Label68.Text = "*"
        Me.Label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(465, 350)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 25)
        Me.Label5.TabIndex = 140
        Me.Label5.Text = "*"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label67.ForeColor = System.Drawing.Color.Red
        Me.Label67.Location = New System.Drawing.Point(465, 325)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(20, 25)
        Me.Label67.TabIndex = 140
        Me.Label67.Text = "*"
        Me.Label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.Red
        Me.Label66.Location = New System.Drawing.Point(465, 294)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(20, 25)
        Me.Label66.TabIndex = 140
        Me.Label66.Text = "*"
        Me.Label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.Red
        Me.Label65.Location = New System.Drawing.Point(465, 265)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(20, 25)
        Me.Label65.TabIndex = 140
        Me.Label65.Text = "*"
        Me.Label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.Red
        Me.Label64.Location = New System.Drawing.Point(465, 241)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(20, 25)
        Me.Label64.TabIndex = 140
        Me.Label64.Text = "*"
        Me.Label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label63.ForeColor = System.Drawing.Color.Red
        Me.Label63.Location = New System.Drawing.Point(465, 209)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(20, 25)
        Me.Label63.TabIndex = 140
        Me.Label63.Text = "*"
        Me.Label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label62.ForeColor = System.Drawing.Color.Red
        Me.Label62.Location = New System.Drawing.Point(465, 185)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(20, 25)
        Me.Label62.TabIndex = 140
        Me.Label62.Text = "*"
        Me.Label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.Red
        Me.Label70.Location = New System.Drawing.Point(465, 125)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(20, 25)
        Me.Label70.TabIndex = 140
        Me.Label70.Text = "*"
        Me.Label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label61.ForeColor = System.Drawing.Color.Red
        Me.Label61.Location = New System.Drawing.Point(465, 157)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(20, 25)
        Me.Label61.TabIndex = 140
        Me.Label61.Text = "*"
        Me.Label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.Red
        Me.Label60.Location = New System.Drawing.Point(465, 71)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(20, 25)
        Me.Label60.TabIndex = 140
        Me.Label60.Text = "*"
        Me.Label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.Red
        Me.Label59.Location = New System.Drawing.Point(465, 100)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(20, 25)
        Me.Label59.TabIndex = 140
        Me.Label59.Text = "*"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.Red
        Me.Label57.Location = New System.Drawing.Point(465, 44)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(20, 25)
        Me.Label57.TabIndex = 140
        Me.Label57.Text = "*"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.Red
        Me.Label56.Location = New System.Drawing.Point(465, 18)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(20, 25)
        Me.Label56.TabIndex = 140
        Me.Label56.Text = "*"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Bcancel
        '
        Me.Bcancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bcancel.Location = New System.Drawing.Point(595, 456)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 139
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bsave.Location = New System.Drawing.Point(514, 456)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 138
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TDOBindingSource, "pLAN_GI_DATE", True))
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker4.Location = New System.Drawing.Point(338, 126)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker4.TabIndex = 5
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(250, 409)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(82, 16)
        Me.Label30.TabIndex = 118
        Me.Label30.Text = "Weight Unit :"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(241, 381)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(91, 16)
        Me.Label29.TabIndex = 119
        Me.Label29.Text = "Gross weight :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(256, 353)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(76, 16)
        Me.Label28.TabIndex = 116
        Me.Label28.Text = "Net weight :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(260, 325)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(72, 16)
        Me.Label27.TabIndex = 117
        Me.Label27.Text = "Sales unit :"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(190, 297)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(142, 16)
        Me.Label26.TabIndex = 106
        Me.Label26.Text = "Base Unit of Measure :"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(79, 269)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(253, 16)
        Me.Label25.TabIndex = 107
        Me.Label25.Text = " Actual quantity delivered (in sales units)  :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(233, 240)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(99, 16)
        Me.Label24.TabIndex = 104
        Me.Label24.Text = "Batch Number :"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(216, 213)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(116, 16)
        Me.Label23.TabIndex = 105
        Me.Label23.Text = "Storage Location :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(288, 185)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(44, 16)
        Me.Label22.TabIndex = 110
        Me.Label22.Text = "Plant :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(236, 157)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(96, 16)
        Me.Label21.TabIndex = 111
        Me.Label21.Text = "Material code :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(159, 131)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(173, 16)
        Me.Label13.TabIndex = 136
        Me.Label13.Text = "Planned goods issue Date :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(207, 73)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(125, 16)
        Me.Label8.TabIndex = 123
        Me.Label8.Text = "Sales organization :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(181, 101)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(151, 16)
        Me.Label7.TabIndex = 124
        Me.Label7.Text = "SD document category :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(205, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 16)
        Me.Label2.TabIndex = 126
        Me.Label2.Text = "Delivery order Item :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(204, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 16)
        Me.Label1.TabIndex = 128
        Me.Label1.Text = "Delivery order NO. : "
        '
        'TextBox30
        '
        Me.TextBox30.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "wEIGHT_UNIT", True))
        Me.TextBox30.Location = New System.Drawing.Point(338, 406)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New System.Drawing.Size(121, 22)
        Me.TextBox30.TabIndex = 15
        '
        'TextBox29
        '
        Me.TextBox29.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "gROSS_WEIGHT", True))
        Me.TextBox29.Location = New System.Drawing.Point(338, 378)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.Size = New System.Drawing.Size(121, 22)
        Me.TextBox29.TabIndex = 14
        '
        'TextBox28
        '
        Me.TextBox28.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "nET_WEIGHT", True))
        Me.TextBox28.Location = New System.Drawing.Point(338, 350)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New System.Drawing.Size(121, 22)
        Me.TextBox28.TabIndex = 13
        '
        'TextBox27
        '
        Me.TextBox27.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sALES_UNIT", True))
        Me.TextBox27.Location = New System.Drawing.Point(338, 322)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New System.Drawing.Size(121, 22)
        Me.TextBox27.TabIndex = 12
        '
        'TextBox26
        '
        Me.TextBox26.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "bASE_UOM", True))
        Me.TextBox26.Location = New System.Drawing.Point(338, 294)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(121, 22)
        Me.TextBox26.TabIndex = 11
        '
        'TextBox25
        '
        Me.TextBox25.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "aCT_QTY_DELV_SU", True))
        Me.TextBox25.Location = New System.Drawing.Point(338, 266)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(121, 22)
        Me.TextBox25.TabIndex = 10
        '
        'TextBox24
        '
        Me.TextBox24.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "bATCH_NO", True))
        Me.TextBox24.Location = New System.Drawing.Point(338, 238)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(121, 22)
        Me.TextBox24.TabIndex = 9
        '
        'TextBox23
        '
        Me.TextBox23.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sTORAGE_LOC", True))
        Me.TextBox23.Location = New System.Drawing.Point(338, 210)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(121, 22)
        Me.TextBox23.TabIndex = 8
        '
        'TextBox22
        '
        Me.TextBox22.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "pLANT", True))
        Me.TextBox22.Location = New System.Drawing.Point(338, 182)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(121, 22)
        Me.TextBox22.TabIndex = 7
        '
        'TextBox21
        '
        Me.TextBox21.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "mAT_CODE", True))
        Me.TextBox21.Location = New System.Drawing.Point(338, 154)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New System.Drawing.Size(121, 22)
        Me.TextBox21.TabIndex = 6
        '
        'TextBox8
        '
        Me.TextBox8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sALES_ORG", True))
        Me.TextBox8.Location = New System.Drawing.Point(338, 70)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(121, 22)
        Me.TextBox8.TabIndex = 3
        '
        'TextBox7
        '
        Me.TextBox7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sD_DOC_CAT", True))
        Me.TextBox7.Location = New System.Drawing.Point(338, 98)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(121, 22)
        Me.TextBox7.TabIndex = 4
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "dO_ITEM", True))
        Me.TextBox2.Location = New System.Drawing.Point(338, 42)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(121, 22)
        Me.TextBox2.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "dO_NO", True))
        Me.TextBox1.Location = New System.Drawing.Point(338, 16)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(121, 22)
        Me.TextBox1.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TextBox14)
        Me.TabPage2.Controls.Add(Me.Label55)
        Me.TabPage2.Controls.Add(Me.TextBox6)
        Me.TabPage2.Controls.Add(Me.Label54)
        Me.TabPage2.Controls.Add(Me.BtPost)
        Me.TabPage2.Controls.Add(Me.ComboBox1)
        Me.TabPage2.Controls.Add(Me.ShipCancel)
        Me.TabPage2.Controls.Add(Me.ShipSave)
        Me.TabPage2.Controls.Add(Me.TextBox13)
        Me.TabPage2.Controls.Add(Me.DateTimePicker13)
        Me.TabPage2.Controls.Add(Me.DateTimePicker14)
        Me.TabPage2.Controls.Add(Me.DateTimePicker11)
        Me.TabPage2.Controls.Add(Me.DateTimePicker12)
        Me.TabPage2.Controls.Add(Me.DateTimePicker9)
        Me.TabPage2.Controls.Add(Me.DateTimePicker10)
        Me.TabPage2.Controls.Add(Me.TextBox40)
        Me.TabPage2.Controls.Add(Me.TextBox39)
        Me.TabPage2.Controls.Add(Me.TextBox38)
        Me.TabPage2.Controls.Add(Me.TextBox37)
        Me.TabPage2.Controls.Add(Me.TextBox36)
        Me.TabPage2.Controls.Add(Me.TextBox35)
        Me.TabPage2.Controls.Add(Me.TextBox17)
        Me.TabPage2.Controls.Add(Me.TextBox16)
        Me.TabPage2.Controls.Add(Me.TextBox15)
        Me.TabPage2.Controls.Add(Me.TextBox12)
        Me.TabPage2.Controls.Add(Me.TextBox5)
        Me.TabPage2.Controls.Add(Me.Label53)
        Me.TabPage2.Controls.Add(Me.Label52)
        Me.TabPage2.Controls.Add(Me.Label51)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.Label49)
        Me.TabPage2.Controls.Add(Me.Label48)
        Me.TabPage2.Controls.Add(Me.Label47)
        Me.TabPage2.Controls.Add(Me.Label46)
        Me.TabPage2.Controls.Add(Me.Label45)
        Me.TabPage2.Controls.Add(Me.Label44)
        Me.TabPage2.Controls.Add(Me.Label43)
        Me.TabPage2.Controls.Add(Me.Label42)
        Me.TabPage2.Controls.Add(Me.Label41)
        Me.TabPage2.Controls.Add(Me.Label40)
        Me.TabPage2.Controls.Add(Me.Label39)
        Me.TabPage2.Controls.Add(Me.Label38)
        Me.TabPage2.Controls.Add(Me.Label37)
        Me.TabPage2.Controls.Add(Me.Label36)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(682, 505)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Shipment"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TextBox14
        '
        Me.TextBox14.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "DO_STATUS", True))
        Me.TextBox14.Enabled = False
        Me.TextBox14.Location = New System.Drawing.Point(540, 135)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(121, 22)
        Me.TextBox14.TabIndex = 146
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(453, 141)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(81, 16)
        Me.Label55.TabIndex = 145
        Me.Label55.Text = "Post Status :"
        '
        'TextBox6
        '
        Me.TextBox6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "DO_POSTDATE", True))
        Me.TextBox6.Enabled = False
        Me.TextBox6.Location = New System.Drawing.Point(540, 108)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(121, 22)
        Me.TextBox6.TabIndex = 144
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(432, 114)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(102, 16)
        Me.Label54.TabIndex = 143
        Me.Label54.Text = "Post Date/time :"
        '
        'BtPost
        '
        Me.BtPost.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtPost.Location = New System.Drawing.Point(424, 460)
        Me.BtPost.Name = "BtPost"
        Me.BtPost.Size = New System.Drawing.Size(75, 30)
        Me.BtPost.TabIndex = 142
        Me.BtPost.Text = "&Post"
        Me.BtPost.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TDOBindingSource, "sHIP_TYPE", True))
        Me.ComboBox1.DataSource = Me.TSHIPMENTTYPEBindingSource
        Me.ComboBox1.DisplayMember = "CODE"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(208, 36)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 24)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.ValueMember = "CODE"
        '
        'TSHIPMENTTYPEBindingSource
        '
        Me.TSHIPMENTTYPEBindingSource.DataMember = "T_SHIPMENT_TYPE"
        Me.TSHIPMENTTYPEBindingSource.DataSource = Me.FPTDataSet
        '
        'ShipCancel
        '
        Me.ShipCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ShipCancel.Location = New System.Drawing.Point(586, 460)
        Me.ShipCancel.Name = "ShipCancel"
        Me.ShipCancel.Size = New System.Drawing.Size(75, 30)
        Me.ShipCancel.TabIndex = 141
        Me.ShipCancel.Text = "&Cancel"
        Me.ShipCancel.UseVisualStyleBackColor = True
        '
        'ShipSave
        '
        Me.ShipSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ShipSave.Location = New System.Drawing.Point(505, 460)
        Me.ShipSave.Name = "ShipSave"
        Me.ShipSave.Size = New System.Drawing.Size(75, 30)
        Me.ShipSave.TabIndex = 140
        Me.ShipSave.Text = "&Save"
        Me.ShipSave.UseVisualStyleBackColor = True
        '
        'TextBox13
        '
        Me.TextBox13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sHIPPdO_NO", True))
        Me.TextBox13.Location = New System.Drawing.Point(208, 349)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(121, 22)
        Me.TextBox13.TabIndex = 15
        '
        'DateTimePicker13
        '
        Me.DateTimePicker13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "aCT_LOAD_ETIME", True))
        Me.DateTimePicker13.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePicker13.Location = New System.Drawing.Point(540, 83)
        Me.DateTimePicker13.Name = "DateTimePicker13"
        Me.DateTimePicker13.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker13.TabIndex = 19
        '
        'DateTimePicker14
        '
        Me.DateTimePicker14.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TDOBindingSource, "aCT_LOAD_EDATE", True))
        Me.DateTimePicker14.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker14.Location = New System.Drawing.Point(540, 59)
        Me.DateTimePicker14.Name = "DateTimePicker14"
        Me.DateTimePicker14.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker14.TabIndex = 18
        '
        'DateTimePicker11
        '
        Me.DateTimePicker11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "aCT_LOAD_STIME", True))
        Me.DateTimePicker11.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePicker11.Location = New System.Drawing.Point(540, 35)
        Me.DateTimePicker11.Name = "DateTimePicker11"
        Me.DateTimePicker11.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker11.TabIndex = 17
        '
        'DateTimePicker12
        '
        Me.DateTimePicker12.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TDOBindingSource, "aCT_LOAD_SDATE", True))
        Me.DateTimePicker12.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker12.Location = New System.Drawing.Point(540, 11)
        Me.DateTimePicker12.Name = "DateTimePicker12"
        Me.DateTimePicker12.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker12.TabIndex = 16
        '
        'DateTimePicker9
        '
        Me.DateTimePicker9.CustomFormat = ""
        Me.DateTimePicker9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "cREATE_TIME", True))
        Me.DateTimePicker9.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePicker9.Location = New System.Drawing.Point(208, 108)
        Me.DateTimePicker9.Name = "DateTimePicker9"
        Me.DateTimePicker9.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker9.TabIndex = 5
        '
        'DateTimePicker10
        '
        Me.DateTimePicker10.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TDOBindingSource, "cREATE_DATE", True))
        Me.DateTimePicker10.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker10.Location = New System.Drawing.Point(208, 84)
        Me.DateTimePicker10.Name = "DateTimePicker10"
        Me.DateTimePicker10.Size = New System.Drawing.Size(121, 22)
        Me.DateTimePicker10.TabIndex = 4
        '
        'TextBox40
        '
        Me.TextBox40.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sHIPP_COND", True))
        Me.TextBox40.Location = New System.Drawing.Point(208, 325)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.Size = New System.Drawing.Size(121, 22)
        Me.TextBox40.TabIndex = 14
        '
        'TextBox39
        '
        Me.TextBox39.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sHIPP_TYPE", True))
        Me.TextBox39.Location = New System.Drawing.Point(208, 301)
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New System.Drawing.Size(121, 22)
        Me.TextBox39.TabIndex = 13
        '
        'TextBox38
        '
        Me.TextBox38.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sEAL_NO", True))
        Me.TextBox38.Location = New System.Drawing.Point(208, 277)
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New System.Drawing.Size(121, 22)
        Me.TextBox38.TabIndex = 12
        '
        'TextBox37
        '
        Me.TextBox37.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "wEIGHT_OUT", True))
        Me.TextBox37.Location = New System.Drawing.Point(208, 253)
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.Size = New System.Drawing.Size(121, 22)
        Me.TextBox37.TabIndex = 11
        '
        'TextBox36
        '
        Me.TextBox36.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "wEIGHT_IN", True))
        Me.TextBox36.Location = New System.Drawing.Point(208, 229)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New System.Drawing.Size(121, 22)
        Me.TextBox36.TabIndex = 10
        '
        'TextBox35
        '
        Me.TextBox35.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "fORWD_AGENT", True))
        Me.TextBox35.Location = New System.Drawing.Point(208, 205)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New System.Drawing.Size(121, 22)
        Me.TextBox35.TabIndex = 9
        '
        'TextBox17
        '
        Me.TextBox17.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "cAR_LINCENSE", True))
        Me.TextBox17.Location = New System.Drawing.Point(208, 181)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(121, 22)
        Me.TextBox17.TabIndex = 8
        '
        'TextBox16
        '
        Me.TextBox16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "dRIVER_NAME", True))
        Me.TextBox16.Location = New System.Drawing.Point(208, 157)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(121, 22)
        Me.TextBox16.TabIndex = 7
        '
        'TextBox15
        '
        Me.TextBox15.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "cONTAINER", True))
        Me.TextBox15.Location = New System.Drawing.Point(208, 133)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(121, 22)
        Me.TextBox15.TabIndex = 6
        '
        'TextBox12
        '
        Me.TextBox12.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "tRN_PLAN_POINT", True))
        Me.TextBox12.Location = New System.Drawing.Point(208, 61)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(121, 22)
        Me.TextBox12.TabIndex = 3
        '
        'TextBox5
        '
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TDOBindingSource, "sHIP_NO", True))
        Me.TextBox5.Location = New System.Drawing.Point(208, 13)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(121, 22)
        Me.TextBox5.TabIndex = 1
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(377, 88)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(157, 16)
        Me.Label53.TabIndex = 0
        Me.Label53.Text = "Actual Loading end time :"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(375, 64)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(159, 16)
        Me.Label52.TabIndex = 0
        Me.Label52.Text = "Actual Loading end date :"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(375, 40)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(159, 16)
        Me.Label51.TabIndex = 0
        Me.Label51.Text = "Actual Loading start time :"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(373, 16)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(161, 16)
        Me.Label50.TabIndex = 0
        Me.Label50.Text = "Actual Loading start date :"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(113, 352)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(89, 16)
        Me.Label49.TabIndex = 0
        Me.Label49.Text = "D/O Number :"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(78, 328)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(124, 16)
        Me.Label48.TabIndex = 0
        Me.Label48.Text = "Shipping condition :"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(100, 304)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(102, 16)
        Me.Label47.TabIndex = 0
        Me.Label47.Text = "Shipping Type :"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(142, 280)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(60, 16)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "Seal no :"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(125, 256)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(77, 16)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Weight out :"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(133, 232)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(69, 16)
        Me.Label44.TabIndex = 0
        Me.Label44.Text = "Weight in :"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(83, 208)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(119, 16)
        Me.Label43.TabIndex = 0
        Me.Label43.Text = "Forwarding Agent :"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(121, 184)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(81, 16)
        Me.Label42.TabIndex = 0
        Me.Label42.Text = "Car license :"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(115, 160)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(87, 16)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Driver name :"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(131, 136)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(71, 16)
        Me.Label40.TabIndex = 0
        Me.Label40.Text = "Container :"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(114, 112)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(88, 16)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Create Time :"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(118, 88)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(84, 16)
        Me.Label38.TabIndex = 0
        Me.Label38.Text = "Create date :"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(15, 64)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(187, 16)
        Me.Label37.TabIndex = 0
        Me.Label37.Text = "Transportation planning point :"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(103, 40)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(99, 16)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "Shipment type :"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(81, 16)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(121, 16)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Shipment Number :"
        '
        'T_SHIPMENT_TYPETableAdapter
        '
        Me.T_SHIPMENT_TYPETableAdapter.ClearBeforeFill = True
        '
        'DoDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(690, 534)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "DoDetail"
        Me.Text = "Do & Shipment Detail"
        CType(Me.TDOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.TSHIPMENTTYPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TDOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_DOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_DOTableAdapter
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox30 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox29 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TextBox39 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox38 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox37 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox36 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox35 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker13 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker14 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker11 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker12 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker9 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker10 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox40 As System.Windows.Forms.TextBox
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents ShipCancel As System.Windows.Forms.Button
    Friend WithEvents ShipSave As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TSHIPMENTTYPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPMENT_TYPETableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPMENT_TYPETableAdapter
    Friend WithEvents BtPost As System.Windows.Forms.Button
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
