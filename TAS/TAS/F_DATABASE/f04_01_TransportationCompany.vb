﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_TransportationCompany
    Public Shared ShowType As Integer
    Public Shared CompanyCode As String
    Dim MyErrorProvider As New ErrorProviderExtended
    Private Sub TransportationCompany_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MeterGrid.SuspendLayout()
        'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
        'Add controls one by one in error provider.
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.ClearAllErrorMessages()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(TbCusname, "Company name")
        MyErrorProvider.Controls.Add(TbCusCode, "Company Code")
        '  MyErrorProvider.Controls.Add(ProMainComboBox, "Product main :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        ' CompanyCode = ""
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_Company"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
                & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_Company")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub


    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                CompanyCode = TbCusname.Text
                TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("update_Date") = Now
                TCOMPANYBindingSource.EndEdit()
                BindingContext(FPTDataSet, "T_Company").EndCurrentEdit()
                T_COMPANYTableAdapter.Update(FPTDataSet.T_COMPANY)
                'Save(FPTDataSet)
                TransportationCompany_Load(sender, e)
                Me.BringToFront()
                If ShowType > 0 Then
                    Me.Close()
                Else : CompanyCode = ""
                End If
            Catch ex As Exception

                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)

            End Try
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TCOMPANYBindingSource.CancelEdit()
        TransportationCompany_Load(sender, e)
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_DATE") = Now
        TbCusname.Focus()
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub DataGridView1_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        TbCusname.Focus()
    End Sub

    Private Sub TransportationCompany_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.TbCusname.Text = CompanyCode
            Me.TbCusCode.Text = CompanyCode
            'Me.TbCusname.Text = Truck.VE_TRAN.Text
            'Me.TbCusCode.Text = Truck.VE_TRAN.Text
            Me.TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("COMPANY_DATE") = Now
            Me.Bsave.Focus()
        End If
    End Sub

    Private Sub TransportationCompany_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
    End Sub

    Private Sub TBPosCode_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TBPosCode.KeyPress, TbPhone.KeyPress, TBFax.KeyPress
        If e.KeyChar <> Chr(8) Then
            Dim strAllowableChars As String
            strAllowableChars = "0123456789-:/ "
            If InStr(strAllowableChars, e.KeyChar.ToString) = 0 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TCOMPANYBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TCOMPANYBindingSource.PositionChanged
        Try
            If BtEdit.Enabled = False Then TCOMPANYBindingSource.Item(TCOMPANYBindingSource.Position)("update_Date") = Now

        Catch ex As Exception

        End Try
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_COMPANY.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_COMPANY.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_COMPANY.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '  Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_COMPANY.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TCOMPANYBindingSource.Filter = StrFilter
        Else : TCOMPANYBindingSource.RemoveFilter()
        End If

    End Sub
End Class