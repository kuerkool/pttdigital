﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class F04_DENLOAD
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDecimalColumn1 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewDecimalColumn2 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GroupDescriptor1 As Telerik.WinControls.Data.GroupDescriptor = New Telerik.WinControls.Data.GroupDescriptor()
        Dim SortDescriptor1 As Telerik.WinControls.Data.SortDescriptor = New Telerik.WinControls.Data.SortDescriptor()
        Dim SortDescriptor2 As Telerik.WinControls.Data.SortDescriptor = New Telerik.WinControls.Data.SortDescriptor()
        Dim SortDescriptor3 As Telerik.WinControls.Data.SortDescriptor = New Telerik.WinControls.Data.SortDescriptor()
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        Me.RadGridView1 = New Telerik.WinControls.UI.RadGridView()
        Me.TYMEESSOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.T_YME_ESSOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_YME_ESSOTableAdapter()
        Me.BTSave = New Telerik.WinControls.UI.RadButton()
        Me.BTCancel = New Telerik.WinControls.UI.RadButton()
        Me.RadTextBox1 = New Telerik.WinControls.UI.RadTextBox()
        Me.TLOADTANKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.T_LOADTANKTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_LOADTANKTableAdapter()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        CType(Me.RadGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TYMEESSOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BTSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BTCancel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TLOADTANKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadGridView1
        '
        Me.RadGridView1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadGridView1.Location = New System.Drawing.Point(0, 0)
        '
        'RadGridView1
        '
        Me.RadGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        GridViewTextBoxColumn1.FieldName = "TANK_NO"
        GridViewTextBoxColumn1.HeaderText = "Tank No."
        GridViewTextBoxColumn1.IsAutoGenerated = True
        GridViewTextBoxColumn1.Name = "TANK_NO"
        GridViewTextBoxColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending
        GridViewTextBoxColumn1.Width = 88
        GridViewTextBoxColumn2.FieldName = "TANK_NO"
        GridViewTextBoxColumn2.HeaderText = "Tank No."
        GridViewTextBoxColumn2.Name = "column1"
        GridViewTextBoxColumn2.Width = 204
        GridViewDecimalColumn1.DataType = GetType(Double)
        GridViewDecimalColumn1.FieldName = "TEMP"
        GridViewDecimalColumn1.HeaderText = "Temp."
        GridViewDecimalColumn1.IsAutoGenerated = True
        GridViewDecimalColumn1.Name = "TEMP"
        GridViewDecimalColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending
        GridViewDecimalColumn1.Width = 299
        GridViewDecimalColumn2.DataType = GetType(Double)
        GridViewDecimalColumn2.FieldName = "DENSITY"
        GridViewDecimalColumn2.HeaderText = "Density"
        GridViewDecimalColumn2.IsAutoGenerated = True
        GridViewDecimalColumn2.Name = "DENSITY"
        GridViewDecimalColumn2.Width = 178
        Me.RadGridView1.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewDecimalColumn1, GridViewDecimalColumn2})
        Me.RadGridView1.MasterTemplate.DataSource = Me.TYMEESSOBindingSource
        Me.RadGridView1.MasterTemplate.EnableFiltering = True
        SortDescriptor1.PropertyName = "TANK_NO"
        GroupDescriptor1.GroupNames.AddRange(New Telerik.WinControls.Data.SortDescriptor() {SortDescriptor1})
        Me.RadGridView1.MasterTemplate.GroupDescriptors.AddRange(New Telerik.WinControls.Data.GroupDescriptor() {GroupDescriptor1})
        SortDescriptor2.PropertyName = "TANK_NO"
        SortDescriptor3.PropertyName = "TEMP"
        Me.RadGridView1.MasterTemplate.SortDescriptors.AddRange(New Telerik.WinControls.Data.SortDescriptor() {SortDescriptor2, SortDescriptor3})
        Me.RadGridView1.Name = "RadGridView1"
        Me.RadGridView1.Size = New System.Drawing.Size(722, 411)
        Me.RadGridView1.TabIndex = 0
        Me.RadGridView1.Text = "RadGridView1"
        Me.RadGridView1.ThemeName = "Office2010Blue"
        '
        'TYMEESSOBindingSource
        '
        Me.TYMEESSOBindingSource.DataMember = "T_YME_ESSO"
        Me.TYMEESSOBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_YME_ESSOTableAdapter
        '
        Me.T_YME_ESSOTableAdapter.ClearBeforeFill = True
        '
        'BTSave
        '
        Me.BTSave.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTSave.Location = New System.Drawing.Point(506, 444)
        Me.BTSave.Name = "BTSave"
        Me.BTSave.Size = New System.Drawing.Size(99, 29)
        Me.BTSave.TabIndex = 1
        Me.BTSave.Text = "Save"
        Me.BTSave.ThemeName = "Office2010Blue"
        '
        'BTCancel
        '
        Me.BTCancel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTCancel.Location = New System.Drawing.Point(611, 444)
        Me.BTCancel.Name = "BTCancel"
        Me.BTCancel.Size = New System.Drawing.Size(99, 29)
        Me.BTCancel.TabIndex = 2
        Me.BTCancel.Text = "Cancel"
        Me.BTCancel.ThemeName = "Office2010Blue"
        '
        'RadTextBox1
        '
        Me.RadTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TLOADTANKBindingSource, "LOAD_TANK", True))
        Me.RadTextBox1.Location = New System.Drawing.Point(90, 28)
        Me.RadTextBox1.Name = "RadTextBox1"
        Me.RadTextBox1.Size = New System.Drawing.Size(168, 20)
        Me.RadTextBox1.TabIndex = 3
        Me.RadTextBox1.ThemeName = "Office2010Blue"
        '
        'TLOADTANKBindingSource
        '
        Me.TLOADTANKBindingSource.DataMember = "T_LOADTANK"
        Me.TLOADTANKBindingSource.DataSource = Me.FPTDataSet
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.Location = New System.Drawing.Point(24, 27)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(61, 19)
        Me.RadLabel1.TabIndex = 4
        Me.RadLabel1.Text = "Tank No:"
        '
        'T_LOADTANKTableAdapter
        '
        Me.T_LOADTANKTableAdapter.ClearBeforeFill = True
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.Controls.Add(Me.RadTextBox1)
        Me.RadGroupBox1.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox1.HeaderText = "Loading Tank to cal. for Esso"
        Me.RadGroupBox1.Location = New System.Drawing.Point(12, 417)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(290, 64)
        Me.RadGroupBox1.TabIndex = 5
        Me.RadGroupBox1.Text = "Loading Tank to cal. for Esso"
        Me.RadGroupBox1.ThemeName = "Office2010Blue"
        '
        'F04_DENLOAD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(722, 485)
        Me.Controls.Add(Me.RadGroupBox1)
        Me.Controls.Add(Me.BTCancel)
        Me.Controls.Add(Me.BTSave)
        Me.Controls.Add(Me.RadGridView1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "F04_DENLOAD"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.Text = "Tank Density Configuration"
        Me.ThemeName = "Office2010Blue"
        CType(Me.RadGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TYMEESSOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BTSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BTCancel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TLOADTANKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        Me.RadGroupBox1.PerformLayout()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents RadGridView1 As Telerik.WinControls.UI.RadGridView
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TYMEESSOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_YME_ESSOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_YME_ESSOTableAdapter
    Friend WithEvents BTSave As Telerik.WinControls.UI.RadButton
    Friend WithEvents BTCancel As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadTextBox1 As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents TLOADTANKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_LOADTANKTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_LOADTANKTableAdapter
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
End Class

