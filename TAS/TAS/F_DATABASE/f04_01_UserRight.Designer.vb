﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_UserRight
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewCheckBoxColumn1 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Dim GridViewCheckBoxColumn2 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Dim GridViewCheckBoxColumn3 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Dim GridViewCheckBoxColumn4 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Dim GridViewCheckBoxColumn5 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_UserRight))
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        Me.RadGridView1 = New Telerik.WinControls.UI.RadGridView()
        Me.VUserRightsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadMultiColumnComboBox2 = New Telerik.WinControls.UI.RadMultiColumnComboBox()
        Me.Btcancel = New Telerik.WinControls.UI.RadButton()
        Me.BtSave = New Telerik.WinControls.UI.RadButton()
        Me.BtAdd = New Telerik.WinControls.UI.RadButton()
        Me.BTAddall = New Telerik.WinControls.UI.RadButton()
        Me.BtDel = New Telerik.WinControls.UI.RadButton()
        Me.BtDelALL = New Telerik.WinControls.UI.RadButton()
        Me.MenusBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenusTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.MenusTableAdapter()
        Me.VUserRightsTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.VUserRightsTableAdapter()
        Me.RadTreeView1 = New Telerik.WinControls.UI.RadTreeView()
        Me.ProgramsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProgramsTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.ProgramsTableAdapter()
        Me.RadPanel1 = New Telerik.WinControls.UI.RadPanel()
        Me.LUser = New Telerik.WinControls.UI.RadLabel()
        Me.RadDock1 = New Telerik.WinControls.UI.Docking.RadDock()
        Me.ToolWindow1 = New Telerik.WinControls.UI.Docking.ToolWindow()
        Me.ToolTabStrip2 = New Telerik.WinControls.UI.Docking.ToolTabStrip()
        Me.ToolWindow2 = New Telerik.WinControls.UI.Docking.ToolWindow()
        Me.ToolTabStrip1 = New Telerik.WinControls.UI.Docking.ToolTabStrip()
        Me.DocumentContainer1 = New Telerik.WinControls.UI.Docking.DocumentContainer()
        Me.DocumentTabStrip1 = New Telerik.WinControls.UI.Docking.DocumentTabStrip()
        Me.DocumentWindow1 = New Telerik.WinControls.UI.Docking.DocumentWindow()
        CType(Me.RadGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VUserRightsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btcancel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtAdd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BTAddall, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtDel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtDelALL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MenusBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTreeView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgramsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel1.SuspendLayout()
        CType(Me.LUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadDock1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadDock1.SuspendLayout()
        Me.ToolWindow1.SuspendLayout()
        CType(Me.ToolTabStrip2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolTabStrip2.SuspendLayout()
        Me.ToolWindow2.SuspendLayout()
        CType(Me.ToolTabStrip1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolTabStrip1.SuspendLayout()
        CType(Me.DocumentContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DocumentContainer1.SuspendLayout()
        CType(Me.DocumentTabStrip1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DocumentTabStrip1.SuspendLayout()
        Me.DocumentWindow1.SuspendLayout()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadGridView1
        '
        Me.RadGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadGridView1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGridView1.Location = New System.Drawing.Point(0, 0)
        '
        'RadGridView1
        '
        Me.RadGridView1.MasterTemplate.AllowAddNewRow = False
        Me.RadGridView1.MasterTemplate.AllowColumnReorder = False
        Me.RadGridView1.MasterTemplate.AllowDeleteRow = False
        Me.RadGridView1.MasterTemplate.AutoGenerateColumns = False
        Me.RadGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        GridViewTextBoxColumn1.FieldName = "ProgramName"
        GridViewTextBoxColumn1.HeaderText = "Program Name"
        GridViewTextBoxColumn1.IsAutoGenerated = True
        GridViewTextBoxColumn1.Name = "ProgramName"
        GridViewTextBoxColumn1.Width = 202
        GridViewTextBoxColumn2.FieldName = "MenuName"
        GridViewTextBoxColumn2.HeaderText = "Menu Name"
        GridViewTextBoxColumn2.IsAutoGenerated = True
        GridViewTextBoxColumn2.Name = "MenuName"
        GridViewTextBoxColumn2.Width = 202
        GridViewCheckBoxColumn1.FieldName = "chkview"
        GridViewCheckBoxColumn1.HeaderText = "View"
        GridViewCheckBoxColumn1.IsAutoGenerated = True
        GridViewCheckBoxColumn1.Name = "chkview"
        GridViewCheckBoxColumn1.Width = 59
        GridViewCheckBoxColumn2.FieldName = "chkadd"
        GridViewCheckBoxColumn2.HeaderText = "Add"
        GridViewCheckBoxColumn2.IsAutoGenerated = True
        GridViewCheckBoxColumn2.Name = "chkadd"
        GridViewCheckBoxColumn2.Width = 44
        GridViewCheckBoxColumn3.FieldName = "chkedit"
        GridViewCheckBoxColumn3.HeaderText = "Edit"
        GridViewCheckBoxColumn3.IsAutoGenerated = True
        GridViewCheckBoxColumn3.Name = "chkedit"
        GridViewCheckBoxColumn3.Width = 44
        GridViewCheckBoxColumn4.FieldName = "chkdel"
        GridViewCheckBoxColumn4.HeaderText = "Del."
        GridViewCheckBoxColumn4.IsAutoGenerated = True
        GridViewCheckBoxColumn4.Name = "chkdel"
        GridViewCheckBoxColumn4.Width = 44
        GridViewCheckBoxColumn5.FieldName = "chkprint"
        GridViewCheckBoxColumn5.HeaderText = "Print"
        GridViewCheckBoxColumn5.IsAutoGenerated = True
        GridViewCheckBoxColumn5.Name = "chkprint"
        GridViewCheckBoxColumn5.Width = 45
        Me.RadGridView1.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewCheckBoxColumn1, GridViewCheckBoxColumn2, GridViewCheckBoxColumn3, GridViewCheckBoxColumn4, GridViewCheckBoxColumn5})
        Me.RadGridView1.MasterTemplate.DataSource = Me.VUserRightsBindingSource
        Me.RadGridView1.MasterTemplate.EnableFiltering = True
        Me.RadGridView1.MasterTemplate.EnableGrouping = False
        Me.RadGridView1.MasterTemplate.MultiSelect = True
        Me.RadGridView1.Name = "RadGridView1"
        Me.RadGridView1.Size = New System.Drawing.Size(656, 262)
        Me.RadGridView1.TabIndex = 3
        Me.RadGridView1.Text = "RadGridView1"
        Me.RadGridView1.ThemeName = "Office2010Blue"
        '
        'VUserRightsBindingSource
        '
        Me.VUserRightsBindingSource.DataMember = "VUserRights"
        Me.VUserRightsBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.Location = New System.Drawing.Point(12, 12)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(42, 19)
        Me.RadLabel1.TabIndex = 4
        Me.RadLabel1.Text = "User :"
        Me.RadLabel1.ThemeName = "Office2010Blue"
        '
        'RadMultiColumnComboBox2
        '
        '
        'RadMultiColumnComboBox2.NestedRadGridView
        '
        Me.RadMultiColumnComboBox2.EditorControl.BackColor = System.Drawing.SystemColors.Window
        Me.RadMultiColumnComboBox2.EditorControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RadMultiColumnComboBox2.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadMultiColumnComboBox2.EditorControl.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.AllowAddNewRow = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.AllowCellContextMenu = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.AllowColumnChooser = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.EnableGrouping = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.ShowFilteringRow = False
        Me.RadMultiColumnComboBox2.EditorControl.Name = "NestedRadGridView"
        Me.RadMultiColumnComboBox2.EditorControl.ReadOnly = True
        Me.RadMultiColumnComboBox2.EditorControl.ShowGroupPanel = False
        Me.RadMultiColumnComboBox2.EditorControl.Size = New System.Drawing.Size(240, 150)
        Me.RadMultiColumnComboBox2.EditorControl.TabIndex = 0
        Me.RadMultiColumnComboBox2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadMultiColumnComboBox2.Location = New System.Drawing.Point(362, 110)
        Me.RadMultiColumnComboBox2.Name = "RadMultiColumnComboBox2"
        Me.RadMultiColumnComboBox2.Size = New System.Drawing.Size(100, 20)
        Me.RadMultiColumnComboBox2.TabIndex = 7
        Me.RadMultiColumnComboBox2.TabStop = False
        Me.RadMultiColumnComboBox2.Text = "RadMultiColumnComboBox2"
        Me.RadMultiColumnComboBox2.ThemeName = "Office2010Blue"
        '
        'Btcancel
        '
        Me.Btcancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btcancel.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.Btcancel.Location = New System.Drawing.Point(928, 8)
        Me.Btcancel.Name = "Btcancel"
        Me.Btcancel.Size = New System.Drawing.Size(110, 34)
        Me.Btcancel.TabIndex = 14
        Me.Btcancel.Text = "Cancel"
        Me.Btcancel.ThemeName = "Breeze"
        '
        'BtSave
        '
        Me.BtSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtSave.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.BtSave.Location = New System.Drawing.Point(812, 8)
        Me.BtSave.Name = "BtSave"
        Me.BtSave.Size = New System.Drawing.Size(110, 34)
        Me.BtSave.TabIndex = 13
        Me.BtSave.Text = "Save"
        Me.BtSave.ThemeName = "Breeze"
        '
        'BtAdd
        '
        Me.BtAdd.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BtAdd.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.BtAdd.Location = New System.Drawing.Point(10, 27)
        Me.BtAdd.Name = "BtAdd"
        Me.BtAdd.Size = New System.Drawing.Size(30, 34)
        Me.BtAdd.TabIndex = 15
        Me.BtAdd.Text = ">"
        Me.BtAdd.ThemeName = "Breeze"
        '
        'BTAddall
        '
        Me.BTAddall.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BTAddall.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.BTAddall.Location = New System.Drawing.Point(10, 67)
        Me.BTAddall.Name = "BTAddall"
        Me.BTAddall.Size = New System.Drawing.Size(30, 34)
        Me.BTAddall.TabIndex = 16
        Me.BTAddall.Text = ">>"
        Me.BTAddall.ThemeName = "Breeze"
        '
        'BtDel
        '
        Me.BtDel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BtDel.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.BtDel.Location = New System.Drawing.Point(10, 107)
        Me.BtDel.Name = "BtDel"
        Me.BtDel.Size = New System.Drawing.Size(30, 34)
        Me.BtDel.TabIndex = 17
        Me.BtDel.Text = "<"
        Me.BtDel.ThemeName = "Breeze"
        '
        'BtDelALL
        '
        Me.BtDelALL.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BtDelALL.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.BtDelALL.Location = New System.Drawing.Point(10, 147)
        Me.BtDelALL.Name = "BtDelALL"
        Me.BtDelALL.Size = New System.Drawing.Size(30, 34)
        Me.BtDelALL.TabIndex = 18
        Me.BtDelALL.Text = "<<"
        Me.BtDelALL.ThemeName = "Breeze"
        '
        'MenusBindingSource
        '
        Me.MenusBindingSource.DataMember = "Menus"
        Me.MenusBindingSource.DataSource = Me.FPTDataSet
        '
        'MenusTableAdapter
        '
        Me.MenusTableAdapter.ClearBeforeFill = True
        '
        'VUserRightsTableAdapter
        '
        Me.VUserRightsTableAdapter.ClearBeforeFill = True
        '
        'RadTreeView1
        '
        Me.RadTreeView1.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.RadTreeView1.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadTreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadTreeView1.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.RadTreeView1.ForeColor = System.Drawing.Color.Black
        Me.RadTreeView1.Location = New System.Drawing.Point(0, 0)
        Me.RadTreeView1.MultiSelect = True
        Me.RadTreeView1.Name = "RadTreeView1"
        Me.RadTreeView1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadTreeView1.Size = New System.Drawing.Size(302, 272)
        Me.RadTreeView1.SpacingBetweenNodes = -1
        Me.RadTreeView1.TabIndex = 19
        Me.RadTreeView1.Text = "RadTreeView1"
        Me.RadTreeView1.ThemeName = "Office2010Blue"
        '
        'ProgramsBindingSource
        '
        Me.ProgramsBindingSource.DataMember = "Programs"
        Me.ProgramsBindingSource.DataSource = Me.FPTDataSet
        '
        'ProgramsTableAdapter
        '
        Me.ProgramsTableAdapter.ClearBeforeFill = True
        '
        'RadPanel1
        '
        Me.RadPanel1.Controls.Add(Me.LUser)
        Me.RadPanel1.Controls.Add(Me.BtSave)
        Me.RadPanel1.Controls.Add(Me.Btcancel)
        Me.RadPanel1.Controls.Add(Me.RadLabel1)
        Me.RadPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadPanel1.Location = New System.Drawing.Point(0, 0)
        Me.RadPanel1.Name = "RadPanel1"
        Me.RadPanel1.Size = New System.Drawing.Size(1046, 50)
        Me.RadPanel1.TabIndex = 20
        Me.RadPanel1.ThemeName = "Office2010Blue"
        '
        'LUser
        '
        Me.LUser.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LUser.Location = New System.Drawing.Point(60, 12)
        Me.LUser.Name = "LUser"
        Me.LUser.Size = New System.Drawing.Size(17, 19)
        Me.LUser.TabIndex = 15
        Me.LUser.Text = "--"
        Me.LUser.ThemeName = "Office2010Blue"
        '
        'RadDock1
        '
        Me.RadDock1.ActiveWindow = Me.ToolWindow1
        Me.RadDock1.Controls.Add(Me.ToolTabStrip2)
        Me.RadDock1.Controls.Add(Me.ToolTabStrip1)
        Me.RadDock1.Controls.Add(Me.DocumentContainer1)
        Me.RadDock1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadDock1.IsCleanUpTarget = True
        Me.RadDock1.Location = New System.Drawing.Point(0, 50)
        Me.RadDock1.MainDocumentContainer = Me.DocumentContainer1
        Me.RadDock1.Name = "RadDock1"
        Me.RadDock1.Padding = New System.Windows.Forms.Padding(0)
        '
        '
        '
        Me.RadDock1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.RadDock1.Size = New System.Drawing.Size(1046, 296)
        Me.RadDock1.TabIndex = 21
        Me.RadDock1.TabStop = False
        Me.RadDock1.Text = "RadDock1"
        Me.RadDock1.ThemeName = "Office2010Blue"
        '
        'ToolWindow1
        '
        Me.ToolWindow1.Caption = Nothing
        Me.ToolWindow1.Controls.Add(Me.BtAdd)
        Me.ToolWindow1.Controls.Add(Me.BtDel)
        Me.ToolWindow1.Controls.Add(Me.BTAddall)
        Me.ToolWindow1.Controls.Add(Me.BtDelALL)
        Me.ToolWindow1.DocumentButtons = Telerik.WinControls.UI.Docking.DocumentStripButtons.ActiveWindowList
        Me.ToolWindow1.Location = New System.Drawing.Point(1, 22)
        Me.ToolWindow1.Name = "ToolWindow1"
        Me.ToolWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked
        Me.ToolWindow1.Size = New System.Drawing.Size(64, 272)
        Me.ToolWindow1.Text = "Control"
        Me.ToolWindow1.ToolCaptionButtons = Telerik.WinControls.UI.Docking.ToolStripCaptionButtons.AutoHide
        '
        'ToolTabStrip2
        '
        Me.ToolTabStrip2.CanUpdateChildIndex = True
        Me.ToolTabStrip2.Controls.Add(Me.ToolWindow2)
        Me.ToolTabStrip2.Location = New System.Drawing.Point(0, 0)
        Me.ToolTabStrip2.Name = "ToolTabStrip2"
        '
        '
        '
        Me.ToolTabStrip2.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.ToolTabStrip2.SelectedIndex = 0
        Me.ToolTabStrip2.Size = New System.Drawing.Size(304, 296)
        Me.ToolTabStrip2.SizeInfo.AbsoluteSize = New System.Drawing.Size(304, 200)
        Me.ToolTabStrip2.SizeInfo.SplitterCorrection = New System.Drawing.Size(104, 0)
        Me.ToolTabStrip2.TabIndex = 2
        Me.ToolTabStrip2.TabStop = False
        Me.ToolTabStrip2.ThemeName = "Office2010Blue"
        '
        'ToolWindow2
        '
        Me.ToolWindow2.Caption = Nothing
        Me.ToolWindow2.Controls.Add(Me.RadTreeView1)
        Me.ToolWindow2.Location = New System.Drawing.Point(1, 22)
        Me.ToolWindow2.Name = "ToolWindow2"
        Me.ToolWindow2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked
        Me.ToolWindow2.Size = New System.Drawing.Size(302, 272)
        Me.ToolWindow2.Text = "Programs"
        '
        'ToolTabStrip1
        '
        Me.ToolTabStrip1.CanUpdateChildIndex = True
        Me.ToolTabStrip1.Controls.Add(Me.ToolWindow1)
        Me.ToolTabStrip1.Location = New System.Drawing.Point(308, 0)
        Me.ToolTabStrip1.Name = "ToolTabStrip1"
        '
        '
        '
        Me.ToolTabStrip1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.ToolTabStrip1.SelectedIndex = 0
        Me.ToolTabStrip1.Size = New System.Drawing.Size(66, 296)
        Me.ToolTabStrip1.SizeInfo.AbsoluteSize = New System.Drawing.Size(66, 200)
        Me.ToolTabStrip1.SizeInfo.SplitterCorrection = New System.Drawing.Size(-134, 0)
        Me.ToolTabStrip1.TabIndex = 1
        Me.ToolTabStrip1.TabStop = False
        Me.ToolTabStrip1.ThemeName = "Office2010Blue"
        '
        'DocumentContainer1
        '
        Me.DocumentContainer1.Controls.Add(Me.DocumentTabStrip1)
        Me.DocumentContainer1.Name = "DocumentContainer1"
        '
        '
        '
        Me.DocumentContainer1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.DocumentContainer1.SizeInfo.AbsoluteSize = New System.Drawing.Size(658, 200)
        Me.DocumentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill
        Me.DocumentContainer1.SizeInfo.SplitterCorrection = New System.Drawing.Size(-135, 0)
        Me.DocumentContainer1.TabIndex = 3
        Me.DocumentContainer1.ThemeName = "Office2010Blue"
        '
        'DocumentTabStrip1
        '
        Me.DocumentTabStrip1.CanUpdateChildIndex = True
        Me.DocumentTabStrip1.Controls.Add(Me.DocumentWindow1)
        Me.DocumentTabStrip1.Location = New System.Drawing.Point(0, 0)
        Me.DocumentTabStrip1.Name = "DocumentTabStrip1"
        '
        '
        '
        Me.DocumentTabStrip1.RootElement.MinSize = New System.Drawing.Size(25, 25)
        Me.DocumentTabStrip1.SelectedIndex = 0
        Me.DocumentTabStrip1.Size = New System.Drawing.Size(668, 296)
        Me.DocumentTabStrip1.TabIndex = 0
        Me.DocumentTabStrip1.TabStop = False
        Me.DocumentTabStrip1.ThemeName = "Office2010Blue"
        '
        'DocumentWindow1
        '
        Me.DocumentWindow1.Controls.Add(Me.RadGridView1)
        Me.DocumentWindow1.DocumentButtons = Telerik.WinControls.UI.Docking.DocumentStripButtons.ActiveWindowList
        Me.DocumentWindow1.Location = New System.Drawing.Point(6, 28)
        Me.DocumentWindow1.Name = "DocumentWindow1"
        Me.DocumentWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument
        Me.DocumentWindow1.Size = New System.Drawing.Size(656, 262)
        Me.DocumentWindow1.Text = "UserRight"
        Me.DocumentWindow1.ToolCaptionButtons = Telerik.WinControls.UI.Docking.ToolStripCaptionButtons.AutoHide
        '
        'f04_01_UserRight
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1046, 346)
        Me.Controls.Add(Me.RadDock1)
        Me.Controls.Add(Me.RadPanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "f04_01_UserRight"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.ShowInTaskbar = False
        Me.Text = "Permission"
        Me.ThemeName = "Office2010Blue"
        CType(Me.RadGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VUserRightsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btcancel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtAdd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BTAddall, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtDel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtDelALL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MenusBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTreeView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgramsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel1.ResumeLayout(False)
        Me.RadPanel1.PerformLayout()
        CType(Me.LUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadDock1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadDock1.ResumeLayout(False)
        Me.ToolWindow1.ResumeLayout(False)
        CType(Me.ToolTabStrip2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolTabStrip2.ResumeLayout(False)
        Me.ToolWindow2.ResumeLayout(False)
        CType(Me.ToolTabStrip1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolTabStrip1.ResumeLayout(False)
        CType(Me.DocumentContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DocumentContainer1.ResumeLayout(False)
        CType(Me.DocumentTabStrip1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DocumentTabStrip1.ResumeLayout(False)
        Me.DocumentWindow1.ResumeLayout(False)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents RadGridView1 As Telerik.WinControls.UI.RadGridView
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadMultiColumnComboBox2 As Telerik.WinControls.UI.RadMultiColumnComboBox
    Friend WithEvents Btcancel As Telerik.WinControls.UI.RadButton
    Friend WithEvents BtSave As Telerik.WinControls.UI.RadButton
    Friend WithEvents BtAdd As Telerik.WinControls.UI.RadButton
    Friend WithEvents BTAddall As Telerik.WinControls.UI.RadButton
    Friend WithEvents BtDel As Telerik.WinControls.UI.RadButton
    Friend WithEvents BtDelALL As Telerik.WinControls.UI.RadButton
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents MenusBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MenusTableAdapter As TAS_TOL.FPTDataSetTableAdapters.MenusTableAdapter
    Friend WithEvents VUserRightsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VUserRightsTableAdapter As TAS_TOL.FPTDataSetTableAdapters.VUserRightsTableAdapter
    Friend WithEvents RadTreeView1 As Telerik.WinControls.UI.RadTreeView
    Friend WithEvents ProgramsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProgramsTableAdapter As TAS_TOL.FPTDataSetTableAdapters.ProgramsTableAdapter
    Friend WithEvents RadPanel1 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadDock1 As Telerik.WinControls.UI.Docking.RadDock
    Friend WithEvents DocumentContainer1 As Telerik.WinControls.UI.Docking.DocumentContainer
    Friend WithEvents ToolWindow1 As Telerik.WinControls.UI.Docking.ToolWindow
    Friend WithEvents ToolTabStrip2 As Telerik.WinControls.UI.Docking.ToolTabStrip
    Friend WithEvents ToolWindow2 As Telerik.WinControls.UI.Docking.ToolWindow
    Friend WithEvents ToolTabStrip1 As Telerik.WinControls.UI.Docking.ToolTabStrip
    Friend WithEvents DocumentTabStrip1 As Telerik.WinControls.UI.Docking.DocumentTabStrip
    Friend WithEvents DocumentWindow1 As Telerik.WinControls.UI.Docking.DocumentWindow
    Friend WithEvents LUser As Telerik.WinControls.UI.RadLabel
End Class

