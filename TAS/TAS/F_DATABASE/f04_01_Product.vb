﻿Imports System.Reflection
Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_Product
    Public Shared ShowType As Integer
    Public Shared SpCode As String
    Dim MyErrorProvider As New ErrorProviderExtended

    Private Sub Product_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MeterGrid.SuspendLayout()
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
        Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)

        'Add controls one by one in error provider.
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.ClearAllErrorMessages()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(P_NAME, "Product name:")
        MyErrorProvider.Controls.Add(P_CODE, "Product Code:")
        MyErrorProvider.Controls.Add(P_BLEND, "% Blending:")
        'MyErrorProvider.Controls.Add(IsNumofMeter, "Product main :")

        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        Me.BringToFront()
        MeterGrid.ResumeLayout()
        AddColorCombox(P_COLOR)
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)

    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Private Sub AddColorCombox(ByVal ComboX As ComboBox)
        Dim colorType As Type = GetType(System.Drawing.Color)
        Dim propInfoList As PropertyInfo() = colorType.GetProperties(BindingFlags.[Static] Or BindingFlags.DeclaredOnly Or BindingFlags.[Public])
        For Each c As PropertyInfo In propInfoList
            ComboX.Items.Add(c.Name)
        Next


    End Sub

    Public Sub SaveProduct(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_PRODUCT"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_PRODUCT")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub




    Private Sub P_COLOR_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles P_COLOR.DrawItem
        Dim g As Graphics = e.Graphics
        Dim rect As Rectangle = e.Bounds
        If e.Index >= 0 Then
            Dim n As String = DirectCast(sender, ComboBox).Items(e.Index).ToString()
            Dim f As New Font(P_COLOR.Font.Name, P_COLOR.Font.Size, P_COLOR.Font.Style)
            Dim c As Color = Color.FromName(n)
            Dim b As Brush = New SolidBrush(c)
            g.DrawString(n, f, Brushes.Black, rect.X, rect.Top)
            g.FillRectangle(b, rect.X + 110, rect.Y + 5, rect.Width - 10, rect.Height - 10)
        End If

    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click

        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    '''''''''''''''''''''''''''''''''''''''
                    'If P_INJECTEN.Checked = True Then
                    '    TProductBindingSource.Item(TProductBindingSource.Position)("Product_additive") = "Yes"

                    'Else
                    '    TProductBindingSource.Item(TProductBindingSource.Position)("Product_additive") = "No"
                    'End If
                    '' Set BAYRIGHT_STATUS En/Dis
                    'If P_MARKEREn.Checked = True Then
                    '    TProductBindingSource.Item(TProductBindingSource.Position)("Product_Marker") = "Yes"

                    'Else
                    '    TProductBindingSource.Item(TProductBindingSource.Position)("Product_Marker") = "No"
                    'End If


                    'If P_typeF.Checked = True Then
                    '    TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = "1"

                    'Else
                    '    TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = "0"
                    'End If


                    'Dim n As String = DirectCast(P_COLOR, ComboBox).Items(P_COLOR.SelectedIndex).ToString()

                    'TProductBindingSource.Item(TProductBindingSource.Position)("Product_color") = n

                    TProductBindingSource.Item(TProductBindingSource.Position)("update_date") = Now
                    TProductBindingSource.EndEdit()
                    ' BindingContext(FPTDataSet, "T_ISLAND").EndCurrentEdit()
                    BindingContext(FPTDataSet, "T_PRODUCT").EndCurrentEdit()
                    T_ProductTableAdapter.Update(FPTDataSet.T_Product)
                    ' SaveProduct(FPTDataSet)
                    Product_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
                If ShowType > 0 Then
                    Me.Close()
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TProductBindingSource.CancelEdit()
        Product_Load(sender, e)
    End Sub



    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        P_INJECTDis.Checked = True
        P_MARKERDis.Checked = True
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        '   Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
        'Where Not r.IsNewRow _
        'Select CInt(r.Cells(0).Value)
        '   'MaxBatchNo = cells.Max
        '   MeterNoBox.Text() = cells.Max + 1
        'Dim min As Integer = cells.Min
        'Dim minAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, min)}
        'Dim max As Integer = cells.Max
        'Dim maxAddress As New cell With {.columnIndex = 0, .rowIndex = Array.IndexOf(cells.ToArray, max)}
        'MeterNoBox.Text = max + 1
        'MeterNoBox.Focus()
        TProductBindingSource.Item(TProductBindingSource.Position)("Product_additive") = "NO"
        TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = "1"
        TProductBindingSource.Item(TProductBindingSource.Position)("Product_Marker") = "NO"
        P_typeF.Checked = True
        P_INJECTDis.Checked = True
        P_MARKERDis.Checked = True
        TProductBindingSource.Item(TProductBindingSource.Position)("PRODUCT_DATE") = Now
        P_NAME.Focus()
        'MeterNoBox.Text = max + 1
    End Sub

    Private Sub TProductBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TProductBindingSource.PositionChanged
        Try
            If BtEdit.Enabled = False Then TProductBindingSource.Item(TProductBindingSource.Position)("update_date") = Now
            If UCase(TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type").ToString) = "1" Then
                P_typeF.Checked = True
            Else
                P_typeN.Checked = True
            End If

            P_COLOR.SelectedIndex = P_COLOR.Items.IndexOf(TProductBindingSource.Item(TProductBindingSource.Position)("Product_color").ToString)

            If UCase(TProductBindingSource.Item(TProductBindingSource.Position)("Product_additive").ToString) = "YES" Then
                P_INJECTEN.Checked = True
            Else
                P_INJECTDis.Checked = True
            End If
            ' Set BAYRIGHT_STATUS En/Dis
            If UCase(TProductBindingSource.Item(TProductBindingSource.Position)("Product_Marker").ToString) = "YES" Then
                P_MARKEREn.Checked = True

            Else
                P_MARKERDis.Checked = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Product_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        TProductBindingSource_PositionChanged(Nothing, Nothing)
        If ShowType > 0 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.P_CODE.Text = SpCode
            TProductBindingSource.Item(TProductBindingSource.Position)("PRODUCT_DATE") = Now
            Me.P_NAME.Focus()
        End If
    End Sub

    Private Sub Product_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
        SpCode = ""
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        P_INJECTDis.Checked = True
        P_MARKERDis.Checked = True
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        P_COLOR.SelectedIndex = -1
    End Sub


    Private Sub P_typeN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles P_typeN.Click
        If sender.Checked = True Then TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = "0"
    End Sub

    Private Sub P_typeF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles P_typeF.Click
        If sender.Checked = True Then TProductBindingSource.Item(TProductBindingSource.Position)("Product_Type") = "1"
    End Sub

    Private Sub P_INJECTEN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles P_INJECTEN.Click
        If sender.Checked = True Then TProductBindingSource.Item(TProductBindingSource.Position)("Product_additive") = "Yes"
    End Sub

    Private Sub P_INJECTDis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles P_INJECTDis.Click
        If sender.Checked = True Then TProductBindingSource.Item(TProductBindingSource.Position)("Product_additive") = "No"
    End Sub

    Private Sub P_MARKEREn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles P_MARKEREn.Click
        If sender.Checked = True Then TProductBindingSource.Item(TProductBindingSource.Position)("Product_Marker") = "Yes"
    End Sub

    Private Sub P_MARKERDis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles P_MARKERDis.Click
        If sender.Checked = True Then TProductBindingSource.Item(TProductBindingSource.Position)("Product_Marker") = "No"
    End Sub

    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_Product.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_Product.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_Product.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_Product.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TProductBindingSource.Filter = StrFilter
        Else : TProductBindingSource.RemoveFilter()
        End If

    End Sub
End Class