﻿Imports System.Data.SqlClient
Imports TAS_TOL.C01_Permission
Public Class f04_01_UserRight
    Dim U_ID As Integer = 0
    Dim ds As New DataSet
    Private Sub UserRight_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.Programs' table. You can move, or remove it, as needed.
        Me.ProgramsTableAdapter.Fill(Me.FPTDataSet.Programs)
        'TODO: This line of code loads data into the 'FPTDataSet.VUserRights' table. You can move, or remove it, as needed.
        '  Me.VUserRightsTableAdapter.Fill(Me.FPTDataSet.VUserRights)
        'TODO: This line of code loads data into the 'FPTDataSet.Menus' table. You can move, or remove it, as needed.
        Me.MenusTableAdapter.Fill(Me.FPTDataSet.Menus)
        Dim con As New SqlConnection(My.Settings.FPTConnectionString)
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        ds = New DataSet
        con.Open()
        cmd.Connection = con
        LUser.Text = f04_01_UserAccount.TUSERBindingSource.Item(f04_01_UserAccount.TUSERBindingSource.Position)("U_NAME_S").ToString
        U_ID = f04_01_UserAccount.TUSERBindingSource.Item(f04_01_UserAccount.TUSERBindingSource.Position)("U_ID").ToString
        cmd.CommandText = "select * from Menus where MenuID not in (select MenuID from UserRights where UserID='" & U_ID & "')"
        da.SelectCommand = cmd
        da.Fill(ds, "Menus")
        cmd.CommandText = " Select * from VUserRights where UserID='" & U_ID & "'"
        da.SelectCommand = cmd
        da.Fill(ds, "VUserRights")
        cmd.CommandText = "select * from vProgramMenu "
        da.SelectCommand = cmd
        da.Fill(ds, "vProgramMenu")

        VUserRightsBindingSource.DataSource = ds
        VUserRightsBindingSource.DataMember = "VUserRights"

        RadTreeView1.Nodes.Clear()
        For i As Integer = 0 To ProgramsBindingSource.Count - 1
            RadTreeView1.Nodes.Add(ProgramsBindingSource.Item(i)("ProgramName").ToString)
            Dim dr() As DataRow = ds.Tables("Menus").Select("programID='" & ProgramsBindingSource.Item(i)("ProgramID").ToString & "'", "")
            ' MenusBindingSource.Filter = "programID='" & ProgramsBindingSource.Item(i)("ProgramID").ToString & "'"
            For j As Integer = 0 To dr.Length - 1
                RadTreeView1.Nodes(ProgramsBindingSource.Item(i)("ProgramName").ToString).Nodes.Add(dr(j)("MenuName").ToString)
            Next
            RadTreeView1.Nodes(ProgramsBindingSource.Item(i)("ProgramName").ToString).Expanded = True
        Next
        Main.vUserRight = Nothing
        Main.vUserRight = New DataTable
        Main.vUserRight = GetUserRight(Main.U_NAME_ID)
        con.Close()
        con.Dispose()
        cmd.Dispose()
        da.Dispose()
        ds.Dispose()

    End Sub

    Private Sub BtDel_Click(sender As System.Object, e As System.EventArgs) Handles BtDel.Click
        For i As Integer = 0 To RadGridView1.SelectedRows.Count - 1
            RadTreeView1.Nodes(RadGridView1.SelectedRows(0).Cells("ProgramName").Value).Nodes.Add(RadGridView1.SelectedRows(0).Cells("MenuName").Value)
            RadGridView1.SelectedRows(0).Delete()

        Next
        ' RadGridView1.ClearSelection()
    End Sub

    Private Sub BtDelALL_Click(sender As System.Object, e As System.EventArgs) Handles BtDelALL.Click
        For i As Integer = 0 To RadGridView1.Rows.Count - 1
            RadTreeView1.Nodes(RadGridView1.Rows(0).Cells("ProgramName").Value).Nodes.Add(RadGridView1.Rows(0).Cells("MenuName").Value)
            RadGridView1.Rows(0).Delete()
        Next
    End Sub

    Private Sub BtAdd_Click(sender As System.Object, e As System.EventArgs) Handles BtAdd.Click
        Dim Node1 As String = ""
        Dim Node2 As String = ""
        Dim Pos As Integer = 0
        For i As Integer = 0 To RadTreeView1.SelectedNodes.Count - 1
            Node1 = RadTreeView1.SelectedNodes.Item(0).RootNode.Value
            Node2 = RadTreeView1.SelectedNodes.Item(0).Value
            If Node1 = Node2 Then
                For j As Integer = 0 To RadTreeView1.Nodes(Node1).Nodes.Count - 1
                    RadGridView1.Rows.AddNew()
                    Pos = VUserRightsBindingSource.Position
                    Node2 = RadTreeView1.Nodes(Node1).Nodes(0).Value()
                    VUserRightsBindingSource.Item(Pos)("UserID") = U_ID
                    VUserRightsBindingSource.Item(Pos)("Programname") = Node1
                    VUserRightsBindingSource.Item(Pos)("ProgramID") = ds.Tables("vProgramMenu").Compute("Max(ProgramID)", "Programname='" & Node1 & "'")
                    VUserRightsBindingSource.Item(Pos)("MenuName") = Node2
                    VUserRightsBindingSource.Item(Pos)("MenuID") = ds.Tables("vProgramMenu").Compute("Max(MenuID)", "Programname='" & Node1 & "'" & " and MenuName='" & Node2 & "'")
                    VUserRightsBindingSource.Item(Pos)("chkadd") = False
                    VUserRightsBindingSource.Item(Pos)("chkedit") = False
                    VUserRightsBindingSource.Item(Pos)("chkdel") = False
                    VUserRightsBindingSource.Item(Pos)("chkprint") = True
                    VUserRightsBindingSource.Item(Pos)("chkview") = True
                    RadTreeView1.Nodes(Node1).Nodes(Node2).Remove()
                Next
            Else
                RadGridView1.Rows.AddNew()
                Pos = VUserRightsBindingSource.Position
                '  Node2 = RadTreeView1.Nodes(Node1).Nodes(0).Value()
                VUserRightsBindingSource.Item(Pos)("UserID") = U_ID
                VUserRightsBindingSource.Item(Pos)("Programname") = Node1
                VUserRightsBindingSource.Item(Pos)("ProgramID") = ds.Tables("vProgramMenu").Compute("Max(ProgramID)", "Programname='" & Node1 & "'")
                VUserRightsBindingSource.Item(Pos)("MenuName") = Node2
                VUserRightsBindingSource.Item(Pos)("MenuID") = ds.Tables("vProgramMenu").Compute("Max(MenuID)", "Programname='" & Node1 & "'" & " and MenuName='" & Node2 & "'")
                VUserRightsBindingSource.Item(Pos)("chkadd") = False
                VUserRightsBindingSource.Item(Pos)("chkedit") = False
                VUserRightsBindingSource.Item(Pos)("chkdel") = False
                VUserRightsBindingSource.Item(Pos)("chkprint") = True
                VUserRightsBindingSource.Item(Pos)("chkview") = True
                RadTreeView1.Nodes(Node1).Nodes(Node2).Remove()

            End If

            'Dim dr() As DataRow = ds.Tables("vProgramMenu").Select("ProgramName='" & RadTreeView1.SelectedNodes(0).Value & "'", "")

            'For j As Integer = 0 To RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value).Nodes.count - 1
            '    RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value).Nodes(j).Value()
            'Next
            'RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value).Nodes.Add(dr(j)("MenuName").ToString)

            'RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value)
            'If dr.Length = 0 Then
            '    dr = ds.Tables("vProgramMenu").Select("MenuName='" & RadTreeView1.SelectedNodes(0).Value & "'", "")
            'Else

            'End If
            '' Dim Pos As Integer = VUserRightsBindingSource.Position

            'VUserRightsBindingSource.Item(Pos)("UserID") = U_ID
            'VUserRightsBindingSource.Item(Pos)("MenuID") = ""
            'VUserRightsBindingSource.Item(Pos)("MenuName") = ""
            'VUserRightsBindingSource.Item(Pos)("ProgramID") = ""
            'VUserRightsBindingSource.Item(Pos)("Programname") = ""
            'VUserRightsBindingSource.Item(Pos)("chkadd") = False
            'VUserRightsBindingSource.Item(Pos)("chkedit") = False
            'VUserRightsBindingSource.Item(Pos)("chkdel") = False
            'VUserRightsBindingSource.Item(Pos)("chkprint") = False
            'VUserRightsBindingSource.Item(Pos)("chkview") = False

            '  RadTreeView1.Nodes(RadGridView1.SelectedRows(0).Cells("ProgramName").Value).Nodes.Add(RadGridView1.SelectedRows(0).Cells("MenuName").Value)

            ' RadGridView1.SelectedRows(0).Delete()

        Next
        VUserRightsBindingSource.EndEdit()
        VUserRightsBindingSource.Position = -1
    End Sub

    Private Sub BTAddall_Click(sender As System.Object, e As System.EventArgs) Handles BTAddall.Click
        Dim Node1 As String = ""
        Dim Node2 As String = ""
        Dim Pos As Integer = 0
        For i As Integer = 0 To RadTreeView1.Nodes.Count - 1
            Node1 = RadTreeView1.Nodes.Item(i).RootNode.Value
            'Node2 = RadTreeView1.SelectedNodes.Item(0).Value
            'If Node1 = Node2 Then
            For j As Integer = 0 To RadTreeView1.Nodes(Node1).Nodes.Count - 1
                RadGridView1.Rows.AddNew()
                Pos = VUserRightsBindingSource.Position
                Node2 = RadTreeView1.Nodes(Node1).Nodes(0).Value()
                VUserRightsBindingSource.Item(Pos)("UserID") = U_ID
                VUserRightsBindingSource.Item(Pos)("Programname") = Node1
                VUserRightsBindingSource.Item(Pos)("ProgramID") = ds.Tables("vProgramMenu").Compute("Max(ProgramID)", "Programname='" & Node1 & "'")
                VUserRightsBindingSource.Item(Pos)("MenuName") = Node2
                VUserRightsBindingSource.Item(Pos)("MenuID") = ds.Tables("vProgramMenu").Compute("Max(MenuID)", "Programname='" & Node1 & "'" & " and MenuName='" & Node2 & "'")
                VUserRightsBindingSource.Item(Pos)("chkadd") = False
                VUserRightsBindingSource.Item(Pos)("chkedit") = False
                VUserRightsBindingSource.Item(Pos)("chkdel") = False
                VUserRightsBindingSource.Item(Pos)("chkprint") = True
                VUserRightsBindingSource.Item(Pos)("chkview") = True
                RadTreeView1.Nodes(Node1).Nodes(Node2).Remove()
            Next
            'Else
            'RadGridView1.Rows.AddNew()
            'Pos = VUserRightsBindingSource.Position
            ''  Node2 = RadTreeView1.Nodes(Node1).Nodes(0).Value()
            'VUserRightsBindingSource.Item(Pos)("UserID") = U_ID
            'VUserRightsBindingSource.Item(Pos)("Programname") = Node1
            'VUserRightsBindingSource.Item(Pos)("ProgramID") = ds.Tables("vProgramMenu").Compute("Max(ProgramID)", "Programname='" & Node1 & "'")
            'VUserRightsBindingSource.Item(Pos)("MenuName") = Node2
            'VUserRightsBindingSource.Item(Pos)("MenuID") = ds.Tables("vProgramMenu").Compute("Max(MenuID)", "Programname='" & Node1 & "'" & " and MenuName='" & Node2 & "'")
            'VUserRightsBindingSource.Item(Pos)("chkadd") = False
            'VUserRightsBindingSource.Item(Pos)("chkedit") = False
            'VUserRightsBindingSource.Item(Pos)("chkdel") = False
            'VUserRightsBindingSource.Item(Pos)("chkprint") = True
            'VUserRightsBindingSource.Item(Pos)("chkview") = True
            'RadTreeView1.Nodes(Node1).Nodes(Node2).Remove()

            'End If

            'Dim dr() As DataRow = ds.Tables("vProgramMenu").Select("ProgramName='" & RadTreeView1.SelectedNodes(0).Value & "'", "")

            'For j As Integer = 0 To RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value).Nodes.count - 1
            '    RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value).Nodes(j).Value()
            'Next
            'RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value).Nodes.Add(dr(j)("MenuName").ToString)

            'RadTreeView1.Nodes(RadTreeView1.SelectedNodes(0).Value)
            'If dr.Length = 0 Then
            '    dr = ds.Tables("vProgramMenu").Select("MenuName='" & RadTreeView1.SelectedNodes(0).Value & "'", "")
            'Else

            'End If
            '' Dim Pos As Integer = VUserRightsBindingSource.Position

            'VUserRightsBindingSource.Item(Pos)("UserID") = U_ID
            'VUserRightsBindingSource.Item(Pos)("MenuID") = ""
            'VUserRightsBindingSource.Item(Pos)("MenuName") = ""
            'VUserRightsBindingSource.Item(Pos)("ProgramID") = ""
            'VUserRightsBindingSource.Item(Pos)("Programname") = ""
            'VUserRightsBindingSource.Item(Pos)("chkadd") = False
            'VUserRightsBindingSource.Item(Pos)("chkedit") = False
            'VUserRightsBindingSource.Item(Pos)("chkdel") = False
            'VUserRightsBindingSource.Item(Pos)("chkprint") = False
            'VUserRightsBindingSource.Item(Pos)("chkview") = False

            '  RadTreeView1.Nodes(RadGridView1.SelectedRows(0).Cells("ProgramName").Value).Nodes.Add(RadGridView1.SelectedRows(0).Cells("MenuName").Value)

            ' RadGridView1.SelectedRows(0).Delete()

        Next
        VUserRightsBindingSource.EndEdit()
        VUserRightsBindingSource.Position = -1
    End Sub


    Private Sub Btcancel_Click(sender As System.Object, e As System.EventArgs) Handles Btcancel.Click
        Close()
    End Sub

    Private Sub BtSave_Click(sender As System.Object, e As System.EventArgs) Handles BtSave.Click
        Dim con As New SqlConnection(My.Settings.FPTConnectionString)
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        ds = New DataSet
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "Delete From UserRights where UserID='" & U_ID & "'"
        cmd.ExecuteNonQuery()
        For i As Integer = 0 To VUserRightsBindingSource.Count - 1
            cmd.CommandText = "insert Into UserRights (UserID,MenuID,ProgramID,chkadd,chkedit,chkdel,chkprint,chkview) VALUES (" &
            "'" & VUserRightsBindingSource.Item(i)("UserID").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("MenuID").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("ProgramID").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("chkadd").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("chkedit").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("chkdel").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("chkprint").ToString & "'," &
            "'" & VUserRightsBindingSource.Item(i)("chkview").ToString & "'" &
            ")"
            cmd.ExecuteNonQuery()
        Next
        
        con.Close()
        con.Dispose()
        cmd.Dispose()
        da.Dispose()
        ds.Dispose()
        UserRight_Load(sender, e)
    End Sub


    Private Sub e(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub
End Class
