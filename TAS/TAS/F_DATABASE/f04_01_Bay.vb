﻿Imports System.Threading
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports Gauge

Public Class f04_01_Bay
    Friend WithEvents ThrRefresh As BackgroundWorker
    Dim CallDataBindToDataGrid As New MethodInvoker(AddressOf Me.DataBindToDataGrid)
    Dim MyDataSet As DataSet
    Dim MyDataAdapter As SqlDataAdapter
    Dim MyQueryString As String
    Dim MyConnection As New SqlConnection(My.Settings.FPTConnectionString)
    Dim StrBay As String

    Declare Function LockWindowUpdate Lib "user32" _
(ByVal hWnd As Long) As Long
    Public Sub LockWindow(ByVal hWnd As Long, ByVal blnValue As Boolean)
        If blnValue Then
            LockWindowUpdate(hWnd)
        Else
            LockWindowUpdate(0)
        End If
    End Sub

    Private Sub FRefresh()
        ThrRefresh = New BackgroundWorker
        ThrRefresh.WorkerSupportsCancellation = True
        AddHandler ThrRefresh.DoWork, AddressOf RefreshGrig
        ThrRefresh.RunWorkerAsync()
    End Sub

    Private Sub RefreshGrig()
        Dim batch_bay As String = "0"
        Dim LOAD_MODE As Int16
        Dim q1, q2 As String
        While True
            If ThrRefresh.CancellationPending Then
                ' StartConnect()
                '   ConnectTCP.Enabled = True
                '   ConnectTCP_Tick(sender, e)
                Exit While
            End If
            Try
                MyDataSet = New DataSet()
                MyConnection.Open()
                Dim Bay_island() As String = Split(StrBay, "/")
                MyQueryString = " SELECT* FROM V_BATCH_STATUS" & _
                                " WHERE BATCH_BAY=" & Bay_island(0) + " and ISLAND_NUMBER=" & Bay_island(1) & _
                                " ORDER BY BATCH_NUMBER"

                Dim cmd As New SqlCommand(MyQueryString, MyConnection)
                MyDataAdapter = New SqlDataAdapter(cmd)
                MyDataAdapter.Fill(MyDataSet, "V_BATCH_STATUS")
                batch_bay = "0"
                For i As Int16 = 0 To MyDataSet.Tables("V_BATCH_STATUS").Rows.Count - 1
                    If batch_bay <> "0" Then
                        batch_bay = batch_bay + ","
                    End If
                    batch_bay = batch_bay + MyDataSet.Tables("V_BATCH_STATUS").Rows(i).Item("BATCH_NUMBER").ToString()

                Next

                ' End Meter Detail

                If LOAD_MODE = 0 Then 'Local or Remote loading
                    q1 = " select * from V_LOADING_LOCALMODE"
                    q1 = q1 + " where LOAD_DATE=(select LOAD_DATE from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + batch_bay + ")))"
                    q1 = q1 + " and CSF=(select CSF from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + batch_bay + "))) "
                    q1 = q1 + " order by CVH,DAY_LOAD_ID "

                    q2 = " select CVH,MAX(CVA) as CVA,MAX(CVI) as CVI, SUM(CVB) as CVB,SUM(CVJ) as CVJ ,MAX(Product_color) as Product_color,MAX(LOAD_STATUS) as LOAD_STATUS from V_LOADING_LOCALMODE"
                    q2 = q2 + " where LOAD_DATE=(select LOAD_DATE from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + batch_bay + ")))"
                    q2 = q2 + " and CSF=(select CSF from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + batch_bay + "))) "
                    q2 = q2 + " Group by CVH"
                    q2 = q2 + " order by CVH"
                Else
                    q1 = " select * from V_LOADING_REMOTEMODE "
                    q1 = q1 + " where Reference =(select top 1(cast(Reference as Float))as Reference from V_LOADING_REMOTEMODE where (ID IS NOT NULL) and  BATCH_NUMBER in (" + batch_bay + ") order by ldate desc )"
                    q1 = q1 + " order by CVH,DAY_LOAD_ID "

                    q2 = " select CVH,MAX(CVA) as CVA,MAX(CVI) as CVI, SUM(CVB) as CVB,SUM(CVJ) as CVJ,MAX(Product_color) as Product_color,MAX(LOAD_STATUS) as LOAD_STATUS from V_LOADING_REMOTEMODE"
                    q2 = q2 + " where  Reference =(select top 1 (cast(Reference as Float))as Reference from V_LOADING_REMOTEMODE where (ID IS NOT NULL) and  BATCH_NUMBER in (" + batch_bay + ") order by ldate desc )"
                    q2 = q2 + " Group by CVH"
                    q2 = q2 + " order by CVH"
                End If
                cmd = New SqlCommand(q1, MyConnection)
                MyDataAdapter = New SqlDataAdapter(cmd)
                MyDataAdapter.Fill(MyDataSet, "LOADING")
                cmd = New SqlCommand(q1, MyConnection)
                MyDataAdapter = New SqlDataAdapter(cmd)
                MyDataAdapter.Fill(MyDataSet, "TRUCK_LOADING")

                MyConnection.Close()
                Me.BeginInvoke(CallDataBindToDataGrid)

                Thread.Sleep(3000)
            Catch ex As Exception
                MyConnection.Close()
                '     MessageBox.Show(ex.Message)
            End Try
        End While

    End Sub

    Public Sub DataBindToDataGrid()
        Try
            ' Set Meter Grid'

            MeterDetail.SuspendLayout()
            FPTDataSetBindingSource.DataSource = MyDataSet
            FPTDataSetBindingSource.DataMember = "V_BATCH_STATUS"
            ' SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))

            '' SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))
            MeterDetail.ResumeLayout()


            'MeterDetail.SuspendLayout()
            'MeterDetail.DataSource = MyDataSet
            'MeterDetail.DataMember = "V_BATCH_STATUS"
            'SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))
            'MeterDetail.ResumeLayout()
            ' End Set Meter Grid


            LoadDetail.SuspendLayout()
            VLOADINGLOCALMODEBindingSource.DataSource = MyDataSet
            VLOADINGLOCALMODEBindingSource.DataMember = "LOADING"
            ' SetLoadDetail(MyDataSet.Tables("LOADING"), MyDataSet.Tables("TRUCK_LOADING"))

            '' SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))
            LoadDetail.ResumeLayout()


            ''Set Loading Grid
            'LoadDetail.SuspendLayout()
            'LoadDetail.DataSource = MyDataSet
            'LoadDetail.DataMember = "LOADING"
            'SetLoadDetail(MyDataSet.Tables("LOADING"), MyDataSet.Tables("TRUCK_LOADING"))
            'LoadDetail.ResumeLayout()
            MyDataAdapter = Nothing
            MyDataSet = Nothing
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.V_LOADING_LOCALMODE' table. You can move, or remove it, as needed.
        Me.V_LOADING_LOCALMODETableAdapter.Fill(Me.FPTDataSet.V_LOADING_LOCALMODE)
        'TODO: This line of code loads data into the 'FPTDataSet.V_ISLANDBAY' table. You can move, or remove it, as needed.
        Me.V_ISLANDBAYTableAdapter.Fill(Me.FPTDataSet.V_ISLANDBAY)
        'TODO: This line of code loads data into the 'FPTDataSet.V_BATCH_STATUS' table. You can move, or remove it, as needed.
        ' V_BATCH_STATUS.
        ' Me.V_BATCH_STATUSTableAdapter.Fill(Me.FPTDataSet.V_BATCH_STATUS)
        Me.PictureBox8.Image = New System.Drawing.Bitmap("Company.bmp")
        ' Me.PictureBox2.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        ' MeterDetail.DataSource = Nothing
        ' LoadDetail.DataSource = Nothing
        CBBay_SelectedIndexChanged(sender, Nothing)
        FRefresh()
    End Sub

    Private Sub GroupBox2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)
        'Dim Text = "Loading Detail"
        '' GroupBox2.Height = CInt(e.Graphics.MeasureString(Text, Font).Width) + 150
        'e.Graphics.TranslateTransform(20, GroupBox2.Height - 10)
        'e.Graphics.RotateTransform(270)
        'e.Graphics.DrawString(Text, New System.Drawing.Font("Microsoft Sans Serif", 18, FontStyle.Bold), Brushes.Green, 0, 0)

    End Sub

    Private Sub Bay_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ThrRefresh.CancelAsync()
    End Sub

    Private Sub CBBay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBBay.SelectedIndexChanged
        StrBay = CBBay.Text
    End Sub

    Private Sub SetbatchEn(ByVal dt As DataTable)
        LockWindow(Me.Handle, True)
        Dim i As Integer
        For i = 1 To 8
            Dim Pbatch() As Control = Me.Controls.Find("Pbatch" & i, True)
            If Pbatch.Length = 1 Then
                '''''''''''''En PBatchXX///////////////////////////////////////
                Dim Pn As Panel = DirectCast(Pbatch(0), Panel)
                If MeterDetail.RowCount >= i Then
                    Pn.Visible = True
                    '''''''''''''Batch name///////////////////////////////////////
                    Dim BNane() As Control = Me.Controls.Find("Name_B" & i, True)
                    If BNane.Length = 1 Then
                        Dim Tb As TextBox = DirectCast(BNane(0), TextBox)
                        Tb.Text = dt.Rows(i - 1).Item("BATCH_NAME").ToString
                        Tb = Nothing
                    End If
                    '///////////////////Product NAme///////////////////////////////
                    Try
                        Dim BPro() As Control = Me.Controls.Find("PRO_B" & i, True)
                        If BPro.Length = 1 Then
                            Dim Tb As TextBox = DirectCast(BPro(0), TextBox)
                            Tb.Text = dt.Rows(i - 1).Item("Product_code").ToString
                            Tb.BackColor = Color.FromName(dt.Rows(i - 1).Item("PRODUCT_COLORM").ToString)
                            Tb = Nothing
                        End If
                    Catch ex As Exception

                    End Try

                    '///////////////////Accum base ///////////////////////////////
                    Try
                        Dim Accum_B() As Control = Me.Controls.Find("Accum_B" & i, True)
                        If Accum_B.Length = 1 Then
                            Dim Tb As Label = DirectCast(Accum_B(0), Label)
                            Tb.Text = dt.Rows(i - 1).Item("BASE_ACCUM_G").ToString
                            Tb = Nothing
                        End If
                    Catch ex As Exception

                    End Try
                Else : Pn.Visible = False
                End If
                Pn = Nothing
            End If
            Pbatch = Nothing
        Next
        LockWindow(Me.Handle, False)
    End Sub

    Private Sub SetLoadDetail(ByVal dt As DataTable, ByVal Truck As DataTable)
        LockWindow(Me.Handle, True)
        Dim Gauge As TGauge
        '  PCompartment.SuspendLayout()
        If dt.Rows.Count > 0 Then
            PCompartment.Visible = True
            EdLoadID.Text = dt.Compute("MAX(DAY_LOAD_ID)", Nothing).ToString()
            EDLOADVEH.Text = dt.Compute("MAX(LOAD_VEHICLE)", Nothing).ToString()
            EDREF.Text = dt.Compute("MAX(REFERENCE)", Nothing).ToString()
            EDPIN.Text = dt.Compute("MAX(CARD_SERIAL)", Nothing).ToString()
            EdCountComp.Text = dt.Compute("MAX(CVH)", Nothing).ToString()
            EDPRESET.Text = FormatNumber(dt.Compute("SUM(PRESETS)", Nothing).ToString(), 0, TriState.False).ToString
            EDGROSS.Text = FormatNumber(dt.Compute("SUM(GROSS)", Nothing).ToString(), 0, TriState.False).ToString
            DEREMAIN.Text = FormatNumber(dt.Compute("SUM(PRESETS)-SUM(GROSS)", Nothing).ToString(), 0, TriState.False).ToString

            Dim sDateFrom As DateTime = dt.Compute("MIN(TIME_START)", Nothing).ToString()
            Dim sDateTo As DateTime = dt.Compute("max(LDATE)", Nothing).ToString()
            sDateFrom = sDateFrom.ToLongTimeString
            sDateTo = sDateTo.ToLongTimeString
            Dim dFrom As DateTime
            Dim dTo As DateTime
            If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                Dim TS As TimeSpan = dTo - dFrom
                Dim hour As Integer = TS.Hours
                Dim mins As Integer = TS.Minutes
                Dim secs As Integer = TS.Seconds
                Dim timeDiff As String = ((hour.ToString("00") & ":") + mins.ToString("00") & ":") + secs.ToString("00")
                EDTIMEUSE.Text = timeDiff
            End If

            Dim MaxCompartment As Int16 = dt.Compute("MAX(CVH)", Nothing).ToString()
            Dim r As Int16 = 0
            For I As Int16 = 1 To 20
                Dim BNane() As Control = Me.Controls.Find("TGauge" & I, True)
                If BNane.Length = 1 Then
                    Gauge = Nothing
                    Gauge = DirectCast(BNane(0), TGauge)
                    '      Gauge.SuspendLayout()
                    If I <= MaxCompartment Then
                        Gauge.Dock = DockStyle.Left
                        Gauge.Width = Int(PCompartment.Width / MaxCompartment)
                        If Truck.Rows(r).Item("CVH").ToString = I Then
                            Gauge.MaxValue = Truck.Rows(r).Item("CVA") + Truck.Rows(r).Item("CVI")
                            Gauge.Progress = Truck.Rows(r).Item("CVB") + Truck.Rows(r).Item("CVJ")
                            Gauge.Color = Color.FromName(Truck.Rows(r).Item("Product_color").ToString)
                            r += 1
                        Else
                            Gauge.Progress = 0
                            If Truck.Rows(r).Item("CVH").ToString <= I Then
                                r += 1
                            End If
                        End If
                        Gauge.Visible = True
                    Else : Gauge.Visible = False
                    End If
                End If
                '    Gauge.ResumeLayout()
            Next
        Else : PCompartment.Visible = False
        End If
        '  PCompartment.ResumeLayout()
        LockWindow(Me.Handle, False)
    End Sub


End Class