﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_Shipper
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_Shipper))
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPNameENDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPNameTHDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.SP_SAP_CODE = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SP_PROD6 = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label18 = New System.Windows.Forms.Label()
        Me.SP_PROD5 = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.SP_PROD4 = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.SP_PROD3 = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SP_PROD2 = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SP_PROD1 = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.SP_WEB = New System.Windows.Forms.TextBox()
        Me.SP_EMAIL = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.SP_FAX = New System.Windows.Forms.TextBox()
        Me.SP_PHONE = New System.Windows.Forms.TextBox()
        Me.SP_POSCODE = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.SP_ADDEN = New System.Windows.Forms.RichTextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.SP_PROEN = New System.Windows.Forms.TextBox()
        Me.SP_DISEN = New System.Windows.Forms.TextBox()
        Me.SP_NAMEEN = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.SP_ADDTH = New System.Windows.Forms.RichTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.SP_PROTH = New System.Windows.Forms.TextBox()
        Me.SP_DISTH = New System.Windows.Forms.TextBox()
        Me.SP_NAMETH = New System.Windows.Forms.TextBox()
        Me.SP_TYPE = New System.Windows.Forms.TextBox()
        Me.SP_CRDATE = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.SP_CODE = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SP_UPDATE = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SP_CONNAME = New System.Windows.Forms.TextBox()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DetailGroup.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.TProductBindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.SPCodeDataGridViewTextBoxColumn, Me.SPNameENDataGridViewTextBoxColumn, Me.SPNameTHDataGridViewTextBoxColumn})
        Me.MeterGrid.DataSource = Me.TSHIPPERBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.MeterGrid.Location = New System.Drawing.Point(5, 31)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(397, 514)
        Me.MeterGrid.TabIndex = 1
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        Me.IDDataGridViewTextBoxColumn.Width = 50
        '
        'SPCodeDataGridViewTextBoxColumn
        '
        Me.SPCodeDataGridViewTextBoxColumn.DataPropertyName = "SP_Code"
        Me.SPCodeDataGridViewTextBoxColumn.HeaderText = "Code"
        Me.SPCodeDataGridViewTextBoxColumn.Name = "SPCodeDataGridViewTextBoxColumn"
        Me.SPCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.SPCodeDataGridViewTextBoxColumn.Width = 80
        '
        'SPNameENDataGridViewTextBoxColumn
        '
        Me.SPNameENDataGridViewTextBoxColumn.DataPropertyName = "SP_NameEN"
        Me.SPNameENDataGridViewTextBoxColumn.HeaderText = "Name"
        Me.SPNameENDataGridViewTextBoxColumn.Name = "SPNameENDataGridViewTextBoxColumn"
        Me.SPNameENDataGridViewTextBoxColumn.ReadOnly = True
        Me.SPNameENDataGridViewTextBoxColumn.Width = 130
        '
        'SPNameTHDataGridViewTextBoxColumn
        '
        Me.SPNameTHDataGridViewTextBoxColumn.DataPropertyName = "SP_NameTH"
        Me.SPNameTHDataGridViewTextBoxColumn.HeaderText = "ชื่อผู้ขนส่ง"
        Me.SPNameTHDataGridViewTextBoxColumn.Name = "SPNameTHDataGridViewTextBoxColumn"
        Me.SPNameTHDataGridViewTextBoxColumn.ReadOnly = True
        Me.SPNameTHDataGridViewTextBoxColumn.Width = 130
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.SP_SAP_CODE)
        Me.DetailGroup.Controls.Add(Me.Label28)
        Me.DetailGroup.Controls.Add(Me.Label27)
        Me.DetailGroup.Controls.Add(Me.Label26)
        Me.DetailGroup.Controls.Add(Me.Label25)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.GroupBox1)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.SP_WEB)
        Me.DetailGroup.Controls.Add(Me.SP_EMAIL)
        Me.DetailGroup.Controls.Add(Me.Label23)
        Me.DetailGroup.Controls.Add(Me.Label24)
        Me.DetailGroup.Controls.Add(Me.SP_FAX)
        Me.DetailGroup.Controls.Add(Me.SP_PHONE)
        Me.DetailGroup.Controls.Add(Me.SP_POSCODE)
        Me.DetailGroup.Controls.Add(Me.Label20)
        Me.DetailGroup.Controls.Add(Me.Label21)
        Me.DetailGroup.Controls.Add(Me.Label22)
        Me.DetailGroup.Controls.Add(Me.GroupBox5)
        Me.DetailGroup.Controls.Add(Me.GroupBox4)
        Me.DetailGroup.Controls.Add(Me.SP_TYPE)
        Me.DetailGroup.Controls.Add(Me.SP_CRDATE)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.SP_CODE)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Controls.Add(Me.SP_UPDATE)
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.SP_CONNAME)
        Me.DetailGroup.Enabled = False
        Me.DetailGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DetailGroup.Location = New System.Drawing.Point(408, 25)
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.Size = New System.Drawing.Size(619, 521)
        Me.DetailGroup.TabIndex = 2
        Me.DetailGroup.TabStop = False
        '
        'SP_SAP_CODE
        '
        Me.SP_SAP_CODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_SAPCODE", True))
        Me.SP_SAP_CODE.Location = New System.Drawing.Point(430, 47)
        Me.SP_SAP_CODE.Name = "SP_SAP_CODE"
        Me.SP_SAP_CODE.Size = New System.Drawing.Size(156, 22)
        Me.SP_SAP_CODE.TabIndex = 6
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(351, 50)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(75, 16)
        Me.Label28.TabIndex = 138
        Me.Label28.Text = "SAP code :"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Red
        Me.Label27.Location = New System.Drawing.Point(596, 92)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(20, 25)
        Me.Label27.TabIndex = 136
        Me.Label27.Text = "*"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Red
        Me.Label26.Location = New System.Drawing.Point(288, 92)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(20, 25)
        Me.Label26.TabIndex = 136
        Me.Label26.Text = "*"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Red
        Me.Label25.Location = New System.Drawing.Point(288, 21)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(20, 25)
        Me.Label25.TabIndex = 135
        Me.Label25.Text = "*"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Bcancel
        '
        Me.Bcancel.Location = New System.Drawing.Point(511, 480)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 17
        Me.Bcancel.Tag = "15"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SP_PROD6)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.SP_PROD5)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.SP_PROD4)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.SP_PROD3)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.SP_PROD2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.SP_PROD1)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(16, 340)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(576, 106)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'SP_PROD6
        '
        Me.SP_PROD6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.SP_PROD6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SP_PROD6.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TSHIPPERBindingSource, "SP_Pro6", True))
        Me.SP_PROD6.DataSource = Me.TProductBindingSource5
        Me.SP_PROD6.DisplayMember = "Product_code"
        Me.SP_PROD6.FormattingEnabled = True
        Me.SP_PROD6.Location = New System.Drawing.Point(414, 76)
        Me.SP_PROD6.Name = "SP_PROD6"
        Me.SP_PROD6.Size = New System.Drawing.Size(156, 24)
        Me.SP_PROD6.TabIndex = 5
        Me.SP_PROD6.ValueMember = "ID"
        '
        'TProductBindingSource5
        '
        Me.TProductBindingSource5.DataMember = "T_Product"
        Me.TProductBindingSource5.DataSource = Me.FPTDataSet
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label18.Location = New System.Drawing.Point(341, 79)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 16)
        Me.Label18.TabIndex = 129
        Me.Label18.Text = "Product 6 :"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROD5
        '
        Me.SP_PROD5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.SP_PROD5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SP_PROD5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TSHIPPERBindingSource, "SP_Pro5", True))
        Me.SP_PROD5.DataSource = Me.TProductBindingSource4
        Me.SP_PROD5.DisplayMember = "Product_code"
        Me.SP_PROD5.FormattingEnabled = True
        Me.SP_PROD5.Location = New System.Drawing.Point(414, 46)
        Me.SP_PROD5.Name = "SP_PROD5"
        Me.SP_PROD5.Size = New System.Drawing.Size(156, 24)
        Me.SP_PROD5.TabIndex = 4
        Me.SP_PROD5.ValueMember = "ID"
        '
        'TProductBindingSource4
        '
        Me.TProductBindingSource4.DataMember = "T_Product"
        Me.TProductBindingSource4.DataSource = Me.FPTDataSet
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label11.Location = New System.Drawing.Point(341, 49)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(70, 16)
        Me.Label11.TabIndex = 127
        Me.Label11.Text = "Product 5 :"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROD4
        '
        Me.SP_PROD4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.SP_PROD4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SP_PROD4.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TSHIPPERBindingSource, "SP_Pro4", True))
        Me.SP_PROD4.DataSource = Me.TProductBindingSource3
        Me.SP_PROD4.DisplayMember = "Product_code"
        Me.SP_PROD4.FormattingEnabled = True
        Me.SP_PROD4.Location = New System.Drawing.Point(414, 16)
        Me.SP_PROD4.Name = "SP_PROD4"
        Me.SP_PROD4.Size = New System.Drawing.Size(156, 24)
        Me.SP_PROD4.TabIndex = 3
        Me.SP_PROD4.ValueMember = "ID"
        '
        'TProductBindingSource3
        '
        Me.TProductBindingSource3.DataMember = "T_Product"
        Me.TProductBindingSource3.DataSource = Me.FPTDataSet
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label9.Location = New System.Drawing.Point(341, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 16)
        Me.Label9.TabIndex = 125
        Me.Label9.Text = "Product 4 :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROD3
        '
        Me.SP_PROD3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.SP_PROD3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SP_PROD3.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TSHIPPERBindingSource, "SP_Pro3", True))
        Me.SP_PROD3.DataSource = Me.TProductBindingSource2
        Me.SP_PROD3.DisplayMember = "Product_code"
        Me.SP_PROD3.FormattingEnabled = True
        Me.SP_PROD3.Location = New System.Drawing.Point(105, 76)
        Me.SP_PROD3.Name = "SP_PROD3"
        Me.SP_PROD3.Size = New System.Drawing.Size(156, 24)
        Me.SP_PROD3.TabIndex = 2
        Me.SP_PROD3.ValueMember = "ID"
        '
        'TProductBindingSource2
        '
        Me.TProductBindingSource2.DataMember = "T_Product"
        Me.TProductBindingSource2.DataSource = Me.FPTDataSet
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label5.Location = New System.Drawing.Point(32, 79)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 16)
        Me.Label5.TabIndex = 123
        Me.Label5.Text = "Product 3 :"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROD2
        '
        Me.SP_PROD2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.SP_PROD2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SP_PROD2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TSHIPPERBindingSource, "SP_Pro2", True))
        Me.SP_PROD2.DataSource = Me.TProductBindingSource1
        Me.SP_PROD2.DisplayMember = "Product_code"
        Me.SP_PROD2.FormattingEnabled = True
        Me.SP_PROD2.Location = New System.Drawing.Point(105, 46)
        Me.SP_PROD2.Name = "SP_PROD2"
        Me.SP_PROD2.Size = New System.Drawing.Size(156, 24)
        Me.SP_PROD2.TabIndex = 1
        Me.SP_PROD2.ValueMember = "ID"
        '
        'TProductBindingSource1
        '
        Me.TProductBindingSource1.DataMember = "T_Product"
        Me.TProductBindingSource1.DataSource = Me.FPTDataSet
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label3.Location = New System.Drawing.Point(32, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 16)
        Me.Label3.TabIndex = 121
        Me.Label3.Text = "Product 2 :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROD1
        '
        Me.SP_PROD1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.SP_PROD1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.SP_PROD1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TSHIPPERBindingSource, "SP_Pro1", True))
        Me.SP_PROD1.DataSource = Me.TProductBindingSource
        Me.SP_PROD1.DisplayMember = "Product_code"
        Me.SP_PROD1.FormattingEnabled = True
        Me.SP_PROD1.Location = New System.Drawing.Point(105, 16)
        Me.SP_PROD1.Name = "SP_PROD1"
        Me.SP_PROD1.Size = New System.Drawing.Size(156, 24)
        Me.SP_PROD1.TabIndex = 0
        Me.SP_PROD1.ValueMember = "ID"
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label10.Location = New System.Drawing.Point(32, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 16)
        Me.Label10.TabIndex = 118
        Me.Label10.Text = "Product 1 :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Bsave
        '
        Me.Bsave.Location = New System.Drawing.Point(430, 480)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 16
        Me.Bsave.Tag = "15"
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'SP_WEB
        '
        Me.SP_WEB.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Website", True))
        Me.SP_WEB.Location = New System.Drawing.Point(430, 293)
        Me.SP_WEB.Name = "SP_WEB"
        Me.SP_WEB.Size = New System.Drawing.Size(156, 22)
        Me.SP_WEB.TabIndex = 12
        '
        'SP_EMAIL
        '
        Me.SP_EMAIL.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Email", True))
        Me.SP_EMAIL.Location = New System.Drawing.Point(430, 265)
        Me.SP_EMAIL.Name = "SP_EMAIL"
        Me.SP_EMAIL.Size = New System.Drawing.Size(156, 22)
        Me.SP_EMAIL.TabIndex = 11
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(358, 294)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(69, 16)
        Me.Label23.TabIndex = 129
        Me.Label23.Text = "Web Site :"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(379, 268)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(48, 16)
        Me.Label24.TabIndex = 128
        Me.Label24.Text = "Email :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_FAX
        '
        Me.SP_FAX.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Fax", True))
        Me.SP_FAX.Location = New System.Drawing.Point(121, 319)
        Me.SP_FAX.Name = "SP_FAX"
        Me.SP_FAX.Size = New System.Drawing.Size(156, 22)
        Me.SP_FAX.TabIndex = 10
        '
        'SP_PHONE
        '
        Me.SP_PHONE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Tel", True))
        Me.SP_PHONE.Location = New System.Drawing.Point(121, 291)
        Me.SP_PHONE.Name = "SP_PHONE"
        Me.SP_PHONE.Size = New System.Drawing.Size(156, 22)
        Me.SP_PHONE.TabIndex = 9
        '
        'SP_POSCODE
        '
        Me.SP_POSCODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_ZIPCODE", True))
        Me.SP_POSCODE.Location = New System.Drawing.Point(121, 265)
        Me.SP_POSCODE.Name = "SP_POSCODE"
        Me.SP_POSCODE.Size = New System.Drawing.Size(156, 22)
        Me.SP_POSCODE.TabIndex = 8
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(82, 322)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(36, 16)
        Me.Label20.TabIndex = 124
        Me.Label20.Text = "Fax :"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(65, 296)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(53, 16)
        Me.Label21.TabIndex = 123
        Me.Label21.Text = "Phone :"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(32, 268)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(86, 16)
        Me.Label22.TabIndex = 122
        Me.Label22.Text = "Postal code :"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.SP_ADDEN)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.SP_PROEN)
        Me.GroupBox5.Controls.Add(Me.SP_DISEN)
        Me.GroupBox5.Controls.Add(Me.SP_NAMEEN)
        Me.GroupBox5.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox5.Location = New System.Drawing.Point(16, 73)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(266, 185)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "English"
        '
        'SP_ADDEN
        '
        Me.SP_ADDEN.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_AddressEN", True))
        Me.SP_ADDEN.Location = New System.Drawing.Point(105, 44)
        Me.SP_ADDEN.Name = "SP_ADDEN"
        Me.SP_ADDEN.Size = New System.Drawing.Size(156, 79)
        Me.SP_ADDEN.TabIndex = 1
        Me.SP_ADDEN.Text = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label8.Location = New System.Drawing.Point(35, 159)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 16)
        Me.Label8.TabIndex = 122
        Me.Label8.Text = "Province :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label7.Location = New System.Drawing.Point(48, 133)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 16)
        Me.Label7.TabIndex = 121
        Me.Label7.Text = "District :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label15.Location = New System.Drawing.Point(37, 48)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(65, 16)
        Me.Label15.TabIndex = 119
        Me.Label15.Text = "Address :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label14.Location = New System.Drawing.Point(46, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(51, 16)
        Me.Label14.TabIndex = 118
        Me.Label14.Text = "Name :"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROEN
        '
        Me.SP_PROEN.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_ProvinceEN", True))
        Me.SP_PROEN.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SP_PROEN.Location = New System.Drawing.Point(105, 157)
        Me.SP_PROEN.Name = "SP_PROEN"
        Me.SP_PROEN.Size = New System.Drawing.Size(156, 22)
        Me.SP_PROEN.TabIndex = 3
        '
        'SP_DISEN
        '
        Me.SP_DISEN.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_AmphurEN", True))
        Me.SP_DISEN.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SP_DISEN.Location = New System.Drawing.Point(105, 129)
        Me.SP_DISEN.Name = "SP_DISEN"
        Me.SP_DISEN.Size = New System.Drawing.Size(156, 22)
        Me.SP_DISEN.TabIndex = 2
        '
        'SP_NAMEEN
        '
        Me.SP_NAMEEN.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_NameEN", True))
        Me.SP_NAMEEN.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SP_NAMEEN.Location = New System.Drawing.Point(105, 16)
        Me.SP_NAMEEN.Name = "SP_NAMEEN"
        Me.SP_NAMEEN.Size = New System.Drawing.Size(156, 22)
        Me.SP_NAMEEN.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.SP_ADDTH)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.SP_PROTH)
        Me.GroupBox4.Controls.Add(Me.SP_DISTH)
        Me.GroupBox4.Controls.Add(Me.SP_NAMETH)
        Me.GroupBox4.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox4.Location = New System.Drawing.Point(326, 73)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(266, 185)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "ไทย"
        '
        'SP_ADDTH
        '
        Me.SP_ADDTH.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_AddressTH", True))
        Me.SP_ADDTH.Location = New System.Drawing.Point(104, 44)
        Me.SP_ADDTH.Name = "SP_ADDTH"
        Me.SP_ADDTH.Size = New System.Drawing.Size(156, 79)
        Me.SP_ADDTH.TabIndex = 1
        Me.SP_ADDTH.Text = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label4.Location = New System.Drawing.Point(54, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 122
        Me.Label4.Text = "จังหวัด :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label16.Location = New System.Drawing.Point(39, 132)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(62, 16)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "เขต/อำเภอ :"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label17.Location = New System.Drawing.Point(67, 48)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 16)
        Me.Label17.TabIndex = 119
        Me.Label17.Text = "ที่อยู่ :"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label19.Location = New System.Drawing.Point(14, 19)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(87, 16)
        Me.Label19.TabIndex = 118
        Me.Label19.Text = "ชื่อผู้ขนส่งสินค้า :"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_PROTH
        '
        Me.SP_PROTH.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_ProvinceTH", True))
        Me.SP_PROTH.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SP_PROTH.Location = New System.Drawing.Point(104, 157)
        Me.SP_PROTH.Name = "SP_PROTH"
        Me.SP_PROTH.Size = New System.Drawing.Size(156, 22)
        Me.SP_PROTH.TabIndex = 3
        '
        'SP_DISTH
        '
        Me.SP_DISTH.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_AmphurTH", True))
        Me.SP_DISTH.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SP_DISTH.Location = New System.Drawing.Point(104, 129)
        Me.SP_DISTH.Name = "SP_DISTH"
        Me.SP_DISTH.Size = New System.Drawing.Size(156, 22)
        Me.SP_DISTH.TabIndex = 2
        '
        'SP_NAMETH
        '
        Me.SP_NAMETH.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_NameTH", True))
        Me.SP_NAMETH.ForeColor = System.Drawing.SystemColors.WindowText
        Me.SP_NAMETH.Location = New System.Drawing.Point(104, 16)
        Me.SP_NAMETH.Name = "SP_NAMETH"
        Me.SP_NAMETH.Size = New System.Drawing.Size(156, 22)
        Me.SP_NAMETH.TabIndex = 0
        '
        'SP_TYPE
        '
        Me.SP_TYPE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Type", True))
        Me.SP_TYPE.Location = New System.Drawing.Point(430, 19)
        Me.SP_TYPE.Name = "SP_TYPE"
        Me.SP_TYPE.Size = New System.Drawing.Size(156, 22)
        Me.SP_TYPE.TabIndex = 5
        '
        'SP_CRDATE
        '
        Me.SP_CRDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.SP_CRDATE.Enabled = False
        Me.SP_CRDATE.Location = New System.Drawing.Point(121, 452)
        Me.SP_CRDATE.Name = "SP_CRDATE"
        Me.SP_CRDATE.Size = New System.Drawing.Size(156, 22)
        Me.SP_CRDATE.TabIndex = 14
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(336, 455)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 95
        Me.Label13.Text = "Update Date :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(21, 455)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 16)
        Me.Label12.TabIndex = 97
        Me.Label12.Text = "Created date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_CODE
        '
        Me.SP_CODE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Code", True))
        Me.SP_CODE.Location = New System.Drawing.Point(126, 19)
        Me.SP_CODE.Name = "SP_CODE"
        Me.SP_CODE.Size = New System.Drawing.Size(156, 22)
        Me.SP_CODE.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(66, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 16)
        Me.Label1.TabIndex = 83
        Me.Label1.Text = "Code :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_UPDATE
        '
        Me.SP_UPDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.SP_UPDATE.Enabled = False
        Me.SP_UPDATE.Location = New System.Drawing.Point(430, 452)
        Me.SP_UPDATE.Name = "SP_UPDATE"
        Me.SP_UPDATE.Size = New System.Drawing.Size(156, 22)
        Me.SP_UPDATE.TabIndex = 15
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(381, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 16)
        Me.Label6.TabIndex = 94
        Me.Label6.Text = "Type :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 16)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "Contact Name :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SP_CONNAME
        '
        Me.SP_CONNAME.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSHIPPERBindingSource, "SP_Contact", True))
        Me.SP_CONNAME.Location = New System.Drawing.Point(126, 47)
        Me.SP_CONNAME.Name = "SP_CONNAME"
        Me.SP_CONNAME.Size = New System.Drawing.Size(156, 22)
        Me.SP_CONNAME.TabIndex = 4
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TSHIPPERBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(1030, 27)
        Me.BindingNavigator1.TabIndex = 4
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'f04_01_Shipper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1030, 552)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.DetailGroup)
        Me.Controls.Add(Me.MeterGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "f04_01_Shipper"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "15"
        Me.Text = "Sales ORG."
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.TProductBindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents SP_TYPE As System.Windows.Forms.TextBox
    Friend WithEvents SP_CRDATE As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents SP_CODE As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SP_UPDATE As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SP_CONNAME As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents SP_PROEN As System.Windows.Forms.TextBox
    Friend WithEvents SP_DISEN As System.Windows.Forms.TextBox
    Friend WithEvents SP_NAMEEN As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents SP_PROTH As System.Windows.Forms.TextBox
    Friend WithEvents SP_DISTH As System.Windows.Forms.TextBox
    Friend WithEvents SP_NAMETH As System.Windows.Forms.TextBox
    Friend WithEvents SP_ADDEN As System.Windows.Forms.RichTextBox
    Friend WithEvents SP_ADDTH As System.Windows.Forms.RichTextBox
    Friend WithEvents SP_WEB As System.Windows.Forms.TextBox
    Friend WithEvents SP_EMAIL As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents SP_FAX As System.Windows.Forms.TextBox
    Friend WithEvents SP_PHONE As System.Windows.Forms.TextBox
    Friend WithEvents SP_POSCODE As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents SP_PROD6 As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents SP_PROD5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents SP_PROD4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents SP_PROD3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SP_PROD2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SP_PROD1 As System.Windows.Forms.ComboBox
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents TProductBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents TProductBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents TProductBindingSource5 As System.Windows.Forms.BindingSource
    Friend WithEvents TProductBindingSource4 As System.Windows.Forms.BindingSource
    Friend WithEvents TProductBindingSource3 As System.Windows.Forms.BindingSource
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SP_SAP_CODE As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPNameENDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPNameTHDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
