﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_ShipmentType
    Dim MyErrorProvider As New ErrorProviderExtended
    Private Sub ShipmentType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPMENT_TYPE' table. You can move, or remove it, as needed.
        Me.T_SHIPMENT_TYPETableAdapter.Fill(Me.FPTDataSet.T_SHIPMENT_TYPE)
        MeterGrid.SuspendLayout()
        'Add controls one by one in error provider.
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.ClearAllErrorMessages()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        MyErrorProvider.Controls.Add(P_CODE, "Shipment Code :")
        MyErrorProvider.Controls.Add(P_NAME, "Shipment Name :")
        ' MyErrorProvider.Controls.Add(P_CAPASITY, "Packing Capasity :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        MeterGrid.ResumeLayout()
        Me.BringToFront()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub
    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        TSHIPMENTTYPEBindingSource.Item(TSHIPMENTTYPEBindingSource.Position)("Cre_date") = Now
        P_CODE.Focus()
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub

    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        DetailGroup.Enabled = True
    End Sub

    Private Sub MeterGrid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MeterGrid.KeyUp
        If e.KeyCode = (46) Then
            DetailGroup.Enabled = True
        End If
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_SHIPMENT_TYPE"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_SHIPMENT_TYPE")
                    ds.AcceptChanges()
                End If
            Finally
                da.Dispose()
            End Try
        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub
    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                Try
                    TSHIPMENTTYPEBindingSource.Item(TSHIPMENTTYPEBindingSource.Position)("Update_date") = Now
                    TSHIPMENTTYPEBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_SHIPMENT_TYPE").EndCurrentEdit()
                    T_SHIPMENT_TYPETableAdapter.Update(FPTDataSet.T_SHIPMENT_TYPE)
                    ' Save(FPTDataSet)
                    ShipmentType_Load(sender, e)
                    Me.BringToFront()
                Finally
                End Try
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TSHIPMENTTYPEBindingSource.CancelEdit()
        ShipmentType_Load(sender, e)
    End Sub

    Private Sub TSHIPMENTTYPEBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSHIPMENTTYPEBindingSource.PositionChanged
        Try
            If BtEdit.Enabled = False Then TSHIPMENTTYPEBindingSource.Item(TSHIPMENTTYPEBindingSource.Position)("Update_date") = Now

        Catch ex As Exception

        End Try
    End Sub
    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_SHIPMENT_TYPE.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_SHIPMENT_TYPE.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_SHIPMENT_TYPE.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        ' Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_SHIPMENT_TYPE.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TSHIPMENTTYPEBindingSource.Filter = StrFilter
        Else : TSHIPMENTTYPEBindingSource.RemoveFilter()
        End If

    End Sub
End Class