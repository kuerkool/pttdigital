﻿Imports System.Net
Imports TAS_TOL.SAP02
Imports TAS_TOL.SAP03
Imports TAS_TOL.FSAP03
Public Class DoDetail


    Private Sub AddDo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPMENT_TYPE' table. You can move, or remove it, as needed.
        Me.T_SHIPMENT_TYPETableAdapter.Fill(Me.FPTDataSet.T_SHIPMENT_TYPE)
        'TODO: This line of code loads data into the 'FPTDataSet.T_DO' table. You can move, or remove it, as needed.
        Me.T_DOTableAdapter.Fill(Me.FPTDataSet.T_DO)
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub Save(ByVal ds As DataSet, ByVal Where As String)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_DO " '+ Where
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                If ds.HasChanges Then
                    '  MessageBox.Show(da.UpdateCommand.CommandText.ToString())
                    da.Update(ds, "T_DO")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub ShipSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShipSave.Click, Bsave.Click
        TDOBindingSource.Item(TDOBindingSource.Position)("Update_date") = Now
        TDOBindingSource.EndEdit()
        'TDOBindingSource.Filter = ""
        BindingContext(FPTDataSet, "T_DO").EndCurrentEdit()
        Save(FPTDataSet, "")
        AddDo_Load(sender, e)
        Me.BringToFront()
        'TDOBindingSource

    End Sub


    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click, ShipCancel.Click
        TDOBindingSource.CancelEdit()
        AddDo_Load(sender, e)
        Close()
    End Sub

    Private Sub DoDetail_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown

        '  TDOBindingSource.
    End Sub

    Private Sub BtPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtPost.Click
        Cursor = Cursors.WaitCursor
        Me.BtPost.Enabled = False
        '  BtPost.Enabled = False
        Bsave.PerformClick()

        Dim _PostDOData(0) As Create_Shipment_DO03_Out_DTITEM
        Dim _PostShipData(0) As Create_Shipment_SHIP03_Out_DTITEM

        _PostDOData(0) = New Create_Shipment_DO03_Out_DTITEM
        _PostShipData(0) = New Create_Shipment_SHIP03_Out_DTITEM

        _PostDOData(0).DO_NO = TDOBindingSource.Item(TDOBindingSource.Position)("DO_NO").ToString
        _PostDOData(0).DO_ITEM = TDOBindingSource.Item(TDOBindingSource.Position)("DO_ITEM").ToString
        _PostDOData(0).SALES_ORG = TDOBindingSource.Item(TDOBindingSource.Position)("SALES_ORG").ToString
        _PostDOData(0).SD_DOC_CAT = TDOBindingSource.Item(TDOBindingSource.Position)("SD_DOC_CAT").ToString
        _PostDOData(0).ACT_GI_DATE = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("pLAN_GI_DATE").ToString).ToString("dd/MM/yyyy")  ' Convert.ToDateTime("10/07/2011").ToString("dd/MM/yyyy") ''''''''''''''มั่ว''''''''''''''''''''
        _PostDOData(0).MAT_CODE = TDOBindingSource.Item(TDOBindingSource.Position)("MAT_CODE").ToString
        _PostDOData(0).PLANT = TDOBindingSource.Item(TDOBindingSource.Position)("PLANT").ToString
        _PostDOData(0).STORAGE_LOC = TDOBindingSource.Item(TDOBindingSource.Position)("STORAGE_LOC").ToString
        _PostDOData(0).BATCH_NO = TDOBindingSource.Item(TDOBindingSource.Position)("BATCH_NO").ToString
        _PostDOData(0).ACT_QTY_DELV_SU = TDOBindingSource.Item(TDOBindingSource.Position)("ACT_QTY_DELV_SU").ToString
        _PostDOData(0).BASE_UOM = TDOBindingSource.Item(TDOBindingSource.Position)("BASE_UOM").ToString
        _PostDOData(0).SALES_UNIT = TDOBindingSource.Item(TDOBindingSource.Position)("SALES_UNIT").ToString
        _PostDOData(0).NET_WEIGHT = TDOBindingSource.Item(TDOBindingSource.Position)("NET_WEIGHT").ToString
        _PostDOData(0).GROSS_WEIGHT = TDOBindingSource.Item(TDOBindingSource.Position)("GROSS_WEIGHT").ToString
        _PostDOData(0).WEIGHT_UNIT = TDOBindingSource.Item(TDOBindingSource.Position)("WEIGHT_UNIT").ToString
        _PostDOData(0).ACT_QTY_DELV_SKU = TDOBindingSource.Item(TDOBindingSource.Position)("ACT_QTY_DELV_SKU").ToString    '''''''''''''''''มั่ว'''''''''''''''''


        '   _PostShipData(0).SHIP_NO = TDOBindingSource.Item(TDOBindingSource.Position)("SHIP_NO").ToString
        _PostShipData(0).SHIP_TYPE = TDOBindingSource.Item(TDOBindingSource.Position)("SHIP_TYPE").ToString  '''''''''''''''''''''''''''
        _PostShipData(0).TRN_PLAN_POINT = TDOBindingSource.Item(TDOBindingSource.Position)("SALES_ORG").ToString  ''''''''''''''''''''
        _PostShipData(0).CREATE_DATE = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("CREATE_DATE").ToString).ToString("dd/MM/yyyy") 'Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
        _PostShipData(0).CREATE_TIME = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("CREATE_TIME").ToString).ToString("hh:mm:ss") ' Convert.ToDateTime("11:11:11").ToString("hh:mm:ss")
        _PostShipData(0).CONTAINER = TDOBindingSource.Item(TDOBindingSource.Position)("CONTAINER").ToString
        _PostShipData(0).DRIVER_NAME = TDOBindingSource.Item(TDOBindingSource.Position)("DRIVER_NAME").ToString
        _PostShipData(0).CAR_LINCENSE = TDOBindingSource.Item(TDOBindingSource.Position)("CAR_LINCENSE").ToString
        _PostShipData(0).FORWD_AGENT = TDOBindingSource.Item(TDOBindingSource.Position)("FORWD_AGENT").ToString ' บริษัทขนส่ง
        _PostShipData(0).WEIGHT_IN = TDOBindingSource.Item(TDOBindingSource.Position)("WEIGHT_IN").ToString
        _PostShipData(0).WEIGHT_OUT = TDOBindingSource.Item(TDOBindingSource.Position)("WEIGHT_OUT").ToString
        _PostShipData(0).SEAL_NO = TDOBindingSource.Item(TDOBindingSource.Position)("SEAL_NO").ToString
        _PostShipData(0).SHIPP_TYPE = TDOBindingSource.Item(TDOBindingSource.Position)("SHIPP_TYPE").ToString
        _PostShipData(0).SHIPP_COND = TDOBindingSource.Item(TDOBindingSource.Position)("SHIPP_COND").ToString
        _PostShipData(0).DO_NO = TDOBindingSource.Item(TDOBindingSource.Position)("sHIPPdO_NO").ToString
        '  _PostShipData(0).ACT_LOAD_SDATE = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("ACT_LOAD_SDATE").ToString).ToString("dd/MM/yyyy")  ' Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
        '  _PostShipData(0).ACT_LOAD_STIME = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("ACT_LOAD_STIME").ToString).ToString("hh:mm:ss") ' Convert.ToDateTime("11:11:11").ToString("hh:mm:ss")
        '   _PostShipData(0).ACT_LOAD_EDATE = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("ACT_LOAD_EDATE").ToString).ToString("dd/MM/yyyy")  ' Convert.ToDateTime("01/07/2011").ToString("dd/MM/yyyy")
        ' _PostShipData(0).ACT_LOAD_ETIME = Convert.ToDateTime(TDOBindingSource.Item(TDOBindingSource.Position)("ACT_LOAD_ETIME").ToString).ToString("hh:mm:ss") ' Convert.ToDateTime("11:11:11").ToString("hh:mm:ss")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '   FSAP03.POST_DO_SHIP(_PostDOData, _PostShipData)
    End Sub

End Class