﻿Public Class F04_DENLOAD

    Private Sub F04_DENLOAD_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_LOADTANK' table. You can move, or remove it, as needed.
        Me.T_LOADTANKTableAdapter.Fill(Me.FPTDataSet.T_LOADTANK)
        'TODO: This line of code loads data into the 'FPTDataSet.T_YME_ESSO' table. You can move, or remove it, as needed.
        Me.T_YME_ESSOTableAdapter.Fill(Me.FPTDataSet.T_YME_ESSO)

    End Sub

    Private Sub BTSave_Click(sender As System.Object, e As System.EventArgs) Handles BTSave.Click
        TLOADTANKBindingSource.EndEdit()
        TYMEESSOBindingSource.EndEdit()
        Me.T_LOADTANKTableAdapter.Update(Me.FPTDataSet.T_LOADTANK)
        Me.T_YME_ESSOTableAdapter.Update(Me.FPTDataSet.T_YME_ESSO)
    End Sub

    Private Sub BTCancel_Click(sender As System.Object, e As System.EventArgs) Handles BTCancel.Click
        TLOADTANKBindingSource.CancelEdit()
        TYMEESSOBindingSource.CancelEdit()
        Me.FPTDataSet.T_LOADTANK.RejectChanges()
        Me.FPTDataSet.T_YME_ESSO.RejectChanges()
    End Sub
End Class
