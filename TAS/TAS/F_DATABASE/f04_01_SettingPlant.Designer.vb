﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_SettingPlant
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_SettingPlant))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDecimalColumn1 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn1 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn2 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDecimalColumn2 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn14 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn15 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn3 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim GridViewTextBoxColumn16 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn4 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim SortDescriptor1 As Telerik.WinControls.Data.SortDescriptor = New Telerik.WinControls.Data.SortDescriptor()
        Dim GridViewDecimalColumn3 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewDateTimeColumn5 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim GridViewTextBoxColumn17 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn18 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn19 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn20 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn21 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn22 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn23 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn24 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn25 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDecimalColumn4 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewTextBoxColumn26 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn27 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn28 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn29 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn30 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn31 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn32 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn33 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn34 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn35 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn36 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn6 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim GridViewTextBoxColumn37 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDecimalColumn5 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewTextBoxColumn38 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn39 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn40 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn41 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn42 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn43 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn44 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.TSETTINGPLANTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.RadGridView1 = New Telerik.WinControls.UI.RadGridView()
        Me.GDETAIL = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadTextBox2 = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel7 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.RadButton2 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton1 = New Telerik.WinControls.UI.RadButton()
        Me.RadTextBox1 = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel5 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.RadDateTimePicker1 = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadMultiColumnComboBox3 = New Telerik.WinControls.UI.RadMultiColumnComboBox()
        Me.TLOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RadMultiColumnComboBox2 = New Telerik.WinControls.UI.RadMultiColumnComboBox()
        Me.TSTO1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadMultiColumnComboBox1 = New Telerik.WinControls.UI.RadMultiColumnComboBox()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TSETTINGPLANTTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.TSETTINGPLANTTableAdapter()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.T_STO1TableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_STO1TableAdapter()
        Me.T_LOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_LOTableAdapter()
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.TSETTINGPLANTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GDETAIL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GDETAIL.SuspendLayout()
        CType(Me.RadTextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadDateTimePicker1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox3.EditorControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TLOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSTO1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox1.EditorControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TSETTINGPLANTBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(884, 27)
        Me.BindingNavigator1.TabIndex = 7
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'TSETTINGPLANTBindingSource
        '
        Me.TSETTINGPLANTBindingSource.DataMember = "TSETTINGPLANT"
        Me.TSETTINGPLANTBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'RadGridView1
        '
        Me.RadGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadGridView1.Location = New System.Drawing.Point(0, 27)
        '
        'RadGridView1
        '
        Me.RadGridView1.MasterTemplate.AllowAddNewRow = False
        Me.RadGridView1.MasterTemplate.AllowDeleteRow = False
        Me.RadGridView1.MasterTemplate.AllowEditRow = False
        Me.RadGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill
        GridViewTextBoxColumn1.FieldName = "SALESORG"
        GridViewTextBoxColumn1.HeaderText = "Sales org code"
        GridViewTextBoxColumn1.IsAutoGenerated = True
        GridViewTextBoxColumn1.Name = "SALESORG"
        GridViewTextBoxColumn1.Width = 164
        GridViewTextBoxColumn2.FieldName = "PLANT"
        GridViewTextBoxColumn2.HeaderText = "Plant"
        GridViewTextBoxColumn2.IsAutoGenerated = True
        GridViewTextBoxColumn2.Name = "PLANT"
        GridViewTextBoxColumn2.Width = 164
        GridViewTextBoxColumn3.FieldName = "LOAD_PT"
        GridViewTextBoxColumn3.HeaderText = "Loading location"
        GridViewTextBoxColumn3.IsAutoGenerated = True
        GridViewTextBoxColumn3.Name = "LOAD_PT"
        GridViewTextBoxColumn3.Width = 252
        Me.RadGridView1.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3})
        Me.RadGridView1.MasterTemplate.DataSource = Me.TSETTINGPLANTBindingSource
        Me.RadGridView1.MasterTemplate.EnableFiltering = True
        Me.RadGridView1.MasterTemplate.EnableGrouping = False
        Me.RadGridView1.Name = "RadGridView1"
        Me.RadGridView1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 1)
        Me.RadGridView1.ReadOnly = True
        Me.RadGridView1.Size = New System.Drawing.Size(600, 365)
        Me.RadGridView1.TabIndex = 8
        Me.RadGridView1.Text = "RadGridView1"
        Me.RadGridView1.ThemeName = "Office2010Blue"
        '
        'GDETAIL
        '
        Me.GDETAIL.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.GDETAIL.Controls.Add(Me.RadTextBox2)
        Me.GDETAIL.Controls.Add(Me.RadLabel7)
        Me.GDETAIL.Controls.Add(Me.RadLabel6)
        Me.GDETAIL.Controls.Add(Me.RichTextBox1)
        Me.GDETAIL.Controls.Add(Me.RadButton2)
        Me.GDETAIL.Controls.Add(Me.RadButton1)
        Me.GDETAIL.Controls.Add(Me.RadTextBox1)
        Me.GDETAIL.Controls.Add(Me.RadLabel5)
        Me.GDETAIL.Controls.Add(Me.RadLabel4)
        Me.GDETAIL.Controls.Add(Me.RadLabel3)
        Me.GDETAIL.Controls.Add(Me.RadDateTimePicker1)
        Me.GDETAIL.Controls.Add(Me.RadLabel2)
        Me.GDETAIL.Controls.Add(Me.RadMultiColumnComboBox3)
        Me.GDETAIL.Controls.Add(Me.RadMultiColumnComboBox2)
        Me.GDETAIL.Controls.Add(Me.RadLabel1)
        Me.GDETAIL.Controls.Add(Me.RadMultiColumnComboBox1)
        Me.GDETAIL.Dock = System.Windows.Forms.DockStyle.Right
        Me.GDETAIL.Enabled = False
        Me.GDETAIL.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GDETAIL.HeaderText = "Detail"
        Me.GDETAIL.Location = New System.Drawing.Point(600, 27)
        Me.GDETAIL.Name = "GDETAIL"
        Me.GDETAIL.Size = New System.Drawing.Size(284, 365)
        Me.GDETAIL.TabIndex = 0
        Me.GDETAIL.Text = "Detail"
        Me.GDETAIL.ThemeName = "Office2010Blue"
        '
        'RadTextBox2
        '
        Me.RadTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGPLANTBindingSource, "PLANT_NAME", True))
        Me.RadTextBox2.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadTextBox2.Location = New System.Drawing.Point(113, 171)
        Me.RadTextBox2.Name = "RadTextBox2"
        Me.RadTextBox2.Size = New System.Drawing.Size(150, 21)
        Me.RadTextBox2.TabIndex = 11
        Me.RadTextBox2.TabStop = False
        Me.RadTextBox2.ThemeName = "Office2010Blue"
        '
        'RadLabel7
        '
        Me.RadLabel7.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel7.Location = New System.Drawing.Point(23, 171)
        Me.RadLabel7.Name = "RadLabel7"
        Me.RadLabel7.Size = New System.Drawing.Size(84, 19)
        Me.RadLabel7.TabIndex = 16
        Me.RadLabel7.Text = "Plant Name :"
        Me.RadLabel7.ThemeName = "Office2010Blue"
        '
        'RadLabel6
        '
        Me.RadLabel6.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel6.Location = New System.Drawing.Point(39, 198)
        Me.RadLabel6.Name = "RadLabel6"
        Me.RadLabel6.Size = New System.Drawing.Size(68, 19)
        Me.RadLabel6.TabIndex = 15
        Me.RadLabel6.Text = "DB Con.  :"
        Me.RadLabel6.ThemeName = "Office2010Blue"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGPLANTBindingSource, "DBConStr", True))
        Me.RichTextBox1.Location = New System.Drawing.Point(113, 198)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(150, 68)
        Me.RichTextBox1.TabIndex = 12
        Me.RichTextBox1.Text = ""
        '
        'RadButton2
        '
        Me.RadButton2.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadButton2.Location = New System.Drawing.Point(153, 319)
        Me.RadButton2.Name = "RadButton2"
        Me.RadButton2.Size = New System.Drawing.Size(110, 34)
        Me.RadButton2.TabIndex = 12
        Me.RadButton2.Tag = "10"
        Me.RadButton2.Text = "Cancel"
        Me.RadButton2.ThemeName = "Office2010Blue"
        '
        'RadButton1
        '
        Me.RadButton1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadButton1.Location = New System.Drawing.Point(30, 319)
        Me.RadButton1.Name = "RadButton1"
        Me.RadButton1.Size = New System.Drawing.Size(110, 34)
        Me.RadButton1.TabIndex = 11
        Me.RadButton1.Tag = "10"
        Me.RadButton1.Text = "Save"
        Me.RadButton1.ThemeName = "Office2010Blue"
        '
        'RadTextBox1
        '
        Me.RadTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGPLANTBindingSource, "UPdateBy", True))
        Me.RadTextBox1.Enabled = False
        Me.RadTextBox1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadTextBox1.Location = New System.Drawing.Point(113, 144)
        Me.RadTextBox1.Name = "RadTextBox1"
        Me.RadTextBox1.Size = New System.Drawing.Size(150, 21)
        Me.RadTextBox1.TabIndex = 10
        Me.RadTextBox1.TabStop = False
        Me.RadTextBox1.ThemeName = "Office2010Blue"
        '
        'RadLabel5
        '
        Me.RadLabel5.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel5.Location = New System.Drawing.Point(35, 146)
        Me.RadLabel5.Name = "RadLabel5"
        Me.RadLabel5.Size = New System.Drawing.Size(72, 19)
        Me.RadLabel5.TabIndex = 9
        Me.RadLabel5.Text = "UpdateBy :"
        Me.RadLabel5.ThemeName = "Office2010Blue"
        '
        'RadLabel4
        '
        Me.RadLabel4.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel4.Location = New System.Drawing.Point(21, 118)
        Me.RadLabel4.Name = "RadLabel4"
        Me.RadLabel4.Size = New System.Drawing.Size(86, 19)
        Me.RadLabel4.TabIndex = 7
        Me.RadLabel4.Text = "UpdateDate :"
        Me.RadLabel4.ThemeName = "Office2010Blue"
        '
        'RadLabel3
        '
        Me.RadLabel3.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel3.Location = New System.Drawing.Point(11, 90)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(96, 19)
        Me.RadLabel3.TabIndex = 6
        Me.RadLabel3.Text = "Loading Point :"
        Me.RadLabel3.ThemeName = "Office2010Blue"
        '
        'RadDateTimePicker1
        '
        Me.RadDateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TSETTINGPLANTBindingSource, "UPdateDate", True))
        Me.RadDateTimePicker1.Enabled = False
        Me.RadDateTimePicker1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.RadDateTimePicker1.Location = New System.Drawing.Point(113, 116)
        Me.RadDateTimePicker1.Name = "RadDateTimePicker1"
        Me.RadDateTimePicker1.Size = New System.Drawing.Size(150, 21)
        Me.RadDateTimePicker1.TabIndex = 1
        Me.RadDateTimePicker1.TabStop = False
        Me.RadDateTimePicker1.Text = "14/5/2556 13:03"
        Me.RadDateTimePicker1.ThemeName = "Office2010Blue"
        Me.RadDateTimePicker1.Value = New Date(2013, 5, 14, 13, 3, 39, 818)
        '
        'RadLabel2
        '
        Me.RadLabel2.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel2.Location = New System.Drawing.Point(62, 62)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(45, 19)
        Me.RadLabel2.TabIndex = 5
        Me.RadLabel2.Text = "Plant :"
        Me.RadLabel2.ThemeName = "Office2010Blue"
        '
        'RadMultiColumnComboBox3
        '
        Me.RadMultiColumnComboBox3.AutoSizeDropDownHeight = True
        Me.RadMultiColumnComboBox3.AutoSizeDropDownToBestFit = True
        Me.RadMultiColumnComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGPLANTBindingSource, "LOAD_PT", True))
        Me.RadMultiColumnComboBox3.DataSource = Me.TLOBindingSource
        Me.RadMultiColumnComboBox3.DblClickRotate = True
        Me.RadMultiColumnComboBox3.DisplayMember = "LOAD_LOCATION"
        '
        'RadMultiColumnComboBox3.NestedRadGridView
        '
        Me.RadMultiColumnComboBox3.EditorControl.BackColor = System.Drawing.SystemColors.Window
        Me.RadMultiColumnComboBox3.EditorControl.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadMultiColumnComboBox3.EditorControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.RadMultiColumnComboBox3.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadMultiColumnComboBox3.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.RadMultiColumnComboBox3.EditorControl.Location = New System.Drawing.Point(22, 57)
        '
        '
        '
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.AllowAddNewRow = False
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.AllowCellContextMenu = False
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.AllowColumnChooser = False
        GridViewDecimalColumn1.DataType = GetType(Integer)
        GridViewDecimalColumn1.EnableExpressionEditor = False
        GridViewDecimalColumn1.FieldName = "ID"
        GridViewDecimalColumn1.HeaderText = "ID"
        GridViewDecimalColumn1.IsAutoGenerated = True
        GridViewDecimalColumn1.IsVisible = False
        GridViewDecimalColumn1.Name = "ID"
        GridViewDecimalColumn1.ReadOnly = True
        GridViewTextBoxColumn4.EnableExpressionEditor = False
        GridViewTextBoxColumn4.FieldName = "LOAD_LOCATION"
        GridViewTextBoxColumn4.HeaderText = "LOAD_LOCATION"
        GridViewTextBoxColumn4.IsAutoGenerated = True
        GridViewTextBoxColumn4.Name = "Load Location"
        GridViewTextBoxColumn4.Width = 125
        GridViewTextBoxColumn5.EnableExpressionEditor = False
        GridViewTextBoxColumn5.FieldName = "PLANT_LOCATION"
        GridViewTextBoxColumn5.HeaderText = "PLANT_LOCATION"
        GridViewTextBoxColumn5.IsAutoGenerated = True
        GridViewTextBoxColumn5.Name = "Plant Location"
        GridViewTextBoxColumn5.Width = 170
        GridViewTextBoxColumn6.EnableExpressionEditor = False
        GridViewTextBoxColumn6.FieldName = "PRINT_LOC_DES"
        GridViewTextBoxColumn6.HeaderText = "PRINT_LOC_DES"
        GridViewTextBoxColumn6.IsAutoGenerated = True
        GridViewTextBoxColumn6.IsVisible = False
        GridViewTextBoxColumn6.Name = "PRINT_LOC_DES"
        GridViewTextBoxColumn7.EnableExpressionEditor = False
        GridViewTextBoxColumn7.FieldName = "VALUE"
        GridViewTextBoxColumn7.HeaderText = "VALUE"
        GridViewTextBoxColumn7.IsAutoGenerated = True
        GridViewTextBoxColumn7.IsVisible = False
        GridViewTextBoxColumn7.Name = "VALUE"
        GridViewTextBoxColumn8.EnableExpressionEditor = False
        GridViewTextBoxColumn8.FieldName = "CreateBy"
        GridViewTextBoxColumn8.HeaderText = "CreateBy"
        GridViewTextBoxColumn8.IsAutoGenerated = True
        GridViewTextBoxColumn8.IsVisible = False
        GridViewTextBoxColumn8.Name = "CreateBy"
        GridViewDateTimeColumn1.EnableExpressionEditor = False
        GridViewDateTimeColumn1.FieldName = "CreateDate"
        GridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn1.HeaderText = "CreateDate"
        GridViewDateTimeColumn1.IsAutoGenerated = True
        GridViewDateTimeColumn1.IsVisible = False
        GridViewDateTimeColumn1.Name = "CreateDate"
        GridViewTextBoxColumn9.EnableExpressionEditor = False
        GridViewTextBoxColumn9.FieldName = "LastUpdateBy"
        GridViewTextBoxColumn9.HeaderText = "LastUpdateBy"
        GridViewTextBoxColumn9.IsAutoGenerated = True
        GridViewTextBoxColumn9.IsVisible = False
        GridViewTextBoxColumn9.Name = "LastUpdateBy"
        GridViewDateTimeColumn2.EnableExpressionEditor = False
        GridViewDateTimeColumn2.FieldName = "LastUpdateDate"
        GridViewDateTimeColumn2.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn2.HeaderText = "LastUpdateDate"
        GridViewDateTimeColumn2.IsAutoGenerated = True
        GridViewDateTimeColumn2.IsVisible = False
        GridViewDateTimeColumn2.Name = "LastUpdateDate"
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewDecimalColumn1, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewDateTimeColumn1, GridViewTextBoxColumn9, GridViewDateTimeColumn2})
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.DataSource = Me.TLOBindingSource
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.EnableGrouping = False
        Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate.ShowFilteringRow = False
        Me.RadMultiColumnComboBox3.EditorControl.Name = "NestedRadGridView"
        Me.RadMultiColumnComboBox3.EditorControl.ReadOnly = True
        Me.RadMultiColumnComboBox3.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadMultiColumnComboBox3.EditorControl.ShowGroupPanel = False
        Me.RadMultiColumnComboBox3.EditorControl.Size = New System.Drawing.Size(240, 150)
        Me.RadMultiColumnComboBox3.EditorControl.TabIndex = 2
        Me.RadMultiColumnComboBox3.Location = New System.Drawing.Point(113, 88)
        Me.RadMultiColumnComboBox3.Name = "RadMultiColumnComboBox3"
        Me.RadMultiColumnComboBox3.NullText = " "
        Me.RadMultiColumnComboBox3.Size = New System.Drawing.Size(150, 21)
        Me.RadMultiColumnComboBox3.TabIndex = 4
        Me.RadMultiColumnComboBox3.TabStop = False
        Me.RadMultiColumnComboBox3.ThemeName = "Office2010Blue"
        Me.RadMultiColumnComboBox3.ValueMember = "LOAD_LOCATION"
        '
        'TLOBindingSource
        '
        Me.TLOBindingSource.DataMember = "T_LO"
        Me.TLOBindingSource.DataSource = Me.FPTDataSet
        '
        'RadMultiColumnComboBox2
        '
        Me.RadMultiColumnComboBox2.AutoSizeDropDownHeight = True
        Me.RadMultiColumnComboBox2.AutoSizeDropDownToBestFit = True
        Me.RadMultiColumnComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGPLANTBindingSource, "PLANT", True))
        Me.RadMultiColumnComboBox2.DataSource = Me.TSTO1BindingSource
        Me.RadMultiColumnComboBox2.DblClickRotate = True
        Me.RadMultiColumnComboBox2.DisplayMember = "PLANT"
        '
        'RadMultiColumnComboBox2.NestedRadGridView
        '
        Me.RadMultiColumnComboBox2.EditorControl.BackColor = System.Drawing.SystemColors.Window
        Me.RadMultiColumnComboBox2.EditorControl.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadMultiColumnComboBox2.EditorControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.RadMultiColumnComboBox2.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadMultiColumnComboBox2.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.RadMultiColumnComboBox2.EditorControl.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.AllowAddNewRow = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.AllowCellContextMenu = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.AllowColumnChooser = False
        GridViewTextBoxColumn10.EnableExpressionEditor = False
        GridViewTextBoxColumn10.FieldName = "SALEORG"
        GridViewTextBoxColumn10.HeaderText = "SALEORG"
        GridViewTextBoxColumn10.IsAutoGenerated = True
        GridViewTextBoxColumn10.Name = "Sale ORG."
        GridViewTextBoxColumn10.Width = 75
        GridViewTextBoxColumn11.EnableExpressionEditor = False
        GridViewTextBoxColumn11.FieldName = "PLANT"
        GridViewTextBoxColumn11.HeaderText = "PLANT"
        GridViewTextBoxColumn11.IsAutoGenerated = True
        GridViewTextBoxColumn11.Name = "Plant"
        GridViewTextBoxColumn11.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending
        GridViewTextBoxColumn11.Width = 161
        GridViewDecimalColumn2.DataType = GetType(Integer)
        GridViewDecimalColumn2.EnableExpressionEditor = False
        GridViewDecimalColumn2.FieldName = "ID"
        GridViewDecimalColumn2.HeaderText = "ID"
        GridViewDecimalColumn2.IsAutoGenerated = True
        GridViewDecimalColumn2.IsVisible = False
        GridViewDecimalColumn2.Name = "ID"
        GridViewDecimalColumn2.ReadOnly = True
        GridViewTextBoxColumn12.EnableExpressionEditor = False
        GridViewTextBoxColumn12.FieldName = "COMPANY"
        GridViewTextBoxColumn12.HeaderText = "COMPANY"
        GridViewTextBoxColumn12.IsAutoGenerated = True
        GridViewTextBoxColumn12.IsVisible = False
        GridViewTextBoxColumn12.Name = "COMPANY"
        GridViewTextBoxColumn12.ReadOnly = True
        GridViewTextBoxColumn13.EnableExpressionEditor = False
        GridViewTextBoxColumn13.FieldName = "STORAGE_DES"
        GridViewTextBoxColumn13.HeaderText = "STORAGE_DES"
        GridViewTextBoxColumn13.IsAutoGenerated = True
        GridViewTextBoxColumn13.IsVisible = False
        GridViewTextBoxColumn13.Name = "STORAGE_DES"
        GridViewTextBoxColumn13.ReadOnly = True
        GridViewTextBoxColumn14.EnableExpressionEditor = False
        GridViewTextBoxColumn14.FieldName = "STORAGE_LOC"
        GridViewTextBoxColumn14.HeaderText = "STORAGE_LOC"
        GridViewTextBoxColumn14.IsAutoGenerated = True
        GridViewTextBoxColumn14.IsVisible = False
        GridViewTextBoxColumn14.Name = "STORAGE_LOC"
        GridViewTextBoxColumn14.ReadOnly = True
        GridViewTextBoxColumn15.EnableExpressionEditor = False
        GridViewTextBoxColumn15.FieldName = "CreateBy"
        GridViewTextBoxColumn15.HeaderText = "CreateBy"
        GridViewTextBoxColumn15.IsAutoGenerated = True
        GridViewTextBoxColumn15.IsVisible = False
        GridViewTextBoxColumn15.Name = "CreateBy"
        GridViewTextBoxColumn15.ReadOnly = True
        GridViewDateTimeColumn3.EnableExpressionEditor = False
        GridViewDateTimeColumn3.FieldName = "CreateDate"
        GridViewDateTimeColumn3.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn3.HeaderText = "CreateDate"
        GridViewDateTimeColumn3.IsAutoGenerated = True
        GridViewDateTimeColumn3.IsVisible = False
        GridViewDateTimeColumn3.Name = "CreateDate"
        GridViewDateTimeColumn3.ReadOnly = True
        GridViewTextBoxColumn16.EnableExpressionEditor = False
        GridViewTextBoxColumn16.FieldName = "LastUpdateBy"
        GridViewTextBoxColumn16.HeaderText = "LastUpdateBy"
        GridViewTextBoxColumn16.IsAutoGenerated = True
        GridViewTextBoxColumn16.IsVisible = False
        GridViewTextBoxColumn16.Name = "LastUpdateBy"
        GridViewTextBoxColumn16.ReadOnly = True
        GridViewDateTimeColumn4.EnableExpressionEditor = False
        GridViewDateTimeColumn4.FieldName = "LastUpdateDate"
        GridViewDateTimeColumn4.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn4.HeaderText = "LastUpdateDate"
        GridViewDateTimeColumn4.IsAutoGenerated = True
        GridViewDateTimeColumn4.IsVisible = False
        GridViewDateTimeColumn4.Name = "LastUpdateDate"
        GridViewDateTimeColumn4.ReadOnly = True
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewDecimalColumn2, GridViewTextBoxColumn12, GridViewTextBoxColumn13, GridViewTextBoxColumn14, GridViewTextBoxColumn15, GridViewDateTimeColumn3, GridViewTextBoxColumn16, GridViewDateTimeColumn4})
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.DataSource = Me.TSTO1BindingSource
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.EnableGrouping = False
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.ShowFilteringRow = False
        SortDescriptor1.PropertyName = "Plant"
        Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate.SortDescriptors.AddRange(New Telerik.WinControls.Data.SortDescriptor() {SortDescriptor1})
        Me.RadMultiColumnComboBox2.EditorControl.Name = "NestedRadGridView"
        Me.RadMultiColumnComboBox2.EditorControl.ReadOnly = True
        Me.RadMultiColumnComboBox2.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadMultiColumnComboBox2.EditorControl.ShowGroupPanel = False
        Me.RadMultiColumnComboBox2.EditorControl.Size = New System.Drawing.Size(240, 150)
        Me.RadMultiColumnComboBox2.EditorControl.TabIndex = 0
        Me.RadMultiColumnComboBox2.Location = New System.Drawing.Point(113, 60)
        Me.RadMultiColumnComboBox2.Name = "RadMultiColumnComboBox2"
        Me.RadMultiColumnComboBox2.NullText = " "
        Me.RadMultiColumnComboBox2.Size = New System.Drawing.Size(150, 21)
        Me.RadMultiColumnComboBox2.TabIndex = 3
        Me.RadMultiColumnComboBox2.TabStop = False
        Me.RadMultiColumnComboBox2.ThemeName = "Office2010Blue"
        Me.RadMultiColumnComboBox2.ValueMember = "PLANT"
        '
        'TSTO1BindingSource
        '
        Me.TSTO1BindingSource.DataMember = "T_STO1"
        Me.TSTO1BindingSource.DataSource = Me.FPTDataSet
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.RadLabel1.Location = New System.Drawing.Point(30, 34)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(77, 19)
        Me.RadLabel1.TabIndex = 1
        Me.RadLabel1.Text = "Sales ORG :"
        Me.RadLabel1.ThemeName = "Office2010Blue"
        '
        'RadMultiColumnComboBox1
        '
        Me.RadMultiColumnComboBox1.AutoSizeDropDownHeight = True
        Me.RadMultiColumnComboBox1.AutoSizeDropDownToBestFit = True
        Me.RadMultiColumnComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TSETTINGPLANTBindingSource, "SALESORG", True))
        Me.RadMultiColumnComboBox1.DataSource = Me.TSHIPPERBindingSource
        Me.RadMultiColumnComboBox1.DblClickRotate = True
        Me.RadMultiColumnComboBox1.DisplayMember = "SP_Code"
        '
        'RadMultiColumnComboBox1.NestedRadGridView
        '
        Me.RadMultiColumnComboBox1.EditorControl.BackColor = System.Drawing.SystemColors.Window
        Me.RadMultiColumnComboBox1.EditorControl.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadMultiColumnComboBox1.EditorControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.RadMultiColumnComboBox1.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadMultiColumnComboBox1.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.RadMultiColumnComboBox1.EditorControl.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.AllowAddNewRow = False
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.AllowCellContextMenu = False
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.AllowColumnChooser = False
        GridViewDecimalColumn3.DataType = GetType(Integer)
        GridViewDecimalColumn3.EnableExpressionEditor = False
        GridViewDecimalColumn3.FieldName = "ID"
        GridViewDecimalColumn3.HeaderText = "ID"
        GridViewDecimalColumn3.IsAutoGenerated = True
        GridViewDecimalColumn3.IsVisible = False
        GridViewDecimalColumn3.Name = "ID"
        GridViewDecimalColumn3.ReadOnly = True
        GridViewDateTimeColumn5.EnableExpressionEditor = False
        GridViewDateTimeColumn5.FieldName = "SP_Date"
        GridViewDateTimeColumn5.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn5.HeaderText = "SP_Date"
        GridViewDateTimeColumn5.IsAutoGenerated = True
        GridViewDateTimeColumn5.IsVisible = False
        GridViewDateTimeColumn5.Name = "SP_Date"
        GridViewTextBoxColumn17.EnableExpressionEditor = False
        GridViewTextBoxColumn17.FieldName = "SP_Code"
        GridViewTextBoxColumn17.HeaderText = "SP_Code"
        GridViewTextBoxColumn17.IsAutoGenerated = True
        GridViewTextBoxColumn17.Name = "Code"
        GridViewTextBoxColumn17.Width = 98
        GridViewTextBoxColumn18.EnableExpressionEditor = False
        GridViewTextBoxColumn18.FieldName = "SP_NameTH"
        GridViewTextBoxColumn18.HeaderText = "SP_NameTH"
        GridViewTextBoxColumn18.IsAutoGenerated = True
        GridViewTextBoxColumn18.IsVisible = False
        GridViewTextBoxColumn18.Name = "SP_NameTH"
        GridViewTextBoxColumn19.EnableExpressionEditor = False
        GridViewTextBoxColumn19.FieldName = "SP_AddressTH"
        GridViewTextBoxColumn19.HeaderText = "SP_AddressTH"
        GridViewTextBoxColumn19.IsAutoGenerated = True
        GridViewTextBoxColumn19.IsVisible = False
        GridViewTextBoxColumn19.Name = "SP_AddressTH"
        GridViewTextBoxColumn20.EnableExpressionEditor = False
        GridViewTextBoxColumn20.FieldName = "SP_AmphurTH"
        GridViewTextBoxColumn20.HeaderText = "SP_AmphurTH"
        GridViewTextBoxColumn20.IsAutoGenerated = True
        GridViewTextBoxColumn20.IsVisible = False
        GridViewTextBoxColumn20.Name = "SP_AmphurTH"
        GridViewTextBoxColumn21.EnableExpressionEditor = False
        GridViewTextBoxColumn21.FieldName = "SP_ProvinceTH"
        GridViewTextBoxColumn21.HeaderText = "SP_ProvinceTH"
        GridViewTextBoxColumn21.IsAutoGenerated = True
        GridViewTextBoxColumn21.IsVisible = False
        GridViewTextBoxColumn21.Name = "SP_ProvinceTH"
        GridViewTextBoxColumn22.EnableExpressionEditor = False
        GridViewTextBoxColumn22.FieldName = "SP_NameEN"
        GridViewTextBoxColumn22.HeaderText = "SP_NameEN"
        GridViewTextBoxColumn22.IsAutoGenerated = True
        GridViewTextBoxColumn22.Name = "Name"
        GridViewTextBoxColumn22.Width = 243
        GridViewTextBoxColumn23.EnableExpressionEditor = False
        GridViewTextBoxColumn23.FieldName = "SP_AddressEN"
        GridViewTextBoxColumn23.HeaderText = "SP_AddressEN"
        GridViewTextBoxColumn23.IsAutoGenerated = True
        GridViewTextBoxColumn23.IsVisible = False
        GridViewTextBoxColumn23.Name = "SP_AddressEN"
        GridViewTextBoxColumn24.EnableExpressionEditor = False
        GridViewTextBoxColumn24.FieldName = "SP_AmphurEN"
        GridViewTextBoxColumn24.HeaderText = "SP_AmphurEN"
        GridViewTextBoxColumn24.IsAutoGenerated = True
        GridViewTextBoxColumn24.IsVisible = False
        GridViewTextBoxColumn24.Name = "SP_AmphurEN"
        GridViewTextBoxColumn25.EnableExpressionEditor = False
        GridViewTextBoxColumn25.FieldName = "SP_ProvinceEN"
        GridViewTextBoxColumn25.HeaderText = "SP_ProvinceEN"
        GridViewTextBoxColumn25.IsAutoGenerated = True
        GridViewTextBoxColumn25.IsVisible = False
        GridViewTextBoxColumn25.Name = "SP_ProvinceEN"
        GridViewDecimalColumn4.DataType = GetType(Integer)
        GridViewDecimalColumn4.EnableExpressionEditor = False
        GridViewDecimalColumn4.FieldName = "SP_ZIPCODE"
        GridViewDecimalColumn4.HeaderText = "SP_ZIPCODE"
        GridViewDecimalColumn4.IsAutoGenerated = True
        GridViewDecimalColumn4.IsVisible = False
        GridViewDecimalColumn4.Name = "SP_ZIPCODE"
        GridViewTextBoxColumn26.EnableExpressionEditor = False
        GridViewTextBoxColumn26.FieldName = "SP_Contact"
        GridViewTextBoxColumn26.HeaderText = "SP_Contact"
        GridViewTextBoxColumn26.IsAutoGenerated = True
        GridViewTextBoxColumn26.IsVisible = False
        GridViewTextBoxColumn26.Name = "SP_Contact"
        GridViewTextBoxColumn27.EnableExpressionEditor = False
        GridViewTextBoxColumn27.FieldName = "SP_Tel"
        GridViewTextBoxColumn27.HeaderText = "SP_Tel"
        GridViewTextBoxColumn27.IsAutoGenerated = True
        GridViewTextBoxColumn27.IsVisible = False
        GridViewTextBoxColumn27.Name = "SP_Tel"
        GridViewTextBoxColumn28.EnableExpressionEditor = False
        GridViewTextBoxColumn28.FieldName = "SP_Fax"
        GridViewTextBoxColumn28.HeaderText = "SP_Fax"
        GridViewTextBoxColumn28.IsAutoGenerated = True
        GridViewTextBoxColumn28.IsVisible = False
        GridViewTextBoxColumn28.Name = "SP_Fax"
        GridViewTextBoxColumn29.EnableExpressionEditor = False
        GridViewTextBoxColumn29.FieldName = "SP_Email"
        GridViewTextBoxColumn29.HeaderText = "SP_Email"
        GridViewTextBoxColumn29.IsAutoGenerated = True
        GridViewTextBoxColumn29.IsVisible = False
        GridViewTextBoxColumn29.Name = "SP_Email"
        GridViewTextBoxColumn30.EnableExpressionEditor = False
        GridViewTextBoxColumn30.FieldName = "SP_Type"
        GridViewTextBoxColumn30.HeaderText = "SP_Type"
        GridViewTextBoxColumn30.IsAutoGenerated = True
        GridViewTextBoxColumn30.IsVisible = False
        GridViewTextBoxColumn30.Name = "SP_Type"
        GridViewTextBoxColumn31.EnableExpressionEditor = False
        GridViewTextBoxColumn31.FieldName = "SP_Pro1"
        GridViewTextBoxColumn31.HeaderText = "SP_Pro1"
        GridViewTextBoxColumn31.IsAutoGenerated = True
        GridViewTextBoxColumn31.IsVisible = False
        GridViewTextBoxColumn31.Name = "SP_Pro1"
        GridViewTextBoxColumn32.EnableExpressionEditor = False
        GridViewTextBoxColumn32.FieldName = "SP_Pro2"
        GridViewTextBoxColumn32.HeaderText = "SP_Pro2"
        GridViewTextBoxColumn32.IsAutoGenerated = True
        GridViewTextBoxColumn32.IsVisible = False
        GridViewTextBoxColumn32.Name = "SP_Pro2"
        GridViewTextBoxColumn33.EnableExpressionEditor = False
        GridViewTextBoxColumn33.FieldName = "SP_Pro3"
        GridViewTextBoxColumn33.HeaderText = "SP_Pro3"
        GridViewTextBoxColumn33.IsAutoGenerated = True
        GridViewTextBoxColumn33.IsVisible = False
        GridViewTextBoxColumn33.Name = "SP_Pro3"
        GridViewTextBoxColumn34.EnableExpressionEditor = False
        GridViewTextBoxColumn34.FieldName = "SP_Pro4"
        GridViewTextBoxColumn34.HeaderText = "SP_Pro4"
        GridViewTextBoxColumn34.IsAutoGenerated = True
        GridViewTextBoxColumn34.IsVisible = False
        GridViewTextBoxColumn34.Name = "SP_Pro4"
        GridViewTextBoxColumn35.EnableExpressionEditor = False
        GridViewTextBoxColumn35.FieldName = "SP_Pro5"
        GridViewTextBoxColumn35.HeaderText = "SP_Pro5"
        GridViewTextBoxColumn35.IsAutoGenerated = True
        GridViewTextBoxColumn35.IsVisible = False
        GridViewTextBoxColumn35.Name = "SP_Pro5"
        GridViewTextBoxColumn36.EnableExpressionEditor = False
        GridViewTextBoxColumn36.FieldName = "SP_Pro6"
        GridViewTextBoxColumn36.HeaderText = "SP_Pro6"
        GridViewTextBoxColumn36.IsAutoGenerated = True
        GridViewTextBoxColumn36.IsVisible = False
        GridViewTextBoxColumn36.Name = "SP_Pro6"
        GridViewDateTimeColumn6.EnableExpressionEditor = False
        GridViewDateTimeColumn6.FieldName = "Update_date"
        GridViewDateTimeColumn6.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn6.HeaderText = "Update_date"
        GridViewDateTimeColumn6.IsAutoGenerated = True
        GridViewDateTimeColumn6.IsVisible = False
        GridViewDateTimeColumn6.Name = "Update_date"
        GridViewTextBoxColumn37.EnableExpressionEditor = False
        GridViewTextBoxColumn37.FieldName = "SP_Website"
        GridViewTextBoxColumn37.HeaderText = "SP_Website"
        GridViewTextBoxColumn37.IsAutoGenerated = True
        GridViewTextBoxColumn37.IsVisible = False
        GridViewTextBoxColumn37.Name = "SP_Website"
        GridViewDecimalColumn5.DataType = GetType(Integer)
        GridViewDecimalColumn5.EnableExpressionEditor = False
        GridViewDecimalColumn5.FieldName = "SP_SAPCODE"
        GridViewDecimalColumn5.HeaderText = "SP_SAPCODE"
        GridViewDecimalColumn5.IsAutoGenerated = True
        GridViewDecimalColumn5.IsVisible = False
        GridViewDecimalColumn5.Name = "SP_SAPCODE"
        GridViewTextBoxColumn38.EnableExpressionEditor = False
        GridViewTextBoxColumn38.FieldName = "LOAD_Advisenote"
        GridViewTextBoxColumn38.HeaderText = "LOAD_Advisenote"
        GridViewTextBoxColumn38.IsAutoGenerated = True
        GridViewTextBoxColumn38.IsVisible = False
        GridViewTextBoxColumn38.Name = "LOAD_Advisenote"
        GridViewTextBoxColumn39.EnableExpressionEditor = False
        GridViewTextBoxColumn39.FieldName = "LOAD_BOL"
        GridViewTextBoxColumn39.HeaderText = "LOAD_BOL"
        GridViewTextBoxColumn39.IsAutoGenerated = True
        GridViewTextBoxColumn39.IsVisible = False
        GridViewTextBoxColumn39.Name = "LOAD_BOL"
        GridViewTextBoxColumn40.EnableExpressionEditor = False
        GridViewTextBoxColumn40.FieldName = "LOAD_WeightTicket"
        GridViewTextBoxColumn40.HeaderText = "LOAD_WeightTicket"
        GridViewTextBoxColumn40.IsAutoGenerated = True
        GridViewTextBoxColumn40.IsVisible = False
        GridViewTextBoxColumn40.Name = "LOAD_WeightTicket"
        GridViewTextBoxColumn41.EnableExpressionEditor = False
        GridViewTextBoxColumn41.FieldName = "UNLOAD_Advisenote"
        GridViewTextBoxColumn41.HeaderText = "UNLOAD_Advisenote"
        GridViewTextBoxColumn41.IsAutoGenerated = True
        GridViewTextBoxColumn41.IsVisible = False
        GridViewTextBoxColumn41.Name = "UNLOAD_Advisenote"
        GridViewTextBoxColumn42.EnableExpressionEditor = False
        GridViewTextBoxColumn42.FieldName = "UNLOAD_BOL"
        GridViewTextBoxColumn42.HeaderText = "UNLOAD_BOL"
        GridViewTextBoxColumn42.IsAutoGenerated = True
        GridViewTextBoxColumn42.IsVisible = False
        GridViewTextBoxColumn42.Name = "UNLOAD_BOL"
        GridViewTextBoxColumn43.EnableExpressionEditor = False
        GridViewTextBoxColumn43.FieldName = "UNLOAD_Workoder"
        GridViewTextBoxColumn43.HeaderText = "UNLOAD_Workoder"
        GridViewTextBoxColumn43.IsAutoGenerated = True
        GridViewTextBoxColumn43.IsVisible = False
        GridViewTextBoxColumn43.Name = "UNLOAD_Workoder"
        GridViewTextBoxColumn44.EnableExpressionEditor = False
        GridViewTextBoxColumn44.FieldName = "UNLOAD_WeightTicket"
        GridViewTextBoxColumn44.HeaderText = "UNLOAD_WeightTicket"
        GridViewTextBoxColumn44.IsAutoGenerated = True
        GridViewTextBoxColumn44.IsVisible = False
        GridViewTextBoxColumn44.Name = "UNLOAD_WeightTicket"
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewDecimalColumn3, GridViewDateTimeColumn5, GridViewTextBoxColumn17, GridViewTextBoxColumn18, GridViewTextBoxColumn19, GridViewTextBoxColumn20, GridViewTextBoxColumn21, GridViewTextBoxColumn22, GridViewTextBoxColumn23, GridViewTextBoxColumn24, GridViewTextBoxColumn25, GridViewDecimalColumn4, GridViewTextBoxColumn26, GridViewTextBoxColumn27, GridViewTextBoxColumn28, GridViewTextBoxColumn29, GridViewTextBoxColumn30, GridViewTextBoxColumn31, GridViewTextBoxColumn32, GridViewTextBoxColumn33, GridViewTextBoxColumn34, GridViewTextBoxColumn35, GridViewTextBoxColumn36, GridViewDateTimeColumn6, GridViewTextBoxColumn37, GridViewDecimalColumn5, GridViewTextBoxColumn38, GridViewTextBoxColumn39, GridViewTextBoxColumn40, GridViewTextBoxColumn41, GridViewTextBoxColumn42, GridViewTextBoxColumn43, GridViewTextBoxColumn44})
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.DataSource = Me.TSHIPPERBindingSource
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.EnableGrouping = False
        Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate.ShowFilteringRow = False
        Me.RadMultiColumnComboBox1.EditorControl.Name = "NestedRadGridView"
        Me.RadMultiColumnComboBox1.EditorControl.ReadOnly = True
        Me.RadMultiColumnComboBox1.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadMultiColumnComboBox1.EditorControl.ShowGroupPanel = False
        Me.RadMultiColumnComboBox1.EditorControl.Size = New System.Drawing.Size(240, 150)
        Me.RadMultiColumnComboBox1.EditorControl.TabIndex = 0
        Me.RadMultiColumnComboBox1.Location = New System.Drawing.Point(113, 32)
        Me.RadMultiColumnComboBox1.Name = "RadMultiColumnComboBox1"
        Me.RadMultiColumnComboBox1.NullText = " "
        Me.RadMultiColumnComboBox1.Size = New System.Drawing.Size(150, 21)
        Me.RadMultiColumnComboBox1.TabIndex = 1
        Me.RadMultiColumnComboBox1.TabStop = False
        Me.RadMultiColumnComboBox1.ThemeName = "Office2010Blue"
        Me.RadMultiColumnComboBox1.ValueMember = "SP_Code"
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'TSETTINGPLANTTableAdapter
        '
        Me.TSETTINGPLANTTableAdapter.ClearBeforeFill = True
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'T_STO1TableAdapter
        '
        Me.T_STO1TableAdapter.ClearBeforeFill = True
        '
        'T_LOTableAdapter
        '
        Me.T_LOTableAdapter.ClearBeforeFill = True
        '
        'f04_01_SettingPlant
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(884, 392)
        Me.Controls.Add(Me.RadGridView1)
        Me.Controls.Add(Me.GDETAIL)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "f04_01_SettingPlant"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "10"
        Me.Text = "SettingPlant"
        Me.ThemeName = "Office2010Blue"
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.TSETTINGPLANTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GDETAIL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GDETAIL.ResumeLayout(False)
        Me.GDETAIL.PerformLayout()
        CType(Me.RadTextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadDateTimePicker1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox3.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox3.EditorControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TLOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox2.EditorControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSTO1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox1.EditorControl.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox1.EditorControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadMultiColumnComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents RadGridView1 As Telerik.WinControls.UI.RadGridView
    Friend WithEvents GDETAIL As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TSETTINGPLANTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TSETTINGPLANTTableAdapter As TAS_TOL.FPTDataSetTableAdapters.TSETTINGPLANTTableAdapter
    Friend WithEvents RadTextBox1 As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel5 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadDateTimePicker1 As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadMultiColumnComboBox3 As Telerik.WinControls.UI.RadMultiColumnComboBox
    Friend WithEvents RadMultiColumnComboBox2 As Telerik.WinControls.UI.RadMultiColumnComboBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadMultiColumnComboBox1 As Telerik.WinControls.UI.RadMultiColumnComboBox
    Friend WithEvents RadButton2 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton1 As Telerik.WinControls.UI.RadButton
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents TSTO1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_STO1TableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_STO1TableAdapter
    Friend WithEvents TLOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_LOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_LOTableAdapter
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents RadLabel6 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents RadTextBox2 As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel7 As Telerik.WinControls.UI.RadLabel
End Class

