﻿Imports TAS_TOL.C01_Permission
Public Class f04_01_SettingPlant

    Private Sub SettingPlant_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
        Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
        'TODO: This line of code loads data into the 'FPTDataSet.T_STO1' table. You can move, or remove it, as needed.
        Me.T_STO1TableAdapter.Fill(Me.FPTDataSet.T_STO1)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'TODO: This line of code loads data into the 'FPTDataSet.TSETTINGPLANT' table. You can move, or remove it, as needed.
        Me.TSETTINGPLANTTableAdapter.Fill(Me.FPTDataSet.TSETTINGPLANT)
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Sub RadButton2_Click(sender As System.Object, e As System.EventArgs) Handles RadButton2.Click
        TSETTINGPLANTBindingSource.CancelEdit()
        FPTDataSet.TSETTINGPLANT.RejectChanges()
        GDETAIL.Enabled = False
        RadGridView1.Enabled = True
        Close()
    End Sub

    Private Sub RadButton1_Click(sender As System.Object, e As System.EventArgs) Handles RadButton1.Click
        Try


            TSETTINGPLANTBindingSource(TSETTINGPLANTBindingSource.Position)("UPdateDate") = Now
            TSETTINGPLANTBindingSource(TSETTINGPLANTBindingSource.Position)("UPdateBy") = Main.U_NAME
            TSETTINGPLANTBindingSource.EndEdit()
            TSETTINGPLANTTableAdapter.Update(FPTDataSet.TSETTINGPLANT)
            GDETAIL.Enabled = False
            RadGridView1.Enabled = True
            '  Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub TSHIPPERBindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSHIPPERBindingSource.PositionChanged
        Try
            TSTO1BindingSource.Filter = "SALEORG='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "'"

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TSTO1BindingSource_PositionChanged(sender As System.Object, e As System.EventArgs) Handles TSTO1BindingSource.PositionChanged
        Try
            TLOBindingSource.Filter = "PLANT_LOCATION='" & TSTO1BindingSource(TSTO1BindingSource.Position)("PLANT").ToString & "'"

        Catch ex As Exception

        End Try

    End Sub

    Private Sub Btadd_MouseUp(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp, BtEdit.MouseUp, BtDelete.MouseUp
        GDETAIL.Enabled = True
        TSETTINGPLANTBindingSource(TSETTINGPLANTBindingSource.Position)("LDATE") = Now
        RadGridView1.DataSource = TSETTINGPLANTBindingSource
        RadGridView1.Enabled = False
    End Sub

    Private Sub Btadd_Click(sender As System.Object, e As System.EventArgs) Handles Btadd.Click
        RadGridView1.DataSource = ""

    End Sub
End Class
