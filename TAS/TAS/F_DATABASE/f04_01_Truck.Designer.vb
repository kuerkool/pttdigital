﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_Truck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_Truck))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.VE_UPDATE = New System.Windows.Forms.TextBox()
        Me.TTRUCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.VE_CAERDNUM = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.VE_CAPA = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.VE_DNAME = New System.Windows.Forms.ComboBox()
        Me.TDRIVERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.VE_NUM = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VE_CRDATE = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRUCKNUMBERDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRUCKCOMPNUMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRUCKCAPASITYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRUCKTYPEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRUCK_BLACK_LIST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.VE_FILLLAST = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.VE_EXPIREDATE = New System.Windows.Forms.DateTimePicker()
        Me.VE_ISSUEDATE = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.VE_BACKYES = New System.Windows.Forms.RadioButton()
        Me.VE_BACKNO = New System.Windows.Forms.RadioButton()
        Me.VE_SP = New System.Windows.Forms.ComboBox()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VE_TRUCKTYPE = New System.Windows.Forms.ComboBox()
        Me.TTRUCKTYPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VE_TRAN = New System.Windows.Forms.ComboBox()
        Me.TCOMPANYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GCompartment = New System.Windows.Forms.GroupBox()
        Me.DataComp = New System.Windows.Forms.DataGridView()
        Me.TTRUCKCOMPNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TTRUCKCOMPCAPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TTRUCKCOMPARTMENTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.VE_ARMBOTTOM = New System.Windows.Forms.RadioButton()
        Me.VE_ARMTOP = New System.Windows.Forms.RadioButton()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.VE_CHR = New System.Windows.Forms.TextBox()
        Me.VE_TRA = New System.Windows.Forms.TextBox()
        Me.VE_COMNUM = New System.Windows.Forms.TextBox()
        Me.T_TRUCKTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTableAdapter()
        Me.T_DRIVERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter()
        Me.T_COMPANYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.T_TRUCKCOMPARTMENTTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_TRUCKCOMPARTMENTTableAdapter()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BtSearch = New System.Windows.Forms.ToolStripButton()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.T_TRUCKTYPETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTYPETableAdapter()
        CType(Me.TTRUCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DetailGroup.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TTRUCKTYPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GCompartment.SuspendLayout()
        CType(Me.DataComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TTRUCKCOMPARTMENTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'VE_UPDATE
        '
        Me.VE_UPDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        resources.ApplyResources(Me.VE_UPDATE, "VE_UPDATE")
        Me.VE_UPDATE.Name = "VE_UPDATE"
        '
        'TTRUCKBindingSource
        '
        Me.TTRUCKBindingSource.DataMember = "T_TRUCK"
        Me.TTRUCKBindingSource.DataSource = Me.FPTDataSet
        Me.TTRUCKBindingSource.Sort = "ID"
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'VE_CAERDNUM
        '
        Me.VE_CAERDNUM.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_CARD", True))
        resources.ApplyResources(Me.VE_CAERDNUM, "VE_CAERDNUM")
        Me.VE_CAERDNUM.Name = "VE_CAERDNUM"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'VE_CAPA
        '
        Me.VE_CAPA.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_CAPASITY", True))
        resources.ApplyResources(Me.VE_CAPA, "VE_CAPA")
        Me.VE_CAPA.Name = "VE_CAPA"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'Bcancel
        '
        Me.Bcancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me.Bcancel, "Bcancel")
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Tag = "4"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        resources.ApplyResources(Me.Bsave, "Bsave")
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Tag = "4"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'VE_DNAME
        '
        Me.VE_DNAME.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.VE_DNAME.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.VE_DNAME.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TTRUCKBindingSource, "TRUCK_DRIVER", True))
        Me.VE_DNAME.DataSource = Me.TDRIVERBindingSource
        Me.VE_DNAME.DisplayMember = "Driver_NAME"
        Me.VE_DNAME.FormattingEnabled = True
        resources.ApplyResources(Me.VE_DNAME, "VE_DNAME")
        Me.VE_DNAME.Name = "VE_DNAME"
        Me.VE_DNAME.ValueMember = "ID"
        '
        'TDRIVERBindingSource
        '
        Me.TDRIVERBindingSource.DataMember = "T_DRIVER"
        Me.TDRIVERBindingSource.DataSource = Me.FPTDataSet
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'VE_NUM
        '
        Me.VE_NUM.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_NUMBER", True))
        resources.ApplyResources(Me.VE_NUM, "VE_NUM")
        Me.VE_NUM.Name = "VE_NUM"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'VE_CRDATE
        '
        Me.VE_CRDATE.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_DATE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        resources.ApplyResources(Me.VE_CRDATE, "VE_CRDATE")
        Me.VE_CRDATE.Name = "VE_CRDATE"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'MeterGrid
        '
        Me.MeterGrid.AllowUserToAddRows = False
        Me.MeterGrid.AllowUserToDeleteRows = False
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.TRUCKNUMBERDataGridViewTextBoxColumn, Me.TRUCKCOMPNUMDataGridViewTextBoxColumn, Me.TRUCKCAPASITYDataGridViewTextBoxColumn, Me.TRUCKTYPEDataGridViewTextBoxColumn, Me.TRUCK_BLACK_LIST})
        Me.MeterGrid.DataSource = Me.TTRUCKBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle2
        resources.ApplyResources(Me.MeterGrid, "MeterGrid")
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        resources.ApplyResources(Me.IDDataGridViewTextBoxColumn, "IDDataGridViewTextBoxColumn")
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TRUCKNUMBERDataGridViewTextBoxColumn
        '
        Me.TRUCKNUMBERDataGridViewTextBoxColumn.DataPropertyName = "TRUCK_NUMBER"
        resources.ApplyResources(Me.TRUCKNUMBERDataGridViewTextBoxColumn, "TRUCKNUMBERDataGridViewTextBoxColumn")
        Me.TRUCKNUMBERDataGridViewTextBoxColumn.Name = "TRUCKNUMBERDataGridViewTextBoxColumn"
        Me.TRUCKNUMBERDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TRUCKCOMPNUMDataGridViewTextBoxColumn
        '
        Me.TRUCKCOMPNUMDataGridViewTextBoxColumn.DataPropertyName = "TRUCK_COMP_NUM"
        resources.ApplyResources(Me.TRUCKCOMPNUMDataGridViewTextBoxColumn, "TRUCKCOMPNUMDataGridViewTextBoxColumn")
        Me.TRUCKCOMPNUMDataGridViewTextBoxColumn.Name = "TRUCKCOMPNUMDataGridViewTextBoxColumn"
        Me.TRUCKCOMPNUMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TRUCKCAPASITYDataGridViewTextBoxColumn
        '
        Me.TRUCKCAPASITYDataGridViewTextBoxColumn.DataPropertyName = "TRUCK_CAPASITY"
        resources.ApplyResources(Me.TRUCKCAPASITYDataGridViewTextBoxColumn, "TRUCKCAPASITYDataGridViewTextBoxColumn")
        Me.TRUCKCAPASITYDataGridViewTextBoxColumn.Name = "TRUCKCAPASITYDataGridViewTextBoxColumn"
        Me.TRUCKCAPASITYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TRUCKTYPEDataGridViewTextBoxColumn
        '
        Me.TRUCKTYPEDataGridViewTextBoxColumn.DataPropertyName = "TRUCK_TYPE"
        resources.ApplyResources(Me.TRUCKTYPEDataGridViewTextBoxColumn, "TRUCKTYPEDataGridViewTextBoxColumn")
        Me.TRUCKTYPEDataGridViewTextBoxColumn.Name = "TRUCKTYPEDataGridViewTextBoxColumn"
        Me.TRUCKTYPEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TRUCK_BLACK_LIST
        '
        Me.TRUCK_BLACK_LIST.DataPropertyName = "TRUCK_BLACK_LIST"
        resources.ApplyResources(Me.TRUCK_BLACK_LIST, "TRUCK_BLACK_LIST")
        Me.TRUCK_BLACK_LIST.Name = "TRUCK_BLACK_LIST"
        Me.TRUCK_BLACK_LIST.ReadOnly = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.Label24)
        Me.DetailGroup.Controls.Add(Me.Label22)
        Me.DetailGroup.Controls.Add(Me.Label20)
        Me.DetailGroup.Controls.Add(Me.Label25)
        Me.DetailGroup.Controls.Add(Me.VE_FILLLAST)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.Label9)
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.Label5)
        Me.DetailGroup.Controls.Add(Me.Label4)
        Me.DetailGroup.Controls.Add(Me.VE_EXPIREDATE)
        Me.DetailGroup.Controls.Add(Me.VE_ISSUEDATE)
        Me.DetailGroup.Controls.Add(Me.GroupBox5)
        Me.DetailGroup.Controls.Add(Me.VE_SP)
        Me.DetailGroup.Controls.Add(Me.VE_TRUCKTYPE)
        Me.DetailGroup.Controls.Add(Me.VE_TRAN)
        Me.DetailGroup.Controls.Add(Me.GCompartment)
        Me.DetailGroup.Controls.Add(Me.Label18)
        Me.DetailGroup.Controls.Add(Me.GroupBox4)
        Me.DetailGroup.Controls.Add(Me.Label16)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.Label14)
        Me.DetailGroup.Controls.Add(Me.VE_CHR)
        Me.DetailGroup.Controls.Add(Me.VE_TRA)
        Me.DetailGroup.Controls.Add(Me.VE_UPDATE)
        Me.DetailGroup.Controls.Add(Me.Label10)
        Me.DetailGroup.Controls.Add(Me.VE_CAERDNUM)
        Me.DetailGroup.Controls.Add(Me.Label8)
        Me.DetailGroup.Controls.Add(Me.Label7)
        Me.DetailGroup.Controls.Add(Me.Label17)
        Me.DetailGroup.Controls.Add(Me.Label19)
        Me.DetailGroup.Controls.Add(Me.VE_CAPA)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.VE_DNAME)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.VE_NUM)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Controls.Add(Me.VE_CRDATE)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.VE_COMNUM)
        resources.ApplyResources(Me.DetailGroup, "DetailGroup")
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.TabStop = False
        '
        'Label24
        '
        resources.ApplyResources(Me.Label24, "Label24")
        Me.Label24.ForeColor = System.Drawing.Color.Red
        Me.Label24.Name = "Label24"
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.ForeColor = System.Drawing.Color.Red
        Me.Label22.Name = "Label22"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Name = "Label20"
        '
        'Label25
        '
        resources.ApplyResources(Me.Label25, "Label25")
        Me.Label25.ForeColor = System.Drawing.Color.Red
        Me.Label25.Name = "Label25"
        '
        'VE_FILLLAST
        '
        resources.ApplyResources(Me.VE_FILLLAST, "VE_FILLLAST")
        Me.VE_FILLLAST.Name = "VE_FILLLAST"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'VE_EXPIREDATE
        '
        Me.VE_EXPIREDATE.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TTRUCKBindingSource, "TRUCK_MeasureNext", True))
        resources.ApplyResources(Me.VE_EXPIREDATE, "VE_EXPIREDATE")
        Me.VE_EXPIREDATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.VE_EXPIREDATE.Name = "VE_EXPIREDATE"
        '
        'VE_ISSUEDATE
        '
        Me.VE_ISSUEDATE.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.TTRUCKBindingSource, "TRUCK_MeasureLast", True))
        resources.ApplyResources(Me.VE_ISSUEDATE, "VE_ISSUEDATE")
        Me.VE_ISSUEDATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.VE_ISSUEDATE.Name = "VE_ISSUEDATE"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.VE_BACKYES)
        Me.GroupBox5.Controls.Add(Me.VE_BACKNO)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'VE_BACKYES
        '
        resources.ApplyResources(Me.VE_BACKYES, "VE_BACKYES")
        Me.VE_BACKYES.Name = "VE_BACKYES"
        Me.VE_BACKYES.UseVisualStyleBackColor = True
        '
        'VE_BACKNO
        '
        resources.ApplyResources(Me.VE_BACKNO, "VE_BACKNO")
        Me.VE_BACKNO.Checked = True
        Me.VE_BACKNO.Name = "VE_BACKNO"
        Me.VE_BACKNO.TabStop = True
        Me.VE_BACKNO.UseVisualStyleBackColor = True
        '
        'VE_SP
        '
        Me.VE_SP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.VE_SP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.VE_SP.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TTRUCKBindingSource, "TRUCK_SHIPPER", True))
        Me.VE_SP.DataSource = Me.TSHIPPERBindingSource
        Me.VE_SP.DisplayMember = "SP_Code"
        Me.VE_SP.FormattingEnabled = True
        resources.ApplyResources(Me.VE_SP, "VE_SP")
        Me.VE_SP.Name = "VE_SP"
        Me.VE_SP.ValueMember = "ID"
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'VE_TRUCKTYPE
        '
        Me.VE_TRUCKTYPE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.VE_TRUCKTYPE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.VE_TRUCKTYPE.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TTRUCKBindingSource, "TRUCK_TYPE", True))
        Me.VE_TRUCKTYPE.DataSource = Me.TTRUCKTYPEBindingSource
        Me.VE_TRUCKTYPE.DisplayMember = "TYPE"
        Me.VE_TRUCKTYPE.FormattingEnabled = True
        resources.ApplyResources(Me.VE_TRUCKTYPE, "VE_TRUCKTYPE")
        Me.VE_TRUCKTYPE.Name = "VE_TRUCKTYPE"
        Me.VE_TRUCKTYPE.ValueMember = "ID"
        '
        'TTRUCKTYPEBindingSource
        '
        Me.TTRUCKTYPEBindingSource.DataMember = "T_TRUCKTYPE"
        Me.TTRUCKTYPEBindingSource.DataSource = Me.FPTDataSet
        '
        'VE_TRAN
        '
        Me.VE_TRAN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.VE_TRAN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.VE_TRAN.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TTRUCKBindingSource, "TRUCK_COMPANY", True))
        Me.VE_TRAN.DataSource = Me.TCOMPANYBindingSource
        Me.VE_TRAN.DisplayMember = "COMPANY_NAME"
        Me.VE_TRAN.DropDownWidth = 300
        Me.VE_TRAN.FormattingEnabled = True
        resources.ApplyResources(Me.VE_TRAN, "VE_TRAN")
        Me.VE_TRAN.Name = "VE_TRAN"
        Me.VE_TRAN.ValueMember = "COMPANY_ID"
        '
        'TCOMPANYBindingSource
        '
        Me.TCOMPANYBindingSource.DataMember = "T_COMPANY"
        Me.TCOMPANYBindingSource.DataSource = Me.FPTDataSet
        '
        'GCompartment
        '
        Me.GCompartment.Controls.Add(Me.DataComp)
        resources.ApplyResources(Me.GCompartment, "GCompartment")
        Me.GCompartment.Name = "GCompartment"
        Me.GCompartment.TabStop = False
        '
        'DataComp
        '
        Me.DataComp.AllowUserToAddRows = False
        Me.DataComp.AutoGenerateColumns = False
        Me.DataComp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataComp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TTRUCKCOMPNODataGridViewTextBoxColumn, Me.TTRUCKCOMPCAPDataGridViewTextBoxColumn})
        Me.DataComp.DataSource = Me.TTRUCKCOMPARTMENTBindingSource
        Me.DataComp.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        resources.ApplyResources(Me.DataComp, "DataComp")
        Me.DataComp.Name = "DataComp"
        Me.DataComp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        '
        'TTRUCKCOMPNODataGridViewTextBoxColumn
        '
        Me.TTRUCKCOMPNODataGridViewTextBoxColumn.DataPropertyName = "T_TRUCKCOMPNO"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TTRUCKCOMPNODataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        resources.ApplyResources(Me.TTRUCKCOMPNODataGridViewTextBoxColumn, "TTRUCKCOMPNODataGridViewTextBoxColumn")
        Me.TTRUCKCOMPNODataGridViewTextBoxColumn.Name = "TTRUCKCOMPNODataGridViewTextBoxColumn"
        Me.TTRUCKCOMPNODataGridViewTextBoxColumn.ReadOnly = True
        '
        'TTRUCKCOMPCAPDataGridViewTextBoxColumn
        '
        Me.TTRUCKCOMPCAPDataGridViewTextBoxColumn.DataPropertyName = "T_TRUCKCOMPCAP"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TTRUCKCOMPCAPDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        resources.ApplyResources(Me.TTRUCKCOMPCAPDataGridViewTextBoxColumn, "TTRUCKCOMPCAPDataGridViewTextBoxColumn")
        Me.TTRUCKCOMPCAPDataGridViewTextBoxColumn.Name = "TTRUCKCOMPCAPDataGridViewTextBoxColumn"
        '
        'TTRUCKCOMPARTMENTBindingSource
        '
        Me.TTRUCKCOMPARTMENTBindingSource.DataMember = "T_TRUCKCOMPARTMENT"
        Me.TTRUCKCOMPARTMENTBindingSource.DataSource = Me.FPTDataSet
        Me.TTRUCKCOMPARTMENTBindingSource.Filter = ""
        Me.TTRUCKCOMPARTMENTBindingSource.Sort = "T_TRUCKCOMPNO"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.VE_ARMBOTTOM)
        Me.GroupBox4.Controls.Add(Me.VE_ARMTOP)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'VE_ARMBOTTOM
        '
        resources.ApplyResources(Me.VE_ARMBOTTOM, "VE_ARMBOTTOM")
        Me.VE_ARMBOTTOM.Name = "VE_ARMBOTTOM"
        Me.VE_ARMBOTTOM.UseVisualStyleBackColor = True
        '
        'VE_ARMTOP
        '
        resources.ApplyResources(Me.VE_ARMTOP, "VE_ARMTOP")
        Me.VE_ARMTOP.Checked = True
        Me.VE_ARMTOP.Name = "VE_ARMTOP"
        Me.VE_ARMTOP.TabStop = True
        Me.VE_ARMTOP.UseVisualStyleBackColor = True
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'VE_CHR
        '
        Me.VE_CHR.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_CHR", True))
        resources.ApplyResources(Me.VE_CHR, "VE_CHR")
        Me.VE_CHR.Name = "VE_CHR"
        '
        'VE_TRA
        '
        Me.VE_TRA.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_TRA", True))
        resources.ApplyResources(Me.VE_TRA, "VE_TRA")
        Me.VE_TRA.Name = "VE_TRA"
        '
        'VE_COMNUM
        '
        Me.VE_COMNUM.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TTRUCKBindingSource, "TRUCK_COMP_NUM", True))
        resources.ApplyResources(Me.VE_COMNUM, "VE_COMNUM")
        Me.VE_COMNUM.Name = "VE_COMNUM"
        '
        'T_TRUCKTableAdapter
        '
        Me.T_TRUCKTableAdapter.ClearBeforeFill = True
        '
        'T_DRIVERTableAdapter
        '
        Me.T_DRIVERTableAdapter.ClearBeforeFill = True
        '
        'T_COMPANYTableAdapter
        '
        Me.T_COMPANYTableAdapter.ClearBeforeFill = True
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'T_TRUCKCOMPARTMENTTableAdapter
        '
        Me.T_TRUCKCOMPARTMENTTableAdapter.ClearBeforeFill = True
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        resources.ApplyResources(Me.BindingNavigator1, "BindingNavigator1")
        Me.BindingNavigator1.BindingSource = Me.TTRUCKBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.BtSearch, Me.EDFillter})
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btadd, "Btadd")
        Me.Btadd.Name = "Btadd"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        resources.ApplyResources(Me.ToolStripLabel1, "ToolStripLabel1")
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.BtDelete, "BtDelete")
        Me.BtDelete.Name = "BtDelete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btfirst, "Btfirst")
        Me.Btfirst.Name = "Btfirst"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btprevious, "Btprevious")
        Me.Btprevious.Name = "Btprevious"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'ToolStripTextBox1
        '
        resources.ApplyResources(Me.ToolStripTextBox1, "ToolStripTextBox1")
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        resources.ApplyResources(Me.ToolStripSeparator2, "ToolStripSeparator2")
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btnext, "Btnext")
        Me.Btnext.Name = "Btnext"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.BtLast, "BtLast")
        Me.BtLast.Name = "BtLast"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        resources.ApplyResources(Me.ToolStripSeparator3, "ToolStripSeparator3")
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.BtEdit, "BtEdit")
        Me.BtEdit.Name = "BtEdit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        resources.ApplyResources(Me.toolStripSeparator, "toolStripSeparator")
        '
        'BtSearch
        '
        Me.BtSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        resources.ApplyResources(Me.BtSearch, "BtSearch")
        Me.BtSearch.Name = "BtSearch"
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        resources.ApplyResources(Me.EDFillter, "EDFillter")
        '
        'T_TRUCKTYPETableAdapter
        '
        Me.T_TRUCKTYPETableAdapter.ClearBeforeFill = True
        '
        'f04_01_Truck
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.Bcancel
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.MeterGrid)
        Me.Controls.Add(Me.DetailGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "f04_01_Truck"
        Me.Tag = "4"
        CType(Me.TTRUCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TTRUCKTYPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GCompartment.ResumeLayout(False)
        CType(Me.DataComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TTRUCKCOMPARTMENTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VE_UPDATE As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents VE_CAERDNUM As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents VE_CAPA As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents VE_DNAME As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents VE_NUM As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VE_CRDATE As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents VE_COMNUM As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents VE_CHR As System.Windows.Forms.TextBox
    Friend WithEvents VE_TRA As System.Windows.Forms.TextBox
    Friend WithEvents VE_TRAN As System.Windows.Forms.ComboBox
    Friend WithEvents GCompartment As System.Windows.Forms.GroupBox
    Friend WithEvents DataComp As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents VE_ARMBOTTOM As System.Windows.Forms.RadioButton
    Friend WithEvents VE_ARMTOP As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents VE_BACKYES As System.Windows.Forms.RadioButton
    Friend WithEvents VE_BACKNO As System.Windows.Forms.RadioButton
    Friend WithEvents VE_SP As System.Windows.Forms.ComboBox
    Friend WithEvents VE_EXPIREDATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents VE_ISSUEDATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents VE_FILLLAST As System.Windows.Forms.TextBox
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TTRUCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_TRUCKTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTableAdapter
    Friend WithEvents TDRIVERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_DRIVERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter
    Friend WithEvents TCOMPANYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_COMPANYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents TTRUCKCOMPARTMENTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_TRUCKCOMPARTMENTTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_TRUCKCOMPARTMENTTableAdapter
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRUCKNUMBERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRUCKCOMPNUMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRUCKCAPASITYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRUCKTYPEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRUCK_BLACK_LIST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TTRUCKCOMPNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TTRUCKCOMPCAPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BtSearch As System.Windows.Forms.ToolStripButton
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents VE_TRUCKTYPE As System.Windows.Forms.ComboBox
    Friend WithEvents TTRUCKTYPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_TRUCKTYPETableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTYPETableAdapter
End Class
