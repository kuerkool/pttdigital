﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Doitems
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Doitems))
        Me.TDOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DONODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DOITEMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SONODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SOITEMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DOCDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DOCTIMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SDDOCCATDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESORGDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCHANDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DIVISIONDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SHIPPOINTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DOCDATEDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PLANGIDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRNPLANDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELVDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PICKDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SHIPTOPARTYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SOLDTOPARTYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTALWEIGHTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MATCODEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PLANTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STORAGELOCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BATCHNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACTQTYDELVSUDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASEUOMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESUNITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NETWEIGHTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GROSSWEIGHTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WEIGHTUNITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VOLUMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VOLUMEUNITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OVRDELVTLIMITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UNDDELVTLIMITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sHIP_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sHIP_TYPE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tRN_PLAN_POINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cREATE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cREATE_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cONTAINER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dRIVER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cAR_LINCENSE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fORWD_AGENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wEIGHT_IN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wEIGHT_OUT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sEAL_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sHIPP_TYPE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sHIPP_COND = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sHIPPdO_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.aCT_LOAD_SDATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.aCT_LOAD_STIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.aCT_LOAD_EDATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.aCT_LOAD_ETIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T_DOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_DOTableAdapter()
        Me.GroupUnload = New System.Windows.Forms.GroupBox()
        Me.BtDelete = New System.Windows.Forms.Button()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.SelectDate = New System.Windows.Forms.DateTimePicker()
        CType(Me.TDOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupUnload.SuspendLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TDOBindingSource
        '
        Me.TDOBindingSource.DataMember = "T_DO"
        Me.TDOBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MeterGrid
        '
        Me.MeterGrid.AllowUserToAddRows = False
        Me.MeterGrid.AllowUserToDeleteRows = False
        Me.MeterGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.DONODataGridViewTextBoxColumn, Me.DOITEMDataGridViewTextBoxColumn, Me.SONODataGridViewTextBoxColumn, Me.SOITEMDataGridViewTextBoxColumn, Me.DOCDATEDataGridViewTextBoxColumn, Me.DOCTIMEDataGridViewTextBoxColumn, Me.SDDOCCATDataGridViewTextBoxColumn, Me.SALESORGDataGridViewTextBoxColumn, Me.DISCHANDataGridViewTextBoxColumn, Me.DIVISIONDataGridViewTextBoxColumn, Me.SHIPPOINTDataGridViewTextBoxColumn, Me.DOCDATEDataGridViewTextBoxColumn1, Me.PLANGIDATEDataGridViewTextBoxColumn, Me.LOADDATEDataGridViewTextBoxColumn, Me.TRNPLANDATEDataGridViewTextBoxColumn, Me.DELVDATEDataGridViewTextBoxColumn, Me.PICKDATEDataGridViewTextBoxColumn, Me.SHIPTOPARTYDataGridViewTextBoxColumn, Me.SOLDTOPARTYDataGridViewTextBoxColumn, Me.TOTALWEIGHTDataGridViewTextBoxColumn, Me.MATCODEDataGridViewTextBoxColumn, Me.PLANTDataGridViewTextBoxColumn, Me.STORAGELOCDataGridViewTextBoxColumn, Me.BATCHNODataGridViewTextBoxColumn, Me.ACTQTYDELVSUDataGridViewTextBoxColumn, Me.BASEUOMDataGridViewTextBoxColumn, Me.SALESUNITDataGridViewTextBoxColumn, Me.NETWEIGHTDataGridViewTextBoxColumn, Me.GROSSWEIGHTDataGridViewTextBoxColumn, Me.WEIGHTUNITDataGridViewTextBoxColumn, Me.VOLUMEDataGridViewTextBoxColumn, Me.VOLUMEUNITDataGridViewTextBoxColumn, Me.OVRDELVTLIMITDataGridViewTextBoxColumn, Me.UNDDELVTLIMITDataGridViewTextBoxColumn, Me.sHIP_NO, Me.sHIP_TYPE, Me.tRN_PLAN_POINT, Me.cREATE_DATE, Me.cREATE_TIME, Me.cONTAINER, Me.dRIVER_NAME, Me.cAR_LINCENSE, Me.fORWD_AGENT, Me.wEIGHT_IN, Me.wEIGHT_OUT, Me.sEAL_NO, Me.sHIPP_TYPE, Me.sHIPP_COND, Me.sHIPPdO_NO, Me.aCT_LOAD_SDATE, Me.aCT_LOAD_STIME, Me.aCT_LOAD_EDATE, Me.aCT_LOAD_ETIME})
        Me.MeterGrid.DataSource = Me.TDOBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MeterGrid.Location = New System.Drawing.Point(0, 46)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MeterGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(905, 376)
        Me.MeterGrid.TabIndex = 3
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DONODataGridViewTextBoxColumn
        '
        Me.DONODataGridViewTextBoxColumn.DataPropertyName = "dO_NO"
        Me.DONODataGridViewTextBoxColumn.HeaderText = "Delivery order No."
        Me.DONODataGridViewTextBoxColumn.Name = "DONODataGridViewTextBoxColumn"
        Me.DONODataGridViewTextBoxColumn.ReadOnly = True
        '
        'DOITEMDataGridViewTextBoxColumn
        '
        Me.DOITEMDataGridViewTextBoxColumn.DataPropertyName = "dO_ITEM"
        Me.DOITEMDataGridViewTextBoxColumn.HeaderText = "Delivery order Item"
        Me.DOITEMDataGridViewTextBoxColumn.Name = "DOITEMDataGridViewTextBoxColumn"
        Me.DOITEMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SONODataGridViewTextBoxColumn
        '
        Me.SONODataGridViewTextBoxColumn.DataPropertyName = "sO_NO"
        Me.SONODataGridViewTextBoxColumn.HeaderText = "Sales order No."
        Me.SONODataGridViewTextBoxColumn.Name = "SONODataGridViewTextBoxColumn"
        Me.SONODataGridViewTextBoxColumn.ReadOnly = True
        '
        'SOITEMDataGridViewTextBoxColumn
        '
        Me.SOITEMDataGridViewTextBoxColumn.DataPropertyName = "sO_ITEM"
        Me.SOITEMDataGridViewTextBoxColumn.HeaderText = "Sales order Item."
        Me.SOITEMDataGridViewTextBoxColumn.Name = "SOITEMDataGridViewTextBoxColumn"
        Me.SOITEMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DOCDATEDataGridViewTextBoxColumn
        '
        Me.DOCDATEDataGridViewTextBoxColumn.DataPropertyName = "dO_CDATE"
        Me.DOCDATEDataGridViewTextBoxColumn.HeaderText = "D/O Create Date"
        Me.DOCDATEDataGridViewTextBoxColumn.Name = "DOCDATEDataGridViewTextBoxColumn"
        Me.DOCDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DOCTIMEDataGridViewTextBoxColumn
        '
        Me.DOCTIMEDataGridViewTextBoxColumn.DataPropertyName = "dO_CTIME"
        Me.DOCTIMEDataGridViewTextBoxColumn.HeaderText = "D/O Create Time"
        Me.DOCTIMEDataGridViewTextBoxColumn.Name = "DOCTIMEDataGridViewTextBoxColumn"
        Me.DOCTIMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SDDOCCATDataGridViewTextBoxColumn
        '
        Me.SDDOCCATDataGridViewTextBoxColumn.DataPropertyName = "sD_DOC_CAT"
        Me.SDDOCCATDataGridViewTextBoxColumn.HeaderText = "SD document category"
        Me.SDDOCCATDataGridViewTextBoxColumn.Name = "SDDOCCATDataGridViewTextBoxColumn"
        Me.SDDOCCATDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SALESORGDataGridViewTextBoxColumn
        '
        Me.SALESORGDataGridViewTextBoxColumn.DataPropertyName = "sALES_ORG"
        Me.SALESORGDataGridViewTextBoxColumn.HeaderText = "Sales organization"
        Me.SALESORGDataGridViewTextBoxColumn.Name = "SALESORGDataGridViewTextBoxColumn"
        Me.SALESORGDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DISCHANDataGridViewTextBoxColumn
        '
        Me.DISCHANDataGridViewTextBoxColumn.DataPropertyName = "dIS_CHAN"
        Me.DISCHANDataGridViewTextBoxColumn.HeaderText = "Distribution channel"
        Me.DISCHANDataGridViewTextBoxColumn.Name = "DISCHANDataGridViewTextBoxColumn"
        Me.DISCHANDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DIVISIONDataGridViewTextBoxColumn
        '
        Me.DIVISIONDataGridViewTextBoxColumn.DataPropertyName = "dIVISION"
        Me.DIVISIONDataGridViewTextBoxColumn.HeaderText = "Division"
        Me.DIVISIONDataGridViewTextBoxColumn.Name = "DIVISIONDataGridViewTextBoxColumn"
        Me.DIVISIONDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SHIPPOINTDataGridViewTextBoxColumn
        '
        Me.SHIPPOINTDataGridViewTextBoxColumn.DataPropertyName = "sHIP_POINT"
        Me.SHIPPOINTDataGridViewTextBoxColumn.HeaderText = "Shipping Point"
        Me.SHIPPOINTDataGridViewTextBoxColumn.Name = "SHIPPOINTDataGridViewTextBoxColumn"
        Me.SHIPPOINTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DOCDATEDataGridViewTextBoxColumn1
        '
        Me.DOCDATEDataGridViewTextBoxColumn1.DataPropertyName = "dOC_DATE"
        Me.DOCDATEDataGridViewTextBoxColumn1.HeaderText = "Document date"
        Me.DOCDATEDataGridViewTextBoxColumn1.Name = "DOCDATEDataGridViewTextBoxColumn1"
        Me.DOCDATEDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'PLANGIDATEDataGridViewTextBoxColumn
        '
        Me.PLANGIDATEDataGridViewTextBoxColumn.DataPropertyName = "pLAN_GI_DATE"
        Me.PLANGIDATEDataGridViewTextBoxColumn.HeaderText = "Planned goods issue date"
        Me.PLANGIDATEDataGridViewTextBoxColumn.Name = "PLANGIDATEDataGridViewTextBoxColumn"
        Me.PLANGIDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADDATEDataGridViewTextBoxColumn
        '
        Me.LOADDATEDataGridViewTextBoxColumn.DataPropertyName = "lOAD_DATE"
        Me.LOADDATEDataGridViewTextBoxColumn.HeaderText = "Loading Date"
        Me.LOADDATEDataGridViewTextBoxColumn.Name = "LOADDATEDataGridViewTextBoxColumn"
        Me.LOADDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TRNPLANDATEDataGridViewTextBoxColumn
        '
        Me.TRNPLANDATEDataGridViewTextBoxColumn.DataPropertyName = "tRN_PLAN_DATE"
        Me.TRNPLANDATEDataGridViewTextBoxColumn.HeaderText = "Transportation Planning Date"
        Me.TRNPLANDATEDataGridViewTextBoxColumn.Name = "TRNPLANDATEDataGridViewTextBoxColumn"
        Me.TRNPLANDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DELVDATEDataGridViewTextBoxColumn
        '
        Me.DELVDATEDataGridViewTextBoxColumn.DataPropertyName = "dELV_DATE"
        Me.DELVDATEDataGridViewTextBoxColumn.HeaderText = "Delivery Date"
        Me.DELVDATEDataGridViewTextBoxColumn.Name = "DELVDATEDataGridViewTextBoxColumn"
        Me.DELVDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PICKDATEDataGridViewTextBoxColumn
        '
        Me.PICKDATEDataGridViewTextBoxColumn.DataPropertyName = "pICK_DATE"
        Me.PICKDATEDataGridViewTextBoxColumn.HeaderText = "Picking Date"
        Me.PICKDATEDataGridViewTextBoxColumn.Name = "PICKDATEDataGridViewTextBoxColumn"
        Me.PICKDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SHIPTOPARTYDataGridViewTextBoxColumn
        '
        Me.SHIPTOPARTYDataGridViewTextBoxColumn.DataPropertyName = "sHIP_TO_PARTY"
        Me.SHIPTOPARTYDataGridViewTextBoxColumn.HeaderText = "Ship-to-Party"
        Me.SHIPTOPARTYDataGridViewTextBoxColumn.Name = "SHIPTOPARTYDataGridViewTextBoxColumn"
        Me.SHIPTOPARTYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SOLDTOPARTYDataGridViewTextBoxColumn
        '
        Me.SOLDTOPARTYDataGridViewTextBoxColumn.DataPropertyName = "sOLD_TO_PARTY"
        Me.SOLDTOPARTYDataGridViewTextBoxColumn.HeaderText = "Sold-to-Party"
        Me.SOLDTOPARTYDataGridViewTextBoxColumn.Name = "SOLDTOPARTYDataGridViewTextBoxColumn"
        Me.SOLDTOPARTYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TOTALWEIGHTDataGridViewTextBoxColumn
        '
        Me.TOTALWEIGHTDataGridViewTextBoxColumn.DataPropertyName = "tOTAL_WEIGHT"
        Me.TOTALWEIGHTDataGridViewTextBoxColumn.HeaderText = "Total Weight"
        Me.TOTALWEIGHTDataGridViewTextBoxColumn.Name = "TOTALWEIGHTDataGridViewTextBoxColumn"
        Me.TOTALWEIGHTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MATCODEDataGridViewTextBoxColumn
        '
        Me.MATCODEDataGridViewTextBoxColumn.DataPropertyName = "mAT_CODE"
        Me.MATCODEDataGridViewTextBoxColumn.HeaderText = "Material code"
        Me.MATCODEDataGridViewTextBoxColumn.Name = "MATCODEDataGridViewTextBoxColumn"
        Me.MATCODEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PLANTDataGridViewTextBoxColumn
        '
        Me.PLANTDataGridViewTextBoxColumn.DataPropertyName = "pLANT"
        Me.PLANTDataGridViewTextBoxColumn.HeaderText = "Plant"
        Me.PLANTDataGridViewTextBoxColumn.Name = "PLANTDataGridViewTextBoxColumn"
        Me.PLANTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'STORAGELOCDataGridViewTextBoxColumn
        '
        Me.STORAGELOCDataGridViewTextBoxColumn.DataPropertyName = "sTORAGE_LOC"
        Me.STORAGELOCDataGridViewTextBoxColumn.HeaderText = "Storage Location"
        Me.STORAGELOCDataGridViewTextBoxColumn.Name = "STORAGELOCDataGridViewTextBoxColumn"
        Me.STORAGELOCDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BATCHNODataGridViewTextBoxColumn
        '
        Me.BATCHNODataGridViewTextBoxColumn.DataPropertyName = "bATCH_NO"
        Me.BATCHNODataGridViewTextBoxColumn.HeaderText = "Batch Number"
        Me.BATCHNODataGridViewTextBoxColumn.Name = "BATCHNODataGridViewTextBoxColumn"
        Me.BATCHNODataGridViewTextBoxColumn.ReadOnly = True
        '
        'ACTQTYDELVSUDataGridViewTextBoxColumn
        '
        Me.ACTQTYDELVSUDataGridViewTextBoxColumn.DataPropertyName = "aCT_QTY_DELV_SU"
        Me.ACTQTYDELVSUDataGridViewTextBoxColumn.HeaderText = "Actual quantity delivered (in sales units)"
        Me.ACTQTYDELVSUDataGridViewTextBoxColumn.Name = "ACTQTYDELVSUDataGridViewTextBoxColumn"
        Me.ACTQTYDELVSUDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BASEUOMDataGridViewTextBoxColumn
        '
        Me.BASEUOMDataGridViewTextBoxColumn.DataPropertyName = "bASE_UOM"
        Me.BASEUOMDataGridViewTextBoxColumn.HeaderText = "Base Unit of Measure"
        Me.BASEUOMDataGridViewTextBoxColumn.Name = "BASEUOMDataGridViewTextBoxColumn"
        Me.BASEUOMDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SALESUNITDataGridViewTextBoxColumn
        '
        Me.SALESUNITDataGridViewTextBoxColumn.DataPropertyName = "sALES_UNIT"
        Me.SALESUNITDataGridViewTextBoxColumn.HeaderText = "Sales unit"
        Me.SALESUNITDataGridViewTextBoxColumn.Name = "SALESUNITDataGridViewTextBoxColumn"
        Me.SALESUNITDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NETWEIGHTDataGridViewTextBoxColumn
        '
        Me.NETWEIGHTDataGridViewTextBoxColumn.DataPropertyName = "nET_WEIGHT"
        Me.NETWEIGHTDataGridViewTextBoxColumn.HeaderText = "Net weight"
        Me.NETWEIGHTDataGridViewTextBoxColumn.Name = "NETWEIGHTDataGridViewTextBoxColumn"
        Me.NETWEIGHTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'GROSSWEIGHTDataGridViewTextBoxColumn
        '
        Me.GROSSWEIGHTDataGridViewTextBoxColumn.DataPropertyName = "gROSS_WEIGHT"
        Me.GROSSWEIGHTDataGridViewTextBoxColumn.HeaderText = "Gross weight"
        Me.GROSSWEIGHTDataGridViewTextBoxColumn.Name = "GROSSWEIGHTDataGridViewTextBoxColumn"
        Me.GROSSWEIGHTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'WEIGHTUNITDataGridViewTextBoxColumn
        '
        Me.WEIGHTUNITDataGridViewTextBoxColumn.DataPropertyName = "wEIGHT_UNIT"
        Me.WEIGHTUNITDataGridViewTextBoxColumn.HeaderText = "Weight Unit"
        Me.WEIGHTUNITDataGridViewTextBoxColumn.Name = "WEIGHTUNITDataGridViewTextBoxColumn"
        Me.WEIGHTUNITDataGridViewTextBoxColumn.ReadOnly = True
        '
        'VOLUMEDataGridViewTextBoxColumn
        '
        Me.VOLUMEDataGridViewTextBoxColumn.DataPropertyName = "vOLUME"
        Me.VOLUMEDataGridViewTextBoxColumn.HeaderText = "Volume"
        Me.VOLUMEDataGridViewTextBoxColumn.Name = "VOLUMEDataGridViewTextBoxColumn"
        Me.VOLUMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'VOLUMEUNITDataGridViewTextBoxColumn
        '
        Me.VOLUMEUNITDataGridViewTextBoxColumn.DataPropertyName = "vOLUME_UNIT"
        Me.VOLUMEUNITDataGridViewTextBoxColumn.HeaderText = "Volume unit"
        Me.VOLUMEUNITDataGridViewTextBoxColumn.Name = "VOLUMEUNITDataGridViewTextBoxColumn"
        Me.VOLUMEUNITDataGridViewTextBoxColumn.ReadOnly = True
        '
        'OVRDELVTLIMITDataGridViewTextBoxColumn
        '
        Me.OVRDELVTLIMITDataGridViewTextBoxColumn.DataPropertyName = "oVR_DELV_TLIMIT"
        Me.OVRDELVTLIMITDataGridViewTextBoxColumn.HeaderText = "Overdelivery Tolerance Limit"
        Me.OVRDELVTLIMITDataGridViewTextBoxColumn.Name = "OVRDELVTLIMITDataGridViewTextBoxColumn"
        Me.OVRDELVTLIMITDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UNDDELVTLIMITDataGridViewTextBoxColumn
        '
        Me.UNDDELVTLIMITDataGridViewTextBoxColumn.DataPropertyName = "uND_DELV_TLIMIT"
        Me.UNDDELVTLIMITDataGridViewTextBoxColumn.HeaderText = "Underdelivery Tolerance Limit"
        Me.UNDDELVTLIMITDataGridViewTextBoxColumn.Name = "UNDDELVTLIMITDataGridViewTextBoxColumn"
        Me.UNDDELVTLIMITDataGridViewTextBoxColumn.ReadOnly = True
        '
        'sHIP_NO
        '
        Me.sHIP_NO.DataPropertyName = "sHIP_NO"
        Me.sHIP_NO.HeaderText = "Shipment Number"
        Me.sHIP_NO.Name = "sHIP_NO"
        Me.sHIP_NO.ReadOnly = True
        '
        'sHIP_TYPE
        '
        Me.sHIP_TYPE.DataPropertyName = "sHIP_TYPE"
        Me.sHIP_TYPE.HeaderText = "Shipment type"
        Me.sHIP_TYPE.Name = "sHIP_TYPE"
        Me.sHIP_TYPE.ReadOnly = True
        '
        'tRN_PLAN_POINT
        '
        Me.tRN_PLAN_POINT.DataPropertyName = "tRN_PLAN_POINT"
        Me.tRN_PLAN_POINT.HeaderText = "Transportation planning point"
        Me.tRN_PLAN_POINT.Name = "tRN_PLAN_POINT"
        Me.tRN_PLAN_POINT.ReadOnly = True
        '
        'cREATE_DATE
        '
        Me.cREATE_DATE.DataPropertyName = "cREATE_DATE"
        Me.cREATE_DATE.HeaderText = "Create date"
        Me.cREATE_DATE.Name = "cREATE_DATE"
        Me.cREATE_DATE.ReadOnly = True
        '
        'cREATE_TIME
        '
        Me.cREATE_TIME.DataPropertyName = "cREATE_TIME"
        Me.cREATE_TIME.HeaderText = "Create Time"
        Me.cREATE_TIME.Name = "cREATE_TIME"
        Me.cREATE_TIME.ReadOnly = True
        '
        'cONTAINER
        '
        Me.cONTAINER.DataPropertyName = "cONTAINER"
        Me.cONTAINER.HeaderText = "Container"
        Me.cONTAINER.Name = "cONTAINER"
        Me.cONTAINER.ReadOnly = True
        '
        'dRIVER_NAME
        '
        Me.dRIVER_NAME.DataPropertyName = "dRIVER_NAME"
        Me.dRIVER_NAME.HeaderText = "Driver name"
        Me.dRIVER_NAME.Name = "dRIVER_NAME"
        Me.dRIVER_NAME.ReadOnly = True
        '
        'cAR_LINCENSE
        '
        Me.cAR_LINCENSE.DataPropertyName = "cAR_LINCENSE"
        Me.cAR_LINCENSE.HeaderText = "Car license"
        Me.cAR_LINCENSE.Name = "cAR_LINCENSE"
        Me.cAR_LINCENSE.ReadOnly = True
        '
        'fORWD_AGENT
        '
        Me.fORWD_AGENT.DataPropertyName = "fORWD_AGENT"
        Me.fORWD_AGENT.HeaderText = "Forwarding Agent"
        Me.fORWD_AGENT.Name = "fORWD_AGENT"
        Me.fORWD_AGENT.ReadOnly = True
        '
        'wEIGHT_IN
        '
        Me.wEIGHT_IN.DataPropertyName = "wEIGHT_IN"
        Me.wEIGHT_IN.HeaderText = "Weight in"
        Me.wEIGHT_IN.Name = "wEIGHT_IN"
        Me.wEIGHT_IN.ReadOnly = True
        '
        'wEIGHT_OUT
        '
        Me.wEIGHT_OUT.DataPropertyName = "wEIGHT_OUT"
        Me.wEIGHT_OUT.HeaderText = "Weight out"
        Me.wEIGHT_OUT.Name = "wEIGHT_OUT"
        Me.wEIGHT_OUT.ReadOnly = True
        '
        'sEAL_NO
        '
        Me.sEAL_NO.DataPropertyName = "sEAL_NO"
        Me.sEAL_NO.HeaderText = "Seal no"
        Me.sEAL_NO.Name = "sEAL_NO"
        Me.sEAL_NO.ReadOnly = True
        '
        'sHIPP_TYPE
        '
        Me.sHIPP_TYPE.DataPropertyName = "sHIPP_TYPE"
        Me.sHIPP_TYPE.HeaderText = "Shipping Type"
        Me.sHIPP_TYPE.Name = "sHIPP_TYPE"
        Me.sHIPP_TYPE.ReadOnly = True
        '
        'sHIPP_COND
        '
        Me.sHIPP_COND.DataPropertyName = "sHIPP_COND"
        Me.sHIPP_COND.HeaderText = "Shipping condition"
        Me.sHIPP_COND.Name = "sHIPP_COND"
        Me.sHIPP_COND.ReadOnly = True
        '
        'sHIPPdO_NO
        '
        Me.sHIPPdO_NO.DataPropertyName = "sHIPPdO_NO"
        Me.sHIPPdO_NO.HeaderText = "D/O Number"
        Me.sHIPPdO_NO.Name = "sHIPPdO_NO"
        Me.sHIPPdO_NO.ReadOnly = True
        '
        'aCT_LOAD_SDATE
        '
        Me.aCT_LOAD_SDATE.DataPropertyName = "aCT_LOAD_SDATE"
        Me.aCT_LOAD_SDATE.HeaderText = "Actual Loading start date"
        Me.aCT_LOAD_SDATE.Name = "aCT_LOAD_SDATE"
        Me.aCT_LOAD_SDATE.ReadOnly = True
        '
        'aCT_LOAD_STIME
        '
        Me.aCT_LOAD_STIME.DataPropertyName = "aCT_LOAD_STIME"
        Me.aCT_LOAD_STIME.HeaderText = "Actual Loading start time"
        Me.aCT_LOAD_STIME.Name = "aCT_LOAD_STIME"
        Me.aCT_LOAD_STIME.ReadOnly = True
        '
        'aCT_LOAD_EDATE
        '
        Me.aCT_LOAD_EDATE.DataPropertyName = "aCT_LOAD_EDATE"
        Me.aCT_LOAD_EDATE.HeaderText = "Actual Loading end date"
        Me.aCT_LOAD_EDATE.Name = "aCT_LOAD_EDATE"
        Me.aCT_LOAD_EDATE.ReadOnly = True
        '
        'aCT_LOAD_ETIME
        '
        Me.aCT_LOAD_ETIME.DataPropertyName = "aCT_LOAD_ETIME"
        Me.aCT_LOAD_ETIME.HeaderText = "Actual Loading end time"
        Me.aCT_LOAD_ETIME.Name = "aCT_LOAD_ETIME"
        Me.aCT_LOAD_ETIME.ReadOnly = True
        '
        'T_DOTableAdapter
        '
        Me.T_DOTableAdapter.ClearBeforeFill = True
        '
        'GroupUnload
        '
        Me.GroupUnload.BackColor = System.Drawing.Color.Transparent
        Me.GroupUnload.Controls.Add(Me.BtDelete)
        Me.GroupUnload.Controls.Add(Me.BindingNavigator2)
        Me.GroupUnload.Controls.Add(Me.Label52)
        Me.GroupUnload.Controls.Add(Me.SelectDate)
        Me.GroupUnload.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupUnload.Location = New System.Drawing.Point(0, 0)
        Me.GroupUnload.Name = "GroupUnload"
        Me.GroupUnload.Size = New System.Drawing.Size(905, 43)
        Me.GroupUnload.TabIndex = 4
        Me.GroupUnload.TabStop = False
        '
        'BtDelete
        '
        Me.BtDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BtDelete.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtDelete.Location = New System.Drawing.Point(611, 10)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.Size = New System.Drawing.Size(74, 30)
        Me.BtDelete.TabIndex = 213
        Me.BtDelete.Tag = "16"
        Me.BtDelete.Text = "&Delete"
        Me.BtDelete.UseVisualStyleBackColor = True
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.BindingSource = Me.TDOBindingSource
        Me.BindingNavigator2.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.ToolStripButton1, Me.BindingNavigatorSeparator2, Me.EDFillter})
        Me.BindingNavigator2.Location = New System.Drawing.Point(205, 11)
        Me.BindingNavigator2.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator2.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator2.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator2.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator2.Size = New System.Drawing.Size(334, 25)
        Me.BindingNavigator2.TabIndex = 212
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.TAS_TOL.My.Resources.Resources.gtk_refresh
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'EDFillter
        '
        Me.EDFillter.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 25)
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label52.Location = New System.Drawing.Point(7, 14)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(52, 20)
        Me.Label52.TabIndex = 211
        Me.Label52.Text = "Date :"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SelectDate
        '
        Me.SelectDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.SelectDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.SelectDate.Location = New System.Drawing.Point(65, 11)
        Me.SelectDate.Name = "SelectDate"
        Me.SelectDate.Size = New System.Drawing.Size(137, 26)
        Me.SelectDate.TabIndex = 210
        '
        'Doitems
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(905, 422)
        Me.Controls.Add(Me.GroupUnload)
        Me.Controls.Add(Me.MeterGrid)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Doitems"
        Me.Tag = "16"
        Me.Text = "Doitems"
        CType(Me.TDOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupUnload.ResumeLayout(False)
        Me.GroupUnload.PerformLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TDOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_DOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_DOTableAdapter
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DONODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DOITEMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SONODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SOITEMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DOCDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DOCTIMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SDDOCCATDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESORGDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCHANDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DIVISIONDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SHIPPOINTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DOCDATEDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PLANGIDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRNPLANDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELVDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PICKDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SHIPTOPARTYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SOLDTOPARTYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTALWEIGHTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MATCODEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PLANTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STORAGELOCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCHNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACTQTYDELVSUDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASEUOMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESUNITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NETWEIGHTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GROSSWEIGHTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WEIGHTUNITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VOLUMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VOLUMEUNITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OVRDELVTLIMITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UNDDELVTLIMITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sHIP_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sHIP_TYPE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tRN_PLAN_POINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cREATE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cREATE_TIME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cONTAINER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dRIVER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cAR_LINCENSE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fORWD_AGENT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents wEIGHT_IN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents wEIGHT_OUT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sEAL_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sHIPP_TYPE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sHIPP_COND As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sHIPPdO_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents aCT_LOAD_SDATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents aCT_LOAD_STIME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents aCT_LOAD_EDATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents aCT_LOAD_ETIME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupUnload As System.Windows.Forms.GroupBox
    Friend WithEvents BtDelete As System.Windows.Forms.Button
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents SelectDate As System.Windows.Forms.DateTimePicker
End Class
