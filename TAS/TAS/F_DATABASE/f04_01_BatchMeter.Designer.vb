﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_BatchMeter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_BatchMeter))
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MeterNameBox = New System.Windows.Forms.TextBox()
        Me.TBATCHMETERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Updatedae = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MeterNoBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CreatedDate = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.IslandNoComboBox = New System.Windows.Forms.ComboBox()
        Me.TISLANDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MarkerDis = New System.Windows.Forms.RadioButton()
        Me.MarkerEn = New System.Windows.Forms.RadioButton()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.MeterButton = New System.Windows.Forms.RadioButton()
        Me.MeterTop = New System.Windows.Forms.RadioButton()
        Me.ProMainComboBox = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProBaseComboBox = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProBlendComboBox = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShipperComboBox = New System.Windows.Forms.ComboBox()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.MeterDis = New System.Windows.Forms.RadioButton()
        Me.MeterEn = New System.Windows.Forms.RadioButton()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TLOCATIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Batch_bay = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.PrintToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.BATCHNUMBERDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BATCHNAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.T_BATCHMETERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_BATCHMETERTableAdapter()
        Me.T_ISLANDTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ISLANDTableAdapter()
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        Me.T_LOCATIONTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_LOCATIONTableAdapter()
        CType(Me.TBATCHMETERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TISLANDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.DetailGroup.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.TLOCATIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MeterNameBox
        '
        Me.MeterNameBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "BATCH_NAME", True))
        Me.MeterNameBox.Location = New System.Drawing.Point(121, 49)
        Me.MeterNameBox.Name = "MeterNameBox"
        Me.MeterNameBox.Size = New System.Drawing.Size(163, 22)
        Me.MeterNameBox.TabIndex = 4
        '
        'TBATCHMETERBindingSource
        '
        Me.TBATCHMETERBindingSource.DataMember = "T_BATCHMETER"
        Me.TBATCHMETERBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(41, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 16)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Island No. :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Meter Name :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 202)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 16)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Product Blend :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Updatedae
        '
        Me.Updatedae.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.Updatedae.Enabled = False
        Me.Updatedae.Location = New System.Drawing.Point(121, 569)
        Me.Updatedae.Name = "Updatedae"
        Me.Updatedae.Size = New System.Drawing.Size(163, 22)
        Me.Updatedae.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(43, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Meter No. :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'MeterNoBox
        '
        Me.MeterNoBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "BATCH_NUMBER", True))
        Me.MeterNoBox.Location = New System.Drawing.Point(121, 21)
        Me.MeterNoBox.Name = "MeterNoBox"
        Me.MeterNoBox.Size = New System.Drawing.Size(163, 22)
        Me.MeterNoBox.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 232)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(107, 16)
        Me.Label9.TabIndex = 29
        Me.Label9.Text = "Service Shipper:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(23, 544)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 16)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "Created date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(24, 572)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Update Date :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(32, 437)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 16)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Meter Type :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CreatedDate
        '
        Me.CreatedDate.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "BATCH_DATE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        Me.CreatedDate.Enabled = False
        Me.CreatedDate.Location = New System.Drawing.Point(121, 541)
        Me.CreatedDate.Name = "CreatedDate"
        Me.CreatedDate.Size = New System.Drawing.Size(162, 22)
        Me.CreatedDate.TabIndex = 19
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 142)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(92, 16)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Product main :"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(20, 172)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 16)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "Product Base :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(27, 513)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(88, 16)
        Me.Label16.TabIndex = 32
        Me.Label16.Text = "Meter Status :"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(59, 475)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "Marker :"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'IslandNoComboBox
        '
        Me.IslandNoComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.IslandNoComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.IslandNoComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TBATCHMETERBindingSource, "BATCH_ISLAND_NO", True))
        Me.IslandNoComboBox.DataSource = Me.TISLANDBindingSource
        Me.IslandNoComboBox.DisplayMember = "ISLAND_NUMBER"
        Me.IslandNoComboBox.FormattingEnabled = True
        Me.IslandNoComboBox.Location = New System.Drawing.Point(121, 77)
        Me.IslandNoComboBox.Name = "IslandNoComboBox"
        Me.IslandNoComboBox.Size = New System.Drawing.Size(162, 24)
        Me.IslandNoComboBox.TabIndex = 5
        Me.IslandNoComboBox.ValueMember = "ISLAND_NUMBER"
        '
        'TISLANDBindingSource
        '
        Me.TISLANDBindingSource.DataMember = "T_ISLAND"
        Me.TISLANDBindingSource.DataSource = Me.FPTDataSet
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.MarkerDis)
        Me.GroupBox1.Controls.Add(Me.MarkerEn)
        Me.GroupBox1.Location = New System.Drawing.Point(121, 461)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(163, 36)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'MarkerDis
        '
        Me.MarkerDis.AutoSize = True
        Me.MarkerDis.Checked = True
        Me.MarkerDis.Location = New System.Drawing.Point(84, 12)
        Me.MarkerDis.Name = "MarkerDis"
        Me.MarkerDis.Size = New System.Drawing.Size(44, 20)
        Me.MarkerDis.TabIndex = 15
        Me.MarkerDis.TabStop = True
        Me.MarkerDis.Text = "No"
        Me.MarkerDis.UseVisualStyleBackColor = True
        '
        'MarkerEn
        '
        Me.MarkerEn.AutoSize = True
        Me.MarkerEn.Location = New System.Drawing.Point(9, 12)
        Me.MarkerEn.Name = "MarkerEn"
        Me.MarkerEn.Size = New System.Drawing.Size(50, 20)
        Me.MarkerEn.TabIndex = 14
        Me.MarkerEn.Text = "Yes"
        Me.MarkerEn.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bsave.Location = New System.Drawing.Point(120, 597)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(75, 30)
        Me.Bsave.TabIndex = 21
        Me.Bsave.Tag = "8"
        Me.Bsave.Text = "&Save"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'Bcancel
        '
        Me.Bcancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bcancel.Location = New System.Drawing.Point(208, 597)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(75, 30)
        Me.Bcancel.TabIndex = 22
        Me.Bcancel.Tag = "8"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.MeterButton)
        Me.GroupBox6.Controls.Add(Me.MeterTop)
        Me.GroupBox6.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox6.Location = New System.Drawing.Point(121, 421)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(163, 38)
        Me.GroupBox6.TabIndex = 10
        Me.GroupBox6.TabStop = False
        '
        'MeterButton
        '
        Me.MeterButton.AutoSize = True
        Me.MeterButton.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MeterButton.Location = New System.Drawing.Point(84, 14)
        Me.MeterButton.Name = "MeterButton"
        Me.MeterButton.Size = New System.Drawing.Size(63, 20)
        Me.MeterButton.TabIndex = 12
        Me.MeterButton.Text = "Button"
        Me.MeterButton.UseVisualStyleBackColor = True
        '
        'MeterTop
        '
        Me.MeterTop.AutoSize = True
        Me.MeterTop.Checked = True
        Me.MeterTop.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MeterTop.Location = New System.Drawing.Point(9, 14)
        Me.MeterTop.Name = "MeterTop"
        Me.MeterTop.Size = New System.Drawing.Size(51, 20)
        Me.MeterTop.TabIndex = 11
        Me.MeterTop.TabStop = True
        Me.MeterTop.Text = "Top"
        Me.MeterTop.UseVisualStyleBackColor = True
        '
        'ProMainComboBox
        '
        Me.ProMainComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ProMainComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ProMainComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TBATCHMETERBindingSource, "BATCH_PRO", True))
        Me.ProMainComboBox.DataSource = Me.TProductBindingSource
        Me.ProMainComboBox.DisplayMember = "Product_code"
        Me.ProMainComboBox.FormattingEnabled = True
        Me.ProMainComboBox.Location = New System.Drawing.Point(121, 139)
        Me.ProMainComboBox.Name = "ProMainComboBox"
        Me.ProMainComboBox.Size = New System.Drawing.Size(163, 24)
        Me.ProMainComboBox.TabIndex = 6
        Me.ProMainComboBox.ValueMember = "ID"
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        '
        'ProBaseComboBox
        '
        Me.ProBaseComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ProBaseComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ProBaseComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TBATCHMETERBindingSource, "BATCH_PROM1", True))
        Me.ProBaseComboBox.DataSource = Me.TProductBindingSource1
        Me.ProBaseComboBox.DisplayMember = "Product_code"
        Me.ProBaseComboBox.FormattingEnabled = True
        Me.ProBaseComboBox.Location = New System.Drawing.Point(121, 169)
        Me.ProBaseComboBox.Name = "ProBaseComboBox"
        Me.ProBaseComboBox.Size = New System.Drawing.Size(163, 24)
        Me.ProBaseComboBox.TabIndex = 7
        Me.ProBaseComboBox.ValueMember = "ID"
        '
        'TProductBindingSource1
        '
        Me.TProductBindingSource1.DataMember = "T_Product"
        Me.TProductBindingSource1.DataSource = Me.FPTDataSet
        '
        'ProBlendComboBox
        '
        Me.ProBlendComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ProBlendComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ProBlendComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TBATCHMETERBindingSource, "BATCH_PROM2", True))
        Me.ProBlendComboBox.DataSource = Me.TProductBindingSource2
        Me.ProBlendComboBox.DisplayMember = "Product_code"
        Me.ProBlendComboBox.FormattingEnabled = True
        Me.ProBlendComboBox.Location = New System.Drawing.Point(121, 199)
        Me.ProBlendComboBox.Name = "ProBlendComboBox"
        Me.ProBlendComboBox.Size = New System.Drawing.Size(162, 24)
        Me.ProBlendComboBox.TabIndex = 8
        Me.ProBlendComboBox.ValueMember = "ID"
        '
        'TProductBindingSource2
        '
        Me.TProductBindingSource2.DataMember = "T_Product"
        Me.TProductBindingSource2.DataSource = Me.FPTDataSet
        '
        'ShipperComboBox
        '
        Me.ShipperComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ShipperComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ShipperComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TBATCHMETERBindingSource, "Service_Shipper", True))
        Me.ShipperComboBox.DataSource = Me.TSHIPPERBindingSource
        Me.ShipperComboBox.DisplayMember = "SP_Code"
        Me.ShipperComboBox.FormattingEnabled = True
        Me.ShipperComboBox.Location = New System.Drawing.Point(121, 229)
        Me.ShipperComboBox.Name = "ShipperComboBox"
        Me.ShipperComboBox.Size = New System.Drawing.Size(163, 24)
        Me.ShipperComboBox.TabIndex = 9
        Me.ShipperComboBox.ValueMember = "ID"
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.MeterDis)
        Me.GroupBox2.Controls.Add(Me.MeterEn)
        Me.GroupBox2.Location = New System.Drawing.Point(121, 499)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(163, 36)
        Me.GroupBox2.TabIndex = 16
        Me.GroupBox2.TabStop = False
        '
        'MeterDis
        '
        Me.MeterDis.AutoSize = True
        Me.MeterDis.Location = New System.Drawing.Point(84, 12)
        Me.MeterDis.Name = "MeterDis"
        Me.MeterDis.Size = New System.Drawing.Size(73, 20)
        Me.MeterDis.TabIndex = 18
        Me.MeterDis.Text = "Disable"
        Me.MeterDis.UseVisualStyleBackColor = True
        '
        'MeterEn
        '
        Me.MeterEn.AutoSize = True
        Me.MeterEn.Checked = True
        Me.MeterEn.Location = New System.Drawing.Point(9, 13)
        Me.MeterEn.Name = "MeterEn"
        Me.MeterEn.Size = New System.Drawing.Size(77, 20)
        Me.MeterEn.TabIndex = 17
        Me.MeterEn.TabStop = True
        Me.MeterEn.Text = "Enabled"
        Me.MeterEn.UseVisualStyleBackColor = True
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.CheckBox1)
        Me.DetailGroup.Controls.Add(Me.GroupBox3)
        Me.DetailGroup.Controls.Add(Me.TextBox2)
        Me.DetailGroup.Controls.Add(Me.Label20)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.TextBox1)
        Me.DetailGroup.Controls.Add(Me.Label8)
        Me.DetailGroup.Controls.Add(Me.Batch_bay)
        Me.DetailGroup.Controls.Add(Me.Label7)
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.Label5)
        Me.DetailGroup.Controls.Add(Me.Label19)
        Me.DetailGroup.Controls.Add(Me.GroupBox2)
        Me.DetailGroup.Controls.Add(Me.ShipperComboBox)
        Me.DetailGroup.Controls.Add(Me.ProBlendComboBox)
        Me.DetailGroup.Controls.Add(Me.ProBaseComboBox)
        Me.DetailGroup.Controls.Add(Me.ProMainComboBox)
        Me.DetailGroup.Controls.Add(Me.GroupBox6)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.GroupBox1)
        Me.DetailGroup.Controls.Add(Me.IslandNoComboBox)
        Me.DetailGroup.Controls.Add(Me.Label18)
        Me.DetailGroup.Controls.Add(Me.Label16)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.Label14)
        Me.DetailGroup.Controls.Add(Me.CreatedDate)
        Me.DetailGroup.Controls.Add(Me.Label10)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.Label9)
        Me.DetailGroup.Controls.Add(Me.MeterNoBox)
        Me.DetailGroup.Controls.Add(Me.Label1)
        Me.DetailGroup.Controls.Add(Me.Updatedae)
        Me.DetailGroup.Controls.Add(Me.Label4)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.MeterNameBox)
        Me.DetailGroup.Enabled = False
        Me.DetailGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DetailGroup.Location = New System.Drawing.Point(479, 26)
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.Size = New System.Drawing.Size(325, 636)
        Me.DetailGroup.TabIndex = 2
        Me.DetailGroup.TabStop = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.TBATCHMETERBindingSource, "PI_STATUS", True))
        Me.CheckBox1.Location = New System.Drawing.Point(34, 287)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(81, 20)
        Me.CheckBox1.TabIndex = 148
        Me.CheckBox1.Text = "PI Temp."
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.TextBox3)
        Me.GroupBox3.Controls.Add(Me.ComboBox1)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.DataBindings.Add(New System.Windows.Forms.Binding("Enabled", Me.TBATCHMETERBindingSource, "PI_STATUS", True))
        Me.GroupBox3.Location = New System.Drawing.Point(11, 289)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(273, 93)
        Me.GroupBox3.TabIndex = 149
        Me.GroupBox3.TabStop = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(13, 63)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(84, 16)
        Me.Label21.TabIndex = 146
        Me.Label21.Text = "Tank Temp.:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "TANK_TEMP", True))
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(108, 60)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(153, 22)
        Me.TextBox3.TabIndex = 147
        '
        'ComboBox1
        '
        Me.ComboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ComboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.TBATCHMETERBindingSource, "TANK_NO", True))
        Me.ComboBox1.DataSource = Me.TLOCATIONBindingSource
        Me.ComboBox1.DisplayMember = "CODE"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(108, 25)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(153, 24)
        Me.ComboBox1.TabIndex = 141
        Me.ComboBox1.ValueMember = "CODE"
        '
        'TLOCATIONBindingSource
        '
        Me.TLOCATIONBindingSource.DataMember = "T_LOCATION"
        Me.TLOCATIONBindingSource.DataSource = Me.FPTDataSet
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(18, 28)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(79, 16)
        Me.Label17.TabIndex = 142
        Me.Label17.Text = "Load Tank :"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "MANUAL_TEMP", True))
        Me.TextBox2.Location = New System.Drawing.Point(120, 260)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(163, 22)
        Me.TextBox2.TabIndex = 145
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(18, 263)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(97, 16)
        Me.Label20.TabIndex = 144
        Me.Label20.Text = "Manual Temp.:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(31, 396)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 16)
        Me.Label11.TabIndex = 140
        Me.Label11.Text = "Load Temp.:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TBATCHMETERBindingSource, "BATCH_TEMP1", True))
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(121, 393)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(163, 22)
        Me.TextBox1.TabIndex = 139
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(290, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(20, 25)
        Me.Label8.TabIndex = 138
        Me.Label8.Text = "*"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Batch_bay
        '
        Me.Batch_bay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.Batch_bay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Batch_bay.FormattingEnabled = True
        Me.Batch_bay.Location = New System.Drawing.Point(121, 107)
        Me.Batch_bay.Name = "Batch_bay"
        Me.Batch_bay.Size = New System.Drawing.Size(163, 24)
        Me.Batch_bay.TabIndex = 6
        Me.Batch_bay.Tag = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(78, 110)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 16)
        Me.Label7.TabIndex = 137
        Me.Label7.Text = "bay :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(290, 77)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(20, 25)
        Me.Label6.TabIndex = 135
        Me.Label6.Text = "*"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(290, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 25)
        Me.Label5.TabIndex = 135
        Me.Label5.Text = "*"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Red
        Me.Label19.Location = New System.Drawing.Point(290, 21)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(20, 25)
        Me.Label19.TabIndex = 135
        Me.Label19.Text = "*"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'SaveToolStripButton
        '
        Me.SaveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.SaveToolStripButton.Image = CType(resources.GetObject("SaveToolStripButton.Image"), System.Drawing.Image)
        Me.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveToolStripButton.Name = "SaveToolStripButton"
        Me.SaveToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.SaveToolStripButton.Text = "&Save"
        '
        'PrintToolStripButton
        '
        Me.PrintToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.PrintToolStripButton.Image = CType(resources.GetObject("PrintToolStripButton.Image"), System.Drawing.Image)
        Me.PrintToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PrintToolStripButton.Name = "PrintToolStripButton"
        Me.PrintToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.PrintToolStripButton.Text = "&Print"
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BATCHNUMBERDataGridViewTextBoxColumn, Me.BATCHNAMEDataGridViewTextBoxColumn})
        Me.MeterGrid.DataSource = Me.TBATCHMETERBindingSource
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle20
        Me.MeterGrid.Location = New System.Drawing.Point(0, 32)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.MeterGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MeterGrid.Size = New System.Drawing.Size(473, 630)
        Me.MeterGrid.TabIndex = 1
        '
        'BATCHNUMBERDataGridViewTextBoxColumn
        '
        Me.BATCHNUMBERDataGridViewTextBoxColumn.DataPropertyName = "BATCH_NUMBER"
        Me.BATCHNUMBERDataGridViewTextBoxColumn.HeaderText = "Meter Number"
        Me.BATCHNUMBERDataGridViewTextBoxColumn.Name = "BATCHNUMBERDataGridViewTextBoxColumn"
        Me.BATCHNUMBERDataGridViewTextBoxColumn.ReadOnly = True
        Me.BATCHNUMBERDataGridViewTextBoxColumn.Width = 140
        '
        'BATCHNAMEDataGridViewTextBoxColumn
        '
        Me.BATCHNAMEDataGridViewTextBoxColumn.DataPropertyName = "BATCH_NAME"
        Me.BATCHNAMEDataGridViewTextBoxColumn.HeaderText = "Meter Name"
        Me.BATCHNAMEDataGridViewTextBoxColumn.Name = "BATCHNAMEDataGridViewTextBoxColumn"
        Me.BATCHNAMEDataGridViewTextBoxColumn.ReadOnly = True
        Me.BATCHNAMEDataGridViewTextBoxColumn.Width = 300
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TBATCHMETERBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(811, 27)
        Me.BindingNavigator1.TabIndex = 0
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 24)
        Me.Btadd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 24)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 24)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 24)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 24)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 24)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 24)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 27)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 24)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 27)
        '
        'T_BATCHMETERTableAdapter
        '
        Me.T_BATCHMETERTableAdapter.ClearBeforeFill = True
        '
        'T_ISLANDTableAdapter
        '
        Me.T_ISLANDTableAdapter.ClearBeforeFill = True
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'T_LOCATIONTableAdapter
        '
        Me.T_LOCATIONTableAdapter.ClearBeforeFill = True
        '
        'f04_01_BatchMeter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(811, 665)
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.DetailGroup)
        Me.Controls.Add(Me.MeterGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "f04_01_BatchMeter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "8"
        Me.Text = "Batch Meter"
        CType(Me.TBATCHMETERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TISLANDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.TLOCATIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MeterNameBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Updatedae As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MeterNoBox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CreatedDate As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents IslandNoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents MarkerDis As System.Windows.Forms.RadioButton
    Friend WithEvents MarkerEn As System.Windows.Forms.RadioButton
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents MeterButton As System.Windows.Forms.RadioButton
    Friend WithEvents MeterTop As System.Windows.Forms.RadioButton
    Friend WithEvents ProMainComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ProBaseComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ProBlendComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ShipperComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents MeterDis As System.Windows.Forms.RadioButton
    Friend WithEvents MeterEn As System.Windows.Forms.RadioButton
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents MeterNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents PrintToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TBATCHMETERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_BATCHMETERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_BATCHMETERTableAdapter
    Friend WithEvents TISLANDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ISLANDTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ISLANDTableAdapter
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents TProductBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents TProductBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents BATCHNUMBERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCHNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Batch_bay As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TLOCATIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_LOCATIONTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_LOCATIONTableAdapter
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
End Class
