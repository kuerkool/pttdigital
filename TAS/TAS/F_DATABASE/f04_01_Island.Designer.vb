﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f04_01_Island
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f04_01_Island))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.IsAddressCard = New System.Windows.Forms.TextBox()
        Me.TISLANDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.IsPortBatch = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ISLANDNUMBERDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.IsPortcard = New System.Windows.Forms.TextBox()
        Me.IsNumofMeter = New System.Windows.Forms.TextBox()
        Me.IsNumofArm = New System.Windows.Forms.TextBox()
        Me.IsNo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DetailGroup = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.IsCrdate = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.IsUpdate = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.IsLeftBay = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DisLeftBay = New System.Windows.Forms.RadioButton()
        Me.EnLeftBay = New System.Windows.Forms.RadioButton()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.IsBottom = New System.Windows.Forms.RadioButton()
        Me.ISTop = New System.Windows.Forms.RadioButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.IsCommulti = New System.Windows.Forms.RadioButton()
        Me.IsComSingle = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.IsRightBay = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.IsDisRightbay = New System.Windows.Forms.RadioButton()
        Me.IsEnRightBay = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Isdis = New System.Windows.Forms.RadioButton()
        Me.IsEn = New System.Windows.Forms.RadioButton()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.T_ISLANDTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ISLANDTableAdapter()
        CType(Me.TISLANDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DetailGroup.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'IsAddressCard
        '
        Me.IsAddressCard.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "Address_Card", True))
        resources.ApplyResources(Me.IsAddressCard, "IsAddressCard")
        Me.IsAddressCard.Name = "IsAddressCard"
        '
        'TISLANDBindingSource
        '
        Me.TISLANDBindingSource.DataMember = "T_ISLAND"
        Me.TISLANDBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'IsPortBatch
        '
        Me.IsPortBatch.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "Port_Batch", True))
        resources.ApplyResources(Me.IsPortBatch, "IsPortBatch")
        Me.IsPortBatch.Name = "IsPortBatch"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'MeterGrid
        '
        Me.MeterGrid.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.ISLANDNUMBERDataGridViewTextBoxColumn})
        Me.MeterGrid.DataSource = Me.TISLANDBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterGrid.DefaultCellStyle = DataGridViewCellStyle2
        resources.ApplyResources(Me.MeterGrid, "MeterGrid")
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.ReadOnly = True
        Me.MeterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        resources.ApplyResources(Me.IDDataGridViewTextBoxColumn, "IDDataGridViewTextBoxColumn")
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ISLANDNUMBERDataGridViewTextBoxColumn
        '
        Me.ISLANDNUMBERDataGridViewTextBoxColumn.DataPropertyName = "ISLAND_NUMBER"
        resources.ApplyResources(Me.ISLANDNUMBERDataGridViewTextBoxColumn, "ISLANDNUMBERDataGridViewTextBoxColumn")
        Me.ISLANDNUMBERDataGridViewTextBoxColumn.Name = "ISLANDNUMBERDataGridViewTextBoxColumn"
        Me.ISLANDNUMBERDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'IsPortcard
        '
        Me.IsPortcard.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "Port_Card", True))
        resources.ApplyResources(Me.IsPortcard, "IsPortcard")
        Me.IsPortcard.Name = "IsPortcard"
        '
        'IsNumofMeter
        '
        Me.IsNumofMeter.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "ISLAND_METER_SUM", True))
        resources.ApplyResources(Me.IsNumofMeter, "IsNumofMeter")
        Me.IsNumofMeter.Name = "IsNumofMeter"
        '
        'IsNumofArm
        '
        Me.IsNumofArm.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "ISLAND_ARM_NUMBER", True))
        resources.ApplyResources(Me.IsNumofArm, "IsNumofArm")
        Me.IsNumofArm.Name = "IsNumofArm"
        '
        'IsNo
        '
        Me.IsNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "ISLAND_NUMBER", True))
        resources.ApplyResources(Me.IsNo, "IsNo")
        Me.IsNo.Name = "IsNo"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'DetailGroup
        '
        Me.DetailGroup.Controls.Add(Me.IsNumofArm)
        Me.DetailGroup.Controls.Add(Me.IsNumofMeter)
        Me.DetailGroup.Controls.Add(Me.Label22)
        Me.DetailGroup.Controls.Add(Me.Label21)
        Me.DetailGroup.Controls.Add(Me.Label20)
        Me.DetailGroup.Controls.Add(Me.Label19)
        Me.DetailGroup.Controls.Add(Me.Label18)
        Me.DetailGroup.Controls.Add(Me.Label17)
        Me.DetailGroup.Controls.Add(Me.Label15)
        Me.DetailGroup.Controls.Add(Me.Bcancel)
        Me.DetailGroup.Controls.Add(Me.Bsave)
        Me.DetailGroup.Controls.Add(Me.IsCrdate)
        Me.DetailGroup.Controls.Add(Me.Label13)
        Me.DetailGroup.Controls.Add(Me.Label12)
        Me.DetailGroup.Controls.Add(Me.IsUpdate)
        Me.DetailGroup.Controls.Add(Me.Label11)
        Me.DetailGroup.Controls.Add(Me.Label10)
        Me.DetailGroup.Controls.Add(Me.Label6)
        Me.DetailGroup.Controls.Add(Me.GroupBox4)
        Me.DetailGroup.Controls.Add(Me.GroupBox6)
        Me.DetailGroup.Controls.Add(Me.GroupBox5)
        Me.DetailGroup.Controls.Add(Me.Label9)
        Me.DetailGroup.Controls.Add(Me.GroupBox3)
        Me.DetailGroup.Controls.Add(Me.GroupBox2)
        Me.DetailGroup.Controls.Add(Me.Label14)
        Me.DetailGroup.Controls.Add(Me.IsAddressCard)
        Me.DetailGroup.Controls.Add(Me.IsPortBatch)
        Me.DetailGroup.Controls.Add(Me.Label8)
        Me.DetailGroup.Controls.Add(Me.Label7)
        Me.DetailGroup.Controls.Add(Me.Label5)
        Me.DetailGroup.Controls.Add(Me.Label3)
        Me.DetailGroup.Controls.Add(Me.Label2)
        Me.DetailGroup.Controls.Add(Me.IsPortcard)
        Me.DetailGroup.Controls.Add(Me.IsNo)
        Me.DetailGroup.Controls.Add(Me.Label1)
        resources.ApplyResources(Me.DetailGroup, "DetailGroup")
        Me.DetailGroup.Name = "DetailGroup"
        Me.DetailGroup.TabStop = False
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.ForeColor = System.Drawing.Color.Red
        Me.Label22.Name = "Label22"
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.ForeColor = System.Drawing.Color.Red
        Me.Label21.Name = "Label21"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Name = "Label20"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.ForeColor = System.Drawing.Color.Red
        Me.Label19.Name = "Label19"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.ForeColor = System.Drawing.Color.Red
        Me.Label18.Name = "Label18"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.ForeColor = System.Drawing.Color.Red
        Me.Label17.Name = "Label17"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Name = "Label15"
        '
        'Bcancel
        '
        resources.ApplyResources(Me.Bcancel, "Bcancel")
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Tag = "7"
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        resources.ApplyResources(Me.Bsave, "Bsave")
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Tag = "7"
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'IsCrdate
        '
        Me.IsCrdate.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "ISLAND_DATE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        resources.ApplyResources(Me.IsCrdate, "IsCrdate")
        Me.IsCrdate.Name = "IsCrdate"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'IsUpdate
        '
        Me.IsUpdate.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "Update_date", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "G"))
        resources.ApplyResources(Me.IsUpdate, "IsUpdate")
        Me.IsUpdate.Name = "IsUpdate"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.IsLeftBay)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.DisLeftBay)
        Me.GroupBox4.Controls.Add(Me.EnLeftBay)
        Me.GroupBox4.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'IsLeftBay
        '
        Me.IsLeftBay.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "ISLAND_LEFT", True))
        resources.ApplyResources(Me.IsLeftBay, "IsLeftBay")
        Me.IsLeftBay.Name = "IsLeftBay"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label4.Name = "Label4"
        '
        'DisLeftBay
        '
        resources.ApplyResources(Me.DisLeftBay, "DisLeftBay")
        Me.DisLeftBay.ForeColor = System.Drawing.SystemColors.WindowText
        Me.DisLeftBay.Name = "DisLeftBay"
        Me.DisLeftBay.UseVisualStyleBackColor = True
        '
        'EnLeftBay
        '
        resources.ApplyResources(Me.EnLeftBay, "EnLeftBay")
        Me.EnLeftBay.Checked = True
        Me.EnLeftBay.ForeColor = System.Drawing.SystemColors.WindowText
        Me.EnLeftBay.Name = "EnLeftBay"
        Me.EnLeftBay.TabStop = True
        Me.EnLeftBay.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.IsBottom)
        Me.GroupBox6.Controls.Add(Me.ISTop)
        Me.GroupBox6.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'IsBottom
        '
        resources.ApplyResources(Me.IsBottom, "IsBottom")
        Me.IsBottom.ForeColor = System.Drawing.SystemColors.WindowText
        Me.IsBottom.Name = "IsBottom"
        Me.IsBottom.UseVisualStyleBackColor = True
        '
        'ISTop
        '
        resources.ApplyResources(Me.ISTop, "ISTop")
        Me.ISTop.Checked = True
        Me.ISTop.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ISTop.Name = "ISTop"
        Me.ISTop.TabStop = True
        Me.ISTop.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.IsCommulti)
        Me.GroupBox5.Controls.Add(Me.IsComSingle)
        Me.GroupBox5.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'IsCommulti
        '
        resources.ApplyResources(Me.IsCommulti, "IsCommulti")
        Me.IsCommulti.ForeColor = System.Drawing.SystemColors.WindowText
        Me.IsCommulti.Name = "IsCommulti"
        Me.IsCommulti.UseVisualStyleBackColor = True
        '
        'IsComSingle
        '
        resources.ApplyResources(Me.IsComSingle, "IsComSingle")
        Me.IsComSingle.Checked = True
        Me.IsComSingle.ForeColor = System.Drawing.SystemColors.WindowText
        Me.IsComSingle.Name = "IsComSingle"
        Me.IsComSingle.TabStop = True
        Me.IsComSingle.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.IsRightBay)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.IsDisRightbay)
        Me.GroupBox3.Controls.Add(Me.IsEnRightBay)
        Me.GroupBox3.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'IsRightBay
        '
        Me.IsRightBay.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TISLANDBindingSource, "ISLAND_RIGHT", True))
        resources.ApplyResources(Me.IsRightBay, "IsRightBay")
        Me.IsRightBay.Name = "IsRightBay"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label16.Name = "Label16"
        '
        'IsDisRightbay
        '
        resources.ApplyResources(Me.IsDisRightbay, "IsDisRightbay")
        Me.IsDisRightbay.ForeColor = System.Drawing.SystemColors.WindowText
        Me.IsDisRightbay.Name = "IsDisRightbay"
        Me.IsDisRightbay.UseVisualStyleBackColor = True
        '
        'IsEnRightBay
        '
        resources.ApplyResources(Me.IsEnRightBay, "IsEnRightBay")
        Me.IsEnRightBay.Checked = True
        Me.IsEnRightBay.ForeColor = System.Drawing.SystemColors.WindowText
        Me.IsEnRightBay.Name = "IsEnRightBay"
        Me.IsEnRightBay.TabStop = True
        Me.IsEnRightBay.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Isdis)
        Me.GroupBox2.Controls.Add(Me.IsEn)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'Isdis
        '
        resources.ApplyResources(Me.Isdis, "Isdis")
        Me.Isdis.Name = "Isdis"
        Me.Isdis.UseVisualStyleBackColor = True
        '
        'IsEn
        '
        resources.ApplyResources(Me.IsEn, "IsEn")
        Me.IsEn.Checked = True
        Me.IsEn.Name = "IsEn"
        Me.IsEn.TabStop = True
        Me.IsEn.UseVisualStyleBackColor = True
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        resources.ApplyResources(Me.BindingNavigator1, "BindingNavigator1")
        Me.BindingNavigator1.BindingSource = Me.TISLANDBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator, Me.EDFillter})
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btadd, "Btadd")
        Me.Btadd.Name = "Btadd"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        resources.ApplyResources(Me.ToolStripLabel1, "ToolStripLabel1")
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.BtDelete, "BtDelete")
        Me.BtDelete.Name = "BtDelete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btfirst, "Btfirst")
        Me.Btfirst.Name = "Btfirst"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btprevious, "Btprevious")
        Me.Btprevious.Name = "Btprevious"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'ToolStripTextBox1
        '
        resources.ApplyResources(Me.ToolStripTextBox1, "ToolStripTextBox1")
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        resources.ApplyResources(Me.ToolStripSeparator2, "ToolStripSeparator2")
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.Btnext, "Btnext")
        Me.Btnext.Name = "Btnext"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.BtLast, "BtLast")
        Me.BtLast.Name = "BtLast"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        resources.ApplyResources(Me.ToolStripSeparator3, "ToolStripSeparator3")
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.BtEdit, "BtEdit")
        Me.BtEdit.Name = "BtEdit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        resources.ApplyResources(Me.toolStripSeparator, "toolStripSeparator")
        '
        'EDFillter
        '
        Me.EDFillter.Name = "EDFillter"
        resources.ApplyResources(Me.EDFillter, "EDFillter")
        '
        'T_ISLANDTableAdapter
        '
        Me.T_ISLANDTableAdapter.ClearBeforeFill = True
        '
        'f04_01_Island
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.BindingNavigator1)
        Me.Controls.Add(Me.MeterGrid)
        Me.Controls.Add(Me.DetailGroup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "f04_01_Island"
        Me.Tag = "7"
        CType(Me.TISLANDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DetailGroup.ResumeLayout(False)
        Me.DetailGroup.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents IsAddressCard As System.Windows.Forms.TextBox
    Friend WithEvents IsPortBatch As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents IsPortcard As System.Windows.Forms.TextBox
    Friend WithEvents IsNumofMeter As System.Windows.Forms.TextBox
    Friend WithEvents IsNumofArm As System.Windows.Forms.TextBox
    Friend WithEvents IsNo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DetailGroup As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Isdis As System.Windows.Forms.RadioButton
    Friend WithEvents IsEn As System.Windows.Forms.RadioButton
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents IsRightBay As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents IsDisRightbay As System.Windows.Forms.RadioButton
    Friend WithEvents IsEnRightBay As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents IsBottom As System.Windows.Forms.RadioButton
    Friend WithEvents ISTop As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents IsCommulti As System.Windows.Forms.RadioButton
    Friend WithEvents IsComSingle As System.Windows.Forms.RadioButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents IsLeftBay As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DisLeftBay As System.Windows.Forms.RadioButton
    Friend WithEvents EnLeftBay As System.Windows.Forms.RadioButton
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents IsCrdate As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents IsUpdate As System.Windows.Forms.TextBox
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents TISLANDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ISLANDTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ISLANDTableAdapter
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ISLANDNUMBERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
End Class
