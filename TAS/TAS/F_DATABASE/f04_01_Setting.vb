﻿Imports TAS_TOL.C01_Permission
Public Class f04_01_Setting
    ' Public Present, weighttime As Integer
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_SETTING"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                If ds.HasChanges Then
                    da.Update(ds, "T_SETTING")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub
    Private Sub BT_Save()
        Try
            TSETTINGBindingSource.EndEdit()
            BindingContext(FPTDataSet, "T_SETTING").EndCurrentEdit()
            Save(FPTDataSet)
            Setting_Load(Nothing, Nothing)
            Me.BringToFront()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
        End Try
    End Sub



    Private Sub Setting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_SETTING' table. You can move, or remove it, as needed.
        Me.T_SETTINGTableAdapter.Fill(Me.FPTDataSet.T_SETTING)
        ' Btadd.Enabled = CheckAddPermisstion(sender.tag)
        Bsave.Enabled = CheckEditPermisstion(sender.tag)
        ' BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        If MsgBox("Are you sure to update?", vbYesNo + vbDefaultButton2, "Confirmation") = vbYes Then
            BT_Save()
        End If
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click, MyBase.FormClosed
        TSETTINGBindingSource.CancelEdit()
        Setting_Load(Nothing, Nothing)
        Close()
    End Sub

End Class