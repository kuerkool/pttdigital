﻿Imports ExtendedErrorProvider
Imports TAS_TOL.C01_Permission
Public Class f04_01_Truck
    Dim Addnew As Boolean
    Dim TRUCK_ID As Integer
    Dim MyErrorProvider As New ErrorProviderExtended
    Public Shared ShowType As Integer
    Public TRUCK_DRIVER As String
    Public TRUCK_COMPANY As String
    Public TRUCK_NO As String

    Private Sub ClearStatus()
        MyErrorProvider.Controls.Clear()
        MyErrorProvider.Controls.Add(VE_NUM, "Vehicle Number :")
        MyErrorProvider.Controls.Add(VE_DNAME, "Driver Name :")
        MyErrorProvider.Controls.Add(VE_COMNUM, "Number of Compartment :")
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        Me.BringToFront()
        Addnew = False
        TRUCK_ID = 0
        MeterGrid.ResumeLayout()
    End Sub

    Private Sub Truck_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_TRUCKTYPE' table. You can move, or remove it, as needed.
        Me.T_TRUCKTYPETableAdapter.Fill(Me.FPTDataSet.T_TRUCKTYPE)
        MeterGrid.SuspendLayout()
        'TODO: This line of code loads data into the 'FPTDataSet.T_TRUCKCOMPARTMENT' table. You can move, or remove it, as needed.
        Me.T_TRUCKCOMPARTMENTTableAdapter.Fill(Me.FPTDataSet.T_TRUCKCOMPARTMENT)
        'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
        Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
        'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
        'TODO: This line of code loads data into the 'FPTDataSet.T_DRIVER' table. You can move, or remove it, as needed.
        Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
        'TODO: This line of code loads data into the 'FPTDataSet.T_TRUCK' table. You can move, or remove it, as needed.
        Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)


        'Add controls one by one in error provider.
        MyErrorProvider.Controls.Clear()
        '  ToolTip1.SetToolTip(MeterNoBox, "กรุณาป้อนตัวเลขเท่านั้น")
        '  MyErrorProvider.Controls.Add(MeterNoBox, "Meter No:")
        ' MyErrorProvider.Controls.Add(VE_CHR, "Vehicle Character :")
        MyErrorProvider.Controls.Add(VE_NUM, "Vehicle Number :")
        MyErrorProvider.Controls.Add(VE_DNAME, "Driver Name :")
        'MyErrorProvider.Controls.Add(VE_TRAN, "Transportation Company :")
        MyErrorProvider.Controls.Add(VE_COMNUM, "Number of Compartment :")
        '  MyErrorProvider.Controls.Add(VE_SP, "Product main :")
        'Initially make emergency contact field as non mandatory
        'MyErrorProvider.Controls(txtEmergencyContact).Validate = False
        'Set summary error message
        MyErrorProvider.SummaryMessage = "Following fields are mandatory,"
        DetailGroup.Enabled = False
        MeterGrid.Enabled = True
        Btadd.Enabled = True
        BtEdit.Enabled = True
        BtDelete.Enabled = True
        Me.BringToFront()
        Addnew = False
        TRUCK_ID = 0
        MeterGrid.ResumeLayout()
        Btadd.Enabled = CheckAddPermisstion(sender.tag)
        BtEdit.Enabled = CheckEditPermisstion(sender.tag)
        BtDelete.Enabled = CheckDelPermisstion(sender.tag)
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Public Sub Save(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try

            Dim sql As String = "Select * from T_Truck"
            Dim sql2 As String = "Select * from T_TRUCKCOMPARTMENT"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql2, conn)
            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                Dim cb2 As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da2)

                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()

                If ds.HasChanges Then
                    da.Update(ds, "T_Truck")
                    da2.Update(ds, "T_TRUCKCOMPARTMENT")
                    ds.AcceptChanges()
                End If

            Finally
                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub TTRUCKBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TTRUCKBindingSource.PositionChanged

        Try
            If BtEdit.Enabled = False Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("update_date") = Now

            'If UCase(TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE").ToString()) = "NORMAL" Then
            '    VE_TYPEN.Checked = True
            'ElseIf UCase(TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE").ToString()) = "TRAIL" Then
            '    VE_TPYETR.Checked = True
            'Else
            '    VE_TYPESE.Checked = True
            'End If


            If UCase(TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM").ToString()) = "1" Then
                VE_ARMTOP.Checked = True
            Else

                VE_ARMBOTTOM.Checked = True
            End If

            If UCase(TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST").ToString()) = "YES" Then
                VE_BACKYES.Checked = True
            Else
                VE_BACKNO.Checked = True
            End If
        Catch ex As Exception

        End Try
        Try
            If ShowType <> 1 Then
                TRUCK_ID = TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("ID").ToString
                TTRUCKCOMPARTMENTBindingSource.Filter = "T_TRUCKID=" & TRUCK_ID

            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        VE_COMNUM_Leave(sender, e)
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then
            Try
                TRUCK_NO = VE_NUM.Text
                TRUCK_DRIVER = VE_DNAME.Text
                TRUCK_COMPANY = VE_TRAN.Text

                TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("update_date") = Now
                'If VE_TYPEN.Checked = True Then
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "NORMAL"
                'ElseIf VE_TPYETR.Checked = True Then
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "TRAIL"
                'Else
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "SEMI"
                'End If

                'If VE_ARMTOP.Checked = True Then
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "1"
                '    VE_ARMTOP.Checked = True
                'Else
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "0"
                'End If

                'If VE_BACKYES.Checked = True Then
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "YES"
                'Else
                '    TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "NO"
                'End If

                Try

                    TTRUCKBindingSource.EndEdit()
                    TTRUCKCOMPARTMENTBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_Truck").EndCurrentEdit()
                    BindingContext(FPTDataSet, "T_TRUCKCOMPARTMENT").EndCurrentEdit()
                    T_TRUCKTableAdapter.Update(FPTDataSet.T_TRUCK)
                    T_TRUCKCOMPARTMENTTableAdapter.Update(FPTDataSet.T_TRUCKCOMPARTMENT)
                    'Save(FPTDataSet)
                    ' Truck_Load(sender, e)
                    ClearStatus()
                    Me.BringToFront()
                Finally
                End Try

                If ShowType > 0 Then
                    Me.Close()
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)
            End Try
        End If
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        MeterGrid.Enabled = False
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
        'MeterNoBox.Focus()
        VE_COMNUM.Text = 0
        VE_CAPA.Text = 0
        '  VE_COMNUM_KeyUp(sender, Nothing)
        '  TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_COMP_NUM") = 1
        TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("ID") = TRUCK_ID
        TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_DATE") = Now
        TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "1"
        TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "1"
        TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "NO"
        VE_ARMTOP.Checked = True
        '   VE_TYPEN.Checked = True
        VE_BACKNO.Checked = True

        VE_ISSUEDATE.Value = Now
        VE_EXPIREDATE.Value = Now
        ' TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_MeasureLast") = Now
        '  TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_MeasureNext") = Now
        TTRUCKCOMPARTMENTBindingSource.Filter = "T_TRUCKID=" & Str(TRUCK_ID)

        Addnew = True
        VE_CHR.Focus()
        'MeterNoBox.Text = max + 1
    End Sub

    Private Sub Btadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btadd.Click
        Dim cells = From r As DataGridViewRow In MeterGrid.Rows.Cast(Of DataGridViewRow)() _
       Where Not r.IsNewRow _
       Select CInt(r.Cells(0).Value)
        Try
            TRUCK_ID = cells.Max + 1
        Catch ex As Exception

        End Try


        If ShowType = 1 Then
            MeterGrid.Enabled = False
            DetailGroup.Enabled = True
            Btadd.Enabled = False
            BtEdit.Enabled = False
            BtDelete.Enabled = False
            'MeterNoBox.Focus()
            VE_COMNUM.Text = 0
            VE_CAPA.Text = 0
            '  VE_COMNUM_KeyUp(sender, Nothing)
            '  TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_COMP_NUM") = 1
            ''  TTRUCKBindingSource.AddNew()
            'TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("ID") = TRUCK_ID
            'TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_DATE") = Now
            'TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "NORMAL"
            'TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "1"
            'TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "NO"
            'VE_ARMTOP.Checked = True
            ' VE_TYPEN.Checked = True
            'VE_BACKNO.Checked = True

            '  VE_ISSUEDATE.Value = Now
            ' VE_EXPIREDATE.Value = Now
            ' TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_MeasureLast") = Now
            '  TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_MeasureNext") = Now
            TTRUCKCOMPARTMENTBindingSource.Filter = "T_TRUCKID=" & Str(TRUCK_ID)
            Addnew = True
        End If

    End Sub

    Private Sub DataComp_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataComp.Leave
        Try
            Dim cells = From r As DataGridViewRow In DataComp.Rows.Cast(Of DataGridViewRow)() _
            Where Not r.IsNewRow _
            Select CInt(r.Cells(1).Value)
            VE_CAPA.Text = cells.Sum
        Catch ex As Exception
        End Try
    End Sub

    Private Sub VE_COMNUM_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VE_COMNUM.Leave
        Dim l As Integer
        Dim SumVal As Double
        Try
            If TTRUCKCOMPARTMENTBindingSource.Count <> VE_COMNUM.Text Then
                If VE_CAPA.Text = "" Then VE_CAPA.Text = 0
                SumVal = VE_CAPA.Text
                While TTRUCKCOMPARTMENTBindingSource.Count > VE_COMNUM.Text
                    TTRUCKCOMPARTMENTBindingSource.MoveLast()
                    SumVal = SumVal - TTRUCKCOMPARTMENTBindingSource.Item(TTRUCKCOMPARTMENTBindingSource.Position)("T_TRUCKCOMPCAP").ToString
                    VE_CAPA.Text = SumVal
                    TTRUCKCOMPARTMENTBindingSource.RemoveCurrent()
                End While
                For l = TTRUCKCOMPARTMENTBindingSource.Count + 1 To VE_COMNUM.Text
                    TTRUCKCOMPARTMENTBindingSource.AddNew()
                    TTRUCKCOMPARTMENTBindingSource.Item(TTRUCKCOMPARTMENTBindingSource.Position)("T_TRUCKID") = TRUCK_ID 'TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("ID").ToString
                    TTRUCKCOMPARTMENTBindingSource.Item(TTRUCKCOMPARTMENTBindingSource.Position)("T_TRUCKCOMPNO") = l
                    TTRUCKCOMPARTMENTBindingSource.Item(TTRUCKCOMPARTMENTBindingSource.Position)("T_TRUCKCOMPCAP") = 3000
                    SumVal = SumVal + TTRUCKCOMPARTMENTBindingSource.Item(TTRUCKCOMPARTMENTBindingSource.Position)("T_TRUCKCOMPCAP").ToString
                    VE_CAPA.Text = SumVal
                Next
                TTRUCKCOMPARTMENTBindingSource.Position = 0
            End If
        Catch ex As Exception
            '   VE_CAPA.Text = 0
        End Try
    End Sub

    Private Sub BtEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtEdit.Click
        MeterGrid.Enabled = True
        DetailGroup.Enabled = True
        Btadd.Enabled = False
        BtEdit.Enabled = False
        BtDelete.Enabled = False
    End Sub


    Private Sub BtDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtDelete.Click
        While TTRUCKCOMPARTMENTBindingSource.Count <> 0
            TTRUCKCOMPARTMENTBindingSource.RemoveCurrent()
        End While
        DetailGroup.Enabled = True

    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        TTRUCKBindingSource.CancelEdit()
        TTRUCKCOMPARTMENTBindingSource.CancelEdit()
        FPTDataSet.T_TRUCK.RejectChanges()
        FPTDataSet.T_TRUCKCOMPARTMENT.RejectChanges()
        ClearStatus()
        'Truck_Load(sender, e)
    End Sub

    Private Sub MeterGrid_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles MeterGrid.CellFormatting
        'change background color of entire row based on value of 4th cell
        Try
            With sender.Rows(e.RowIndex)
                Select Case UCase(.Cells(5).Value)
                    Case "NO"
                        .DefaultCellStyle.BackColor = Color.White
                    Case "YES"
                        .DefaultCellStyle.BackColor = Color.Red
                End Select
            End With

        Catch ex As Exception

        End Try

    End Sub

    Private Sub VE_DNAME_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VE_DNAME.Leave, VE_TRAN.Leave, VE_SP.Leave, VE_TRUCKTYPE.Leave
        Dim SelectText As String
        Dim Name As String
        SelectText = sender.text
        Name = UCase(sender.name)
        sender.SelectedIndex = sender.FindStringExact(SelectText)
        If (sender.SelectedIndex < 0) And (SelectText <> "") Then
            sender.SelectedIndex = -1
            If Name = "VE_TRAN" Then
                f04_01_TransportationCompany.Close()
                Me.AddOwnedForm(f04_01_TransportationCompany)
                f04_01_TransportationCompany.ShowType = 1
                f04_01_TransportationCompany.CompanyCode = SelectText
                f04_01_TransportationCompany.ShowDialog()
                Me.RemoveOwnedForm(f04_01_TransportationCompany)
                Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
                sender.SelectedIndex = sender.FindStringExact(SelectText)
            ElseIf Name = "VE_DNAME" Then
                Driver.Close()
                Me.AddOwnedForm(Driver)
                Driver.ShowType = 1
                Driver.DRIVER_NAME = SelectText
                Driver.ShowDialog()
                Me.RemoveOwnedForm(Driver)
                Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
                Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
                sender.SelectedIndex = sender.FindStringExact(SelectText)
                If f04_01_TransportationCompany.CompanyCode <> "" Then
                    VE_TRAN.SelectedIndex = VE_TRAN.FindStringExact(f04_01_TransportationCompany.CompanyCode)
                    If VE_TRAN.SelectedIndex <> -1 Then
                        VE_COMNUM.Focus()
                    End If
                Else : VE_TRAN.SelectedIndex = -1
                End If
            ElseIf Name = "VE_SP" Then
                f04_01_Shipper.Close()
                Me.AddOwnedForm(f04_01_Shipper)
                f04_01_Shipper.ShowType = 1
                f04_01_Shipper.Shipper_code = SelectText
                f04_01_Shipper.ShowDialog()
                Me.RemoveOwnedForm(f04_01_Shipper)
                Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
                sender.SelectedIndex = sender.FindStringExact(SelectText)
            End If
        Else


        End If

    End Sub


    Private Sub VE_TRA_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles VE_TRA.KeyPress, VE_CAERDNUM.KeyPress, VE_COMNUM.KeyPress, VE_CAPA.KeyPress, DataComp.KeyPress
        If e.KeyChar <> Chr(8) Then
            Dim strAllowableChars As String
            strAllowableChars = "0123456789-:/ "
            If InStr(strAllowableChars, e.KeyChar.ToString) = 0 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub VE_DNAME_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles VE_TRAN.KeyUp, VE_DNAME.KeyUp, VE_SP.KeyUp, VE_TRUCKTYPE.KeyUp
        If e.KeyCode = 13 Then
            VE_DNAME_Leave(sender, Nothing)
        End If
    End Sub

    Private Sub DataComp_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataComp.KeyUp
        DataComp_Leave(sender, Nothing)
    End Sub

    Private Sub VE_COMNUM_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles VE_COMNUM.KeyUp
        VE_COMNUM_Leave(sender, Nothing)
    End Sub

    Private Sub DataComp_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataComp.CellValueChanged
        DataComp_Leave(sender, Nothing)
    End Sub

    Private Sub SearchBy(ByVal FiledSearch As String, ByVal TextSearch As String)
        If TextSearch.Length > 0 Then
            TTRUCKBindingSource.Sort = FiledSearch
            TTRUCKBindingSource.Filter = FiledSearch & " LIKE '" & TextSearch & "*'"
        Else : TTRUCKBindingSource.Filter = ""
        End If
    End Sub


    Private Sub VE_TYPEN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "NORMAL"
    End Sub

    Private Sub VE_TPYETR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "TRAIL"
    End Sub

    Private Sub VE_TYPESE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "SEMI"
    End Sub

    Private Sub VE_ARMTOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VE_ARMTOP.Click
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "1"
    End Sub

    Private Sub VE_ARMBOTTOM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VE_ARMBOTTOM.Click
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "0"
    End Sub

    Private Sub VE_BACKYES_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VE_BACKYES.Click
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "YES"
    End Sub

    Private Sub VE_BACKNO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VE_BACKNO.Click
        If sender.Checked = True Then TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "NO"
    End Sub

    Private Sub Truck_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        If ShowType = 1 And Btadd.Enabled Then
            Me.Btadd.PerformClick()
            Me.VE_NUM.Text = TRUCK_NO
            '  If VE_NUM.Text = "" Then
            '  Me.VE_NUM.Text = Unloading.EDVehicle.Text
            ' End If
            'Me.TDRIVERBindingSource.Item(TDRIVERBindingSource.Position)("Driver_Date") = Now
            TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("ID") = TRUCK_ID
            TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_DATE") = Now
            TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_TYPE") = "1"
            TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_ARM") = "1"
            TTRUCKBindingSource.Item(TTRUCKBindingSource.Position)("TRUCK_BLACK_LIST") = "NO"
            VE_ARMTOP.Checked = True
            '   VE_TYPEN.Checked = True
            VE_BACKNO.Checked = True

            VE_ISSUEDATE.Value = Now
            VE_EXPIREDATE.Value = Now
            Me.VE_DNAME.Focus()
        ElseIf ShowType = 2 Then
            Me.BtEdit.PerformClick()
            TTRUCKBindingSource.Position = TTRUCKBindingSource.Find("TRUCK_NUMBER", TRUCK_NO)
            'If TTRUCKBindingSource.Find("TRUCK_NUMBER", Advisenote.Cbn2.Text) < 0 Then
            '    TTRUCKBindingSource.Position = (TTRUCKBindingSource.Find("TRUCK_NUMBER", Unloading.EDVehicle.Text))
            'End If

        ElseIf ShowType = 3 Then
            Me.BtEdit.PerformClick()
            TTRUCKBindingSource.Position = TTRUCKBindingSource.Find("TRUCK_NUMBER", TRUCK_NO)
            'If TTRUCKBindingSource.Find("TRUCK_NUMBER",  Unloading .EDVehicle ..Text) < 0 Then
            '    TTRUCKBindingSource.Position = (TTRUCKBindingSource.Find("TRUCK_NUMBER", Unloading.EDVehicle.Text))
            'End If

        End If
    End Sub

    Private Sub Truck_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ShowType = 0
    End Sub

    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.T_TRUCK.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.T_TRUCK.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.T_TRUCK.Columns(i).ColumnName & " like '" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        '    Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.T_TRUCK.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            TTRUCKBindingSource.Filter = StrFilter
        Else : TTRUCKBindingSource.RemoveFilter()
        End If

    End Sub


End Class