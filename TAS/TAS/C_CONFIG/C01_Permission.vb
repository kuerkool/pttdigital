﻿Imports System.Data.SqlClient
Public Class C01_Permission
    Public Shared Function GetUserRight(ByVal U_ID As Integer) As DataTable
        Dim con As New SqlConnection(My.Settings.FPTConnectionString)
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable
        con.Open()
        cmd.Connection = con
        cmd.CommandText = " Select * from vUserRights where UserID='" & U_ID.ToString & "'"
        da.SelectCommand = cmd
        da.Fill(dt)
        con.Close()
        con.Dispose()
        cmd.Dispose()
        da.Dispose()
        Return dt
    End Function
    Public Shared Function CheckViewPermisstion(ByVal FormID As Integer) As Boolean
        Try
            If Main.vUserRight.Compute("Max(chkview)", "FormID=" & FormID.ToString).ToString <> "True" Then
                MessageBox.Show("Access Denied", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return False
                Exit Function
            End If
        Catch ex As Exception
            MessageBox.Show("Access Denied", "Permission [" & ex.Message & "]", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            Exit Function
        End Try
        Return True
    End Function

    Public Shared Function CheckAddPermisstion(ByVal FormID As Integer) As Boolean
        Try
            If Main.vUserRight.Compute("Max(chkAdd)", "FormID=" & FormID.ToString).ToString = "True" Then
                '  MessageBox.Show("Access Denied", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return True
                Exit Function
            End If
        Catch ex As Exception
            'MessageBox.Show("Access Denied", "Permission [" & ex.Message & "]", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            Exit Function
        End Try
        Return False
    End Function

    Public Shared Function CheckEditPermisstion(ByVal FormID As Integer) As Boolean
        Try
            If Main.vUserRight.Compute("Max(chkEdit)", "FormID=" & FormID.ToString).ToString = "True" Then
                ' MessageBox.Show("Access Denied", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return True
                Exit Function
            End If
        Catch ex As Exception
            ' MessageBox.Show("Access Denied", "Permission [" & ex.Message & "]", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            Exit Function
        End Try
        Return False
    End Function

    Public Shared Function CheckDelPermisstion(ByVal FormID As Integer) As Boolean
        Try
            If Main.vUserRight.Compute("Max(chkDel)", "FormID=" & FormID.ToString).ToString = "True" Then
                '  MessageBox.Show("Access Denied", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return True
                Exit Function
            End If
        Catch ex As Exception
            ' MessageBox.Show("Access Denied", "Permission [" & ex.Message & "]", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            Exit Function
        End Try
        Return False
    End Function

    Public Shared Function CheckPrintPermisstion(ByVal FormID As Integer) As Boolean
        Try
            If Main.vUserRight.Compute("Max(chkPrint)", "FormID=" & FormID.ToString).ToString = "True" Then
                '  MessageBox.Show("Access Denied", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return True
                Exit Function
            End If
        Catch ex As Exception
            ' MessageBox.Show("Access Denied", "Permission [" & ex.Message & "]", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
            Exit Function
        End Try
        Return False
    End Function

End Class
