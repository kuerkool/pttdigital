﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class treeManu
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(treeManu))
        Me.TrackBarDThumbShape1 = New Telerik.WinControls.UI.TrackBarDThumbShape()
        Me.TrackBarUThumbShape1 = New Telerik.WinControls.UI.TrackBarUThumbShape()
        Me.TabOffice12Shape1 = New Telerik.WinControls.UI.TabOffice12Shape()
        Me.CustomShape1 = New Telerik.WinControls.OldShapeEditor.CustomShape()
        Me.DonutShape1 = New Telerik.WinControls.Tests.DonutShape()
        Me.MediaShape1 = New Telerik.WinControls.Tests.MediaShape()
        Me.EllipseShape1 = New Telerik.WinControls.EllipseShape()
        Me.OfficeShape1 = New Telerik.WinControls.UI.OfficeShape()
        Me.QaShape1 = New Telerik.WinControls.Tests.QAShape()
        Me.TabIEShape1 = New Telerik.WinControls.UI.TabIEShape()
        Me.TabVsShape1 = New Telerik.WinControls.UI.TabVsShape()
        Me.TrackBarLThumbShape1 = New Telerik.WinControls.UI.TrackBarLThumbShape()
        Me.RadPanel2 = New Telerik.WinControls.UI.RadPanel()
        Me.RadButton5 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton4 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton3 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton2 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton1 = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox9 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadClock1 = New Telerik.WinControls.UI.RadClock()
        Me.RadPanel1 = New Telerik.WinControls.UI.RadPanel()
        Me.W_DATETIME = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.weight = New System.Windows.Forms.Label()
        Me.weight_Unit = New System.Windows.Forms.Label()
        Me.RoundRectShape1 = New Telerik.WinControls.RoundRectShape(Me.components)
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.IMCus = New System.Windows.Forms.PictureBox()
        Me.RadCalendar1 = New Telerik.WinControls.UI.RadCalendar()
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        Me.BreezeTheme1 = New Telerik.WinControls.Themes.BreezeTheme()
        Me.RadStatusStrip1 = New Telerik.WinControls.UI.RadStatusStrip()
        Me.RadLabelElement1 = New Telerik.WinControls.UI.RadLabelElement()
        Me.M_NAME = New Telerik.WinControls.UI.RadLabelElement()
        Me.RadImageButtonElement1 = New Telerik.WinControls.UI.RadImageButtonElement()
        Me.RadLabelElement3 = New Telerik.WinControls.UI.RadLabelElement()
        Me.M_GROUP = New Telerik.WinControls.UI.RadLabelElement()
        Me.PMMI = New Telerik.WinControls.UI.RadPanel()
        Me.RadButton30 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton28 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton29 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton25 = New Telerik.WinControls.UI.RadButton()
        Me.PDatabase = New Telerik.WinControls.UI.RadPanel()
        Me.RadGroupBox4 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadButton17 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton16 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton11 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton21 = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadButton18 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton19 = New Telerik.WinControls.UI.RadButton()
        Me.BtUPMaster = New Telerik.WinControls.UI.RadButton()
        Me.RadButton15 = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadButton12 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton13 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton14 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton10 = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadButton31 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton6 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton7 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton8 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton9 = New Telerik.WinControls.UI.RadButton()
        Me.PREPORT = New Telerik.WinControls.UI.RadPanel()
        Me.RadButton20 = New Telerik.WinControls.UI.RadButton()
        Me.PUNload = New Telerik.WinControls.UI.RadPanel()
        Me.RadButton24 = New Telerik.WinControls.UI.RadButton()
        Me.PLaoding = New Telerik.WinControls.UI.RadPanel()
        Me.RadButton27 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton26 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton23 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton22 = New Telerik.WinControls.UI.RadButton()
        Me.RadButton32 = New Telerik.WinControls.UI.RadButton()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel2.SuspendLayout()
        CType(Me.RadButton5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox9.SuspendLayout()
        CType(Me.RadClock1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel1.SuspendLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IMCus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadCalendar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadStatusStrip1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PMMI, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PMMI.SuspendLayout()
        CType(Me.RadButton30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PDatabase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PDatabase.SuspendLayout()
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox4.SuspendLayout()
        CType(Me.RadButton17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox3.SuspendLayout()
        CType(Me.RadButton18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtUPMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox2.SuspendLayout()
        CType(Me.RadButton12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.RadButton31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PREPORT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PREPORT.SuspendLayout()
        CType(Me.RadButton20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PUNload, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PUNload.SuspendLayout()
        CType(Me.RadButton24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PLaoding, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PLaoding.SuspendLayout()
        CType(Me.RadButton27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CustomShape1
        '
        Me.CustomShape1.Dimension = New System.Drawing.Rectangle(0, 0, 0, 0)
        '
        'RadPanel2
        '
        Me.RadPanel2.BackColor = System.Drawing.Color.Transparent
        Me.RadPanel2.Controls.Add(Me.RadButton5)
        Me.RadPanel2.Controls.Add(Me.RadButton4)
        Me.RadPanel2.Controls.Add(Me.RadButton3)
        Me.RadPanel2.Controls.Add(Me.RadButton2)
        Me.RadPanel2.Controls.Add(Me.RadButton1)
        Me.RadPanel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.RadPanel2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel2.Location = New System.Drawing.Point(0, 147)
        Me.RadPanel2.Name = "RadPanel2"
        Me.RadPanel2.Size = New System.Drawing.Size(266, 525)
        Me.RadPanel2.TabIndex = 1
        Me.RadPanel2.ThemeName = "Breeze"
        CType(Me.RadPanel2.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel2.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Nothing
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.OfficeGlassRect
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 2.0!
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(251, Byte), Integer))
        '
        'RadButton5
        '
        Me.RadButton5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton5.ForeColor = System.Drawing.Color.White
        Me.RadButton5.Image = Global.TAS_TOL.My.Resources.Resources.report
        Me.RadButton5.Location = New System.Drawing.Point(10, 344)
        Me.RadButton5.Name = "RadButton5"
        Me.RadButton5.Size = New System.Drawing.Size(250, 73)
        Me.RadButton5.TabIndex = 4
        Me.RadButton5.Text = "&Report System"
        Me.RadButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton5.ThemeName = "Breeze"
        CType(Me.RadButton5.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.report
        CType(Me.RadButton5.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.RadButton5.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "&Report System"
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(71, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.Color.White
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).PositionOffset = New System.Drawing.SizeF(-10.0!, 0.0!)
        CType(Me.RadButton5.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'RadButton4
        '
        Me.RadButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton4.ForeColor = System.Drawing.Color.White
        Me.RadButton4.Image = Global.TAS_TOL.My.Resources.Resources.screen_sharing__1_
        Me.RadButton4.Location = New System.Drawing.Point(10, 263)
        Me.RadButton4.Name = "RadButton4"
        Me.RadButton4.Size = New System.Drawing.Size(250, 73)
        Me.RadButton4.TabIndex = 3
        Me.RadButton4.Text = "&MMI System"
        Me.RadButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton4.ThemeName = "Breeze"
        CType(Me.RadButton4.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.screen_sharing__1_
        CType(Me.RadButton4.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.RadButton4.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "&MMI System"
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(71, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.Color.White
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).PositionOffset = New System.Drawing.SizeF(-10.0!, 0.0!)
        CType(Me.RadButton4.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'RadButton3
        '
        Me.RadButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton3.ForeColor = System.Drawing.Color.White
        Me.RadButton3.Image = Global.TAS_TOL.My.Resources.Resources.tanker64
        Me.RadButton3.Location = New System.Drawing.Point(10, 182)
        Me.RadButton3.Name = "RadButton3"
        Me.RadButton3.Size = New System.Drawing.Size(250, 73)
        Me.RadButton3.TabIndex = 2
        Me.RadButton3.Text = "&Unloading System"
        Me.RadButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton3.ThemeName = "Breeze"
        CType(Me.RadButton3.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.tanker64
        CType(Me.RadButton3.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.RadButton3.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "&Unloading System"
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(71, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.Color.White
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).PositionOffset = New System.Drawing.SizeF(0.0!, 0.0!)
        CType(Me.RadButton3.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'RadButton2
        '
        Me.RadButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton2.ForeColor = System.Drawing.Color.White
        Me.RadButton2.Image = Global.TAS_TOL.My.Resources.Resources.fuel_tanker2
        Me.RadButton2.Location = New System.Drawing.Point(10, 101)
        Me.RadButton2.Name = "RadButton2"
        Me.RadButton2.Size = New System.Drawing.Size(250, 73)
        Me.RadButton2.TabIndex = 1
        Me.RadButton2.Text = "&Loading System"
        Me.RadButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton2.ThemeName = "Breeze"
        CType(Me.RadButton2.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.fuel_tanker2
        CType(Me.RadButton2.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.RadButton2.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "&Loading System"
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(71, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.Color.White
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).PositionOffset = New System.Drawing.SizeF(-10.0!, 0.0!)
        CType(Me.RadButton2.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'RadButton1
        '
        Me.RadButton1.BackColor = System.Drawing.Color.Transparent
        Me.RadButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton1.ForeColor = System.Drawing.Color.White
        Me.RadButton1.Image = Global.TAS_TOL.My.Resources.Resources.db
        Me.RadButton1.Location = New System.Drawing.Point(10, 20)
        Me.RadButton1.Name = "RadButton1"
        Me.RadButton1.Size = New System.Drawing.Size(250, 73)
        Me.RadButton1.TabIndex = 0
        Me.RadButton1.Text = "&Database System"
        Me.RadButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton1.ThemeName = "Breeze"
        CType(Me.RadButton1.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.db
        CType(Me.RadButton1.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.RadButton1.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "&Database System"
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(71, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.Color.White
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).PositionOffset = New System.Drawing.SizeF(-10.0!, 0.0!)
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(3), Telerik.WinControls.Primitives.FocusPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(3), Telerik.WinControls.Primitives.FocusPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(46, Byte), Integer))
        CType(Me.RadButton1.GetChildAt(0).GetChildAt(3), Telerik.WinControls.Primitives.FocusPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(46, Byte), Integer))
        '
        'RadGroupBox9
        '
        Me.RadGroupBox9.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox9.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RadGroupBox9.Controls.Add(Me.RadClock1)
        Me.RadGroupBox9.Controls.Add(Me.RadPanel1)
        Me.RadGroupBox9.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox9.Controls.Add(Me.IMCus)
        Me.RadGroupBox9.Controls.Add(Me.RadCalendar1)
        Me.RadGroupBox9.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadGroupBox9.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office
        Me.RadGroupBox9.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox9.HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Bottom
        Me.RadGroupBox9.HeaderText = ""
        Me.RadGroupBox9.Location = New System.Drawing.Point(0, 0)
        Me.RadGroupBox9.Name = "RadGroupBox9"
        Me.RadGroupBox9.Size = New System.Drawing.Size(1358, 147)
        Me.RadGroupBox9.TabIndex = 24
        Me.RadGroupBox9.ThemeName = "Office2010Blue"
        CType(Me.RadGroupBox9.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox9.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).InvalidateMeasureInMainLayout = 1
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor3 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor4 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor3 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor4 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.GroupBoxHeader).HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Bottom
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.GroupBoxHeader).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        '
        'RadClock1
        '
        Me.RadClock1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadClock1.Location = New System.Drawing.Point(1037, 3)
        Me.RadClock1.Name = "RadClock1"
        Me.RadClock1.Size = New System.Drawing.Size(134, 135)
        Me.RadClock1.TabIndex = 39
        Me.RadClock1.Text = "RadClock1"
        Me.RadClock1.ThemeName = "Office2010Blue"
        '
        'RadPanel1
        '
        Me.RadPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.RadPanel1.Controls.Add(Me.W_DATETIME)
        Me.RadPanel1.Controls.Add(Me.Label15)
        Me.RadPanel1.Controls.Add(Me.Label14)
        Me.RadPanel1.Controls.Add(Me.weight)
        Me.RadPanel1.Controls.Add(Me.weight_Unit)
        Me.RadPanel1.Location = New System.Drawing.Point(427, 60)
        Me.RadPanel1.Name = "RadPanel1"
        Me.RadPanel1.Size = New System.Drawing.Size(489, 66)
        Me.RadPanel1.TabIndex = 33
        CType(Me.RadPanel1.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel1.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        '
        'W_DATETIME
        '
        Me.W_DATETIME.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.W_DATETIME.AutoSize = True
        Me.W_DATETIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.W_DATETIME.ForeColor = System.Drawing.Color.White
        Me.W_DATETIME.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.W_DATETIME.Location = New System.Drawing.Point(75, 50)
        Me.W_DATETIME.Name = "W_DATETIME"
        Me.W_DATETIME.Size = New System.Drawing.Size(114, 13)
        Me.W_DATETIME.TabIndex = 32
        Me.W_DATETIME.Text = "dd/MM/yyyy hh:mm:ss"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(15, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(173, 52)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "WEIGHT"
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(22, 50)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(57, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Updated : "
        '
        'weight
        '
        Me.weight.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight.ForeColor = System.Drawing.Color.Lime
        Me.weight.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight.Location = New System.Drawing.Point(186, 6)
        Me.weight.Name = "weight"
        Me.weight.Size = New System.Drawing.Size(210, 52)
        Me.weight.TabIndex = 30
        Me.weight.Text = "0"
        Me.weight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'weight_Unit
        '
        Me.weight_Unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weight_Unit.AutoSize = True
        Me.weight_Unit.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight_Unit.ForeColor = System.Drawing.Color.Lime
        Me.weight_Unit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight_Unit.Location = New System.Drawing.Point(395, 6)
        Me.weight_Unit.Name = "weight_Unit"
        Me.weight_Unit.Size = New System.Drawing.Size(77, 52)
        Me.weight_Unit.TabIndex = 31
        Me.weight_Unit.Text = "KG"
        '
        'RadLabel1
        '
        Me.RadLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.RadLabel1.BackColor = System.Drawing.Color.Transparent
        Me.RadLabel1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.ForeColor = System.Drawing.Color.White
        Me.RadLabel1.Location = New System.Drawing.Point(425, 18)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(509, 34)
        Me.RadLabel1.TabIndex = 31
        Me.RadLabel1.TabStop = True
        Me.RadLabel1.Text = "TERMINAL AUTOMATION SYSTEM (TAS)"
        Me.RadLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'IMCus
        '
        Me.IMCus.BackColor = System.Drawing.Color.Transparent
        Me.IMCus.Image = Global.TAS_TOL.My.Resources.Resources.PTT2
        Me.IMCus.Location = New System.Drawing.Point(6, 3)
        Me.IMCus.Name = "IMCus"
        Me.IMCus.Size = New System.Drawing.Size(260, 133)
        Me.IMCus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.IMCus.TabIndex = 14
        Me.IMCus.TabStop = False
        '
        'RadCalendar1
        '
        Me.RadCalendar1.AllowSelect = False
        Me.RadCalendar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadCalendar1.Location = New System.Drawing.Point(1177, 3)
        Me.RadCalendar1.Name = "RadCalendar1"
        Me.RadCalendar1.ReadOnly = True
        Me.RadCalendar1.ShowHeader = False
        Me.RadCalendar1.ShowNavigationButtons = False
        Me.RadCalendar1.ShowViewHeader = True
        Me.RadCalendar1.Size = New System.Drawing.Size(176, 133)
        Me.RadCalendar1.TabIndex = 12
        Me.RadCalendar1.Text = "RadCalendar1"
        Me.RadCalendar1.ThemeName = "Office2010Blue"
        '
        'RadStatusStrip1
        '
        Me.RadStatusStrip1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.RadLabelElement1, Me.M_NAME, Me.RadImageButtonElement1, Me.RadLabelElement3, Me.M_GROUP})
        Me.RadStatusStrip1.Location = New System.Drawing.Point(0, 672)
        Me.RadStatusStrip1.Name = "RadStatusStrip1"
        Me.RadStatusStrip1.Size = New System.Drawing.Size(1358, 22)
        Me.RadStatusStrip1.TabIndex = 30
        Me.RadStatusStrip1.Text = "RadStatusStrip1"
        Me.RadStatusStrip1.ThemeName = "Office2010Blue"
        '
        'RadLabelElement1
        '
        Me.RadLabelElement1.AccessibleDescription = "User:"
        Me.RadLabelElement1.AccessibleName = "User:"
        Me.RadLabelElement1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabelElement1.Name = "RadLabelElement1"
        Me.RadStatusStrip1.SetSpring(Me.RadLabelElement1, False)
        Me.RadLabelElement1.Text = "User:"
        Me.RadLabelElement1.TextWrap = True
        Me.RadLabelElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'M_NAME
        '
        Me.M_NAME.AccessibleDescription = "Banyat Prasong"
        Me.M_NAME.AccessibleName = "Banyat Prasong"
        Me.M_NAME.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_NAME.Name = "M_NAME"
        Me.RadStatusStrip1.SetSpring(Me.M_NAME, False)
        Me.M_NAME.Text = "-"
        Me.M_NAME.TextWrap = True
        Me.M_NAME.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'RadImageButtonElement1
        '
        Me.RadImageButtonElement1.AccessibleDescription = "RadImageButtonElement1"
        Me.RadImageButtonElement1.AccessibleName = "RadImageButtonElement1"
        Me.RadImageButtonElement1.ImageIndexClicked = 0
        Me.RadImageButtonElement1.ImageIndexHovered = 0
        Me.RadImageButtonElement1.Name = "RadImageButtonElement1"
        Me.RadStatusStrip1.SetSpring(Me.RadImageButtonElement1, False)
        Me.RadImageButtonElement1.Text = "RadImageButtonElement1"
        Me.RadImageButtonElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'RadLabelElement3
        '
        Me.RadLabelElement3.AccessibleDescription = "Group: "
        Me.RadLabelElement3.AccessibleName = "Group: "
        Me.RadLabelElement3.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.RadLabelElement3.Name = "RadLabelElement3"
        Me.RadStatusStrip1.SetSpring(Me.RadLabelElement3, False)
        Me.RadLabelElement3.Text = "Group: "
        Me.RadLabelElement3.TextWrap = True
        Me.RadLabelElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'M_GROUP
        '
        Me.M_GROUP.AccessibleDescription = "Administrator"
        Me.M_GROUP.AccessibleName = "Administrator"
        Me.M_GROUP.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.M_GROUP.Name = "M_GROUP"
        Me.RadStatusStrip1.SetSpring(Me.M_GROUP, False)
        Me.M_GROUP.Text = ""
        Me.M_GROUP.TextWrap = True
        Me.M_GROUP.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'PMMI
        '
        Me.PMMI.BackColor = System.Drawing.Color.Transparent
        Me.PMMI.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.PTT3
        Me.PMMI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PMMI.Controls.Add(Me.RadButton30)
        Me.PMMI.Controls.Add(Me.RadButton28)
        Me.PMMI.Controls.Add(Me.RadButton29)
        Me.PMMI.Controls.Add(Me.RadButton25)
        Me.PMMI.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PMMI.Location = New System.Drawing.Point(266, 147)
        Me.PMMI.Name = "PMMI"
        Me.PMMI.Size = New System.Drawing.Size(1092, 525)
        Me.PMMI.TabIndex = 28
        Me.PMMI.ThemeName = "Breeze"
        CType(Me.PMMI.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.PMMI.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Nothing
        CType(Me.PMMI.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.PMMI.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.Transparent
        CType(Me.PMMI.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Transparent
        CType(Me.PMMI.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.PMMI.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.PMMI.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 2.0!
        CType(Me.PMMI.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(251, Byte), Integer))
        '
        'RadButton30
        '
        Me.RadButton30.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton30.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton30.Image = Global.TAS_TOL.My.Resources.Resources.screen_sharing__1_1
        Me.RadButton30.Location = New System.Drawing.Point(85, 317)
        Me.RadButton30.Name = "RadButton30"
        Me.RadButton30.Size = New System.Drawing.Size(298, 77)
        Me.RadButton30.TabIndex = 15
        Me.RadButton30.Tag = "22"
        Me.RadButton30.Text = "<html><span style=""font-size: 10px""><p>Ammonia Unloading</p></span></html>"
        Me.RadButton30.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton30.ThemeName = "Breeze"
        '
        'RadButton28
        '
        Me.RadButton28.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton28.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton28.Image = Global.TAS_TOL.My.Resources.Resources.screen_sharing__1_1
        Me.RadButton28.Location = New System.Drawing.Point(85, 234)
        Me.RadButton28.Name = "RadButton28"
        Me.RadButton28.Size = New System.Drawing.Size(298, 77)
        Me.RadButton28.TabIndex = 15
        Me.RadButton28.Tag = "22"
        Me.RadButton28.Text = "<html><span style=""font-size: 10px""><p>Bay Detail</p></span></html>"
        Me.RadButton28.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton28.ThemeName = "Breeze"
        '
        'RadButton29
        '
        Me.RadButton29.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton29.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton29.Image = Global.TAS_TOL.My.Resources.Resources.screen_sharing__1_1
        Me.RadButton29.Location = New System.Drawing.Point(85, 151)
        Me.RadButton29.Name = "RadButton29"
        Me.RadButton29.Size = New System.Drawing.Size(298, 77)
        Me.RadButton29.TabIndex = 16
        Me.RadButton29.Tag = "22"
        Me.RadButton29.Text = "<html><span style=""font-size: 10px""><p>Bay OverView</p></span></html>"
        Me.RadButton29.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton29.ThemeName = "Breeze"
        '
        'RadButton25
        '
        Me.RadButton25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton25.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton25.Image = Global.TAS_TOL.My.Resources.Resources.screen_sharing__1_1
        Me.RadButton25.Location = New System.Drawing.Point(85, 68)
        Me.RadButton25.Name = "RadButton25"
        Me.RadButton25.Size = New System.Drawing.Size(298, 77)
        Me.RadButton25.TabIndex = 14
        Me.RadButton25.Tag = "22"
        Me.RadButton25.Text = "<html><span style=""font-size: 10px""><p>Bay Loading</p></span></html>"
        Me.RadButton25.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton25.ThemeName = "Breeze"
        '
        'PDatabase
        '
        Me.PDatabase.BackColor = System.Drawing.Color.Transparent
        Me.PDatabase.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.PTT3
        Me.PDatabase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PDatabase.Controls.Add(Me.RadGroupBox4)
        Me.PDatabase.Controls.Add(Me.RadGroupBox3)
        Me.PDatabase.Controls.Add(Me.RadGroupBox2)
        Me.PDatabase.Controls.Add(Me.RadGroupBox1)
        Me.PDatabase.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PDatabase.Location = New System.Drawing.Point(266, 147)
        Me.PDatabase.Name = "PDatabase"
        Me.PDatabase.Size = New System.Drawing.Size(1092, 525)
        Me.PDatabase.TabIndex = 2
        Me.PDatabase.ThemeName = "Breeze"
        CType(Me.PDatabase.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.PDatabase.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Nothing
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.Transparent
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Transparent
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 2.0!
        CType(Me.PDatabase.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(251, Byte), Integer))
        '
        'RadGroupBox4
        '
        Me.RadGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RadGroupBox4.Controls.Add(Me.RadButton17)
        Me.RadGroupBox4.Controls.Add(Me.RadButton16)
        Me.RadGroupBox4.Controls.Add(Me.RadButton11)
        Me.RadGroupBox4.Controls.Add(Me.RadButton21)
        Me.RadGroupBox4.HeaderText = ""
        Me.RadGroupBox4.Location = New System.Drawing.Point(802, 68)
        Me.RadGroupBox4.Name = "RadGroupBox4"
        Me.RadGroupBox4.Size = New System.Drawing.Size(202, 267)
        Me.RadGroupBox4.TabIndex = 17
        Me.RadGroupBox4.ThemeName = "Office2010Blue"
        '
        'RadButton17
        '
        Me.RadButton17.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton17.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton17.Image = Global.TAS_TOL.My.Resources.Resources.location
        Me.RadButton17.Location = New System.Drawing.Point(5, 21)
        Me.RadButton17.Name = "RadButton17"
        Me.RadButton17.Size = New System.Drawing.Size(189, 55)
        Me.RadButton17.TabIndex = 10
        Me.RadButton17.Tag = "13"
        Me.RadButton17.Text = "&Location"
        Me.RadButton17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton17.ThemeName = "Breeze"
        '
        'RadButton16
        '
        Me.RadButton16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton16.Image = Global.TAS_TOL.My.Resources.Resources.deliver_shipment
        Me.RadButton16.Location = New System.Drawing.Point(5, 82)
        Me.RadButton16.Name = "RadButton16"
        Me.RadButton16.Size = New System.Drawing.Size(189, 55)
        Me.RadButton16.TabIndex = 9
        Me.RadButton16.Tag = "14"
        Me.RadButton16.Text = "&Shipment Type"
        Me.RadButton16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton16.ThemeName = "Breeze"
        '
        'RadButton11
        '
        Me.RadButton11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton11.Image = Global.TAS_TOL.My.Resources.Resources.How_to_Convert_Marketing_Leads_to_Sales
        Me.RadButton11.Location = New System.Drawing.Point(5, 143)
        Me.RadButton11.Name = "RadButton11"
        Me.RadButton11.Size = New System.Drawing.Size(189, 55)
        Me.RadButton11.TabIndex = 5
        Me.RadButton11.Tag = "15"
        Me.RadButton11.Text = "&Sales ORG."
        Me.RadButton11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton11.ThemeName = "Breeze"
        '
        'RadButton21
        '
        Me.RadButton21.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton21.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton21.Image = Global.TAS_TOL.My.Resources.Resources.cart
        Me.RadButton21.Location = New System.Drawing.Point(5, 204)
        Me.RadButton21.Name = "RadButton21"
        Me.RadButton21.Size = New System.Drawing.Size(189, 55)
        Me.RadButton21.TabIndex = 13
        Me.RadButton21.Tag = "16"
        Me.RadButton21.Text = "&Do && Shipment"
        Me.RadButton21.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton21.ThemeName = "Breeze"
        '
        'RadGroupBox3
        '
        Me.RadGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RadGroupBox3.Controls.Add(Me.RadButton18)
        Me.RadGroupBox3.Controls.Add(Me.RadButton19)
        Me.RadGroupBox3.Controls.Add(Me.BtUPMaster)
        Me.RadGroupBox3.Controls.Add(Me.RadButton15)
        Me.RadGroupBox3.HeaderText = ""
        Me.RadGroupBox3.Location = New System.Drawing.Point(568, 68)
        Me.RadGroupBox3.Name = "RadGroupBox3"
        Me.RadGroupBox3.Size = New System.Drawing.Size(202, 267)
        Me.RadGroupBox3.TabIndex = 16
        Me.RadGroupBox3.ThemeName = "Office2010Blue"
        '
        'RadButton18
        '
        Me.RadButton18.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton18.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton18.Image = Global.TAS_TOL.My.Resources.Resources.Setting
        Me.RadButton18.Location = New System.Drawing.Point(5, 21)
        Me.RadButton18.Name = "RadButton18"
        Me.RadButton18.Size = New System.Drawing.Size(189, 55)
        Me.RadButton18.TabIndex = 11
        Me.RadButton18.Tag = "9"
        Me.RadButton18.Text = "&Setting"
        Me.RadButton18.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton18.ThemeName = "Breeze"
        '
        'RadButton19
        '
        Me.RadButton19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton19.Image = Global.TAS_TOL.My.Resources.Resources.coal_power_plant
        Me.RadButton19.Location = New System.Drawing.Point(5, 82)
        Me.RadButton19.Name = "RadButton19"
        Me.RadButton19.Size = New System.Drawing.Size(189, 55)
        Me.RadButton19.TabIndex = 12
        Me.RadButton19.Tag = "10"
        Me.RadButton19.Text = "&Plant Configuration"
        Me.RadButton19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton19.ThemeName = "Breeze"
        '
        'BtUPMaster
        '
        Me.BtUPMaster.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtUPMaster.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtUPMaster.Image = Global.TAS_TOL.My.Resources.Resources.db_update
        Me.BtUPMaster.Location = New System.Drawing.Point(5, 143)
        Me.BtUPMaster.Name = "BtUPMaster"
        Me.BtUPMaster.Size = New System.Drawing.Size(189, 55)
        Me.BtUPMaster.TabIndex = 12
        Me.BtUPMaster.Tag = "11"
        Me.BtUPMaster.Text = "&Master database"
        Me.BtUPMaster.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtUPMaster.ThemeName = "Breeze"
        '
        'RadButton15
        '
        Me.RadButton15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton15.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton15.Image = Global.TAS_TOL.My.Resources.Resources.user_group_icon
        Me.RadButton15.Location = New System.Drawing.Point(5, 204)
        Me.RadButton15.Name = "RadButton15"
        Me.RadButton15.Size = New System.Drawing.Size(189, 55)
        Me.RadButton15.TabIndex = 8
        Me.RadButton15.Tag = "12"
        Me.RadButton15.Text = "&User Account"
        Me.RadButton15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton15.ThemeName = "Breeze"
        '
        'RadGroupBox2
        '
        Me.RadGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RadGroupBox2.Controls.Add(Me.RadButton32)
        Me.RadGroupBox2.Controls.Add(Me.RadButton12)
        Me.RadGroupBox2.Controls.Add(Me.RadButton13)
        Me.RadGroupBox2.Controls.Add(Me.RadButton14)
        Me.RadGroupBox2.Controls.Add(Me.RadButton10)
        Me.RadGroupBox2.HeaderText = ""
        Me.RadGroupBox2.Location = New System.Drawing.Point(334, 68)
        Me.RadGroupBox2.Name = "RadGroupBox2"
        Me.RadGroupBox2.Size = New System.Drawing.Size(202, 329)
        Me.RadGroupBox2.TabIndex = 15
        Me.RadGroupBox2.ThemeName = "Office2010Blue"
        '
        'RadButton12
        '
        Me.RadButton12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton12.Image = Global.TAS_TOL.My.Resources.Resources.product_48
        Me.RadButton12.Location = New System.Drawing.Point(5, 21)
        Me.RadButton12.Name = "RadButton12"
        Me.RadButton12.Size = New System.Drawing.Size(189, 55)
        Me.RadButton12.TabIndex = 6
        Me.RadButton12.Tag = "5"
        Me.RadButton12.Text = "&Products"
        Me.RadButton12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton12.ThemeName = "Breeze"
        '
        'RadButton13
        '
        Me.RadButton13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton13.Image = Global.TAS_TOL.My.Resources.Resources.DANLOAD6000_SIDEVIEW4
        Me.RadButton13.Location = New System.Drawing.Point(5, 204)
        Me.RadButton13.Name = "RadButton13"
        Me.RadButton13.Size = New System.Drawing.Size(189, 55)
        Me.RadButton13.TabIndex = 7
        Me.RadButton13.Tag = "8"
        Me.RadButton13.Text = "&Batch Meter"
        Me.RadButton13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton13.ThemeName = "Breeze"
        '
        'RadButton14
        '
        Me.RadButton14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton14.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton14.Image = Global.TAS_TOL.My.Resources.Resources.sdsd1
        Me.RadButton14.Location = New System.Drawing.Point(5, 143)
        Me.RadButton14.Name = "RadButton14"
        Me.RadButton14.Size = New System.Drawing.Size(189, 55)
        Me.RadButton14.TabIndex = 8
        Me.RadButton14.Tag = "7"
        Me.RadButton14.Text = "&Island"
        Me.RadButton14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton14.ThemeName = "Breeze"
        '
        'RadButton10
        '
        Me.RadButton10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton10.Image = Global.TAS_TOL.My.Resources.Resources.packing48
        Me.RadButton10.Location = New System.Drawing.Point(5, 82)
        Me.RadButton10.Name = "RadButton10"
        Me.RadButton10.Size = New System.Drawing.Size(189, 55)
        Me.RadButton10.TabIndex = 4
        Me.RadButton10.Tag = "6"
        Me.RadButton10.Text = "&Packing "
        Me.RadButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton10.ThemeName = "Breeze"
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RadGroupBox1.Controls.Add(Me.RadButton31)
        Me.RadGroupBox1.Controls.Add(Me.RadButton6)
        Me.RadGroupBox1.Controls.Add(Me.RadButton7)
        Me.RadGroupBox1.Controls.Add(Me.RadButton8)
        Me.RadGroupBox1.Controls.Add(Me.RadButton9)
        Me.RadGroupBox1.HeaderText = ""
        Me.RadGroupBox1.Location = New System.Drawing.Point(100, 68)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(202, 329)
        Me.RadGroupBox1.TabIndex = 14
        Me.RadGroupBox1.ThemeName = "Office2010Blue"
        CType(Me.RadGroupBox1.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'RadButton31
        '
        Me.RadButton31.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton31.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton31.Location = New System.Drawing.Point(5, 265)
        Me.RadButton31.Name = "RadButton31"
        Me.RadButton31.Size = New System.Drawing.Size(189, 55)
        Me.RadButton31.TabIndex = 8
        Me.RadButton31.Tag = "8"
        Me.RadButton31.Text = "&Load Card"
        Me.RadButton31.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton31.ThemeName = "Breeze"
        '
        'RadButton6
        '
        Me.RadButton6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton6.Image = Global.TAS_TOL.My.Resources.Resources.company
        Me.RadButton6.Location = New System.Drawing.Point(5, 21)
        Me.RadButton6.Name = "RadButton6"
        Me.RadButton6.Size = New System.Drawing.Size(189, 55)
        Me.RadButton6.TabIndex = 0
        Me.RadButton6.Tag = "1"
        Me.RadButton6.Text = "&Customer Company"
        Me.RadButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton6.ThemeName = "Breeze"
        '
        'RadButton7
        '
        Me.RadButton7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton7.Image = Global.TAS_TOL.My.Resources.Resources.company
        Me.RadButton7.Location = New System.Drawing.Point(5, 82)
        Me.RadButton7.Name = "RadButton7"
        Me.RadButton7.Size = New System.Drawing.Size(189, 55)
        Me.RadButton7.TabIndex = 1
        Me.RadButton7.Tag = "2"
        Me.RadButton7.Text = "&Forwarding Agent "
        Me.RadButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton7.ThemeName = "Breeze"
        '
        'RadButton8
        '
        Me.RadButton8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton8.Image = Global.TAS_TOL.My.Resources.Resources.user_male48
        Me.RadButton8.Location = New System.Drawing.Point(5, 143)
        Me.RadButton8.Name = "RadButton8"
        Me.RadButton8.Size = New System.Drawing.Size(189, 55)
        Me.RadButton8.TabIndex = 2
        Me.RadButton8.Tag = "3"
        Me.RadButton8.Text = "&Driver"
        Me.RadButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton8.ThemeName = "Breeze"
        '
        'RadButton9
        '
        Me.RadButton9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton9.Image = Global.TAS_TOL.My.Resources.Resources.fuel_tanker2
        Me.RadButton9.Location = New System.Drawing.Point(5, 204)
        Me.RadButton9.Name = "RadButton9"
        Me.RadButton9.Size = New System.Drawing.Size(189, 55)
        Me.RadButton9.TabIndex = 3
        Me.RadButton9.Tag = "4"
        Me.RadButton9.Text = "&Vehicle"
        Me.RadButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton9.ThemeName = "Breeze"
        '
        'PREPORT
        '
        Me.PREPORT.BackColor = System.Drawing.Color.Transparent
        Me.PREPORT.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.PTT3
        Me.PREPORT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PREPORT.Controls.Add(Me.RadButton20)
        Me.PREPORT.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PREPORT.Location = New System.Drawing.Point(266, 147)
        Me.PREPORT.Name = "PREPORT"
        Me.PREPORT.Size = New System.Drawing.Size(1092, 525)
        Me.PREPORT.TabIndex = 29
        Me.PREPORT.ThemeName = "Breeze"
        CType(Me.PREPORT.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.PREPORT.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Nothing
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.Transparent
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Transparent
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 2.0!
        CType(Me.PREPORT.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(251, Byte), Integer))
        '
        'RadButton20
        '
        Me.RadButton20.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton20.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton20.Image = Global.TAS_TOL.My.Resources.Resources.report
        Me.RadButton20.Location = New System.Drawing.Point(79, 111)
        Me.RadButton20.Name = "RadButton20"
        Me.RadButton20.Size = New System.Drawing.Size(298, 77)
        Me.RadButton20.TabIndex = 13
        Me.RadButton20.Tag = "23"
        Me.RadButton20.Text = "<html><span style=""font-size: 10px""><p>Report</p></span></html>"
        Me.RadButton20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton20.ThemeName = "Breeze"
        '
        'PUNload
        '
        Me.PUNload.BackColor = System.Drawing.Color.Transparent
        Me.PUNload.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.PTT3
        Me.PUNload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PUNload.Controls.Add(Me.RadButton24)
        Me.PUNload.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PUNload.Location = New System.Drawing.Point(266, 147)
        Me.PUNload.Name = "PUNload"
        Me.PUNload.Size = New System.Drawing.Size(1092, 525)
        Me.PUNload.TabIndex = 27
        Me.PUNload.ThemeName = "Breeze"
        CType(Me.PUNload.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.PUNload.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Nothing
        CType(Me.PUNload.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.PUNload.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.Transparent
        CType(Me.PUNload.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Transparent
        CType(Me.PUNload.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.PUNload.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.PUNload.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 2.0!
        CType(Me.PUNload.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(251, Byte), Integer))
        '
        'RadButton24
        '
        Me.RadButton24.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton24.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton24.Image = Global.TAS_TOL.My.Resources.Resources.Comic_Icons_3_files_edit
        Me.RadButton24.Location = New System.Drawing.Point(82, 73)
        Me.RadButton24.Name = "RadButton24"
        Me.RadButton24.Size = New System.Drawing.Size(298, 77)
        Me.RadButton24.TabIndex = 12
        Me.RadButton24.Tag = "21"
        Me.RadButton24.Text = "<html><span style=""font-size: 10px""><p>UNLoading Advise Note</p></span></html>"
        Me.RadButton24.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton24.ThemeName = "Breeze"
        '
        'PLaoding
        '
        Me.PLaoding.BackColor = System.Drawing.Color.Transparent
        Me.PLaoding.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.PTT3
        Me.PLaoding.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PLaoding.Controls.Add(Me.RadButton27)
        Me.PLaoding.Controls.Add(Me.RadButton26)
        Me.PLaoding.Controls.Add(Me.RadButton23)
        Me.PLaoding.Controls.Add(Me.RadButton22)
        Me.PLaoding.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PLaoding.Location = New System.Drawing.Point(266, 147)
        Me.PLaoding.Name = "PLaoding"
        Me.PLaoding.Size = New System.Drawing.Size(1092, 525)
        Me.PLaoding.TabIndex = 26
        Me.PLaoding.ThemeName = "Breeze"
        CType(Me.PLaoding.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.PLaoding.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Nothing
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.Transparent
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Transparent
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 2.0!
        CType(Me.PLaoding.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(251, Byte), Integer))
        '
        'RadButton27
        '
        Me.RadButton27.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton27.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton27.Image = Global.TAS_TOL.My.Resources.Resources.Comic_Icons_3_files_edit
        Me.RadButton27.Location = New System.Drawing.Point(85, 320)
        Me.RadButton27.Name = "RadButton27"
        Me.RadButton27.Size = New System.Drawing.Size(298, 77)
        Me.RadButton27.TabIndex = 14
        Me.RadButton27.Tag = "20"
        Me.RadButton27.Text = "<html><span style=""font-size: 10px""><p>Easy weight</p></span></html>"
        Me.RadButton27.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton27.ThemeName = "Breeze"
        '
        'RadButton26
        '
        Me.RadButton26.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton26.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton26.Image = Global.TAS_TOL.My.Resources.Resources.Comic_Tiger_2_folder_edit
        Me.RadButton26.Location = New System.Drawing.Point(85, 237)
        Me.RadButton26.Name = "RadButton26"
        Me.RadButton26.Size = New System.Drawing.Size(298, 77)
        Me.RadButton26.TabIndex = 13
        Me.RadButton26.Tag = "19"
        Me.RadButton26.Text = "<html><span style=""font-size: 10px""><p><span lang=""TH"">Plan Loading </span></p></" & _
    "span></html>"
        Me.RadButton26.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton26.ThemeName = "Breeze"
        '
        'RadButton23
        '
        Me.RadButton23.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton23.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton23.Image = Global.TAS_TOL.My.Resources.Resources.Comic_Tiger_2_folder_edit
        Me.RadButton23.Location = New System.Drawing.Point(85, 154)
        Me.RadButton23.Name = "RadButton23"
        Me.RadButton23.Size = New System.Drawing.Size(298, 77)
        Me.RadButton23.TabIndex = 12
        Me.RadButton23.Tag = "18"
        Me.RadButton23.Text = "<html><span style=""font-size: 10px""><p>Loading Advise Note</p><span lang=""TH""><p>" & _
    "Other station</p></span></span></html>"
        Me.RadButton23.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton23.ThemeName = "Breeze"
        '
        'RadButton22
        '
        Me.RadButton22.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton22.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton22.Image = Global.TAS_TOL.My.Resources.Resources.Comic_Icons_3_files_edit
        Me.RadButton22.Location = New System.Drawing.Point(85, 69)
        Me.RadButton22.Name = "RadButton22"
        Me.RadButton22.Size = New System.Drawing.Size(298, 77)
        Me.RadButton22.TabIndex = 11
        Me.RadButton22.Tag = "17"
        Me.RadButton22.Text = "<html><span style=""font-size: 10px""><p>Loading Advise Note</p></span></html>"
        Me.RadButton22.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton22.ThemeName = "Breeze"
        '
        'RadButton32
        '
        Me.RadButton32.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RadButton32.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton32.Location = New System.Drawing.Point(5, 265)
        Me.RadButton32.Name = "RadButton32"
        Me.RadButton32.Size = New System.Drawing.Size(189, 55)
        Me.RadButton32.TabIndex = 9
        Me.RadButton32.Tag = "8"
        Me.RadButton32.Text = "<html><p><u>Cal.</u> Voulme for Esso </p><p>configuration</p></html>"
        Me.RadButton32.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.RadButton32.ThemeName = "Breeze"
        '
        'treeManu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(239, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1358, 694)
        Me.Controls.Add(Me.PDatabase)
        Me.Controls.Add(Me.PLaoding)
        Me.Controls.Add(Me.PMMI)
        Me.Controls.Add(Me.PREPORT)
        Me.Controls.Add(Me.PUNload)
        Me.Controls.Add(Me.RadPanel2)
        Me.Controls.Add(Me.RadGroupBox9)
        Me.Controls.Add(Me.RadStatusStrip1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "treeManu"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TAS Menu"
        Me.ThemeName = "Office2010Blue"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel2.ResumeLayout(False)
        CType(Me.RadButton5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox9.ResumeLayout(False)
        Me.RadGroupBox9.PerformLayout()
        CType(Me.RadClock1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel1.ResumeLayout(False)
        Me.RadPanel1.PerformLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IMCus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadCalendar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadStatusStrip1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PMMI, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PMMI.ResumeLayout(False)
        CType(Me.RadButton30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PDatabase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PDatabase.ResumeLayout(False)
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox4.ResumeLayout(False)
        CType(Me.RadButton17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox3.ResumeLayout(False)
        CType(Me.RadButton18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtUPMaster, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox2.ResumeLayout(False)
        CType(Me.RadButton12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        CType(Me.RadButton31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PREPORT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PREPORT.ResumeLayout(False)
        CType(Me.RadButton20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PUNload, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PUNload.ResumeLayout(False)
        CType(Me.RadButton24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PLaoding, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PLaoding.ResumeLayout(False)
        CType(Me.RadButton27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TrackBarDThumbShape1 As Telerik.WinControls.UI.TrackBarDThumbShape
    Friend WithEvents TrackBarUThumbShape1 As Telerik.WinControls.UI.TrackBarUThumbShape
    Friend WithEvents TabOffice12Shape1 As Telerik.WinControls.UI.TabOffice12Shape
    Friend WithEvents CustomShape1 As Telerik.WinControls.OldShapeEditor.CustomShape
    Friend WithEvents DonutShape1 As Telerik.WinControls.Tests.DonutShape
    Friend WithEvents MediaShape1 As Telerik.WinControls.Tests.MediaShape
    Friend WithEvents EllipseShape1 As Telerik.WinControls.EllipseShape
    Friend WithEvents OfficeShape1 As Telerik.WinControls.UI.OfficeShape
    Friend WithEvents QaShape1 As Telerik.WinControls.Tests.QAShape
    Friend WithEvents TabIEShape1 As Telerik.WinControls.UI.TabIEShape
    Friend WithEvents TabVsShape1 As Telerik.WinControls.UI.TabVsShape
    Friend WithEvents TrackBarLThumbShape1 As Telerik.WinControls.UI.TrackBarLThumbShape
    Friend WithEvents RadPanel2 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents PDatabase As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadGroupBox9 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents W_DATETIME As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents weight_Unit As System.Windows.Forms.Label
    Friend WithEvents weight As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents IMCus As System.Windows.Forms.PictureBox
    Friend WithEvents RadCalendar1 As Telerik.WinControls.UI.RadCalendar
    Friend WithEvents RadPanel1 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadButton1 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton2 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton3 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton4 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton5 As Telerik.WinControls.UI.RadButton
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents BreezeTheme1 As Telerik.WinControls.Themes.BreezeTheme
    Friend WithEvents RoundRectShape1 As Telerik.WinControls.RoundRectShape
    Friend WithEvents RadButton6 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton7 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton8 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton9 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton10 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton11 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton12 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton13 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton14 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton15 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton16 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton17 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton18 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton19 As Telerik.WinControls.UI.RadButton
    Friend WithEvents BtUPMaster As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton21 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadGroupBox4 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadGroupBox3 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadGroupBox2 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents PLaoding As Telerik.WinControls.UI.RadPanel
    Friend WithEvents PUNload As Telerik.WinControls.UI.RadPanel
    Friend WithEvents PMMI As Telerik.WinControls.UI.RadPanel
    Friend WithEvents PREPORT As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadButton22 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton23 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton24 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadStatusStrip1 As Telerik.WinControls.UI.RadStatusStrip
    Friend WithEvents RadLabelElement1 As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents M_NAME As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents RadImageButtonElement1 As Telerik.WinControls.UI.RadImageButtonElement
    Friend WithEvents RadLabelElement3 As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents M_GROUP As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents RadButton20 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton25 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton26 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadClock1 As Telerik.WinControls.UI.RadClock
    Friend WithEvents RadButton27 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton28 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton29 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton30 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton31 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadButton32 As Telerik.WinControls.UI.RadButton
End Class

