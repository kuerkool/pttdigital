﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Net
Imports TAS_TOL.C01_Permission
Imports TAS_TOL.corp.pttgc.schwebpart
Imports TAS_TOL.corp

Public Class Main
    Public vUserRight As New DataTable
    Public U_NAME As String = "None"
    Public U_GROUP As String = "None"
    Public U_NAME_ID As Integer
    Public U_GROUP_ID As Integer
    Public SALE_ORG As Integer
    Friend WithEvents ThrRefresh As BackgroundWorker
    Private CallDataBindToTextBox As New MethodInvoker(AddressOf Me.DataBindToTextBox)
    ' Private MyConnection As New SqlConnection(My.Settings.FPTConnectionString)
    '  Private MyDataSet As DataSet
    'Private MyDataAdapter As SqlDataAdapter
    'Private MyQueryString As String
    Private WEIGHT_DATA As Double
    Private WEIGHT_Date As String
    Private weightTemp As String
    Private TimeTemp, timeWi As Integer
    Declare Function LockWindowUpdate Lib "user32" _
(ByVal hWnd As Long) As Long
    Public Sub LockWindow(ByVal hWnd As Long, ByVal blnValue As Boolean)
        If blnValue Then
            LockWindowUpdate(hWnd)
        Else
            LockWindowUpdate(0)
        End If
    End Sub

    Private Sub FRefresh()
        ThrRefresh = New BackgroundWorker
        ThrRefresh.WorkerSupportsCancellation = True
        AddHandler ThrRefresh.DoWork, AddressOf RefreshData
        ThrRefresh.RunWorkerAsync()
    End Sub
    Private Sub RefreshData()
        Dim MyConnection As New SqlConnection(My.Settings.FPTConnectionString)
        MyConnection.Open()
        While True

            If ThrRefresh.CancellationPending Then
                ' StartConnect()
                '   ConnectTCP.Enabled = True
                '   ConnectTCP_Tick(sender, e)
                Exit While
            End If
            Try
                Dim MyDataSet = New DataSet()
                Dim MyQueryString As String
                Dim cmd As New SqlCommand
                Dim MyDataAdapter = New SqlDataAdapter
                cmd.Connection = MyConnection

                MyQueryString = " SELECT * FROM T_WEIGHT where W_ID= " & My.Settings.WIGHTidx
                cmd.CommandText = MyQueryString
                MyDataAdapter.SelectCommand = cmd
                MyDataAdapter.Fill(MyDataSet, "T_WEIGHT")

                MyQueryString = " SELECT * FROM T_SETTING"
                cmd.CommandText = MyQueryString
                MyDataAdapter.SelectCommand = cmd
                MyDataAdapter.Fill(MyDataSet, "T_SETTING")

                WEIGHT_DATA = MyDataSet.Tables("T_WEIGHT").Rows(0).Item("W_WEIGHT").ToString()
                timeWi = MyDataSet.Tables("T_SETTING").Rows(0)("TIME_WIGHT").ToString()
                Dim Datetime As New DateTimePicker
                Datetime.Value = MyDataSet.Tables("T_WEIGHT").Rows(0).Item("W_DATETIME")
                WEIGHT_Date = Datetime.Value.Day & "/" & Datetime.Value.Month & "/" & Datetime.Value.Year &
                " " & Datetime.Value.Hour & ":" & Datetime.Value.Minute & ":" & Datetime.Value.Second
                Me.BeginInvoke(CallDataBindToTextBox)

                Thread.Sleep(1000)
            Catch ex As Exception
                MyConnection.Close()
                '     MessageBox.Show(ex.Message)
            End Try
        End While
    End Sub

    Public Sub DataBindToTextBox()
        ' Dim MyConnection As New SqlConnection(My.Settings.FPTConnectionString)
        Try
            If Now.TimeOfDay.Hours = 1 And Now.TimeOfDay.Minutes = 1 And Now.TimeOfDay.Seconds = 1 Then
                FSAP01.StartMaster()
            End If
            ' date And Time

            ' M_DATE.Text = Date.Now.Day.ToString("00") + "/" + Date.Now.Month.ToString("00") + "/" + Date.Now.Year.ToString("00")
            ' M_TIME.Text = Date.Now.Hour.ToString("00") + ":" + Date.Now.Minute.ToString("00") + ":" + Date.Now.Second.ToString("00")
            '  treeManu.M_DATE.Text = Date.Now.Day.ToString("00") + "/" + Date.Now.Month.ToString("00") + "/" + Date.Now.Year.ToString("00")
            ' treeManu.M_TIME.Text = Date.Now.Hour.ToString("00") + ":" + Date.Now.Minute.ToString("00") + ":" + Date.Now.Second.ToString("00")
            f04_01_Bay.M_DATE.Text = Date.Now.Day.ToString("00") + "/" + Date.Now.Month.ToString("00") + "/" + Date.Now.Year.ToString("00")
            f04_01_Bay.M_TIME.Text = Date.Now.Hour.ToString("00") + ":" + Date.Now.Minute.ToString("00") + ":" + Date.Now.Second.ToString("00")
            BayOverview.M_DATE.Text = Date.Now.Day.ToString("00") + "/" + Date.Now.Month.ToString("00") + "/" + Date.Now.Year.ToString("00")
            BayOverview.M_TIME.Text = Date.Now.Hour.ToString("00") + ":" + Date.Now.Minute.ToString("00") + ":" + Date.Now.Second.ToString("00")
            Bay.M_DATE.Text = Date.Now.Day.ToString("00") + "/" + Date.Now.Month.ToString("00") + "/" + Date.Now.Year.ToString("00")
            Bay.M_TIME.Text = Date.Now.Hour.ToString("00") + ":" + Date.Now.Minute.ToString("00") + ":" + Date.Now.Second.ToString("00")

            AmmoniaUnload.M_DATE.Text = Date.Now.Day.ToString("00") + "/" + Date.Now.Month.ToString("00") + "/" + Date.Now.Year.ToString("00")
            AmmoniaUnload.M_TIME.Text = Date.Now.Hour.ToString("00") + ":" + Date.Now.Minute.ToString("00") + ":" + Date.Now.Second.ToString("00")


            ' Username and Group 
            M_NAME.Text = U_NAME
            M_GROUP.Text = U_GROUP
            treeManu.M_NAME.Text = U_NAME
            treeManu.M_GROUP.Text = U_GROUP
            f04_01_Bay.M_NAME.Text = U_NAME
            f04_01_Bay.M_GROUP.Text = U_GROUP
            BayOverview.M_NAME.Text = U_NAME
            BayOverview.M_GROUP.Text = U_GROUP
            Bay.M_NAME.Text = U_NAME
            Bay.M_GROUP.Text = U_GROUP

            AmmoniaUnload.M_NAME.Text = U_NAME
            AmmoniaUnload.M_GROUP.Text = U_GROUP

            '''' weight''''''

            '''' weight''''''
            If WEIGHT_DATA > 0 Or weight.Text.Replace(",", "") > 0 Then
                weight.Text = WEIGHT_DATA.ToString("#,00")
                treeManu.weight.Text = weight.Text
                f04_01_Bay.weight.Text = weight.Text
                BayLoading.weight.Text = weight.Text
                BayOverview.weight.Text = weight.Text
                AmmoniaUnload.weight.Text = weight.Text
                f05_01_Advisenote.WeightScal.Text = weight.Text.Replace(",", "")
                f05_02_Advisenote2.WeightScal.Text = weight.Text.Replace(",", "")
                Unloading.WeightScal.Text = weight.Text.Replace(",", "")
            End If
            W_DATETIME.Text = WEIGHT_Date
            treeManu.W_DATETIME.Text = WEIGHT_Date
            f04_01_Bay.W_DATETIME.Text = WEIGHT_Date
            BayLoading.W_DATETIME.Text = WEIGHT_Date
            BayOverview.W_DATETIME.Text = WEIGHT_Date
            AmmoniaUnload.W_DATETIME.Text = WEIGHT_Date
            'weight.Text = WEIGHT_DATA.ToString("#,000")
            'treeManu.weight.Text = weight.Text
            'Bay.weight.Text = weight.Text
            'BayOverview.weight.Text = weight.Text

            'Advisenote.WeightScal.Text = weight.Text.Replace(",", "")
            'Advisenote2.WeightScal.Text = weight.Text.Replace(",", "")
            'Unloading.WeightScal.Text = weight.Text.Replace(",", "")
            'W_DATETIME.Text = WEIGHT_Date
            'treeManu.W_DATETIME.Text = WEIGHT_Date
            'Bay.W_DATETIME.Text = WEIGHT_Date
            'BayOverview.W_DATETIME.Text = WEIGHT_Date


            'Dim MyQueryString As String = " SELECT * FROM T_SETTING"
            'Dim MyDatatable As New DataTable
            'Dim da As SqlDataAdapter
            'MyConnection.Open()
            'Dim cmd As New SqlCommand(MyQueryString, MyConnection)
            'da = New SqlDataAdapter(cmd)
            'da.Fill(MyDatatable)


            'Dim timeWi As Integer = MyDatatable.Rows(0)("TIME_WIGHT").ToString()
            ' Time Weight
            If weight.Text <> weightTemp Then
                weightTemp = weight.Text
                f05_01_Advisenote.WeightIn.Enabled = False
                f05_01_Advisenote.WeightOut.Enabled = False
                f05_02_Advisenote2.WeightIn.Enabled = False
                f05_02_Advisenote2.WeightOut.Enabled = False
                Unloading.BTWeightIn.Enabled = False
                Unloading.BTWeightOut.Enabled = False
                TimeTemp = 0
                'If TimeTemp < timeWi Then
                '    weightTemp = weight.Text
                '    f05_01_Advisenote.WeightIn.Enabled = False
                '    f05_01_Advisenote.WeightOut.Enabled = False
                '    f05_02_Advisenote2.WeightIn.Enabled = False
                '    f05_02_Advisenote2.WeightOut.Enabled = False
                '    Unloading.BTWeightIn.Enabled = False
                '    Unloading.BTWeightOut.Enabled = False
                '    If TimeTemp > timeWi Then TimeTemp = 1
                'End If
            ElseIf TimeTemp > timeWi Then
                f05_01_Advisenote.WeightIn.Enabled = True
                f05_01_Advisenote.WeightOut.Enabled = True
                f05_02_Advisenote2.WeightIn.Enabled = True
                f05_02_Advisenote2.WeightOut.Enabled = True
                Unloading.BTWeightIn.Enabled = True
                Unloading.BTWeightOut.Enabled = True
                TimeTemp = timeWi
            End If
            TimeTemp += 1
            '  MyConnection.Close()
        Catch ex As Exception
            '  MyConnection.Close()
        End Try

    End Sub


    Private Sub BLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BLogout.Click
        BTas.Enabled = False
        BLogout.Visible = False
        BLogin.Visible = True
        U_NAME = "None"
        U_GROUP = "None"
        U_NAME_ID = -1
        U_GROUP_ID = -1
        treeManu.Close()
        Me.RemoveOwnedForm(treeManu)
        vUserRight.Dispose()
        ' treeManu.Show()
    End Sub

    Private Sub BLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BLogin.Click
        f02_Login.UserName.Text = ""
        f02_Login.UserName.Focus()
        f02_Login.Pass.Text = ""
        f02_Login.UserName.Focus()
        f05_01_Advisenote.loginId = 0
        f05_02_Advisenote2.loginId = 0
        f02_Login.ShowDialog()

        vUserRight = New DataTable
        vUserRight = GetUserRight(U_NAME_ID)
        'BTas.Enabled = False
        'BLogout.Visible = True
        'BLogin.Visible = False
    End Sub

    Private Sub BTas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTas.Click
        Try
            If vUserRight.Compute("Max(chkview)", "FormID=24").ToString <> "True" Then
                MessageBox.Show("Access Denied", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show("Access Denied", "Permission [" & ex.Message & "]", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try


        Me.AddOwnedForm(treeManu)
        treeManu.Show()
        treeManu.BringToFront()
    End Sub

    Private Sub BtClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtClose.Click
        Me.Close()
    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.IMCus.Image = New System.Drawing.Bitmap("Company.png")
        Me.IMBackg.BackgroundImage = New System.Drawing.Bitmap("PTT.png")

        Try
            ''''''''''''Default S Sales ORG''''''''''''''''''''''''''
            ' 

            Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
            ' SALE_ORG = objIniFile.GetInteger("Advisenote", "saleorg", 1500)

            Dim Constring As String = objIniFile.GetString("Database", "ConString", "Data Source=.;Initial Catalog=TAS;Persist Security Info=True;User ID=sa;Password=P@$$w0rd")
            My.Settings.SetUserOverride("FPTConnectionString", Constring)
            ' My.Settings.SetUserOverride("FPTConnectionString", Constring)

            '      Dim netCred As NetworkCredential = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            '      Dim uri As Uri = New Uri(objIniFile.GetString("Webservice", "URL01", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING01"))
            '  Dim credentials As ICredentials = netCred.GetCredential(uri, "Basic")
            '      My.WebServices.MI_TASI01Service.Url = uri.AbsoluteUri

            ' My.Settings.TAS_SAP01_MI_TASI01Service = uri.AbsoluteUri
            '      MessageBox.Show(My.Settings.TAS_SAP01_MI_TASI01Service)
            '  My.WebServices.MI_TASI01Service.Credentials = credentials
            '  My.WebServices.MI_TASI01Service.PreAuthenticate = True
            '  My.WebServices.MI_TASI01Service.Credentials = credentials


            '       netCred = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            '       uri = New Uri(objIniFile.GetString("Webservice", "URL02", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING02"))
            ' credentials = netCred.GetCredential(uri, "Basic")
            '      My.WebServices.MI_TASI02Service.Url = uri.AbsoluteUri
            ' My.WebServices.MI_TASI02Service.Credentials = credentials
            ' My.WebServices.MI_TASI02Service.PreAuthenticate = True
            ' My.WebServices.MI_TASI02Service.Credentials = credentials
            '
            '        netCred = New NetworkCredential(objIniFile.GetString("Webservice", "user", "TAS01"), objIniFile.GetString("Webservice", "password", "initial1"))
            '       uri = New Uri(objIniFile.GetString("Webservice", "URL03", "http://schariba17:50000/XISOAPAdapter/MessageServlet?channel=:BS_TAS_TRUCK_LOADING:CC_TAS_TRUCK_LOADING03"))
            ' credentials = netCred.GetCredential(uri, "Basic")
            '       My.WebServices.MI_TASI03Service.Url = uri.AbsoluteUri
            ' My.WebServices.MI_TASI03Service.Credentials = credentials
            ' My.WebServices.MI_TASI03Service.PreAuthenticate = True
            '  My.WebServices.MI_TASI03Service.Credentials = credentials

            '''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception

        End Try
        'End Default ShipMent and Sales ORG
        FRefresh()
        BackgroundWorker1.RunWorkerAsync()
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function
    Public Sub CEvent(ByVal Detail As String)
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            If Detail <> "" Then
                Dim q As String
                q = ""
                q = "INSERT INTO T_EVENT (DETAIL)  VALUES "
                q &= "('" + (Detail) + "')"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                Dim ds As New DataSet()
                Dim dt As New DataTable
                Try
                    da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                    da.InsertCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MessageBox.Show("Unable to add information, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
            End If
            conn.Close()
        Catch ex As Exception
            conn.Close()
        End Try
    End Sub

    Private Sub Main_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ThrRefresh.CancelAsync()
        BackgroundWorker1.CancelAsync()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MessageBox.Show("Panot Leecharoenpak" + Chr(9) + ": 0888646466" + Chr(13) + "Banyat Prasong" + Chr(9) + Chr(9) + ": 0867350826" + Chr(13) + Chr(13) + "INFOLDER Co.,Ltd." + Chr(13) + "http://www.infolder.co.th", "CONTACT No.", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        While Not BackgroundWorker1.CancellationPending
            Try
                GETTEMP()
            Catch ex As Exception

            End Try

            Thread.Sleep(60000)
        End While

       
    End Sub
    Private Sub GETTEMP()
        Try
            Dim conn As SqlClient.SqlConnection = GetConnection()
            Dim cmd As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet

            conn.Open()
            cmd.Connection = conn


            cmd.CommandText = "select * from T_LOCATION where PI_TEMPTAG<>''  and PI_TEMPTAG is not null "
            da.SelectCommand = cmd
            da.Fill(ds, "T_LOCATION")

            Dim wservice As New pttgc.schwebpart.PIGetData
            For i As Integer = 0 To ds.Tables("T_LOCATION").Rows.Count - 1
                Try
                    ''Dim dt As DataTable = wservice.GetCurrentValue("TOL-3100TI143.PV", "piserver", "ictviewer", "ictviewer")
                    Dim dt As DataTable = wservice.GetCurrentValue(ds.Tables("T_LOCATION").Rows(i)("PI_TEMPTAG").ToString, My.Settings.PI_SERVER, My.Settings.PI_USER, My.Settings.PI_PASSWORD)

                    cmd.CommandText = "update T_LOCATION set PI_TEMP='" & dt.Rows(0)(1) & "' Where ID='" & ds.Tables("T_LOCATION").Rows(i)("ID").ToString & "'"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "update T_BATCHMETER set TANK_TEMP='" & dt.Rows(0)(1) & "' Where TANK_NO='" & ds.Tables("T_LOCATION").Rows(i)("CODE").ToString & "'"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "update T_BATCHMETER set BATCH_TEMP1=TANK_TEMP where ID=ID and PI_STATUS=1"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "update T_BATCHMETER set BATCH_TEMP1=MANUAL_TEMP where ID=ID and PI_STATUS=0"
                    cmd.ExecuteNonQuery()

                    dt.Dispose()
                Catch ex As Exception

                End Try

            Next
            wservice.Dispose()
            conn.Close()
            conn.Dispose()
            cmd.Dispose()
            da.Dispose()
            ds.Dispose()



        Catch ex As Exception

        End Try

    End Sub
End Class