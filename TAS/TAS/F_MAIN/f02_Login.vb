﻿Public Class f02_Login
    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Inumrow As Integer
        If f05_01_Advisenote.loginId = 1 Or f05_02_Advisenote2.loginId = 1 Then
            Try
                Dim conn As SqlClient.SqlConnection = GetConnection()
                conn.Open()
                Dim sql As String = "Select * from V_USER  where U_NAME='" + UserName.Text + "'" + _
                                    " and U_PASSWD Collate Latin1_General_cs_as='" + Pass.Text + "'" + _
                                    " and U_STATUS ='10' and G_id >=3"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                Dim ds As New DataSet()
                Dim dt As New DataTable
                da.Fill(dt)
                Inumrow = ds.HasChanges()
                If (dt.Rows.Count > 0) Then
                    If (f05_01_Advisenote.LoginDo = 0) Then ' And (Advisenote.loginId = 1) Then
                        f05_01_Advisenote.UpdateWeightIn.Enabled = True
                        f05_01_Advisenote.UpdateWeightOut.Enabled = True
                        f05_01_Advisenote.LawWeightIn.Enabled = True
                        f05_01_Advisenote.CBLoadingPoint.Enabled = True


                        Unloading.UpdateWeightIn.Enabled = True
                        Unloading.UpdateWeightOut.Enabled = True
                        f05_01_Advisenote.loginId = 2
                        Me.Close()
                    End If
                    If (f05_02_Advisenote2.LoginDo = 0) Then ' And (Advisenote.loginId = 1) Then
                        f05_02_Advisenote2.UpdateWeightIn.Enabled = True
                        f05_02_Advisenote2.UpdateWeightOut.Enabled = True
                        f05_02_Advisenote2.LawWeightIn.Enabled = True
                        f05_02_Advisenote2.CBLoadingPoint.Enabled = True

                        'Unloading2.UpdateWeightIn.Enabled = True
                        'Unloading2.UpdateWeightOut.Enabled = True
                        f05_02_Advisenote2.loginId = 2
                        Me.Close()
                    End If
                    If (f05_01_Advisenote.LoginDo = 1) Then
                        f05_01_Advisenote.EDDONo.Enabled = True
                        f05_01_Advisenote.EDDO_item.Enabled = True
                        f05_01_Advisenote.Sono.Enabled = True
                        f05_01_Advisenote.Soitem.Enabled = True
                        f05_01_Advisenote.EDCustomer.Enabled = True
                        f05_01_Advisenote.EDShipper.Enabled = True
                        f05_01_Advisenote.LoginDo = 0
                        Me.Close()
                    End If
                    If (f05_02_Advisenote2.LoginDo = 1) Then
                        f05_02_Advisenote2.EDDONo.Enabled = True
                        f05_02_Advisenote2.EDDO_item.Enabled = True
                        f05_02_Advisenote2.Sono.Enabled = True
                        f05_02_Advisenote2.Soitem.Enabled = True
                        f05_02_Advisenote2.EDCustomer.Enabled = True
                        f05_02_Advisenote2.EDShipper.Enabled = True
                        f05_02_Advisenote2.LoginDo = 0
                        Me.Close()
                    End If
                Else

                    MessageBox.Show("User And Password Fail", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                End If






            Finally
            End Try
        Else

            Try
                Dim conn As SqlClient.SqlConnection = GetConnection()
                conn.Open()
                Dim sql As String = "Select * from V_USER  where U_NAME='" + UserName.Text + "'" + _
                                    " and U_PASSWD Collate Latin1_General_cs_as='" + Pass.Text + "'" + _
                                    " and U_STATUS ='10'"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                Dim ds As New DataSet()
                Dim dt As New DataTable
                da.Fill(dt)
                Inumrow = ds.HasChanges()
                If dt.Rows.Count > 0 Then
                    Main.BTas.Enabled = True
                    Main.BLogin.Visible = False
                    Main.BLogout.Visible = True
                    Main.U_NAME = dt.Rows(0).Item("U_NAME").ToString
                    Main.U_GROUP = dt.Rows(0).Item("G_NAME").ToString
                    Main.U_NAME_ID = dt.Rows(0).Item("U_ID").ToString
                    Main.U_GROUP_ID = dt.Rows(0).Item("G_id").ToString

                    If Main.U_GROUP_ID >= 3 Then
                        f05_01_Advisenote.WeightScal.ReadOnly = False
                        f05_02_Advisenote2.WeightScal.ReadOnly = False
                        f05_03_Advisenote3.WeightScal.ReadOnly = False
                        Unloading.WeightScal.ReadOnly = False
                    Else
                        f05_01_Advisenote.WeightScal.ReadOnly = True
                        f05_02_Advisenote2.WeightScal.ReadOnly = True
                        f05_03_Advisenote3.WeightScal.ReadOnly = True
                        Unloading.WeightScal.ReadOnly = True
                    End If

                    Me.Close()
                Else
                    MessageBox.Show("User And Password Fail", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Main.BTas.Enabled = False
                    Main.BLogin.Visible = False
                    Main.BLogin.Visible = True
                End If
            Finally
            End Try
        End If

    End Sub

    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UserName.Text = ""
        UserName.Focus()
        Pass.Text = ""
        Main.BTas.Enabled = False
        Main.BLogin.Visible = False
        Main.BLogin.Visible = True
    End Sub

    Private Sub Pass_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Pass.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Button1_Click(sender, e)
            End If
        Finally
        End Try
    End Sub

    Private Sub Login_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        UserName.Focus()
    End Sub
End Class