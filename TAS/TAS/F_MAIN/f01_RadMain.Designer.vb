﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.RadStatusStrip1 = New Telerik.WinControls.UI.RadStatusStrip()
        Me.RadLabelElement1 = New Telerik.WinControls.UI.RadLabelElement()
        Me.M_NAME = New Telerik.WinControls.UI.RadLabelElement()
        Me.RadImageButtonElement1 = New Telerik.WinControls.UI.RadImageButtonElement()
        Me.RadLabelElement3 = New Telerik.WinControls.UI.RadLabelElement()
        Me.M_GROUP = New Telerik.WinControls.UI.RadLabelElement()
        Me.RadPanel1 = New Telerik.WinControls.UI.RadPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BLogin = New Telerik.WinControls.UI.RadButton()
        Me.RoundRectShape1 = New Telerik.WinControls.RoundRectShape(Me.components)
        Me.BTas = New Telerik.WinControls.UI.RadButton()
        Me.BLogout = New Telerik.WinControls.UI.RadButton()
        Me.BtClose = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox9 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadClock1 = New Telerik.WinControls.UI.RadClock()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadPanel2 = New Telerik.WinControls.UI.RadPanel()
        Me.W_DATETIME = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.weight = New System.Windows.Forms.Label()
        Me.weight_Unit = New System.Windows.Forms.Label()
        Me.IMCus = New System.Windows.Forms.PictureBox()
        Me.RadCalendar1 = New Telerik.WinControls.UI.RadCalendar()
        Me.IMBackg = New System.Windows.Forms.PictureBox()
        Me.Office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
        Me.BreezeTheme1 = New Telerik.WinControls.Themes.BreezeTheme()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        CType(Me.RadStatusStrip1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel1.SuspendLayout()
        CType(Me.BLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BTas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BLogout, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox9.SuspendLayout()
        CType(Me.RadClock1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel2.SuspendLayout()
        CType(Me.IMCus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadCalendar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IMBackg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadStatusStrip1
        '
        Me.RadStatusStrip1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.RadLabelElement1, Me.M_NAME, Me.RadImageButtonElement1, Me.RadLabelElement3, Me.M_GROUP})
        Me.RadStatusStrip1.Location = New System.Drawing.Point(0, 673)
        Me.RadStatusStrip1.Name = "RadStatusStrip1"
        Me.RadStatusStrip1.Size = New System.Drawing.Size(1356, 22)
        Me.RadStatusStrip1.TabIndex = 25
        Me.RadStatusStrip1.Text = "RadStatusStrip1"
        Me.RadStatusStrip1.ThemeName = "Office2010Blue"
        '
        'RadLabelElement1
        '
        Me.RadLabelElement1.AccessibleDescription = "User:"
        Me.RadLabelElement1.AccessibleName = "User:"
        Me.RadLabelElement1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabelElement1.Name = "RadLabelElement1"
        Me.RadStatusStrip1.SetSpring(Me.RadLabelElement1, False)
        Me.RadLabelElement1.Text = "User:"
        Me.RadLabelElement1.TextWrap = True
        Me.RadLabelElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'M_NAME
        '
        Me.M_NAME.AccessibleDescription = "Banyat Prasong"
        Me.M_NAME.AccessibleName = "Banyat Prasong"
        Me.M_NAME.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_NAME.Name = "M_NAME"
        Me.RadStatusStrip1.SetSpring(Me.M_NAME, False)
        Me.M_NAME.Text = "-"
        Me.M_NAME.TextWrap = True
        Me.M_NAME.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'RadImageButtonElement1
        '
        Me.RadImageButtonElement1.AccessibleDescription = "RadImageButtonElement1"
        Me.RadImageButtonElement1.AccessibleName = "RadImageButtonElement1"
        Me.RadImageButtonElement1.Name = "RadImageButtonElement1"
        Me.RadStatusStrip1.SetSpring(Me.RadImageButtonElement1, False)
        Me.RadImageButtonElement1.Text = "RadImageButtonElement1"
        Me.RadImageButtonElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'RadLabelElement3
        '
        Me.RadLabelElement3.AccessibleDescription = "Group: "
        Me.RadLabelElement3.AccessibleName = "Group: "
        Me.RadLabelElement3.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.RadLabelElement3.Name = "RadLabelElement3"
        Me.RadStatusStrip1.SetSpring(Me.RadLabelElement3, False)
        Me.RadLabelElement3.Text = "Group: "
        Me.RadLabelElement3.TextWrap = True
        Me.RadLabelElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'M_GROUP
        '
        Me.M_GROUP.AccessibleDescription = "Administrator"
        Me.M_GROUP.AccessibleName = "Administrator"
        Me.M_GROUP.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.M_GROUP.Name = "M_GROUP"
        Me.RadStatusStrip1.SetSpring(Me.M_GROUP, False)
        Me.M_GROUP.Text = ""
        Me.M_GROUP.TextWrap = True
        Me.M_GROUP.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'RadPanel1
        '
        Me.RadPanel1.Controls.Add(Me.Button1)
        Me.RadPanel1.Controls.Add(Me.BLogin)
        Me.RadPanel1.Controls.Add(Me.BTas)
        Me.RadPanel1.Controls.Add(Me.BLogout)
        Me.RadPanel1.Controls.Add(Me.BtClose)
        Me.RadPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.RadPanel1.Location = New System.Drawing.Point(0, 599)
        Me.RadPanel1.Name = "RadPanel1"
        Me.RadPanel1.Size = New System.Drawing.Size(1356, 74)
        Me.RadPanel1.TabIndex = 35
        Me.RadPanel1.ThemeName = "Breeze"
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(108, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button1.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Ifc2
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(1216, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 61)
        Me.Button1.TabIndex = 30
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BLogin
        '
        Me.BLogin.BackColor = System.Drawing.Color.Transparent
        Me.BLogin.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold)
        Me.BLogin.Image = Global.TAS_TOL.My.Resources.Resources.log
        Me.BLogin.Location = New System.Drawing.Point(6, 6)
        Me.BLogin.Name = "BLogin"
        Me.BLogin.Size = New System.Drawing.Size(134, 61)
        Me.BLogin.TabIndex = 26
        Me.BLogin.Text = "Login"
        Me.BLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BLogin.ThemeName = "Breeze"
        CType(Me.BLogin.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.log
        CType(Me.BLogin.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.BLogin.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "Login"
        CType(Me.BLogin.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Shape = Me.RoundRectShape1
        '
        'BTas
        '
        Me.BTas.BackColor = System.Drawing.Color.Transparent
        Me.BTas.Enabled = False
        Me.BTas.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold)
        Me.BTas.Image = Global.TAS_TOL.My.Resources.Resources.sdsd2
        Me.BTas.Location = New System.Drawing.Point(146, 6)
        Me.BTas.Name = "BTas"
        Me.BTas.Size = New System.Drawing.Size(530, 61)
        Me.BTas.TabIndex = 25
        Me.BTas.Text = "TERMINAL AUTOMATION SYSTEM (TAS)"
        Me.BTas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTas.ThemeName = "Breeze"
        CType(Me.BTas.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.sdsd2
        CType(Me.BTas.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.BTas.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "TERMINAL AUTOMATION SYSTEM (TAS)"
        CType(Me.BTas.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Shape = Me.RoundRectShape1
        CType(Me.BTas.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        '
        'BLogout
        '
        Me.BLogout.BackColor = System.Drawing.Color.Transparent
        Me.BLogout.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold)
        Me.BLogout.Image = Global.TAS_TOL.My.Resources.Resources.unlog
        Me.BLogout.Location = New System.Drawing.Point(6, 6)
        Me.BLogout.Name = "BLogout"
        Me.BLogout.Size = New System.Drawing.Size(134, 61)
        Me.BLogout.TabIndex = 24
        Me.BLogout.Text = "Logout"
        Me.BLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BLogout.ThemeName = "Breeze"
        CType(Me.BLogout.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources.unlog
        CType(Me.BLogout.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.BLogout.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "Logout"
        CType(Me.BLogout.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Shape = Me.RoundRectShape1
        '
        'BtClose
        '
        Me.BtClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtClose.BackColor = System.Drawing.Color.Transparent
        Me.BtClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtClose.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtClose.ForeColor = System.Drawing.Color.White
        Me.BtClose.Image = Global.TAS_TOL.My.Resources.Resources._exit
        Me.BtClose.Location = New System.Drawing.Point(1097, 6)
        Me.BtClose.Name = "BtClose"
        Me.BtClose.Size = New System.Drawing.Size(113, 61)
        Me.BtClose.TabIndex = 23
        Me.BtClose.Text = "&Close"
        Me.BtClose.TextAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.BtClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.BtClose.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Image = Global.TAS_TOL.My.Resources.Resources._exit
        CType(Me.BtClose.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        CType(Me.BtClose.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).TextAlignment = System.Drawing.ContentAlignment.MiddleRight
        CType(Me.BtClose.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "&Close"
        CType(Me.BtClose.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Shape = Me.RoundRectShape1
        CType(Me.BtClose.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(121, Byte), Integer), CType(CType(121, Byte), Integer))
        CType(Me.BtClose.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        CType(Me.BtClose.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(53, Byte), Integer))
        CType(Me.BtClose.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.BtClose.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(163, Byte), Integer))
        CType(Me.BtClose.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.ImagePrimitive).PositionOffset = New System.Drawing.SizeF(2.0!, 0.0!)
        CType(Me.BtClose.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.Color.White
        CType(Me.BtClose.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleRight
        CType(Me.BtClose.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).PositionOffset = New System.Drawing.SizeF(-10.0!, 0.0!)
        CType(Me.BtClose.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        CType(Me.BtClose.GetChildAt(0).GetChildAt(3), Telerik.WinControls.Primitives.FocusPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.BtClose.GetChildAt(0).GetChildAt(3), Telerik.WinControls.Primitives.FocusPrimitive).ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(46, Byte), Integer))
        CType(Me.BtClose.GetChildAt(0).GetChildAt(3), Telerik.WinControls.Primitives.FocusPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(174, Byte), Integer), CType(CType(46, Byte), Integer))
        '
        'RadGroupBox9
        '
        Me.RadGroupBox9.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox9.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RadGroupBox9.Controls.Add(Me.RadClock1)
        Me.RadGroupBox9.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox9.Controls.Add(Me.RadPanel2)
        Me.RadGroupBox9.Controls.Add(Me.IMCus)
        Me.RadGroupBox9.Controls.Add(Me.RadCalendar1)
        Me.RadGroupBox9.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadGroupBox9.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office
        Me.RadGroupBox9.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox9.HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Bottom
        Me.RadGroupBox9.HeaderText = ""
        Me.RadGroupBox9.Location = New System.Drawing.Point(0, 0)
        Me.RadGroupBox9.Name = "RadGroupBox9"
        Me.RadGroupBox9.Size = New System.Drawing.Size(1356, 147)
        Me.RadGroupBox9.TabIndex = 36
        CType(Me.RadGroupBox9.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox9.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).InvalidateMeasureInMainLayout = 1
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(215, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor3 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor4 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor3 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor4 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.GroupBoxHeader).HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Bottom
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.GroupBoxHeader).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
        '
        'RadClock1
        '
        Me.RadClock1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadClock1.Location = New System.Drawing.Point(1034, 6)
        Me.RadClock1.Name = "RadClock1"
        Me.RadClock1.Size = New System.Drawing.Size(134, 135)
        Me.RadClock1.TabIndex = 38
        Me.RadClock1.Text = "RadClock1"
        Me.RadClock1.ThemeName = "Office2010Blue"
        '
        'RadLabel1
        '
        Me.RadLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.RadLabel1.BackColor = System.Drawing.Color.Transparent
        Me.RadLabel1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.ForeColor = System.Drawing.Color.White
        Me.RadLabel1.Location = New System.Drawing.Point(424, 18)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(509, 34)
        Me.RadLabel1.TabIndex = 31
        Me.RadLabel1.TabStop = True
        Me.RadLabel1.Text = "TERMINAL AUTOMATION SYSTEM (TAS)"
        Me.RadLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RadPanel2
        '
        Me.RadPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.RadPanel2.Controls.Add(Me.W_DATETIME)
        Me.RadPanel2.Controls.Add(Me.Label15)
        Me.RadPanel2.Controls.Add(Me.Label14)
        Me.RadPanel2.Controls.Add(Me.weight)
        Me.RadPanel2.Controls.Add(Me.weight_Unit)
        Me.RadPanel2.Location = New System.Drawing.Point(424, 58)
        Me.RadPanel2.Name = "RadPanel2"
        Me.RadPanel2.Size = New System.Drawing.Size(489, 66)
        Me.RadPanel2.TabIndex = 37
        Me.RadPanel2.ThemeName = "Breeze"
        CType(Me.RadPanel2.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        '
        'W_DATETIME
        '
        Me.W_DATETIME.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.W_DATETIME.AutoSize = True
        Me.W_DATETIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.W_DATETIME.ForeColor = System.Drawing.Color.White
        Me.W_DATETIME.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.W_DATETIME.Location = New System.Drawing.Point(75, 50)
        Me.W_DATETIME.Name = "W_DATETIME"
        Me.W_DATETIME.Size = New System.Drawing.Size(114, 13)
        Me.W_DATETIME.TabIndex = 32
        Me.W_DATETIME.Text = "dd/MM/yyyy hh:mm:ss"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(15, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(173, 52)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "WEIGHT"
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(22, 50)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(57, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Updated : "
        '
        'weight
        '
        Me.weight.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight.ForeColor = System.Drawing.Color.Lime
        Me.weight.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight.Location = New System.Drawing.Point(186, 6)
        Me.weight.Name = "weight"
        Me.weight.Size = New System.Drawing.Size(210, 52)
        Me.weight.TabIndex = 30
        Me.weight.Text = "0"
        Me.weight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'weight_Unit
        '
        Me.weight_Unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weight_Unit.AutoSize = True
        Me.weight_Unit.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight_Unit.ForeColor = System.Drawing.Color.Lime
        Me.weight_Unit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight_Unit.Location = New System.Drawing.Point(395, 6)
        Me.weight_Unit.Name = "weight_Unit"
        Me.weight_Unit.Size = New System.Drawing.Size(77, 52)
        Me.weight_Unit.TabIndex = 31
        Me.weight_Unit.Text = "KG"
        '
        'IMCus
        '
        Me.IMCus.BackColor = System.Drawing.Color.Transparent
        Me.IMCus.Image = Global.TAS_TOL.My.Resources.Resources.PTT2
        Me.IMCus.Location = New System.Drawing.Point(6, 3)
        Me.IMCus.Name = "IMCus"
        Me.IMCus.Size = New System.Drawing.Size(260, 133)
        Me.IMCus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.IMCus.TabIndex = 14
        Me.IMCus.TabStop = False
        '
        'RadCalendar1
        '
        Me.RadCalendar1.AllowSelect = False
        Me.RadCalendar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadCalendar1.Location = New System.Drawing.Point(1175, 3)
        Me.RadCalendar1.Name = "RadCalendar1"
        Me.RadCalendar1.ReadOnly = True
        Me.RadCalendar1.ShowHeader = False
        Me.RadCalendar1.ShowNavigationButtons = False
        Me.RadCalendar1.ShowViewHeader = True
        Me.RadCalendar1.Size = New System.Drawing.Size(176, 133)
        Me.RadCalendar1.TabIndex = 12
        Me.RadCalendar1.Text = "RadCalendar1"
        Me.RadCalendar1.ThemeName = "Office2010Blue"
        '
        'IMBackg
        '
        Me.IMBackg.BackColor = System.Drawing.Color.Transparent
        Me.IMBackg.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.PTT2
        Me.IMBackg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.IMBackg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.IMBackg.Location = New System.Drawing.Point(0, 147)
        Me.IMBackg.Name = "IMBackg"
        Me.IMBackg.Size = New System.Drawing.Size(1356, 452)
        Me.IMBackg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.IMBackg.TabIndex = 31
        Me.IMBackg.TabStop = False
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1356, 695)
        Me.Controls.Add(Me.IMBackg)
        Me.Controls.Add(Me.RadPanel1)
        Me.Controls.Add(Me.RadStatusStrip1)
        Me.Controls.Add(Me.RadGroupBox9)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Main"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.Text = "TERMINAL AUTOMATION SYSTEM (TAS)"
        Me.ThemeName = "Office2010Blue"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RadStatusStrip1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel1.ResumeLayout(False)
        CType(Me.BLogin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BTas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BLogout, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox9.ResumeLayout(False)
        Me.RadGroupBox9.PerformLayout()
        CType(Me.RadClock1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel2.ResumeLayout(False)
        Me.RadPanel2.PerformLayout()
        CType(Me.IMCus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadCalendar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IMBackg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RadStatusStrip1 As Telerik.WinControls.UI.RadStatusStrip
    Friend WithEvents RadLabelElement1 As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents M_NAME As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents RadImageButtonElement1 As Telerik.WinControls.UI.RadImageButtonElement
    Friend WithEvents RadLabelElement3 As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents M_GROUP As Telerik.WinControls.UI.RadLabelElement
    Friend WithEvents IMBackg As System.Windows.Forms.PictureBox
    Friend WithEvents RadPanel1 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadGroupBox9 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadPanel2 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents W_DATETIME As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents weight As System.Windows.Forms.Label
    Friend WithEvents weight_Unit As System.Windows.Forms.Label
    Friend WithEvents IMCus As System.Windows.Forms.PictureBox
    Friend WithEvents RadCalendar1 As Telerik.WinControls.UI.RadCalendar
    Friend WithEvents BtClose As Telerik.WinControls.UI.RadButton
    Friend WithEvents BLogout As Telerik.WinControls.UI.RadButton
    Friend WithEvents BTas As Telerik.WinControls.UI.RadButton
    Friend WithEvents BLogin As Telerik.WinControls.UI.RadButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
    Friend WithEvents BreezeTheme1 As Telerik.WinControls.Themes.BreezeTheme
    Friend WithEvents RoundRectShape1 As Telerik.WinControls.RoundRectShape
    Friend WithEvents RadClock1 As Telerik.WinControls.UI.RadClock
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class

