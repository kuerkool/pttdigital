﻿Imports TAS_TOL.C01_Permission
Public Class treeManu
    Public reportID As Int16
    Shared Sub RunCommandCom(ByVal command As String, ByVal arguments As String, ByVal permanent As Boolean)
        Dim p As Process = New Process()
        Dim pi As ProcessStartInfo = New ProcessStartInfo()
        pi.Arguments = " " + If(permanent = True, "/K", "/C") + " " + command + " " + arguments
        pi.FileName = "cmd.exe"
        p.StartInfo = pi
        p.Start()
    End Sub

    Private Sub RadButton6_Click(sender As System.Object, e As System.EventArgs) Handles RadButton6.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If

        Me.RemoveOwnedForm(f04_01_CustomerCompany)
        Me.AddOwnedForm(f04_01_CustomerCompany)
        f04_01_CustomerCompany.Show()
        f04_01_CustomerCompany.BringToFront()
    End Sub

    Private Sub RadButton7_Click(sender As System.Object, e As System.EventArgs) Handles RadButton7.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_TransportationCompany)
        Me.AddOwnedForm(f04_01_TransportationCompany)
        f04_01_TransportationCompany.Show()
        f04_01_TransportationCompany.BringToFront()
    End Sub

    Private Sub RadButton8_Click(sender As System.Object, e As System.EventArgs) Handles RadButton8.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(Driver)
        Me.AddOwnedForm(Driver)
        Driver.Show()
        Driver.BringToFront()
    End Sub

    Private Sub RadButton9_Click(sender As System.Object, e As System.EventArgs) Handles RadButton9.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_Truck)
        Me.AddOwnedForm(f04_01_Truck)
        f04_01_Truck.Show()
        f04_01_Truck.BringToFront()
    End Sub

    Private Sub RadButton10_Click(sender As System.Object, e As System.EventArgs) Handles RadButton10.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(Fpacking)
        Me.AddOwnedForm(Fpacking)
        Fpacking.Show()
        Fpacking.BringToFront()
    End Sub

    Private Sub RadButton12_Click(sender As System.Object, e As System.EventArgs) Handles RadButton12.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_Product)
        Me.AddOwnedForm(f04_01_Product)
        f04_01_Product.Show()
        f04_01_Product.BringToFront()
    End Sub

    Private Sub RadButton13_Click(sender As System.Object, e As System.EventArgs) Handles RadButton13.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_BatchMeter)
        Me.AddOwnedForm(f04_01_BatchMeter)
        f04_01_BatchMeter.Show()
        f04_01_BatchMeter.BringToFront()
    End Sub

    Private Sub RadButton14_Click(sender As System.Object, e As System.EventArgs) Handles RadButton14.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_Island)
        Me.AddOwnedForm(f04_01_Island)
        f04_01_Island.Show()
        f04_01_Island.BringToFront()
    End Sub

    Private Sub RadButton15_Click(sender As System.Object, e As System.EventArgs) Handles RadButton15.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_UserAccount)
        Me.AddOwnedForm(f04_01_UserAccount)
        f04_01_UserAccount.Show()
        f04_01_UserAccount.BringToFront()
    End Sub

    Private Sub RadButton16_Click(sender As System.Object, e As System.EventArgs) Handles RadButton16.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_ShipmentType)
        Me.AddOwnedForm(f04_01_ShipmentType)
        f04_01_ShipmentType.Show()
        f04_01_ShipmentType.BringToFront()
    End Sub

    Private Sub RadButton21_Click(sender As System.Object, e As System.EventArgs) Handles RadButton21.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(Doitems)
        Me.AddOwnedForm(Doitems)
        Doitems.TDOBindingSource.Filter = ""
        Doitems.Show()
        Doitems.BringToFront()
    End Sub

    Private Sub RadButton17_Click(sender As System.Object, e As System.EventArgs) Handles RadButton17.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(FRLocation)
        Me.AddOwnedForm(FRLocation)
        FRLocation.Show()
        FRLocation.BringToFront()
    End Sub

    Private Sub RadButton18_Click(sender As System.Object, e As System.EventArgs) Handles RadButton18.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_FSetting)
        Me.AddOwnedForm(f04_01_FSetting)
        f04_01_FSetting.Show()
        f04_01_FSetting.BringToFront()
    End Sub

    Private Sub RadButton19_Click(sender As System.Object, e As System.EventArgs) Handles RadButton19.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_SettingPlant)
        Me.AddOwnedForm(f04_01_SettingPlant)
        f04_01_SettingPlant.Show()
        f04_01_SettingPlant.BringToFront()
    End Sub

    Private Sub RadButton20_Click(sender As System.Object, e As System.EventArgs) Handles BtUPMaster.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_SettingPlant)
        Me.AddOwnedForm(f04_01_SettingPlant)
        FSAP01.ShowDialog()


    End Sub

    Private Sub RadButton11_Click(sender As System.Object, e As System.EventArgs) Handles RadButton11.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_Shipper)
        Me.AddOwnedForm(f04_01_Shipper)
        f04_01_Shipper.Show()
        f04_01_Shipper.BringToFront()
    End Sub

    Private Sub RadButton1_Click(sender As System.Object, e As System.EventArgs) Handles RadButton1.Click
        PDatabase.BringToFront()
    End Sub

    Private Sub RadButton2_Click(sender As System.Object, e As System.EventArgs) Handles RadButton2.Click
        PLaoding.BringToFront()
    End Sub

    Private Sub RadButton3_Click(sender As System.Object, e As System.EventArgs) Handles RadButton3.Click
        PUNload.BringToFront()
    End Sub

    Private Sub RadButton4_Click(sender As System.Object, e As System.EventArgs) Handles RadButton4.Click
        PMMI.BringToFront()
    End Sub

    Private Sub RadButton5_Click(sender As System.Object, e As System.EventArgs) Handles RadButton5.Click
        PREPORT.BringToFront()
    End Sub

    Private Sub TASMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.IMCus.Image = New System.Drawing.Bitmap("Company.png")
        Me.PDatabase.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        Me.PLaoding.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        Me.PUNload.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        Me.PMMI.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        Me.PREPORT.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        PDatabase.BringToFront()
    End Sub

    Private Sub RadButton22_Click(sender As System.Object, e As System.EventArgs) Handles RadButton22.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f05_01_Advisenote)
        Me.AddOwnedForm(f05_01_Advisenote)
        f05_01_Advisenote.Show()
        f05_01_Advisenote.BringToFront()

    End Sub

    Private Sub RadButton23_Click(sender As System.Object, e As System.EventArgs) Handles RadButton23.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f05_02_Advisenote2)
        Me.AddOwnedForm(f05_02_Advisenote2)
        f05_02_Advisenote2.Show()
        f05_02_Advisenote2.BringToFront()
    End Sub

    Private Sub RadButton24_Click(sender As System.Object, e As System.EventArgs) Handles RadButton24.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.AddOwnedForm(Unloading)
        Unloading.Show()
        Unloading.BringToFront()
    End Sub

    Private Sub RadButton20_Click_1(sender As System.Object, e As System.EventArgs) Handles RadButton20.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.AddOwnedForm(Report)
        Report.Show()
        Report.BringToFront()
    End Sub

    Private Sub RadButton25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadButton25.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.AddOwnedForm(BayLoading)
        BayLoading.Show()
        BayLoading.BringToFront()
    End Sub

    Private Sub RadButton26_Click(sender As System.Object, e As System.EventArgs) Handles RadButton26.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f05_03_Advisenote3)
        Me.AddOwnedForm(f05_03_Advisenote3)
        f05_03_Advisenote3.Show()
        f05_03_Advisenote3.BringToFront()
    End Sub


    Private Sub RadButton27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadButton27.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        RunCommandCom("taskkill /IM Weight Scale interface for WMS.exe", "/f", False)
        Process.Start(My.Settings.WMSApp)
    End Sub


    Private Sub RadButton29_Click(sender As System.Object, e As System.EventArgs) Handles RadButton29.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.AddOwnedForm(BayOverview)
        BayOverview.Show()
        BayOverview.BringToFront()
    End Sub

    Private Sub RadButton28_Click(sender As System.Object, e As System.EventArgs) Handles RadButton28.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.AddOwnedForm(Bay)
        Bay.Show()
        Bay.BringToFront()
    End Sub

    Private Sub RadButton30_Click(sender As System.Object, e As System.EventArgs) Handles RadButton30.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.AddOwnedForm(AmmoniaUnload)
        AmmoniaUnload.Show()
        AmmoniaUnload.BringToFront()
    End Sub

    Private Sub RadButton31_Click(sender As System.Object, e As System.EventArgs) Handles RadButton31.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(f04_01_BADGE)
        Me.AddOwnedForm(f04_01_BADGE)
        f04_01_BADGE.Show()
        f04_01_BADGE.BringToFront()
    End Sub

    Private Sub RadButton32_Click(sender As System.Object, e As System.EventArgs) Handles RadButton32.Click
        If Not CheckViewPermisstion(sender.tag) Then
            Exit Sub
        End If
        Me.RemoveOwnedForm(F04_DENLOAD)
        Me.AddOwnedForm(F04_DENLOAD)
        F04_DENLOAD.Show()
        F04_DENLOAD.BringToFront()
    End Sub
End Class
