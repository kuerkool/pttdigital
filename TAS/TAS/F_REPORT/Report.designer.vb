﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Report))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BTSAPReport = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CbProduct = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.printdate = New System.Windows.Forms.DateTimePicker()
        Me.Enddate = New System.Windows.Forms.DateTimePicker()
        Me.LoadingBut = New System.Windows.Forms.Button()
        Me.TruckLoading = New System.Windows.Forms.Button()
        Me.TruckUnloading = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UnloadBut = New System.Windows.Forms.Button()
        Me.BtScapPrint = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadingToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnloadingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TruckReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TruckLoadingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TruckUnloadingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SAPReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SCAPReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnloadingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DBGrid1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_VEHICLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Product_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_TANK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_DRIVER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_DOfull = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LC_COMPARTMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADCAPACITYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADPRESETDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADCARDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LCCOMPARTMENTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LCBAYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReferenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADTANKDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUSNAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDOfullDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADVEHICLEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductnameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomernameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDRIVERDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LoadidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.V_LoadingnoteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.V_LOADINGNOTETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_LOADINGNOTETableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.Panel1.SuspendLayout()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.DBGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.V_LoadingnoteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.BTSAPReport)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.CbProduct)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.printdate)
        Me.Panel1.Controls.Add(Me.Enddate)
        Me.Panel1.Controls.Add(Me.LoadingBut)
        Me.Panel1.Controls.Add(Me.TruckLoading)
        Me.Panel1.Controls.Add(Me.TruckUnloading)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.UnloadBut)
        Me.Panel1.Controls.Add(Me.BtScapPrint)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Panel1.Location = New System.Drawing.Point(1, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1015, 199)
        Me.Panel1.TabIndex = 0
        '
        'BTSAPReport
        '
        Me.BTSAPReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTSAPReport.Location = New System.Drawing.Point(406, 129)
        Me.BTSAPReport.Name = "BTSAPReport"
        Me.BTSAPReport.Size = New System.Drawing.Size(130, 41)
        Me.BTSAPReport.TabIndex = 181
        Me.BTSAPReport.Text = "Print"
        Me.BTSAPReport.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 20)
        Me.Label2.TabIndex = 175
        Me.Label2.Text = "Start Date :"
        '
        'CbProduct
        '
        Me.CbProduct.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CbProduct.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CbProduct.DataSource = Me.TProductBindingSource
        Me.CbProduct.DisplayMember = "Product_name"
        Me.CbProduct.DropDownHeight = 400
        Me.CbProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.CbProduct.FormattingEnabled = True
        Me.CbProduct.IntegralHeight = False
        Me.CbProduct.Location = New System.Drawing.Point(668, 73)
        Me.CbProduct.Name = "CbProduct"
        Me.CbProduct.Size = New System.Drawing.Size(214, 28)
        Me.CbProduct.TabIndex = 180
        Me.CbProduct.ValueMember = "Product_name"
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(589, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 20)
        Me.Label4.TabIndex = 179
        Me.Label4.Text = "Product :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(301, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 20)
        Me.Label3.TabIndex = 179
        Me.Label3.Text = "End Date :"
        '
        'printdate
        '
        Me.printdate.CustomFormat = "dd/MM/yyyy HH:mm"
        Me.printdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.printdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.printdate.Location = New System.Drawing.Point(107, 74)
        Me.printdate.Name = "printdate"
        Me.printdate.Size = New System.Drawing.Size(179, 26)
        Me.printdate.TabIndex = 1
        Me.printdate.Value = New Date(2013, 6, 21, 0, 0, 0, 0)
        '
        'Enddate
        '
        Me.Enddate.CustomFormat = "dd/MM/yyyy HH:mm"
        Me.Enddate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Enddate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Enddate.Location = New System.Drawing.Point(394, 74)
        Me.Enddate.Name = "Enddate"
        Me.Enddate.Size = New System.Drawing.Size(179, 26)
        Me.Enddate.TabIndex = 178
        '
        'LoadingBut
        '
        Me.LoadingBut.BackColor = System.Drawing.SystemColors.Control
        Me.LoadingBut.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LoadingBut.Location = New System.Drawing.Point(406, 129)
        Me.LoadingBut.Name = "LoadingBut"
        Me.LoadingBut.Size = New System.Drawing.Size(130, 41)
        Me.LoadingBut.TabIndex = 2
        Me.LoadingBut.Text = "Export"
        Me.LoadingBut.UseVisualStyleBackColor = True
        '
        'TruckLoading
        '
        Me.TruckLoading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TruckLoading.Location = New System.Drawing.Point(16, 129)
        Me.TruckLoading.Name = "TruckLoading"
        Me.TruckLoading.Size = New System.Drawing.Size(130, 41)
        Me.TruckLoading.TabIndex = 176
        Me.TruckLoading.Text = "Print"
        Me.TruckLoading.UseVisualStyleBackColor = True
        Me.TruckLoading.Visible = False
        '
        'TruckUnloading
        '
        Me.TruckUnloading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TruckUnloading.Location = New System.Drawing.Point(16, 129)
        Me.TruckUnloading.Name = "TruckUnloading"
        Me.TruckUnloading.Size = New System.Drawing.Size(130, 41)
        Me.TruckUnloading.TabIndex = 177
        Me.TruckUnloading.Text = "Print"
        Me.TruckUnloading.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1007, 58)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "REPORT"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'UnloadBut
        '
        Me.UnloadBut.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UnloadBut.Location = New System.Drawing.Point(406, 129)
        Me.UnloadBut.Name = "UnloadBut"
        Me.UnloadBut.Size = New System.Drawing.Size(130, 41)
        Me.UnloadBut.TabIndex = 3
        Me.UnloadBut.Text = "Export"
        Me.UnloadBut.UseVisualStyleBackColor = True
        '
        'BtScapPrint
        '
        Me.BtScapPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtScapPrint.Location = New System.Drawing.Point(406, 129)
        Me.BtScapPrint.Name = "BtScapPrint"
        Me.BtScapPrint.Size = New System.Drawing.Size(130, 41)
        Me.BtScapPrint.TabIndex = 182
        Me.BtScapPrint.Text = "Print"
        Me.BtScapPrint.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1020, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadingToolStripMenuItem, Me.TruckReportToolStripMenuItem, Me.SAPReportToolStripMenuItem, Me.SCAPReportToolStripMenuItem, Me.LoadingReportToolStripMenuItem, Me.UnloadingReportToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.FileToolStripMenuItem.Text = "Report"
        '
        'LoadingToolStripMenuItem
        '
        Me.LoadingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadingToolStripMenuItem1, Me.UnloadingToolStripMenuItem})
        Me.LoadingToolStripMenuItem.Name = "LoadingToolStripMenuItem"
        Me.LoadingToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.LoadingToolStripMenuItem.Text = " Export To Excel"
        Me.LoadingToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.LoadingToolStripMenuItem.Visible = False
        '
        'LoadingToolStripMenuItem1
        '
        Me.LoadingToolStripMenuItem1.Name = "LoadingToolStripMenuItem1"
        Me.LoadingToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.LoadingToolStripMenuItem1.Text = "Loading"
        '
        'UnloadingToolStripMenuItem
        '
        Me.UnloadingToolStripMenuItem.Name = "UnloadingToolStripMenuItem"
        Me.UnloadingToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.UnloadingToolStripMenuItem.Text = "Unloading"
        '
        'TruckReportToolStripMenuItem
        '
        Me.TruckReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TruckLoadingReportToolStripMenuItem, Me.TruckUnloadingReportToolStripMenuItem})
        Me.TruckReportToolStripMenuItem.Name = "TruckReportToolStripMenuItem"
        Me.TruckReportToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.TruckReportToolStripMenuItem.Text = "Truck Report"
        Me.TruckReportToolStripMenuItem.Visible = False
        '
        'TruckLoadingReportToolStripMenuItem
        '
        Me.TruckLoadingReportToolStripMenuItem.Name = "TruckLoadingReportToolStripMenuItem"
        Me.TruckLoadingReportToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.TruckLoadingReportToolStripMenuItem.Text = "Truck Loading Report"
        '
        'TruckUnloadingReportToolStripMenuItem
        '
        Me.TruckUnloadingReportToolStripMenuItem.Name = "TruckUnloadingReportToolStripMenuItem"
        Me.TruckUnloadingReportToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.TruckUnloadingReportToolStripMenuItem.Text = "Truck Unloading Report"
        '
        'SAPReportToolStripMenuItem
        '
        Me.SAPReportToolStripMenuItem.Name = "SAPReportToolStripMenuItem"
        Me.SAPReportToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.SAPReportToolStripMenuItem.Text = "SAP Report"
        '
        'SCAPReportToolStripMenuItem
        '
        Me.SCAPReportToolStripMenuItem.Name = "SCAPReportToolStripMenuItem"
        Me.SCAPReportToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.SCAPReportToolStripMenuItem.Text = "SCAP Report"
        '
        'LoadingReportToolStripMenuItem
        '
        Me.LoadingReportToolStripMenuItem.Name = "LoadingReportToolStripMenuItem"
        Me.LoadingReportToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.LoadingReportToolStripMenuItem.Text = "Loading Report"
        '
        'UnloadingReportToolStripMenuItem
        '
        Me.UnloadingReportToolStripMenuItem.Name = "UnloadingReportToolStripMenuItem"
        Me.UnloadingReportToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.UnloadingReportToolStripMenuItem.Text = "Unloading Report"
        '
        'DBGrid1
        '
        Me.DBGrid1.AutoGenerateColumns = False
        Me.DBGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.LOAD_VEHICLE, Me.SP_Code, Me.Product_name, Me.LOAD_TANK, Me.STATUS_NAME, Me.LOAD_DRIVER, Me.LOAD_DOfull, Me.LC_COMPARTMENT, Me.LOADCAPACITYDataGridViewTextBoxColumn, Me.LOADPRESETDataGridViewTextBoxColumn, Me.LOADCARDDataGridViewTextBoxColumn, Me.LCCOMPARTMENTDataGridViewTextBoxColumn, Me.LCBAYDataGridViewTextBoxColumn, Me.LOADDATEDataGridViewTextBoxColumn, Me.ReferenceDataGridViewTextBoxColumn, Me.LOADDIDDataGridViewTextBoxColumn, Me.SPCodeDataGridViewTextBoxColumn, Me.LOADTANKDataGridViewTextBoxColumn, Me.STATUSNAMEDataGridViewTextBoxColumn, Me.LOADDOfullDataGridViewTextBoxColumn, Me.LOADVEHICLEDataGridViewTextBoxColumn, Me.ProductnameDataGridViewTextBoxColumn, Me.CustomernameDataGridViewTextBoxColumn, Me.LOADDRIVERDataGridViewTextBoxColumn, Me.LoadidDataGridViewTextBoxColumn})
        Me.DBGrid1.DataSource = Me.V_LoadingnoteBindingSource
        Me.DBGrid1.Location = New System.Drawing.Point(-3, 314)
        Me.DBGrid1.Name = "DBGrid1"
        Me.DBGrid1.ReadOnly = True
        Me.DBGrid1.Size = New System.Drawing.Size(982, 104)
        Me.DBGrid1.TabIndex = 168
        Me.DBGrid1.Visible = False
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "LOAD_DID"
        Me.Column1.HeaderText = "LOAD NO."
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'LOAD_VEHICLE
        '
        Me.LOAD_VEHICLE.DataPropertyName = "LOAD_VEHICLE"
        Me.LOAD_VEHICLE.HeaderText = "VEHICLE"
        Me.LOAD_VEHICLE.Name = "LOAD_VEHICLE"
        Me.LOAD_VEHICLE.ReadOnly = True
        '
        'SP_Code
        '
        Me.SP_Code.DataPropertyName = "SP_Code"
        Me.SP_Code.HeaderText = "COMPANY"
        Me.SP_Code.Name = "SP_Code"
        Me.SP_Code.ReadOnly = True
        '
        'Product_name
        '
        Me.Product_name.DataPropertyName = "Product_name"
        Me.Product_name.HeaderText = "PRODUCT"
        Me.Product_name.Name = "Product_name"
        Me.Product_name.ReadOnly = True
        '
        'LOAD_TANK
        '
        Me.LOAD_TANK.DataPropertyName = "LOAD_TANK"
        Me.LOAD_TANK.HeaderText = "TANK"
        Me.LOAD_TANK.Name = "LOAD_TANK"
        Me.LOAD_TANK.ReadOnly = True
        '
        'STATUS_NAME
        '
        Me.STATUS_NAME.DataPropertyName = "STATUS_NAME"
        Me.STATUS_NAME.HeaderText = "STATUS"
        Me.STATUS_NAME.Name = "STATUS_NAME"
        Me.STATUS_NAME.ReadOnly = True
        '
        'LOAD_DRIVER
        '
        Me.LOAD_DRIVER.DataPropertyName = "LOAD_DRIVER"
        Me.LOAD_DRIVER.HeaderText = "DRIVER"
        Me.LOAD_DRIVER.Name = "LOAD_DRIVER"
        Me.LOAD_DRIVER.ReadOnly = True
        '
        'LOAD_DOfull
        '
        Me.LOAD_DOfull.DataPropertyName = "LOAD_DOfull"
        Me.LOAD_DOfull.HeaderText = "DO No."
        Me.LOAD_DOfull.Name = "LOAD_DOfull"
        Me.LOAD_DOfull.ReadOnly = True
        '
        'LC_COMPARTMENT
        '
        Me.LC_COMPARTMENT.DataPropertyName = "LC_COMPARTMENT"
        Me.LC_COMPARTMENT.HeaderText = "COMPARTMENT"
        Me.LC_COMPARTMENT.Name = "LC_COMPARTMENT"
        Me.LC_COMPARTMENT.ReadOnly = True
        '
        'LOADCAPACITYDataGridViewTextBoxColumn
        '
        Me.LOADCAPACITYDataGridViewTextBoxColumn.DataPropertyName = "LOAD_CAPACITY"
        Me.LOADCAPACITYDataGridViewTextBoxColumn.HeaderText = "LOAD_CAPACITY"
        Me.LOADCAPACITYDataGridViewTextBoxColumn.Name = "LOADCAPACITYDataGridViewTextBoxColumn"
        Me.LOADCAPACITYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADPRESETDataGridViewTextBoxColumn
        '
        Me.LOADPRESETDataGridViewTextBoxColumn.DataPropertyName = "LOAD_PRESET"
        Me.LOADPRESETDataGridViewTextBoxColumn.HeaderText = "LOAD_PRESET"
        Me.LOADPRESETDataGridViewTextBoxColumn.Name = "LOADPRESETDataGridViewTextBoxColumn"
        Me.LOADPRESETDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADCARDDataGridViewTextBoxColumn
        '
        Me.LOADCARDDataGridViewTextBoxColumn.DataPropertyName = "LOAD_CARD"
        Me.LOADCARDDataGridViewTextBoxColumn.HeaderText = "LOAD_CARD"
        Me.LOADCARDDataGridViewTextBoxColumn.Name = "LOADCARDDataGridViewTextBoxColumn"
        Me.LOADCARDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LCCOMPARTMENTDataGridViewTextBoxColumn
        '
        Me.LCCOMPARTMENTDataGridViewTextBoxColumn.DataPropertyName = "LC_COMPARTMENT"
        Me.LCCOMPARTMENTDataGridViewTextBoxColumn.HeaderText = "LC_COMPARTMENT"
        Me.LCCOMPARTMENTDataGridViewTextBoxColumn.Name = "LCCOMPARTMENTDataGridViewTextBoxColumn"
        Me.LCCOMPARTMENTDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LCBAYDataGridViewTextBoxColumn
        '
        Me.LCBAYDataGridViewTextBoxColumn.DataPropertyName = "LC_BAY"
        Me.LCBAYDataGridViewTextBoxColumn.HeaderText = "LC_BAY"
        Me.LCBAYDataGridViewTextBoxColumn.Name = "LCBAYDataGridViewTextBoxColumn"
        Me.LCBAYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADDATEDataGridViewTextBoxColumn
        '
        Me.LOADDATEDataGridViewTextBoxColumn.DataPropertyName = "LOAD_DATE"
        Me.LOADDATEDataGridViewTextBoxColumn.HeaderText = "LOAD_DATE"
        Me.LOADDATEDataGridViewTextBoxColumn.Name = "LOADDATEDataGridViewTextBoxColumn"
        Me.LOADDATEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ReferenceDataGridViewTextBoxColumn
        '
        Me.ReferenceDataGridViewTextBoxColumn.DataPropertyName = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.HeaderText = "Reference"
        Me.ReferenceDataGridViewTextBoxColumn.Name = "ReferenceDataGridViewTextBoxColumn"
        Me.ReferenceDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADDIDDataGridViewTextBoxColumn
        '
        Me.LOADDIDDataGridViewTextBoxColumn.DataPropertyName = "LOAD_DID"
        Me.LOADDIDDataGridViewTextBoxColumn.HeaderText = "LOAD_DID"
        Me.LOADDIDDataGridViewTextBoxColumn.Name = "LOADDIDDataGridViewTextBoxColumn"
        Me.LOADDIDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SPCodeDataGridViewTextBoxColumn
        '
        Me.SPCodeDataGridViewTextBoxColumn.DataPropertyName = "SP_Code"
        Me.SPCodeDataGridViewTextBoxColumn.HeaderText = "SP_Code"
        Me.SPCodeDataGridViewTextBoxColumn.Name = "SPCodeDataGridViewTextBoxColumn"
        Me.SPCodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADTANKDataGridViewTextBoxColumn
        '
        Me.LOADTANKDataGridViewTextBoxColumn.DataPropertyName = "LOAD_TANK"
        Me.LOADTANKDataGridViewTextBoxColumn.HeaderText = "LOAD_TANK"
        Me.LOADTANKDataGridViewTextBoxColumn.Name = "LOADTANKDataGridViewTextBoxColumn"
        Me.LOADTANKDataGridViewTextBoxColumn.ReadOnly = True
        '
        'STATUSNAMEDataGridViewTextBoxColumn
        '
        Me.STATUSNAMEDataGridViewTextBoxColumn.DataPropertyName = "STATUS_NAME"
        Me.STATUSNAMEDataGridViewTextBoxColumn.HeaderText = "STATUS_NAME"
        Me.STATUSNAMEDataGridViewTextBoxColumn.Name = "STATUSNAMEDataGridViewTextBoxColumn"
        Me.STATUSNAMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADDOfullDataGridViewTextBoxColumn
        '
        Me.LOADDOfullDataGridViewTextBoxColumn.DataPropertyName = "LOAD_DOfull"
        Me.LOADDOfullDataGridViewTextBoxColumn.HeaderText = "LOAD_DOfull"
        Me.LOADDOfullDataGridViewTextBoxColumn.Name = "LOADDOfullDataGridViewTextBoxColumn"
        Me.LOADDOfullDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADVEHICLEDataGridViewTextBoxColumn
        '
        Me.LOADVEHICLEDataGridViewTextBoxColumn.DataPropertyName = "LOAD_VEHICLE"
        Me.LOADVEHICLEDataGridViewTextBoxColumn.HeaderText = "LOAD_VEHICLE"
        Me.LOADVEHICLEDataGridViewTextBoxColumn.Name = "LOADVEHICLEDataGridViewTextBoxColumn"
        Me.LOADVEHICLEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ProductnameDataGridViewTextBoxColumn
        '
        Me.ProductnameDataGridViewTextBoxColumn.DataPropertyName = "Product_name"
        Me.ProductnameDataGridViewTextBoxColumn.HeaderText = "Product_name"
        Me.ProductnameDataGridViewTextBoxColumn.Name = "ProductnameDataGridViewTextBoxColumn"
        Me.ProductnameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustomernameDataGridViewTextBoxColumn
        '
        Me.CustomernameDataGridViewTextBoxColumn.DataPropertyName = "Customer_name"
        Me.CustomernameDataGridViewTextBoxColumn.HeaderText = "Customer_name"
        Me.CustomernameDataGridViewTextBoxColumn.Name = "CustomernameDataGridViewTextBoxColumn"
        Me.CustomernameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LOADDRIVERDataGridViewTextBoxColumn
        '
        Me.LOADDRIVERDataGridViewTextBoxColumn.DataPropertyName = "LOAD_DRIVER"
        Me.LOADDRIVERDataGridViewTextBoxColumn.HeaderText = "LOAD_DRIVER"
        Me.LOADDRIVERDataGridViewTextBoxColumn.Name = "LOADDRIVERDataGridViewTextBoxColumn"
        Me.LOADDRIVERDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LoadidDataGridViewTextBoxColumn
        '
        Me.LoadidDataGridViewTextBoxColumn.DataPropertyName = "Load_id"
        Me.LoadidDataGridViewTextBoxColumn.HeaderText = "Load_id"
        Me.LoadidDataGridViewTextBoxColumn.Name = "LoadidDataGridViewTextBoxColumn"
        Me.LoadidDataGridViewTextBoxColumn.ReadOnly = True
        '
        'V_LoadingnoteBindingSource
        '
        Me.V_LoadingnoteBindingSource.AllowNew = True
        Me.V_LoadingnoteBindingSource.DataMember = "V_LOADINGNOTE"
        Me.V_LoadingnoteBindingSource.DataSource = Me.FPTDataSet
        '
        'V_LOADINGNOTETableAdapter
        '
        Me.V_LOADINGNOTETableAdapter.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(611, 351)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(113, 51)
        Me.Button3.TabIndex = 170
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(-4, 316)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(928, 104)
        Me.DataGridView1.TabIndex = 171
        Me.DataGridView1.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(130, 323)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(113, 51)
        Me.Button4.TabIndex = 173
        Me.Button4.Text = "Adnote"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(249, 324)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(113, 51)
        Me.Button5.TabIndex = 174
        Me.Button5.Text = "Unload"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 291)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.DBGrid1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "23"
        Me.Text = "Report"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.DBGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.V_LoadingnoteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadingToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnloadingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadingBut As System.Windows.Forms.Button
    Friend WithEvents DBGrid1 As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_VEHICLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SP_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Product_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_TANK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_DRIVER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_DOfull As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LC_COMPARTMENT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents V_LOADINGNOTETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_LOADINGNOTETableAdapter
    Friend WithEvents UnloadBut As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents V_LoadingnoteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LOADCAPACITYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADPRESETDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADCARDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCCOMPARTMENTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCBAYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReferenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SPCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADTANKDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUSNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDOfullDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADVEHICLEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductnameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomernameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDRIVERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LoadidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents printdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TruckLoading As System.Windows.Forms.Button
    Friend WithEvents TruckUnloading As System.Windows.Forms.Button
    Friend WithEvents TruckReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TruckLoadingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TruckUnloadingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CbProduct As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Enddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents SAPReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BTSAPReport As System.Windows.Forms.Button
    Friend WithEvents SCAPReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BtScapPrint As System.Windows.Forms.Button
    Friend WithEvents LoadingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnloadingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
