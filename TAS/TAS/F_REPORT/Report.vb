﻿'Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports TAS_TOL.C01_Permission
Imports CrystalDecisions.Shared

Public Class Report
    Dim xlApp As New Microsoft.Office.Interop.Excel.Application


    Public Shared Sub exportExcelfile(ByVal objValue As DataGridView, _
                                           ByVal objBGW As System.ComponentModel.BackgroundWorker)
        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
            Dim xlApp As Microsoft.Office.Interop.Excel.Application
            ' elApp = CreateObject("Excel.Application")

            Dim xlWorkBook As Excel.Workbook
            Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            Dim i As Integer
            Dim j As Integer

            Dim strPath As String = "D:\Export\Excel\"
            xlApp = New Microsoft.Office.Interop.Excel.Application

            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("" + Date.Now.Date + "")

            For i = 0 To objValue.RowCount - 1
                objBGW.ReportProgress(CInt(100 * i / objValue.RowCount))

                For j = 0 To objValue.ColumnCount - 1
                    xlWorkSheet.Cells.NumberFormat = "@"
                    xlWorkSheet.Cells(i + 1, j + 1) = _
                        objValue(j, i).Value.ToString()
                Next
            Next

            '    If Directory.Exists(strPath) = False Then
            '        Directory.CreateDirectory(strPath)
            '    End If
            '    xlWorkSheet.SaveAs(strPath& _getDate() & ".xls")
            '    xlWorkBook.Close()
            '    xlApp.Quit()

            '    releaseObject(xlApp)
            '    releaseObject(xlWorkBook)
            '    releaseObject(xlWorkSheet)

            '    MsgBox(strPath & _getDate() & ".xlsx", MsgBoxStyle.Information)

        Catch ex As Exception
            '    writeLogFile(ex)
        End Try

    End Sub

    Private Sub ToolStripComboBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'MessageBox.Show("Please Check Lawweight", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub LoadingToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadingToolStripMenuItem1.Click
        'Dim xlApp As Excel.Application
        LoadingBut.Visible = True
        UnloadBut.Visible = False
        TruckLoading.Visible = False
        TruckUnloading.Visible = False
        Label1.Text = "Loading Report To Excel"
    End Sub

    Private Sub Report_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        'Dim A As Integer
        'A = Report.Size.Width
        'Panel1.Width = Report.Width.ToString
    End Sub

    Private Sub UnloadingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnloadingToolStripMenuItem.Click
        UnloadBut.Visible = True
        LoadingBut.Visible = False
        TruckLoading.Visible = False
        TruckUnloading.Visible = False
        Label1.Text = "Unloading Report To Excel"
    End Sub
    Private Function GetConnection() As SqlClient.SqlConnection
        Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadingBut.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Dim q As String
        conn.Open()
        q = ""
        q &= " SELECT CASE WHEN isnull(MAX(AddnoteDate), '-') = '' THEN '-' ELSE isnull(MAX(AddnoteDate), '-') END AS Addnotedate, "
        q &= " CASE WHEN isnull(MAX(LOAD_VEHICLE), '-') = '' THEN '-' ELSE isnull(MAX(LOAD_VEHICLE), '-') END AS Vehicle, CASE WHEN isnull(MAX(LOAD_DRIVER), '-') "
        q &= " = '' THEN '-' ELSE isnull(MAX(LOAD_DRIVER), '-') END AS Driver, CASE WHEN isnull(MAX(Product_name), '-') = '' THEN '-' ELSE isnull(MAX(Product_name), '-') "
        q &= " END AS Product, CASE WHEN isnull(MAX(LOAD_TANK), '-') = '' THEN '-' ELSE isnull(MAX(LOAD_TANK), '-') END AS Tank, CASE WHEN isnull(MAX(Customer_name), '-') "
        q &= " = '' THEN '-' ELSE isnull(MAX(Customer_name), '-') END AS Customer, CASE WHEN isnull(MAX(TANKFARM), '-') = '' THEN '-' ELSE isnull(MAX(TANKFARM), '-') "
        q &= " END AS TANKFARM, CASE WHEN isnull(MAX(COA), '-') = '' THEN '-' ELSE isnull(MAX(COA), '-') END AS COA, CASE WHEN isnull(MAX(Batchlot), '-') "
        q &= " = '' THEN '-' ELSE isnull(MAX(Batchlot), '-') END AS Batchlot, CASE WHEN isnull(MAX(LC_SEAL), '-') = '' THEN '-' ELSE isnull(MAX(LC_SEAL), '-') END AS Seal, "
        q &= " CASE WHEN isnull(MAX(CAST(Weightin_time AS varchar(8))), '-') = '' THEN '-' ELSE isnull(MAX(CAST(Weightin_time AS varchar(8))), '-') END AS WeightinTime, "
        q &= " CASE WHEN isnull(MAX(CAST(WeightOut_time AS varchar(8))), '-') = '' THEN '-' ELSE isnull(MAX(CAST(WeightOut_time AS varchar(8))), '-') END AS WeightOutTime, "
        q &= " CASE WHEN isnull(MAX(TakenTime), '-') = '' THEN '-' ELSE isnull(MAX(TakenTime), '-') END AS TakenTime, CASE WHEN isnull(MAX(Raw_Weight_IN), '0') "
        q &= " = '0' THEN '0' ELSE isnull(MAX(Raw_Weight_IN), '0') END AS WeightIN, CASE WHEN isnull(MAX(Raw_Weight_OUT), '0') "
        q &= " = '' THEN '0' ELSE isnull(MAX(Raw_Weight_OUT), '0') END AS WeightOut, CASE WHEN isnull(MAX(Raw_Weight_OUT), '0') - isnull(MAX(Raw_Weight_IN), '0')-ISNULL(MAX(Packing_weight), 0) "
        q &= " = '0' THEN '0' ELSE isnull(MAX(Raw_Weight_OUT), '0') - isnull(MAX(Raw_Weight_IN), '0')-ISNULL(MAX(Packing_weight), 0) END AS WeightTotal, CASE WHEN isnull(MAX(Density), '0') "
        q &= " = '0' THEN '0' ELSE isnull(MAX(Density), '0') END AS Density, CASE WHEN isnull(MAX(WeightCal), '0') = '0' THEN '0' ELSE isnull(MAX(WeightCal), '0') "
        q &= " END AS Volume_Liters, CASE WHEN isnull(MAX(dO_NO), '-') = '' THEN '-' ELSE isnull(MAX(dO_NO), '-') END AS Delivery_order_No, CASE WHEN isnull(MAX(dO_ITEM), "
        q &= " '-') = '' THEN '-' ELSE isnull(MAX(dO_ITEM), '-') END AS Delivery_order_Item, CASE WHEN isnull(MAX(sO_NO), '-') = '' THEN '-' ELSE isnull(MAX(sO_NO), '-') "
        q &= " END AS Sales_order_No, CASE WHEN isnull(MAX(sO_ITEM), '-') = '' THEN '-' ELSE isnull(MAX(sO_ITEM), '-') END AS Sales_order_Item, "
        q &= " CASE WHEN isnull(MAX(Remark), '-') = '' THEN '-' ELSE isnull(MAX(Remark), '-') END +' ' + CASE WHEN isnull(MAX(DO_INS_TXT), '-') = '' THEN '-' ELSE isnull(MAX(DO_INS_TXT), '-') END AS Remark, CASE WHEN ISNULL(MAX(Raw_Weight_OUT), 0) "
        q &= " - ISNULL(MAX(Raw_Weight_IN), 0) - ISNULL(MAX(WeightTotal), 0) - ISNULL(MAX(Packing_weight), 0) = '0' THEN '0' ELSE ISNULL(MAX(Raw_Weight_OUT), 0) "
        q &= " - ISNULL(MAX(Raw_Weight_IN), 0) - ISNULL(MAX(WeightTotal), 0) - ISNULL(MAX(Packing_weight), 0) END AS LOAD_DIFF,ISNULL(MAX(Packing_weight), 0) as Packing_weight ,"
        q &= " CASE WHEN isnull(MAX(TRANSPORTER), '-')  = '' THEN '-' ELSE isnull(MAX(TRANSPORTER), '-') END AS TRANSPORTER ,"
        q &= " CASE WHEN isnull(MAX(Container_Tare), '-')  = '' THEN '-' ELSE isnull(MAX(Container_Tare), '-') END AS Container_Tare "
        q &= " FROM V_LOADINGNOTE "
        q &= " where Addnotedate between '" + String.Format("{0:yyyy-M-d}", printdate.Value) + "'" + ""
        q &= " and '" + String.Format("{0:yyyy-M-d}", Enddate.Value) + "'" + "  and load_status=3 "
        If CbProduct.Text <> "" Then q &= " and Product_name='" + CbProduct.Text + "'" + ""

        'q &= " where addnotedate='" + String.Format("{0:yyyy-MM-dd}", printdate.Value) + "'" + ""
        q &= " group by reference order by AddnoteDate, Product,Customer "
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim ds As New DataSet()


        Dim Myreport As New ReportDocument
        Myreport = New ReportDocument
        Dim Myreport2 As New ReportDocument
        Myreport2 = New ReportDocument

        da.Fill(ds, "V_LOADINGNOTE")
        Try



            Myreport.Load("ExportLoadReport.rpt")
            Myreport2.Load("ExportLoadReportNotsum.rpt")

            'Myreport.Load("weightticket.rpt")

            Myreport.SetDataSource(ds.Tables("V_LOADINGNOTE"))
            Myreport2.SetDataSource(ds.Tables("V_LOADINGNOTE"))
            My.Computer.FileSystem.CreateDirectory("D:\TASExport\Loading\" & String.Format("{0:yyyy-M-d}", Now))
            Dim Oexcel As New Microsoft.Office.Interop.Excel.ApplicationClass
            Dim wBook1, wBook2 As Microsoft.Office.Interop.Excel.Workbook

            '  Dim wSheet As New Microsoft.Office.Interop.Excel.Worksheet
            '  Dim SSname As String = "D:\TASExport\Loading\" & String.Format("{0:yyyy-M-d}", Now) & "\" & String.Format("{0:yyyy_M_d_hhmmss}", Now) & ".xls"
            Dim SSname1 As String = "D:\TASExport\Loading\" & String.Format("{0:yyyy-M-d}", Now) & "\" & String.Format("{0:yyyy_M_d_hhmmss}", Now) & "_01.xls"
            Dim SSname2 As String = "D:\TASExport\Loading\" & String.Format("{0:yyyy-M-d}", Now) & "\" & String.Format("{0:yyyy_M_d_hhmmss}", Now) & "_02.xls"

            Myreport.ExportToDisk(ExportFormatType.Excel, SSname1)
            Myreport2.ExportToDisk(ExportFormatType.Excel, SSname2)
            wBook1 = Oexcel.Workbooks.Open(SSname1)
            wBook2 = Oexcel.Workbooks.Open(SSname2)
            wBook1.Worksheets(1).name = "SUM LOADING REPORT"
            wBook2.Worksheets(1).name = "LOADING REPORT"
            wBook2.Worksheets.Copy(After:=wBook1.Worksheets(1))
            wBook1.Close(SaveChanges:=True)
            wBook2.Close(SaveChanges:=True)
            wBook1 = Nothing
            wBook2 = Nothing
            ' System.IO.File.Delete(SSname1)
            System.IO.File.Delete(SSname2)

            Oexcel.Workbooks.Open(SSname1)

            Oexcel.Columns.AutoFit()
            Oexcel.Visible = True
        Catch ex As Exception

        End Try

        conn.Close()
        conn.Dispose()
        da.Dispose()
        ds.Dispose()
        'Dim conn As SqlClient.SqlConnection = GetConnection()
        'conn.Open()
        'Dim xlApp As Object 'Excel.Application
        'Dim ra As Object 'Excel.Range
        ''On Error Resume Next
        ''xlApp = GetObject(, "Excel.Application")
        ''If Err.Number <> 0 Then
        ''    xlApp = New Microsoft.Office.Interop.Excel.Application
        ''    xlApp = CreateObject("Excel.Application")
        ''End If
        ''xlApp.Visible = True
        ''xlApp.Workbooks.Add()
        ''ra = xlApp.ActiveCell

        ''Dim oExcel As Object
        ''Dim oBook As Object
        ''Dim oSheet As Object
        ''oBook = xlApp.Workbooks.Add
        ''Dim strFileName As String = "D:\Loading " + String.Format("{0:yyyy-MM-dd}", Date.Now.Date) + ".xls"
        ''oBook.SaveAs(strFileName)
        ''xlApp = Nothing
        ''GC.Collect()

        'Dim q As String
        'q = ""
        'q = "select max(Addnotedate) as Addnotedate, "
        'q &= " max(LOAD_VEHICLE) as Vehicle,"
        'q &= " max(LOAD_DRIVER) as Driver,"
        'q &= " max(Product_name) as Product,"
        'q &= " max(LOAD_TANK) as Tank,"
        'q &= " max(Customer_name) as Customer,"
        'q &= " max(TANKFARM) as TANKFARM,"
        'q &= " max(COA) as COA,"
        'q &= " max(Batchlot) as Batchlot,"
        'q &= " max(LC_SEAL) as Seal,"
        'q &= " max(cast(Weightin_time as varchar(8))) as WeightinTime,"
        'q &= " max(cast(Weightout_time as varchar(8))) as WeightOutTime,"
        'q &= " max(Takentime) as TakenTime, "
        'q &= " isnull(max(WeightIN),0) as WeightIN,"
        'q &= " isnull(max(WeightOut),0) as WeightOut,"
        'q &= " isnull(max(WeightTotal),0) as WeightTotal,"
        'q &= " isnull(max(Density),0) as Density,"
        'q &= " isnull(max(Weightcal),0) as Volume_Liters,"
        'q &= " max(DO_NO) as Delivery_order_No,"
        'q &= " max(DO_ITEM) as Delivery_order_Item,"
        'q &= " max(SO_NO) as Sales_order_No,"
        'q &= " max(SO_ITEM) as Sales_order_Item,"
        'q &= " max(Remark) as Remark"
        'q &= " FROM V_LOADINGNOTE "
        'q &= " where Addnotedate between '" + String.Format("{0:yyyy-M-d}", printdate.Value) + "'" + ""
        'q &= " and '" + String.Format("{0:yyyy-M-d}", Enddate.Value) + "'" + "  and load_status=3 "
        'If CbProduct.Text <> "" Then q &= " and Product_name='" + CbProduct.Text + "'" + ""

        ''q &= " where addnotedate='" + String.Format("{0:yyyy-MM-dd}", printdate.Value) + "'" + ""
        'q &= " group by reference order by Product,Reference "
        'Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds1 As New DataSet()
        'Dim dt As New DataTable
        'da1.Fill(dt)
        'DBGrid1.DataSource = dt
        'DataGridView1.DataSource = dt


        'Dim Oexcel As New Microsoft.Office.Interop.Excel.ApplicationClass
        'Dim wBook As Microsoft.Office.Interop.Excel.Workbook
        'Dim wSheet As New Microsoft.Office.Interop.Excel.Worksheet


        'wBook = Oexcel.Workbooks.Add()
        'wSheet = wBook.ActiveSheet()

        ''  Dim dt As System.Data.DataTable = ds1.Tables(0)
        'Dim dc As System.Data.DataColumn
        'Dim dr As System.Data.DataRow
        'Dim colIndex As Integer = 0
        'Dim rowIndex As Integer = 0

        'Oexcel.Cells(1, 7) = "Loaddate"
        'Oexcel.Cells(1, 7).Font.Name = "Arial"
        'Oexcel.Cells(1, 7).Font.FontStyle = "Regular"
        'Oexcel.Cells(1, 7).Font.Size = 16

        'For Each dc In dt.Columns
        '    colIndex = colIndex + 1


        '    Oexcel.Cells(2, colIndex) = dc.ColumnName

        '    Oexcel.Cells(2, colIndex).Font.Name = "Arial"
        '    Oexcel.Cells(2, colIndex).Font.FontStyle = "Bold" '"Regular"
        '    Oexcel.Cells(2, colIndex).Font.Size = 10
        '    Oexcel.Cells(2, colIndex).Interior.Color = RGB(0, 255, 0)
        '    Oexcel.Cells(2, colIndex).font.Color = RGB(0, 0, 0)
        '    Oexcel.Cells(2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        '    Oexcel.Cells(2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        '    'Oexcel.Cells(2, colIndex).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        '    'Oexcel.Cells(2, colIndex).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        '    'Oexcel.Cells(2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        '    'Oexcel.Cells(2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous

        '    Select Case UCase(dc.ColumnName)
        '        Case UCase("Addnotedate")
        '            Oexcel.Cells(2, colIndex).Value = "วันที่"
        '        Case UCase("Vehicle")
        '            Oexcel.Cells(2, colIndex).Value = "ทะเบียนรถ"
        '        Case UCase("Driver")
        '            Oexcel.Cells(2, colIndex).Value = "พนักงานขับรถ"
        '        Case UCase("Product")
        '            Oexcel.Cells(2, colIndex).Value = "Product"
        '        Case UCase("Tank")
        '            Oexcel.Cells(2, colIndex).Value = "Tank No."
        '        Case UCase("Customer")
        '            Oexcel.Cells(2, colIndex).Value = "ลูกค้า"
        '        Case UCase("TANKFARM")
        '            Oexcel.Cells(2, colIndex).Value = "คลัง"

        '        Case UCase("COA")
        '            Oexcel.Cells(2, colIndex).Value = "COA.No"
        '        Case UCase("Batchlot")
        '            Oexcel.Cells(2, colIndex).Value = "Batchlot"
        '        Case UCase("Seal")
        '            Oexcel.Cells(2, colIndex).Value = "Seal No."

        '        Case UCase("WeightinTime")
        '            Oexcel.Cells(2, colIndex).Value = "เวลาชั่งเข้า"
        '        Case UCase("WeightOutTime")
        '            Oexcel.Cells(2, colIndex).Value = "เวลาชั่งออก"

        '        Case UCase("TakenTime")
        '            Oexcel.Cells(2, colIndex).Value = "เวลาที่อยู่ในโรงงาน"
        '        Case UCase("WeightIN")
        '            Oexcel.Cells(2, colIndex).Value = "น้ำหนักชั่งเข้า"
        '        Case UCase("WeightOut")
        '            Oexcel.Cells(2, colIndex).Value = "น้ำหนักชั่งออก"

        '        Case UCase("WeightTotal")
        '            Oexcel.Cells(2, colIndex).Value = "น้ำหนักโหลด (kg)"
        '        Case UCase("Density")
        '            Oexcel.Cells(2, colIndex).Value = "Density"
        '        Case UCase("Volume_Liters")
        '            Oexcel.Cells(2, colIndex).Value = "ปริมาตร (Liters)"
        '        Case UCase("Delivery_order_No")
        '            Oexcel.Cells(2, colIndex).Value = "D/O No."
        '        Case UCase("Delivery_order_Item")
        '            Oexcel.Cells(2, colIndex).Value = "D/O Item"
        '        Case UCase("Sales_order_No")
        '            Oexcel.Cells(2, colIndex).Value = "S/O No."
        '        Case UCase("Sales_order_Item")
        '            Oexcel.Cells(2, colIndex).Value = "S/O Item"
        '        Case Else
        '            Oexcel.Cells(2, colIndex).Value = dc.ColumnName      '.Rows(i).Cells(colIndex).Value   
        '    End Select
        'Next


        'For Each dr In dt.Rows
        '    rowIndex = rowIndex + 1
        '    colIndex = 0
        '    For Each dc In dt.Columns
        '        colIndex = colIndex + 1
        '        Oexcel.Cells(rowIndex + 2, colIndex) = dr(dc.ColumnName)




        '        Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        '        Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        '        'Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        '        'Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        '        'Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        '        'Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous


        '        Oexcel.Cells(rowIndex + 2, colIndex).Value = dr(dc.ColumnName)
        '        Oexcel.Cells(rowIndex + 2, colIndex).Font.Name = "Arial"
        '        Oexcel.Cells(rowIndex + 2, colIndex).Font.FontStyle = "Regular"
        '        Oexcel.Cells(rowIndex + 2, colIndex).Font.Size = 10

        '        Oexcel.Cells(rowIndex + 2, 1).NumberFormat = "dd-MM-yyyy"
        '        Oexcel.Cells(rowIndex + 2, 7).NumberFormat = "@"
        '        Oexcel.Cells(rowIndex + 2, 8).NumberFormat = "@"
        '        Oexcel.Cells(rowIndex + 2, 9).NumberFormat = "@"
        '        Oexcel.Cells(rowIndex + 2, 14).NumberFormat = "#,##0"
        '        Oexcel.Cells(rowIndex + 2, 15).NumberFormat = "#,##0"
        '        Oexcel.Cells(rowIndex + 2, 16).NumberFormat = "#,##0.00"
        '        Oexcel.Cells(rowIndex + 2, 17).NumberFormat = "#,##0.0000"
        '        Oexcel.Cells(rowIndex + 2, 18).NumberFormat = "#,##0"
        '        If Oexcel.Cells(rowIndex + 2, 17).Value <> "0" Then
        '            '    'Oexcel.Cells(rowIndex + 2, 16).Value = Convert.ToDouble(.Rows(rowIndex).Cells(14).Value) / Convert.ToDouble(.Rows(rowIndex).Cells(15).Value)
        '            '    'Oexcel.Cells(rowIndex + 2, 16).Value = .Rows(rowIndex).Cells(14).Value / .Rows(rowIndex).Cells(15).Value

        '        Else
        '            '     Oexcel.Cells(rowIndex + 2, 15).Value = ""
        '            Oexcel.Cells(rowIndex + 2, 17).Value = ""
        '        End If



        '        '     Oexcel.Cells(2, colIndex).Interior.Color = RGB(0, 255, 0)
        '        '    Oexcel.Cells(2, colIndex).font.Color = RGB(0, 0, 0)
        '        '  Oexcel.Cells(1, colIndex).HorizontalAlignment = Excel.Constants.xlCenter

        '    Next
        'Next


        '' Oexcel.Rows(1).EntireRow.Delete()
        'wSheet.Columns.AutoFit()
        'Oexcel.Cells(1, 7) = "Loading Date  " + String.Format("{0:dd/MM/yyyy}", printdate.Value) + "-" + String.Format("{0:dd/MM/yyyy}", Enddate.Value)
        'Oexcel.Cells(1, 7).Font.Name = "Arial"
        'Oexcel.Cells(1, 7).Font.FontStyle = "Regular"
        'Oexcel.Cells(1, 7).Font.Size = 16

        '' wSheet.Columns(7).Width = 10
        'Dim tolist(1) As Integer
        'tolist(0) = 16
        'tolist(1) = 18
        'With Oexcel.ActiveSheet.UsedRange
        '    '.Sort(Key1:=.Columns(15), Key2:=.Columns(15), Header:=15)
        '    .Subtotal(GroupBy:=4, Function:=-4157, TotalList:=tolist)
        'End With


        ''For i = 0 To 1
        ''    rowIndex = rowIndex + 1
        ''    colIndex = 0
        ''    For Each dc In dt.Columns
        ''        colIndex = colIndex + 1
        ''        '  Oexcel.Cells(rowIndex + 2, colIndex) = dr(dc.ColumnName)

        ''        Oexcel.Cells(rowIndex + 4, colIndex).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        Oexcel.Cells(rowIndex + 4, colIndex).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        Oexcel.Cells(rowIndex + 4, colIndex).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        Oexcel.Cells(rowIndex + 4, colIndex).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        Oexcel.Cells(rowIndex + 4, colIndex).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        Oexcel.Cells(rowIndex + 2, colIndex).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous



        ''        '     Oexcel.Cells(2, colIndex).Interior.Color = RGB(0, 255, 0)
        ''        '    Oexcel.Cells(2, colIndex).font.Color = RGB(0, 0, 0)
        ''        '  Oexcel.Cells(1, colIndex).HorizontalAlignment = Excel.Constants.xlCenter

        ''    Next
        ''Next


        'Oexcel.Rows(3).EntireRow.Delete()
        'For j As Integer = 1 To DataGridView1.RowCount + 300
        '    If Trim(Oexcel.Cells(j, 2).Value) = "" And Trim(Oexcel.Cells(j, 16).Value) <> "" Then
        '        '  Oexcel.Cells(j, 15).Value = .Columns(j).HeaderText      '.Rows(i).Cells(j).Value
        '        Oexcel.Cells(j, 16).Font.Name = "Arial"
        '        Oexcel.Cells(j, 16).Font.Bold = True
        '        Oexcel.Cells(j, 16).Font.Underline = True
        '        Oexcel.Cells(j, 16).Font.Size = 10
        '        Oexcel.Cells(j, 16).Font.Color = RGB(0, 0, 255)

        '        Oexcel.Cells(j, 18).Font.Bold = True
        '        Oexcel.Cells(j, 18).Font.Underline = True
        '        Oexcel.Cells(j, 18).Font.Size = 10

        '        Oexcel.Cells(j, 18).Font.Color = RGB(0, 0, 255)

        '        Oexcel.Cells(j, 4).Font.Bold = True
        '        Oexcel.Cells(j, 4).Font.Underline = True
        '        Oexcel.Cells(j, 4).Font.Size = 10
        '        Oexcel.Cells(j, 4).Font.Color = RGB(0, 0, 255)
        '        For K As Integer = 1 To DataGridView1.ColumnCount
        '            Oexcel.Cells(j, K).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        '            Oexcel.Cells(j, K).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        '            'Oexcel.Cells(j, K).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        '            'Oexcel.Cells(j, K).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        '            'Oexcel.Cells(j, K).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        '            'Oexcel.Cells(j, K).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
        '        Next

        '    End If

        'Next

        'Dim strFileName As String = "D:\ss.xls"
        'Dim blnFileOpen As Boolean = False

        'Try
        '    Dim fileTemp As System.IO.FileStream = System.IO.File.OpenWrite(strFileName)
        '    fileTemp.Close()
        'Catch ex As Exception
        '    blnFileOpen = False
        'End Try


        'If System.IO.File.Exists(strFileName) Then
        '    System.IO.File.Delete(strFileName)
        'End If

        'wBook.SaveAs(strFileName)
        'Oexcel.Workbooks.Open(strFileName)
        'Oexcel.Visible = True





        ''With DataGridView1
        ''    ra.offset(0, 7).value = "Loading Date  " + String.Format("{0:dd/MM/yyyy}", printdate.Value) + "-" + String.Format("{0:dd/MM/yyyy}", Enddate.Value)
        ''    ra.Offset(0, 7).Font.Name = "Arial"
        ''    ra.Offset(0, 7).Font.FontStyle = "Regular"
        ''    ra.Offset(0, 7).Font.Size = 16

        ''    For j As Integer = 0 To .ColumnCount - 1
        ''        ra.Offset(1, j).Font.Name = "Arial"
        ''        ra.Offset(1, j).Font.FontStyle = "Bold" '"Regular"
        ''        ra.Offset(1, j).Font.Size = 10
        ''        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous


        ''        Select Case UCase(.Columns(j).HeaderText)
        ''            Case UCase("Addnotedate")
        ''                ra.Offset(1, j).Value = "วันที่"
        ''            Case UCase("Vehicle")
        ''                ra.Offset(1, j).Value = "ทะเบียนรถ"
        ''            Case UCase("Driver")
        ''                ra.Offset(1, j).Value = "พนักงานขับรถ"
        ''            Case UCase("Product")
        ''                ra.Offset(1, j).Value = "Product"
        ''            Case UCase("Tank")
        ''                ra.Offset(1, j).Value = "Tank No."
        ''            Case UCase("Customer")
        ''                ra.Offset(1, j).Value = "ลูกค้า"
        ''            Case UCase("TANKFARM")
        ''                ra.Offset(1, j).Value = "คลัง"

        ''            Case UCase("COA")
        ''                ra.Offset(1, j).Value = "COA.No"
        ''            Case UCase("Batchlot")
        ''                ra.Offset(1, j).Value = "Batchlot"
        ''            Case UCase("Seal")
        ''                ra.Offset(1, j).Value = "Seal No."

        ''            Case UCase("WeightinTime")
        ''                ra.Offset(1, j).Value = "เวลาชั่งเข้า"
        ''            Case UCase("WeightOutTime")
        ''                ra.Offset(1, j).Value = "เวลาชั่งออก"

        ''            Case UCase("TakenTime")
        ''                ra.Offset(1, j).Value = "เวลาที่อยู่ในโรงงาน"
        ''            Case UCase("WeightIN")
        ''                ra.Offset(1, j).Value = "น้ำหนักชั่งเข้า"
        ''            Case UCase("WeightOut")
        ''                ra.Offset(1, j).Value = "น้ำหนักชั่งออก"

        ''            Case UCase("WeightTotal")
        ''                ra.Offset(1, j).Value = "น้ำหนักโหลด (kg)"
        ''            Case UCase("Density")
        ''                ra.Offset(1, j).Value = "Density"
        ''            Case UCase("Volume_Liters")
        ''                ra.Offset(1, j).Value = "ปริมาตร (Liters)"
        ''            Case UCase("Delivery_order_No")
        ''                ra.Offset(1, j).Value = "D/O No."
        ''            Case UCase("Delivery_order_Item")
        ''                ra.Offset(1, j).Value = "D/O Item"
        ''            Case UCase("Sales_order_No")
        ''                ra.Offset(1, j).Value = "S/O No."
        ''            Case UCase("Sales_order_Item")
        ''                ra.Offset(1, j).Value = "S/O Item"
        ''            Case Else
        ''                ra.Offset(1, j).Value = .Columns(j).HeaderText      '.Rows(i).Cells(j).Value   
        ''        End Select

        ''    Next
        ''    'Dim cFormula As String = _
        ''    '   "=" & ra.Offset(1, 2).Address(False) & _
        ''    '   "<" & ra.Offset(1, 3).Address(False)
        ''    'With ra.CurrentRegion
        ''    '    .Offset(1, 0).Resize(.Rows.Count - 1).Select()
        ''    'End With

        ''    'With xlApp.Selection.FormatConditions.Add( _
        ''    '         Type:=2, Formula1:=cFormula)
        ''    '    .Font.Bold = True
        ''    '    .Interior.Color = RGB(255, 255, 224)
        ''    '    .Font.Color = RGB(255, 0, 0)
        ''    'End With


        ''    'ra.Offset(1, 16).Value = "Volume (Liters)"
        ''    'ra.Offset(1, 16).Font.Name = "Arial"
        ''    'ra.Offset(1, 16).Font.FontStyle = "Bold" '"Regular"
        ''    'ra.Offset(1, 16).Font.Size = 10
        ''    'ra.Offset(1, 16).AutoFormat()
        ''    'ra.Offset(1, 16).Interior.Color = RGB(0, 255, 0)
        ''    'ra.Offset(1, 16).font.Color = RGB(0, 0, 0)

        ''    'ra.Offset(1, 16).HorizontalAlignment = Excel.Constants.xlCenter



        ''    For i As Integer = 0 To .RowCount - 2
        ''        For j As Integer = 0 To .ColumnCount - 1

        ''            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous


        ''            ra.Offset(i + 2, j).Value = .Rows(i).Cells(j).Value
        ''            ra.Offset(i + 2, j).Font.Name = "Arial"
        ''            ra.Offset(i + 2, j).Font.FontStyle = "Regular"
        ''            ra.Offset(i + 2, j).Font.Size = 10

        ''            ra.Offset(i + 2, 0).NumberFormat = "dd-MM-yyyy"
        ''            ra.Offset(i + 2, 6).NumberFormat = "@"
        ''            ra.Offset(i + 2, 7).NumberFormat = "@"
        ''            ra.Offset(i + 2, 8).NumberFormat = "@"
        ''            ra.Offset(i + 2, 13).NumberFormat = "#,##0"
        ''            ra.Offset(i + 2, 14).NumberFormat = "#,##0"
        ''            ra.Offset(i + 2, 15).NumberFormat = "#,##0.00"
        ''            ra.Offset(i + 2, 16).NumberFormat = "#,##0.0000"
        ''            ra.Offset(i + 2, 17).NumberFormat = "#,##0"
        ''            If .Rows(i).Cells(16).Value <> "0" Then
        ''                'ra.Offset(i + 2, 16).Value = Convert.ToDouble(.Rows(i).Cells(14).Value) / Convert.ToDouble(.Rows(i).Cells(15).Value)
        ''                'ra.Offset(i + 2, 16).Value = .Rows(i).Cells(14).Value / .Rows(i).Cells(15).Value

        ''            Else
        ''                '     ra.Offset(i + 2, 15).Value = ""
        ''                ra.Offset(i + 2, 16).Value = ""
        ''            End If



        ''            ra.Offset(1, j).Interior.Color = RGB(0, 255, 0)
        ''            ra.Offset(1, j).font.Color = RGB(0, 0, 0)
        ''            ra.Offset(1, j).HorizontalAlignment = Excel.Constants.xlCenter


        ''        Next
        ''    Next
        ''    '''''''''''''''''
        ''    With xlApp
        ''        .Columns(0).AutoFit()
        ''        .Columns(1).AutoFit()
        ''        .Columns(2).AutoFit()
        ''        .Columns(3).AutoFit()
        ''        .Columns(4).AutoFit()
        ''        .Columns(5).AutoFit()
        ''        .Columns(6).AutoFit()
        ''        .Columns(7).AutoFit()
        ''        .Columns(8).AutoFit()
        ''        .Columns(9).AutoFit()
        ''        .Columns(10).AutoFit()
        ''        .Columns(11).AutoFit()
        ''        .Columns(12).AutoFit()
        ''        .Columns(13).AutoFit()
        ''        .Columns(14).AutoFit()
        ''        .Columns(15).AutoFit()
        ''        .Columns(16).AutoFit()
        ''        .Columns(17).AutoFit()
        ''        .Columns(18).AutoFit()
        ''        .Columns(19).AutoFit()
        ''        .Columns(20).AutoFit()
        ''        .Columns(21).AutoFit()
        ''        .Columns(22).AutoFit()
        ''        .Columns(23).AutoFit()
        ''        .Visible = True
        ''    End With

        ''End With
        ''xlApp.col()
        ''Dim tolist(1) As Integer
        ''tolist(0) = 16
        ''tolist(1) = 18

        ''With xlApp.ActiveSheet.UsedRange
        ''    '.Sort(Key1:=.Columns(15), Key2:=.Columns(15), Header:=15)
        ''    .Subtotal(GroupBy:=4, Function:=-4157, TotalList:=tolist)
        ''End With


        ''ra.Rows(3).EntireRow.Delete()
        ''For j As Integer = 0 To DataGridView1.RowCount + 300
        ''    If Trim(ra.Offset(j, 1).Value) = "" And Trim(ra.Offset(j, 15).Value) <> "" Then
        ''        '  ra.Offset(j, 15).Value = .Columns(j).HeaderText      '.Rows(i).Cells(j).Value
        ''        '  ra.Offset(j, 15).Font.Name = "Arial"
        ''        ra.Offset(j, 15).Font.Bold = True
        ''        ra.Offset(j, 15).Font.Underline = True
        ''        ra.Offset(j, 15).Font.Size = 10
        ''        ra.Offset(j, 15).Font.Color = RGB(0, 0, 255)

        ''        ra.Offset(j, 17).Font.Bold = True
        ''        ra.Offset(j, 17).Font.Underline = True
        ''        ra.Offset(j, 17).Font.Size = 10

        ''        ra.Offset(j, 17).Font.Color = RGB(0, 0, 255)

        ''        ra.Offset(j, 3).Font.Bold = True
        ''        ra.Offset(j, 3).Font.Underline = True
        ''        ra.Offset(j, 3).Font.Size = 10
        ''        ra.Offset(j, 3).Font.Color = RGB(0, 0, 255)
        ''        For K As Integer = 0 To DataGridView1.ColumnCount - 1
        ''            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        ''            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        Next

        ''    End If

        ''Next

        ' ''oBook = xlApp.Workbooks.Add
        ' ''Dim strFileName As String = "D:\Loading " + String.Format("{0:yyyy-MM-dd}", Date.Now.Date) + ".xls"
        ' ''oBook.SaveAs(strFileName)
        ' ''xlApp.saveas(strFileName)
        ' ''oSheet = Nothing
        ' ''oBook = Nothing
        ' ''xlApp.Quit()
        ' ''xlApp = Nothing
        ' ''GC.Collect()
        ' ''oBook = xlApp.Workbooks.Add
        ' ''ra = oBook.ActiveCell
        ' ''xlsApp = New Excel.Application
        ' ''Dim xlsBook As Excel.Workbook
        ' ''Dim xlsSheet As Excel.Worksheet
        ' ''xlsBook = xlApp.Workbooks.Open(Filename:="D:\test.xls", UpdateLinks:=False, ReadOnly:=False)
        ' ''xlsSheet = xlsBook.ActiveSheet()
        ' ''xlsSheet.SaveAs(Filename:="D:\commaTEST", FileFormat:="xlCSV")
        ' ''xlApp.SaveAs(strFileName)
        ' ''xlApp.Workbooks.Open(strFileName)
        ' ''xlApp.Visible = True
        '' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' ''elApp = CreateObject("Excel.Application")

        ' ''Dim xlWorkBook As Excel.Workbook
        ' ''Dim xlWorkSheet As Excel.Worksheet
        ' ''Dim misValue As Object = System.Reflection.Missing.Value
        ' ''Dim i As Integer
        ' ''Dim j As Integer

        ' ''Dim strPath As String = "D:\Export\Excel\"
        ' ''xlApp = New Excel.Application

        ' ''Dim excel1 As New Microsoft.Office.Interop.Excel.ApplicationClass
        ' ''Dim wBook As Microsoft.Office.Interop.Excel.Workbook
        ' ''Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet

        ' ''wBook = excel.Workbooks.Add()
        ' ''wSheet = wBook.ActiveSheet()

        ' ''Dim colIndex As Integer = 0
        ' ''Dim rowIndex As Integer = 0

        ' ''For Each dc In dt.Columns
        ' ''    colIndex = colIndex + 1
        ' ''    Excel.Cells(1, colIndex) = dc.ColumnName
        ' ''Next

        ' ''For Each dr In dt.Rows
        ' ''    rowIndex = rowIndex + 1
        ' ''    colIndex = 0
        ' ''    For Each dc In dt.Columns
        ' ''        colIndex = colIndex + 1
        ' ''        Excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)

        ' ''    Next
        ' ''Next

        ' ''wSheet.Columns.AutoFit()
        ' ''Dim strFileName As String = "D:\ss.xls"
        ' ''Dim blnFileOpen As Boolean = False
        ' ''Try
        ' ''    Dim fileTemp As System.IO.FileStream = System.IO.File.OpenWrite(strFileName)
        ' ''    fileTemp.Close()
        ' ''Catch ex As Exception
        ' ''    blnFileOpen = False
        ' ''End Try

        ' ''If System.IO.File.Exists(strFileName) Then
        ' ''    System.IO.File.Delete(strFileName)
        ' ''End If

        ' ''wBook.SaveAs(strFileName)
        ' ''Excel.Workbooks.Open(strFileName)
        ' ''Excel.Visible = True

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnloadBut.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        'Dim xlApp As Object 'Excel.Application
        'Dim ra As Object 'Excel.Range
        'On Error Resume Next
        'xlApp = GetObject(, "Excel.Application")
        'If Err.Number <> 0 Then
        '    xlApp = New Microsoft.Office.Interop.Excel.Application
        '    xlApp = CreateObject("Excel.Application")
        'End If
        'xlApp.Visible = True
        'xlApp.Workbooks.Add()
        'ra = xlApp.ActiveCell


        Dim q As String
        q = ""
        q = "select CASE WHEN isnull(MAX(AddnoteDate), '-') = '' THEN '-' ELSE isnull(MAX(AddnoteDate), '-') END AS Addnotedate, "
        q &= " CASE WHEN isnull(MAX(LOAD_VEHICLE), '-') = '' THEN '-' ELSE isnull(MAX(LOAD_VEHICLE), '-') END AS Vehicle,"
        q &= " CASE WHEN isnull(MAX(LOAD_DRIVER), '-') = '' THEN '-' ELSE isnull(MAX(LOAD_DRIVER), '-') END AS Driver,"
        q &= " CASE WHEN isnull(MAX(PO_NO), '-') = '' THEN '-' ELSE isnull(MAX(PO_NO), '-') END AS PONo,"
        q &= " CASE WHEN isnull(MAX(Product_name), '-') = '' THEN '-' ELSE isnull(MAX(Product_name), '-') END AS Product,"
        q &= " CASE WHEN isnull(MAX(Customer_name), '-') = '' THEN '-' ELSE isnull(MAX(Customer_name), '-') END AS Vender,"
        q &= " CASE WHEN isnull(MAX(LOAD_DOfull), '-') = '' THEN '-' ELSE isnull(MAX(LOAD_DOfull), '-') END AS DONo,"
        q &= " CASE WHEN isnull(MAX(Invoice), '-') = '' THEN '-' ELSE isnull(MAX(Invoice), '-') END AS Invoice,"



        'q &= " max(LOAD_VEHICLE) as Vehicle,"
        'q &= " max(LOAD_DRIVER) as Driver,"
        'q &= " max(PO_NO) as PONo,"
        'q &= " max(Product_name) as Product,"

        'q &= " max(Customer_name) as Vender,"
        'q &= " max(LOAD_DOfull) as DONo,"
        'q &= " max(Invoice) as Invoice,"
        q &= " max(cast(Weightin_time as varchar(8))) as WeightinTime,"
        q &= " max(cast(Weightout_time as varchar(8))) as WeightOutTime,"
        q &= " max(Takentime) as TakenTime,"
        q &= " max(WeightIN) as WeightIN,"
        q &= " max(WeightOut) as WeightOut,"
        q &= " max(WeightTotal) as WeightTotal,"
        q &= " max(Load_preset) as DoPreset,"
        '   q &= " max(WeightTol) as WeightTol,"
        q &= " max(WeightTotal)-max(Load_preset) as WeightDIFF,"
        q &= " max(LOAD_DID) as LOAD_DID,"

        q &= " (CASE WHEN max(Load_preset)='0' then '0' else  ((max(Load_preset)-max(WeightTotal))*100) / max(Load_preset) end) * -1 as PERCENTDIFF,"

        q &= " CASE WHEN isnull(MAX(LOCATION_CODE), '-') = '' THEN '-' ELSE isnull(MAX(LOCATION_CODE), '-') END AS Tank,"
        q &= " CASE WHEN isnull(MAX(TRANSPORTER), '-')  = '' THEN '-' ELSE isnull(MAX(TRANSPORTER), '-') END AS TRANSPORTER "

        'q &= " max(LOCATION_CODE) as Tank"

        q &= " FROM V_UnLOADINGNOTE "
        q &= " where Addnotedate between '" + String.Format("{0:yyyy-M-d}", printdate.Value) + "'" + ""
        q &= " and '" + String.Format("{0:yyyy-M-d}", Enddate.Value) + "'" + " and load_status=3 "
        If CbProduct.Text <> "" Then q &= "   and Product_name='" + CbProduct.Text + "'" + ""

        'q &= " where addnotedate='" + String.Format("{0:yyyy-MM-dd}", printdate.Value) + "'" + ""
        q &= " group by reference order by AddnoteDate, Product,Vender"
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim ds As New DataSet()
        Dim dt As New DataTable
        ' da1.Fill(dt1)
        '  DBGrid1.DataSource = dt1
        ' DataGridView1.DataSource = dt1


        Dim Myreport As New ReportDocument
        Myreport = New ReportDocument
        Dim Myreport2 As New ReportDocument
        Myreport2 = New ReportDocument

        da.Fill(ds, "V_UnLOADINGNOTE")

        Myreport.Load("ExportUNLoadReport.rpt")
        Myreport2.Load("ExportUNLoadReportNotsum.rpt")

        'Myreport.Load("weightticket.rpt")

        Myreport.SetDataSource(ds.Tables("V_UnLOADINGNOTE"))
        Myreport2.SetDataSource(ds.Tables("V_UnLOADINGNOTE"))
        My.Computer.FileSystem.CreateDirectory("D:\TASExport\UNLoading\" & String.Format("{0:yyyy-M-d}", Now))
        Dim Oexcel As New Microsoft.Office.Interop.Excel.ApplicationClass
        Dim wBook1, wBook2 As Microsoft.Office.Interop.Excel.Workbook
        'Dim wSheet As New Microsoft.Office.Interop.Excel.Worksheet
        Dim SSname1 As String = "D:\TASExport\UNLoading\" & String.Format("{0:yyyy-M-d}", Now) & "\" & String.Format("{0:yyyy_M_d_hhmmss}", Now) & "_1.xls"
        Dim SSname2 As String = "D:\TASExport\UNLoading\" & String.Format("{0:yyyy-M-d}", Now) & "\" & String.Format("{0:yyyy_M_d_hhmmss}", Now) & "_2.xls"
        Myreport.ExportToDisk(ExportFormatType.Excel, SSname1)
        Myreport2.ExportToDisk(ExportFormatType.Excel, SSname2)

        wBook1 = Oexcel.Workbooks.Open(SSname1)
        wBook2 = Oexcel.Workbooks.Open(SSname2)

        wBook1.Worksheets(1).name = "SUM UNLOADING REPORT"
        wBook2.Worksheets(1).name = "UNLOADING REPORT"
        wBook2.Worksheets.Copy(After:=wBook1.Worksheets(1))
        wBook1.Close(SaveChanges:=True)
        wBook2.Close(SaveChanges:=True)
        wBook1 = Nothing
        wBook2 = Nothing
        ' System.IO.File.Delete(SSname1)
        System.IO.File.Delete(SSname2)

        Oexcel.Workbooks.Open(SSname1)
        Oexcel.Columns.AutoFit()
        Oexcel.Visible = True
        conn.Close()
        conn.Dispose()
        da.Dispose()
        ds.Dispose()

        'Dim conn As SqlClient.SqlConnection = GetConnection()
        'conn.Open()
        'Dim xlApp As Object 'Excel.Application
        'Dim ra As Object 'Excel.Range
        'On Error Resume Next
        'xlApp = GetObject(, "Excel.Application")
        'If Err.Number <> 0 Then
        '    xlApp = New Microsoft.Office.Interop.Excel.Application
        '    xlApp = CreateObject("Excel.Application")
        'End If
        'xlApp.Visible = True
        'xlApp.Workbooks.Add()
        'ra = xlApp.ActiveCell


        'Dim q As String
        'q = ""
        'q = "select max(Addnotedate) as Addnotedate, "
        'q &= " max(LOAD_VEHICLE) as Vehicle,"
        'q &= " max(LOAD_DRIVER) as Driver,"
        'q &= " max(PO_NO) as PONo,"
        'q &= " max(Product_name) as Product,"
        'q &= " max(Customer_name) as Vender,"
        'q &= " max(LOAD_DOfull) as DONo,"
        'q &= " max(Invoice) as Invoice,"
        'q &= " max(cast(Weightin_time as varchar(8))) as WeightinTime,"
        'q &= " max(cast(Weightout_time as varchar(8))) as WeightOutTime,"
        'q &= " max(Takentime) as TakenTime,"
        'q &= " max(WeightIN) as WeightIN,"
        'q &= " max(WeightOut) as WeightOut,"
        'q &= " max(WeightTotal) as WeightTotal,"
        'q &= " max(Load_preset) as DoPreset,"
        ''   q &= " max(WeightTol) as WeightTol,"
        'q &= " max(Load_preset)-max(WeightTotal) as WeightDIFF,"
        'q &= " ((max(Load_preset)-max(WeightTotal))*100) / max(Load_preset) as PERCENTDIFF,"
        'q &= " max(LOCATION_CODE) as Tank"

        'q &= " FROM V_UnLOADINGNOTE "
        'q &= " where Addnotedate between '" + String.Format("{0:yyyy-M-d}", printdate.Value) + "'" + ""
        'q &= " and '" + String.Format("{0:yyyy-M-d}", Enddate.Value) + "'" + " and load_status=3 "
        'If CbProduct.Text <> "" Then q &= "   and Product_name='" + CbProduct.Text + "'" + ""

        ''q &= " where addnotedate='" + String.Format("{0:yyyy-MM-dd}", printdate.Value) + "'" + ""
        'q &= " group by reference order by Addnotedate desc,Reference "
        'Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds1 As New DataSet()
        'Dim dt1 As New DataTable
        'da1.Fill(dt1)
        'DBGrid1.DataSource = dt1
        'DataGridView1.DataSource = dt1

        'With DataGridView1
        '    ra.offset(0, 7).value = "Unloading Date :" + String.Format("{0:dd/MM/yyyy}", printdate.Value) + "-" + String.Format("{0:dd/MM/yyyy}", Enddate.Value)
        '    ra.Offset(0, 7).Font.Name = "Arial"
        '    ra.Offset(0, 7).Font.FontStyle = "Regular"
        '    ra.Offset(0, 7).Font.Size = 16


        '    For j As Integer = 0 To .ColumnCount - 1
        '        ra.Offset(1, j).Font.Name = "Arial"
        '        ra.Offset(1, j).Font.FontStyle = "Bold" '"Regular"
        '        ra.Offset(1, j).Font.Size = 10
        '        ' ra.Offset(1, j).Borders = "ALL"
        '        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        '        ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        '        ' ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        '        ' ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        '        ' ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        '        ' ra.Offset(1, j).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
        '        Select Case UCase(.Columns(j).HeaderText)
        '            Case UCase("Addnotedate")
        '                ra.Offset(1, j).Value = "วันที่"
        '            Case UCase("Vehicle")
        '                ra.Offset(1, j).Value = "เลขทะเบียนรถ"
        '            Case UCase("Driver")
        '                ra.Offset(1, j).Value = "ชื่อคนขับ"
        '            Case UCase("PONo")
        '                ra.Offset(1, j).Value = "เลขที่ใบสั่งซื้อ"
        '            Case UCase("Product")
        '                ra.Offset(1, j).Value = "ชื่อผลิตภัณฑ์"
        '            Case UCase("Vender")
        '                ra.Offset(1, j).Value = "Vendor name"
        '            Case UCase("DONo")
        '                ra.Offset(1, j).Value = "เลขที่ D/O"
        '            Case UCase("Invoice")
        '                ra.Offset(1, j).Value = "เลขที่ Invoice"
        '            Case UCase("WeightinTime")
        '                ra.Offset(1, j).Value = "เวลาชั่งเข้า"
        '            Case UCase("WeightIN")
        '                ra.Offset(1, j).Value = "น้ำหนักชั่งเข้า"
        '            Case UCase("WeightOutTime")
        '                ra.Offset(1, j).Value = "เวลาชั่งออก"
        '            Case UCase("WeightOut")
        '                ra.Offset(1, j).Value = "น้ำหนักชั่งออก"
        '            Case UCase("WeightTotal")
        '                ra.Offset(1, j).Value = "น้ำหนักเข้า-ออก"
        '            Case UCase("DoPreset")
        '                ra.Offset(1, j).Value = "น้ำหนักตามDO"


        '            Case UCase("WeightDIFF")
        '                ra.Offset(1, j).Value = "น้ำหนักแตกต่าง"
        '            Case UCase("PERCENTDIFF")
        '                ra.Offset(1, j).Value = "% ความแตกต่าง"

        '            Case UCase("TakenTime")
        '                ra.Offset(1, j).Value = "เวลาที่อยู่ในโรงงาน"

        '            Case UCase("Tank")
        '                ra.Offset(1, j).Value = "Tank"
        '            Case Else
        '                ra.Offset(1, j).Value = .Columns(j).HeaderText      '.Rows(i).Cells(j).Value   
        '        End Select



        '        ra.Offset(1, j).Interior.Color = RGB(0, 255, 0)
        '        ra.Offset(1, j).font.Color = RGB(0, 0, 0)
        '        ra.Offset(1, j).HorizontalAlignment = Excel.Constants.xlCenter

        '    Next


        '    For i As Integer = 0 To .RowCount - 2
        '        For j As Integer = 0 To .ColumnCount - 1
        '            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous

        '            ra.Offset(i + 2, j).Value = .Rows(i).Cells(j).Value
        '            ra.Offset(i + 2, j).Font.Name = "Arial"
        '            ra.Offset(i + 2, j).Font.FontStyle = "Regular"
        '            ra.Offset(i + 2, j).Font.Size = 10

        '            ra.Offset(i + 2, 0).NumberFormat = "dd-MM-eeee"
        '            ra.Offset(i + 2, 3).NumberFormat = "@"
        '            ra.Offset(i + 2, 6).NumberFormat = "@"
        '            ra.Offset(i + 2, 7).NumberFormat = "@"
        '            ra.Offset(i + 2, 9).NumberFormat = "@"
        '            ra.Offset(i + 2, 10).NumberFormat = "hh:mm:ss"
        '            ra.Offset(i + 2, 11).NumberFormat = "#,##0"
        '            ra.Offset(i + 2, 12).NumberFormat = "#,##0"
        '            ra.Offset(i + 2, 13).NumberFormat = "#,##0.00"
        '            ra.Offset(i + 2, 14).NumberFormat = "#,##0"
        '            ra.Offset(i + 2, 15).NumberFormat = "#,##0"
        '            ra.Offset(i + 2, 16).NumberFormat = "#,##0.00"
        '        Next
        '    Next
        '    With xlApp
        '        .Columns(0).AutoFit()
        '        .Columns(1).AutoFit()
        '        .Columns(2).AutoFit()
        '        .Columns(3).AutoFit()
        '        .Columns(4).AutoFit()
        '        .Columns(5).AutoFit()
        '        .Columns(6).AutoFit()
        '        .Columns(7).AutoFit()
        '        .Columns(8).AutoFit()
        '        .Columns(9).AutoFit()
        '        .Columns(10).AutoFit()
        '        .Columns(11).AutoFit()
        '        .Columns(12).AutoFit()
        '        .Columns(13).AutoFit()
        '        .Columns(14).AutoFit()
        '        .Columns(15).AutoFit()
        '        .Columns(16).AutoFit()
        '        .Columns(17).AutoFit()
        '        .Columns(18).AutoFit()
        '        .Columns(19).AutoFit()
        '        .Visible = True
        '    End With
        'End With

        ''Dim tolist(1) As Integer
        ''tolist(0) = 15
        ''tolist(1) = 17

        ''With xlApp.ActiveSheet.UsedRange
        ''    '.Sort(Key1:=.Columns(15), Key2:=.Columns(15), Header:=15)
        ''    .Subtotal(GroupBy:=4, Function:=-4157, TotalList:=tolist)
        ''End With

        ''For i As Integer = DataGridView1.RowCount - 2 To DataGridView1.RowCount
        ''    For j As Integer = 0 To DataGridView1.ColumnCount - 1
        ''        ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        ''        ra.Offset(i + 2, j).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous


        ''    Next
        ''Next
        ''ra.Offset(DataGridView1.RowCount + 1, 0)



        'With xlApp.ActiveSheet.UsedRange
        '    '.Sort(Key1:=.Columns(15), Key2:=.Columns(15), Header:=15)
        '    .Subtotal(GroupBy:=5, Function:=-4157, TotalList:=14)
        'End With

        'ra.Rows(3).EntireRow.Delete()
        'For j As Integer = 0 To DataGridView1.RowCount + 300
        '    If Trim(ra.Offset(j, 1).Value) = "" And Trim(ra.Offset(j, 13).Value) <> "" Then
        '        '  ra.Offset(j, 15).Value = .Columns(j).HeaderText      '.Rows(i).Cells(j).Value
        '        '  ra.Offset(j, 15).Font.Name = "Arial"
        '        ra.Offset(j, 13).Font.Bold = True
        '        ra.Offset(j, 13).Font.Underline = True
        '        ra.Offset(j, 13).Font.Size = 10
        '        ra.Offset(j, 13).Font.Color = RGB(0, 0, 255)
        '        ra.Offset(j, 4).Font.Bold = True
        '        ra.Offset(j, 4).Font.Underline = True
        '        ra.Offset(j, 4).Font.Size = 10
        '        ra.Offset(j, 4).Font.Color = RGB(0, 0, 255)

        '        For K As Integer = 0 To DataGridView1.ColumnCount - 1
        '            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
        '            ra.Offset(j, K).Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
        '        Next

        '    End If

        'Next


    End Sub

    Private Sub Report_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
        Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
        Me.V_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_LOADINGNOTE)
        With DBGrid1
            .ReadOnly = True
            .MultiSelect = False
            '.Dock = DockStyle.Fill
            .RowHeadersVisible = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
        End With
        printdate.Value = String.Format("{0:yyyy-M-d 00:00:00}", Now)
        Enddate.Value = String.Format("{0:yyyy-M-d 23:59:59}", Now)


        BTSAPReport.Enabled = CheckPrintPermisstion(sender.tag)
        BtScapPrint.Enabled = CheckPrintPermisstion(sender.tag)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        Dim q As String
        q = ""
        q = "select max(Addnotedate) as Addnotedate, "
        q &= " max(LOAD_VEHICLE) as VEHICLE,"
        q &= " max(LOAD_DRIVER) as DRIVER,"
        q &= " max(Product_name) as Product,"
        q &= " max(LOAD_TANK) as TANK,"
        q &= " max(Customer_name) as Customer,"
        q &= " max(COA) as COA,"
        q &= " max(LC_SEAL) as SEAL,"
        q &= " max(cast(Weightin_time as varchar(20))) as WeightinTime,"
        q &= " max(WeightIN) as WeightIN,"
        q &= " max(WeightOut_time) as WeightOutTime,"
        q &= " max(WeightOut) as WeightOut,"
        q &= " max(WeightTotal) as WeightTotal,"
        q &= " max(Density) as Density,"
        q &= " max(Takentime) as TakenTime "
        q &= " FROM V_LOADINGNOTE "
        q &= " where Addnotedate='" + String.Format("{0:yyyy-MM-dd}", printdate.Value) + "'" + ""
        q &= " group by reference order by Addnotedate desc,Reference "
        Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        Dim ds1 As New DataSet()
        Dim dt1 As New DataTable
        da1.Fill(dt1)
        DBGrid1.DataSource = dt1
        DataGridView1.DataSource = dt1

        'With DataGridView1
        '    .Columns(0).Width = .Width * 0.2
        '    .Columns(1).Width = .Width * 0.5
        '    .Columns(2).Width = .Width * 0.1
        '    .Columns(3).Width = .Width * 0.2
        '    .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        '    .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'End With


        DataGridView1.SelectAll()
        Clipboard.SetText( _
          DataGridView1.GetClipboardContent().GetText, _
          TextDataFormat.Text)
        DataGridView1.ClearSelection()

        Dim xlapp As Object
        On Error Resume Next
        xlapp = GetObject(, "Excel.Application")
        If Err.Number <> 0 Then
            xlapp = CreateObject("Excel.Application")
        End If
        xlapp.Visible = True
        xlapp.ScreenUpdating = False
        With xlapp.Workbooks.Add
            With .ActiveSheet
                .Paste()
                .Columns.Autofit()
                .Rows(1).Font.Bold = True
                .Range("A2").Select()
            End With
        End With
        xlapp.CutCopyMode = False
        xlapp.ScreenUpdating = True

        'DataGridView1.SelectAll()
        'Clipboard.SetText( _
        '  DataGridView1.GetClipboardContent().GetText, _
        '  TextDataFormat.Text)
        'DataGridView1.ClearSelection()

        'With DataGridView1
        '    For i As Integer = 0 To .RowCount - 1
        '        For j As Integer = 0 To .ColumnCount - 1
        '            ra.Offset(i, j).Value = .Rows(i).Cells(j).Value
        '        Next
        '    Next
        'End With
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        f05_01_Advisenote.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Unloading.Show()
    End Sub

    Private Sub Report_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        LoadingBut.Visible = False
        UnloadBut.Visible = False
        TruckLoading.Visible = False
        TruckUnloading.Visible = False
        Label1.Text = "Report Print"
        CbProduct.SelectedIndex = -1
        If treeManu.reportID = 1 Then
            LoadingToolStripMenuItem1_Click(sender, e)
        End If

        If treeManu.reportID = 2 Then
            UnloadingToolStripMenuItem_Click(sender, e)
        End If

        If treeManu.reportID = 3 Then
            TruckLoadingReportToolStripMenuItem_Click(sender, e)
        End If

        If treeManu.reportID = 4 Then
            TruckUnloadingReportToolStripMenuItem_Click(sender, e)
        End If
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TruckLoading.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        Dim ref As String
        'ref = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("reference").ToString()
        ref = 12751

        Dim Myreport As New ReportDocument
        Myreport = New ReportDocument
        Dim sql As String
        'sql = "Select * from V_Truckloadingreport where  Addnotedate='" + String.Format("{0:yyyy-MM-dd}", printdate.Value) + "'" + " and load_status=3"
        sql = "Select  * from V_Truckloadingreport "
        sql &= " where loaddate='" + String.Format("{0:yyyy-M-d}", printdate.Value) + "'" + ""
        sql &= " order by load_id desc "
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        Dim ds As New DataSet()
        Dim dt As New DataTable
        da.Fill(dt)

        If dt.Rows.Count = 0 Then
            MessageBox.Show("ไม่มีข้อมูล", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        Else
            da.Fill(ds, "V_Truckloadingreport")
            Myreport.Load("Truckloadingreport.rpt")
            Myreport.SetDataSource(ds.Tables("V_Truckloadingreport"))
            ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            ReportPrint.Show()
        End If

    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TruckUnloading.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()


        Dim Myreport As New ReportDocument
        Myreport = New ReportDocument
        Dim sql As String
        sql = "Select  * from V_Truckunloadingreport "
        sql &= " where loaddate='" + String.Format("{0:yyyy-M-d}", printdate.Value) + "'" + ""
        sql &= " order by load_id desc "
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        Dim ds As New DataSet()
        Dim dt As New DataTable
        da.Fill(dt)

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        Else
            da.Fill(ds, "V_Truckunloadingreport")
            Myreport.Load("Truckunloadingreport.rpt")
            Myreport.SetDataSource(ds.Tables("V_TruckUnloadingreport"))
            ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            ReportPrint.Show()
        End If
    End Sub

    Private Sub TruckLoadingReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TruckLoadingReportToolStripMenuItem.Click
        LoadingBut.Visible = False
        UnloadBut.Visible = False
        TruckLoading.Visible = True
        TruckUnloading.Visible = False
        Label1.Text = "Truck Loading Report"
    End Sub

    Private Sub TruckUnloadingReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TruckUnloadingReportToolStripMenuItem.Click
        LoadingBut.Visible = False
        UnloadBut.Visible = False
        TruckLoading.Visible = False
        TruckUnloading.Visible = True
        Label1.Text = "Truck Unloading Report"
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub SAPReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SAPReportToolStripMenuItem.Click
        BTSAPReport.BringToFront()
        LoadingBut.Visible = False
        UnloadBut.Visible = False
        BTSAPReport.Visible = True
        BtScapPrint.Visible = False
        Label1.Text = "SAP Report"
    End Sub

    Private Sub BTSAPReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTSAPReport.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()


        Dim Myreport As New ReportDocument
        Myreport = New ReportDocument
        Dim sql As String
        sql = "Select  * from V_SAPReport "
        sql &= " where do_no is not null and  do_no<>'' and (CAST( convert(varchar(10),Weightin_Date,21)+' ' +convert(varchar(10),Weightin_time,120) as Datetime)) between  '" + String.Format("{0:yyyy-M-d HH:mm:00}", printdate.Value) + "'" + " and '" +
        String.Format("{0:yyyy-M-d HH:mm:59}", Enddate.Value) + "'" + ""
        If CbProduct.Text <> "" Then sql &= " and Product_name='" + CbProduct.Text + "'" + ""
        sql &= " order by Ship_NO,do_no  "
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        Dim ds As New DataSet()
        Dim dt As New DataTable
        da.Fill(dt)

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        Else
            da.Fill(ds, "V_SAPReport")
            ds.Tables("V_SAPReport").TableName = "V_LOADINGNOTE"
            Myreport.Load("DoReport.rpt")
            Myreport.SetDataSource(ds.Tables("V_LOADINGNOTE"))
            ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            ReportPrint.Show()
        End If
        conn.Close()
        conn.Dispose()
        dt.Dispose()
        ds.Dispose()
        da.Dispose()
    End Sub

    Private Sub SCAPReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SCAPReportToolStripMenuItem.Click
        BtScapPrint.BringToFront()
        LoadingBut.Visible = False
        UnloadBut.Visible = False
        BTSAPReport.Visible = False
        BtScapPrint.Visible = True
        Label1.Text = "Scap Report"
    End Sub

    Private Sub BtScapPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtScapPrint.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()


        Dim Myreport As New ReportDocument
        Myreport = New ReportDocument
        Dim sql As String
        sql = "Select  * from V_SAPReport "
        sql &= " where (do_no is  null or  do_no='') and (CAST( convert(varchar(10),Weightin_Date,21)+' ' +convert(varchar(10),Weightin_time,120) as Datetime)) between  '" + String.Format("{0:yyyy-M-d HH:mm:00}", printdate.Value) + "'" + " and '" +
        String.Format("{0:yyyy-M-d HH:mm:59}", Enddate.Value) + "'" + ""
        If CbProduct.Text <> "" Then sql &= " and Product_name='" + CbProduct.Text + "'" + ""
        sql &= " order by do_no  "
        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        Dim ds As New DataSet()
        Dim dt As New DataTable
        da.Fill(dt)
        dt.TableName = "V_LOADINGNOTE"
        If dt.Rows.Count = 0 Then
            MessageBox.Show("No Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        Else
            da.Fill(ds, "V_LOADINGNOTE")
            Myreport.Load("ScapReport.rpt")
            Myreport.SetDataSource(ds.Tables("V_LOADINGNOTE"))
            ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            ReportPrint.Show()
        End If
        conn.Close()
        conn.Dispose()
        dt.Dispose()
        ds.Dispose()
        da.Dispose()
        'Myreport.Dispose()

    End Sub

    Private Sub LoadingReportToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LoadingReportToolStripMenuItem.Click
        LoadingBut.BringToFront()
        LoadingBut.Visible = True
        UnloadBut.Visible = False
        BTSAPReport.Visible = False
        BtScapPrint.Visible = False

        Label1.Text = "Loading Report"
    End Sub

    Private Sub UnloadingReportToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles UnloadingReportToolStripMenuItem.Click
        UnloadBut.BringToFront()
        LoadingBut.Visible = False
        UnloadBut.Visible = True
        BTSAPReport.Visible = False
        BtScapPrint.Visible = False
        Label1.Text = "Unloading Report"
    End Sub
End Class