﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class f05_02_Advisenote2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewDecimalColumn1 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn1 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDateTimeColumn2 As Telerik.WinControls.UI.GridViewDateTimeColumn = New Telerik.WinControls.UI.GridViewDateTimeColumn()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(f05_02_Advisenote2))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupAdnote = New System.Windows.Forms.GroupBox()
        Me.GroupGeneral = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.CBLoadingPoint = New Telerik.WinControls.UI.RadMultiColumnComboBox()
        Me.TLOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.EDLoaddate = New System.Windows.Forms.TextBox()
        Me.EDDriver = New System.Windows.Forms.ComboBox()
        Me.TDRIVERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EDTruck = New System.Windows.Forms.ComboBox()
        Me.TTRUCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CBTankfarm = New System.Windows.Forms.ComboBox()
        Me.TTANKFARMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.weightouttime = New System.Windows.Forms.TextBox()
        Me.BtVehicleEdit = New System.Windows.Forms.Button()
        Me.CBCardNO = New System.Windows.Forms.ComboBox()
        Me.VCARDFREEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Weightintime = New System.Windows.Forms.TextBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.EDSaleQtr = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.EDSale_Unit = New System.Windows.Forms.TextBox()
        Me.EDDO_item = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Soitem = New System.Windows.Forms.TextBox()
        Me.BTIMPORT_SAP = New System.Windows.Forms.Button()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Sono = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.RadioLiquid = New System.Windows.Forms.RadioButton()
        Me.RadioBig = New System.Windows.Forms.RadioButton()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.ShipMent_Type = New System.Windows.Forms.ComboBox()
        Me.TSHIPMENTTYPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Calculate = New System.Windows.Forms.TextBox()
        Me.Tank = New System.Windows.Forms.TextBox()
        Me.Density = New System.Windows.Forms.TextBox()
        Me.Temp = New System.Windows.Forms.TextBox()
        Me.MeterRead = New System.Windows.Forms.TextBox()
        Me.WeightTotal = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Company_sapcode = New System.Windows.Forms.ComboBox()
        Me.TCOMPANYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.P_Weight = New System.Windows.Forms.ComboBox()
        Me.TPACKINGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PackingId = New System.Windows.Forms.ComboBox()
        Me.Shipment_id = New System.Windows.Forms.ComboBox()
        Me.EDLoadDTime = New System.Windows.Forms.TextBox()
        Me.Updatedate = New System.Windows.Forms.TextBox()
        Me.EDLoadID = New System.Windows.Forms.TextBox()
        Me.TruckId = New System.Windows.Forms.ComboBox()
        Me.Prono = New System.Windows.Forms.ComboBox()
        Me.ProductId = New System.Windows.Forms.ComboBox()
        Me.TProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusId = New System.Windows.Forms.ComboBox()
        Me.TSTATUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TruckCompanyId = New System.Windows.Forms.ComboBox()
        Me.DriverID = New System.Windows.Forms.ComboBox()
        Me.CustomerID = New System.Windows.Forms.ComboBox()
        Me.TCustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShipperId = New System.Windows.Forms.ComboBox()
        Me.TSHIPPERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DateeditTemp = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ShipmentNo = New System.Windows.Forms.TextBox()
        Me.PackingWeight = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.SealCount = New System.Windows.Forms.TextBox()
        Me.Quantity = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Packing = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.EDPreset = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Status = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.EDCustomer = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.AccessCode = New System.Windows.Forms.TextBox()
        Me.Seal_No = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GiDate = New System.Windows.Forms.DateTimePicker()
        Me.Dateedit = New System.Windows.Forms.DateTimePicker()
        Me.Container = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Coa = New System.Windows.Forms.TextBox()
        Me.EDDONo = New System.Windows.Forms.TextBox()
        Me.EDREf = New System.Windows.Forms.TextBox()
        Me.EDShipper = New System.Windows.Forms.ComboBox()
        Me.EDForword = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.EDTruckCapa = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.LawWeightout = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.editweightout = New System.Windows.Forms.Button()
        Me.editweightin = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.LawWeightIn = New System.Windows.Forms.TextBox()
        Me.UpdateWeightIn = New System.Windows.Forms.TextBox()
        Me.UpdateWeightOut = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupUnload = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.BtUnload = New System.Windows.Forms.Button()
        Me.BindingNavigator2 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.V_LoadingnoteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.EDFillter = New System.Windows.Forms.ToolStripTextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.SelectDate = New System.Windows.Forms.DateTimePicker()
        Me.GroupBt = New System.Windows.Forms.GroupBox()
        Me.BtWorkPrint = New System.Windows.Forms.Button()
        Me.weightoutprint = New System.Windows.Forms.Button()
        Me.GroupBtadd = New System.Windows.Forms.GroupBox()
        Me.PrintBut = New System.Windows.Forms.Button()
        Me.EditData = New System.Windows.Forms.Button()
        Me.Adddata = New System.Windows.Forms.Button()
        Me.GroupPort = New System.Windows.Forms.GroupBox()
        Me.BTEDIT_SAP = New System.Windows.Forms.Button()
        Me.WeightOut = New System.Windows.Forms.Button()
        Me.WeightIn = New System.Windows.Forms.Button()
        Me.Bcancel = New System.Windows.Forms.Button()
        Me.Edit = New System.Windows.Forms.Button()
        Me.Bsave = New System.Windows.Forms.Button()
        Me.WeightScal = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.BTDownload = New System.Windows.Forms.Button()
        Me.DBGridAddnote = New System.Windows.Forms.DataGridView()
        Me.LOAD_DOfull = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_DID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOADDATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Do_status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Customer_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Product_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_VEHICLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_DRIVER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LCPRESETDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LCBAYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOAD_TANK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.do_postdate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReferenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PTruck = New System.Windows.Forms.Panel()
        Me.Imagetruck = New System.Windows.Forms.PictureBox()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.BTG = New Telerik.WinControls.UI.RadButton()
        Me.EllipseShape1 = New Telerik.WinControls.EllipseShape()
        Me.BTR = New Telerik.WinControls.UI.RadButton()
        Me.InG = New Telerik.WinControls.UI.RadPanel()
        Me.InR = New Telerik.WinControls.UI.RadPanel()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape6 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.InGrean = New System.Windows.Forms.PictureBox()
        Me.GDetail = New System.Windows.Forms.GroupBox()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.GPreset = New System.Windows.Forms.GroupBox()
        Me.PRESETNO1 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.GProduct = New System.Windows.Forms.GroupBox()
        Me.RadPanel2 = New Telerik.WinControls.UI.RadPanel()
        Me.TrackBarUThumbShape1 = New Telerik.WinControls.UI.TrackBarUThumbShape()
        Me.RadPanel3 = New Telerik.WinControls.UI.RadPanel()
        Me.RadPanel4 = New Telerik.WinControls.UI.RadPanel()
        Me.RadPanel5 = New Telerik.WinControls.UI.RadPanel()
        Me.RadPanel6 = New Telerik.WinControls.UI.RadPanel()
        Me.RadPanel1 = New Telerik.WinControls.UI.RadPanel()
        Me.Edremark2 = New System.Windows.Forms.RichTextBox()
        Me.GroupBatchtable = New System.Windows.Forms.GroupBox()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Btadd = New System.Windows.Forms.ToolStripButton()
        Me.TBATCHLOSTTASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BtDelete = New System.Windows.Forms.ToolStripButton()
        Me.Btfirst = New System.Windows.Forms.ToolStripButton()
        Me.Btprevious = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Btnext = New System.Windows.Forms.ToolStripButton()
        Me.BtLast = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BtEdit = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.MeterGrid = New System.Windows.Forms.DataGridView()
        Me.DO_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DO_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MAT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stock_quantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BATCH_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STOR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PLANT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Higher_Level = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupProduct = New System.Windows.Forms.GroupBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.CbProname = New System.Windows.Forms.ComboBox()
        Me.Meter = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.OrderBut = New System.Windows.Forms.Button()
        Me.Order = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Bay = New System.Windows.Forms.ComboBox()
        Me.Product = New System.Windows.Forms.ComboBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Edremark = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.T_TRUCKTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTableAdapter()
        Me.T_COMPANYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter()
        Me.T_DRIVERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter()
        Me.V_LOADINGNOTETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_LOADINGNOTETableAdapter()
        Me.T_SHIPPERTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter()
        Me.T_CustomerTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_CustomerTableAdapter()
        Me.T_SHIPMENT_TYPETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SHIPMENT_TYPETableAdapter()
        Me.T_STATUSTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_STATUSTableAdapter()
        Me.T_PACKINGTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_PACKINGTableAdapter()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T_ProductTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter()
        Me.V_CARDFREETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_CARDFREETableAdapter()
        Me.T_TANKFARMTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_TANKFARMTableAdapter()
        Me.VCARDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.V_CARDTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_CARDTableAdapter()
        Me.MediaShape1 = New Telerik.WinControls.Tests.MediaShape()
        Me.DonutShape1 = New Telerik.WinControls.Tests.DonutShape()
        Me.CustomShape1 = New Telerik.WinControls.OldShapeEditor.CustomShape()
        Me.TrackBarDThumbShape1 = New Telerik.WinControls.UI.TrackBarDThumbShape()
        Me.TrackBarLThumbShape1 = New Telerik.WinControls.UI.TrackBarLThumbShape()
        Me.TrackBarRThumbShape1 = New Telerik.WinControls.UI.TrackBarRThumbShape()
        Me.TUNITBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TUNITTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.TUNITTableAdapter()
        Me.DateFormatBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DateFormatTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.DateFormatTableAdapter()
        Me.T_LOTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_LOTableAdapter()
        Me.TSETTINGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_SETTINGTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.T_SETTINGTableAdapter()
        Me.QaShape1 = New Telerik.WinControls.Tests.QAShape()
        Me.RoundRectShape1 = New Telerik.WinControls.RoundRectShape(Me.components)
        Me.BreezeTheme1 = New Telerik.WinControls.Themes.BreezeTheme()
        Me.TSETTINGPLANTTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.TSETTINGPLANTTableAdapter()
        Me.TSETTINGPLANTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupAdnote.SuspendLayout()
        Me.GroupGeneral.SuspendLayout()
        CType(Me.CBLoadingPoint, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TLOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TTRUCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TTANKFARMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VCARDFREEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox16.SuspendLayout()
        CType(Me.TSHIPMENTTYPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TPACKINGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSTATUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupUnload.SuspendLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator2.SuspendLayout()
        CType(Me.V_LoadingnoteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBt.SuspendLayout()
        Me.GroupBtadd.SuspendLayout()
        Me.GroupPort.SuspendLayout()
        CType(Me.DBGridAddnote, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PTruck.SuspendLayout()
        CType(Me.Imagetruck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.BTG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BTR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InGrean, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GDetail.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GPreset.SuspendLayout()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBatchtable.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.TBATCHLOSTTASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupProduct.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.VCARDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TUNITBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFormatBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSETTINGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TSETTINGPLANTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupAdnote
        '
        Me.GroupAdnote.Controls.Add(Me.GroupGeneral)
        Me.GroupAdnote.Controls.Add(Me.GroupUnload)
        Me.GroupAdnote.Controls.Add(Me.GroupBt)
        Me.GroupAdnote.Controls.Add(Me.DBGridAddnote)
        Me.GroupAdnote.Controls.Add(Me.PTruck)
        Me.GroupAdnote.Controls.Add(Me.GDetail)
        Me.GroupAdnote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupAdnote.Enabled = False
        Me.GroupAdnote.Location = New System.Drawing.Point(0, 0)
        Me.GroupAdnote.Name = "GroupAdnote"
        Me.GroupAdnote.Size = New System.Drawing.Size(1370, 750)
        Me.GroupAdnote.TabIndex = 0
        Me.GroupAdnote.TabStop = False
        '
        'GroupGeneral
        '
        Me.GroupGeneral.BackColor = System.Drawing.Color.Transparent
        Me.GroupGeneral.Controls.Add(Me.Label21)
        Me.GroupGeneral.Controls.Add(Me.Label20)
        Me.GroupGeneral.Controls.Add(Me.CBLoadingPoint)
        Me.GroupGeneral.Controls.Add(Me.Button3)
        Me.GroupGeneral.Controls.Add(Me.Label19)
        Me.GroupGeneral.Controls.Add(Me.Label5)
        Me.GroupGeneral.Controls.Add(Me.EDLoaddate)
        Me.GroupGeneral.Controls.Add(Me.EDDriver)
        Me.GroupGeneral.Controls.Add(Me.EDTruck)
        Me.GroupGeneral.Controls.Add(Me.CBTankfarm)
        Me.GroupGeneral.Controls.Add(Me.Label48)
        Me.GroupGeneral.Controls.Add(Me.Label61)
        Me.GroupGeneral.Controls.Add(Me.Label70)
        Me.GroupGeneral.Controls.Add(Me.Label47)
        Me.GroupGeneral.Controls.Add(Me.weightouttime)
        Me.GroupGeneral.Controls.Add(Me.BtVehicleEdit)
        Me.GroupGeneral.Controls.Add(Me.CBCardNO)
        Me.GroupGeneral.Controls.Add(Me.Weightintime)
        Me.GroupGeneral.Controls.Add(Me.Label69)
        Me.GroupGeneral.Controls.Add(Me.Label64)
        Me.GroupGeneral.Controls.Add(Me.Label63)
        Me.GroupGeneral.Controls.Add(Me.Label65)
        Me.GroupGeneral.Controls.Add(Me.Label60)
        Me.GroupGeneral.Controls.Add(Me.Label59)
        Me.GroupGeneral.Controls.Add(Me.Label58)
        Me.GroupGeneral.Controls.Add(Me.EDSaleQtr)
        Me.GroupGeneral.Controls.Add(Me.Label57)
        Me.GroupGeneral.Controls.Add(Me.EDSale_Unit)
        Me.GroupGeneral.Controls.Add(Me.EDDO_item)
        Me.GroupGeneral.Controls.Add(Me.Label55)
        Me.GroupGeneral.Controls.Add(Me.Button1)
        Me.GroupGeneral.Controls.Add(Me.Soitem)
        Me.GroupGeneral.Controls.Add(Me.BTIMPORT_SAP)
        Me.GroupGeneral.Controls.Add(Me.Label54)
        Me.GroupGeneral.Controls.Add(Me.Sono)
        Me.GroupGeneral.Controls.Add(Me.Label53)
        Me.GroupGeneral.Controls.Add(Me.GroupBox16)
        Me.GroupGeneral.Controls.Add(Me.Label51)
        Me.GroupGeneral.Controls.Add(Me.ShipMent_Type)
        Me.GroupGeneral.Controls.Add(Me.Label44)
        Me.GroupGeneral.Controls.Add(Me.Label45)
        Me.GroupGeneral.Controls.Add(Me.Label46)
        Me.GroupGeneral.Controls.Add(Me.Label43)
        Me.GroupGeneral.Controls.Add(Me.Label42)
        Me.GroupGeneral.Controls.Add(Me.Label41)
        Me.GroupGeneral.Controls.Add(Me.Calculate)
        Me.GroupGeneral.Controls.Add(Me.Tank)
        Me.GroupGeneral.Controls.Add(Me.Density)
        Me.GroupGeneral.Controls.Add(Me.Temp)
        Me.GroupGeneral.Controls.Add(Me.MeterRead)
        Me.GroupGeneral.Controls.Add(Me.WeightTotal)
        Me.GroupGeneral.Controls.Add(Me.GroupBox6)
        Me.GroupGeneral.Controls.Add(Me.PackingWeight)
        Me.GroupGeneral.Controls.Add(Me.Label40)
        Me.GroupGeneral.Controls.Add(Me.Label38)
        Me.GroupGeneral.Controls.Add(Me.SealCount)
        Me.GroupGeneral.Controls.Add(Me.Quantity)
        Me.GroupGeneral.Controls.Add(Me.Label33)
        Me.GroupGeneral.Controls.Add(Me.Packing)
        Me.GroupGeneral.Controls.Add(Me.Label32)
        Me.GroupGeneral.Controls.Add(Me.EDPreset)
        Me.GroupGeneral.Controls.Add(Me.Label16)
        Me.GroupGeneral.Controls.Add(Me.Status)
        Me.GroupGeneral.Controls.Add(Me.Label30)
        Me.GroupGeneral.Controls.Add(Me.EDCustomer)
        Me.GroupGeneral.Controls.Add(Me.Label29)
        Me.GroupGeneral.Controls.Add(Me.AccessCode)
        Me.GroupGeneral.Controls.Add(Me.Seal_No)
        Me.GroupGeneral.Controls.Add(Me.Label6)
        Me.GroupGeneral.Controls.Add(Me.GiDate)
        Me.GroupGeneral.Controls.Add(Me.Dateedit)
        Me.GroupGeneral.Controls.Add(Me.Container)
        Me.GroupGeneral.Controls.Add(Me.Label31)
        Me.GroupGeneral.Controls.Add(Me.Label27)
        Me.GroupGeneral.Controls.Add(Me.Label26)
        Me.GroupGeneral.Controls.Add(Me.Label25)
        Me.GroupGeneral.Controls.Add(Me.Coa)
        Me.GroupGeneral.Controls.Add(Me.EDDONo)
        Me.GroupGeneral.Controls.Add(Me.EDREf)
        Me.GroupGeneral.Controls.Add(Me.EDShipper)
        Me.GroupGeneral.Controls.Add(Me.EDForword)
        Me.GroupGeneral.Controls.Add(Me.Label18)
        Me.GroupGeneral.Controls.Add(Me.Label15)
        Me.GroupGeneral.Controls.Add(Me.Label14)
        Me.GroupGeneral.Controls.Add(Me.Label10)
        Me.GroupGeneral.Controls.Add(Me.Label12)
        Me.GroupGeneral.Controls.Add(Me.Label9)
        Me.GroupGeneral.Controls.Add(Me.EDTruckCapa)
        Me.GroupGeneral.Controls.Add(Me.Label1)
        Me.GroupGeneral.Controls.Add(Me.Label4)
        Me.GroupGeneral.Controls.Add(Me.Label2)
        Me.GroupGeneral.Controls.Add(Me.Label3)
        Me.GroupGeneral.Controls.Add(Me.GroupBox5)
        Me.GroupGeneral.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupGeneral.ForeColor = System.Drawing.Color.Black
        Me.GroupGeneral.Location = New System.Drawing.Point(6, 8)
        Me.GroupGeneral.Name = "GroupGeneral"
        Me.GroupGeneral.Size = New System.Drawing.Size(770, 570)
        Me.GroupGeneral.TabIndex = 0
        Me.GroupGeneral.TabStop = False
        Me.GroupGeneral.Text = "General"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label21.Location = New System.Drawing.Point(730, 265)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(23, 16)
        Me.Label21.TabIndex = 2122
        Me.Label21.Text = "EA"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.Location = New System.Drawing.Point(730, 290)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(22, 16)
        Me.Label20.TabIndex = 2121
        Me.Label20.Text = "Kg"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CBLoadingPoint
        '
        Me.CBLoadingPoint.AutoSizeDropDownHeight = True
        Me.CBLoadingPoint.AutoSizeDropDownToBestFit = True
        Me.CBLoadingPoint.DataSource = Me.TLOBindingSource
        Me.CBLoadingPoint.DblClickRotate = True
        Me.CBLoadingPoint.DisplayMember = "LOAD_LOCATION"
        '
        'CBLoadingPoint.NestedRadGridView
        '
        Me.CBLoadingPoint.EditorControl.BackColor = System.Drawing.SystemColors.Window
        Me.CBLoadingPoint.EditorControl.Cursor = System.Windows.Forms.Cursors.Default
        Me.CBLoadingPoint.EditorControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.CBLoadingPoint.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CBLoadingPoint.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.CBLoadingPoint.EditorControl.Location = New System.Drawing.Point(-251, -89)
        '
        '
        '
        Me.CBLoadingPoint.EditorControl.MasterTemplate.AllowAddNewRow = False
        Me.CBLoadingPoint.EditorControl.MasterTemplate.AllowCellContextMenu = False
        Me.CBLoadingPoint.EditorControl.MasterTemplate.AllowColumnChooser = False
        GridViewDecimalColumn1.DataType = GetType(Integer)
        GridViewDecimalColumn1.EnableExpressionEditor = False
        GridViewDecimalColumn1.FieldName = "ID"
        GridViewDecimalColumn1.HeaderText = "ID"
        GridViewDecimalColumn1.IsAutoGenerated = True
        GridViewDecimalColumn1.IsVisible = False
        GridViewDecimalColumn1.Name = "ID"
        GridViewDecimalColumn1.ReadOnly = True
        GridViewTextBoxColumn1.EnableExpressionEditor = False
        GridViewTextBoxColumn1.FieldName = "LOAD_LOCATION"
        GridViewTextBoxColumn1.HeaderText = "LOAD_LOCATION"
        GridViewTextBoxColumn1.IsAutoGenerated = True
        GridViewTextBoxColumn1.Name = "LOAD_LOCATION"
        GridViewTextBoxColumn1.Width = 144
        GridViewTextBoxColumn2.EnableExpressionEditor = False
        GridViewTextBoxColumn2.FieldName = "PLANT_LOCATION"
        GridViewTextBoxColumn2.HeaderText = "PLANT_LOCATION"
        GridViewTextBoxColumn2.IsAutoGenerated = True
        GridViewTextBoxColumn2.Name = "PLANT_LOCATION"
        GridViewTextBoxColumn2.Width = 123
        GridViewTextBoxColumn3.EnableExpressionEditor = False
        GridViewTextBoxColumn3.FieldName = "PRINT_LOC_DES"
        GridViewTextBoxColumn3.HeaderText = "PRINT_LOC_DES"
        GridViewTextBoxColumn3.IsAutoGenerated = True
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "PRINT_LOC_DES"
        GridViewTextBoxColumn4.EnableExpressionEditor = False
        GridViewTextBoxColumn4.FieldName = "VALUE"
        GridViewTextBoxColumn4.HeaderText = "VALUE"
        GridViewTextBoxColumn4.IsAutoGenerated = True
        GridViewTextBoxColumn4.IsVisible = False
        GridViewTextBoxColumn4.Name = "VALUE"
        GridViewTextBoxColumn5.EnableExpressionEditor = False
        GridViewTextBoxColumn5.FieldName = "CreateBy"
        GridViewTextBoxColumn5.HeaderText = "CreateBy"
        GridViewTextBoxColumn5.IsAutoGenerated = True
        GridViewTextBoxColumn5.IsVisible = False
        GridViewTextBoxColumn5.Name = "CreateBy"
        GridViewDateTimeColumn1.EnableExpressionEditor = False
        GridViewDateTimeColumn1.FieldName = "CreateDate"
        GridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn1.HeaderText = "CreateDate"
        GridViewDateTimeColumn1.IsAutoGenerated = True
        GridViewDateTimeColumn1.IsVisible = False
        GridViewDateTimeColumn1.Name = "CreateDate"
        GridViewTextBoxColumn6.EnableExpressionEditor = False
        GridViewTextBoxColumn6.FieldName = "LastUpdateBy"
        GridViewTextBoxColumn6.HeaderText = "LastUpdateBy"
        GridViewTextBoxColumn6.IsAutoGenerated = True
        GridViewTextBoxColumn6.IsVisible = False
        GridViewTextBoxColumn6.Name = "LastUpdateBy"
        GridViewDateTimeColumn2.EnableExpressionEditor = False
        GridViewDateTimeColumn2.FieldName = "LastUpdateDate"
        GridViewDateTimeColumn2.Format = System.Windows.Forms.DateTimePickerFormat.[Long]
        GridViewDateTimeColumn2.HeaderText = "LastUpdateDate"
        GridViewDateTimeColumn2.IsAutoGenerated = True
        GridViewDateTimeColumn2.IsVisible = False
        GridViewDateTimeColumn2.Name = "LastUpdateDate"
        Me.CBLoadingPoint.EditorControl.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewDecimalColumn1, GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewDateTimeColumn1, GridViewTextBoxColumn6, GridViewDateTimeColumn2})
        Me.CBLoadingPoint.EditorControl.MasterTemplate.DataSource = Me.TLOBindingSource
        Me.CBLoadingPoint.EditorControl.MasterTemplate.EnableGrouping = False
        Me.CBLoadingPoint.EditorControl.MasterTemplate.ShowFilteringRow = False
        Me.CBLoadingPoint.EditorControl.Name = "NestedRadGridView"
        Me.CBLoadingPoint.EditorControl.ReadOnly = True
        Me.CBLoadingPoint.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CBLoadingPoint.EditorControl.ShowGroupPanel = False
        Me.CBLoadingPoint.EditorControl.Size = New System.Drawing.Size(240, 150)
        Me.CBLoadingPoint.EditorControl.TabIndex = 2119
        Me.CBLoadingPoint.Enabled = False
        Me.CBLoadingPoint.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBLoadingPoint.Location = New System.Drawing.Point(143, 540)
        Me.CBLoadingPoint.Name = "CBLoadingPoint"
        Me.CBLoadingPoint.Size = New System.Drawing.Size(191, 24)
        Me.CBLoadingPoint.TabIndex = 2120
        Me.CBLoadingPoint.TabStop = False
        Me.CBLoadingPoint.ThemeName = "Windows7"
        '
        'TLOBindingSource
        '
        Me.TLOBindingSource.DataMember = "T_LO"
        Me.TLOBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.edit_32
        Me.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button3.Location = New System.Drawing.Point(340, 537)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(35, 23)
        Me.Button3.TabIndex = 2115
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.Location = New System.Drawing.Point(42, 540)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(97, 16)
        Me.Label19.TabIndex = 2114
        Me.Label19.Text = "Loading Point  :"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(341, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 18)
        Me.Label5.TabIndex = 2112
        Me.Label5.Text = "*"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'EDLoaddate
        '
        Me.EDLoaddate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EDLoaddate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDLoaddate.ForeColor = System.Drawing.Color.Navy
        Me.EDLoaddate.Location = New System.Drawing.Point(564, 149)
        Me.EDLoaddate.Name = "EDLoaddate"
        Me.EDLoaddate.ReadOnly = True
        Me.EDLoaddate.Size = New System.Drawing.Size(160, 23)
        Me.EDLoaddate.TabIndex = 23
        '
        'EDDriver
        '
        Me.EDDriver.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDDriver.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDDriver.DataSource = Me.TDRIVERBindingSource
        Me.EDDriver.DisplayMember = "Driver_NAME"
        Me.EDDriver.DropDownWidth = 500
        Me.EDDriver.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDDriver.ForeColor = System.Drawing.Color.Navy
        Me.EDDriver.FormattingEnabled = True
        Me.EDDriver.Location = New System.Drawing.Point(143, 57)
        Me.EDDriver.Name = "EDDriver"
        Me.EDDriver.Size = New System.Drawing.Size(191, 24)
        Me.EDDriver.TabIndex = 2111
        Me.EDDriver.ValueMember = "ID"
        '
        'TDRIVERBindingSource
        '
        Me.TDRIVERBindingSource.DataMember = "T_DRIVER"
        Me.TDRIVERBindingSource.DataSource = Me.FPTDataSet
        '
        'EDTruck
        '
        Me.EDTruck.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDTruck.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDTruck.DataSource = Me.TTRUCKBindingSource
        Me.EDTruck.DisplayMember = "TRUCK_NUMBER"
        Me.EDTruck.DropDownWidth = 500
        Me.EDTruck.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDTruck.ForeColor = System.Drawing.Color.Navy
        Me.EDTruck.FormattingEnabled = True
        Me.EDTruck.Location = New System.Drawing.Point(143, 31)
        Me.EDTruck.Name = "EDTruck"
        Me.EDTruck.Size = New System.Drawing.Size(191, 24)
        Me.EDTruck.TabIndex = 2110
        Me.EDTruck.ValueMember = "ID"
        '
        'TTRUCKBindingSource
        '
        Me.TTRUCKBindingSource.DataMember = "T_TRUCK"
        Me.TTRUCKBindingSource.DataSource = Me.FPTDataSet
        '
        'CBTankfarm
        '
        Me.CBTankfarm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBTankfarm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBTankfarm.DataSource = Me.TTANKFARMBindingSource
        Me.CBTankfarm.DisplayMember = "CODE"
        Me.CBTankfarm.DropDownWidth = 500
        Me.CBTankfarm.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CBTankfarm.ForeColor = System.Drawing.Color.Navy
        Me.CBTankfarm.FormattingEnabled = True
        Me.CBTankfarm.Location = New System.Drawing.Point(143, 135)
        Me.CBTankfarm.Name = "CBTankfarm"
        Me.CBTankfarm.Size = New System.Drawing.Size(191, 24)
        Me.CBTankfarm.TabIndex = 8
        Me.CBTankfarm.ValueMember = "CODE"
        '
        'TTANKFARMBindingSource
        '
        Me.TTANKFARMBindingSource.DataMember = "T_TANKFARM"
        Me.TTANKFARMBindingSource.DataSource = Me.FPTDataSet
        '
        'Label48
        '
        Me.Label48.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label48.AutoSize = True
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label48.Location = New System.Drawing.Point(441, 512)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(119, 16)
        Me.Label48.TabIndex = 197
        Me.Label48.Text = "Weight-Out Time  :"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.BackColor = System.Drawing.Color.Transparent
        Me.Label61.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label61.Location = New System.Drawing.Point(63, 136)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(76, 16)
        Me.Label61.TabIndex = 2107
        Me.Label61.Text = "Tank farm :"
        Me.Label61.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.Red
        Me.Label70.Location = New System.Drawing.Point(730, 233)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(20, 25)
        Me.Label70.TabIndex = 2105
        Me.Label70.Text = "*"
        Me.Label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label47
        '
        Me.Label47.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label47.Location = New System.Drawing.Point(30, 513)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(110, 16)
        Me.Label47.TabIndex = 196
        Me.Label47.Text = "Weight-In Time  :"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'weightouttime
        '
        Me.weightouttime.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weightouttime.BackColor = System.Drawing.SystemColors.Control
        Me.weightouttime.Enabled = False
        Me.weightouttime.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.weightouttime.ForeColor = System.Drawing.Color.Navy
        Me.weightouttime.Location = New System.Drawing.Point(564, 511)
        Me.weightouttime.Name = "weightouttime"
        Me.weightouttime.Size = New System.Drawing.Size(159, 23)
        Me.weightouttime.TabIndex = 5
        '
        'BtVehicleEdit
        '
        Me.BtVehicleEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtVehicleEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BtVehicleEdit.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtVehicleEdit.Location = New System.Drawing.Point(267, 10)
        Me.BtVehicleEdit.Name = "BtVehicleEdit"
        Me.BtVehicleEdit.Size = New System.Drawing.Size(67, 20)
        Me.BtVehicleEdit.TabIndex = 2104
        Me.BtVehicleEdit.Text = "Vehicle Edit"
        Me.BtVehicleEdit.UseVisualStyleBackColor = True
        '
        'CBCardNO
        '
        Me.CBCardNO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CBCardNO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CBCardNO.BackColor = System.Drawing.SystemColors.Control
        Me.CBCardNO.DataSource = Me.VCARDFREEBindingSource
        Me.CBCardNO.DisplayMember = "CARD_NUMBER"
        Me.CBCardNO.DropDownWidth = 100
        Me.CBCardNO.Enabled = False
        Me.CBCardNO.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CBCardNO.FormattingEnabled = True
        Me.CBCardNO.Location = New System.Drawing.Point(275, 310)
        Me.CBCardNO.Name = "CBCardNO"
        Me.CBCardNO.Size = New System.Drawing.Size(59, 26)
        Me.CBCardNO.TabIndex = 2103
        Me.CBCardNO.ValueMember = "CARD_NUMBER"
        '
        'VCARDFREEBindingSource
        '
        Me.VCARDFREEBindingSource.DataMember = "V_CARDFREE"
        Me.VCARDFREEBindingSource.DataSource = Me.FPTDataSet
        '
        'Weightintime
        '
        Me.Weightintime.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Weightintime.BackColor = System.Drawing.SystemColors.Control
        Me.Weightintime.Enabled = False
        Me.Weightintime.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Weightintime.ForeColor = System.Drawing.Color.Navy
        Me.Weightintime.Location = New System.Drawing.Point(143, 511)
        Me.Weightintime.Name = "Weightintime"
        Me.Weightintime.Size = New System.Drawing.Size(191, 23)
        Me.Weightintime.TabIndex = 4
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label69.Location = New System.Drawing.Point(188, 314)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(72, 16)
        Me.Label69.TabIndex = 2102
        Me.Label69.Text = "Password :"
        Me.Label69.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.Red
        Me.Label64.Location = New System.Drawing.Point(730, 122)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(20, 25)
        Me.Label64.TabIndex = 2100
        Me.Label64.Text = "*"
        Me.Label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label63.ForeColor = System.Drawing.Color.Red
        Me.Label63.Location = New System.Drawing.Point(730, 175)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(20, 25)
        Me.Label63.TabIndex = 2100
        Me.Label63.Text = "*"
        Me.Label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.Red
        Me.Label65.Location = New System.Drawing.Point(730, 437)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(20, 25)
        Me.Label65.TabIndex = 2099
        Me.Label65.Text = "*"
        Me.Label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.Red
        Me.Label60.Location = New System.Drawing.Point(340, 124)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(16, 18)
        Me.Label60.TabIndex = 2098
        Me.Label60.Text = "*"
        Me.Label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.Red
        Me.Label59.Location = New System.Drawing.Point(340, 97)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(16, 18)
        Me.Label59.TabIndex = 2097
        Me.Label59.Text = "*"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label58.Location = New System.Drawing.Point(221, 265)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(31, 16)
        Me.Label58.TabIndex = 2096
        Me.Label58.Text = "Qtr:"
        Me.Label58.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'EDSaleQtr
        '
        Me.EDSaleQtr.BackColor = System.Drawing.SystemColors.Control
        Me.EDSaleQtr.Enabled = False
        Me.EDSaleQtr.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDSaleQtr.ForeColor = System.Drawing.Color.Navy
        Me.EDSaleQtr.Location = New System.Drawing.Point(258, 262)
        Me.EDSaleQtr.Name = "EDSaleQtr"
        Me.EDSaleQtr.ReadOnly = True
        Me.EDSaleQtr.Size = New System.Drawing.Size(76, 23)
        Me.EDSaleQtr.TabIndex = 2095
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label57.Location = New System.Drawing.Point(341, 265)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(35, 16)
        Me.Label57.TabIndex = 2094
        Me.Label57.Text = "Unit:"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'EDSale_Unit
        '
        Me.EDSale_Unit.BackColor = System.Drawing.SystemColors.Control
        Me.EDSale_Unit.Enabled = False
        Me.EDSale_Unit.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDSale_Unit.ForeColor = System.Drawing.Color.Navy
        Me.EDSale_Unit.Location = New System.Drawing.Point(389, 262)
        Me.EDSale_Unit.Name = "EDSale_Unit"
        Me.EDSale_Unit.ReadOnly = True
        Me.EDSale_Unit.Size = New System.Drawing.Size(45, 23)
        Me.EDSale_Unit.TabIndex = 2093
        '
        'EDDO_item
        '
        Me.EDDO_item.Enabled = False
        Me.EDDO_item.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDDO_item.ForeColor = System.Drawing.Color.Navy
        Me.EDDO_item.Location = New System.Drawing.Point(143, 212)
        Me.EDDO_item.Name = "EDDO_item"
        Me.EDDO_item.Size = New System.Drawing.Size(191, 23)
        Me.EDDO_item.TabIndex = 12
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.BackColor = System.Drawing.Color.Transparent
        Me.Label55.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label55.Location = New System.Drawing.Point(70, 214)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(69, 16)
        Me.Label55.TabIndex = 2092
        Me.Label55.Text = "D/O Item :"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.edit_32
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button1.Location = New System.Drawing.Point(365, 119)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(35, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Soitem
        '
        Me.Soitem.BackColor = System.Drawing.SystemColors.Control
        Me.Soitem.Enabled = False
        Me.Soitem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Soitem.ForeColor = System.Drawing.Color.Navy
        Me.Soitem.Location = New System.Drawing.Point(143, 262)
        Me.Soitem.Name = "Soitem"
        Me.Soitem.Size = New System.Drawing.Size(72, 23)
        Me.Soitem.TabIndex = 14
        '
        'BTIMPORT_SAP
        '
        Me.BTIMPORT_SAP.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTIMPORT_SAP.Image = Global.TAS_TOL.My.Resources.Resources.sap
        Me.BTIMPORT_SAP.Location = New System.Drawing.Point(361, 58)
        Me.BTIMPORT_SAP.Name = "BTIMPORT_SAP"
        Me.BTIMPORT_SAP.Size = New System.Drawing.Size(79, 34)
        Me.BTIMPORT_SAP.TabIndex = 5
        Me.BTIMPORT_SAP.Text = "&Data"
        Me.BTIMPORT_SAP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTIMPORT_SAP.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.Transparent
        Me.Label54.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label54.Location = New System.Drawing.Point(26, 266)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(113, 16)
        Me.Label54.TabIndex = 204
        Me.Label54.Text = "Sales order Item :"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Sono
        '
        Me.Sono.Enabled = False
        Me.Sono.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Sono.ForeColor = System.Drawing.Color.Navy
        Me.Sono.Location = New System.Drawing.Point(143, 237)
        Me.Sono.Name = "Sono"
        Me.Sono.Size = New System.Drawing.Size(191, 23)
        Me.Sono.TabIndex = 13
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label53.Location = New System.Drawing.Point(37, 240)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(102, 16)
        Me.Label53.TabIndex = 202
        Me.Label53.Text = "Sales order No.:"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox16
        '
        Me.GroupBox16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox16.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox16.Controls.Add(Me.RadioLiquid)
        Me.GroupBox16.Controls.Add(Me.RadioBig)
        Me.GroupBox16.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox16.Location = New System.Drawing.Point(564, 165)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(160, 39)
        Me.GroupBox16.TabIndex = 24
        Me.GroupBox16.TabStop = False
        '
        'RadioLiquid
        '
        Me.RadioLiquid.AutoSize = True
        Me.RadioLiquid.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RadioLiquid.Location = New System.Drawing.Point(85, 12)
        Me.RadioLiquid.Name = "RadioLiquid"
        Me.RadioLiquid.Size = New System.Drawing.Size(68, 20)
        Me.RadioLiquid.TabIndex = 1
        Me.RadioLiquid.Text = "Actual"
        Me.RadioLiquid.UseVisualStyleBackColor = True
        '
        'RadioBig
        '
        Me.RadioBig.AutoSize = True
        Me.RadioBig.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RadioBig.Location = New System.Drawing.Point(11, 12)
        Me.RadioBig.Name = "RadioBig"
        Me.RadioBig.Size = New System.Drawing.Size(63, 20)
        Me.RadioBig.TabIndex = 0
        Me.RadioBig.Text = "Order"
        Me.RadioBig.UseVisualStyleBackColor = True
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.BackColor = System.Drawing.Color.Transparent
        Me.Label51.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label51.Location = New System.Drawing.Point(457, 317)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(103, 16)
        Me.Label51.TabIndex = 200
        Me.Label51.Text = "Shipment Type :"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShipMent_Type
        '
        Me.ShipMent_Type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ShipMent_Type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ShipMent_Type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ShipMent_Type.DataSource = Me.TSHIPMENTTYPEBindingSource
        Me.ShipMent_Type.DisplayMember = "CODE"
        Me.ShipMent_Type.DropDownWidth = 500
        Me.ShipMent_Type.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ShipMent_Type.ForeColor = System.Drawing.Color.Navy
        Me.ShipMent_Type.FormattingEnabled = True
        Me.ShipMent_Type.Location = New System.Drawing.Point(564, 312)
        Me.ShipMent_Type.Name = "ShipMent_Type"
        Me.ShipMent_Type.Size = New System.Drawing.Size(160, 24)
        Me.ShipMent_Type.TabIndex = 29
        Me.ShipMent_Type.ValueMember = "ID"
        '
        'TSHIPMENTTYPEBindingSource
        '
        Me.TSHIPMENTTYPEBindingSource.DataMember = "T_SHIPMENT_TYPE"
        Me.TSHIPMENTTYPEBindingSource.DataSource = Me.FPTDataSet
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label44.Location = New System.Drawing.Point(491, 488)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(69, 16)
        Me.Label44.TabIndex = 198
        Me.Label44.Text = "Calculate :"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label45.Location = New System.Drawing.Point(496, 464)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(64, 16)
        Me.Label45.TabIndex = 197
        Me.Label45.Text = "Tank No.:"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label46.Location = New System.Drawing.Point(502, 440)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(58, 16)
        Me.Label46.TabIndex = 196
        Me.Label46.Text = "Density :"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label43.Location = New System.Drawing.Point(42, 488)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(98, 16)
        Me.Label43.TabIndex = 195
        Me.Label43.Text = "Loading Temp :"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label42.Location = New System.Drawing.Point(40, 463)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(100, 16)
        Me.Label42.TabIndex = 194
        Me.Label42.Text = "Meter Reading :"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label41.Location = New System.Drawing.Point(50, 438)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(90, 16)
        Me.Label41.TabIndex = 193
        Me.Label41.Text = "Weight Total :"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Calculate
        '
        Me.Calculate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Calculate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Calculate.ForeColor = System.Drawing.Color.Navy
        Me.Calculate.Location = New System.Drawing.Point(564, 486)
        Me.Calculate.Name = "Calculate"
        Me.Calculate.ReadOnly = True
        Me.Calculate.Size = New System.Drawing.Size(160, 23)
        Me.Calculate.TabIndex = 37
        '
        'Tank
        '
        Me.Tank.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tank.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Tank.ForeColor = System.Drawing.Color.Navy
        Me.Tank.Location = New System.Drawing.Point(564, 461)
        Me.Tank.Name = "Tank"
        Me.Tank.Size = New System.Drawing.Size(160, 23)
        Me.Tank.TabIndex = 36
        '
        'Density
        '
        Me.Density.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Density.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Density.ForeColor = System.Drawing.Color.Navy
        Me.Density.Location = New System.Drawing.Point(564, 436)
        Me.Density.Name = "Density"
        Me.Density.Size = New System.Drawing.Size(160, 23)
        Me.Density.TabIndex = 35
        '
        'Temp
        '
        Me.Temp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Temp.ForeColor = System.Drawing.Color.Navy
        Me.Temp.Location = New System.Drawing.Point(143, 486)
        Me.Temp.Name = "Temp"
        Me.Temp.Size = New System.Drawing.Size(191, 23)
        Me.Temp.TabIndex = 34
        '
        'MeterRead
        '
        Me.MeterRead.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MeterRead.ForeColor = System.Drawing.Color.Navy
        Me.MeterRead.Location = New System.Drawing.Point(143, 461)
        Me.MeterRead.Name = "MeterRead"
        Me.MeterRead.ReadOnly = True
        Me.MeterRead.Size = New System.Drawing.Size(191, 23)
        Me.MeterRead.TabIndex = 33
        '
        'WeightTotal
        '
        Me.WeightTotal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.WeightTotal.ForeColor = System.Drawing.Color.Navy
        Me.WeightTotal.Location = New System.Drawing.Point(143, 436)
        Me.WeightTotal.Name = "WeightTotal"
        Me.WeightTotal.ReadOnly = True
        Me.WeightTotal.Size = New System.Drawing.Size(191, 23)
        Me.WeightTotal.TabIndex = 32
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Company_sapcode)
        Me.GroupBox6.Controls.Add(Me.P_Weight)
        Me.GroupBox6.Controls.Add(Me.PackingId)
        Me.GroupBox6.Controls.Add(Me.Shipment_id)
        Me.GroupBox6.Controls.Add(Me.EDLoadDTime)
        Me.GroupBox6.Controls.Add(Me.Updatedate)
        Me.GroupBox6.Controls.Add(Me.EDLoadID)
        Me.GroupBox6.Controls.Add(Me.TruckId)
        Me.GroupBox6.Controls.Add(Me.Prono)
        Me.GroupBox6.Controls.Add(Me.ProductId)
        Me.GroupBox6.Controls.Add(Me.StatusId)
        Me.GroupBox6.Controls.Add(Me.TruckCompanyId)
        Me.GroupBox6.Controls.Add(Me.DriverID)
        Me.GroupBox6.Controls.Add(Me.CustomerID)
        Me.GroupBox6.Controls.Add(Me.ShipperId)
        Me.GroupBox6.Controls.Add(Me.TextBox1)
        Me.GroupBox6.Controls.Add(Me.DateeditTemp)
        Me.GroupBox6.Controls.Add(Me.ComboBox1)
        Me.GroupBox6.Controls.Add(Me.Label11)
        Me.GroupBox6.Controls.Add(Me.ShipmentNo)
        Me.GroupBox6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(427, 470)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(34, 27)
        Me.GroupBox6.TabIndex = 173
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Hidden"
        Me.GroupBox6.Visible = False
        '
        'Company_sapcode
        '
        Me.Company_sapcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Company_sapcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Company_sapcode.DataSource = Me.TCOMPANYBindingSource
        Me.Company_sapcode.DisplayMember = "COMPANY_NUMBER"
        Me.Company_sapcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Company_sapcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Company_sapcode.FormattingEnabled = True
        Me.Company_sapcode.Location = New System.Drawing.Point(14, 278)
        Me.Company_sapcode.Name = "Company_sapcode"
        Me.Company_sapcode.Size = New System.Drawing.Size(62, 21)
        Me.Company_sapcode.TabIndex = 202
        Me.Company_sapcode.ValueMember = "COMPANY_NUMBER"
        '
        'TCOMPANYBindingSource
        '
        Me.TCOMPANYBindingSource.DataMember = "T_COMPANY"
        Me.TCOMPANYBindingSource.DataSource = Me.FPTDataSet
        '
        'P_Weight
        '
        Me.P_Weight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.P_Weight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.P_Weight.DataSource = Me.TPACKINGBindingSource
        Me.P_Weight.DisplayMember = "P_WEIGHT"
        Me.P_Weight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.P_Weight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.P_Weight.FormattingEnabled = True
        Me.P_Weight.Location = New System.Drawing.Point(14, 251)
        Me.P_Weight.Name = "P_Weight"
        Me.P_Weight.Size = New System.Drawing.Size(62, 21)
        Me.P_Weight.TabIndex = 199
        Me.P_Weight.ValueMember = "P_WEIGHT"
        '
        'TPACKINGBindingSource
        '
        Me.TPACKINGBindingSource.DataMember = "T_PACKING"
        Me.TPACKINGBindingSource.DataSource = Me.FPTDataSet
        '
        'PackingId
        '
        Me.PackingId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.PackingId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.PackingId.DataSource = Me.TPACKINGBindingSource
        Me.PackingId.DisplayMember = "P_ID"
        Me.PackingId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.PackingId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PackingId.FormattingEnabled = True
        Me.PackingId.Location = New System.Drawing.Point(14, 224)
        Me.PackingId.Name = "PackingId"
        Me.PackingId.Size = New System.Drawing.Size(59, 21)
        Me.PackingId.TabIndex = 180
        Me.PackingId.ValueMember = "P_ID"
        '
        'Shipment_id
        '
        Me.Shipment_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Shipment_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Shipment_id.DataSource = Me.TSHIPMENTTYPEBindingSource
        Me.Shipment_id.DisplayMember = "ID"
        Me.Shipment_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Shipment_id.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Shipment_id.FormattingEnabled = True
        Me.Shipment_id.Location = New System.Drawing.Point(14, 195)
        Me.Shipment_id.Name = "Shipment_id"
        Me.Shipment_id.Size = New System.Drawing.Size(62, 21)
        Me.Shipment_id.TabIndex = 201
        Me.Shipment_id.ValueMember = "ID"
        '
        'EDLoadDTime
        '
        Me.EDLoadDTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDLoadDTime.Location = New System.Drawing.Point(6, 480)
        Me.EDLoadDTime.Name = "EDLoadDTime"
        Me.EDLoadDTime.Size = New System.Drawing.Size(77, 20)
        Me.EDLoadDTime.TabIndex = 158
        Me.EDLoadDTime.Visible = False
        '
        'Updatedate
        '
        Me.Updatedate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Updatedate.Location = New System.Drawing.Point(6, 456)
        Me.Updatedate.Name = "Updatedate"
        Me.Updatedate.Size = New System.Drawing.Size(53, 20)
        Me.Updatedate.TabIndex = 111
        Me.Updatedate.Visible = False
        '
        'EDLoadID
        '
        Me.EDLoadID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDLoadID.Location = New System.Drawing.Point(5, 428)
        Me.EDLoadID.Name = "EDLoadID"
        Me.EDLoadID.Size = New System.Drawing.Size(55, 20)
        Me.EDLoadID.TabIndex = 149
        Me.EDLoadID.Visible = False
        '
        'TruckId
        '
        Me.TruckId.DataSource = Me.TTRUCKBindingSource
        Me.TruckId.DisplayMember = "ID"
        Me.TruckId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TruckId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TruckId.FormattingEnabled = True
        Me.TruckId.Location = New System.Drawing.Point(6, 90)
        Me.TruckId.Name = "TruckId"
        Me.TruckId.Size = New System.Drawing.Size(55, 21)
        Me.TruckId.TabIndex = 154
        Me.TruckId.ValueMember = "ID"
        '
        'Prono
        '
        Me.Prono.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Prono.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Prono.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Prono.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Prono.FormattingEnabled = True
        Me.Prono.Location = New System.Drawing.Point(16, 176)
        Me.Prono.Name = "Prono"
        Me.Prono.Size = New System.Drawing.Size(0, 21)
        Me.Prono.TabIndex = 160
        '
        'ProductId
        '
        Me.ProductId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ProductId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ProductId.DataSource = Me.TProductBindingSource
        Me.ProductId.DisplayMember = "ID"
        Me.ProductId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ProductId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ProductId.FormattingEnabled = True
        Me.ProductId.Location = New System.Drawing.Point(16, 325)
        Me.ProductId.Name = "ProductId"
        Me.ProductId.Size = New System.Drawing.Size(53, 21)
        Me.ProductId.TabIndex = 175
        Me.ProductId.ValueMember = "ID"
        '
        'TProductBindingSource
        '
        Me.TProductBindingSource.DataMember = "T_Product"
        Me.TProductBindingSource.DataSource = Me.FPTDataSet
        Me.TProductBindingSource.Filter = "product_type=1"
        Me.TProductBindingSource.Sort = "PRODUCT_CODE"
        '
        'StatusId
        '
        Me.StatusId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.StatusId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.StatusId.DataSource = Me.TSTATUSBindingSource
        Me.StatusId.DisplayMember = "STATUS_ID"
        Me.StatusId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.StatusId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.StatusId.FormattingEnabled = True
        Me.StatusId.Location = New System.Drawing.Point(14, 302)
        Me.StatusId.Name = "StatusId"
        Me.StatusId.Size = New System.Drawing.Size(59, 21)
        Me.StatusId.TabIndex = 177
        Me.StatusId.ValueMember = "STATUS_ID"
        '
        'TSTATUSBindingSource
        '
        Me.TSTATUSBindingSource.DataMember = "T_STATUS"
        Me.TSTATUSBindingSource.DataSource = Me.FPTDataSet
        '
        'TruckCompanyId
        '
        Me.TruckCompanyId.DataSource = Me.TCOMPANYBindingSource
        Me.TruckCompanyId.DisplayMember = "COMPANY_ID"
        Me.TruckCompanyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TruckCompanyId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TruckCompanyId.FormattingEnabled = True
        Me.TruckCompanyId.Location = New System.Drawing.Point(14, 25)
        Me.TruckCompanyId.Name = "TruckCompanyId"
        Me.TruckCompanyId.Size = New System.Drawing.Size(55, 21)
        Me.TruckCompanyId.TabIndex = 156
        Me.TruckCompanyId.ValueMember = "COMPANY_ID"
        '
        'DriverID
        '
        Me.DriverID.DataSource = Me.TDRIVERBindingSource
        Me.DriverID.DisplayMember = "ID"
        Me.DriverID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DriverID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DriverID.FormattingEnabled = True
        Me.DriverID.Location = New System.Drawing.Point(40, 15)
        Me.DriverID.Name = "DriverID"
        Me.DriverID.Size = New System.Drawing.Size(55, 21)
        Me.DriverID.TabIndex = 157
        Me.DriverID.ValueMember = "ID"
        '
        'CustomerID
        '
        Me.CustomerID.DataSource = Me.TCustomerBindingSource
        Me.CustomerID.DisplayMember = "ID"
        Me.CustomerID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CustomerID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CustomerID.FormattingEnabled = True
        Me.CustomerID.Location = New System.Drawing.Point(5, 49)
        Me.CustomerID.Name = "CustomerID"
        Me.CustomerID.Size = New System.Drawing.Size(55, 21)
        Me.CustomerID.TabIndex = 170
        Me.CustomerID.ValueMember = "ID"
        '
        'TCustomerBindingSource
        '
        Me.TCustomerBindingSource.DataMember = "T_Customer"
        Me.TCustomerBindingSource.DataSource = Me.FPTDataSet
        '
        'ShipperId
        '
        Me.ShipperId.DataSource = Me.TSHIPPERBindingSource
        Me.ShipperId.DisplayMember = "ID"
        Me.ShipperId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ShipperId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ShipperId.FormattingEnabled = True
        Me.ShipperId.Location = New System.Drawing.Point(5, 70)
        Me.ShipperId.Name = "ShipperId"
        Me.ShipperId.Size = New System.Drawing.Size(55, 21)
        Me.ShipperId.TabIndex = 155
        Me.ShipperId.ValueMember = "ID"
        '
        'TSHIPPERBindingSource
        '
        Me.TSHIPPERBindingSource.DataMember = "T_SHIPPER"
        Me.TSHIPPERBindingSource.DataSource = Me.FPTDataSet
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(6, 116)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(31, 20)
        Me.TextBox1.TabIndex = 174
        '
        'DateeditTemp
        '
        Me.DateeditTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DateeditTemp.Location = New System.Drawing.Point(4, 402)
        Me.DateeditTemp.Name = "DateeditTemp"
        Me.DateeditTemp.Size = New System.Drawing.Size(57, 20)
        Me.DateeditTemp.TabIndex = 139
        Me.DateeditTemp.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"a", "b", "c"})
        Me.ComboBox1.Location = New System.Drawing.Point(14, 145)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(46, 21)
        Me.ComboBox1.TabIndex = 153
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(-75, 28)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 20)
        Me.Label11.TabIndex = 167
        Me.Label11.Text = "Shipment No.:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShipmentNo
        '
        Me.ShipmentNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ShipmentNo.Enabled = False
        Me.ShipmentNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ShipmentNo.Location = New System.Drawing.Point(40, 25)
        Me.ShipmentNo.Name = "ShipmentNo"
        Me.ShipmentNo.Size = New System.Drawing.Size(93, 26)
        Me.ShipmentNo.TabIndex = 30
        '
        'PackingWeight
        '
        Me.PackingWeight.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PackingWeight.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PackingWeight.ForeColor = System.Drawing.Color.Navy
        Me.PackingWeight.Location = New System.Drawing.Point(564, 286)
        Me.PackingWeight.Name = "PackingWeight"
        Me.PackingWeight.Size = New System.Drawing.Size(160, 23)
        Me.PackingWeight.TabIndex = 28
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label40.Location = New System.Drawing.Point(452, 290)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(108, 16)
        Me.Label40.TabIndex = 186
        Me.Label40.Text = "Packing Weight  :"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.Color.Transparent
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label38.Location = New System.Drawing.Point(60, 318)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(79, 16)
        Me.Label38.TabIndex = 184
        Me.Label38.Text = "Seal Count :"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SealCount
        '
        Me.SealCount.BackColor = System.Drawing.SystemColors.Control
        Me.SealCount.Enabled = False
        Me.SealCount.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.SealCount.ForeColor = System.Drawing.Color.Navy
        Me.SealCount.Location = New System.Drawing.Point(143, 312)
        Me.SealCount.Name = "SealCount"
        Me.SealCount.Size = New System.Drawing.Size(39, 23)
        Me.SealCount.TabIndex = 16
        '
        'Quantity
        '
        Me.Quantity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Quantity.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Quantity.ForeColor = System.Drawing.Color.Navy
        Me.Quantity.Location = New System.Drawing.Point(564, 260)
        Me.Quantity.Name = "Quantity"
        Me.Quantity.Size = New System.Drawing.Size(160, 23)
        Me.Quantity.TabIndex = 27
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label33.Location = New System.Drawing.Point(492, 263)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(68, 16)
        Me.Label33.TabIndex = 182
        Me.Label33.Text = "Quantity  :"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Packing
        '
        Me.Packing.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Packing.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Packing.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Packing.DataSource = Me.TPACKINGBindingSource
        Me.Packing.DisplayMember = "P_CODE"
        Me.Packing.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Packing.ForeColor = System.Drawing.Color.Navy
        Me.Packing.FormattingEnabled = True
        Me.Packing.Location = New System.Drawing.Point(564, 233)
        Me.Packing.Name = "Packing"
        Me.Packing.Size = New System.Drawing.Size(160, 24)
        Me.Packing.TabIndex = 26
        Me.Packing.ValueMember = "P_ID"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label32.Location = New System.Drawing.Point(468, 236)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(92, 16)
        Me.Label32.TabIndex = 178
        Me.Label32.Text = "Packing Type :"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'EDPreset
        '
        Me.EDPreset.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EDPreset.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDPreset.ForeColor = System.Drawing.Color.Navy
        Me.EDPreset.Location = New System.Drawing.Point(564, 43)
        Me.EDPreset.Name = "EDPreset"
        Me.EDPreset.ReadOnly = True
        Me.EDPreset.Size = New System.Drawing.Size(160, 23)
        Me.EDPreset.TabIndex = 19
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.Location = New System.Drawing.Point(462, 47)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(98, 16)
        Me.Label16.TabIndex = 112
        Me.Label16.Text = "Presets(Total) :"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Status
        '
        Me.Status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Status.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Status.DataSource = Me.TSTATUSBindingSource
        Me.Status.DisplayMember = "STATUS_NAME"
        Me.Status.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Status.ForeColor = System.Drawing.Color.Navy
        Me.Status.FormattingEnabled = True
        Me.Status.Location = New System.Drawing.Point(564, 206)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(160, 24)
        Me.Status.TabIndex = 25
        Me.Status.ValueMember = "STATUS_ID"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label30.Location = New System.Drawing.Point(507, 209)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(53, 16)
        Me.Label30.TabIndex = 175
        Me.Label30.Text = "Status :"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'EDCustomer
        '
        Me.EDCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDCustomer.DataSource = Me.TCustomerBindingSource
        Me.EDCustomer.DisplayMember = "Customer_code"
        Me.EDCustomer.DropDownWidth = 500
        Me.EDCustomer.Enabled = False
        Me.EDCustomer.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDCustomer.ForeColor = System.Drawing.Color.Navy
        Me.EDCustomer.FormattingEnabled = True
        Me.EDCustomer.Location = New System.Drawing.Point(143, 109)
        Me.EDCustomer.Name = "EDCustomer"
        Me.EDCustomer.Size = New System.Drawing.Size(191, 24)
        Me.EDCustomer.TabIndex = 7
        Me.EDCustomer.ValueMember = "ID"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label29.Location = New System.Drawing.Point(34, 110)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(105, 16)
        Me.Label29.TabIndex = 172
        Me.Label29.Text = "Customer Code :"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'AccessCode
        '
        Me.AccessCode.Enabled = False
        Me.AccessCode.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.AccessCode.ForeColor = System.Drawing.Color.Navy
        Me.AccessCode.Location = New System.Drawing.Point(564, 339)
        Me.AccessCode.Name = "AccessCode"
        Me.AccessCode.Size = New System.Drawing.Size(160, 23)
        Me.AccessCode.TabIndex = 30
        '
        'Seal_No
        '
        Me.Seal_No.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Seal_No.ForeColor = System.Drawing.Color.Navy
        Me.Seal_No.Location = New System.Drawing.Point(143, 287)
        Me.Seal_No.Name = "Seal_No"
        Me.Seal_No.Size = New System.Drawing.Size(191, 23)
        Me.Seal_No.TabIndex = 15
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(78, 292)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 16)
        Me.Label6.TabIndex = 161
        Me.Label6.Text = "Seal No.:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GiDate
        '
        Me.GiDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GiDate.CalendarMonthBackground = System.Drawing.SystemColors.Control
        Me.GiDate.CustomFormat = "d/M/yyyy"
        Me.GiDate.Enabled = False
        Me.GiDate.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GiDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.GiDate.Location = New System.Drawing.Point(564, 95)
        Me.GiDate.Name = "GiDate"
        Me.GiDate.Size = New System.Drawing.Size(160, 24)
        Me.GiDate.TabIndex = 21
        '
        'Dateedit
        '
        Me.Dateedit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Dateedit.CustomFormat = "d/M/yyyy HH:mm"
        Me.Dateedit.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Dateedit.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Dateedit.Location = New System.Drawing.Point(564, 122)
        Me.Dateedit.Name = "Dateedit"
        Me.Dateedit.Size = New System.Drawing.Size(160, 24)
        Me.Dateedit.TabIndex = 22
        '
        'Container
        '
        Me.Container.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Container.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Container.Location = New System.Drawing.Point(341, 30)
        Me.Container.Name = "Container"
        Me.Container.Size = New System.Drawing.Size(120, 26)
        Me.Container.TabIndex = 3
        '
        'Label31
        '
        Me.Label31.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label31.Location = New System.Drawing.Point(340, 11)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(86, 16)
        Me.Label31.TabIndex = 147
        Me.Label31.Text = "Container No."
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.Location = New System.Drawing.Point(471, 344)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(89, 16)
        Me.Label27.TabIndex = 140
        Me.Label27.Text = "Access Code :"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.Location = New System.Drawing.Point(97, 343)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(42, 16)
        Me.Label26.TabIndex = 138
        Me.Label26.Text = "COA :"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.Location = New System.Drawing.Point(469, 101)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(91, 16)
        Me.Label25.TabIndex = 136
        Me.Label25.Text = "Plant GI Date :"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Coa
        '
        Me.Coa.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Coa.ForeColor = System.Drawing.Color.Navy
        Me.Coa.Location = New System.Drawing.Point(143, 339)
        Me.Coa.Name = "Coa"
        Me.Coa.Size = New System.Drawing.Size(191, 23)
        Me.Coa.TabIndex = 17
        '
        'EDDONo
        '
        Me.EDDONo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDDONo.ForeColor = System.Drawing.Color.Navy
        Me.EDDONo.Location = New System.Drawing.Point(143, 187)
        Me.EDDONo.Name = "EDDONo"
        Me.EDDONo.Size = New System.Drawing.Size(191, 23)
        Me.EDDONo.TabIndex = 11
        '
        'EDREf
        '
        Me.EDREf.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EDREf.Enabled = False
        Me.EDREf.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDREf.ForeColor = System.Drawing.Color.Navy
        Me.EDREf.Location = New System.Drawing.Point(564, 17)
        Me.EDREf.Name = "EDREf"
        Me.EDREf.ReadOnly = True
        Me.EDREf.Size = New System.Drawing.Size(160, 23)
        Me.EDREf.TabIndex = 18
        '
        'EDShipper
        '
        Me.EDShipper.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDShipper.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDShipper.DataSource = Me.TSHIPPERBindingSource
        Me.EDShipper.DisplayMember = "SP_NameEN"
        Me.EDShipper.DropDownWidth = 500
        Me.EDShipper.Enabled = False
        Me.EDShipper.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDShipper.ForeColor = System.Drawing.Color.Navy
        Me.EDShipper.FormattingEnabled = True
        Me.EDShipper.Location = New System.Drawing.Point(143, 83)
        Me.EDShipper.Name = "EDShipper"
        Me.EDShipper.Size = New System.Drawing.Size(191, 24)
        Me.EDShipper.TabIndex = 6
        Me.EDShipper.ValueMember = "ID"
        '
        'EDForword
        '
        Me.EDForword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.EDForword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.EDForword.DataSource = Me.TCOMPANYBindingSource
        Me.EDForword.DisplayMember = "COMPANY_NAME"
        Me.EDForword.DropDownWidth = 500
        Me.EDForword.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDForword.ForeColor = System.Drawing.Color.Navy
        Me.EDForword.FormattingEnabled = True
        Me.EDForword.Location = New System.Drawing.Point(143, 161)
        Me.EDForword.Name = "EDForword"
        Me.EDForword.Size = New System.Drawing.Size(191, 24)
        Me.EDForword.TabIndex = 9
        Me.EDForword.ValueMember = "COMPANY_ID"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(40, 188)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(99, 16)
        Me.Label18.TabIndex = 114
        Me.Label18.Text = "Delivery Order :"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(450, 74)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(110, 16)
        Me.Label15.TabIndex = 111
        Me.Label15.Text = "Vehicle Capacity :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(62, 84)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(77, 16)
        Me.Label14.TabIndex = 110
        Me.Label14.Text = "Sales ORG :"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(484, 182)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(76, 16)
        Me.Label10.TabIndex = 96
        Me.Label10.Text = "Load Type :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(462, 128)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(98, 16)
        Me.Label12.TabIndex = 97
        Me.Label12.Text = "Actual GI Date :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(485, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 16)
        Me.Label9.TabIndex = 93
        Me.Label9.Text = "Reference :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'EDTruckCapa
        '
        Me.EDTruckCapa.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EDTruckCapa.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDTruckCapa.ForeColor = System.Drawing.Color.Navy
        Me.EDTruckCapa.Location = New System.Drawing.Point(564, 69)
        Me.EDTruckCapa.Name = "EDTruckCapa"
        Me.EDTruckCapa.ReadOnly = True
        Me.EDTruckCapa.Size = New System.Drawing.Size(160, 23)
        Me.EDTruckCapa.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(62, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 16)
        Me.Label1.TabIndex = 83
        Me.Label1.Text = "Vehicle No.:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(469, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 16)
        Me.Label4.TabIndex = 90
        Me.Label4.Text = "Load Of Date :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(20, 162)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 16)
        Me.Label2.TabIndex = 88
        Me.Label2.Text = "Forwarding Agent :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(51, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 89
        Me.Label3.Text = "Driver Name :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.LawWeightout)
        Me.GroupBox5.Controls.Add(Me.Label39)
        Me.GroupBox5.Controls.Add(Me.editweightout)
        Me.GroupBox5.Controls.Add(Me.editweightin)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Controls.Add(Me.LawWeightIn)
        Me.GroupBox5.Controls.Add(Me.UpdateWeightIn)
        Me.GroupBox5.Controls.Add(Me.UpdateWeightOut)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.Black
        Me.GroupBox5.Location = New System.Drawing.Point(6, 358)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(760, 71)
        Me.GroupBox5.TabIndex = 31
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Weight"
        '
        'LawWeightout
        '
        Me.LawWeightout.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LawWeightout.Enabled = False
        Me.LawWeightout.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LawWeightout.ForeColor = System.Drawing.Color.Blue
        Me.LawWeightout.Location = New System.Drawing.Point(558, 14)
        Me.LawWeightout.Name = "LawWeightout"
        Me.LawWeightout.Size = New System.Drawing.Size(159, 23)
        Me.LawWeightout.TabIndex = 4
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label39.Location = New System.Drawing.Point(724, 17)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(26, 16)
        Me.Label39.TabIndex = 169
        Me.Label39.Text = "Kg."
        '
        'editweightout
        '
        Me.editweightout.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.edit_32
        Me.editweightout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.editweightout.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.editweightout.Location = New System.Drawing.Point(723, 41)
        Me.editweightout.Name = "editweightout"
        Me.editweightout.Size = New System.Drawing.Size(35, 23)
        Me.editweightout.TabIndex = 5
        Me.editweightout.UseVisualStyleBackColor = True
        '
        'editweightin
        '
        Me.editweightin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.editweightin.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.edit_32
        Me.editweightin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.editweightin.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.editweightin.Location = New System.Drawing.Point(334, 40)
        Me.editweightin.Name = "editweightin"
        Me.editweightin.Size = New System.Drawing.Size(35, 23)
        Me.editweightin.TabIndex = 2
        Me.editweightin.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.Location = New System.Drawing.Point(335, 17)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(26, 16)
        Me.Label17.TabIndex = 166
        Me.Label17.Text = "Kg."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(77, 18)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(57, 16)
        Me.Label13.TabIndex = 95
        Me.Label13.Text = "Raw In :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label24.Location = New System.Drawing.Point(62, 46)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(72, 16)
        Me.Label24.TabIndex = 95
        Me.Label24.Text = "Update In :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LawWeightIn
        '
        Me.LawWeightIn.Enabled = False
        Me.LawWeightIn.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LawWeightIn.ForeColor = System.Drawing.Color.Blue
        Me.LawWeightIn.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LawWeightIn.Location = New System.Drawing.Point(137, 13)
        Me.LawWeightIn.Name = "LawWeightIn"
        Me.LawWeightIn.Size = New System.Drawing.Size(191, 23)
        Me.LawWeightIn.TabIndex = 1
        '
        'UpdateWeightIn
        '
        Me.UpdateWeightIn.Enabled = False
        Me.UpdateWeightIn.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UpdateWeightIn.ForeColor = System.Drawing.Color.Red
        Me.UpdateWeightIn.Location = New System.Drawing.Point(137, 40)
        Me.UpdateWeightIn.Name = "UpdateWeightIn"
        Me.UpdateWeightIn.Size = New System.Drawing.Size(191, 23)
        Me.UpdateWeightIn.TabIndex = 3
        '
        'UpdateWeightOut
        '
        Me.UpdateWeightOut.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UpdateWeightOut.Enabled = False
        Me.UpdateWeightOut.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.UpdateWeightOut.ForeColor = System.Drawing.Color.Red
        Me.UpdateWeightOut.Location = New System.Drawing.Point(558, 41)
        Me.UpdateWeightOut.Name = "UpdateWeightOut"
        Me.UpdateWeightOut.Size = New System.Drawing.Size(159, 23)
        Me.UpdateWeightOut.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(489, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 16)
        Me.Label8.TabIndex = 164
        Me.Label8.Text = "Raw Out :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(474, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(81, 16)
        Me.Label7.TabIndex = 163
        Me.Label7.Text = "Update Out :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupUnload
        '
        Me.GroupUnload.BackColor = System.Drawing.Color.Transparent
        Me.GroupUnload.Controls.Add(Me.Button2)
        Me.GroupUnload.Controls.Add(Me.BtUnload)
        Me.GroupUnload.Controls.Add(Me.BindingNavigator2)
        Me.GroupUnload.Controls.Add(Me.Label52)
        Me.GroupUnload.Controls.Add(Me.SelectDate)
        Me.GroupUnload.Location = New System.Drawing.Point(6, 575)
        Me.GroupUnload.Name = "GroupUnload"
        Me.GroupUnload.Size = New System.Drawing.Size(770, 43)
        Me.GroupUnload.TabIndex = 3
        Me.GroupUnload.TabStop = False
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button2.Location = New System.Drawing.Point(627, 10)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 30)
        Me.Button2.TabIndex = 2109
        Me.Button2.Text = "&Import other station"
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button2.UseVisualStyleBackColor = True
        '
        'BtUnload
        '
        Me.BtUnload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BtUnload.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtUnload.Location = New System.Drawing.Point(545, 10)
        Me.BtUnload.Name = "BtUnload"
        Me.BtUnload.Size = New System.Drawing.Size(76, 30)
        Me.BtUnload.TabIndex = 213
        Me.BtUnload.Text = "&Unloading"
        Me.BtUnload.UseVisualStyleBackColor = True
        '
        'BindingNavigator2
        '
        Me.BindingNavigator2.AddNewItem = Nothing
        Me.BindingNavigator2.BindingSource = Me.V_LoadingnoteBindingSource
        Me.BindingNavigator2.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator2.DeleteItem = Nothing
        Me.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.ToolStripButton1, Me.BindingNavigatorSeparator2, Me.EDFillter})
        Me.BindingNavigator2.Location = New System.Drawing.Point(205, 11)
        Me.BindingNavigator2.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator2.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator2.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator2.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator2.Name = "BindingNavigator2"
        Me.BindingNavigator2.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator2.Size = New System.Drawing.Size(334, 25)
        Me.BindingNavigator2.TabIndex = 212
        Me.BindingNavigator2.Text = "BindingNavigator2"
        '
        'V_LoadingnoteBindingSource
        '
        Me.V_LoadingnoteBindingSource.AllowNew = True
        Me.V_LoadingnoteBindingSource.DataMember = "V_LOADINGNOTE"
        Me.V_LoadingnoteBindingSource.DataSource = Me.FPTDataSet
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.TAS_TOL.My.Resources.Resources.gtk_refresh
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'EDFillter
        '
        Me.EDFillter.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDFillter.Name = "EDFillter"
        Me.EDFillter.Size = New System.Drawing.Size(100, 25)
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label52.Location = New System.Drawing.Point(7, 14)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(52, 20)
        Me.Label52.TabIndex = 211
        Me.Label52.Text = "Date :"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SelectDate
        '
        Me.SelectDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.SelectDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.SelectDate.Location = New System.Drawing.Point(65, 11)
        Me.SelectDate.Name = "SelectDate"
        Me.SelectDate.Size = New System.Drawing.Size(137, 26)
        Me.SelectDate.TabIndex = 210
        '
        'GroupBt
        '
        Me.GroupBt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBt.BackColor = System.Drawing.Color.Transparent
        Me.GroupBt.Controls.Add(Me.BtWorkPrint)
        Me.GroupBt.Controls.Add(Me.weightoutprint)
        Me.GroupBt.Controls.Add(Me.GroupBtadd)
        Me.GroupBt.Controls.Add(Me.GroupPort)
        Me.GroupBt.Controls.Add(Me.WeightScal)
        Me.GroupBt.Controls.Add(Me.Label50)
        Me.GroupBt.Controls.Add(Me.BTDownload)
        Me.GroupBt.Location = New System.Drawing.Point(782, 509)
        Me.GroupBt.Name = "GroupBt"
        Me.GroupBt.Size = New System.Drawing.Size(586, 108)
        Me.GroupBt.TabIndex = 2
        Me.GroupBt.TabStop = False
        '
        'BtWorkPrint
        '
        Me.BtWorkPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BtWorkPrint.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtWorkPrint.Image = Global.TAS_TOL.My.Resources.Resources.Printer_2
        Me.BtWorkPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtWorkPrint.Location = New System.Drawing.Point(117, 61)
        Me.BtWorkPrint.Name = "BtWorkPrint"
        Me.BtWorkPrint.Size = New System.Drawing.Size(121, 43)
        Me.BtWorkPrint.TabIndex = 199
        Me.BtWorkPrint.Text = "Work Order Print"
        Me.BtWorkPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtWorkPrint.UseVisualStyleBackColor = True
        '
        'weightoutprint
        '
        Me.weightoutprint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.weightoutprint.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.weightoutprint.Image = Global.TAS_TOL.My.Resources.Resources.Printer_2
        Me.weightoutprint.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.weightoutprint.Location = New System.Drawing.Point(3, 61)
        Me.weightoutprint.Name = "weightoutprint"
        Me.weightoutprint.Size = New System.Drawing.Size(112, 43)
        Me.weightoutprint.TabIndex = 2
        Me.weightoutprint.Text = "Weight Out Print"
        Me.weightoutprint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.weightoutprint.UseVisualStyleBackColor = True
        '
        'GroupBtadd
        '
        Me.GroupBtadd.BackColor = System.Drawing.Color.Transparent
        Me.GroupBtadd.Controls.Add(Me.PrintBut)
        Me.GroupBtadd.Controls.Add(Me.EditData)
        Me.GroupBtadd.Controls.Add(Me.Adddata)
        Me.GroupBtadd.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBtadd.Location = New System.Drawing.Point(5, 1)
        Me.GroupBtadd.Name = "GroupBtadd"
        Me.GroupBtadd.Size = New System.Drawing.Size(312, 59)
        Me.GroupBtadd.TabIndex = 0
        Me.GroupBtadd.TabStop = False
        '
        'PrintBut
        '
        Me.PrintBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PrintBut.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PrintBut.Image = Global.TAS_TOL.My.Resources.Resources.Printer_21
        Me.PrintBut.Location = New System.Drawing.Point(210, 12)
        Me.PrintBut.Name = "PrintBut"
        Me.PrintBut.Size = New System.Drawing.Size(99, 44)
        Me.PrintBut.TabIndex = 2
        Me.PrintBut.Text = "&Print"
        Me.PrintBut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.PrintBut.UseVisualStyleBackColor = True
        Me.PrintBut.Visible = False
        '
        'EditData
        '
        Me.EditData.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EditData.Image = Global.TAS_TOL.My.Resources.Resources.edit_32
        Me.EditData.Location = New System.Drawing.Point(108, 13)
        Me.EditData.Name = "EditData"
        Me.EditData.Size = New System.Drawing.Size(96, 43)
        Me.EditData.TabIndex = 1
        Me.EditData.Text = "&Edit"
        Me.EditData.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.EditData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.EditData.UseVisualStyleBackColor = True
        '
        'Adddata
        '
        Me.Adddata.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Adddata.Image = Global.TAS_TOL.My.Resources.Resources.add_1
        Me.Adddata.Location = New System.Drawing.Point(6, 13)
        Me.Adddata.Name = "Adddata"
        Me.Adddata.Size = New System.Drawing.Size(96, 43)
        Me.Adddata.TabIndex = 0
        Me.Adddata.Text = "&Add"
        Me.Adddata.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Adddata.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Adddata.UseVisualStyleBackColor = True
        '
        'GroupPort
        '
        Me.GroupPort.BackColor = System.Drawing.Color.Transparent
        Me.GroupPort.Controls.Add(Me.BTEDIT_SAP)
        Me.GroupPort.Controls.Add(Me.WeightOut)
        Me.GroupPort.Controls.Add(Me.WeightIn)
        Me.GroupPort.Controls.Add(Me.Bcancel)
        Me.GroupPort.Controls.Add(Me.Edit)
        Me.GroupPort.Controls.Add(Me.Bsave)
        Me.GroupPort.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupPort.Location = New System.Drawing.Point(323, 1)
        Me.GroupPort.Name = "GroupPort"
        Me.GroupPort.Size = New System.Drawing.Size(443, 59)
        Me.GroupPort.TabIndex = 1
        Me.GroupPort.TabStop = False
        Me.GroupPort.Visible = False
        '
        'BTEDIT_SAP
        '
        Me.BTEDIT_SAP.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTEDIT_SAP.Image = Global.TAS_TOL.My.Resources.Resources.post_to_blog_32
        Me.BTEDIT_SAP.Location = New System.Drawing.Point(6, 12)
        Me.BTEDIT_SAP.Name = "BTEDIT_SAP"
        Me.BTEDIT_SAP.Size = New System.Drawing.Size(95, 44)
        Me.BTEDIT_SAP.TabIndex = 4
        Me.BTEDIT_SAP.Text = "&Post"
        Me.BTEDIT_SAP.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTEDIT_SAP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTEDIT_SAP.UseVisualStyleBackColor = True
        Me.BTEDIT_SAP.Visible = False
        '
        'WeightOut
        '
        Me.WeightOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.WeightOut.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.WeightOut.Image = Global.TAS_TOL.My.Resources.Resources.weight
        Me.WeightOut.Location = New System.Drawing.Point(107, 12)
        Me.WeightOut.Name = "WeightOut"
        Me.WeightOut.Size = New System.Drawing.Size(112, 44)
        Me.WeightOut.TabIndex = 1
        Me.WeightOut.Text = "weight out"
        Me.WeightOut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.WeightOut.UseVisualStyleBackColor = True
        '
        'WeightIn
        '
        Me.WeightIn.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.WeightIn.Image = Global.TAS_TOL.My.Resources.Resources.weight
        Me.WeightIn.Location = New System.Drawing.Point(107, 12)
        Me.WeightIn.Name = "WeightIn"
        Me.WeightIn.Size = New System.Drawing.Size(112, 44)
        Me.WeightIn.TabIndex = 143
        Me.WeightIn.Text = "weight in"
        Me.WeightIn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.WeightIn.UseVisualStyleBackColor = True
        '
        'Bcancel
        '
        Me.Bcancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Bcancel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Bcancel.Image = Global.TAS_TOL.My.Resources.Resources.Button_Cancel
        Me.Bcancel.Location = New System.Drawing.Point(332, 12)
        Me.Bcancel.Name = "Bcancel"
        Me.Bcancel.Size = New System.Drawing.Size(104, 44)
        Me.Bcancel.TabIndex = 3
        Me.Bcancel.Tag = "18"
        Me.Bcancel.Text = "&Cancel"
        Me.Bcancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Bcancel.UseVisualStyleBackColor = True
        '
        'Edit
        '
        Me.Edit.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Edit.Image = Global.TAS_TOL.My.Resources.Resources.gtk_refresh
        Me.Edit.Location = New System.Drawing.Point(225, 13)
        Me.Edit.Name = "Edit"
        Me.Edit.Size = New System.Drawing.Size(101, 44)
        Me.Edit.TabIndex = 5
        Me.Edit.Text = "&Update"
        Me.Edit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Edit.UseVisualStyleBackColor = True
        '
        'Bsave
        '
        Me.Bsave.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Bsave.Image = Global.TAS_TOL.My.Resources.Resources.document_save
        Me.Bsave.Location = New System.Drawing.Point(226, 12)
        Me.Bsave.Name = "Bsave"
        Me.Bsave.Size = New System.Drawing.Size(100, 44)
        Me.Bsave.TabIndex = 2
        Me.Bsave.Text = "&Save"
        Me.Bsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Bsave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Bsave.UseVisualStyleBackColor = True
        '
        'WeightScal
        '
        Me.WeightScal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WeightScal.BackColor = System.Drawing.SystemColors.MenuText
        Me.WeightScal.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.WeightScal.ForeColor = System.Drawing.Color.Lime
        Me.WeightScal.Location = New System.Drawing.Point(323, 62)
        Me.WeightScal.Name = "WeightScal"
        Me.WeightScal.Size = New System.Drawing.Size(8, 40)
        Me.WeightScal.TabIndex = 3
        Me.WeightScal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label50
        '
        Me.Label50.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label50.AutoSize = True
        Me.Label50.BackColor = System.Drawing.Color.Transparent
        Me.Label50.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label50.Location = New System.Drawing.Point(218, 77)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(30, 18)
        Me.Label50.TabIndex = 193
        Me.Label50.Text = "Kg."
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'BTDownload
        '
        Me.BTDownload.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTDownload.Image = Global.TAS_TOL.My.Resources.Resources.Download_32
        Me.BTDownload.Location = New System.Drawing.Point(427, 62)
        Me.BTDownload.Name = "BTDownload"
        Me.BTDownload.Size = New System.Drawing.Size(116, 43)
        Me.BTDownload.TabIndex = 200
        Me.BTDownload.Text = "&Download"
        Me.BTDownload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTDownload.UseVisualStyleBackColor = True
        Me.BTDownload.Visible = False
        '
        'DBGridAddnote
        '
        Me.DBGridAddnote.AllowUserToAddRows = False
        Me.DBGridAddnote.AllowUserToDeleteRows = False
        Me.DBGridAddnote.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DBGridAddnote.AutoGenerateColumns = False
        Me.DBGridAddnote.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGridAddnote.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LOAD_DOfull, Me.DataGridViewTextBoxColumn1, Me.LOAD_DID, Me.LOADDATEDataGridViewTextBoxColumn, Me.STATUS_NAME, Me.Do_status, Me.DataGridViewTextBoxColumn2, Me.Customer_name, Me.Product_name, Me.LOAD_VEHICLE, Me.LOAD_DRIVER, Me.LCPRESETDataGridViewTextBoxColumn, Me.DataGridViewTextBoxColumn3, Me.LCBAYDataGridViewTextBoxColumn, Me.Column1, Me.SP_Code, Me.LOAD_TANK, Me.do_postdate, Me.ReferenceDataGridViewTextBoxColumn})
        Me.DBGridAddnote.DataSource = Me.V_LoadingnoteBindingSource
        Me.DBGridAddnote.Location = New System.Drawing.Point(6, 623)
        Me.DBGridAddnote.MultiSelect = False
        Me.DBGridAddnote.Name = "DBGridAddnote"
        Me.DBGridAddnote.ReadOnly = True
        Me.DBGridAddnote.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DBGridAddnote.Size = New System.Drawing.Size(1361, 122)
        Me.DBGridAddnote.TabIndex = 4
        '
        'LOAD_DOfull
        '
        Me.LOAD_DOfull.DataPropertyName = "LOAD_DOfull"
        Me.LOAD_DOfull.HeaderText = "Delivery order"
        Me.LOAD_DOfull.Name = "LOAD_DOfull"
        Me.LOAD_DOfull.ReadOnly = True
        Me.LOAD_DOfull.Width = 120
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "dO_ITEM"
        Me.DataGridViewTextBoxColumn1.HeaderText = "D/O item"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'LOAD_DID
        '
        Me.LOAD_DID.DataPropertyName = "LOAD_DID"
        Me.LOAD_DID.HeaderText = "Load NO."
        Me.LOAD_DID.Name = "LOAD_DID"
        Me.LOAD_DID.ReadOnly = True
        Me.LOAD_DID.Width = 80
        '
        'LOADDATEDataGridViewTextBoxColumn
        '
        Me.LOADDATEDataGridViewTextBoxColumn.DataPropertyName = "LOAD_DATE"
        DataGridViewCellStyle1.Format = "dd/MM/yyyy HH:MM"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.LOADDATEDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle1
        Me.LOADDATEDataGridViewTextBoxColumn.FillWeight = 120.0!
        Me.LOADDATEDataGridViewTextBoxColumn.HeaderText = "Load date"
        Me.LOADDATEDataGridViewTextBoxColumn.Name = "LOADDATEDataGridViewTextBoxColumn"
        Me.LOADDATEDataGridViewTextBoxColumn.ReadOnly = True
        Me.LOADDATEDataGridViewTextBoxColumn.Width = 120
        '
        'STATUS_NAME
        '
        Me.STATUS_NAME.DataPropertyName = "STATUS_NAME"
        Me.STATUS_NAME.HeaderText = "Status"
        Me.STATUS_NAME.Name = "STATUS_NAME"
        Me.STATUS_NAME.ReadOnly = True
        '
        'Do_status
        '
        Me.Do_status.DataPropertyName = "Do_status"
        Me.Do_status.HeaderText = "Post"
        Me.Do_status.Name = "Do_status"
        Me.Do_status.ReadOnly = True
        Me.Do_status.Width = 50
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "pLANT"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Plant"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Customer_name
        '
        Me.Customer_name.DataPropertyName = "Customer_name"
        Me.Customer_name.HeaderText = "Customer"
        Me.Customer_name.Name = "Customer_name"
        Me.Customer_name.ReadOnly = True
        Me.Customer_name.Width = 150
        '
        'Product_name
        '
        Me.Product_name.DataPropertyName = "Product_name"
        Me.Product_name.HeaderText = "Product"
        Me.Product_name.Name = "Product_name"
        Me.Product_name.ReadOnly = True
        Me.Product_name.Width = 150
        '
        'LOAD_VEHICLE
        '
        Me.LOAD_VEHICLE.DataPropertyName = "LOAD_VEHICLE"
        Me.LOAD_VEHICLE.HeaderText = "Vehicle"
        Me.LOAD_VEHICLE.Name = "LOAD_VEHICLE"
        Me.LOAD_VEHICLE.ReadOnly = True
        '
        'LOAD_DRIVER
        '
        Me.LOAD_DRIVER.DataPropertyName = "LOAD_DRIVER"
        Me.LOAD_DRIVER.HeaderText = "Driver"
        Me.LOAD_DRIVER.Name = "LOAD_DRIVER"
        Me.LOAD_DRIVER.ReadOnly = True
        '
        'LCPRESETDataGridViewTextBoxColumn
        '
        Me.LCPRESETDataGridViewTextBoxColumn.DataPropertyName = "LOAD_PRESET"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.LCPRESETDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.LCPRESETDataGridViewTextBoxColumn.HeaderText = "Setpoint"
        Me.LCPRESETDataGridViewTextBoxColumn.Name = "LCPRESETDataGridViewTextBoxColumn"
        Me.LCPRESETDataGridViewTextBoxColumn.ReadOnly = True
        Me.LCPRESETDataGridViewTextBoxColumn.Width = 80
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "LOAD_CARD"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn3.HeaderText = "Password"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 70
        '
        'LCBAYDataGridViewTextBoxColumn
        '
        Me.LCBAYDataGridViewTextBoxColumn.DataPropertyName = "LC_BAY"
        Me.LCBAYDataGridViewTextBoxColumn.HeaderText = "Bay"
        Me.LCBAYDataGridViewTextBoxColumn.Name = "LCBAYDataGridViewTextBoxColumn"
        Me.LCBAYDataGridViewTextBoxColumn.ReadOnly = True
        Me.LCBAYDataGridViewTextBoxColumn.Width = 50
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "WeightIN"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column1.HeaderText = "Weight In"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 70
        '
        'SP_Code
        '
        Me.SP_Code.DataPropertyName = "WeightOut"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.SP_Code.DefaultCellStyle = DataGridViewCellStyle5
        Me.SP_Code.HeaderText = "Weight Out"
        Me.SP_Code.Name = "SP_Code"
        Me.SP_Code.ReadOnly = True
        Me.SP_Code.Width = 70
        '
        'LOAD_TANK
        '
        Me.LOAD_TANK.DataPropertyName = "WeightTotal"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.LOAD_TANK.DefaultCellStyle = DataGridViewCellStyle6
        Me.LOAD_TANK.HeaderText = "Weight Product"
        Me.LOAD_TANK.Name = "LOAD_TANK"
        Me.LOAD_TANK.ReadOnly = True
        Me.LOAD_TANK.Width = 70
        '
        'do_postdate
        '
        Me.do_postdate.DataPropertyName = "do_postdate"
        DataGridViewCellStyle7.Format = "dd/MM/yyyy HH:MM"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.do_postdate.DefaultCellStyle = DataGridViewCellStyle7
        Me.do_postdate.HeaderText = "D/O Post date"
        Me.do_postdate.Name = "do_postdate"
        Me.do_postdate.ReadOnly = True
        '
        'ReferenceDataGridViewTextBoxColumn
        '
        Me.ReferenceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ReferenceDataGridViewTextBoxColumn.DataPropertyName = "LC_SEAL"
        Me.ReferenceDataGridViewTextBoxColumn.HeaderText = "Seal No."
        Me.ReferenceDataGridViewTextBoxColumn.MinimumWidth = 50
        Me.ReferenceDataGridViewTextBoxColumn.Name = "ReferenceDataGridViewTextBoxColumn"
        Me.ReferenceDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PTruck
        '
        Me.PTruck.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PTruck.BackColor = System.Drawing.Color.Black
        Me.PTruck.Controls.Add(Me.Imagetruck)
        Me.PTruck.Controls.Add(Me.RadGroupBox1)
        Me.PTruck.Controls.Add(Me.InG)
        Me.PTruck.Controls.Add(Me.InR)
        Me.PTruck.Controls.Add(Me.Label68)
        Me.PTruck.Controls.Add(Me.PictureBox4)
        Me.PTruck.Controls.Add(Me.PictureBox6)
        Me.PTruck.Controls.Add(Me.PictureBox5)
        Me.PTruck.Controls.Add(Me.PictureBox1)
        Me.PTruck.Controls.Add(Me.Panel1)
        Me.PTruck.Controls.Add(Me.PictureBox2)
        Me.PTruck.Controls.Add(Me.InGrean)
        Me.PTruck.Location = New System.Drawing.Point(784, 19)
        Me.PTruck.Name = "PTruck"
        Me.PTruck.Size = New System.Drawing.Size(580, 458)
        Me.PTruck.TabIndex = 1
        '
        'Imagetruck
        '
        Me.Imagetruck.ErrorImage = Nothing
        Me.Imagetruck.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook2
        Me.Imagetruck.Location = New System.Drawing.Point(159, 81)
        Me.Imagetruck.Name = "Imagetruck"
        Me.Imagetruck.Size = New System.Drawing.Size(604, 213)
        Me.Imagetruck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Imagetruck.TabIndex = 6
        Me.Imagetruck.TabStop = False
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox1.Controls.Add(Me.BTG)
        Me.RadGroupBox1.Controls.Add(Me.BTR)
        Me.RadGroupBox1.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox1.HeaderText = "Traffic light Control"
        Me.RadGroupBox1.Location = New System.Drawing.Point(424, 343)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(322, 104)
        Me.RadGroupBox1.TabIndex = 23
        Me.RadGroupBox1.Text = "Traffic light Control"
        Me.RadGroupBox1.ThemeName = "Breeze"
        '
        'BTG
        '
        Me.BTG.BackColor = System.Drawing.Color.Transparent
        Me.BTG.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTG.Location = New System.Drawing.Point(32, 16)
        Me.BTG.Name = "BTG"
        Me.BTG.Size = New System.Drawing.Size(85, 78)
        Me.BTG.TabIndex = 22
        Me.BTG.Text = "Go"
        Me.BTG.ThemeName = "Breeze"
        CType(Me.BTG.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "Go"
        CType(Me.BTG.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Shape = Me.EllipseShape1
        CType(Me.BTG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Green
        CType(Me.BTG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.Green
        CType(Me.BTG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Green
        CType(Me.BTG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Lime
        CType(Me.BTG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).Shape = Me.EllipseShape1
        CType(Me.BTG.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        CType(Me.BTG.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.BTG.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'BTR
        '
        Me.BTR.BackColor = System.Drawing.Color.Transparent
        Me.BTR.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTR.Location = New System.Drawing.Point(213, 16)
        Me.BTR.Name = "BTR"
        Me.BTR.Size = New System.Drawing.Size(85, 78)
        Me.BTR.TabIndex = 19
        Me.BTR.Text = "Go"
        Me.BTR.ThemeName = "Breeze"
        CType(Me.BTR.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Text = "Go"
        CType(Me.BTR.GetChildAt(0), Telerik.WinControls.UI.RadButtonElement).Shape = Me.EllipseShape1
        CType(Me.BTR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(104, Byte), Integer), CType(CType(104, Byte), Integer))
        CType(Me.BTR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer))
        CType(Me.BTR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.Red
        CType(Me.BTR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(93, Byte), Integer))
        CType(Me.BTR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).Shape = Me.EllipseShape1
        CType(Me.BTR.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        CType(Me.BTR.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.Primitives.TextPrimitive).Alignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.BTR.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'InG
        '
        Me.InG.BackColor = System.Drawing.Color.Transparent
        Me.InG.Location = New System.Drawing.Point(90, 153)
        Me.InG.Name = "InG"
        Me.InG.Size = New System.Drawing.Size(31, 26)
        Me.InG.TabIndex = 21
        Me.InG.Visible = False
        CType(Me.InG.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.InG.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.EllipseShape1
        CType(Me.InG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(80, Byte), Integer))
        CType(Me.InG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(53, Byte), Integer))
        CType(Me.InG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer))
        CType(Me.InG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.OfficeGlass
        CType(Me.InG.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(165, Byte), Integer))
        CType(Me.InG.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'InR
        '
        Me.InR.BackColor = System.Drawing.Color.Transparent
        Me.InR.Location = New System.Drawing.Point(90, 122)
        Me.InR.Name = "InR"
        Me.InR.Size = New System.Drawing.Size(31, 26)
        Me.InR.TabIndex = 20
        Me.InR.Visible = False
        CType(Me.InR.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.InR.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.EllipseShape1
        CType(Me.InR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(88, Byte), Integer))
        CType(Me.InR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(49, Byte), Integer), CType(CType(49, Byte), Integer))
        CType(Me.InR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        CType(Me.InR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.OfficeGlassRect
        CType(Me.InR.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(134, Byte), Integer), CType(CType(134, Byte), Integer))
        CType(Me.InR.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 1.0!
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("DS-Digital", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.LawnGreen
        Me.Label68.Location = New System.Drawing.Point(239, 21)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(429, 47)
        Me.Label68.TabIndex = 15
        Me.Label68.Text = "weightcontrol Room"
        Me.Label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Black
        Me.PictureBox4.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox4.ErrorImage = Nothing
        Me.PictureBox4.Location = New System.Drawing.Point(208, 262)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 10
        Me.PictureBox4.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Black
        Me.PictureBox6.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox6.ErrorImage = Nothing
        Me.PictureBox6.Location = New System.Drawing.Point(622, 262)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 9
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Black
        Me.PictureBox5.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox5.ErrorImage = Nothing
        Me.PictureBox5.Location = New System.Drawing.Point(484, 262)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 9
        Me.PictureBox5.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Black
        Me.PictureBox1.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Location = New System.Drawing.Point(346, 262)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DimGray
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ShapeContainer1)
        Me.Panel1.Location = New System.Drawing.Point(110, 293)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(647, 18)
        Me.Panel1.TabIndex = 8
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape6})
        Me.ShapeContainer1.Size = New System.Drawing.Size(645, 16)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape6
        '
        Me.RectangleShape6.BackColor = System.Drawing.Color.Gray
        Me.RectangleShape6.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape6.BorderColor = System.Drawing.Color.White
        Me.RectangleShape6.Location = New System.Drawing.Point(0, 0)
        Me.RectangleShape6.Name = "RectangleShape6"
        Me.RectangleShape6.Size = New System.Drawing.Size(644, 15)
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Black
        Me.PictureBox2.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.ErrorImage = Nothing
        Me.PictureBox2.Location = New System.Drawing.Point(104, 181)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(6, 128)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 10
        Me.PictureBox2.TabStop = False
        '
        'InGrean
        '
        Me.InGrean.ErrorImage = Nothing
        Me.InGrean.Image = Global.TAS_TOL.My.Resources.Resources.Traffic_light_icongen5
        Me.InGrean.Location = New System.Drawing.Point(63, 110)
        Me.InGrean.Name = "InGrean"
        Me.InGrean.Size = New System.Drawing.Size(86, 72)
        Me.InGrean.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.InGrean.TabIndex = 2
        Me.InGrean.TabStop = False
        '
        'GDetail
        '
        Me.GDetail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GDetail.BackColor = System.Drawing.Color.Transparent
        Me.GDetail.Controls.Add(Me.GroupBox15)
        Me.GDetail.Controls.Add(Me.GPreset)
        Me.GDetail.Controls.Add(Me.GroupBox10)
        Me.GDetail.Controls.Add(Me.GroupBox11)
        Me.GDetail.Controls.Add(Me.GroupBox12)
        Me.GDetail.Controls.Add(Me.GProduct)
        Me.GDetail.Controls.Add(Me.RadPanel2)
        Me.GDetail.Controls.Add(Me.RadPanel3)
        Me.GDetail.Controls.Add(Me.RadPanel4)
        Me.GDetail.Controls.Add(Me.RadPanel5)
        Me.GDetail.Controls.Add(Me.RadPanel6)
        Me.GDetail.Controls.Add(Me.RadPanel1)
        Me.GDetail.Controls.Add(Me.Edremark2)
        Me.GDetail.Controls.Add(Me.GroupBatchtable)
        Me.GDetail.Controls.Add(Me.GroupProduct)
        Me.GDetail.Controls.Add(Me.Label28)
        Me.GDetail.Controls.Add(Me.Edremark)
        Me.GDetail.Location = New System.Drawing.Point(782, 8)
        Me.GDetail.Name = "GDetail"
        Me.GDetail.Size = New System.Drawing.Size(585, 505)
        Me.GDetail.TabIndex = 1
        Me.GDetail.TabStop = False
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.TextBox25)
        Me.GroupBox15.Location = New System.Drawing.Point(4, 108)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(50, 206)
        Me.GroupBox15.TabIndex = 45
        Me.GroupBox15.TabStop = False
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(1, 277)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(43, 21)
        Me.TextBox25.TabIndex = 127
        Me.TextBox25.Text = "10"
        Me.TextBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GPreset
        '
        Me.GPreset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GPreset.Controls.Add(Me.PRESETNO1)
        Me.GPreset.Controls.Add(Me.TextBox14)
        Me.GPreset.Location = New System.Drawing.Point(285, 108)
        Me.GPreset.Name = "GPreset"
        Me.GPreset.Size = New System.Drawing.Size(87, 206)
        Me.GPreset.TabIndex = 48
        Me.GPreset.TabStop = False
        '
        'PRESETNO1
        '
        Me.PRESETNO1.Enabled = False
        Me.PRESETNO1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PRESETNO1.Location = New System.Drawing.Point(3, 202)
        Me.PRESETNO1.Name = "PRESETNO1"
        Me.PRESETNO1.ReadOnly = True
        Me.PRESETNO1.Size = New System.Drawing.Size(84, 22)
        Me.PRESETNO1.TabIndex = 131
        Me.PRESETNO1.Visible = False
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(2, 276)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(120, 21)
        Me.TextBox14.TabIndex = 119
        '
        'GroupBox10
        '
        Me.GroupBox10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox10.Location = New System.Drawing.Point(464, 109)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(115, 206)
        Me.GroupBox10.TabIndex = 50
        Me.GroupBox10.TabStop = False
        '
        'GroupBox11
        '
        Me.GroupBox11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox11.Location = New System.Drawing.Point(377, 108)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(83, 206)
        Me.GroupBox11.TabIndex = 49
        Me.GroupBox11.TabStop = False
        '
        'GroupBox12
        '
        Me.GroupBox12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox12.Location = New System.Drawing.Point(193, 108)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(87, 206)
        Me.GroupBox12.TabIndex = 47
        Me.GroupBox12.TabStop = False
        '
        'GProduct
        '
        Me.GProduct.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GProduct.Location = New System.Drawing.Point(58, 108)
        Me.GProduct.Name = "GProduct"
        Me.GProduct.Size = New System.Drawing.Size(131, 206)
        Me.GProduct.TabIndex = 46
        Me.GProduct.TabStop = False
        '
        'RadPanel2
        '
        Me.RadPanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadPanel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel2.Location = New System.Drawing.Point(60, 88)
        Me.RadPanel2.Name = "RadPanel2"
        Me.RadPanel2.Size = New System.Drawing.Size(128, 24)
        Me.RadPanel2.TabIndex = 2102
        Me.RadPanel2.Text = "Product"
        Me.RadPanel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel2.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "Product"
        CType(Me.RadPanel2.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.TrackBarUThumbShape1
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 0.0!
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel2.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.SystemColors.Window
        '
        'RadPanel3
        '
        Me.RadPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadPanel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel3.Location = New System.Drawing.Point(192, 88)
        Me.RadPanel3.Name = "RadPanel3"
        Me.RadPanel3.Size = New System.Drawing.Size(87, 24)
        Me.RadPanel3.TabIndex = 2103
        Me.RadPanel3.Text = "Capacity"
        Me.RadPanel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "Capacity"
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.TrackBarUThumbShape1
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 0.0!
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.SystemColors.Window
        '
        'RadPanel4
        '
        Me.RadPanel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadPanel4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel4.Location = New System.Drawing.Point(284, 88)
        Me.RadPanel4.Name = "RadPanel4"
        Me.RadPanel4.Size = New System.Drawing.Size(86, 24)
        Me.RadPanel4.TabIndex = 2104
        Me.RadPanel4.Text = "Preset"
        Me.RadPanel4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "Preset"
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.TrackBarUThumbShape1
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 0.0!
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.SystemColors.Window
        '
        'RadPanel5
        '
        Me.RadPanel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadPanel5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel5.Location = New System.Drawing.Point(376, 88)
        Me.RadPanel5.Name = "RadPanel5"
        Me.RadPanel5.Size = New System.Drawing.Size(83, 24)
        Me.RadPanel5.TabIndex = 2105
        Me.RadPanel5.Text = "Island/Bay"
        Me.RadPanel5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel5.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "Island/Bay"
        CType(Me.RadPanel5.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.TrackBarUThumbShape1
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 0.0!
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.SystemColors.Window
        '
        'RadPanel6
        '
        Me.RadPanel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadPanel6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel6.Location = New System.Drawing.Point(464, 88)
        Me.RadPanel6.Name = "RadPanel6"
        Me.RadPanel6.Size = New System.Drawing.Size(113, 24)
        Me.RadPanel6.TabIndex = 2106
        Me.RadPanel6.Text = "Meter"
        Me.RadPanel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "Meter"
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.TrackBarUThumbShape1
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 0.0!
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.SystemColors.Window
        '
        'RadPanel1
        '
        Me.RadPanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadPanel1.Location = New System.Drawing.Point(5, 88)
        Me.RadPanel1.Name = "RadPanel1"
        Me.RadPanel1.Size = New System.Drawing.Size(49, 24)
        Me.RadPanel1.TabIndex = 2107
        Me.RadPanel1.Text = "Comp."
        Me.RadPanel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel1.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "Comp."
        CType(Me.RadPanel1.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.TrackBarUThumbShape1
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(22, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(182, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(111, Byte), Integer), CType(CType(98, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Width = 0.0!
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(2), Telerik.WinControls.Primitives.TextPrimitive).ForeColor = System.Drawing.SystemColors.Window
        '
        'Edremark2
        '
        Me.Edremark2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Edremark2.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.Edremark2.Location = New System.Drawing.Point(323, 472)
        Me.Edremark2.Name = "Edremark2"
        Me.Edremark2.Size = New System.Drawing.Size(254, 27)
        Me.Edremark2.TabIndex = 146
        Me.Edremark2.Text = ""
        '
        'GroupBatchtable
        '
        Me.GroupBatchtable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBatchtable.Controls.Add(Me.BindingNavigator1)
        Me.GroupBatchtable.Controls.Add(Me.MeterGrid)
        Me.GroupBatchtable.Enabled = False
        Me.GroupBatchtable.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBatchtable.Location = New System.Drawing.Point(4, 313)
        Me.GroupBatchtable.Name = "GroupBatchtable"
        Me.GroupBatchtable.Size = New System.Drawing.Size(575, 157)
        Me.GroupBatchtable.TabIndex = 1
        Me.GroupBatchtable.TabStop = False
        Me.GroupBatchtable.Text = "Batch table"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Me.Btadd
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BindingSource = Me.TBATCHLOSTTASBindingSource
        Me.BindingNavigator1.CountItem = Me.ToolStripLabel1
        Me.BindingNavigator1.DeleteItem = Me.BtDelete
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Btfirst, Me.Btprevious, Me.ToolStripSeparator1, Me.ToolStripTextBox1, Me.ToolStripLabel1, Me.ToolStripSeparator2, Me.Btnext, Me.BtLast, Me.ToolStripSeparator3, Me.Btadd, Me.BtEdit, Me.BtDelete, Me.toolStripSeparator})
        Me.BindingNavigator1.Location = New System.Drawing.Point(3, 19)
        Me.BindingNavigator1.MoveFirstItem = Me.Btfirst
        Me.BindingNavigator1.MoveLastItem = Me.BtLast
        Me.BindingNavigator1.MoveNextItem = Me.Btnext
        Me.BindingNavigator1.MovePreviousItem = Me.Btprevious
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.ToolStripTextBox1
        Me.BindingNavigator1.Size = New System.Drawing.Size(569, 23)
        Me.BindingNavigator1.TabIndex = 6
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'Btadd
        '
        Me.Btadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btadd.Image = CType(resources.GetObject("Btadd.Image"), System.Drawing.Image)
        Me.Btadd.Name = "Btadd"
        Me.Btadd.RightToLeftAutoMirrorImage = True
        Me.Btadd.Size = New System.Drawing.Size(23, 20)
        Me.Btadd.Text = "Add new"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(35, 20)
        Me.ToolStripLabel1.Text = "of {0}"
        Me.ToolStripLabel1.ToolTipText = "Total number of items"
        '
        'BtDelete
        '
        Me.BtDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtDelete.Image = CType(resources.GetObject("BtDelete.Image"), System.Drawing.Image)
        Me.BtDelete.Name = "BtDelete"
        Me.BtDelete.RightToLeftAutoMirrorImage = True
        Me.BtDelete.Size = New System.Drawing.Size(23, 20)
        Me.BtDelete.Text = "Delete"
        '
        'Btfirst
        '
        Me.Btfirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btfirst.Image = CType(resources.GetObject("Btfirst.Image"), System.Drawing.Image)
        Me.Btfirst.Name = "Btfirst"
        Me.Btfirst.RightToLeftAutoMirrorImage = True
        Me.Btfirst.Size = New System.Drawing.Size(23, 20)
        Me.Btfirst.Text = "Move first"
        '
        'Btprevious
        '
        Me.Btprevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btprevious.Image = CType(resources.GetObject("Btprevious.Image"), System.Drawing.Image)
        Me.Btprevious.Name = "Btprevious"
        Me.Btprevious.RightToLeftAutoMirrorImage = True
        Me.Btprevious.Size = New System.Drawing.Size(23, 20)
        Me.Btprevious.Text = "Move previous"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 23)
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.AccessibleName = "Position"
        Me.ToolStripTextBox1.AutoSize = False
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBox1.Text = "0"
        Me.ToolStripTextBox1.ToolTipText = "Current position"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 23)
        '
        'Btnext
        '
        Me.Btnext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Btnext.Image = CType(resources.GetObject("Btnext.Image"), System.Drawing.Image)
        Me.Btnext.Name = "Btnext"
        Me.Btnext.RightToLeftAutoMirrorImage = True
        Me.Btnext.Size = New System.Drawing.Size(23, 20)
        Me.Btnext.Text = "Move next"
        '
        'BtLast
        '
        Me.BtLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtLast.Image = CType(resources.GetObject("BtLast.Image"), System.Drawing.Image)
        Me.BtLast.Name = "BtLast"
        Me.BtLast.RightToLeftAutoMirrorImage = True
        Me.BtLast.Size = New System.Drawing.Size(23, 20)
        Me.BtLast.Text = "Move last"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 23)
        '
        'BtEdit
        '
        Me.BtEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BtEdit.Image = CType(resources.GetObject("BtEdit.Image"), System.Drawing.Image)
        Me.BtEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtEdit.Name = "BtEdit"
        Me.BtEdit.RightToLeftAutoMirrorImage = True
        Me.BtEdit.Size = New System.Drawing.Size(23, 20)
        Me.BtEdit.Text = "Edit"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(6, 23)
        '
        'MeterGrid
        '
        Me.MeterGrid.AllowUserToAddRows = False
        Me.MeterGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MeterGrid.AutoGenerateColumns = False
        Me.MeterGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MeterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MeterGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DO_NO, Me.DO_ITEM, Me.MAT_CODE, Me.stock_quantity, Me.BATCH_NO, Me.STOR, Me.PLANT, Me.Higher_Level})
        Me.MeterGrid.DataSource = Me.TBATCHLOSTTASBindingSource
        Me.MeterGrid.Location = New System.Drawing.Point(3, 42)
        Me.MeterGrid.Name = "MeterGrid"
        Me.MeterGrid.Size = New System.Drawing.Size(569, 109)
        Me.MeterGrid.TabIndex = 4
        '
        'DO_NO
        '
        Me.DO_NO.DataPropertyName = "DO_NO"
        Me.DO_NO.HeaderText = "Delivery order"
        Me.DO_NO.Name = "DO_NO"
        Me.DO_NO.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DO_NO.Width = 80
        '
        'DO_ITEM
        '
        Me.DO_ITEM.DataPropertyName = "DO_ITEM"
        Me.DO_ITEM.HeaderText = "D/O item"
        Me.DO_ITEM.Name = "DO_ITEM"
        Me.DO_ITEM.Width = 60
        '
        'MAT_CODE
        '
        Me.MAT_CODE.DataPropertyName = "MAT_CODE"
        Me.MAT_CODE.HeaderText = "Material code"
        Me.MAT_CODE.Name = "MAT_CODE"
        '
        'stock_quantity
        '
        Me.stock_quantity.DataPropertyName = "stock_quantity"
        Me.stock_quantity.HeaderText = "Quantity "
        Me.stock_quantity.Name = "stock_quantity"
        '
        'BATCH_NO
        '
        Me.BATCH_NO.DataPropertyName = "BATCH_NO"
        Me.BATCH_NO.HeaderText = "Batch Number"
        Me.BATCH_NO.Name = "BATCH_NO"
        '
        'STOR
        '
        Me.STOR.DataPropertyName = "STOR"
        Me.STOR.HeaderText = "Storage Location"
        Me.STOR.Name = "STOR"
        '
        'PLANT
        '
        Me.PLANT.DataPropertyName = "PLANT"
        Me.PLANT.HeaderText = "Plant"
        Me.PLANT.Name = "PLANT"
        '
        'Higher_Level
        '
        Me.Higher_Level.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Higher_Level.DataPropertyName = "Higher_Level"
        Me.Higher_Level.HeaderText = "Higher Level"
        Me.Higher_Level.Name = "Higher_Level"
        '
        'GroupProduct
        '
        Me.GroupProduct.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupProduct.Controls.Add(Me.Label35)
        Me.GroupProduct.Controls.Add(Me.Label62)
        Me.GroupProduct.Controls.Add(Me.Label49)
        Me.GroupProduct.Controls.Add(Me.Label56)
        Me.GroupProduct.Controls.Add(Me.CbProname)
        Me.GroupProduct.Controls.Add(Me.Meter)
        Me.GroupProduct.Controls.Add(Me.Label37)
        Me.GroupProduct.Controls.Add(Me.OrderBut)
        Me.GroupProduct.Controls.Add(Me.Order)
        Me.GroupProduct.Controls.Add(Me.Label36)
        Me.GroupProduct.Controls.Add(Me.Bay)
        Me.GroupProduct.Controls.Add(Me.Product)
        Me.GroupProduct.Controls.Add(Me.Label34)
        Me.GroupProduct.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupProduct.Location = New System.Drawing.Point(7, 8)
        Me.GroupProduct.Name = "GroupProduct"
        Me.GroupProduct.Size = New System.Drawing.Size(572, 79)
        Me.GroupProduct.TabIndex = 0
        Me.GroupProduct.TabStop = False
        Me.GroupProduct.Text = "Product"
        '
        'Label35
        '
        Me.Label35.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label35.Location = New System.Drawing.Point(328, 20)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(37, 16)
        Me.Label35.TabIndex = 177
        Me.Label35.Text = "Bay :"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label62.ForeColor = System.Drawing.Color.Red
        Me.Label62.Location = New System.Drawing.Point(273, 52)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(20, 25)
        Me.Label62.TabIndex = 2100
        Me.Label62.Text = "*"
        Me.Label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label49.Location = New System.Drawing.Point(303, 52)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(62, 16)
        Me.Label49.TabIndex = 185
        Me.Label49.Text = "KG / LTR."
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label56.Location = New System.Drawing.Point(273, 20)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(50, 16)
        Me.Label56.TabIndex = 184
        Me.Label56.Text = "Name :"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CbProname
        '
        Me.CbProname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CbProname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CbProname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CbProname.DataSource = Me.TProductBindingSource
        Me.CbProname.DisplayMember = "Product_name"
        Me.CbProname.DropDownWidth = 500
        Me.CbProname.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CbProname.FormattingEnabled = True
        Me.CbProname.Location = New System.Drawing.Point(338, 16)
        Me.CbProname.Name = "CbProname"
        Me.CbProname.Size = New System.Drawing.Size(0, 26)
        Me.CbProname.TabIndex = 183
        Me.CbProname.ValueMember = "Product_code"
        '
        'Meter
        '
        Me.Meter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Meter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Meter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Meter.DisplayMember = "ID"
        Me.Meter.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Meter.FormattingEnabled = True
        Me.Meter.Location = New System.Drawing.Point(496, 16)
        Me.Meter.Name = "Meter"
        Me.Meter.Size = New System.Drawing.Size(67, 26)
        Me.Meter.TabIndex = 2
        '
        'Label37
        '
        Me.Label37.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label37.Location = New System.Drawing.Point(432, 19)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(50, 16)
        Me.Label37.TabIndex = 182
        Me.Label37.Text = "Meter :"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'OrderBut
        '
        Me.OrderBut.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.OrderBut.Location = New System.Drawing.Point(384, 44)
        Me.OrderBut.Name = "OrderBut"
        Me.OrderBut.Size = New System.Drawing.Size(79, 30)
        Me.OrderBut.TabIndex = 4
        Me.OrderBut.Text = "Order"
        Me.OrderBut.UseVisualStyleBackColor = True
        '
        'Order
        '
        Me.Order.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Order.Location = New System.Drawing.Point(127, 46)
        Me.Order.Name = "Order"
        Me.Order.Size = New System.Drawing.Size(140, 26)
        Me.Order.TabIndex = 3
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label36.Location = New System.Drawing.Point(57, 49)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 16)
        Me.Label36.TabIndex = 179
        Me.Label36.Text = "Order Qtr :"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Bay
        '
        Me.Bay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Bay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.Bay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Bay.DisplayMember = "ID"
        Me.Bay.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Bay.FormattingEnabled = True
        Me.Bay.Location = New System.Drawing.Point(374, 16)
        Me.Bay.Name = "Bay"
        Me.Bay.Size = New System.Drawing.Size(50, 26)
        Me.Bay.TabIndex = 1
        '
        'Product
        '
        Me.Product.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.Product.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Product.DataSource = Me.TProductBindingSource
        Me.Product.DisplayMember = "Product_code"
        Me.Product.DropDownWidth = 500
        Me.Product.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Product.FormattingEnabled = True
        Me.Product.Location = New System.Drawing.Point(127, 16)
        Me.Product.Name = "Product"
        Me.Product.Size = New System.Drawing.Size(140, 26)
        Me.Product.TabIndex = 0
        Me.Product.ValueMember = "Product_code"
        '
        'Label34
        '
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label34.Location = New System.Drawing.Point(15, 18)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(114, 20)
        Me.Label34.TabIndex = 174
        Me.Label34.Text = "Product Code :"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label28.Location = New System.Drawing.Point(6, 478)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(62, 16)
        Me.Label28.TabIndex = 145
        Me.Label28.Text = "Remark :"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Edremark
        '
        Me.Edremark.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Edremark.BackColor = System.Drawing.SystemColors.Control
        Me.Edremark.Enabled = False
        Me.Edremark.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.Edremark.Location = New System.Drawing.Point(74, 472)
        Me.Edremark.Name = "Edremark"
        Me.Edremark.Size = New System.Drawing.Size(243, 27)
        Me.Edremark.TabIndex = 2
        Me.Edremark.Text = ""
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(184, 26)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(183, 22)
        Me.ToolStripMenuItem1.Text = "ToolStripMenuItem1"
        '
        'T_TRUCKTableAdapter
        '
        Me.T_TRUCKTableAdapter.ClearBeforeFill = True
        '
        'T_COMPANYTableAdapter
        '
        Me.T_COMPANYTableAdapter.ClearBeforeFill = True
        '
        'T_DRIVERTableAdapter
        '
        Me.T_DRIVERTableAdapter.ClearBeforeFill = True
        '
        'V_LOADINGNOTETableAdapter
        '
        Me.V_LOADINGNOTETableAdapter.ClearBeforeFill = True
        '
        'T_SHIPPERTableAdapter
        '
        Me.T_SHIPPERTableAdapter.ClearBeforeFill = True
        '
        'T_CustomerTableAdapter
        '
        Me.T_CustomerTableAdapter.ClearBeforeFill = True
        '
        'T_SHIPMENT_TYPETableAdapter
        '
        Me.T_SHIPMENT_TYPETableAdapter.ClearBeforeFill = True
        '
        'T_STATUSTableAdapter
        '
        Me.T_STATUSTableAdapter.ClearBeforeFill = True
        '
        'T_PACKINGTableAdapter
        '
        Me.T_PACKINGTableAdapter.ClearBeforeFill = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "ID"
        Me.Column2.HeaderText = "Column2"
        Me.Column2.Name = "Column2"
        '
        'T_ProductTableAdapter
        '
        Me.T_ProductTableAdapter.ClearBeforeFill = True
        '
        'V_CARDFREETableAdapter
        '
        Me.V_CARDFREETableAdapter.ClearBeforeFill = True
        '
        'T_TANKFARMTableAdapter
        '
        Me.T_TANKFARMTableAdapter.ClearBeforeFill = True
        '
        'VCARDBindingSource
        '
        Me.VCARDBindingSource.DataMember = "V_CARD"
        Me.VCARDBindingSource.DataSource = Me.FPTDataSet
        '
        'V_CARDTableAdapter
        '
        Me.V_CARDTableAdapter.ClearBeforeFill = True
        '
        'CustomShape1
        '
        Me.CustomShape1.Dimension = New System.Drawing.Rectangle(0, 0, 0, 0)
        '
        'TUNITBindingSource
        '
        Me.TUNITBindingSource.DataMember = "TUNIT"
        Me.TUNITBindingSource.DataSource = Me.FPTDataSet
        '
        'TUNITTableAdapter
        '
        Me.TUNITTableAdapter.ClearBeforeFill = True
        '
        'DateFormatBindingSource
        '
        Me.DateFormatBindingSource.DataMember = "DateFormat"
        Me.DateFormatBindingSource.DataSource = Me.FPTDataSet
        '
        'DateFormatTableAdapter
        '
        Me.DateFormatTableAdapter.ClearBeforeFill = True
        '
        'T_LOTableAdapter
        '
        Me.T_LOTableAdapter.ClearBeforeFill = True
        '
        'TSETTINGBindingSource
        '
        Me.TSETTINGBindingSource.DataMember = "T_SETTING"
        Me.TSETTINGBindingSource.DataSource = Me.FPTDataSet
        '
        'T_SETTINGTableAdapter
        '
        Me.T_SETTINGTableAdapter.ClearBeforeFill = True
        '
        'RoundRectShape1
        '
        Me.RoundRectShape1.Radius = 50
        '
        'TSETTINGPLANTTableAdapter
        '
        Me.TSETTINGPLANTTableAdapter.ClearBeforeFill = True
        '
        'TSETTINGPLANTBindingSource
        '
        Me.TSETTINGPLANTBindingSource.DataMember = "TSETTINGPLANT"
        Me.TSETTINGPLANTBindingSource.DataSource = Me.FPTDataSet
        '
        'f05_02_Advisenote2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.Bcancel
        Me.ClientSize = New System.Drawing.Size(1370, 750)
        Me.Controls.Add(Me.GroupAdnote)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "f05_02_Advisenote2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "18"
        Me.Text = "Advisenote other station"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupAdnote.ResumeLayout(False)
        Me.GroupGeneral.ResumeLayout(False)
        Me.GroupGeneral.PerformLayout()
        CType(Me.CBLoadingPoint, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TLOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TDRIVERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TTRUCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TTANKFARMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VCARDFREEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        CType(Me.TSHIPMENTTYPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.TCOMPANYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TPACKINGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSTATUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSHIPPERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupUnload.ResumeLayout(False)
        Me.GroupUnload.PerformLayout()
        CType(Me.BindingNavigator2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator2.ResumeLayout(False)
        Me.BindingNavigator2.PerformLayout()
        CType(Me.V_LoadingnoteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBt.ResumeLayout(False)
        Me.GroupBt.PerformLayout()
        Me.GroupBtadd.ResumeLayout(False)
        Me.GroupPort.ResumeLayout(False)
        CType(Me.DBGridAddnote, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PTruck.ResumeLayout(False)
        Me.PTruck.PerformLayout()
        CType(Me.Imagetruck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        CType(Me.BTG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BTR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InGrean, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GDetail.ResumeLayout(False)
        Me.GDetail.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GPreset.ResumeLayout(False)
        Me.GPreset.PerformLayout()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBatchtable.ResumeLayout(False)
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.TBATCHLOSTTASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MeterGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupProduct.ResumeLayout(False)
        Me.GroupProduct.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.VCARDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TUNITBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFormatBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSETTINGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TSETTINGPLANTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend TTRUCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_TRUCKTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_TRUCKTableAdapter
    Friend WithEvents TCOMPANYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_COMPANYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_COMPANYTableAdapter
    Friend WithEvents TDRIVERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupAdnote As System.Windows.Forms.GroupBox
    Friend WithEvents GDetail As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents GPreset As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents GProduct As System.Windows.Forms.GroupBox
    Friend WithEvents GroupGeneral As System.Windows.Forms.GroupBox
    Friend WithEvents Edremark As System.Windows.Forms.RichTextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents UpdateWeightIn As System.Windows.Forms.TextBox
    Friend WithEvents LawWeightIn As System.Windows.Forms.TextBox
    Friend WithEvents EDPreset As System.Windows.Forms.TextBox
    Friend WithEvents EDDONo As System.Windows.Forms.TextBox
    Friend WithEvents EDREf As System.Windows.Forms.TextBox
    Friend WithEvents EDLoaddate As System.Windows.Forms.TextBox
    Friend WithEvents EDShipper As System.Windows.Forms.ComboBox
    Friend WithEvents EDForword As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents EDTruckCapa As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Coa As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Container As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Dateedit As System.Windows.Forms.DateTimePicker
    Friend WithEvents T_DRIVERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_DRIVERTableAdapter
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GiDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Seal_No As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PRESETNO1 As System.Windows.Forms.TextBox
    Friend WithEvents DBGridAddnote As System.Windows.Forms.DataGridView
    Friend WithEvents V_LOADINGNOTETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_LOADINGNOTETableAdapter
    Friend WithEvents UpdateWeightOut As System.Windows.Forms.TextBox
    Friend WithEvents LawWeightout As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ShipmentNo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents AccessCode As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents editweightin As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents EDCustomer As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Packing As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Quantity As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents GroupProduct As System.Windows.Forms.GroupBox
    Friend WithEvents Product As System.Windows.Forms.ComboBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Bay As System.Windows.Forms.ComboBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Order As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents OrderBut As System.Windows.Forms.Button
    Friend WithEvents Meter As System.Windows.Forms.ComboBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents SealCount As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents editweightout As System.Windows.Forms.Button
    Friend WithEvents PackingWeight As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Calculate As System.Windows.Forms.TextBox
    Friend WithEvents Tank As System.Windows.Forms.TextBox
    Friend WithEvents Density As System.Windows.Forms.TextBox
    Friend WithEvents Temp As System.Windows.Forms.TextBox
    Friend WithEvents MeterRead As System.Windows.Forms.TextBox
    Friend WithEvents WeightTotal As System.Windows.Forms.TextBox
    Friend WithEvents WeightScal As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents V_LoadingnoteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LOADVEHICLEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDRIVERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADCUSTOMERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDOfullDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCHNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddnoteDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents weightoutprint As System.Windows.Forms.Button
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents weightouttime As System.Windows.Forms.TextBox
    Friend WithEvents Weightintime As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents ShipMent_Type As System.Windows.Forms.ComboBox
    Friend WithEvents TSHIPPERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPPERTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPPERTableAdapter
    Friend WithEvents TCustomerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_CustomerTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_CustomerTableAdapter
    Friend WithEvents TSHIPMENTTYPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SHIPMENT_TYPETableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SHIPMENT_TYPETableAdapter
    Friend WithEvents TSTATUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_STATUSTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_STATUSTableAdapter
    Friend WithEvents TPACKINGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_PACKINGTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_PACKINGTableAdapter
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Company_sapcode As System.Windows.Forms.ComboBox
    Friend WithEvents P_Weight As System.Windows.Forms.ComboBox
    Friend WithEvents PackingId As System.Windows.Forms.ComboBox
    Friend WithEvents Shipment_id As System.Windows.Forms.ComboBox
    Friend WithEvents EDLoadDTime As System.Windows.Forms.TextBox
    Friend WithEvents Updatedate As System.Windows.Forms.TextBox
    Friend WithEvents EDLoadID As System.Windows.Forms.TextBox
    Friend WithEvents TruckId As System.Windows.Forms.ComboBox
    Friend WithEvents Prono As System.Windows.Forms.ComboBox
    Friend WithEvents ProductId As System.Windows.Forms.ComboBox
    Friend WithEvents StatusId As System.Windows.Forms.ComboBox
    Friend WithEvents TruckCompanyId As System.Windows.Forms.ComboBox
    Friend WithEvents DriverID As System.Windows.Forms.ComboBox
    Friend WithEvents CustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents ShipperId As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DateeditTemp As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBt As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBtadd As System.Windows.Forms.GroupBox
    Friend WithEvents PrintBut As System.Windows.Forms.Button
    Friend WithEvents EditData As System.Windows.Forms.Button
    Friend WithEvents Adddata As System.Windows.Forms.Button
    Friend WithEvents GroupPort As System.Windows.Forms.GroupBox
    Friend WithEvents BTEDIT_SAP As System.Windows.Forms.Button
    Friend WithEvents WeightOut As System.Windows.Forms.Button
    Friend WithEvents WeightIn As System.Windows.Forms.Button
    Friend WithEvents Bcancel As System.Windows.Forms.Button
    Friend WithEvents BTIMPORT_SAP As System.Windows.Forms.Button
    Friend WithEvents Edit As System.Windows.Forms.Button
    Friend WithEvents Bsave As System.Windows.Forms.Button
    Friend WithEvents GroupBatchtable As System.Windows.Forms.GroupBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents Btadd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BtDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btfirst As System.Windows.Forms.ToolStripButton
    Friend WithEvents Btprevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Btnext As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtLast As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MeterGrid As System.Windows.Forms.DataGridView
    Friend WithEvents TBATCHLOSTTASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioLiquid As System.Windows.Forms.RadioButton
    Friend WithEvents RadioBig As System.Windows.Forms.RadioButton
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents SelectDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents BindingNavigator2 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EDFillter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Sono As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Soitem As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupUnload As System.Windows.Forms.GroupBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents EDDO_item As System.Windows.Forms.TextBox
    Friend WithEvents BtUnload As System.Windows.Forms.Button
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents CbProname As System.Windows.Forms.ComboBox
    Friend WithEvents TProductBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ProductTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_ProductTableAdapter
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents EDSale_Unit As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents EDSaleQtr As System.Windows.Forms.TextBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents CBCardNO As System.Windows.Forms.ComboBox
    Friend WithEvents VCARDFREEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_CARDFREETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_CARDFREETableAdapter
    Friend WithEvents BtVehicleEdit As System.Windows.Forms.Button
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents CBTankfarm As System.Windows.Forms.ComboBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents TTANKFARMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_TANKFARMTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_TANKFARMTableAdapter
    Friend WithEvents PTruck As System.Windows.Forms.Panel
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape6 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents InGrean As System.Windows.Forms.PictureBox
    Friend WithEvents BTDownload As System.Windows.Forms.Button
    Friend WithEvents BtWorkPrint As System.Windows.Forms.Button
    Friend WithEvents VCARDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_CARDTableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_CARDTableAdapter
    Friend WithEvents EDDriver As System.Windows.Forms.ComboBox
    Friend WithEvents EDTruck As System.Windows.Forms.ComboBox
    Friend WithEvents Edremark2 As System.Windows.Forms.RichTextBox
    Friend WithEvents RadPanel2 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents TrackBarUThumbShape1 As Telerik.WinControls.UI.TrackBarUThumbShape
    Friend WithEvents RadPanel3 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadPanel4 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadPanel5 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadPanel6 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadPanel1 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents MediaShape1 As Telerik.WinControls.Tests.MediaShape
    Friend WithEvents DonutShape1 As Telerik.WinControls.Tests.DonutShape
    Friend WithEvents CustomShape1 As Telerik.WinControls.OldShapeEditor.CustomShape
    Friend WithEvents TrackBarDThumbShape1 As Telerik.WinControls.UI.TrackBarDThumbShape
    Friend WithEvents TrackBarLThumbShape1 As Telerik.WinControls.UI.TrackBarLThumbShape
    Friend WithEvents TrackBarRThumbShape1 As Telerik.WinControls.UI.TrackBarRThumbShape
    Friend WithEvents TUNITBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TUNITTableAdapter As TAS_TOL.FPTDataSetTableAdapters.TUNITTableAdapter
    Friend WithEvents DateFormatBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DateFormatTableAdapter As TAS_TOL.FPTDataSetTableAdapters.DateFormatTableAdapter
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TLOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_LOTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_LOTableAdapter
    Friend WithEvents TSETTINGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SETTINGTableAdapter As TAS_TOL.FPTDataSetTableAdapters.T_SETTINGTableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents CBLoadingPoint As Telerik.WinControls.UI.RadMultiColumnComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents QaShape1 As Telerik.WinControls.Tests.QAShape
    Friend WithEvents RoundRectShape1 As Telerik.WinControls.RoundRectShape
    Friend WithEvents BTR As Telerik.WinControls.UI.RadButton
    Friend WithEvents InG As Telerik.WinControls.UI.RadPanel
    Friend WithEvents InR As Telerik.WinControls.UI.RadPanel
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents BTG As Telerik.WinControls.UI.RadButton
    Friend WithEvents EllipseShape1 As Telerik.WinControls.EllipseShape
    Friend WithEvents BreezeTheme1 As Telerik.WinControls.Themes.BreezeTheme
    Friend WithEvents Imagetruck As System.Windows.Forms.PictureBox
    Friend WithEvents LOAD_DOfull As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_DID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOADDATEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Do_status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Customer_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Product_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_VEHICLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_DRIVER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCPRESETDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LCBAYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SP_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOAD_TANK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents do_postdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReferenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DO_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DO_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MAT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stock_quantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCH_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STOR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PLANT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Higher_Level As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TSETTINGPLANTTableAdapter As TAS_TOL.FPTDataSetTableAdapters.TSETTINGPLANTTableAdapter
    Friend WithEvents TSETTINGPLANTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton

End Class
