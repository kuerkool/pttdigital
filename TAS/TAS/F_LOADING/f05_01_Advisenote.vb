﻿Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Threading
Imports TAS_TOL.FSAP02_2
'Imports TAS_TOL.SAP02
'Imports TAS_TOL.SAP03
Imports ExtendedErrorProvider
Imports System.Drawing.Printing
Imports System.ComponentModel
Imports System.Reflection
Imports System.IO
Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
Imports TAS_TOL.C01_Permission
Imports Microsoft.Office.Interop

Public Class f05_01_Advisenote
    Structure YME_DATA
        Public LOAD_TEMP As Single
        Public LOAD_WEIGHT As Single
        Public LOAD_VOULME As Single
        Public LOAD_DENSITY As Single
    End Structure
#Region "Private Variable"
    Public Const MOD_ALT As Integer = &H1 'Either ALT key must be held down.
    Public Const MOD_CONTROL As Integer = &H2 'Either CTRL key must be held down.
    Public Const MOD_NOREPEAT As Integer = &H4000 'Changes the hotkey behavior so that the keyboard auto-repeat does not yield multiple hotkey notifications. 
    Public Const MOD_SHIFT As Integer = &H4 'Either SHIFT key must be held down.
    Public Const MOD_WIN As Integer = &H8 'Either WINDOWS key was held down. These keys are labeled with the Windows logo. Keyboard shortcuts that involve the WINDOWS key are reserved for use by the operating system.
    Public Const WM_HOTKEY As Integer = &H312

    <DllImport("User32.dll")> _
    Public Shared Function RegisterHotKey(ByVal hwnd As IntPtr, _
                        ByVal id As Integer, ByVal fsModifiers As Integer, _
                        ByVal vk As Integer) As Integer
    End Function

    <DllImport("User32.dll")> _
    Public Shared Function UnregisterHotKey(ByVal hwnd As IntPtr, _
                        ByVal id As Integer) As Integer
    End Function
    '  Private conn As SqlClient.SqlConnection = GetConnection()
    Private weightIndate As Date
    Private weightOutdate As Date
    Private eBay As Integer = 0
    Private zDoNotDoEvent As Boolean = False
    Public AddNew As Integer
    Private da As New SqlClient.SqlDataAdapter
    Private ds As DataSet
    Private dt As DataTable
    Private ProductNo() As ComboBox
    Private CapacityNo() As TextBox
    Private PRESETNO() As TextBox
    Private COMPNO() As TextBox
    Private BayNo() As ComboBox
    Private Meterno() As ComboBox
    Private test() As Array
    Private TRUCK_COMP_NUM, sum As Integer
    Public Shared loginId As Integer = 0
    Public Shared LoginDo As Integer = 0
    Private Adnote_DO As DO_ITEM
    Private Adnote_ALLDO() As DO_ITEM
    Private Adnote_ALLBATCH() As BATCH_ITEM
    Public load_id As Integer
    Private MyErrorProvider As New ErrorProviderExtended
    Private SelectVLoadingNotecase As Int16
    Friend WithEvents ThrLIGHTCONTROL As BackgroundWorker
    Private CallDataBindToDataGrid As New MethodInvoker(AddressOf Me.DataBindToDataGrid)
    Private DsLight As New DataSet
    Private DtLight As DataTable
    Private PlantFilter, StatusFilter As String
    Private BATCH_INDICATOR As String
    Declare Function LockWindowUpdate Lib "user32" _
(ByVal hWnd As Long) As Long
    Dim UpdateThread As Thread
    Dim UpdateThreadStart As New ThreadStart(AddressOf QueryDataBase)
    Dim CallDataBindToData As New MethodInvoker(AddressOf Me.DataBindToData)

    Public Sub DataBindToData()
        Try
            zDoNotDoEvent = True
            'TSETTINGBindingSource.ResetBindings(False)
            ''TLOBindingSource.ResetBindings(False)
            'TUNITBindingSource.ResetBindings(False)
            'DateFormatBindingSource.ResetBindings(False)
            'TTANKFARMBindingSource.ResetBindings(False)
            'VCARDBindingSource.ResetBindings(False)
            'VCARDFREEBindingSource.ResetBindings(False)
            'TPACKINGBindingSource.ResetBindings(False)
            'TProductBindingSource.ResetBindings(False)
            'TSHIPMENTTYPEBindingSource.ResetBindings(False)
            'TSHIPPERBindingSource.ResetBindings(False)
            'TCustomerBindingSource.ResetBindings(False)
            'TCOMPANYBindingSource.ResetBindings(False)
            'TDRIVERBindingSource.ResetBindings(False)
            'TTRUCKBindingSource.ResetBindings(False)
            'TSTATUSBindingSource.ResetBindings(False)
            'TProductBindingSource.ResetBindings(False)



            'Add controls one by one in error provider.
            MyErrorProvider.Controls.Clear()
            MyErrorProvider.Controls.Add(EDTruck, "Vehicle No")
            MyErrorProvider.ClearAllErrorMessages()
            MyErrorProvider.SummaryMessage = "Following fields are mandatory,"


            ' StatusFilter = " (Do_status=0) and (load_status in(1,2,3)) "
            StatusFilter = " (((LOAD_DOFULL<>'') and (Do_status=0) and (load_status in(1,2,3))) or  ((LOAD_DOFULL='') and (load_status in(1,2)))) "
            PlantFilter = " (PLANT='" & TSETTINGBindingSource(0)("PLANT") & "' or PLANT='') "
            V_LoadingnoteBindingSource.Filter = PlantFilter & " and " & StatusFilter
            Try

                TLOBindingSource.Position = TLOBindingSource.Find("PLANT_LOCATION", TSETTINGBindingSource(0)("PLANT").ToString)

            Catch ex As Exception

            End Try

            GroupPort.Enabled = False
            GroupBtadd.Visible = True
            GroupBtadd.Enabled = True
            GDetail.Enabled = False
            SelectDate.Value = Date.Now.Date
            ClearData()
            zDoNotDoEvent = False

        Catch ex As Exception

        End Try
    End Sub

    Public Sub QueryDataBase()
        Try
            zDoNotDoEvent = True
            'TODO: This line of code loads data into the 'FPTDataSet.T_SETTING' table. You can move, or remove it, as needed.
            Me.T_SETTINGTableAdapter.Fill(Me.FPTDataSet.T_SETTING)
            'TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
            ' Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
            'TODO: This line of code loads data into the 'FPTDataSet.DateFormat' table. You can move, or remove it, as needed.
            Me.DateFormatTableAdapter.Fill(Me.FPTDataSet.DateFormat)
            'TODO: This line of code loads data into the 'FPTDataSet.TUNIT' table. You can move, or remove it, as needed.
            Me.TUNITTableAdapter.Fill(Me.FPTDataSet.TUNIT)
            'TODO: This line of code loads data into the 'FPTDataSet.V_CARD' table. You can move, or remove it, as needed.
            'TODO: This line of code loads data into the 'FPTDataSet.T_TANKFARM' table. You can move, or remove it, as needed.
            Me.T_TANKFARMTableAdapter.Fill(Me.FPTDataSet.T_TANKFARM)
            'TODO: This line of code loads data into the 'FPTDataSet.V_CARDFREE' table. You can move, or remove it, as needed.
            Me.V_CARDFREETableAdapter.Fill(Me.FPTDataSet.V_CARDFREE)

            Me.V_CARDTableAdapter.Fill(Me.FPTDataSet.V_CARD)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
            Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
            '  TProductBindingSource.Filter = "product_number<>''"
            'TODO: This line of code loads data into the 'FPTDataSet.T_PACKING' table. You can move, or remove it, as needed.
            Me.T_PACKINGTableAdapter.Fill(Me.FPTDataSet.T_PACKING)
            'TODO: This line of code loads data into the 'FPTDataSet.T_STATUS' table. You can move, or remove it, as needed.
            Me.T_STATUSTableAdapter.Fill(Me.FPTDataSet.T_STATUS)
            'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPMENT_TYPE' table. You can move, or remove it, as needed.
            Me.T_SHIPMENT_TYPETableAdapter.Fill(Me.FPTDataSet.T_SHIPMENT_TYPE)
            'TODO: This line of code loads data into the 'FPTDataSet.T_Customer' table. You can move, or remove it, as needed.
            Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
            ' TCustomerBindingSource.Filter = "SORG_CODE<>''"
            'TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
            Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
            'Me.FPTDataSet.V_LOADINGNOTE
            'TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
            Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
            '  TCOMPANYBindingSource.Filter = "company_number<>''"
            'TODO: This line of code loads data into the 'FPTDataSet.T_TRUCK' table. You can move, or remove it, as needed.
            Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
            Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)


            Me.ResumeLayout()

            DataBindToData()
            '  Me.BeginInvoke(CallDataBindToData)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Public Sub LockWindow(ByVal hWnd As Long, ByVal blnValue As Boolean)
        If blnValue Then
            LockWindowUpdate(hWnd)
        Else
            LockWindowUpdate(0)
        End If
    End Sub
#End Region
#Region "BARCODE"
    Function GENCODE1D(ByVal _LOAD_DATE As Date, ByVal LOAD_DID As Integer) As Image
        Try
            Dim Barcode As String
            Barcode = _LOAD_DATE.ToString("yyyyMMdd") & LOAD_DID.ToString("000")
            'Dim a As Graphics = PicBarCode.CreateGraphics
            'a.DrawString(TextBox1.Text, Me.Font, Brushes.Black, 100, 5)
            Return Code128(Barcode, "A")
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

#Region "LIGHT CONTROL"
    Private Sub RANThrLIGHTCONTROL()
        ThrLIGHTCONTROL = New BackgroundWorker
        ThrLIGHTCONTROL.WorkerSupportsCancellation = True
        AddHandler ThrLIGHTCONTROL.DoWork, AddressOf AsyncLIGHTCONTROL
        ThrLIGHTCONTROL.RunWorkerAsync()
    End Sub
    Private Sub AsyncLIGHTCONTROL(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        Dim SqlCon As New SqlClient.SqlConnection
        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        SqlCon.Open()
        Dim q As String
        'Dim ds As New DataSet
        Dim i As Integer
        Dim da As New SqlClient.SqlDataAdapter


        Do

            If ThrLIGHTCONTROL.CancellationPending = True Then
                e.Cancel = True
                Exit Do
            Else

                q = "Select * from T_LIGHTCONTROL order by TAG_VAL Desc,CODE"
                da.SelectCommand = New SqlClient.SqlCommand(q, SqlCon)
                Try
                    DsLight.Dispose()
                Catch ex As Exception
                End Try
                '  DsLight = New DataSet
                DtLight = New DataTable
                da.Fill(DtLight)
                ' da.Fill(DsLight, "T_LIGHTCONTROL")







                '' Perform a time consuming operation and report progress.
                'item = ("PLC.GE.WEIGHT.IN_LIGHT_ON")

                'Myrp = ReadOPC(item)
                'If Myrp.Quality = "GOOD" Then
                '    Try
                '        q = "Update T_WEIGHT set W_WEIGHT=" + Trim(Myrp.Value) + ", W_DATETIME  = getdate()"
                '        da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
                '        da.UpdateCommand.ExecuteNonQuery()
                '    Catch ex As Exception
                '    End Try
                'End If

            End If
            Me.BeginInvoke(CallDataBindToDataGrid)
            Threading.Thread.Sleep(1000)

        Loop While True



    End Sub

    Private Sub DataBindToDataGrid()
        Dim i
        Try

            For i = 0 To DtLight.Rows.Count - 1
                Select Case DtLight.Rows(i)("CODE").ToString()
                    Case "LIGHTIN"

                        If DtLight.Rows(i)("TAG_VAL").ToString = 1 Then
                            InR.Visible = False
                            InO.Visible = False
                            InG.Visible = True

                            outR.Visible = True
                            outO.Visible = False
                            outG.Visible = False
                            Exit For
                            '   BtINGrean.Text = "Auto Delay"
                        Else
                            InR.Visible = True
                            InO.Visible = False
                            InG.Visible = False

                            'outR.Visible = False
                            'outO.Visible = False
                            'outG.Visible = True
                            '  BtINGrean.Text = "GO."
                        End If
                    Case "LIGHTOUT"

                        If DtLight.Rows(i)("TAG_VAL").ToString = 1 Then
                            InR.Visible = True
                            InO.Visible = False
                            InG.Visible = False

                            outR.Visible = False
                            outO.Visible = False
                            outG.Visible = True
                            Exit For
                            '   BtINGrean.Text = "Auto Delay"
                        Else
                            'InR.Visible = False
                            'InO.Visible = False
                            'InG.Visible = False

                            outR.Visible = True
                            outO.Visible = False
                            outG.Visible = False
                            '  BtINGrean.Text = "GO."
                        End If


                End Select
            Next

        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Private And function"
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_HOTKEY Then
            Dim id As IntPtr = m.WParam
            Select Case (id.ToString)
                Case "100" 'Add
                    If Adddata.Enabled = True And GroupBtadd.Enabled Then
                        Adddata.Focus()
                        Adddata.PerformClick()
                    End If

                Case "101" 'Edit
                    If EditData.Enabled = True And GroupBtadd.Enabled Then
                        EditData.Focus()
                        EditData.PerformClick()
                    End If
                Case "102" 'Print
                    If weightoutprint.Enabled = True And GroupBtadd.Enabled Then
                        weightoutprint.Focus()
                        weightoutprint.PerformClick()
                    End If
                Case "200" 'Sap
                    If BTIMPORT_SAP.Enabled = True And GroupGeneral.Enabled And BTIMPORT_SAP.Visible Then
                        BTIMPORT_SAP.Focus()
                        BTIMPORT_SAP.PerformClick()
                    End If
                Case "201" 'w int
                    If WeightIn.Enabled = True And GroupPort.Enabled And WeightIn.Visible Then
                        WeightIn.Focus()
                        WeightIn.PerformClick()
                    End If
                Case "202" 'w out
                    If WeightOut.Enabled = True And GroupPort.Enabled And WeightOut.Visible Then
                        WeightOut.Focus()
                        WeightOut.PerformClick()
                    End If
                Case "203" 'Save
                    If Bsave.Enabled = True And GroupPort.Enabled And Bsave.Visible Then
                        Bsave.Focus()
                        Bsave.Focus()
                        Bsave.PerformClick()
                    End If
                Case "204" 'Update
                    If Edit.Enabled = True And GroupPort.Enabled And Edit.Visible Then
                        Edit.Focus()
                        Edit.PerformClick()
                    End If
                Case "205" 'Update
                    If BTEDIT_SAP.Enabled = True And GroupPort.Enabled And BTEDIT_SAP.Visible Then
                        BTEDIT_SAP.Focus()
                        BTEDIT_SAP.PerformClick()
                    End If
            End Select
        End If
        MyBase.WndProc(m)
    End Sub

    Private Sub SelectVLoadingNote()
        Dim q As String
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try
            conn.Open()
            q = "SELECT     max(LOAD_CAPACITY) as LOAD_CAPACITY" _
    & " , max(LOAD_PRESET) as LOAD_PRESET, max(LOAD_CARD) as LOAD_CARD," _
    & " max(LC_COMPARTMENT) as LC_COMPARTMENT," _
    & " max(LC_BAY) as LC_BAY,           Reference, max(LOAD_DATE) as LOAD_DATE" _
    & " ,max(LOAD_DID) as LOAD_DID ,max(SP_Code) as SP_Code ,max(LOAD_TANK) as LOAD_TANK ,max(STATUS_NAME) as STATUS_NAME ,max(LOAD_DOfull) as LOAD_DOfull ,max(LOAD_VEHICLE) as LOAD_VEHICLE ,max(Product_name) as Product_name ,max(Customer_name) as Customer_name ,max(LOAD_DRIVER) as LOAD_DRIVER,max(load_id) as Load_id,max(DO_STATUS) as DO_STATUS,max(do_POSTDATE) as DO_POSTDATE," _
    & " max(Loaddate) as Loaddate" _
    & " ,max(dO_ITEM) as dO_ITEM,max(pLANT) as pLANT,max(sTORAGE_LOC) as sTORAGE_LOC, max(Raw_Weight_in) as Raw_Weight_in , max(Raw_Weight_out) as Raw_Weight_out ,max(Container) as Container , Max(Container_Tare) as Container_Tare" _
    & " ,max(WeightIN) as WeightIN,max(WeightOut) as WeightOut,max(WeightTotal) as WeightTotal, max(LC_SEAL) as LC_SEAL , Max(LOAD_STATUS) as LOAD_STATUS, Max(cUST_PO_NUMBER) as cUST_PO_NUMBER ,MAX(SO_NO) as SO_NO , Max(BADGE_NO) as BADGE_NO" _
    & " FROM V_LOADINGNOTE " _
    & " where Loaddate='" + String.Format("{0:yyyy-M-d}", SelectDate.Value) + "'" _
    & " or STATUS_NAME='" & "Waiting" & "'" _
    & " group by reference" _
    & " order by load_date desc,Reference  "
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim MyDataSet As New DataSet
            da.Fill(MyDataSet, "V_LOADINGNOTE")
            V_LoadingnoteBindingSource.DataSource = MyDataSet
            V_LoadingnoteBindingSource.DataMember = "V_LOADINGNOTE"
            da.Dispose()
            conn.Close()
            conn.Dispose()
        Catch ex As Exception
            conn.Close()
        End Try
    End Sub

    Private Sub SelectVLoadingNoteAllWaiting()
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try
            conn.Open()
            Dim q As String
            q = "SELECT     max(LOAD_CAPACITY) as LOAD_CAPACITY" _
    & " , max(LOAD_PRESET) as LOAD_PRESET, max(LOAD_CARD) as LOAD_CARD," _
    & " max(LC_COMPARTMENT) as LC_COMPARTMENT," _
    & " max(LC_BAY) as LC_BAY,           Reference, max(LOAD_DATE) as LOAD_DATE" _
    & " ,max(LOAD_DID) as LOAD_DID ,max(SP_Code) as SP_Code ,max(LOAD_TANK) as LOAD_TANK ,max(STATUS_NAME) as STATUS_NAME ,max(LOAD_DOfull) as LOAD_DOfull ,max(LOAD_VEHICLE) as LOAD_VEHICLE ,max(Product_name) as Product_name ,max(Customer_name) as Customer_name ,max(LOAD_DRIVER) as LOAD_DRIVER,max(load_id) as Load_id,max(DO_STATUS) as DO_STATUS,max(do_POSTDATE) as DO_POSTDATE," _
    & " max(Loaddate) as Loaddate" _
    & " ,max(dO_ITEM) as dO_ITEM,max(pLANT) as pLANT,max(sTORAGE_LOC) as sTORAGE_LOC, max(Raw_Weight_in) as Raw_Weight_in , max(Raw_Weight_out) as Raw_Weight_out ,max(Container) as Container, Max(Container_Tare) as Container_Tare" _
    & " ,max(WeightIN) as WeightIN,max(WeightOut) as WeightOut,max(WeightTotal) as WeightTotal, max(LC_SEAL) as LC_SEAL , Max(LOAD_STATUS) as LOAD_STATUS , Max(cUST_PO_NUMBER) as cUST_PO_NUMBER,MAX(SO_NO) as SO_NO, Max(BADGE_NO) as BADGE_NO" _
    & " FROM V_LOADINGNOTE " _
    & " group by reference" _
    & " order by load_date desc,Reference  "
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim MyDataSet As New DataSet
            da.Fill(MyDataSet, "V_LOADINGNOTE")
            V_LoadingnoteBindingSource.DataSource = MyDataSet
            V_LoadingnoteBindingSource.DataMember = "V_LOADINGNOTE"

            da.Dispose()
            conn.Close()
            conn.Dispose()
        Catch ex As Exception
            conn.Close()
        End Try

    End Sub

    Public Function RemoveElementFromArray(ByRef objArray As System.Array, ByVal IndexElement As Integer, ByVal objType As System.Type)
        Dim objArrayList As New ArrayList(objArray)
        objArrayList.RemoveAt(IndexElement)
        Return objArrayList.ToArray(objType)
    End Function

    Private Sub Advisenote_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.T_BADGE' table. You can move, or remove it, as needed.
        Me.T_BADGETableAdapter.Fill(Me.FPTDataSet.T_BADGE)
        ' Me.SuspendLayout()
        zDoNotDoEvent = False

        Try
            'UpdateThread = New Thread(UpdateThreadStart)
            'UpdateThread.IsBackground = True
            'UpdateThread.Start()
            RANThrLIGHTCONTROL()
            Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
            QueryDataBase()
            ''TODO: This line of code loads data into the 'FPTDataSet.T_SETTING' table. You can move, or remove it, as needed.
            'Me.T_SETTINGTableAdapter.Fill(Me.FPTDataSet.T_SETTING)
            ''TODO: This line of code loads data into the 'FPTDataSet.T_LO' table. You can move, or remove it, as needed.
            'Me.T_LOTableAdapter.Fill(Me.FPTDataSet.T_LO)
            ''TODO: This line of code loads data into the 'FPTDataSet.DateFormat' table. You can move, or remove it, as needed.
            'Me.DateFormatTableAdapter.Fill(Me.FPTDataSet.DateFormat)
            ''TODO: This line of code loads data into the 'FPTDataSet.TUNIT' table. You can move, or remove it, as needed.
            'Me.TUNITTableAdapter.Fill(Me.FPTDataSet.TUNIT)
            ''TODO: This line of code loads data into the 'FPTDataSet.V_CARD' table. You can move, or remove it, as needed.
            ''TODO: This line of code loads data into the 'FPTDataSet.T_TANKFARM' table. You can move, or remove it, as needed.
            'Me.T_TANKFARMTableAdapter.Fill(Me.FPTDataSet.T_TANKFARM)
            ''TODO: This line of code loads data into the 'FPTDataSet.V_CARDFREE' table. You can move, or remove it, as needed.
            'Me.V_CARDFREETableAdapter.Fill(Me.FPTDataSet.V_CARDFREE)
            'Me.V_CARDTableAdapter.Fill(Me.FPTDataSet.V_CARD)

            ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''TODO: This line of code loads data into the 'FPTDataSet.T_Product' table. You can move, or remove it, as needed.
            'Me.T_ProductTableAdapter.Fill(Me.FPTDataSet.T_Product)
            ''  TProductBindingSource.Filter = "product_number<>''"
            ''TODO: This line of code loads data into the 'FPTDataSet.T_PACKING' table. You can move, or remove it, as needed.
            'Me.T_PACKINGTableAdapter.Fill(Me.FPTDataSet.T_PACKING)
            ''TODO: This line of code loads data into the 'FPTDataSet.T_STATUS' table. You can move, or remove it, as needed.
            'Me.T_STATUSTableAdapter.Fill(Me.FPTDataSet.T_STATUS)
            ''TODO: This line of code loads data into the 'FPTDataSet.T_SHIPMENT_TYPE' table. You can move, or remove it, as needed.
            'Me.T_SHIPMENT_TYPETableAdapter.Fill(Me.FPTDataSet.T_SHIPMENT_TYPE)
            ''TODO: This line of code loads data into the 'FPTDataSet.T_Customer' table. You can move, or remove it, as needed.
            'Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)
            '' TCustomerBindingSource.Filter = "SORG_CODE<>''"
            ''TODO: This line of code loads data into the 'FPTDataSet.T_SHIPPER' table. You can move, or remove it, as needed.
            'Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
            ''Me.FPTDataSet.V_LOADINGNOTE
            ''TODO: This line of code loads data into the 'FPTDataSet.T_COMPANY' table. You can move, or remove it, as needed.
            'Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
            ''  TCOMPANYBindingSource.Filter = "company_number<>''"
            ''TODO: This line of code loads data into the 'FPTDataSet.T_TRUCK' table. You can move, or remove it, as needed.
            'Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
            'Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)

            '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '    'Add controls one by one in error provider.
            '    MyErrorProvider.Controls.Clear()
            '    MyErrorProvider.Controls.Add(EDTruck, "Vehicle No")
            '    ' MyErrorProvider.Controls.Add(EDDriver, "Driver Name")
            '    'MyErrorProvider.Controls.Add(EDShipper, "Sales ORG")
            '    'MyErrorProvider.Controls.Add(EDCustomer, " Customer code")
            '    ' MyErrorProvider.Controls.Add(Order, "Order Qtr")
            '    ' MyErrorProvider.Controls.Add(Product, "Product Code or Mat Code")
            '    MyErrorProvider.ClearAllErrorMessages()
            '    MyErrorProvider.SummaryMessage = "Following fields are mandatory,"


            '    ' StatusFilter = " (Do_status=0) and (load_status in(1,2,3)) "
            '    StatusFilter = " (((LOAD_DOFULL<>'') and (Do_status=0) and (load_status in(1,2,3))) or  ((LOAD_DOFULL='') and (load_status in(1,2)))) "
            '    PlantFilter = " (PLANT='" & TSETTINGBindingSource(0)("PLANT") & "' or PLANT='') "
            '    V_LoadingnoteBindingSource.Filter = PlantFilter & " and " & StatusFilter



            '    Try
            '        'TLOBindingSource.Filter = "PLANT_LOCATION='" & TSETTINGBindingSource(0)("PLANT").ToString & "'"
            '        ' CBLoadingPoint.Text = TLOBindingSource(0)("LOAD_LOCATION").ToString
            '        ' MessageBox.Show(TLOBindingSource.Find("PLANT_LOCATION", TSETTINGBindingSource(0)("PLANT").ToString))


            '        TLOBindingSource.Position = TLOBindingSource.Find("PLANT_LOCATION", TSETTINGBindingSource(0)("PLANT").ToString)

            '        'TLOBindingSource.RemoveFilter()
            '    Catch ex As Exception
            '        'TLOBindingSource.RemoveFilter()
            '    End Try

            '    GroupPort.Enabled = False
            '    GroupBtadd.Visible = True
            '    GroupBtadd.Enabled = True
            '    GDetail.Enabled = False
            '    SelectDate.Value = Date.Now.Date
            '    '  RANThrLIGHTCONTROL()
            '    '   TLOBindingSource.Filter = "PLANT_LOCATION='" & TSETTINGBindingSource(0)("PLANT").ToString & "'"
        Catch ex As Exception
        End Try
        Adddata.Enabled = CheckAddPermisstion(Me.Tag)
        EditData.Enabled = CheckEditPermisstion(Me.Tag)
        weightoutprint.Enabled = CheckPrintPermisstion(Me.Tag)
        BtWorkPrint.Enabled = CheckPrintPermisstion(Me.Tag)
        '  BtUserRights.Enabled = CheckAddPermisstion(sender.tag)

        '  Me.ResumeLayout()
        'Me.ReportViewer1.RefreshReport()
    End Sub

    Private Function GetConnection() As SqlClient.SqlConnection
        'return a new connection to the database
        Try

            Return New SqlClient.SqlConnection(My.Settings.FPTConnectionString)

        Catch ex As Exception

        End Try
    End Function

    Private Sub Bsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bsave.Click
        Dim q, id As String
        Dim i, SL, SC, ST, r, j As Integer
        Dim presetsum As Double
        Dim loadtype As String
        Dim Truck_ID As Integer
        Dim conn As SqlClient.SqlConnection = GetConnection()

        If (WeightScal.Text.Replace(",", "") <> Main.weight.Text.Replace(",", "")) And (Main.weight.Text.Replace(",", "") <> 0) And (WeightScal.Text.Replace(",", "") <> "0") _
     Or WeightScal.Text.Replace(",", "") = "" Or WeightScal.Text.Replace(",", "") = "0" Then
            WeightScal.Text = Main.weight.Text.Replace(",", "")
        End If
        ' WeightIn.PerformClick()

        '' Check Error ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = False Then
            Exit Sub
        End If
        If LawWeightIn.Text = "" Then
            MessageBox.Show("Please Check Weight-In", "Check", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If (RadioBig.Checked = False) And (RadioLiquid.Checked = False) Then
            MessageBox.Show("Please Check Load Type", "Check", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        '' End Check Error ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        conn.Open()
        '' Sum Preset 
        For i = 0 To TRUCK_COMP_NUM - 1
            If PRESETNO(i).Text = "" Then
                PRESETNO(i).Text = "0"
            End If
            presetsum = Int(PRESETNO(i).Text) + presetsum
        Next i
        EDPreset.Text = presetsum
        ''End Sum Preset '''''

        ''Loading Type
        If (RadioBig.Checked = True) Then
            loadtype = RadioBig.Text
        Else
            loadtype = RadioLiquid.Text
        End If
        ''End Loading Type
        '' Confirm Save To Database  ''''''''''
        If MsgBox("Are you sure to save?", vbYesNo + vbDefaultButton1, "Confirmation") = vbYes Then
            Cursor = Cursors.WaitCursor
            Try
                If Adnote_ALLDO.Length < 0 Then
                    ReDim Preserve Adnote_ALLDO(0)
                End If
            Catch ex As Exception
                ReDim Preserve Adnote_ALLDO(0)
            End Try

            Dim incOne As Integer = 0
            Dim BaySelectIndex, MeterSelectIndex As Integer
            BaySelectIndex = Bay.SelectedIndex
            MeterSelectIndex = Meter.SelectedIndex
            Truck_ID = TruckId.Text

            For i = 0 To Adnote_ALLDO.Length - 1
                If Adnote_ALLDO.Length > 1 Then
                    Adnote_DO = Adnote_ALLDO(i)
                Else
                    Try
                        Adnote_DO.dO_ITEM = TBATCHLOSTTASBindingSource(0)("DO_ITEM").ToString
                    Catch ex As Exception

                    End Try
                End If

                EDLoadID.Text = EDLoadID.Text + incOne
                EDREf.Text = EDREf.Text + incOne
                incOne = 1

                Try
                    If Adnote_ALLDO.Length > 1 Then
                        presetsum = 0
                        If TBATCHLOSTTASBindingSource.Count = 0 Then
                            presetsum = Adnote_ALLDO(i).aCT_QTY_DELV_SU
                        End If

                        For j = 0 To TBATCHLOSTTASBindingSource.Count - 1
                            If TBATCHLOSTTASBindingSource.Item(j)("DO_ITEM").ToString = Adnote_DO.dO_ITEM Then
                                TBATCHLOSTTASBindingSource.Item(j)("LOAD_ID") = EDLoadID.Text
                                presetsum = presetsum + TBATCHLOSTTASBindingSource.Item(j)("stock_quantity").ToString
                                EDPreset.Text = presetsum
                                ' PRESETNO(0).Text = presetsum
                            End If
                        Next

                        Try
                            Dim Qsql As String = "Select Product_code from T_PRODUCT WHERE Product_number='" & Adnote_DO.mAT_CODE & "'"
                            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(Qsql, conn)
                            Dim dt As New DataTable
                            da.Fill(dt)
                            Product.SelectedIndex = Product.FindStringExact(dt.Rows(0)("Product_code").ToString)
                        Catch ex As Exception
                        End Try


                        Try

                            Bay.SelectedIndex = BaySelectIndex
                            Meter.SelectedIndex = MeterSelectIndex

                            TUNITBindingSource.Filter = "CODE='" & UCase(Adnote_DO.sALES_UNIT) & "'"
                            If TUNITBindingSource.Count > 0 Then
                                Order.Text = presetsum * TUNITBindingSource(0)("TOkg")
                            Else
                                Order.Text = presetsum
                            End If

                            'Select Case UCase(Adnote_DO.SALES_UNIT)
                            '    Case "L"
                            '        Order.Text = presetsum
                            '    Case "TO", "MT"
                            '        Order.Text = presetsum * 1000
                            '    Case Else
                            '        Order.Text = presetsum
                            'End Select
                        Catch ex As Exception

                        End Try
                        If Adnote_DO.bATCH_INDICATOR = "" Then
                            presetsum = Adnote_DO.aCT_QTY_DELV_SU
                            TUNITBindingSource.Filter = "CODE='" & UCase(Adnote_DO.sALES_UNIT) & "'"
                            If TUNITBindingSource.Count > 0 Then
                                Order.Text = presetsum * TUNITBindingSource(0)("TOkg")
                            Else
                                Order.Text = presetsum
                            End If

                        End If

                        '   Order.Text = presetsum
                        OrderBut.PerformClick()
                    End If
                Catch ex As Exception
                End Try

                q = ""
                q = "select max(ST_ID)+1 as ST_ID"
                q &= " from T_ST"
                Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                Dim ds1 As New DataSet()
                Dim dt1 As New DataTable
                da1.Fill(dt1)
                ST = dt1.Rows(0).Item("ST_ID").ToString
                dt1.Clear()
                ds1.Clear()

                q = ""
                q = "select count(load_id) as CLoad_id "
                q &= "from T_loadingnote "
                q &= "where  (DATEPART(dd, LOAD_DATE) = (DATEPART(dd,GETDATE()))) "
                q &= "AND  (DATEPART(mm, LOAD_DATE) = (DATEPART(mm,GETDATE()))) "
                q &= "AND  (DATEPART(yy, LOAD_DATE) = (DATEPART(yy,GETDATE()))) "
                q &= "AND (ST_ID NOT IN (SELECT st_id FROM t_st))  "
                Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                'Dim ds2 As New DataSet()
                Dim dt2 As New DataTable
                da2.Fill(dt2)
                SL = dt2.Rows(0).Item("CLoad_id").ToString
                SC = ST + SL

                q = ""
                q = "Insert into T_Loadingnote "
                q &= " (LOAD_ID,"
                q &= " LOAD_CARD,"
                q &= " BADGE_NO,"
                q &= " LOAD_DID,"
                q &= " LOAD_VEHICLE,"
                q &= " LOAD_STATUS,"
                q &= " LOAD_CAPACITY,"
                q &= " ST_ID,"
                q &= " LOAD_Shipper,"
                q &= " LOAD_STARTTIME,"
                q &= " AddnoteDate,"
                q &= " LOAD_DRIVER,"
                q &= " LOAD_PRESET,"
                q &= " LOAD_DOfull, "
                q &= " Reference, "
                q &= " COA, "
                q &= " Remark, "
                q &= " Container, "
                q &= " Container_Tare, "
                q &= " Gi_date, "
                q &= " BatchLot, "
                q &= " Raw_Weight_in, "
                q &= " Update_Weight_in, "
                q &= " AccessCode, "
                q &= " ShipmentNo, "
                q &= " LOAD_SEALCOUNT, "
                q &= " load_Temp, "
                q &= " Density, "
                q &= " load_Tank, "
                q &= " Packing_Qty, "
                q &= " Packing_weight, "
                q &= " Packing_Type, "
                q &= " Load_customer, "
                q &= " weightIn_time, "
                q &= " weightIn_date, "
                q &= " ShipmentType, "
                q &= " Load_Type, "
                q &= " Load_seal, "
                q &= " BATCH_LOT, "
                q &= " TANKFARM, "

                q &= " LOAD_TRUCKCOMPANY) "
                q &= " Values ("
                q &= "'" + (EDLoadID.Text) + "'" + ","

                If CBCardNO.Text <> "" Then
                    q &= "'" + (CBCardNO.Text) + "'" + ","
                Else
                    q &= "'" + ("0") + "'" + ","
                End If

                If CBCard.Text <> "" Then
                    q &= "'" + (CBCard.Text) + "'" + ","
                Else
                    q &= "'" + ("0") + "'" + ","
                End If

                q &= "'" + (EDLoaddate.Text + i).ToString + "'" + ","
                q &= "'" + (Truck_ID.ToString) + "'" + ","
                'q &= "1" + ","
                q &= "'" + (StatusId.Text) + "'" + ","
                q &= "'" + (EDTruckCapa.Text) + "'" + ","
                q &= "'" + Str(SC) + "'" + ","
                q &= "'" + (ShipperId.Text) + "'" + ","
                q &= "'" + (EDLoadDTime.Text) + "'" + ","
                q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
                q &= "'" + (DriverID.Text) + "'" + ","
                q &= "'" + (EDPreset.Text) + "'" + ","
                q &= "'" + (EDDONo.Text) + "'" + ","
                q &= "'" + (EDREf.Text) + "'" + ","
                q &= "'" + (Coa.Text) + "'" + ","
                q &= "'" + (Edremark.Text) + "'" + ","
                q &= "'" + (Container.Text) + "'" + ","
                q &= "'" + (Container_Tare.Text) + "'" + ","
                q &= "'" + String.Format("{0:yyyy-MM-dd}", GiDate.Value) + "'" + ","
                ' q &= "'" + (Batchlot.Text) + "'" + ","
                q &= "'" + ("") + "'" + ","
                q &= "'" + (LawWeightIn.Text) + "'" + ","
                q &= "'" + (UpdateWeightIn.Text) + "'" + ","
                q &= "'" + (AccessCode.Text) + "'" + ","
                q &= "'" + (ShipmentNo.Text) + "'" + ","
                q &= "'" + (SealCount.Text) + "'" + ","
                q &= "'" + (Temp.Text) + "'" + ","
                q &= "'" + (Density.Text) + "'" + ","
                q &= "'" + (Tank.Text) + "'" + ","
                q &= "'" + (Quantity.Text) + "'" + ","
                q &= "'" + (PackingWeight.Text) + "'" + ","
                q &= "'" + (PackingId.Text) + "'" + ","
                q &= "'" + (CustomerID.Text) + "'" + ","
                q &= "'" + (Weightintime.Text) + "'" + ","
                q &= "'" + Now.Date.ToString("yyyy-MM-dd") + "',"
                q &= "'" + (Shipment_id.Text) + "'" + ","
                q &= "'" + (loadtype) + "'" + ","
                q &= "'" + (Seal_No.Text) + "'" + ","
                Try
                    Dim batchlot As String = ""
                    For b = 0 To TBATCHLOSTTASBindingSource.Count - 1
                        If batchlot <> "" Then batchlot = batchlot & "/"
                        batchlot = batchlot & TBATCHLOSTTASBindingSource.Item(b)("BATCH_NO").ToString
                    Next
                    q &= "'" + (batchlot) + "'" + ","
                Catch ex As Exception
                    q &= "'" + ("") + "'" + ","
                End Try

                q &= "'" + (CBTankfarm.Text) + "'" + ","
                q &= "'" + (TruckCompanyId.Text) + "'" + ")"

                Try

                    da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                    da.InsertCommand.ExecuteNonQuery()

                    If Adnote_DO.dO_CDATE = "" Then Adnote_DO.dO_CDATE = Dateedit.Value.Date.ToString("dd/MM/yyyy")
                    If Adnote_DO.dELV_DATE = "" Then Adnote_DO.dELV_DATE = Dateedit.Value.Date.ToString("dd/MM/yyyy")
                    If Adnote_DO.dOC_DATE = "" Then Adnote_DO.dOC_DATE = Dateedit.Value.Date.ToString("dd/MM/yyyy")
                    If Adnote_DO.lOAD_DATE = "" Then Adnote_DO.lOAD_DATE = Dateedit.Value.Date.ToString("dd/MM/yyyy")
                    If Adnote_DO.pICK_DATE = "" Then Adnote_DO.pICK_DATE = Dateedit.Value.Date.ToString("dd/MM/yyyy")
                    If Adnote_DO.pLAN_GI_DATE = "" Then Adnote_DO.pLAN_GI_DATE = GiDate.Value.Date.ToString("dd/MM/yyyy")
                    If Adnote_DO.tRN_PLAN_DATE = "" Then Adnote_DO.tRN_PLAN_DATE = Dateedit.Value.Date.ToString("dd/MM/yyyy")

                    If Adnote_DO.dO_CTIME = "" Then Adnote_DO.dO_CTIME = Now.TimeOfDay.Hours.ToString("00") & ":" & Now.TimeOfDay.Minutes.ToString("00") & ":" & Now.TimeOfDay.Seconds.ToString("00")

                    Adnote_DO.dO_NO = EDDONo.Text

                    ' Adnote_DO.DO_NO = Cbn11.Text
                    If Adnote_DO.dO_ITEM = "" Then Adnote_DO.dO_ITEM = EDDO_item.Text
                    If Adnote_DO.sO_NO = "" Then Adnote_DO.sO_NO = Sono.Text
                    If Adnote_DO.sO_ITEM = "" Then Adnote_DO.sO_ITEM = Soitem.Text
                    If Adnote_DO.sALES_ORG = "" Then Adnote_DO.sALES_ORG = TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString

                    q = ""
                    q = "insert into T_DO ("
                    q &= "Load_Id,"
                    q &= "DO_NO,"
                    q &= "DO_ITEM,"
                    q &= "SO_NO,"
                    q &= "SO_ITEM,"
                    q &= "DO_CDATE,"
                    q &= "DO_CTIME,"
                    q &= "SD_DOC_CAT,"
                    q &= "SALES_ORG,"
                    q &= "DIS_CHAN,"
                    q &= "DIVISION,"
                    q &= "SHIP_POINT,"
                    q &= "DOC_DATE,"
                    q &= "PLAN_GI_DATE,"
                    q &= "LOAD_DATE,"
                    q &= "TRN_PLAN_DATE,"
                    q &= "DELV_DATE,"
                    q &= "PICK_DATE,"
                    q &= "SHIP_TO_PARTY,"
                    q &= "SOLD_TO_PARTY,"
                    q &= "TOTAL_WEIGHT,"
                    q &= "MAT_CODE,"
                    q &= "PLANT,"
                    q &= "STORAGE_LOC,"
                    q &= "BATCH_NO,"
                    q &= "ACT_QTY_DELV_SU,"
                    q &= "BASE_UOM,"
                    q &= "SALES_UNIT,"
                    q &= "NET_WEIGHT,"
                    q &= "GROSS_WEIGHT,"
                    q &= "WEIGHT_UNIT,"
                    q &= "VOLUME,"
                    q &= "VOLUME_UNIT,"
                    q &= "OVR_DELV_TLIMIT,"
                    q &= "UND_DELV_TLIMIT,"
                    q &= "ACT_GI_DATE,"
                    q &= "ACT_GI_TIME,"
                    q &= "BATCH_INDICATOR,"
                    q &= "COA,"
                    q &= "lOAD_TIME,"
                    q &= "mAT_DES,"
                    q &= "dELIVERY_INSTRUCTION_TEXT,"
                    q &= "pUR_ORDER_TYPE,"
                    q &= "lOADING_POINT,"
                    q &= "sTAT_WAREHOUSE,"
                    q &= "OVERALL_STAT,"
                    q &= "DO_INS_TXT,"
                    q &= "LOAD_PT,"
                    q &= "CHK_DAT,"
                    q &= "CHK_TIM,"
                    q &= "DO_sHIP_TYPE,"
                    q &= "sHIP_TYPE_DESC,"
                    q &= "cUST_PO_NUMBER"
                    q &= ") values ("
                    q &= EDLoadID.Text + ","
                    q &= "'" + Adnote_DO.dO_NO + "',"
                    q &= "'" + Adnote_DO.dO_ITEM + "',"
                    q &= "'" + Adnote_DO.sO_NO + "',"
                    q &= "'" + Adnote_DO.sO_ITEM + "',"

                    'q &= "'" + Adnote_DO.DO_CDATE + "',"
                    '  Dim s As String = "20060801"
                    ' Response.Write(DateTime.ParseExact(s, "yyyyMMdd", Nothing))

                    '    Convert.ToDateTime(Adnote_DO.DO_CDATE)

                    q &= "'" + DateTime.ParseExact(Adnote_DO.dO_CDATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    'q &= "'" + DateTime.Parse(Adnote_DO.dO_CTIME) + "'" + ","
                    q &= "'" + DateTime.ParseExact(Adnote_DO.dO_CTIME.Replace(":", ""), "HHmmss", Nothing).ToString("HH:mm:ss") + "'" + ","
                    'q &= "'" + Convert.ToDateTime(Adnote_DO.dO_CTIME).ToString("hhmmss") + "'" + ","
                    q &= "'" + Adnote_DO.sD_DOC_CAT + "',"
                    q &= "'" + Adnote_DO.sALES_ORG + "',"
                    q &= "'" + Adnote_DO.dIS_CHAN + "',"
                    q &= "'" + Adnote_DO.dIVISION + "',"
                    q &= "'" + Adnote_DO.sHIP_POINT + "',"
                    q &= "'" + DateTime.ParseExact(Adnote_DO.dOC_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    q &= "'" + DateTime.ParseExact(Adnote_DO.pLAN_GI_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    q &= "'" + DateTime.ParseExact(Adnote_DO.lOAD_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    q &= "'" + DateTime.ParseExact(Adnote_DO.tRN_PLAN_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    q &= "'" + DateTime.ParseExact(Adnote_DO.dELV_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    q &= "'" + DateTime.ParseExact(Adnote_DO.pICK_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd") + "'" + ","
                    q &= "'" + Adnote_DO.sHIP_TO_PARTY + "',"
                    q &= "'" + Adnote_DO.sOLD_TO_PARTY + "',"
                    q &= "'" + Adnote_DO.tOTAL_WEIGHT + "',"
                    q &= "'" + Adnote_DO.mAT_CODE + "',"
                    q &= "'" + Adnote_DO.pLANT + "',"
                    q &= "'" + Adnote_DO.sTORAGE_LOC + "',"
                    q &= "'" + Adnote_DO.bATCH_NO + "',"
                    q &= "'" + Adnote_DO.aCT_QTY_DELV_SU + "',"
                    q &= "'" + Adnote_DO.bASE_UOM + "',"
                    q &= "'" + Adnote_DO.sALES_UNIT + "',"
                    q &= "'" + Adnote_DO.nET_WEIGHT + "',"
                    q &= "'" + Adnote_DO.gROSS_WEIGHT + "',"
                    q &= "'" + Adnote_DO.wEIGHT_UNIT + "',"
                    q &= "'" + Adnote_DO.vOLUME + "',"
                    q &= "'" + Adnote_DO.vOLUME_UNIT + "',"
                    q &= "'" + Adnote_DO.oVR_DELV_TLIMIT + "',"
                    q &= "'" + Adnote_DO.uND_DELV_TLIMIT + "',"
                    q &= "'" + Dateedit.Value.ToString("yyyy-MM-dd") + "',"
                    q &= "'" + Dateedit.Value.ToString("HH:mm:ss") + "',"
                    q &= "'" + Adnote_DO.bATCH_INDICATOR + "',"
                    q &= "'" + Coa.Text + "',"
                    q &= "'" + Adnote_DO.lOAD_TIME + "',"
                    q &= "'" + Adnote_DO.mAT_DES + "',"
                    q &= "'" + Adnote_DO.dELIVERY_INSTRUCTION_TEXT + "',"
                    q &= "'" + Adnote_DO.pUR_ORDER_TYPE + "',"
                    q &= "'" + Adnote_DO.lOADING_POINT + "',"
                    q &= "'" + Adnote_DO.sTAT_WAREHOUSE + "',"
                    q &= "'" + Adnote_DO.oVERALL_STAT + "',"
                    q &= "'" + Edremark2.Text + "',"
                    q &= "'" + CBLoadingPoint.Text + "',"
                    q &= "'" + String.Format("{0:yyyy-MM-dd}", Now) + "'" + ","
                    q &= "'" + (Weightintime.Text) + "'" + ","
                    q &= "'" + (Adnote_DO.sHIP_TYPE) + "'" + ","
                    q &= "'" + (Adnote_DO.sHIP_TYPE_DESC) + "'" + ","
                    q &= "'" + (Adnote_DO.cUST_PO_NUMBER) + "'" + ""
                    q &= ")"

                    Try
                        da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                        da.InsertCommand.ExecuteNonQuery()

                    Catch ex As Exception
                        q = ""
                        q = "delete t_loadingnote where load_id = "
                        q &= EDLoadID.Text
                        da.DeleteCommand = New SqlClient.SqlCommand(q, conn)
                        da.DeleteCommand.ExecuteNonQuery()

                        MessageBox.Show(ex.Message + " Wrong information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Cursor = Cursors.Default
                        Exit Sub
                    End Try


                    ''''''End Update T_truck''''''''''''''



                Catch ex As Exception
                    MessageBox.Show(ex.Message + " Unable to add information, please check " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Cursor = Cursors.Default
                    Exit Sub
                End Try
                For r = 0 To TRUCK_COMP_NUM - 1


                    q = ""
                    q = "Insert into T_LOADINGNOTECOMPARTMENT "
                    q &= " (LC_LOAD,"
                    q &= " LC_COMPARTMENT,"
                    q &= " LC_STATUS,"
                    q &= " LC_BASE,"
                    q &= " LC_BLEND,"
                    q &= " LC_PRO,"
                    q &= " LC_CAPACITY,"
                    q &= " LC_PRESET,"
                    q &= " LC_ISLAND,"
                    q &= " LC_BAY,"
                    q &= " LC_SEAL,"
                    q &= " LC_METER) "
                    q &= "Values ("
                    q &= "'" + EDLoadID.Text + "'" + ","
                    q &= "'" + (COMPNO(r).Text) + "'" + ","

                    Try
                        If PRESETNO(r).Text > 0 Then
                            q &= "1" + ","
                        Else
                            q &= "99" + ","
                        End If
                    Catch ex As Exception
                        q &= "99" + ","
                    End Try
                    q &= "'" + (PRESETNO(r).Text) + "'" + ","
                    q &= "0" + ","

                    id = "Select * from T_Product where product_code="
                    id &= "'" + ProductNo(r).Text + "'"
                    Dim da5 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(id, conn)
                    Dim ds5 As New DataSet()
                    Dim dt5 As New DataTable
                    da5.Fill(dt5)
                    If dt5.Rows.Count > 0 Then
                        q &= "'" + (dt5.Rows(0).Item("ID").ToString) + "'" + ","
                    Else
                        q &= "1" + ","
                    End If

                    q &= "'" + (CapacityNo(r).Text) + "'" + ","
                    q &= "'" + (PRESETNO(r).Text) + "'" + ","
                    q &= "'" + (Strings.Left(Trim(BayNo(r).Text), 1)) + "'" + ","

                    If BayNo(r).Text = "" Then
                        q &= "'5',"
                    Else
                        q &= "'" + (Strings.Right(Trim(BayNo(r).Text), 1)) + "'" + ","
                    End If

                    ' q &= "'" + (Strings.Right(Trim(BayNo(r).Text), 1)) + "'" + ","
                    q &= "'" + (Seal_No.Text) + "'" + ","

                    id = "select * from T_BATCHMETER where Batch_Name ="
                    id &= "'" + Meterno(r).Text + "'"
                    Dim da6 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(id, conn)
                    Dim ds6 As New DataSet()
                    Dim dt6 As New DataTable
                    da6.Fill(dt6)
                    If dt6.Rows.Count > 0 Then
                        q &= "" + (dt6.Rows(0).Item("Batch_Number").ToString) + "" + ")"
                    Else
                        q &= "0" + ")"
                    End If

                    Try
                        da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                        da.InsertCommand.ExecuteNonQuery()
                    Catch ex As Exception
                        q = ""
                        q = "delete t_loadingnote where load_id = "
                        q &= EDLoadID.Text
                        da.DeleteCommand = New SqlClient.SqlCommand(q, conn)
                        da.DeleteCommand.ExecuteNonQuery()

                        q = ""
                        q = "delete t_do where load_id = "
                        q &= EDLoadID.Text
                        da.DeleteCommand = New SqlClient.SqlCommand(q, conn)
                        da.DeleteCommand.ExecuteNonQuery()

                        q = ""
                        q = "delete t_loadingnotecompartment where lc_load = "
                        q &= EDLoadID.Text
                        da.DeleteCommand = New SqlClient.SqlCommand(q, conn)
                        da.DeleteCommand.ExecuteNonQuery()

                        MessageBox.Show(ex.Message + " Wrong information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Cursor = Cursors.Default
                        Exit Sub
                    End Try
                Next r





                '    MessageBox.Show("Insert data Successfully", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Main.CEvent("USER :" + Main.U_NAME + " Created Advisenote Reference: " + EDREf.Text)

                'ref = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("reference").ToString()

                'If i = Adnote_ALLDO.Length - 1 Then
                '    BtWorkPrint_Click(sender, e)
                'End If


                'Dim ref As String
                'ref = EDREf.Text
                'Dim Myreport As New ReportDocument
                'Myreport = New ReportDocument
                'Dim sql As String
                'sql = "Select * from V_Loadingnote where reference =" + ref + ""
                'Dim da7 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                'Dim ds7 As New DataSet()
                'Dim dt7 As New DataTable
                'da7.Fill(ds7, "V_Loadingnote")

                ''Select Case Main.SALE_ORG
                ''    Case 1100
                ''        Myreport.Load("TOCAdvisenotereport.rpt")
                ''    Case 1200
                ''        Myreport.Load("TEAAdvisenotereport.rpt")
                ''    Case 1500
                ''        Myreport.Load("TOLAdvisenotereport.rpt")
                ''    Case 1600
                ''        Myreport.Load("TOLAdvisenotereport.rpt")
                ''    Case Else
                ''        Myreport.Load("Advisenotereport.rpt")
                ''End Select

                'Myreport.Load(TSHIPPERBindingSource(0)("LOAD_Workorder").ToString())
                ''Select Case Main.SALE_ORG
                ''    Case 1100

                ''        Myreport.Load("TOCWorkOrder.rpt")
                ''    Case 1200
                ''        Myreport.Load("TEAWorkOrder.rpt")
                ''    Case 1500
                ''        Myreport.Load("TOLWorkOrder.rpt")
                ''    Case 1600
                ''        Myreport.Load("TOLWorkOrder.rpt")
                ''    Case Else
                ''        Myreport.Load("WorkOrder.rpt")
                ''End Select


                ''    TOCWorkOrder()
                '' Myreport.Load("Advisenotereport.rpt")
                'Try
                '    Myreport.SetDataSource(ds7.Tables("V_Loadingnote"))
                '    Try
                '        Myreport.PrintOptions.PrinterName = "GCCOA"
                '    Catch ex As Exception

                '    End Try
                '    RemoveOwnedForm(ReportPrint)
                '    ReportPrint.Close()
                '    ReportPrint.CrystalReportViewer1.ReportSource = Myreport
                '    ReportPrint.ShowDialog()
                'Catch ex As Exception

                'End Try

            Next

            'Update T_truck
            Try
                q = "Update T_truck set truck_driver='" & DriverID.Text & "', TRUCK_COMPANY='" & TruckCompanyId.Text _
                    & "' where ID='" & Truck_ID.ToString & "'"
                da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                da.UpdateCommand.ExecuteNonQuery()
                Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)

            Catch ex As Exception
                ' q = ""
                ' MessageBox.Show("Wrong information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '  Cursor = Cursors.Default
                '  Exit Sub
            End Try
            Try
                Savebatch(TBATCHLOSTTASBindingSource.DataSource, EDDONo.Text, "")

                BtWorkPrint_Click(sender, e)

                Bcancel_Click(sender, e)
                conn.Close()
                conn.Dispose()
                Cursor = Cursors.Default

            Catch ex As Exception

            End Try
        End If

    End Sub

    Private Sub Advisenote_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Me.SuspendLayout()
        Dim q As String ', s_day, s_month, s_year, s_hr, s_mn, s_sc, TTimes As String
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try
            conn.Open()
            's_day = Date.Now.Day
            's_month = Date.Now.Month
            's_year = Date.Now.Year
            ' ClearData()


            'q = ""
            'q = "select * from v_Loadingnote order by id desc"
            'Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds As New DataSet()
            'Dim dt As New DataTable



            'q = ""
            'q = "Select max(id) as ID,Max(TRUCK_NUMBER) as TRUCK_NUMBER   from V_TRUCK2 group by TRUCK_NUMBER order by TRUCK_NUMBER"
            'Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds1 As New DataSet()
            'Dim dt1 As New DataTable
            Dim numrow, indexx As Integer



            'da1.Fill(dt1)
            'numrow = dt1.Rows.Count
            'EDTruck.Items.Clear()
            'TruckId.Items.Clear()
            'EDTruck.Items.Add("")
            'TruckId.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    TruckId.Items.Add(dt1.Rows(indexx).Item("ID").ToString())
            '    EDTruck.Items.Add(dt1.Rows(indexx).Item("TRUCK_NUMBER").ToString())
            'Next indexx



            'q = ""
            'q = "select * from T_company order by COMPANY_Code"
            'Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds2 As New DataSet()
            'Dim dt2 As New DataTable
            'da2.Fill(dt2)
            'numrow = dt2.Rows.Count
            'TruckCompanyId.Items.Clear()
            'EDForword.Items.Clear()
            'TruckCompanyId.Items.Add("")
            'EDForword.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    TruckCompanyId.Items.Add(dt2.Rows(indexx).Item("COMPANY_ID").ToString())
            '    EDForword.Items.Add(dt2.Rows(indexx).Item("Company_code").ToString())
            'Next indexx

            'q = ""
            'q = "Select * from V_DRIVER Where Driver_BLACK_LIST <>'Yes'"
            ''q &= " and  Driver_date_End >'" + (FormatDateTime(Now, DateFormat.ShortDate)) + "'" '("yyyy-MM-dd", Now)) + ""
            'q &= " order by Driver_name"

            'Dim da3 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds3 As New DataSet()
            'Dim dt3 As New DataTable
            'da3.Fill(dt3)
            'numrow = dt3.Rows.Count
            'EDDriver.Items.Clear()
            'DriverID.Items.Clear()

            'EDDriver.Items.Add("")
            'DriverID.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    DriverID.Items.Add(dt3.Rows(indexx).Item("ID").ToString())
            '    EDDriver.Items.Add(dt3.Rows(indexx).Item("Driver_Name").ToString())
            'Next indexx




            'q = ""
            'q = "Select * from T_Shipper order by SP_CODE"
            'Dim da4 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds4 As New DataSet()
            'Dim dt4 As New DataTable
            'da4.Fill(dt4)
            'numrow = dt4.Rows.Count
            'EDShipper.Items.Clear()
            'ShipperId.Items.Clear()
            'EDShipper.Items.Add("")
            'ShipperId.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    ShipperId.Items.Add(dt4.Rows(indexx).Item("ID").ToString())
            '    EDShipper.Items.Add(dt4.Rows(indexx).Item("SP_CODE").ToString())
            'Next indexx



            'q = ""
            'q = "Select * from T_Customer"
            'Dim da8 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds8 As New DataSet()
            'Dim dt8 As New DataTable
            'da8.Fill(dt8)
            'numrow = dt8.Rows.Count
            'EDCustomer.Items.Clear()
            'CustomerID.Items.Clear()
            'Customer_sapcode.Items.Clear()

            'EDCustomer.Items.Add("")
            'CustomerID.Items.Add("")
            'Customer_sapcode.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    CustomerID.Items.Add(dt8.Rows(indexx).Item("ID").ToString())
            '    EDCustomer.Items.Add(dt8.Rows(indexx).Item("Customer_code").ToString())
            '    Customer_sapcode.Items.Add(dt8.Rows(indexx).Item("Customer_Number").ToString())
            'Next indexx

            'q = ""
            'q = "Select * from T_status order by status_id"
            'Dim da9 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds9 As New DataSet()
            'Dim dt9 As New DataTable
            'da9.Fill(dt9)
            'numrow = dt9.Rows.Count
            'Status.Items.Clear()
            'StatusId.Items.Clear()
            'Status.Items.Add("")
            'StatusId.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    StatusId.Items.Add(dt9.Rows(indexx).Item("Status_id").ToString())
            '    Status.Items.Add(dt9.Rows(indexx).Item("Status_name").ToString())
            'Next indexx

            'q = ""
            'q = "Select * from T_packing order by p_code"
            'Dim da10 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds10 As New DataSet()
            'Dim dt10 As New DataTable
            'da10.Fill(dt10)
            'numrow = dt10.Rows.Count

            'Packing.Items.Clear()
            'PackingId.Items.Clear()
            'P_Weight.Items.Clear()

            'Packing.Items.Add("")
            'PackingId.Items.Add("")
            'P_Weight.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    PackingId.Items.Add(dt10.Rows(indexx).Item("P_ID").ToString())
            '    Packing.Items.Add(dt10.Rows(indexx).Item("P_Code").ToString())
            '    P_Weight.Items.Add(dt10.Rows(indexx).Item("P_Weight").ToString())
            'Next indexx

            'q = ""
            'q = "Select * from T_product where product_type=1 order by product_code"
            'Dim da11 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds11 As New DataSet()
            'Dim dt11 As New DataTable
            'da11.Fill(dt11)
            'numrow = dt11.Rows.Count
            'Product.Items.Clear()
            'ProductId.Items.Clear()

            'Product.Items.Add("")
            'ProductId.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    ProductId.Items.Add(dt11.Rows(indexx).Item("ID").ToString())
            '    Product.Items.Add(dt11.Rows(indexx).Item("product_code").ToString())
            'Next indexx



            q = ""
            ' q = "select BATCH_ISLAND_NO,max(Batch_Bay) as Batch_Bay,max(ISLAND_LEFT) as ISLAND_LEFT,max(ISLAND_RIGHT) as ISLAND_RIGHT from V_BATCHMETER"
            ' q &= " GROUP BY BATCH_ISLAND_NO order by Batch_Bay"
            q = "select Batch_Bay from V_BATCHMETER"
            q &= " GROUP BY Batch_Bay order by Batch_Bay"
            Dim da12 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim ds12 As New DataSet()
            Dim dt12 As New DataTable
            da12.Fill(dt12)
            numrow = dt12.Rows.Count
            Bay.Items.Clear()
            Bay.Items.Add("")
            ' ProductId.Items.Add("")
            For indexx = 0 To numrow - 1
                'ProductId.Items.Add(dt12.Rows(indexx).Item("ID").ToString())

                'Bay.Items.Add(dt12.Rows(indexx).Item("BATCH_ISLAND_NO").ToString())

                Bay.Items.Add(dt12.Rows(indexx).Item("Batch_Bay").ToString())
            Next indexx
            ' Bay.Items.Add("5")
            'q = ""
            'q = "select * from V_BATCHMETER order by Batch_NAME"
            'Dim da13 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds13 As New DataSet()
            'Dim dt13 As New DataTable
            'da13.Fill(dt13)
            'numrow = dt13.Rows.Count
            'Meter.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    Meter.Items.Add(dt13.Rows(indexx).Item("Batch_NAME").ToString())
            'Next indexx

            'q = ""
            'q = "select * from T_SHIPMENT_TYPE order by Code"
            'Dim da14 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            'Dim ds14 As New DataSet()
            'Dim dt14 As New DataTable
            'da14.Fill(dt14)
            'numrow = dt14.Rows.Count

            'ShipMent_Type.Items.Clear()
            'Shipment_id.Items.Clear()

            'ShipMent_Type.Items.Add("")
            'Shipment_id.Items.Add("")
            'For indexx = 0 To numrow - 1
            '    ShipMent_Type.Items.Add(dt14.Rows(indexx).Item("code").ToString())
            '    Shipment_id.Items.Add(dt14.Rows(indexx).Item("ID").ToString())
            'Next indexx
            da12.Dispose()
            dt12.Dispose()
            ds12.Dispose()
            conn.Close()
            conn.Dispose()

        Catch ex As Exception
            conn.Close()
            conn.Dispose()
        End Try
        Me.ResumeLayout()
    End Sub

    Private Sub EDTruck_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim q As String
        Dim TRUCK_CAPASITY As Integer
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try
            conn.Open()


            EDTruck.Text = EDTruck.Text
            TRUCK_COMP_NUM = 0
            EDTruck.SelectedIndex = EDTruck.FindStringExact(EDTruck.Text)

            If EDTruck.SelectedIndex >= 0 And EDTruck.Text <> "" Then
                q = "select * from V_TRUCK2 where TRUCK_NUMBER= '" + (EDTruck.Text) + "'"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                'Dim ds As New DataSet()
                Dim dt As New DataTable
                da.Fill(dt)


                ' EDDriver.SelectedIndex = EDDriver.Items.IndexOf(dt.Rows(0).Item("TRUCK_DRIVER").ToString())
                TRUCK_CAPASITY = dt.Rows(0).Item("TRUCK_CAPASITY").ToString()
                TRUCK_COMP_NUM = dt.Rows(0).Item("TRUCK_COMP_NUM").ToString()
                '   EDForword.Text = dt.Rows(0).Item("TRUCK_COMPANY").ToString()
                EDTruckCapa.Text = TRUCK_CAPASITY.ToString
                'Cbn10.Text = TRUCK_CAPASITY
                'RichTextBox1.Text = TRUCK_COMP_NUM

                Dim i As Integer
                Try
                    Try
                        'For i = ProductNo.Length To 0 Step -1
                        'ProductNo = Nothing
                        'CapacityNo = Nothing
                        'BayNo(i) = Nothing
                        'Meterno(i) = Nothing
                        'PRESETNO(i) = Nothing
                        'COMPNO(i) = Nothing
                        'GroupBox15.Controls.Clear()
                        'GProduct.Controls.Clear()
                        'GroupBox12.Controls.Clear()
                        'GPreset.Controls.Clear()
                        'GroupBox10.Controls.Clear()

                        For i = 0 To ProductNo.Length
                            ProductNo(i).Dispose()
                            CapacityNo(i).Dispose()
                            BayNo(i).Dispose()
                            Meterno(i).Dispose()
                            PRESETNO(i).Dispose()
                            COMPNO(i).Dispose()
                        Next
                    Catch ex As Exception

                    End Try
                Finally
                    ReDim ProductNo(0)
                    ReDim CapacityNo(0)
                    ReDim BayNo(0)
                    ReDim Meterno(0)
                    ReDim PRESETNO(0)
                    ReDim COMPNO(0)
                End Try
                Dim aa As Integer = 1


                For i = TRUCK_COMP_NUM - 1 To 0 Step -1
                    ReDim Preserve COMPNO(TRUCK_COMP_NUM)
                    COMPNO(i) = New TextBox
                    COMPNO(i).Text = (i - TRUCK_COMP_NUM) + TRUCK_COMP_NUM + 1 '(TRUCK_COMP_NUM - i) + 1
                    COMPNO(i).TextAlign = HorizontalAlignment.Center
                    COMPNO(i).Height = 15
                    COMPNO(i).Width = GroupBox15.Width
                    COMPNO(i).Parent = GroupBox15
                    COMPNO(i).Dock = DockStyle.Top
                    COMPNO(i).Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    COMPNO(i).TabIndex = i


                    ReDim Preserve ProductNo(TRUCK_COMP_NUM)
                    ProductNo(i) = New ComboBox
                    'ProductNo(i).DropDownStyle = ComboBoxStyle.DropDownList
                    'ProductNo(i).DrawMode = DrawMode.OwnerDrawFixed

                    ProductNo(i).Height = 15
                    ProductNo(i).Width = GProduct.Width
                    ProductNo(i).Parent = GProduct
                    ProductNo(i).ItemHeight = 13
                    ProductNo(i).Dock = DockStyle.Top
                    ProductNo(i).Parent = GProduct
                    ProductNo(i).SelectedValue = Text
                    ProductNo(i).Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    ProductNo(i).TabIndex = i


                    ReDim Preserve CapacityNo(TRUCK_COMP_NUM)
                    CapacityNo(i) = New TextBox
                    CapacityNo(i).Text = ""
                    CapacityNo(i).Height = 15
                    CapacityNo(i).Width = GroupBox12.Width
                    CapacityNo(i).Parent = GroupBox12
                    CapacityNo(i).Dock = DockStyle.Top
                    CapacityNo(i).TextAlign = HorizontalAlignment.Center
                    CapacityNo(i).Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    CapacityNo(i).ReadOnly = True
                    CapacityNo(i).TabIndex = i

                    ReDim Preserve PRESETNO(TRUCK_COMP_NUM)
                    'Dim handler As TextChangedEventHandler

                    'PRESETNO(i).txtMyTextBox.TextChanged = New System.EventHandler(TextChange)


                    PRESETNO(i) = New TextBox
                    PRESETNO(i).Text = ""
                    PRESETNO(i).Height = 15
                    PRESETNO(i).Width = GPreset.Width
                    PRESETNO(i).TextAlign = HorizontalAlignment.Center
                    PRESETNO(i).Parent = GPreset
                    PRESETNO(i).Dock = DockStyle.Top
                    PRESETNO(i).Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    PRESETNO(i).ForeColor = Color.Blue
                    PRESETNO(i).TabIndex = i
                    'PRESETNO(i).


                    ReDim Preserve BayNo(TRUCK_COMP_NUM)
                    BayNo(i) = New ComboBox
                    BayNo(i).Height = 15
                    BayNo(i).Width = GroupBox11.Width
                    BayNo(i).Parent = GroupBox11
                    BayNo(i).ItemHeight = 13
                    BayNo(i).Dock = DockStyle.Top
                    BayNo(i).Parent = GroupBox11
                    BayNo(i).Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    BayNo(i).TabIndex = i
                    'BayNo(i).Items.Add("")


                    ReDim Preserve Meterno(TRUCK_COMP_NUM)
                    Meterno(i) = New ComboBox
                    Meterno(i).Height = 15
                    Meterno(i).Width = GroupBox10.Width
                    Meterno(i).Parent = GroupBox10
                    Meterno(i).ItemHeight = 13
                    Meterno(i).Dock = DockStyle.Top
                    Meterno(i).Parent = GroupBox10
                    Meterno(i).Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    Meterno(i).TabIndex = i

                Next i

                '''''''''''''CapacityNo''''''''''''''

                For i = 0 To TRUCK_COMP_NUM - 1 'dt.Rows.Count - 1
                    Try
                        CapacityNo(i).Text = dt.Rows(i).Item("T_TRUCKCOMPCAP").ToString()
                    Catch ex As Exception
                        Exit Sub
                    End Try


                Next i


                '''''''ProductNo''''''''''''''

                q = ""
                q = "Select * from T_Product where product_type=1 Order by PRODUCT_CODE"
                da.SelectCommand = New SqlClient.SqlCommand(q, conn)
                For i = 0 To TRUCK_COMP_NUM - 1 'ProductNo.Length - 2
                    Dim T_Product As New DataTable
                    da.Fill(T_Product)
                    With ProductNo(i)
                        .DataSource = T_Product
                        .DisplayMember = "PRODUCT_CODE" 'T_Product.Rows(i).Item("PRODUCT_CODE")
                        .ValueMember = "PRODUCT_CODE" 'T_Product.Rows(i).Item("PRODUCT_CODE")
                        .SelectedIndex = -1
                    End With
                Next



                ''''''''''Island/Bay''''''''''''''
                q = ""
                q = "select max(Batch_Bay) as Batch_Bay  from V_BATCHMETER"
                q &= " GROUP BY Batch_Bay order by Batch_Bay"
                da.SelectCommand = New SqlClient.SqlCommand(q, conn)

                'BayNo(1).Items.Add("")
                For i = 0 To TRUCK_COMP_NUM - 1 ' BayNo.Length - 2
                    Dim Batch_Bay As New DataTable
                    da.Fill(Batch_Bay)
                    With BayNo(i)
                        .Items.Add("")
                        .DataSource = Batch_Bay
                        .DisplayMember = "Batch_Bay"
                        .ValueMember = "Batch_Bay"
                        .SelectedIndex = -1
                    End With
                Next



                ''''''''''''''''''''Meter''''''''''''''

                'Sql = ""
                'Sql = "select * from V_BATCHMETER WHERE Product_code ='" + Product.Text + "'"
                'Sql &= " order by Batch_NAME"

                q = ""
                q = "select * from V_BATCHMETER order by Batch_NAME"
                da.SelectCommand = New SqlClient.SqlCommand(q, conn)

                Try
                    Meterno(1).Items.Add("")
                Catch ex As Exception
                    Meterno(0).Items.Add("")
                End Try


                For i = 0 To TRUCK_COMP_NUM - 1 ' BayNo.Length - 2
                    Dim V_BATCHMETER As New DataTable
                    da.Fill(V_BATCHMETER)
                    With Meterno(i)
                        '.Items.Add("")
                        .DataSource = V_BATCHMETER
                        .DisplayMember = "BATCH_NAME"
                        .ValueMember = "BATCH_NAME"
                        .SelectedIndex = -1
                    End With
                Next

            End If
            conn.Close()
            conn.Dispose()
            da.Dispose()
            dt.Dispose()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Edit.Click
        If MyErrorProvider.CheckAndShowSummaryErrorMessage = False Then
            Exit Sub
        End If

        'If RadioBig.Checked And StatusId.Text = "3" Then
        '    editweightout.PerformClick()
        'End If


        'Cbn1Chang()
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        '        If (LawWeightout.Text = "" Or LawWeightout.Text = "0") And (UpdateWeightOut.Text = "" Or UpdateWeightOut.Text = "0") Then
        '       MessageBox.Show("Please Check weight-Out", "Check", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '      Exit Sub
        '     End If
        Density_TextChanged(sender, e)
        '  EDCustomer_TextChanged(sender, e)
        Dim presetsum As Integer
        For i = 0 To TRUCK_COMP_NUM - 1
            If PRESETNO(i).Text = "" Then
                PRESETNO(i).Text = "0"
            End If
            presetsum = Int(PRESETNO(i).Text) + presetsum
        Next i
        EDPreset.Text = presetsum

        If weightouttime.Text = "" Then
            weightOutdate = Now
            weightouttime.Text = Date.Now.Hour.ToString + ":" + Date.Now.Minute.ToString + ":" + Date.Now.Second.ToString
        End If

        ''''''''''''''Weight Present'''''''''''''''

        Dim sql As String
        sql = "Select * from T_Setting"
        Dim da9 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
        Dim ds9 As New DataSet()
        Dim dt9 As New DataTable
        da9.Fill(dt9)


        If UpdateWeightIn.Text <> 0 And UpdateWeightIn.Text <> "" And WAck.Checked = False Then
            Dim weightinpersent, Weightin1, Weightin2 As Integer
            weightinpersent = (Int(LawWeightout.Text) * (dt9.Rows(0).Item("Percent_weight").ToString)) / 100
            Weightin1 = LawWeightIn.Text + weightinpersent
            Weightin2 = LawWeightIn.Text - weightinpersent
            If (UpdateWeightIn.Text < Weightin2) Or (UpdateWeightIn.Text > Weightin1) Then
                MessageBox.Show("WeightIn % Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If

        Try


            If UpdateWeightOut.Text <> 0 And UpdateWeightOut.Text <> "" And WAck.Checked = False Then
                Dim weightoutpersent, Weightout1 As Single
                If dt9.Rows(0).Item("PERCENTTYPE").ToString = "True" Then
                    weightoutpersent = Math.Abs((Int(EDPreset.Text) * (dt9.Rows(0).Item("Percent_weight").ToString)))
                Else
                    weightoutpersent = Math.Abs((Int(EDPreset.Text)))

                End If
                Weightout1 = Math.Abs(LawWeightout.Text - UpdateWeightOut.Text)
                If (Weightout1 > weightoutpersent) Then
                    MessageBox.Show("Weightout % Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            End If
        Catch ex As Exception

        End Try



        'Try
        '    If LawWeightout.Text <> 0 And LawWeightout.Text <> "" Then
        '        Dim sumBatchWeight As Double = 0
        '        Dim WTotal As Double = 0
        '        Select Case UCase(sAles_UNIT)
        '            Case "L"
        '                '''''
        '                WTotal = Calculate.Text
        '            Case "TO"
        '                ''
        '                WTotal = WeightTotal.Text / 1000
        '            Case Else
        '                '"KG"
        '                WTotal = WeightTotal.Text
        '        End Select

        '        sumBatchWeight = WeightTotal.Text
        '        If TBATCHLOSTTASBindingSource.Count > 0 Then
        '            sumBatchWeight = 0
        '        End If


        '        For i = 0 To TBATCHLOSTTASBindingSource.Count - 1
        '            sumBatchWeight += TBATCHLOSTTASBindingSource(i)("stock_quantity").ToString()
        '        Next

        '        If RadioBig.Checked = True Then
        '            '  If TBATCHLOSTTASBindingSource.Count > 0 Then
        '            If (WTotal <> sumBatchWeight) Or (Order.Text <> sumBatchWeight) Then
        '                MessageBox.Show("Order Quantity,Weight Total,Batch Quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                Exit Sub
        '            End If
        '            'End If
        '        End If
        '        '  If TBATCHLOSTTASBindingSource.Count > 0 Then
        '        If WTotal <> sumBatchWeight Then
        '            MessageBox.Show("Weight Total And Batch Quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '            Exit Sub
        '        End If
        '        ' End If

        '    End If


        'Catch ex As Exception

        'End Try

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If MsgBox("Are you sure to update?", vbYesNo + vbDefaultButton1, "Confirmation") = vbYes Then
            Dim loadtype As String
            If (RadioBig.Checked = True) Then
                loadtype = RadioBig.Text
            Else
                loadtype = RadioLiquid.Text
            End If

            If RadioLiquid.Checked And GroupBatchtable.Enabled Then
                Try
                    TUNITBindingSource.Filter = "CODE='" & UCase(EDSale_Unit.Text) & "'"
                    If TUNITBindingSource.Count > 0 Then

                        If TBATCHLOSTTASBindingSource.Count > 1 Then
                            Dim SumBatch As Single
                            For i = 0 To TBATCHLOSTTASBindingSource.Count - 1
                                SumBatch += TBATCHLOSTTASBindingSource(i)("stock_quantity")
                            Next

                            If Trim(Calculate.Text) <> "0" And Trim(Calculate.Text) <> "" Then
                                If (SumBatch) <> (Calculate.Text / TUNITBindingSource(0)("TOkg")) Then
                                    MessageBox.Show("ปริมาณใน Batch กับ ปริมาณ ที่ชั่งได้ไม่เท่ากัน " & Chr(13) & "กรุณาใส่ค่า quantity ใน Batch Table ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                                    Exit Sub
                                End If

                            Else
                                If (SumBatch) <> (WeightTotal.Text / TUNITBindingSource(0)("TOkg")) Then
                                    MessageBox.Show("ปริมาณใน Batch กับ ปริมาณ ที่ชั่งได้ไม่เท่ากัน " & Chr(13) & "กรุณาใส่ค่า quantity ใน Batch Table ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                                    Exit Sub
                                End If

                            End If

                        Else
                            If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text / TUNITBindingSource(0)("TOkg")
                            If Trim(Calculate.Text) <> "0" And Trim(Calculate.Text) <> "" Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text


                        End If


                    Else
                        ' If Calculate.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text
                        If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text
                        If Trim(Calculate.Text) <> "0" And Trim(Calculate.Text) <> "" Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text

                    End If

                    'Select Case UCase(EDSale_Unit.Text)
                    '    Case "L"
                    '        If Calculate.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text
                    '    Case "TO", "MT"
                    '        If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text / 1000
                    '    Case Else
                    '        If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text
                    'End Select
                Catch ex As Exception

                End Try
            End If

            Dim q As String
            q = ""
            q = "UPDATE T_LOADINGNOTE "
            q &= "SET LOAD_SHIPPER = "
            q &= "'" + (ShipperId.Text) + "',"
            q &= "LOAD_DOfull = "
            q &= "'" + (EDDONo.Text) + "'" + ","
            q &= "LOAD_STARTTIME = "
            q &= "'" + (EDLoadDTime.Text) + "',"
            'q &= " AddnoteDate = "
            'q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
            q &= "LOAD_DRIVER = "
            q &= "'" + (DriverID.Text) + "'" + ","
            q &= "LOAD_PRESET = "
            q &= "'" + (EDPreset.Text) + "'" + ","
            q &= "LOAD_VEHICLE = "
            q &= "'" + (TruckId.Text) + "'" + ","
            q &= "LOAD_CAPACITY = "
            q &= "'" + (EDTruckCapa.Text) + "'" + ","
            q &= "Container = "
            q &= "'" + (Container.Text) + "'" + ","
            q &= "Container_Tare = "
            q &= "'" + (Container_Tare.Text) + "'" + ","
            q &= "Load_customer = "
            '  CustomerID.SelectedIndex = EDCustomer.SelectedIndex
            q &= "'" + (CustomerID.Text) + "'" + ","
            q &= "COA = "
            q &= "'" + (Coa.Text) + "'" + ","
            q &= "BatchLot = "
            'q &= "'" + (Batchlot.Text) + "'" + ","
            q &= "'" + ("") + "'" + ","
            q &= "GI_Date = "
            q &= "'" + String.Format("{0:yyyy-MM-dd}", GiDate.Value) + "'" + ","
            q &= " Raw_Weight_in = "
            q &= "'" + (LawWeightIn.Text) + "'" + ","
            q &= " update_Weight_in = "
            q &= "'" + (UpdateWeightIn.Text) + "'" + ","
            q &= " Raw_Weight_Out = "
            q &= "'" + (LawWeightout.Text) + "'" + ","
            q &= " update_Weight_Out = "
            q &= "'" + (UpdateWeightOut.Text) + "'" + ","
            q &= " Remark = "
            q &= "'" + (Edremark.Text) + "'" + ","
            q &= " AccessCode = "
            q &= "'" + (AccessCode.Text) + "'" + ","
            q &= " ShipmentNo = "
            q &= "'" + (ShipmentNo.Text) + "'" + ","

            q &= " LOAD_SEALCOUNT = "
            q &= "'" + (SealCount.Text) + "'" + ","
            q &= " Packing_Type = "
            q &= "'" + (PackingId.Text) + "'" + ","

            q &= " Packing_Qty = "
            q &= "'" + (Quantity.Text) + "'" + ","
            q &= " Packing_weight = "
            q &= "'" + (PackingWeight.Text) + "'" + ","
            q &= " WeightTotal = "
            q &= "'" + (WeightTotal.Text) + "'" + ","
            q &= " MeterReader = "
            q &= "'" + (MeterRead.Text) + "'" + ","
            q &= " LOAD_TEMP = "
            q &= "'" + (Temp.Text) + "'" + ","
            q &= " Density = "
            q &= "'" + (Density.Text) + "'" + ","
            q &= " LOAD_TANK = "
            q &= "'" + (Tank.Text) + "'" + ","
            q &= " WeightCal = "
            q &= "'" + (Calculate.Text) + "'" + ","

            If WeightIn.Visible = True Then
                Dateedit.Value = Now
                q &= " AddnoteDate = "
                q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
                q &= " weightIn_time= "
                q &= "'" + (Weightintime.Text) + "'" + ","
                q &= " weightIn_date="
                q &= "'" + weightIndate.ToString("yyyy-MM-dd") + "',"
                '  q &= "'" + Now.Date.ToString("yyyy-MM-dd") + "',"
                q &= " LOAD_DID= "
                q &= " (select isnull(Max(LOAD_DID),0)+1 as LOAD_DID from t_LOADINGNOTE where AddnoteDate =CONVERT(varchar(10),  GETDATE(),120) and Weightin_time<>'00:00')  " + ","
            Else
                q &= " AddnoteDate = "
                q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
                q &= "LOAD_DID = "
                q &= "" + (EDLoaddate.Text) + ","

            End If

            If IsNumeric(LawWeightout.Text) And LawWeightout.Text <> "0" Then
                q &= " weightout_time = "
                q &= "'" + (weightouttime.Text) + "'" + ","
                q &= " weightout_Date = "
                q &= "'" + weightOutdate.ToString("yyyy-MM-dd") + "',"
                'q &= "'" + Now.Date.ToString("yyyy-MM-dd") + "',"
            End If
            q &= " load_status = "
            q &= "'" + (StatusId.Text) + "'" + ","
            q &= " ShipmentType = "
            q &= "'" + (Shipment_id.Text) + "'" + ","
            q &= " Load_Type = "
            q &= "'" + (loadtype) + "'" + ","
            q &= " Load_seal = "
            q &= "'" + (Seal_No.Text) + "'" + ","
            q &= " Load_card = "
            q &= "'" + (CBCardNO.Text) + "'" + ","
            q &= " BADGE_NO = "
            q &= "'" + (CBCard.Text) + "'" + ","

            q &= " TANKFARM = "
            q &= "'" + (CBTankfarm.Text) + "'" + ","


            Try
                Dim batchlot As String = ""
                For b = 0 To TBATCHLOSTTASBindingSource.Count - 1
                    If batchlot <> "" Then batchlot = batchlot & "/"
                    batchlot = batchlot & TBATCHLOSTTASBindingSource.Item(b)("BATCH_NO").ToString
                Next
                q &= " BATCH_LOT = "
                q &= "'" + (batchlot) + "'" + ","
            Catch ex As Exception
                q &= " BATCH_LOT = "
                q &= "'" + ("") + "'" + ","
            End Try



            q &= " LoadFinish_date = Getdate() ,"
            q &= "LOAD_TRUCKCOMPANY = "
            q &= "'" + (TruckCompanyId.Text) + "'" + " "
            q &= "WHERE LOAD_ID= "
            q &= "'" + (EDLoadID.Text) + "'  "

            Try
                da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                da.UpdateCommand.ExecuteNonQuery()

                'dELETE oTHOR DO
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Try
                    If EDDONo.Text.Length > 9 Then
                        q = "  delete T_LOADINGNOTECOMPARTMENT where LC_LOAD " &
                            "  in(select LOAD_ID from  V_LOADINGNOTE " &
                            "  where LOAD_DOfull<>'' and LOAD_DOfull='" & EDDONo.Text & "'  and (DO_ITEM ='' or DO_ITEM is null or DO_ITEM ='" & EDDO_item.Text & "') and LOAD_ID<>'" & EDLoadID.Text & "'); " &
                            "  delete T_BATCH_LOT_TAS where LOAD_ID  " &
                            "  in(select LOAD_ID from  V_LOADINGNOTE " &
                            "  where LOAD_DOfull<>'' and LOAD_DOfull='" & EDDONo.Text & "'  and (DO_ITEM ='' or DO_ITEM is null or DO_ITEM ='" & EDDO_item.Text & "') and LOAD_ID<>'" & EDLoadID.Text & "'); " &
                            "  delete T_DO where LOAD_ID  " &
                            "  in(select LOAD_ID from  V_LOADINGNOTE " &
                            "  where LOAD_DOfull<>'' and LOAD_DOfull='" & EDDONo.Text & "'  and (DO_ITEM ='' or DO_ITEM is null or DO_ITEM ='" & EDDO_item.Text & "') and LOAD_ID<>'" & EDLoadID.Text & "'); " &
                            "  delete T_LOADINGNOTE WHERE LOAD_ID " &
                            "  in(select LOAD_ID from  V_LOADINGNOTE " &
                            "  where LOAD_DOfull<>'' and LOAD_DOfull='" & EDDONo.Text & "'  and (DO_ITEM ='' or DO_ITEM is null or DO_ITEM ='" & EDDO_item.Text & "') and LOAD_ID<>'" & EDLoadID.Text & "'); " &
                            "  delete T_BATCH_LOT_TAS where LOAD_ID is null; "
                        Dim cmd As New SqlCommand
                        cmd.Connection = conn
                        cmd.CommandText = q
                        cmd.ExecuteNonQuery()
                        cmd.Dispose()
                    End If
                Catch ex As Exception

                End Try

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ''''Update Weight other
                Try
                    If EDDONo.Text.Length > 9 Then
                        If Convert.ToDouble(LawWeightout.Text) > 0 Then
                            q = " UPDATE T_LOADINGNOTE SET  Raw_Weight_in=" & LawWeightout.Text & "," &
                                 " weightIn_time= " &
                                 "'" + (Weightintime.Text) & "'," &
                                 " weightIn_date=" &
                                 "'" + Now.Date.ToString("yyyy-MM-dd") + "'" &
                                 " WHERE LOAD_DOfull <> '' and " &
                                 " LOAD_DOfull= '" & EDDONo.Text & "' and load_status=1  and LOAD_ID<>'" & EDLoadID.Text & "' and" &
                                 " AddnoteDate = '" & String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'"

                            da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                            da.UpdateCommand.ExecuteNonQuery()
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    If EDDONo.Text.Length > 9 Then
                        q = " UPDATE T_LOADINGNOTE SET  Load_Type='" & loadtype & "' WHERE LOAD_DOfull <> '' and " &
                            " LOAD_DOfull= '" & EDDONo.Text & "'"
                        da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                        da.UpdateCommand.ExecuteNonQuery()
                    End If

                Catch ex As Exception

                End Try
                '''''''''''''''''''''''''''''''''


                q = ""
                q = "UPDATE T_DO "

                ' q &= "SET  sHIP_NO = "
                'q &= "'" + (ShipMent_Type.Text) + "'" + ","
                q &= "SET sHIP_TYPE = "
                q &= "'" + (ShipMent_Type.Text) + "'" + ","

                q &= " tRN_PLAN_POINT = "
                q &= "'" + (TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString) + "'" + ","

                q &= " sALES_ORG = "
                q &= "'" + (TSHIPPERBindingSource.Item(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString) + "'" + ","

                q &= " cREATE_DATE = "
                q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
                q &= " cREATE_TIME = "
                ' q &= "'" + String.Format("{0:hh:mm:ss}", Now) + "'" + ","
                q &= "'" + (Weightintime.Text) + "'" + ","
                ' q &= "'" + Now.ToString + "'" + ","
                'If Container.Text = "" Then
                '    q &= " cONTAINER = "
                '    q &= "'" + (EDTruck.Text) + "'" + ","
                '    q &= " cAR_LINCENSE = "
                '    q &= "'" + (EDTruck.Text) + "'" + ","
                'Else
                '    q &= " cONTAINER = "
                '    q &= "'" + (Container.Text) + "'" + ","
                '    q &= " cAR_LINCENSE = "
                '    q &= "'" + (Container.Text) + "'" + ","
                'End If
                q &= " cONTAINER = "
                q &= "'" + (Container.Text) + "'" + ","
                q &= " cAR_LINCENSE = "
                q &= "'" + (EDTruck.Text) + "'" + ","

                q &= " dRIVER_NAME = "
                q &= "'" + (EDDriver.Text) + "'" + ","
                Try


                    TUNITBindingSource.Filter = "CODE='" & UCase(EDSale_Unit.Text) & "'"
                    If FSAP02_2.SAP_ITEM.BT_RETURN = 1 Then
                        If TUNITBindingSource.Count > 0 Then
                            q &= " ACT_QTY_DELV_SU = "
                            q &= "'" + (Convert.ToDouble(EDPreset.Text) / TUNITBindingSource(0)("TOkg")).ToString + "'" + ","
                        Else
                            q &= " ACT_QTY_DELV_SU = "
                            q &= "'" + (EDPreset.Text) + "'" + ","
                        End If
                    End If
                Catch ex As Exception

                End Try

                'Select Case UCase(EDSale_Unit.Text)


                '    Case "L"
                '        '''''''
                '        q &= " ACT_QTY_DELV_SU = "
                '        q &= "'" + (Cbn10.Text) + "'" + ","

                '    Case "TO", "MT"
                '        q &= " ACT_QTY_DELV_SU = "
                '        q &= "'" + (Convert.ToDouble(Cbn10.Text) / 1000).ToString + "'" + ","
                '    Case Else
                '        ' "KG"
                '        q &= " ACT_QTY_DELV_SU = "
                '        q &= "'" + (Cbn10.Text) + "'" + ","
                'End Select


                '  q &= " ACT_QTY_DELV_SU = "
                '  q &= "'" + (Cbn10.Text) + "'" + ","

                q &= " DO_NO = "
                q &= "'" + (EDDONo.Text) + "'" + ","
                q &= " DO_ITEM = "
                q &= "'" + (EDDO_item.Text) + "'" + ","
                q &= " so_no = "
                q &= "'" + (Sono.Text) + "'" + ","
                q &= " SALES_UNIT = "
                q &= "'" + (EDSale_Unit.Text) + "'" + ","

                q &= " so_item = "
                q &= "'" + Soitem.Text + "',"

                q &= " COA = "
                q &= "'" + Coa.Text + "',"

                q &= " fORWD_AGENT = "
                q &= "'" + (Company_sapcode.Text) + "'" + ","

                Dim weightIn, weightOut As Integer

                If UpdateWeightIn.Text = "0" Or UpdateWeightIn.Text = "" Then
                    weightIn = LawWeightIn.Text
                Else
                    weightIn = UpdateWeightIn.Text
                End If

                If UpdateWeightOut.Text = "0" Or UpdateWeightOut.Text = "" Then
                    weightOut = LawWeightout.Text
                Else
                    weightOut = UpdateWeightOut.Text
                End If


                q &= " wEIGHT_IN = "
                q &= "'" + (weightIn.ToString) + "'" + ","
                q &= " wEIGHT_OUT = "
                q &= "'" + (weightOut.ToString) + "'" + ","
                q &= " sEAL_NO = "
                q &= "'" + (Seal_No.Text) + "'" + ","
                q &= " sHIPPdO_NO = "
                q &= "'" + (EDDONo.Text) + "'" + ","

                'q &= " sHIPP_TYPE = "
                'q &= "'" + (ShipMent_Type.Text) + "'" + ","
                'q &= " sHIPP_COND = "
                'q &= "'" + (ShipMent_Type.Text) + "'" + ","


                q &= " aCT_LOAD_SDATE = "
                q &= "'" + String.Format("{0:yyyy-MM-dd}", Dateedit.Value) + "'" + ","
                q &= " aCT_LOAD_STIME = "
                q &= "'" + (Weightintime.Text) + "'" + ","
                q &= " aCT_LOAD_EDATE = Getdate() , "
                q &= " aCT_LOAD_ETIME = "
                q &= "'" + (weightouttime.Text) + "'" + ","
                q &= " STATUS = "
                q &= "'" + (StatusId.Text) + "'" + ","

                q &= " ACT_GI_DATE = "
                q &= "'" + Dateedit.Value.Date.ToString("yyyy-MM-dd") + "',"
                q &= " ACT_GI_TIME = "
                q &= "'" + String.Format("{0:HH:mm:ss}", Dateedit.Value) + "'" + ","
                'q &= "'" + (weightouttime.Text) + "'" + ","

                q &= " DO_INS_TXT = "
                q &= "'" + (Edremark2.Text) + "'" + ","
                '  q &= " LOAD_PT = "
                '  q &= "'" + (CBLoadingPoint.Text) + "'" + ","
                q &= " LOAD_E_DAT = "
                q &= "'" + weightOutdate.ToString("yyyy-MM-dd") + "',"
                'q &= "'" + Now.Date.ToString("yyyy-MM-dd") + "',"
                q &= " LOAD_E_TIM = "
                q &= "'" + (weightouttime.Text) + "'" + ","

                ' If Me.WeightIn.Visible = True Then
                q &= " CHK_DAT="
                q &= "'" + weightIndate.ToString("yyyy-MM-dd") + "',"
                'q &= "'" + String.Format("{0:yyyy-MM-dd}", Now) + "'" + ","
                q &= " CHK_TIM="
                q &= "'" + (Weightintime.Text) + "'" + ","

                ' End If

                TUNITBindingSource.Filter = "CODE='" & UCase(EDSale_Unit.Text) & "'"
                If TUNITBindingSource.Count > 0 Then
                    If Trim(Calculate.Text) <> "" And Trim(Calculate.Text) <> "0" Then

                        q &= " ACT_QTY_DELV_SKU = "
                        q &= "'" + (Convert.ToDouble(Calculate.Text) / TUNITBindingSource(0)("TOkg")).ToString + "'" + ","

                        q &= " PICK_QTY_SU = "
                        q &= "'" + (Convert.ToDouble(Calculate.Text) / TUNITBindingSource(0)("TOkg")).ToString + "'" + " "

                    Else

                        q &= " ACT_QTY_DELV_SKU = "
                        q &= "'" + (Convert.ToDouble(WeightTotal.Text) / TUNITBindingSource(0)("TOkg")).ToString + "'" + ","

                        q &= " PICK_QTY_SU = "
                        q &= "'" + (Convert.ToDouble(WeightTotal.Text) / TUNITBindingSource(0)("TOkg")).ToString + "'" + " "


                    End If

                Else
                    q &= " ACT_QTY_DELV_SKU = "
                    q &= "'" + (WeightTotal.Text) + "'" + ","

                    q &= " PICK_QTY_SU = "
                    q &= "'" + (WeightTotal.Text) + "'" + " "
                End If

                '  Select Case UCase(EDSale_Unit.Text)

                '    Case "L"
                '        '''''''
                '        q &= " ACT_QTY_DELV_SKU = "
                '        q &= "'" + (Calculate.Text) + "'" + ","

                '        q &= " PICK_QTY_SU = "
                '        q &= "'" + (Calculate.Text) + "'" + " "

                '    Case "TO", "MT"
                '        q &= " ACT_QTY_DELV_SKU = "
                '        q &= "'" + (Convert.ToDouble(WeightTotal.Text) / 1000).ToString + "'" + ","

                '        q &= " PICK_QTY_SU = "
                '        q &= "'" + (Convert.ToDouble(WeightTotal.Text) / 1000).ToString + "'" + " "
                '    Case Else
                '        ' "KG"
                '        q &= " ACT_QTY_DELV_SKU = "
                '        q &= "'" + (WeightTotal.Text) + "'" + ","

                '        q &= " PICK_QTY_SU = "
                '        q &= "'" + (WeightTotal.Text) + "'" + " "
                'End Select




                q &= "WHERE LOAD_ID= "
                q &= "" + (EDLoadID.Text) + "  "

                da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                da.UpdateCommand.ExecuteNonQuery()

                If EDDONo.Text.Length > 9 Then
                    q = "Update T_DO SET "
                    ' q &= " cONTAINER = "
                    ' q &= "'" + (Container.Text) + "'" + ","
                    q &= " cAR_LINCENSE = "
                    q &= "'" + (EDTruck.Text) + "'" + ","
                    q &= " sEAL_NO = "
                    q &= "'" + (Seal_No.Text) + "'" + ","
                    q &= " dELIVERY_INSTRUCTION_TEXT = "
                    q &= "'" + (Edremark.Text) + "'" + ","
                    q &= " DO_INS_TXT = "
                    q &= "'" + (Edremark2.Text) + "'" + ","
                    q &= " dRIVER_NAME = "
                    q &= "'" + (EDDriver.Text) + "'" + ","
                    q &= " ACT_GI_DATE = "
                    q &= "'" + Dateedit.Value.Date.ToString("yyyy-MM-dd") + "',"
                    q &= " ACT_GI_TIME = "
                    q &= "'" + String.Format("{0:HH:mm:ss}", Dateedit.Value) + "'" + ","
                    q &= " COA = "
                    q &= "'" + Coa.Text + "',"
                    q &= " fORWD_AGENT = "
                    q &= "'" + (Company_sapcode.Text) + "'" + ","
                    q &= " LOAD_PT = "
                    q &= "'" + (CBLoadingPoint.Text) + "'" + ""
                    q &= "WHERE DO_NO <> '' and DO_NO= "
                    q &= "'" + (EDDONo.Text) + "'  "

                    da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                    da.UpdateCommand.ExecuteNonQuery()
                End If

                If Adnote_DO.dO_NO <> "" And Adnote_DO.mAT_CODE <> "" Then
                    q = "Update T_DO SET "
                    q &= " pLANT= "
                    q &= "'" + Adnote_DO.pLANT + "',"
                    q &= " bASE_UOM= "
                    q &= "'" + Adnote_DO.bASE_UOM + "',"
                    q &= " sD_DOC_CAT= "
                    q &= "'" + Adnote_DO.sD_DOC_CAT + "',"
                    q &= " mAT_CODE= "
                    q &= "'" + Adnote_DO.mAT_CODE + "',"
                    q &= " sTORAGE_LOC= "
                    q &= "'" + Adnote_DO.sTORAGE_LOC + "',"
                    q &= " bATCH_NO= "
                    q &= "'" + Adnote_DO.bATCH_NO + "',"
                    q &= " sALES_UNIT= "
                    q &= "'" + Adnote_DO.sALES_UNIT + "',"
                    q &= " wEIGHT_UNIT= "
                    q &= "'" + Adnote_DO.wEIGHT_UNIT + "',"
                    q &= " nET_WEIGHT= "
                    q &= "'" + Adnote_DO.nET_WEIGHT + "',"
                    q &= " gROSS_WEIGHT= "
                    q &= "'" + Adnote_DO.gROSS_WEIGHT + "',"
                    q &= " BATCH_INDICATOR= "
                    q &= "'" + Adnote_DO.bATCH_INDICATOR + "',"
                    q &= " hIGHER_LEVEL_ITEM= "
                    q &= "'" + Adnote_DO.hIGHER_LEVEL_ITEM + "' "
                    q &= " WHERE DO_NO <> '' and DO_NO= "
                    q &= "'" + (EDDONo.Text) + "'  "
                    da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                    da.UpdateCommand.ExecuteNonQuery()
                End If


                If EDDONo.Text.Length > 9 Then
                    q = ""
                    q = "UPDATE T_LOADINGNOTE SET "
                    q &= "LOAD_DRIVER = "
                    q &= "'" + (DriverID.Text) + "'" + ","
                    q &= "LOAD_VEHICLE = "
                    q &= "'" + (TruckId.Text) + "'" + ","
                    q &= "Container = "
                    q &= "'" + (Container.Text) + "'" + ","
                    q &= "Container_Tare = "
                    q &= "'" + (Container_Tare.Text) + "'" + ","

                    q &= "Load_customer = "
                    q &= "'" + (CustomerID.Text) + "'" + ","
                    q &= "COA = "
                    q &= "'" + (Coa.Text) + "'" + ","
                    q &= " Remark = "
                    q &= "'" + (Edremark.Text) + "'" + ","
                    q &= " LOAD_SEALCOUNT = "
                    q &= "'" + (SealCount.Text) + "'" + ","
                    q &= " Load_seal = "
                    q &= "'" + (Seal_No.Text) + "'" + ","
                    q &= " TANKFARM = "
                    q &= "'" + (CBTankfarm.Text) + "'" + ""
                    q &= "WHERE LOAD_DOFULL <> '' and  LOAD_DOFULL= "
                    q &= "'" + (EDDONo.Text) + "'  "

                    da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                    da.UpdateCommand.ExecuteNonQuery()
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message + " Unable to add information, please check" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try

            'Update T_truck
            Try
                q = "Update T_truck set truck_driver='" & DriverID.Text & "', TRUCK_COMPANY='" & TruckCompanyId.Text _
                    & "' where truck_number='" & EDTruck.Text & "'"
                da.InsertCommand = New SqlClient.SqlCommand(q, conn)
                da.InsertCommand.ExecuteNonQuery()
                Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)

            Catch ex As Exception
                ' q = ""
                ' MessageBox.Show("Wrong information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '  Cursor = Cursors.Default
                '  Exit Sub
            End Try
            ''''''End Update T_truck''''''''''''''

            For r = 0 To TRUCK_COMP_NUM - 1

                Dim id As String
                q = ""
                q = "UPDATE T_LOADINGNOTECOMPARTMENT "
                q &= "SET LC_COMPARTMENT = "
                q &= "'" + (COMPNO(r).Text) + "'" + ","

                id = ""
                id = "Select LC_ID from T_LOADINGNOTECOMPARTMENT where lc_load="
                id &= "'" + EDLoadID.Text + "'"
                id &= " and lc_compartment = '" + COMPNO(r).Text + "'"
                id &= " and Lc_Status=99 "
                Dim da5 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(id, conn)
                Dim ds5 As New DataSet()
                Dim dt5 As New DataTable
                Try
                    da5.Fill(dt5)
                Catch ex As Exception
                End Try

                If dt5.Rows.Count > 0 Then
                    q &= " lc_status =1 , "
                End If

                q &= " LC_PRO = "

                id = ""
                id = "Select * from T_Product where product_code="
                id &= "'" + ProductNo(r).Text + "'"
                Dim da6 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(id, conn)
                Dim ds6 As New DataSet()
                Dim dt6 As New DataTable
                da6.Fill(dt6)

                If ProductNo(r).Text <> "" Then
                    q &= "'" + (dt6.Rows(0).Item("ID").ToString) + "'" + ","
                    q &= " LC_CAPACITY = "
                    q &= "'" + (CapacityNo(r).Text) + "'" + ","
                    q &= " LC_PRESET = "
                    q &= "'" + (PRESETNO(r).Text) + "'" + ","
                    If PRESETNO(r).Text = "" Then
                        q &= " LC_STATUS = 99 , "
                    End If

                    If BayNo(r).Text = "" Then
                        q &= " LC_BAY = 5 ,"
                    Else
                        q &= " LC_BAY = "
                        q &= "'" + (BayNo(r).Text) + "'" + ","
                    End If

                    'q &= " LC_BAY = "
                    'q &= "'" + (BayNo(r).Text) + "'" + ","

                    id = ""
                    id = "select * from T_BATCHMETER where Batch_Name ="
                    id &= "'" + Meterno(r).Text + "'"
                    Dim da7 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(id, conn)
                    Dim ds7 As New DataSet()
                    Dim dt7 As New DataTable
                    da7.Fill(dt7)
                    q &= " LC_METER =  "
                    Try
                        q &= "'" + (dt7.Rows(0).Item("Batch_Number").ToString) + "'" + ","
                    Catch ex As Exception
                        q &= "'0', "
                    End Try


                    q &= " LC_SEAL =  "
                    q &= "'" + (Seal_No.Text) + "' "
                    q &= "WHERE LC_LOAD= "
                    q &= EDLoadID.Text + " "
                    q &= "AND LC_COMPARTMENT= "
                    q &= "'" + (COMPNO(r).Text) + "" + "'"

                    Try
                        da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                        da.UpdateCommand.ExecuteNonQuery()
                        ' MessageBox.Show("Update data Successfully, ok", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message + " Unable to add information, please check" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End Try

                Else
                    q = ""
                    q = "UPDATE T_LOADINGNOTECOMPARTMENT set "
                    q &= " LC_PRO = 0,"
                    q &= " LC_Status =99,"
                    q &= " LC_preset =0,"
                    q &= " LC_Meter =0,"
                    q &= " LC_Island =0,"
                    q &= " LC_Seal = '',"
                    q &= " LC_Customer =0"
                    q &= " WHERE LC_LOAD= "
                    q &= EDLoadID.Text + " "
                    q &= "AND LC_COMPARTMENT= "
                    q &= "'" + (COMPNO(r).Text) + "" + "'"

                    Try
                        da.UpdateCommand = New SqlClient.SqlCommand(q, conn)
                        da.UpdateCommand.ExecuteNonQuery()
                        ' MessageBox.Show("Update data Successfully, ok", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message + " Unable to add information, please check" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End Try
                End If

            Next

            For i = 0 To TBATCHLOSTTASBindingSource.Count - 1
                TBATCHLOSTTASBindingSource(i)("STATUS") = StatusId.Text
            Next

            Savebatch(TBATCHLOSTTASBindingSource.DataSource, EDDONo.Text, EDLoadID.Text)
            'MessageBox.Show("Update data Successfully", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'LBStatus.Text = "Update data successfully"
            Main.CEvent("USER :" + Main.U_NAME + " Update Advisenote Reference: " + EDREf.Text)

            If IsNumeric(LawWeightout.Text) And LawWeightout.Text <> "0" And UCase(Status.Text) = UCase("Finished") Then
                If MsgBox("Do you want to Print Weight Ticket?", vbYesNo + vbDefaultButton1, "Confirmation") = vbYes Then
                    weightoutprint_Click(sender, e)
                    'weightoutprint.PerformClick()
                    '    Button5_Click_2(sender, e)
                End If
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim qsql As String
                qsql = "select count(LOAD_ID) as LOAD_ID from T_LOADINGNOTE WHERE LOAD_STATUS=1 and LOAD_DOfull<> '' and  LOAD_DOfull='" & EDDONo.Text & "'"

                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(qsql, conn)
                Dim dt As New DataTable
                da.Fill(dt)

                If dt.Rows(0)(0) > 0 Or EDDONo.Text = "" Then
                    BTEDIT_SAP.Enabled = False
                Else
                    BTEDIT_SAP.Enabled = True
                    If MsgBox("Do you want to Post DO to SAP ?", vbYesNo + vbDefaultButton1, "Confirmation") = vbYes Then
                        Button5_Click(sender, e)
                    End If
                End If
            Catch ex As Exception

            End Try

            Bcancel_Click(sender, e)
        End If
        conn.Close()
        conn.Dispose()
    End Sub

    Private Sub EDTruck_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        EDTruck_TextChanged(sender, e)
    End Sub

    Private Sub Bcancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bcancel.Click
        Try
            TextBox2.Text = 0
            MyErrorProvider.ClearAllErrorMessages()
            Try
                'For i = ProductNo.Length To 0 Step -1
                For i = 0 To ProductNo.Length
                    ProductNo(i).Dispose()
                    CapacityNo(i).Dispose()
                    BayNo(i).Dispose()
                    Meterno(i).Dispose()
                    PRESETNO(i).Dispose()
                    COMPNO(i).Dispose()
                Next
            Catch ex As Exception

            End Try
        Finally
            ReDim ProductNo(0)
            ReDim CapacityNo(0)
            ReDim BayNo(0)
            ReDim Meterno(0)
            ReDim PRESETNO(0)
            ReDim COMPNO(0)
        End Try
        ClearData()

        Adddata.Enabled = CheckAddPermisstion(Me.Tag)
        EditData.Enabled = CheckEditPermisstion(Me.Tag)
        weightoutprint.Enabled = CheckPrintPermisstion(Me.Tag)
        BtWorkPrint.Enabled = CheckPrintPermisstion(Me.Tag)
        'Advisenote_Shown(sender, e)
    End Sub

    Private Sub Adddata_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Adddata.Click
        zDoNotDoEvent = True
        Me.SuspendLayout()
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try

            ''''''''''''''''Set Group En or Dis''''''''
            AddNew = 1
            conn.Open()

            CBCardNO.DataSource = VCARDFREEBindingSource
            Me.V_CARDFREETableAdapter.Fill(Me.FPTDataSet.V_CARDFREE)
            Dim q, s_day, s_month, s_year As String
            DBGridAddnote.Enabled = False
            GroupPort.Visible = True
            GroupPort.Enabled = True
            GDetail.Enabled = True
            GroupBatchtable.Enabled = True
            GProduct.Enabled = True
            GPreset.Enabled = True
            CbProname.Enabled = True
            Product.Enabled = True
            GroupBtadd.Enabled = False
            GroupGeneral.Enabled = True
            BTIMPORT_SAP.Visible = True
            BTEDIT_SAP.Visible = False
            Bsave.Visible = True
            Edit.Visible = False
            WeightIn.Visible = True
            WeightOut.Visible = False
            s_day = Date.Now.Day
            s_month = Date.Now.Month
            s_year = Date.Now.Year
            GroupAdnote.Enabled = True
            NoteAddNew()
            CBLoadingPoint.SelectedIndex = -1
            CBCardNO.SelectedIndex = 0
            CBCard.SelectedIndex = 0
            EDLoadDTime.Text = Format(Date.Now.Date, "dd/MM/yyyy")
            Dateedit.Value = Now
            GiDate.Value = Now
            RadioLiquid.Checked = False
            RadioBig.Checked = True

            Try

                Adnote_DO = FSAP02_2.SAP_ITEM '' Order DO
                Adnote_ALLDO = FSAP02_2.SAP_ALLITEM
                Adnote_ALLBATCH = FSAP02_2.SAP_BATCH_ITEM

            Catch ex As Exception

            End Try


            Try
                CBLoadingPoint.SelectedIndex = -1
                'CBLoadingPoint.SelectedIndex = TLOBindingSource.Find("PLANT_LOCATION", TSETTINGBindingSource(0)("PLANT").ToString)
                'TLOBindingSource.RemoveFilter()
            Catch ex As Exception
                'TLOBindingSource.RemoveFilter()
            End Try
            ''''''''''''''''''END''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''
            Try
                ''''''''''''Default ShipMent and Sales ORG''''''''''''''''''''''''''
                ' 
                Status.SelectedIndex = 0
                StatusId.SelectedIndex = Status.SelectedIndex
                Packing.SelectedIndex = Packing.FindStringExact("BULK")
                Dim objIniFile As New IniFile(Application.StartupPath & "\config.ini")
                TSHIPPERBindingSource.Position = TSHIPPERBindingSource.Find("SP_SAPCODE", TSETTINGBindingSource(0)("SALESORG").ToString())
                EDShipper.SelectedIndex = TSHIPPERBindingSource.Position
                ShipperId.SelectedIndex = EDShipper.SelectedIndex
                ShipMent_Type.SelectedIndex = ShipMent_Type.FindStringExact(TSETTINGBindingSource(0)("ShipMentType").ToString())


                '''''''''''''''''''''''''''''''''''''''
            Catch ex As Exception

            End Try
            'End Default ShipMent and Sales ORG

            ''Select Max load ID,Reference
            Try
                q = ""
                q = "select isnull(max(Load_id),0)+1 as load_id ,isnull(max(Reference),0)+1 as Reference  from T_loadingnote"
                Dim da6 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                'Dim ds6 As New DataSet()
                Dim dt6 As New DataTable
                da6.Fill(dt6)
                EDLoadID.Text = dt6.Rows(0).Item("load_id").ToString()
                EDREf.Text = dt6.Rows(0).Item("Reference").ToString()
                AccessCode.Text = Strings.Right("000000" & dt6.Rows(0).Item("Reference").ToString, 6)
                da6.Dispose()
                'ds6.Dispose()
                dt6.Dispose()
            Catch ex As Exception

            End Try
            ''end Select Max load ID,Reference
            Try
                ' select Max Date load ID  
                Dim yearthai As String
                yearthai = Str(Int(s_year + 543))
                q = ""
                q = "select  isnull(max(LOAD_DID),0)+1 as LOAD_DID from T_LOADINGNOTE "
                q &= " where LOAD_DAY=" + s_day + " and LOAD_MONTH =" + s_month + " and LOAD_YEAR=" + yearthai + ""
                Dim da7 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                'Dim ds7 As New DataSet()
                Dim dt7 As New DataTable
                da7.Fill(dt7)
                EDLoaddate.Text = dt7.Rows(0).Item("Load_did").ToString()
                ' _LOAD_DATE = Now
                'BARCODE1D(_LOAD_DATE, EDLoaddate.Text)
                da7.Dispose()
                ' ds7.Dispose()
                dt7.Dispose()
            Catch ex As Exception

            End Try ' end select Max Date load ID  

            Adnote_DO = Nothing
            Adnote_ALLBATCH = Nothing
            Adnote_ALLDO = Nothing
            SelectBatch(EDLoadID.Text)
            EDTruck.Focus()
            conn.Close()
            conn.Dispose()
        Catch ex As Exception
        End Try
        PTruck.Visible = False
        zDoNotDoEvent = False
        conn.Close()
        conn.Dispose()
        Me.ResumeLayout()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTEDIT_SAP.Click
        Dim sumBatchWeight As Double = 0
        Dim WTotal As Double = 0
        Dim QrOrder As Double = 0
        Dim NonBWTotal As Double = 0
        Dim NonBQrOrder As Double = 0

        Try
            Dim conn As SqlClient.SqlConnection = GetConnection()
            Dim q As String
            Dim Ds As New DataSet
            ''''''''''''''''Set Group En or Dis''''''''
            conn.Open()
            q = ""
            q = "Select BATCH_NO,STOR  from T_BATCH_LOT_TAS WHERE DO_NO='" & EDDONo.Text & "'"
            da.SelectCommand = New SqlClient.SqlCommand(q, conn)
            da.Fill(Ds, "T_BATCH_LOT_TAS")

            If Ds.Tables("T_BATCH_LOT_TAS").Rows.Count > 0 Then
                For i = 0 To Ds.Tables("T_BATCH_LOT_TAS").Rows.Count - 1
                    If Trim(Ds.Tables("T_BATCH_LOT_TAS").Rows(i)("BATCH_NO").ToString()) = "" Then
                        MessageBox.Show("Batch Number Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If

                    If Trim(Ds.Tables("T_BATCH_LOT_TAS").Rows(i)("STOR").ToString()) = "" Then
                        MessageBox.Show("batch Location Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                Next
            Else
                If BATCH_INDICATOR = "X" Then
                    MessageBox.Show("Batch Table Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            End If

            conn.Close()
            conn.Dispose()
            Ds.Dispose()

        Catch ex As Exception

        End Try


        If TBATCHLOSTTASBindingSource.Count > 0 Then
            For i = 0 To TBATCHLOSTTASBindingSource.Count - 1
                If Trim(TBATCHLOSTTASBindingSource(i)("BATCH_NO").ToString()) = "" Then
                    MessageBox.Show("Batch Number Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                If Trim(TBATCHLOSTTASBindingSource(i)("STOR").ToString()) = "" Then
                    MessageBox.Show("batch Location Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Next
        Else
            If BATCH_INDICATOR = "X" Then
                MessageBox.Show("Batch Table Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If



        If WAck.Checked = False Then
            Try
                Dim conn As SqlClient.SqlConnection = GetConnection()
                Dim q As String
                Dim Ds, Ds2 As New DataSet
                ''''''''''''''''Set Group En or Dis''''''''
                conn.Open()
                q = ""
                q = "Select isnull(sum(cast(aCT_QTY_DELV_SU as Float)),0) as aCT_QTY_DELV_SU,isnull(sum(cast(PICK_QTY_SU as Float)),0) as PICK_QTY_SU from T_DO WHERE BATCH_INDICATOR = ''and  DO_NO='" & EDDONo.Text & "'"
                da.SelectCommand = New SqlClient.SqlCommand(q, conn)
                da.Fill(Ds2, "T_DO")
                NonBQrOrder = Ds2.Tables("T_DO").Rows(0)("aCT_QTY_DELV_SU").ToString
                NonBWTotal = Ds2.Tables("T_DO").Rows(0)("PICK_QTY_SU").ToString

                q = ""
                q = "Select isnull(sum(cast(aCT_QTY_DELV_SU as Float)),0) as aCT_QTY_DELV_SU,isnull(sum(cast(PICK_QTY_SU as Float)),0) as PICK_QTY_SU from T_DO WHERE BATCH_INDICATOR<>''and  DO_NO='" & EDDONo.Text & "'"
                da.SelectCommand = New SqlClient.SqlCommand(q, conn)
                da.Fill(Ds, "T_DO")
                q = ""
                q = "Select count(ID) as CID, isnull(sum(cast(stock_quantity as Float)),0) as stock_quantity from T_BATCH_LOT_TAS WHERE  DO_NO='" & EDDONo.Text & "'"
                da.SelectCommand = New SqlClient.SqlCommand(q, conn)
                da.Fill(Ds, "T_BATCH_LOT_TAS")

                QrOrder = Ds.Tables("T_DO").Rows(0)("aCT_QTY_DELV_SU").ToString
                WTotal = Ds.Tables("T_DO").Rows(0)("PICK_QTY_SU").ToString
                If Ds.Tables("T_BATCH_LOT_TAS").Rows(0)("CID") > 0 Then
                    sumBatchWeight = Ds.Tables("T_BATCH_LOT_TAS").Rows(0)("stock_quantity").ToString
                Else
                    sumBatchWeight = WTotal
                End If

                Ds.Dispose()
                conn.Close()
                conn.Dispose()
            Catch ex As Exception
                QrOrder = Order.Text
                WTotal = WeightTotal.Text
                sumBatchWeight = WeightTotal.Text
            End Try


            If NonBQrOrder > 0 Then
                Dim weightinpersent As Single = 0
                Dim weightinpersentD As Single = 0
                Dim Trolan As Single
                If RadioBig.Checked = True Then
                    Trolan = 0
                Else
                    Trolan = TSETTINGBindingSource(0)("Percent_weightLQ").ToString
                End If


                If TSETTINGBindingSource(0)("PERCENTTYPE").ToString = "True" Then
                    weightinpersent = Math.Abs(NonBQrOrder) + (NonBQrOrder * Trolan)
                    weightinpersentD = Math.Abs(NonBQrOrder) - (NonBQrOrder * Trolan)
                Else
                    weightinpersent = Math.Abs(NonBQrOrder) + (Trolan)
                    weightinpersentD = Math.Abs(NonBQrOrder) - (Trolan)
                End If

                'weightinpersent = Math.Abs(100 - ((NonBQrOrder / NonBWTotal) * 100))


                If (NonBWTotal < weightinpersentD) Or (NonBWTotal > weightinpersent) Then
                    MessageBox.Show("Tolerance Weight Product Not accept , please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            End If

            If QrOrder > 0 Then
                Dim weightinpersent As Single = 0
                Dim weightinpersentD As Single = 0
                Dim Trolan As Single
                'weightinpersent = Math.Abs(100 - ((QrOrder / WTotal) * 100))
                If RadioBig.Checked = True Then
                    Trolan = 0
                Else
                    Trolan = TSETTINGBindingSource(0)("Percent_weightLQ").ToString
                End If


                If TSETTINGBindingSource(0)("PERCENTTYPE").ToString = "True" Then
                    weightinpersent = Math.Abs(QrOrder) + (QrOrder * Trolan)
                    weightinpersentD = Math.Abs(QrOrder) - (QrOrder * Trolan)
                Else
                    weightinpersent = Math.Abs(QrOrder) + (Trolan)
                    weightinpersentD = Math.Abs(QrOrder) - (Trolan)
                End If



                If (WTotal < weightinpersentD) Or (WTotal > weightinpersent) Then
                    MessageBox.Show("Tolerance Weight Product Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If



                'weightinpersent = Math.Abs(100 - ((QrOrder / sumBatchWeight) * 100))
                If RadioBig.Checked = True Then
                    Trolan = 0
                Else
                    Trolan = TSETTINGBindingSource(0)("Percent_weightLQ").ToString
                End If
                weightinpersentD = 0
                If TSETTINGBindingSource(0)("PERCENTTYPE").ToString = "True" Then
                    weightinpersent = Math.Abs(QrOrder) + (QrOrder * Trolan)
                    weightinpersentD = Math.Abs(QrOrder) - (QrOrder * Trolan)
                Else
                    weightinpersent = Math.Abs(QrOrder) + (Trolan)
                    weightinpersentD = Math.Abs(QrOrder) - (Trolan)
                End If

                If (sumBatchWeight < weightinpersentD) Or (sumBatchWeight > weightinpersent) Then
                    MessageBox.Show("Tolerance Batch quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            End If
        End If
        '    If NonBQrOrder > 0 Then
        '        Dim weightinpersent As Single
        '        Dim Trolan As Single
        '        If RadioBig.Checked = True Then
        '            Trolan = 0
        '        Else
        '            Trolan = TSETTINGBindingSource(0)("Percent_weightLQ").ToString
        '        End If


        '        If TSETTINGBindingSource(0)("PERCENTTYPE").ToString = "True" Then
        '            weightinpersent = Math.Abs(NonBQrOrder) + (NonBQrOrder * Trolan)
        '        Else
        '            weightinpersent = Math.Abs(NonBQrOrder) + (Trolan)
        '        End If

        '        'weightinpersent = Math.Abs(100 - ((NonBQrOrder / NonBWTotal) * 100))




        '        If NonBWTotal > weightinpersent Then
        '            MessageBox.Show("Tolerance Weight Product Not accept , please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '            Exit Sub
        '        End If
        '    End If

        '    If QrOrder > 0 Then
        '        Dim weightinpersent As Single
        '        Dim Trolan As Single
        '        'weightinpersent = Math.Abs(100 - ((QrOrder / WTotal) * 100))
        '        If RadioBig.Checked = True Then
        '            Trolan = 0
        '        Else
        '            Trolan = TSETTINGBindingSource(0)("Percent_weightLQ").ToString
        '        End If

        '        If TSETTINGBindingSource(0)("PERCENTTYPE").ToString = "True" Then
        '            weightinpersent = Math.Abs(QrOrder) + (QrOrder * Trolan)
        '        Else
        '            weightinpersent = Math.Abs(QrOrder) + (Trolan)
        '        End If



        '        If WTotal > weightinpersent Then
        '            MessageBox.Show("Tolerance Weight Product Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '            Exit Sub
        '        End If



        '        'weightinpersent = Math.Abs(100 - ((QrOrder / sumBatchWeight) * 100))
        '        If RadioBig.Checked = True Then
        '            Trolan = 0
        '        Else
        '            Trolan = TSETTINGBindingSource(0)("Percent_weightLQ").ToString
        '        End If

        '        If TSETTINGBindingSource(0)("PERCENTTYPE").ToString = "True" Then
        '            weightinpersent = Math.Abs(QrOrder) + (QrOrder * Trolan)
        '        Else
        '            weightinpersent = Math.Abs(QrOrder) + (Trolan)
        '        End If

        '        If sumBatchWeight > weightinpersent Then
        '            MessageBox.Show("Tolerance Batch quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '            Exit Sub
        '        End If
        '    End If
        'End If


        Try
            ''If LawWeightout.Text <> 0 And LawWeightout.Text <> "" Then

            ''    TUNITBindingSource.Filter = "CODE='" & UCase(EDSale_Unit.Text) & "'"
            ''    If TUNITBindingSource.Count > 0 Then

            ''        WTotal = WeightTotal.Text / TUNITBindingSource(0)("TOkg")
            ''        QrOrder = Order.Text / TUNITBindingSource(0)("TOkg")
            ''        sumBatchWeight = WeightTotal.Text / TUNITBindingSource(0)("TOkg")

            ''    Else
            ''        WTotal = WeightTotal.Text
            ''        sumBatchWeight = WeightTotal.Text
            ''    End If
            ''    'Select Case UCase(EDSale_Unit.Text)
            ''    '    Case "L"
            ''    '        '''''
            ''    '        WTotal = Calculate.Text
            ''    '        sumBatchWeight = Calculate.Text
            ''    '    Case "TO", "MT"
            ''    '        ''
            ''    '        WTotal = WeightTotal.Text / 1000
            ''    '        QrOrder = Order.Text / 1000
            ''    '        sumBatchWeight = WeightTotal.Text / 1000
            ''    '    Case Else
            ''    '        '"KG"
            ''    '        WTotal = WeightTotal.Text
            ''    '        sumBatchWeight = WeightTotal.Text
            ''    'End Select


            ''    '                sumBatchWeight = WeightTotal.Text

            ''    If TBATCHLOSTTASBindingSource.Count > 0 Then
            ''        sumBatchWeight = 0
            ''    End If


            ''    For i = 0 To TBATCHLOSTTASBindingSource.Count - 1
            ''        sumBatchWeight += TBATCHLOSTTASBindingSource(i)("stock_quantity").ToString()
            ''    Next

            'If RadioBig.Checked = True Or GroupBatchtable.Enabled = False Then
            '    '  If TBATCHLOSTTASBindingSource.Count > 0 Then

            '    If NonBWTotal <> NonBQrOrder Then
            '        MessageBox.Show("Weight Total And Order Quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            '        Exit Sub
            '    End If

            '    If (WTotal <> sumBatchWeight) Or (QrOrder <> sumBatchWeight) Then
            '        MessageBox.Show("Order Quantity,Weight Total,Batch Quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            '        Exit Sub
            '    End If
            '    'End If
            '    '  If TBATCHLOSTTASBindingSource.Count > 0 Then

            '    ' End If
            'End If

            'If WTotal <> sumBatchWeight Then
            '    MessageBox.Show("Weight Total And Batch Quantity Not accept, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            '    Exit Sub
            'End If
            ''  End If


        Catch ex As Exception

        End Try

        ''''''''


        FSAP03_2.DO_NO = EDDONo.Text
        FSAP03_2.ShowDialog()
        'load_id = EDLoadID.Text
        'DoDetail.TDOBindingSource.Filter = "Load_id=" & load_id
        'DoDetail.ShowDialog()
    End Sub

    Sub ClearData()
        Adnote_DO = Nothing
        Adnote_ALLDO = Nothing
        zDoNotDoEvent = True
        AddNew = 0
        BTOverrideWight.Visible = False
        WAck.Checked = False
        BtUnload.Enabled = True
        DBGridAddnote.Enabled = True
        GDetail.Enabled = False
        GroupBatchtable.Enabled = False
        GProduct.Enabled = False
        GPreset.Enabled = False
        CbProname.Enabled = False
        Product.Enabled = False
        EDTruck.Text = ""
        EDForword.Text = ""
        MeterGrid.DataSource = Nothing
        BindingNavigator1.BindingSource = Nothing
        EDDONo.Enabled = False
        EDDO_item.Enabled = False
        Sono.Enabled = False
        Soitem.Enabled = False
        '  EDCustomer.Enabled = False
        EDShipper.Enabled = False
        EDSale_Unit.Text = ""
        EDShipper.Text = ""
        EDCustomer.Text = ""
        Product.Text = ""
        Status.Text = ""
        Packing.Text = ""
        Bay.Text = ""
        Meter.Text = ""
        ShipMent_Type.Text = ""
        RadioBig.Checked = False
        RadioLiquid.Checked = False
        CBCardNO.SelectedIndex = -1
        CBCard.SelectedIndex = -1
        CBTankfarm.SelectedIndex = -1
        CBLoadingPoint.SelectedIndex = -1
        ' CBLoadingPoint.Enabled = False
        Order.Enabled = True
        'GPreset.Enabled = True
        Dateedit.Enabled = True
        'EDTruck.Items.Clear()
        'EDForword.Items.Clear()
        'EDDriver.Items.Clear()
        'EDShipper.Items.Clear()
        'EDCustomer.Items.Clear()
        'Product.Items.Clear()
        'Status.Items.Clear()
        'Packing.Items.Clear()
        'Bay.Items.Clear()
        'Meter.Items.Clear()
        'ShipMent_Type.Items.Clear()
        EDDO_item.Text = ""
        EDDONo.Text = ""
        Coa.Text = ""
        Container.Text = ""
        Container_Tare.Text = ""
        'Batchlot.Text = ""
        Seal_No.Text = ""
        EDREf.Text = ""
        EDTruckCapa.Text = ""
        EDPreset.Text = ""
        LawWeightIn.Text = ""
        UpdateWeightIn.Text = ""
        LawWeightout.Text = ""
        UpdateWeightOut.Text = ""
        AccessCode.Text = ""
        ShipmentNo.Text = ""
        EDSaleQtr.Text = ""

        SealCount.Text = ""
        Quantity.Text = ""
        PackingWeight.Text = ""
        Edremark.Text = ""
        EDLoaddate.Text = ""
        WeightTotal.Text = ""
        MeterRead.Text = ""
        Temp.Text = ""
        Density.Text = ""
        Tank.Text = ""
        Calculate.Text = ""
        Order.Text = ""
        Weightintime.Text = ""
        weightouttime.Text = ""
        Sono.Text = ""
        Soitem.Text = ""


        DriverID.SelectedItem = ""
        EDDriver.SelectedItem = ""
        EDDriver.Text = ""

        GroupGeneral.Enabled = False
        GroupAdnote.Enabled = True
        GroupGeneral.Visible = True
        'GroupBox2.Visible = False
        GroupPort.Enabled = False
        GroupBtadd.Visible = True
        GroupBtadd.Enabled = True
        UpdateWeightIn.Enabled = False
        UpdateWeightOut.Enabled = False
        LawWeightIn.Enabled = False
        'weightoutprint.Enabled = False


        EDTruck.Text = ""
        EDForword.Text = ""
        EDShipper.Text = ""
        EDCustomer.Text = ""
        ShipMent_Type.Text = ""
        Status.Text = ""
        Packing.Text = ""
        Edremark2.Text = ""
        TCustomerBindingSource.RemoveFilter()



        Timer1.Enabled = False
        RadPanel7.Visible = False
        RadPanel8.Visible = False
        'Dim q As String
        'q = ""
        'q = "Select max(id) as ID,Max(TRUCK_NUMBER) as TRUCK_NUMBER   from V_TRUCK2 group by TRUCK_NUMBER order by TRUCK_NUMBER"
        'Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
        'Dim ds1 As New DataSet()
        'Dim dt1 As New DataTable
        'Dim numrow, indexx As Integer
        'da1.Fill(dt1)
        'numrow = dt1.Rows.Count
        'EDTruck.Items.Clear()
        'TruckId.Items.Clear()
        'EDTruck.Items.Add("")
        'TruckId.Items.Add("")
        'For indexx = 0 To numrow - 1
        '    TruckId.Items.Add(dt1.Rows(indexx).Item("ID").ToString())
        '    EDTruck.Items.Add(dt1.Rows(indexx).Item("TRUCK_NUMBER").ToString())
        'Next indexx



        'Try
        '    Me.V_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_LOADINGNOTE)
        'Catch ex As Exception

        'End Try
        '  Me.V_LOADINGNOTETableAdapter.Fill(Me.FPTDataSet.V_LOADINGNOTE)
        SelectVLoadingNote()
        PTruck.Visible = True
        zDoNotDoEvent = False
    End Sub

    Private Sub EditData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditData.Click
        ''Check Error Message''''
        zDoNotDoEvent = True
        Me.SuspendLayout()
        AddNew = 2

        'Try

        '    Adnote_DO = FSAP02_2.SAP_ITEM '' Order DO
        '    Adnote_ALLDO = FSAP02_2.SAP_ALLITEM
        '    Adnote_ALLBATCH = FSAP02_2.SAP_BATCH_ITEM

        'Catch ex As Exception

        'End Try

        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try
            conn.Open()
            CBCardNO.DataSource = VCARDBindingSource
            Me.V_CARDTableAdapter.Fill(Me.FPTDataSet.V_CARD)

            If DBGridAddnote.SelectedRows.Count < 0 Then
                MessageBox.Show("Please Select Data, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If


            If V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("Do_status").ToString = 1 Then

                'If MsgBox(MsgString, vbYesNo + vbDefaultButton2, "Confirmation") = vbNo Then
                '    Exit Sub
                'End If
                Dim MsgString As String = "D/O " & V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_DOfull").ToString _
                                          & " ได้มีการ Post do เรียนร้อยแล้ว ต้องการแก้ไขข้อมูลใช่หรือไม่ ?"

                If MessageBox.Show(MsgString, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbNo Then
                    Exit Sub
                End If

            End If


            Dim load_id As String
            Try
                load_id = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_ID").ToString()
            Catch ex As Exception
                MessageBox.Show(ex.Message + " Please Select Data, please check", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            ''EndCheck Error Message''''
            'Set En or dis Control
            Try
                NoteAddNew()
                DBGridAddnote.Enabled = False
                GDetail.Enabled = True
                WeightIn.Visible = False
                WeightOut.Visible = True
                Bsave.Visible = False
                Edit.Visible = True
                GroupBatchtable.Enabled = True
                GProduct.Enabled = True
                GPreset.Enabled = True
                CbProname.Enabled = True
                Product.Enabled = True
                GroupBtadd.Enabled = False
                GroupPort.Visible = True
                GroupPort.Enabled = True
                GroupAdnote.Visible = True
                GroupAdnote.Enabled = True
                GroupGeneral.Visible = True
                BTIMPORT_SAP.Visible = True
                BTEDIT_SAP.Visible = True
                GroupGeneral.Enabled = True
                BTOverrideWight.Visible = True
                EDSale_Unit.Text = ""
            Catch ex As Exception

            End Try
            '' End Set En or dis Control
            'Select Edit Val  From V_loadingnote

            'zDoNotDoEvent = False
            Dim q As String
            q &= ""
            q &= "Select isnull(load_did,0) as Load_did ,"
            q &= "isnull(load_DATE,0) as LOAD_DATE ,"
            q &= "isnull(LOAD_TRUCKCOMPANY,0) as LOAD_TRUCKCOMPANY ,"
            q &= "isnull(load_Shipper,0) as load_shipper ,"
            q &= "isnull(SP_CODE,0) as SP_CODE ,"
            q &= "isnull(load_delivery,0) as load_delivery ,"
            q &= "isnull(load_vehicle,0) as load_vehicle ,"
            q &= "isnull(load_capacity,0) as load_capacity ,"
            q &= "isnull(load_driver,0) as load_driver ,"
            q &= "isnull(load_preset,0) as load_preset ,"
            q &= "isnull(Batch_Name,0) as Batch_Name ,"
            q &= "isnull(LC_SEAL,0) as LC_SEAL ,"
            q &= "isnull(LOAD_SEAL,0) as LOAD_SEAL ,"
            q &= "isnull(load_card,0) as load_card ,"
            q &= "isnull(BADGE_NO,0) as BADGE_NO ,"
            q &= "isnull(AddnoteDate,0) as AddnoteDate ,"
            q &= "isnull(Reference,0) as Reference ,"
            q &= "isnull(load_id,0) as load_id ,"
            q &= "isnull(load_Sealcount,0) as load_Sealcount ,"
            q &= "isnull(LOAD_WEIGHT_IN,0) as LOAD_WEIGHT_IN ,"
            q &= "isnull(Raw_Weight_in,0) as Raw_Weight_in ,"
            q &= "isnull(Update_Weight_in,0) as Update_Weight_in ,"
            q &= "isnull(GI_Date,0) as GI_Date ,"
            q &= "isnull(BatchLot,0) as BatchLot ,"
            q &= "isnull(Container,0) as Container ,"
            q &= "isnull(Container_Tare,0) as Container_Tare ,"
            q &= "isnull(COA,0) as COA ,"
            q &= "isnull(Remark,0) as Remark ,"
            q &= "isnull(LOAD_Customer,0) as Customer_name ,"
            q &= "isnull(status_name,0) as Status_name ,"
            q &= "isnull(packing_type,0) as packing_type ,"
            q &= "isnull(Packing_weight,0) as Packing_weight ,"
            q &= "isnull(Packing_Qty,0) as Packing_Qty ,"
            q &= "isnull(Load_status,0) as Load_status ,"
            q &= "isnull(status_name,0) as status_Name ,"
            q &= "isnull(P_code,0) as P_code ,"
            q &= "isnull(WeightTotal,0) as WeightTotal ,"
            q &= "isnull(MeterReader,0) as MeterReader ,"
            q &= "isnull(load_Temp,0) as load_Temp ,"
            q &= "isnull(Density,0) as Density ,"
            q &= "isnull(load_Tank,0) as load_Tank ,"
            q &= "isnull(WeightCal,0) as WeightCal ,"
            q &= "isnull(Raw_Weight_Out,0) as Raw_Weight_Out ,"
            q &= "isnull(Update_Weight_Out,0) as Update_Weight_Out ,"
            q &= "isnull(AccessCode,0) as AccessCode ,"
            q &= "isnull(ShipmentNo,0) as ShipmentNo ,"
            q &= "isnull(ShipmentType,'') as ShipmentType ,"
            q &= "Weightin_time ,"
            q &= "WeightOut_time ,"
            q &= "isnull(Weightin_Date ,AddnoteDate) as Weightin_Date ,"
            q &= "isnull(WeightOut_Date ,AddnoteDate) as WeightOut_Date ,"
            q &= "isnull(LOAD_DOfull,0) as LOAD_DOfull ,"
            q &= "isnull(LOAD_TYPE,0) as LOAD_TYPE ,"
            q &= "isnull(So_no,0) as SO_NO ,"
            q &= "isnull(SO_ITEM,0) as SO_ITEM ,"
            q &= "isnull(DO_ITEM,0) as DO_ITEM ,"
            q &= "isnull(SALES_UNIT,'KG') as SALES_UNIT ,"
            q &= "isnull(aCT_QTY_DELV_SU,'0') as aCT_QTY_DELV_SU ,"
            q &= "(ACT_GI_DATE) as ACT_GI_DATE ,"
            q &= "cast( CONVERT(Varchar(10),ACT_GI_DATE,120)+' '+CONVERT(Varchar(8),ACT_GI_TIME,120)as Datetime) ACT_GI_DATETIME ,"
            q &= "isnull(LC_BAY ,5) as LC_BAY ,"
            q &= "isnull(TANKFARM ,'') as TANKFARM ,"
            q &= "isnull(BATCH_INDICATOR ,'') as BATCH_INDICATOR ,"
            q &= "isnull(STAT_WAREHOUSE ,'') as STAT_WAREHOUSE ,"
            q &= "isnull(OVERALL_STAT ,'') as OVERALL_STAT ,"
            q &= "isnull(DO_INS_TXT ,'') as DO_INS_TXT ,"
            q &= "isnull(LOAD_PT ,'') as LOAD_PT ,"
            q &= "remark  "
            q &= "From V_LOADINGNOTE "
            q &= "where Load_Id = "
            q &= "" + (load_id) + " "
            q &= "and load_status <> 99   "
            Try
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
                Dim dt As New DataTable
                da.Fill(dt)


                'load_vehicle'
                If dt.Rows(0).Item("load_vehicle").ToString <> "" And dt.Rows(0).Item("load_vehicle").ToString <> "0" Then
                    EDTruck.SelectedIndex = EDTruck.FindStringExact(dt.Rows(0).Item("load_vehicle").ToString)
                    TruckId.SelectedIndex = EDTruck.SelectedIndex
                    '  EDTruck_Leave(Nothing,not
                End If
                '' End load_vehicle

                ' Forward Agent 
                If dt.Rows(0).Item("Load_TRUCKCOMPANY").ToString <> "" And dt.Rows(0).Item("Load_TRUCKCOMPANY").ToString <> "0" Then
                    TCOMPANYBindingSource.Position = TCOMPANYBindingSource.Find("COMPANY_CODE", dt.Rows(0).Item("Load_TRUCKCOMPANY").ToString)
                    EDForword.SelectedIndex = TCOMPANYBindingSource.Position
                    TruckCompanyId.SelectedIndex = TCOMPANYBindingSource.Position
                Else
                    TCOMPANYBindingSource.Position = -1
                    EDForword.Text = ""
                    EDForword_TextChanged(sender, e)
                End If
                '' End Forward Agent 

                ' Driver Name
                If dt.Rows(0).Item("load_driver").ToString <> "" And dt.Rows(0).Item("load_driver").ToString <> "0" Then
                    EDDriver.SelectedIndex = EDDriver.FindStringExact(dt.Rows(0).Item("load_driver").ToString)
                    DriverID.SelectedIndex = EDDriver.SelectedIndex
                End If
                ''End  Driver Name

                ' Sales ORG
                If dt.Rows(0).Item("SP_CODE").ToString <> "" And dt.Rows(0).Item("SP_CODE").ToString <> "0" Then
                    TSHIPPERBindingSource.Position = TSHIPPERBindingSource.Find("SP_CODE", dt.Rows(0).Item("SP_CODE").ToString)
                    EDShipper.SelectedIndex = TSHIPPERBindingSource.Position
                    ShipperId.SelectedIndex = EDShipper.SelectedIndex
                End If
                ' ' Sales ORG

                ' Customer name
                If dt.Rows(0).Item("Customer_name").ToString <> "" And dt.Rows(0).Item("Customer_name").ToString <> "0" Then
                    TCustomerBindingSource.Position = TCustomerBindingSource.Find("Customer_CODE", dt.Rows(0).Item("Customer_name").ToString)
                    EDCustomer.SelectedIndex = 1
                    EDCustomer.SelectedIndex = EDCustomer.FindStringExact(dt.Rows(0).Item("Customer_name").ToString)
                    CustomerID.SelectedIndex = EDCustomer.SelectedIndex

                Else
                    TCustomerBindingSource.Position = -1
                    EDCustomer.Text = ""
                    EDCustomer_TextChanged(sender, e)
                End If
                '' End Customer name

                ' Packing'
                If dt.Rows(0).Item("P_code").ToString <> "" And dt.Rows(0).Item("P_code").ToString <> "0" Then
                    Packing.SelectedIndex = Packing.FindStringExact(dt.Rows(0).Item("P_code").ToString)
                    PackingId.SelectedIndex = Packing.SelectedIndex
                End If
                ''end Packing'

                ' TANKFARM'
                If dt.Rows(0).Item("TANKFARM").ToString <> "" And dt.Rows(0).Item("TANKFARM").ToString <> "0" Then
                    CBTankfarm.SelectedIndex = CBTankfarm.FindStringExact(dt.Rows(0).Item("TANKFARM").ToString)

                End If
                ''end TANKFARM'

                'Status Name
                If dt.Rows(0).Item("status_Name").ToString <> "" And dt.Rows(0).Item("status_Name").ToString <> "0" Then
                    Status.SelectedIndex = Status.FindStringExact(dt.Rows(0).Item("status_Name").ToString)
                    StatusId.SelectedIndex = Status.SelectedIndex
                End If
                ''end Status Name

                'ShipmentType
                If dt.Rows(0).Item("ShipmentType").ToString <> "" And dt.Rows(0).Item("ShipmentType").ToString <> "0" Then
                    ShipMent_Type.SelectedIndex = ShipMent_Type.FindStringExact(dt.Rows(0).Item("ShipmentType").ToString)
                    Shipment_id.SelectedIndex = ShipMent_Type.SelectedIndex
                End If
                '' end ShipmentType
                'Load Type
                If (UCase(dt.Rows(0).Item("LOAD_TYPE").ToString)) = UCase(RadioBig.Text) Then
                    RadioBig.Checked = True
                Else
                    RadioLiquid.Checked = True
                End If
                '' end load type
                ' Card No.
                If dt.Rows(0).Item("LOAD_CARD").ToString <> "" And dt.Rows(0).Item("LOAD_CARD").ToString <> "0" Then
                    ' VCARDFREEBindingSource.Position = VCARDFREEBindingSource.Find("ID", dt.Rows(0).Item("LOAD_CARD").ToString)
                    CBCardNO.SelectedIndex = CBCardNO.FindStringExact(dt.Rows(0).Item("LOAD_CARD").ToString)

                End If
                If dt.Rows(0).Item("BADGE_NO").ToString <> "" And dt.Rows(0).Item("BADGE_NO").ToString <> "0" Then
                    ' VCARDFREEBindingSource.Position = VCARDFREEBindingSource.Find("ID", dt.Rows(0).Item("LOAD_CARD").ToString)
                    CBCard.SelectedIndex = CBCard.FindStringExact(dt.Rows(0).Item("BADGE_NO").ToString)

                End If
                ''End card No.

                'load_PT'
                If dt.Rows(0).Item("LOAD_PT").ToString <> "" And dt.Rows(0).Item("LOAD_PT").ToString <> "0" Then
                    CBLoadingPoint.SelectedIndex = TLOBindingSource.Find("LOAD_LOCATION", dt.Rows(0).Item("LOAD_PT").ToString)
                    'CBLoadingPoint.SelectedIndex = CBLoadingPoint.FindStringExact(dt.Rows(0).Item("LOAD_PT").ToString)
                End If
                '' End load_vehicle
                '

                ' bay
                If dt.Rows(0).Item("LC_BAY").ToString <> "" And dt.Rows(0).Item("LC_BAY").ToString <> "0" Then

                    Bay.SelectedIndex = Bay.FindStringExact(dt.Rows(0).Item("lc_Bay").ToString)
                    eBay = dt.Rows(0).Item("lc_Bay").ToString

                End If

                If dt.Rows(0).Item("OVERALL_STAT").ToString <> "" Then
                    GroupBatchtable.Enabled = False
                    GProduct.Enabled = False
                    GPreset.Enabled = False
                    Order.Enabled = False
                    CbProname.Enabled = False
                    Product.Enabled = False
                    '   GPreset.Enabled = False
                Else
                    GroupBatchtable.Enabled = True
                    Order.Enabled = True
                    GProduct.Enabled = True
                    GPreset.Enabled = True
                    CbProname.Enabled = True
                    Product.Enabled = True
                    ' GPreset.Enabled = True
                End If
                ' _LOAD_DATE = (dt.Rows(0).Item("LOAD_DATE"))
                Quantity.Text = (dt.Rows(0).Item("Packing_Qty").ToString)
                PackingWeight.Text = (dt.Rows(0).Item("Packing_weight").ToString)
                Edremark.Text = (dt.Rows(0).Item("remark").ToString)
                Edremark2.Text = (dt.Rows(0).Item("DO_INS_TXT").ToString)
                EDTruckCapa.Text = (dt.Rows(0).Item("load_capacity").ToString)
                Seal_No.Text = (dt.Rows(0).Item("LOAD_SEAL").ToString)
                EDPreset.Text = (dt.Rows(0).Item("load_preset").ToString)
                Order.Text = (dt.Rows(0).Item("load_preset").ToString)
                EDLoadID.Text = (dt.Rows(0).Item("load_id").ToString)
                EDLoaddate.Text = (dt.Rows(0).Item("Load_did").ToString)
                EDREf.Text = (dt.Rows(0).Item("Reference").ToString)
                EDLoadDTime.Text = Format(Date.Now.Date, "dd/MM/yyyy")
                GiDate.Text = (dt.Rows(0).Item("Gi_date").ToString)
                EDDONo.Text = (dt.Rows(0).Item("load_DOfull").ToString)
                Sono.Text = (dt.Rows(0).Item("SO_no").ToString)
                Soitem.Text = (dt.Rows(0).Item("SO_item").ToString)
                EDDO_item.Text = (dt.Rows(0).Item("DO_ITEM").ToString)
                Dateedit.Text = (dt.Rows(0).Item("ACT_GI_DATETIME").ToString)
                Coa.Text = (dt.Rows(0).Item("coa").ToString)
                Container.Text = (dt.Rows(0).Item("Container").ToString)
                Container_Tare.Text = (dt.Rows(0).Item("Container_Tare").ToString)
                UpdateWeightIn.Text = (dt.Rows(0).Item("Update_Weight_in").ToString)
                LawWeightIn.Text = (dt.Rows(0).Item("Raw_Weight_in").ToString)
                AccessCode.Text = (dt.Rows(0).Item("AccessCode").ToString)
                ShipmentNo.Text = (dt.Rows(0).Item("ShipmentNo").ToString)
                SealCount.Text = (dt.Rows(0).Item("LOAD_SEALCOUNT").ToString)
                WeightTotal.Text = (dt.Rows(0).Item("WeightTotal").ToString)
                MeterRead.Text = (dt.Rows(0).Item("MeterReader").ToString)
                Temp.Text = (dt.Rows(0).Item("load_Temp").ToString)
                Density.Text = (dt.Rows(0).Item("Density").ToString)
                Tank.Text = (dt.Rows(0).Item("load_Tank").ToString)
                Calculate.Text = (dt.Rows(0).Item("WeightCal").ToString)
                LawWeightout.Text = (dt.Rows(0).Item("Raw_Weight_Out").ToString)
                UpdateWeightOut.Text = (dt.Rows(0).Item("Update_Weight_Out").ToString)
                Weightintime.Text = (dt.Rows(0).Item("Weightin_time").ToString)
                weightouttime.Text = (dt.Rows(0).Item("WeightOut_time").ToString)
                EDSale_Unit.Text = (dt.Rows(0).Item("sAles_UNIT").ToString)
                EDSaleQtr.Text = (dt.Rows(0).Item("aCT_QTY_DELV_SU").ToString)
                EDREf.Text = (dt.Rows(0).Item("Reference").ToString)
                BATCH_INDICATOR = (dt.Rows(0).Item("BATCH_INDICATOR").ToString)

                weightIndate = (dt.Rows(0).Item("Weightin_Date"))
                weightOutdate = (dt.Rows(0).Item("WeightOut_Date"))
                'BARCODE1D(_LOAD_DATE, EDLoaddate.Text)
                Try
                    If LawWeightIn.Text = 0 Then
                        WeightIn.Visible = True
                        WeightOut.Visible = False
                    End If
                Catch ex As Exception

                End Try
                da.Dispose()
                dt.Dispose()
            Catch ex As Exception
                da.Dispose()
                dt.Dispose()
            End Try
            '' End Select Edit Val  From V_loadingnote
            '''''''''''''''''''''''''''''''''''''''''''''''''
            ' Check Status Laod Advisnote Finished All And En Post Sap
            Try
                Dim Sql As String
                Sql = "select count(LOAD_ID) as LOAD_ID from T_LOADINGNOTE WHERE LOAD_STATUS=1 and LOAD_DOfull<> '' and  LOAD_DOfull='" & EDDONo.Text & "'"

                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(Sql, conn)
                Dim dt As New DataTable
                da.Fill(dt)

                If dt.Rows(0)(0) > 0 Then
                    BTEDIT_SAP.Enabled = False
                Else
                    BTEDIT_SAP.Enabled = True
                End If
                da.Dispose()
                dt.Dispose()
            Catch ex As Exception
                da.Dispose()
                dt.Dispose()
            End Try
            ' Check Status Laod Advisnote Finished All And En Post Sap

            'Select load Product,Compartment,loadPreset
            q = ""
            q = "select * "
            q &= " From V_Loadingnotecompartment "
            q &= " Where LC_LOAD = "
            q &= "" + load_id + ""
            Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim ds1 As New DataSet()
            Dim dt1 As New DataTable
            da1.Fill(dt1)
            EDTruck_TextChanged(sender, e)
            Try
                Dim product, Meter, Bay As String
                Dim index As Integer
                For i = 0 To dt1.Rows.Count - 1

                    If (dt1.Rows(i).Item("Product_code").ToString) <> "" Then
                        product = ""
                        product = (dt1.Rows(i).Item("Product_code").ToString)
                        index = ProductNo(i).FindString(product)
                        ProductNo(i).SelectedIndex = index
                        If i = 0 Then Me.Product.SelectedIndex = index

                        If (dt1.Rows(i).Item("Lc_Meter").ToString) <> "0" Then
                            Meter = ""
                            Meter = (dt1.Rows(i).Item("Batch_name").ToString)
                            index = Meterno(i).FindString(Meter)
                            Meterno(i).SelectedIndex = index
                        Else
                            Meterno(i).SelectedIndex = -1
                        End If
                    Else
                        ProductNo(i).Text = ""
                        Meterno(i).Text = ""
                    End If

                    PRESETNO(i).Text = (dt1.Rows(i).Item("LC_PRESET").ToString)
                    Bay = ""
                    Bay = (dt1.Rows(i).Item("LC_BAY").ToString)
                    index = BayNo(i).FindString(Bay)
                    BayNo(i).SelectedIndex = index
                Next
                da1.Dispose()
                dt1.Dispose()
            Catch ex As Exception
                da1.Dispose()
                dt1.Dispose()
            End Try
            '' End Select load Product,Compartment,loadPreset

            'Select Batch Lot
            SelectBatch(EDLoadID.Text)
            ''End Select Batch Lot
            conn.Close()
        Catch ex As Exception

        End Try
        WeightScal_TextChanged(sender, e)
        PTruck.Visible = False
        zDoNotDoEvent = False
        conn.Dispose()
        Me.ResumeLayout()
    End Sub

    Private Sub Order_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrderBut.Click
        EDTruck_TextChanged(sender, e)
        Dim tov, Bayt As Integer
        Try
            tov = Int(Order.Text)
            Bayt = Bay.SelectedIndex
            For i = 0 To TRUCK_COMP_NUM - 1
                If Bayt = -1 Then

                    If CapacityNo(i).Text <= tov Then
                        BayNo(i).SelectedIndex = -1
                        Meterno(i).SelectedIndex = -1
                        ProductNo(i).SelectedIndex = Product.SelectedIndex
                        PRESETNO(i).Text = CapacityNo(i).Text
                        tov = tov - Int(PRESETNO(i).Text)
                    Else
                        If tov > 0 Then
                            BayNo(i).SelectedIndex = -1
                            Meterno(i).SelectedIndex = -1
                            PRESETNO(i).Text = tov
                            tov = tov - PRESETNO(i).Text
                            ProductNo(i).SelectedIndex = Product.SelectedIndex
                        Else
                            BayNo(i).SelectedIndex = -1
                            ProductNo(i).SelectedIndex = -2
                            Meterno(i).SelectedIndex = -1
                            PRESETNO(i).Text = 0
                        End If

                    End If
                Else


                    If CapacityNo(i).Text <= tov Then
                        BayNo(i).SelectedIndex = BayNo(i).FindStringExact(Bay.Text)
                        Meterno(i).SelectedIndex = Meterno(i).FindStringExact(Meter.Text)

                        'BayNo(i).SelectedIndex = Bay.SelectedIndex - 1
                        'Meterno(i).SelectedIndex = Meter.SelectedIndex - 1
                        ProductNo(i).SelectedIndex = Product.SelectedIndex
                        PRESETNO(i).Text = CapacityNo(i).Text
                        tov = tov - Int(PRESETNO(i).Text)
                    Else
                        If tov >= 0 Then
                            PRESETNO(i).Text = tov
                            tov = tov - PRESETNO(i).Text
                            ProductNo(i).SelectedIndex = Product.SelectedIndex

                            BayNo(i).SelectedIndex = BayNo(i).FindStringExact(Bay.Text)
                            Meterno(i).SelectedIndex = Meterno(i).FindStringExact(Meter.Text)

                            'BayNo(i).SelectedIndex = Bay.SelectedIndex - 1
                            'Meterno(i).SelectedIndex = Meter.SelectedIndex - 1
                        Else
                            BayNo(i).SelectedIndex = -1
                            ProductNo(i).SelectedIndex = -2
                            Meterno(i).SelectedIndex = -1
                            PRESETNO(i).Text = 0
                        End If
                    End If

                End If

            Next
        Catch ex As Exception

        End Try


        Dim presetsum As Integer

        For i = 0 To TRUCK_COMP_NUM - 1
            If PRESETNO(i).Text = "" Then
                PRESETNO(i).Text = "0"
            End If
            presetsum = Int(PRESETNO(i).Text) + presetsum
        Next i
        EDPreset.Text = presetsum

    End Sub

    Private Sub Packing_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Packing.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        '  PackingId.SelectedIndex = Packing.SelectedIndex
        P_Weight.SelectedIndex = sender.SelectedIndex
        PackingId.SelectedIndex = sender.SelectedIndex
        If sender.Text = "" Then
            P_Weight.SelectedIndex = -1
            PackingId.SelectedIndex = -1
        End If
        Quantity.Text = 1
        Try

            Quantity.Text = Math.Ceiling(EDPreset.Text / TPACKINGBindingSource.Item(sender.SelectedIndex)("P_CAPASITY").ToString())

        Catch ex As Exception
            Quantity.Text = 1
        End Try

        Quantity_TextChanged(sender, e)
    End Sub

    Private Sub Quantity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Quantity.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        Try
            PackingWeight.Text = Convert.ToDouble(Quantity.Text * P_Weight.Text)
        Catch ex As Exception
            PackingWeight.Text = 0
        End Try
    End Sub

    Private Sub LawWeightout_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LawWeightout.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If UpdateWeightOut.Text <> "" And UpdateWeightOut.Text <> "0" Then
            UpdateWeightOut_TextChanged(sender, e)
            Exit Sub
        End If
        If PackingWeight.Text = "" Then
            PackingWeight.Text = "0"
        End If

        If UpdateWeightIn.Text = "" Or UpdateWeightIn.Text = "0" Then
            Try

                'WeightTotal.Text = Int(LawWeightout.Text - LawWeightIn.Text - PackingWeight.Text)
                WeightTotal.Text = Convert.ToDouble(LawWeightout.Text - LawWeightIn.Text - PackingWeight.Text)
            Catch ex As Exception
            End Try
        Else
            Try
                WeightTotal.Text = Convert.ToDouble(LawWeightout.Text - UpdateWeightIn.Text - PackingWeight.Text)
            Catch ex As Exception
            End Try
        End If

    End Sub

    Private Sub editweightin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles editweightin.Click, Button3.Click
        loginId = 1
        f02_Login.ShowDialog()
        'If Advisenote.loginId = 2 Then
        '    UpdateWeightOut.Text = Convert.ToDouble(Cbn10.Text) + Convert.ToDouble(LawWeightIn.Text) + Convert.ToDouble(PackingWeight.Text)
        'End If

    End Sub

    Private Sub UpdateWeightOut_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateWeightOut.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If UpdateWeightOut.Text = "" Or UpdateWeightOut.Text = "0" Then
            LawWeightout_TextChanged(sender, e)
            Exit Sub
        End If

        If PackingWeight.Text = "" Then
            PackingWeight.Text = "0"
        End If

        If UpdateWeightIn.Text = "" Or UpdateWeightIn.Text = "0" Then
            Try
                WeightTotal.Text = Convert.ToInt64(UpdateWeightOut.Text - LawWeightIn.Text - PackingWeight.Text)
            Catch ex As Exception
            End Try
        Else
            Try

                WeightTotal.Text = Convert.ToInt64(UpdateWeightOut.Text - UpdateWeightIn.Text - PackingWeight.Text)
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub UpdateWeightIn_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateWeightIn.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        UpdateWeightOut_TextChanged(sender, e)
    End Sub

    Private Sub PackingWeight_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PackingWeight.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If UpdateWeightOut.Text = "" Or UpdateWeightOut.Text = "0" Then
            LawWeightout_TextChanged(sender, e)
            Exit Sub
        Else
            If LawWeightout.Text = "" Or LawWeightout.Text = "0" Then
                Exit Sub
            End If
            UpdateWeightOut_TextChanged(sender, e)
        End If
    End Sub

    Private Sub weightoutprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles weightoutprint.Click
        Try

            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()

            Dim ref As String
            ref = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("reference").ToString()

            Dim Myreport As New ReportDocument
            Myreport = New ReportDocument
            Dim ds As New DataSet
            Dim sql As String
            sql = "Select * from V_weightticket where reference =" + ref + " and load_status in(1,2,3)"

            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            'Dim ds As New DataSet()
            Dim dt As New DataTable
            da.Fill(dt)

            If dt.Rows.Count = 0 Then
                MessageBox.Show("Load Id Status can not Weight-out", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                'Dim bmpHold As StdPicture

                da.Fill(ds, "V_weightticket")
                Myreport.Load(TSHIPPERBindingSource(0)("LOAD_WeightTicket").ToString())
                'Select Case Main.SALE_ORG
                '    Case 1100
                '        Myreport.Load("TOCweightticket.rpt")
                '    Case 1200
                '        Myreport.Load("TEAweightticket.rpt")
                '    Case 1500
                '        Myreport.Load("TOLweightticket.rpt")
                '    Case 1600
                '        Myreport.Load("TOLweightticket.rpt")
                '    Case Else
                '        Myreport.Load("weightticket.rpt")
                'End Select
                'Myreport.Load("weightticket.rpt")


                Myreport.SetDataSource(ds.Tables("V_weightticket"))
                '  Myreport.Refresh()
                ' Myreport.ReadRecords()
                'Myreport.ParameterFields.
                Try
                    Myreport.PrintOptions.PrinterName = My.Settings.WIGHT_P


                Catch ex As Exception

                End Try
                ' Myreport.PrintToPrinter(My.Settings.WIGHT_COPY, False, 0, 0)
                Try
                    '  Myreport.PrintOptions.PaperOrientation = My.Settings.WIGHT_PaperOrientation
                    '  Myreport.PrintOptions.PaperSize = My.Settings.WIGHT_PaperSize
                Catch ex As Exception

                End Try

                If CShowP.Checked Then
                    Dim frm1 As New ReportPrint
                    frm1.CrystalReportViewer1.ReportSource = Nothing
                    frm1.CrystalReportViewer1.ReportSource = Myreport
                    frm1.CrystalReportViewer1.Refresh()
                    frm1.CrystalReportViewer1.RefreshReport()
                    frm1.ShowDialog(Me)
                    frm1.Dispose()

                    'RemoveOwnedForm(ReportPrint)
                    'ReportPrint.Close()
                    'ReportPrint.CrystalReportViewer1.ReportSource = Nothing
                    'ReportPrint.CrystalReportViewer1.ReportSource = Myreport
                    'ReportPrint.CrystalReportViewer1.Refresh()
                    'ReportPrint.CrystalReportViewer1.RefreshReport()
                    'ReportPrint.ShowDialog()

                Else


                    Myreport.PrintToPrinter(My.Settings.WIGHT_COPY, False, 0, 0)
                End If

            End If
            Try

                conn.Close()
                conn.Dispose()
                Myreport.Dispose()
                da.Dispose()
                dt.Dispose()
                ds.Dispose()

            Catch ex As Exception

            End Try
        Catch ex As Exception
            ' MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BTIMPORT_SAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTIMPORT_SAP.Click, BTIMPORT_SAP.Enter
        If zDoNotDoEvent = True Then Exit Sub
        FSAP02_2.DisableCloseButton(FSAP02_2)
        FSAP02_2.ShowDialog()
        If FSAP02_2.SAP_ITEM.BT_RETURN = 1 Then
            Adnote_DO = FSAP02_2.SAP_ITEM '' Order DO
            Adnote_ALLDO = FSAP02_2.SAP_ALLITEM
            Adnote_ALLBATCH = FSAP02_2.SAP_BATCH_ITEM

            If Edit.Visible = False Then
                Adnote_DO = Adnote_ALLDO(0) '' Order DO
            End If

            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()

            Dim sql As String
            sql = "Select * from T_SHIPPER WHERE SP_SAPCODE='" & Adnote_DO.sALES_ORG & "'"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim dt As New DataTable
            da.Fill(dt)


            Try
                '  EDShipper.SelectedIndex = EDShipper.FindStringExact(dt.Rows(0).Item("SP_CODE"))
                If TSHIPPERBindingSource.Find("SP_SAPCODE", dt.Rows(0).Item("SP_CODE")) >= 0 Then
                    TSHIPPERBindingSource.Position = TSHIPPERBindingSource.Find("SP_SAPCODE", dt.Rows(0).Item("SP_CODE"))
                    EDShipper.SelectedIndex = TSHIPPERBindingSource.Position
                    EDShipper_TextChanged(sender, e)
                Else
                    TSHIPPERBindingSource.Position = -1
                    EDShipper.Text = ""
                End If
                TCustomerBindingSource.Filter = "SORG_CODE='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "'"


                If TCustomerBindingSource.Find("CUS_CODE", Adnote_DO.sHIP_TO_PARTY) >= 0 Then
                    EDCustomer.SelectedIndex = 1
                    CustomerID.SelectedIndex = 1
                    TCustomerBindingSource.Position = TCustomerBindingSource.Find("CUS_CODE", Adnote_DO.sHIP_TO_PARTY)
                    EDCustomer.SelectedIndex = TCustomerBindingSource.Position
                    CustomerID.SelectedIndex = TCustomerBindingSource.Position
                    EDCustomer_TextChanged(sender, e)
                Else
                    TCustomerBindingSource.Position = -1
                    EDCustomer.Text = ""
                End If

            Catch ex As Exception

            End Try




            ' Order.Text = Adnote_DO.VOLUME
            ' Batchlot.Text = Adnote_DO.BATCH_NO

            '   Order.Text =  Adnote_DO.ACT_QTY_DELV_SU.


            ' add all batch_lost 
            SelectBatch(EDLoadID.Text)

            'If Adnote_DO.sTAT_WAREHOUSE <> "" Then
            '    GroupBatchtable.Enabled = False
            'Else
            '    GroupBatchtable.Enabled = True
            'End If

            ' TBATCHLOSTTASBindingSource.
            ' If Adnote_ALLBATCH = Nothing Then
            If Tank.Text = "" Then
                Tank.Text = Adnote_DO.sTORAGE_LOC
                Try
                    sql = ""
                    sql = "SELECT STORAGE_DES FROM T_STO  WHERE PLANT='" & Adnote_DO.pLANT & "'" &
                          " and STORAGE_LOC ='" & Adnote_DO.sTORAGE_LOC & "'"
                    da = New SqlClient.SqlDataAdapter(sql, conn)
                    Dim T_STO As New DataTable
                    da.Fill(T_STO)
                    Tank.Text = T_STO.Rows(0)("STORAGE_DES").ToString
                    T_STO.Dispose()
                Catch ex As Exception
                End Try

            End If

            Dim Sumstock_quantity As Double = 0
            Try
                For i As Int16 = 0 To TBATCHLOSTTASBindingSource.Count - 1
                    TBATCHLOSTTASBindingSource.RemoveCurrent()
                Next

                If TBATCHLOSTTASBindingSource.Count = 0 Then
                    For i = 0 To Adnote_ALLBATCH.Length - 1
                        Try
                            If (Adnote_DO.oVERALL_STAT <> "") Then
                                GroupBatchtable.Enabled = False
                                Order.Enabled = False
                                GProduct.Enabled = False
                                GPreset.Enabled = False
                                CbProname.Enabled = False
                                Product.Enabled = False
                                '  GPreset.Enabled = False
                            End If

                            'If (Adnote_DO.sTAT_WAREHOUSE <> "") Or (Adnote_ALLBATCH(i).sTAT_WAREHOUSE <> "") Then
                            '    GroupBatchtable.Enabled = False
                            '    If Adnote_DO.sTAT_WAREHOUSE = "" Then
                            '        Adnote_DO.sTAT_WAREHOUSE = Adnote_ALLBATCH(i).sTAT_WAREHOUSE
                            '        For d = 0 To Adnote_ALLDO.Length - 1
                            '            Adnote_ALLDO(d).sTAT_WAREHOUSE = (Adnote_ALLBATCH(i).sTAT_WAREHOUSE)
                            '        Next
                            '    End If
                            'End If

                            TBATCHLOSTTASBindingSource.AddNew()
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("LOAD_ID") = EDLoadID.Text
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("DO_NO") = Adnote_ALLBATCH(i).DO_NO
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("DO_ITEM") = Adnote_ALLBATCH(i).DO_ITEM
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("PLANT") = Adnote_ALLBATCH(i).PLANT
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("STOR") = Adnote_ALLBATCH(i).STORAGE_LOC
                            If Tank.Text = "" Then

                                Tank.Text = Adnote_ALLBATCH(i).STORAGE_LOC
                                Try
                                    sql = ""
                                    sql = "SELECT STORAGE_DES FROM T_STO  WHERE PLANT='" & Adnote_ALLBATCH(i).PLANT & "'" &
                                          " and STORAGE_LOC ='" & Adnote_ALLBATCH(i).STORAGE_LOC & "'"
                                    da = New SqlClient.SqlDataAdapter(sql, conn)
                                    Dim T_STO As New DataTable
                                    da.Fill(T_STO)
                                    Tank.Text = T_STO.Rows(0)("STORAGE_DES").ToString
                                    T_STO.Dispose()
                                Catch ex As Exception
                                End Try

                            End If


                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("BATCH_NO") = Adnote_ALLBATCH(i).BATCH_NO
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Adnote_ALLBATCH(i).QTY_DELV
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("Higher_Level") = Adnote_ALLBATCH(i).HIGHER_LEVEL_ITEM
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("MAT_CODE") = Adnote_ALLBATCH(i).MAT_CODE
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("Cre_date") = Now
                            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("Update_date") = Now
                            If Adnote_DO.bATCH_INDICATOR = "" Then
                                Sumstock_quantity = Adnote_DO.aCT_QTY_DELV_SU
                            ElseIf (Adnote_DO.dO_NO = Adnote_ALLBATCH(i).DO_NO) And (Adnote_DO.dO_ITEM = Adnote_ALLBATCH(i).DO_ITEM) Then
                                Sumstock_quantity = Sumstock_quantity + Adnote_ALLBATCH(i).QTY_DELV

                            End If


                        Catch ex As Exception

                        End Try

                    Next
                Else
                    For i = 0 To Adnote_ALLBATCH.Length - 1
                        Try

                            If (Adnote_DO.oVERALL_STAT <> "") Then
                                GroupBatchtable.Enabled = False
                                Order.Enabled = False
                                GProduct.Enabled = False
                                GPreset.Enabled = False
                                CbProname.Enabled = False
                                Product.Enabled = False
                                '  GPreset.Enabled = False
                            End If
                            'If (Adnote_DO.sTAT_WAREHOUSE <> "") Or (Adnote_ALLBATCH(i).sTAT_WAREHOUSE <> "") Then
                            '    GroupBatchtable.Enabled = False
                            '    If Adnote_DO.sTAT_WAREHOUSE = "" Then
                            '        Adnote_DO.sTAT_WAREHOUSE = Adnote_ALLBATCH(i).sTAT_WAREHOUSE
                            '        For d = 0 To Adnote_ALLDO.Length - 1
                            '            Adnote_ALLDO(d).sTAT_WAREHOUSE = (Adnote_ALLBATCH(i).sTAT_WAREHOUSE)
                            '        Next
                            '    End If
                            'End If
                            If Trim(TBATCHLOSTTASBindingSource(0)("DO_ITEM").ToString) = "" Then
                                TBATCHLOSTTASBindingSource(0)("DO_ITEM") = "000010"

                            End If
                            If UCase(Adnote_ALLBATCH(i).DO_ITEM) = UCase(TBATCHLOSTTASBindingSource(0)("DO_ITEM").ToString) Then
                                If TBATCHLOSTTASBindingSource.Find("BATCH_NO", Adnote_ALLBATCH(i).BATCH_NO) < 0 Then
                                    TBATCHLOSTTASBindingSource.AddNew()
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("LOAD_ID") = EDLoadID.Text
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_NO") = Adnote_ALLBATCH(i).DO_NO
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_ITEM") = Adnote_ALLBATCH(i).DO_ITEM
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("PLANT") = Adnote_ALLBATCH(i).PLANT
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("STOR") = Adnote_ALLBATCH(i).STORAGE_LOC
                                    If Tank.Text = "" Then
                                        Tank.Text = Adnote_ALLBATCH(i).STORAGE_LOC
                                        Try
                                            sql = ""
                                            sql = "SELECT STORAGE_DES FROM T_STO  WHERE PLANT='" & Adnote_ALLBATCH(i).PLANT & "'" &
                                                  " and STORAGE_LOC ='" & Adnote_ALLBATCH(i).STORAGE_LOC & "'"
                                            da = New SqlClient.SqlDataAdapter(sql, conn)
                                            Dim T_STO As New DataTable
                                            da.Fill(T_STO)
                                            Tank.Text = T_STO.Rows(0)("STORAGE_DES").ToString
                                            T_STO.Dispose()
                                        Catch ex As Exception
                                        End Try

                                    End If

                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("BATCH_NO") = Adnote_ALLBATCH(i).BATCH_NO
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("stock_quantity") = Adnote_ALLBATCH(i).QTY_DELV
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level") = Adnote_ALLBATCH(i).HIGHER_LEVEL_ITEM
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("MAT_CODE") = Adnote_ALLBATCH(i).MAT_CODE
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Cre_date") = Now
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Update_date") = Now
                                Else
                                    ' TBATCHLOSTTASBindingSource.Position = TBATCHLOSTTASBindingSource.Find("BATCH_NO", Adnote_ALLBATCH(i).BATCH_NO)
                                    TBATCHLOSTTASBindingSource.Position = TBATCHLOSTTASBindingSource.Find("BATCH_NO", Adnote_ALLBATCH(i).BATCH_NO)
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_NO") = Adnote_ALLBATCH(i).DO_NO
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_ITEM") = Adnote_ALLBATCH(i).DO_ITEM
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("PLANT") = Adnote_ALLBATCH(i).PLANT
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("STOR") = Adnote_ALLBATCH(i).STORAGE_LOC
                                    If Tank.Text = "" Then
                                        Tank.Text = Adnote_ALLBATCH(i).STORAGE_LOC
                                        Try
                                            sql = ""
                                            sql = "SELECT STORAGE_DES FROM T_STO  WHERE PLANT='" & Adnote_ALLBATCH(i).PLANT & "'" &
                                                  " and STORAGE_LOC ='" & Adnote_ALLBATCH(i).STORAGE_LOC & "'"
                                            da = New SqlClient.SqlDataAdapter(sql, conn)
                                            Dim T_STO As New DataTable
                                            da.Fill(T_STO)
                                            Tank.Text = T_STO.Rows(0)("STORAGE_DES").ToString
                                            T_STO.Dispose()
                                        Catch ex As Exception
                                        End Try
                                    End If

                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("BATCH_NO") = Adnote_ALLBATCH(i).BATCH_NO
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("stock_quantity") = Adnote_ALLBATCH(i).QTY_DELV
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level") = Adnote_ALLBATCH(i).HIGHER_LEVEL_ITEM
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("MAT_CODE") = Adnote_ALLBATCH(i).MAT_CODE
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Cre_date") = Now
                                    TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Update_date") = Now
                                End If

                                'TBATCHLOSTTASBindingSource.Position = TBATCHLOSTTASBindingSource.Find("BATCH_NO", Adnote_ALLBATCH(i).BATCH_NO)
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("LOAD_ID") = EDLoadID.Text
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_NO") = Adnote_ALLBATCH(i).DO_NO
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_ITEM") = Adnote_ALLBATCH(i).DO_ITEM
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("PLANT") = Adnote_ALLBATCH(i).PLANT
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("STOR") = Adnote_ALLBATCH(i).STORAGE_LOC
                                'If Tank.Text = "" Then Tank.Text = Adnote_ALLBATCH(i).STORAGE_LOC
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("BATCH_NO") = Adnote_ALLBATCH(i).BATCH_NO
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("stock_quantity") = Adnote_ALLBATCH(i).QTY_DELV
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level") = Adnote_ALLBATCH(i).HIGHER_LEVEL_ITEM
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("MAT_CODE") = Adnote_ALLBATCH(i).MAT_CODE
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Cre_date") = Now
                                'TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Update_date") = Now

                                If Adnote_DO.bATCH_INDICATOR = "" Then
                                    Sumstock_quantity = Adnote_DO.aCT_QTY_DELV_SU
                                ElseIf (Adnote_DO.dO_NO = Adnote_ALLBATCH(i).DO_NO) And (Adnote_DO.dO_ITEM = Adnote_ALLBATCH(i).DO_ITEM) Then
                                    Sumstock_quantity = Sumstock_quantity + Adnote_ALLBATCH(i).QTY_DELV
                                End If
                            End If


                        Catch ex As Exception

                        End Try

                    Next
                End If



            Catch ex As Exception
                GroupBatchtable.Enabled = False
                GProduct.Enabled = False
                GPreset.Enabled = False
                CbProname.Enabled = False
                Product.Enabled = False
                Sumstock_quantity = Adnote_DO.aCT_QTY_DELV_SU
            End Try


            ' TBATCHLOSTTASBindingSource.Sort = "DO_ITEM"
            TBATCHLOSTTASBindingSource.Position = 0

            EDDONo.Text = Adnote_DO.dO_NO
            Sono.Text = Adnote_DO.sO_NO
            Soitem.Text = Adnote_DO.sO_ITEM
            EDDO_item.Text = Adnote_DO.dO_ITEM
            EDSale_Unit.Text = Adnote_DO.sALES_UNIT
            EDSaleQtr.Text = Adnote_DO.aCT_QTY_DELV_SU
            Edremark.Text = Adnote_DO.dELIVERY_INSTRUCTION_TEXT


            '  EsD_DOC_CAT.Text = Adnote_DO.sD_DOC_CAT
            ' EmAT_CODE.Text = Adnote_DO.mAT_CODE

            Try
                sql = "Select Product_code from T_PRODUCT WHERE Product_number='" & Adnote_DO.mAT_CODE & "'"
                Dim da2 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                Dim dt2 As New DataTable
                da2.Fill(dt2)
                Product.SelectedIndex = Product.FindStringExact(dt2.Rows(0)("Product_code").ToString)
                ' ShipMent_Type.SelectedIndex = ShipMent_Type.FindStringExact(Strings.Left(Adnote_DO.pUR_ORDER_TYPE, 4))
                If Bay.SelectedIndex > 1 Then
                    Bay.SelectedIndex = Bay.FindStringExact(Int(Adnote_DO.lOADING_POINT).ToString)
                End If

            Catch ex As Exception
            End Try
            Try
                TUNITBindingSource.Filter = "CODE='" & UCase(Adnote_DO.sALES_UNIT) & "'"
                If TUNITBindingSource.Count > 0 Then
                    Order.Text = Sumstock_quantity * TUNITBindingSource(0)("TOkg")

                Else
                    Order.Text = Sumstock_quantity
                End If

                'Select Case UCase(Adnote_DO.sALES_UNIT)
                '    Case "L"
                '        Order.Text = Sumstock_quantity
                '    Case "TO", "MT"
                '        Order.Text = Sumstock_quantity * 1000
                '    Case Else
                '        Order.Text = Sumstock_quantity
                'End Select


                OrderBut.PerformClick()
                GiDate.Value = DateTime.ParseExact(Adnote_DO.pLAN_GI_DATE, "dd/MM/yyyy", Nothing).ToString("yyyy-MM-dd")

            Catch ex As Exception

            End Try
        Else
            Try

                '  Adnote_DO = FSAP02_2.SAP_ITEM '' Order DO
                '  Adnote_ALLDO = FSAP02_2.SAP_ALLITEM
                '  Adnote_ALLBATCH = FSAP02_2.SAP_BATCH_ITEM

            Catch ex As Exception

            End Try


        End If
    End Sub

    Private Sub ShipMent_Type_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShipMent_Type.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        Shipment_id.SelectedIndex = ShipMent_Type.SelectedIndex
        If ShipMent_Type.Text = "" Then Shipment_id.SelectedIndex = -1
    End Sub

    Private Sub Product_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Product.SelectedIndexChanged
        If zDoNotDoEvent = True Then Exit Sub
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        Try
            Dim sql As String
            sql = ""
            ' sql = "select * from V_BATCHMETER  WHERE Product_code ='" + Product.Text + "'"
            sql = "select Batch_Bay from V_BATCHMETER "
            sql &= " group by Batch_Bay  order by Batch_Bay"
            Dim da13 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            ' Dim ds13 As New DataSet()
            Dim dt13 As New DataTable
            Dim numrow, indexx As Integer
            da13.Fill(dt13)
            numrow = dt13.Rows.Count
            If numrow = 0 Then
                Bay.Items.Clear()
                Bay.Items.Add("")
                ' Bay.Items.Add("5")
                Bay.SelectedIndex = 1
            Else
                Bay.Items.Clear()
                Bay.Items.Add("")
                For indexx = 0 To numrow - 1
                    Bay.Items.Add(dt13.Rows(indexx).Item("Batch_Bay").ToString())
                Next indexx
                '  Bay.Items.Add("5")
                Bay.SelectedIndex = 1
                Bay_TextChanged(sender, e)
            End If
            Try
                Bay.SelectedIndex = Bay.FindStringExact(eBay.ToString)
            Catch ex As Exception
            End Try

            da13.Dispose()
            dt13.Dispose()

        Catch ex As Exception

        End Try
        conn.Close()
        conn.Dispose()
    End Sub

    Private Sub Bay_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bay.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        Dim sql As String
        'sql = ""
        'sql = "select * from V_BATCHMETER WHERE BATCH_ISLAND_NO ='" + Bay.Text + "'"
        'sql &= " order by Batch_NAME"
        Try
            sql = ""
            sql = "select * from V_BATCHMETER WHERE Product_code ='" + Product.Text + "'"
            sql &= " order by Batch_NAME"
            Dim da13 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            '  Dim ds13 As New DataSet()
            Dim dt13 As New DataTable
            Dim numrow, indexx As Integer
            da13.Fill(dt13)
            numrow = dt13.Rows.Count
            If numrow = 0 Then
                Meter.Items.Clear()
                Meter.Text = ""
            Else
                Meter.Items.Clear()
                Meter.Items.Add("")
                For indexx = 0 To numrow - 1
                    Meter.Items.Add(dt13.Rows(indexx).Item("Batch_NAME").ToString())
                Next indexx
                Meter.SelectedIndex = 1
            End If
            da13.Dispose()
            dt13.Dispose()
        Catch ex As Exception

        End Try
        conn.Close()
        conn.Dispose()

    End Sub

    Private Sub WeightOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WeightOut.Click
        BTGetTemp.PerformClick()
        Try
            WeightScal_TextChanged(sender, e)
            Dim conn As SqlClient.SqlConnection = GetConnection()
            conn.Open()
            Dim DO_NO As String
            Dim WIN() As Single
            Dim WOUT() As Integer
            Dim LOAD_PRESET As Integer
            Dim LOAD_WEIGHT As Integer
            Dim LOAD_WDIFF As Integer
            Dim LC_PRESET As Integer
            Dim INDEX_ITEM As Integer
            Dim LOAD_ID As Integer
            '''''''''''''''''''''''''''''''''''cal weight''''''''''''''''''''''''''''''''''''''''''''''
            DO_NO = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_DOFULL").ToString()
            LOAD_ID = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_ID").ToString()
            Try

                Dim sql As String
                sql = "Select * from T_Loadingnote where LOAD_DOFULL<>'' and LOAD_DOFULL ='" + DO_NO + "'" &
                    " Order by LOAD_ID"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                Dim ds As New DataSet()
                da.Fill(ds, "T_Loadingnote")
                If ds.Tables("T_Loadingnote").Rows.Count > 1 And DO_NO.Length > 9 Then

                    Array.Resize(WIN, ds.Tables("T_Loadingnote").Rows.Count)
                    Array.Resize(WOUT, ds.Tables("T_Loadingnote").Rows.Count)
                    WIN(0) = ds.Tables("T_Loadingnote").Rows(0)("Raw_Weight_in")
                    LOAD_PRESET = ds.Tables("T_Loadingnote").Compute("sum(LOAD_PRESET)", "")
                    LC_PRESET = ds.Tables("T_Loadingnote").Rows(0)("LOAD_PRESET")
                    LOAD_WEIGHT = WeightScal.Text - WIN(0)
                    LOAD_WDIFF = LOAD_WEIGHT - LOAD_PRESET
                    WOUT(0) = WIN(0) + (LC_PRESET + (LOAD_WDIFF / LOAD_PRESET * LC_PRESET))

                    If LOAD_ID = ds.Tables("T_Loadingnote").Rows(0)("LOAD_ID") Then
                        INDEX_ITEM = 0
                    End If
                    For i = 1 To ds.Tables("T_Loadingnote").Rows.Count - 1
                        LC_PRESET = ds.Tables("T_Loadingnote").Rows(i)("LOAD_PRESET")
                        WIN(i) = WOUT(i - 1)
                        WOUT(i) = WIN(i) + (LC_PRESET + (LOAD_WDIFF / LOAD_PRESET * LC_PRESET))

                        If LOAD_ID = ds.Tables("T_Loadingnote").Rows(i)("LOAD_ID") Then
                            INDEX_ITEM = i
                        End If
                    Next
                    LawWeightIn.Text = WIN(INDEX_ITEM)
                    LawWeightout.Text = WOUT(INDEX_ITEM)
                Else
                    LawWeightout.Text = WeightScal.Text

                End If

            Catch ex As Exception

            End Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''cal packing'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try
                Dim Prospint() As String
                Dim PkQT As Single
                Dim PkType As String
                Packing.Text = "BULK"
                Prospint = Split(CbProname.Text)
                Dim FindP As String = "P/P"
                Dim FindW As String = "W/P"
                Dim FindUN As String = "UNWRAP"


                For Each Str As String In Prospint
                    Str = UCase(Str)
                    If IsNumeric(Str) Then
                        PkQT = Str
                    End If
                    If Str.Contains(FindP) Then
                        PkType = FindP
                    End If
                    If Str.Contains(FindW) Then
                        PkType = FindW
                    End If
                    If Str.Contains(FindUN) Then
                        PkType = FindUN
                    End If
                Next
                Dim zIndex As Integer
                Dim PackCode() As String
                Array.Resize(PackCode, Packing.Items.Count)
                For i = 0 To Packing.Items.Count - 1
                    PackCode(i) = TPACKINGBindingSource(i)("P_CODE")
                Next
                For Each Str As String In PackCode
                    Str = UCase(Str)
                    If Str.Contains(PkType) Then
                        If Str.Contains(PkQT) Then
                            Packing.SelectedIndex = zIndex
                            Quantity.Text = Math.Round(EDPreset.Text / PkQT)
                            Exit For
                        End If

                    End If
                    zIndex += 1
                Next

            Catch ex As Exception

            End Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            Dateedit.Value = Now
            Status.Text = "Finished"
            '    LawWeightout.Text = WeightScal.Text



            ' LawWeightout.Text = Main.weight.Text.Replace(",", "")
            weightOutdate = Now
            weightouttime.Text = Date.Now.Hour.ToString + ":" + Date.Now.Minute.ToString + ":" + Date.Now.Second.ToString
            Dateedit.Value = Now
            LawWeightout_TextChanged(sender, e)

            conn.Close()
        Catch ex As Exception
        End Try
        Density_TextChanged(sender, e)


        If RadioLiquid.Checked And GroupBatchtable.Enabled Then
            Try

                TUNITBindingSource.Filter = "CODE='" & UCase(EDSale_Unit.Text) & "'"
                If TUNITBindingSource.Count > 0 Then

                    If Calculate.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text
                    If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text / TUNITBindingSource(0)("TOkg")

                Else
                    ' If Calculate.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text
                    If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text

                End If

                'Select Case UCase(EDSale_Unit.Text)
                '    Case "L"
                '        If Calculate.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Calculate.Text
                '    Case "TO", "MT"
                '        If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text / 1000
                '    Case Else
                '        If WeightTotal.Text > 0 Then TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = WeightTotal.Text
                'End Select
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub DBGrid1_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DBGridAddnote.CellFormatting
        If zDoNotDoEvent = True Then Exit Sub
        Try
            With (sender.Rows(e.RowIndex))

                Select Case UCase(.Cells(5).Value)
                    Case UCase("Waiting")
                        '.DefaultCellStyle.BackColor = Color.White

                        If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                            If e.Value IsNot Nothing Then
                                e.CellStyle.BackColor = Color.White
                            End If
                        End If
                    Case UCase("Loading")
                        '.DefaultCellStyle.BackColor = Color.White

                        If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                            If e.Value IsNot Nothing Then
                                e.CellStyle.BackColor = Color.LightSkyBlue
                            End If
                        End If

                    Case UCase("Finished")
                        Select Case (.Cells(6).Value)
                            Case "0"
                                If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                                    If e.Value IsNot Nothing Then
                                        e.CellStyle.BackColor = Color.Yellow
                                    End If
                                    If Trim(.Cells(0).Value) = "" Then
                                        e.CellStyle.BackColor = Color.Green

                                    End If

                                End If
                                '  .DefaultCellStyle.BackColor = Color.Yellow
                            Case "1"
                                If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                                    If e.Value IsNot Nothing Then
                                        e.CellStyle.BackColor = Color.Green
                                        .Cells(5).Value = "Finished/Posted"
                                    End If
                                End If

                                ' .DefaultCellStyle.BackColor = Color.Green
                        End Select
                    Case UCase("Finished/Posted")
                        Select Case (.Cells(6).Value)
                            Case "0"
                                If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                                    If e.Value IsNot Nothing Then
                                        e.CellStyle.BackColor = Color.Yellow
                                    End If
                                    If Trim(.Cells(0).Value) = "" Then
                                        e.CellStyle.BackColor = Color.Green

                                    End If

                                End If
                                '  .DefaultCellStyle.BackColor = Color.Yellow
                            Case "1"
                                If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                                    If e.Value IsNot Nothing Then
                                        e.CellStyle.BackColor = Color.Green
                                        '.Cells(4).Value = "Finished/Posted"
                                    End If
                                End If

                                ' .DefaultCellStyle.BackColor = Color.Green
                        End Select
                    Case UCase("cancel")
                        If Me.DBGridAddnote.Columns(e.ColumnIndex).Name = "LOAD_DOfull" Then
                            If e.Value IsNot Nothing Then
                                e.CellStyle.BackColor = Color.Red
                            End If
                        End If
                End Select
            End With

        Catch ex As Exception

        End Try



    End Sub

    Private Sub WeightIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WeightIn.Click
        CBLoadingPoint.SelectedIndex = -1
        LawWeightIn.Text = WeightScal.Text
        'LawWeightIn.Text = Main.weight.Text.Replace(",", "")
        Weightintime.Text = Date.Now.Hour.ToString + ":" + Date.Now.Minute.ToString + ":" + Date.Now.Second.ToString
    End Sub

    Private Sub PrintBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintBut.Click
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        '''''''''''''''''''''
        'Dim Picture1 As Image
        'Picture1.Picture = LoadPicture("C:PromoEmailer1.jpg")

        Try
            Dim ref As String
            ref = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("reference").ToString()

            Dim Myreport As New ReportDocument
            Myreport = New ReportDocument
            Dim sql As String
            sql = "Select * from V_Loadingnote where reference =" + ref + ""
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
            Dim ds As New DataSet()
            Dim dt As New DataTable

            da.Fill(ds, "V_Loadingnote")

            sql = "Select * from T_BARCODE"
            da.SelectCommand.CommandText = sql
            da.Fill(ds, "T_BARCODE")

            sql = "Select * from T_USERLOGIN"
            da.SelectCommand.CommandText = sql
            da.Fill(ds, "T_USERLOGIN")

            Myreport.Load(TSHIPPERBindingSource(0)("LOAD_Workorder").ToString())
            'Myreport.DataSourceConnections.Item(
            'Select Case Main.SALE_ORG
            '    Case 1100
            '        Myreport.Load("TOCAdvisenotereport.rpt")
            '    Case 1200
            '        Myreport.Load("TEAAdvisenotereport.rpt")
            '    Case 1500
            '        Myreport.Load("TOLAdvisenotereport.rpt")
            '    Case 1600
            '        Myreport.Load("TOLAdvisenotereport.rpt")
            '    Case Else
            '        Myreport.Load("Advisenotereport.rpt")
            'End Select

            Myreport.SetDataSource(ds)
            '  Myreport.PrintOptions.PaperOrientation = 1
            ' Myreport.PrintOptio()
            Try
                Myreport.PrintOptions.PrinterName = "GCCOA"
            Catch ex As Exception

            End Try
            RemoveOwnedForm(ReportPrint)
            ReportPrint.Close()
            ReportPrint.CrystalReportViewer1.ReportSource = Myreport
            ReportPrint.ShowDialog()


            '  Dim pd As New PrintDocument
            'AddHandler pd.PrintPage, AddressOf pd_PrintPage
            'pd.PrinterSettings.PrinterName = ListBox1.SelectedItem
            'pd.DefaultPageSettings.Landscape = True
            'pd.Print()
            ' pd.PrinterSettings.Copies = 2

            'Myreport.SetParameterValue(
            'PrintDialog1.PrinterSettings.Copies = 2

            'Myreport.PrintOptions.PrinterName = "\\RAPCONT1\Canon iR3300 PS3"
            'Myreport.PrintOptions.PaperSource = PaperSource.Lower
            'Myreport.PrintToPrinter(2, False, 1, 1)


            ' ReportPrint.ShowDialog()


        Catch ex As Exception

        End Try
    End Sub

    Private Sub EDTruck_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDTruck.Leave, EDShipper.Leave, EDCustomer.Leave, EDDriver.Leave, EDForword.Leave
        If zDoNotDoEvent = True Then Exit Sub
        Dim SelectText As String
        Dim Name As String
        Try
            SelectText = sender.text
            Name = UCase(sender.name)

            sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (sender.SelectedIndex < 0) And (SelectText <> "") Then
                sender.SelectedIndex = -1

                If Name = UCase("EDTruck") Then
                    f04_01_Truck.Close()
                    Me.AddOwnedForm(f04_01_Truck)
                    f04_01_Truck.ShowType = 1
                    f04_01_Truck.TRUCK_NO = SelectText
                    f04_01_Truck.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_Truck)
                    Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If f04_01_Truck.TRUCK_COMPANY <> "" Then
                        Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
                        EDForword.SelectedIndex = EDForword.FindStringExact(f04_01_Truck.TRUCK_COMPANY)
                        If EDForword.SelectedIndex <> -1 Then
                            EDDriver.Focus()
                        End If
                        f04_01_Truck.TRUCK_COMPANY = ""
                    End If

                    ''''''''''''''driver'''''''''''''''''''''''''''''''''''''
                    If f04_01_Truck.TRUCK_DRIVER <> "" Then
                        Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
                        EDDriver.SelectedIndex = EDDriver.FindStringExact(f04_01_Truck.TRUCK_DRIVER)
                        If EDDriver.SelectedIndex <> -1 Then
                            EDShipper.Focus()
                        End If
                        f04_01_Truck.TRUCK_DRIVER = ""
                    End If

                ElseIf Name = UCase("EDForword") Then
                    f04_01_TransportationCompany.Close()
                    Me.AddOwnedForm(f04_01_TransportationCompany)
                    f04_01_TransportationCompany.ShowType = 1
                    f04_01_TransportationCompany.CompanyCode = SelectText
                    f04_01_TransportationCompany.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_TransportationCompany)
                    Me.T_COMPANYTableAdapter.Fill(Me.FPTDataSet.T_COMPANY)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)

                ElseIf Name = UCase("EDDriver") Then
                    f04_01_Truck.VE_DNAME.Text = ""
                    Unloading.EDDriver.Text = ""
                    Driver.Close()
                    Me.AddOwnedForm(Driver)
                    Driver.ShowType = 1
                    Driver.DRIVER_NAME = SelectText
                    Driver.ShowDialog()
                    Me.RemoveOwnedForm(Driver)
                    Me.T_DRIVERTableAdapter.Fill(Me.FPTDataSet.T_DRIVER)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)



                ElseIf Name = UCase("EDSHIPPER") Then
                    f04_01_Shipper.Close()
                    Me.AddOwnedForm(f04_01_Shipper)
                    f04_01_Shipper.ShowType = 1
                    f04_01_Shipper.Shipper_code = SelectText
                    f04_01_Shipper.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_Shipper)
                    '   Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
                    Me.T_SHIPPERTableAdapter.Fill(Me.FPTDataSet.T_SHIPPER)
                    sender.SelectedIndex = sender.FindStringExact(SelectText)


                ElseIf Name = UCase("EDCUSTOMER") Then
                    f04_01_CustomerCompany.Close()
                    Me.AddOwnedForm(f04_01_CustomerCompany)
                    f04_01_CustomerCompany.ShowType = 1
                    f04_01_CustomerCompany.CompanyCode = SelectText
                    f04_01_CustomerCompany.CompanyName = SelectText
                    f04_01_CustomerCompany.ShowDialog()
                    Me.RemoveOwnedForm(f04_01_CustomerCompany)
                    Me.T_CustomerTableAdapter.Fill(Me.FPTDataSet.T_Customer)

                    sender.SelectedIndex = sender.FindStringExact(SelectText)

                End If
            End If
            If Name = UCase("EDTruck") Then
                EDTruck_TextChanged_1(sender, e)
                EDTruck_TextChanged(sender, e)


            End If


        Catch ex As Exception

        End Try
    End Sub

    Private Sub EDTruck_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles EDTruck.KeyUp, EDShipper.KeyUp, EDCustomer.KeyUp, EDDriver.KeyUp, EDForword.KeyUp
        If zDoNotDoEvent = True Then Exit Sub
        If e.KeyCode = 13 Then
            EDTruck_Leave(sender, Nothing)
        End If
    End Sub

    Private Sub EDShipper_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDShipper.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If EDShipper.Text = "" Then
            ShipperId.SelectedIndex = -1
            TCustomerBindingSource.RemoveFilter()
        Else
            ShipperId.SelectedIndex = EDShipper.SelectedIndex
            TCustomerBindingSource.Filter = "SORG_CODE='" & TSHIPPERBindingSource(TSHIPPERBindingSource.Position)("SP_SAPCODE").ToString & "'" _
               & " or SORG_CODE=''" & " or SORG_CODE IS NULL"
            EDCustomer.SelectedIndex = -1

        End If
    End Sub

    Private Sub EDCustomer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDCustomer.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        '  Company_sapcode.SelectedIndex = sender.SelectedIndex
        If EDCustomer.Text = "" Then
            '    Company_sapcode.SelectedIndex = -1
            CustomerID.SelectedIndex = -1
        Else
            CustomerID.SelectedIndex = EDCustomer.SelectedIndex
        End If

    End Sub

    Private Sub Order_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Order.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        Try



            If EDTruckCapa.Text = "" Then EDTruckCapa.Text = "0"
            If Order.Text = "" Then Order.Text = "0"

            If Convert.ToDouble(Order.Text) > Convert.ToDouble(EDTruckCapa.Text) Then
                MessageBox.Show("Please Check Vehicle No. ('Vehicle Capacity' And 'Order Qtr.' Not accept)", "Check", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Order.Text = EDTruckCapa.Text
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        Try
            InitializeComponent()

        Catch ex As Exception

        End Try

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub DBGrid1_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles DBGridAddnote.CellPainting
        If zDoNotDoEvent = True Then Exit Sub
        Try

            If e.ColumnIndex = 0 AndAlso e.RowIndex <> -1 Then

                Using gridBrush As Brush = New SolidBrush(sender.GridColor), backColorBrush As Brush = New SolidBrush(e.CellStyle.BackColor)

                    Using gridLinePen As Pen = New Pen(gridBrush)
                        ' Clear cell  
                        e.Graphics.FillRectangle(backColorBrush, e.CellBounds)
                        ' Draw line (bottom border and right border of current cell)  
                        'If next row cell has different content, only draw bottom border line of current cell  
                        If e.RowIndex < sender.Rows.Count - 2 AndAlso sender.Rows(e.RowIndex + 1).Cells(e.ColumnIndex).Value.ToString() <> e.Value.ToString() Then
                            e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left, e.CellBounds.Bottom - 1, e.CellBounds.Right - 1, e.CellBounds.Bottom - 1)
                        End If


                        ' Draw right border line of current cell  

                        e.Graphics.DrawLine(gridLinePen, e.CellBounds.Right - 1, e.CellBounds.Top, e.CellBounds.Right - 1, e.CellBounds.Bottom)

                        ' draw/fill content in current cell, and fill only one cell of multiple same cells  

                        If Not e.Value Is Nothing Then

                            If e.RowIndex > 0 AndAlso sender.Rows(e.RowIndex - 1).Cells(e.ColumnIndex).Value.ToString() = e.Value.ToString() Then

                            Else

                                e.Graphics.DrawString(CType(e.Value, String), e.CellStyle.Font, Brushes.Black, e.CellBounds.X + 2, e.CellBounds.Y + 5, StringFormat.GenericDefault)

                            End If

                        End If

                        e.Handled = True
                    End Using

                End Using
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub SelectBatch(ByVal Load_id As String)
        Dim conn As SqlClient.SqlConnection = GetConnection()
        Try
            conn.Open()
            Dim q As String
            q = "select * from  T_BATCH_LOT_TAS Where LOAD_ID='" & Load_id + "' order by DO_ITEM"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(q, conn)
            Dim MyDataSet As New DataSet
            da.Fill(MyDataSet, "T_BATCH_LOT_TAS")
            TBATCHLOSTTASBindingSource.DataSource = MyDataSet
            TBATCHLOSTTASBindingSource.DataMember = "T_BATCH_LOT_TAS"
            MeterGrid.DataSource = TBATCHLOSTTASBindingSource
            BindingNavigator1.BindingSource = TBATCHLOSTTASBindingSource
            conn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Savebatch(ByVal ds As DataSet, ByVal DO_NO As String, ByVal LOAD_ID As String)
        TBATCHLOSTTASBindingSource.EndEdit()
        BindingContext(ds, "T_BATCH_LOT_TAS").EndCurrentEdit()
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()

        Try

            Dim sql As String = "select * from  T_BATCH_LOT_TAS Where DO_NO='" & DO_NO + "'"
            If LOAD_ID <> "" Then
                sql = sql + "  and LOAD_ID = " & LOAD_ID
            End If
            sql = sql & " order by DO_ITEM"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                If ds.HasChanges Then
                    da.Update(ds, "T_BATCH_LOT_TAS")
                    ds.AcceptChanges()
                End If
            Finally

                da.Dispose()
                conn.Close()
                conn.Dispose()
            End Try

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NoteAddNew()
        Try
            'AddNew = 1
            BtUnload.Enabled = False
            TTANKFARMBindingSource.Position = -1
            TTRUCKBindingSource.Position = -1
            TCOMPANYBindingSource.Position = -1
            TDRIVERBindingSource.Position = -1
            TCustomerBindingSource.Position = -1
            TSHIPMENTTYPEBindingSource.Position = -1
            TSHIPPERBindingSource.Position = -1
            TPACKINGBindingSource.Position = -1
            TSTATUSBindingSource.Position = -1
            TProductBindingSource.Position = -1
            VCARDFREEBindingSource.Position = -1
            ' Ship
            CBTankfarm.SelectedIndex = -1
            EDShipper.SelectedIndex = -1
            ShipperId.SelectedIndex = -1
            EDCustomer.SelectedIndex = -1
            Product.SelectedIndex = -1
            CbProname.SelectedIndex = -1
            ProductId.SelectedIndex = -1

            EDTruck.SelectedIndex = -1
            EDForword.SelectedIndex = -1
            EDDriver.SelectedIndex = -1
            CBCardNO.SelectedIndex = -1
            CBCard.SelectedIndex = -1
            Product.Text = ""
            Status.SelectedIndex = -1
            Packing.SelectedIndex = -1
            Bay.Text = ""
            Meter.Text = ""
            ShipMent_Type.SelectedIndex = -1
            Company_sapcode.SelectedIndex = -1
            P_Weight.SelectedIndex = -1
            PackingId.SelectedIndex = -1
            Shipment_id.SelectedIndex = -1
            EDLoadDTime.Text = ""
            Updatedate.Text = ""
            EDLoadID.Text = ""
            TruckId.SelectedIndex = -1
            Prono.SelectedIndex = -1
            ProductId.SelectedIndex = -1
            StatusId.SelectedIndex = -1
            TruckCompanyId.SelectedIndex = -1
            DriverID.SelectedIndex = -1
            CustomerID.SelectedIndex = -1
            ShipperId.SelectedIndex = -1
            TextBox1.Text = ""
            DateeditTemp.Text = ""
            ComboBox1.SelectedIndex = -1
            EDDO_item.Text = ""
            EDSale_Unit.Text = ""
            EDSaleQtr.Text = ""

            EDDONo.Text = ""
            Coa.Text = ""
            Container.Text = ""
            Container_Tare.Text = ""
            'Batchlot.Text = ""
            Seal_No.Text = ""
            EDREf.Text = ""
            EDTruckCapa.Text = ""
            EDPreset.Text = ""
            LawWeightIn.Text = ""
            UpdateWeightIn.Text = ""
            LawWeightout.Text = ""
            UpdateWeightOut.Text = ""
            AccessCode.Text = ""
            ShipmentNo.Text = ""

            SealCount.Text = ""
            Quantity.Text = ""
            PackingWeight.Text = ""
            Edremark.Text = ""
            EDLoaddate.Text = ""
            WeightTotal.Text = ""
            MeterRead.Text = ""
            Temp.Text = ""
            Density.Text = ""
            Tank.Text = ""
            Calculate.Text = ""
            Order.Text = ""
            Weightintime.Text = ""
            weightouttime.Text = ""
            DriverID.SelectedIndex = -1
            EDDriver.SelectedIndex = -1
            Edremark2.Text = ""

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Btadd_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Btadd.MouseUp
        If zDoNotDoEvent = True Then Exit Sub
        Try
            Dim conn As SqlClient.SqlConnection = GetConnection()

            Dim PMAT_CODE As String
            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("LOAD_ID") = EDLoadID.Text
            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("DO_NO") = EDDONo.Text
            TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("DO_ITEM") = EDDO_item.Text
            Try

                Dim sql As String
                sql = "Select Product_number from T_PRODUCT where Product_code ='" + Product.Text + "'"
                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)
                Dim dt As New DataTable
                da.Fill(dt)
                PMAT_CODE = dt.Rows(0).Item("Product_number").ToString()

            Catch ex As Exception
            End Try
            If Trim(EDDO_item.Text) = "" Then EDDO_item.Text = "000010"
            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("DO_ITEM") = EDDO_item.Text
            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("MAT_CODE") = PMAT_CODE
            'TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("PLANT") = Adnote_ALLBATCH(i).PLANT
            'TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("STOR") = Adnote_ALLBATCH(i).STORAGE_LOC
            ' TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("BATCH_NO") = Adnote_ALLBATCH(i).BATCH_NO
            ' TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("stock_quantity") = Adnote_ALLBATCH(i).QTY_DELV
            'TBATCHLOSTTASBindingSource(MeterGrid.DataSource.Position)("Higher_Level") = Adnote_ALLBATCH(i).HIGHER_LEVEL_ITEM
            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Cre_date") = Now
            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Update_date") = Now
            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level") = EDDO_item.Text

            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level") = TBATCHLOSTTASBindingSource(0)("Higher_Level")

            If Trim(TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level")) = "" Then
                TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("Higher_Level") = EDDO_item.Text
            End If


            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("PLANT") = Adnote_DO.pLANT

            TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("STOR") = TBATCHLOSTTASBindingSource(0)("STOR")

            If TBATCHLOSTTASBindingSource.Count > 0 Then
                TBATCHLOSTTASBindingSource(TBATCHLOSTTASBindingSource.Position)("PLANT") = TBATCHLOSTTASBindingSource(0)("PLANT")
            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub editweightout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles editweightout.Click

        loginId = 1
        If Main.U_GROUP_ID < 2 Then
            f02_Login.ShowDialog()

            Try

                If f05_01_Advisenote.loginId = 2 Then
                    UpdateWeightOut.Text = Convert.ToDouble(EDPreset.Text) + Convert.ToDouble(LawWeightIn.Text) + Convert.ToDouble(PackingWeight.Text)
                    If UpdateWeightIn.Text <> "" And UpdateWeightIn.Text <> "0" Then
                        UpdateWeightOut.Text = Convert.ToDouble(EDPreset.Text) + Convert.ToDouble(UpdateWeightIn.Text) + Convert.ToDouble(PackingWeight.Text)
                    End If
                End If

            Catch ex As Exception

            End Try
        Else
            Try
                Me.UpdateWeightIn.Enabled = True
                Me.UpdateWeightOut.Enabled = True
                Me.LawWeightIn.Enabled = True
                '   Unloading.UpdateWeightIn.Enabled = True
                ' Unloading.UpdateWeightOut.Enabled = True
                ' Advisenote.loginId = 2
                UpdateWeightOut.Text = Convert.ToDouble(EDPreset.Text) + Convert.ToDouble(LawWeightIn.Text) + Convert.ToDouble(PackingWeight.Text)
                If UpdateWeightIn.Text <> "" And UpdateWeightIn.Text <> "0" Then
                    UpdateWeightOut.Text = Convert.ToDouble(EDPreset.Text) + Convert.ToDouble(UpdateWeightIn.Text) + Convert.ToDouble(PackingWeight.Text)
                End If
            Catch ex As Exception

            End Try

        End If
    End Sub

    Private Sub DatetimeDBGrid1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectDate.ValueChanged
        If zDoNotDoEvent = True Then Exit Sub
        ' Dim DD As String
        Try
            'DD = "Loaddate='" + String.Format("{0:yyyy-M-d}", DatetimeDBGrid1.Value) + "'"
            'V_LoadingnoteBindingSource.Filter = DD
            SelectVLoadingNote()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub EDFillter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDFillter.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        '  Dim StrFilterAll As String
        If UCase(Trim(EDFillter.Text)) <> UCase("") Then
            If SelectVLoadingNotecase <> 2 Then
                SelectVLoadingNoteAllWaiting()
                SelectVLoadingNotecase = 2
                EDFillter.Focus()
                PlantFilter = "(PLANT='' or PLANT <>'' )"
            End If

        Else
            If SelectVLoadingNotecase <> 1 Then
                SelectVLoadingNote()
                SelectVLoadingNotecase = 1
                EDFillter.Focus()
            End If
        End If

        If EDFillter.Text <> "" Then
            Dim StrFilter As String = ""
            For i = 0 To FPTDataSet.V_LOADINGNOTE.Columns.Count - 1
                Dim _DataType As String = FPTDataSet.V_LOADINGNOTE.Columns(i).DataType.ToString

                If _DataType = "System.String" Then
                    If StrFilter <> "" Then StrFilter += " or "
                    StrFilter += FPTDataSet.V_LOADINGNOTE.Columns(i).ColumnName & " like '%" & EDFillter.Text & "%' "
                ElseIf _DataType = "System.Int16" Or _DataType = "System.Int32" Or _DataType = "System.Int64" Or _DataType = "System.Double" Then
                    Try
                        Int(EDFillter.Text)
                        Dim FilterText As String = Math.Abs(Int(EDFillter.Text))
                        If StrFilter <> "" Then StrFilter += " or "
                        StrFilter += FPTDataSet.V_LOADINGNOTE.Columns(i).ColumnName & " = '" & FilterText & "'"
                    Catch ex As Exception
                    End Try
                End If
            Next
            If PlantShow.Checked Then
                'StrFilter = "(" & StrFilter & " ) "
                '  V_LoadingnoteBindingSource.Filter = "(" & StrFilter & " ) " 'and (Loaddate='" + String.Format("{0:yyyy-M-d}", SelectDate.Value) + "')"
            Else
                StrFilter &= " and " & "(" & PlantFilter & " ) "  'and (Loaddate='" + String.Format("{0:yyyy-M-d}", SelectDate.Value) + "')"
            End If
            If StatusShow.Checked Then
                ' StrFilter &= " and " & "(" & PlantFilter & " ) "
                'V_LoadingnoteBindingSource.Filter = "(" & StrFilter & " ) " 'and (Loaddate='" + String.Format("{0:yyyy-M-d}", SelectDate.Value) + "')"
            Else
                StrFilter &= " and " & "(" & StrFilter & " ) "
                'V_LoadingnoteBindingSource.Filter = PlantFilter & " and " & "(" & StrFilter & " ) " & " and " & "(" & StatusFilter & " ) "  'and (Loaddate='" + String.Format("{0:yyyy-M-d}", SelectDate.Value) + "')"
            End If

            V_LoadingnoteBindingSource.Filter = StrFilter
            EDFillter.Focus()
        Else

            V_LoadingnoteBindingSource.RemoveFilter()
            Dim DD As String
            DD = "(Loaddate='" + String.Format("{0:yyyy-M-d}", SelectDate.Value) + "')"
            '  If Trim(PlantFilter) = "" Then
            'V_LoadingnoteBindingSource.Filter = DD
            '  Else
            V_LoadingnoteBindingSource.Filter = PlantFilter ' & " and " & DD
            ' End If




        End If

    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If zDoNotDoEvent = True Then Exit Sub
        LoginDo = 1
        loginId = 1
        '    Login.ShowDialog()

        If Main.U_GROUP_ID < 2 Then
            f02_Login.ShowDialog()
        Else
            Me.EDDONo.Enabled = True
            Me.EDDO_item.Enabled = True
            Me.Sono.Enabled = True
            Me.Soitem.Enabled = True
            Me.EDCustomer.Enabled = True
            Me.EDShipper.Enabled = True
        End If

    End Sub

    Private Sub EDTruck_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDTruck.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If EDTruck.Text = "" Then
            TruckId.SelectedIndex = -1
        Else
            TruckId.SelectedIndex = EDTruck.SelectedIndex

            Try


                TDRIVERBindingSource.Position = TDRIVERBindingSource.Find("ID", TTRUCKBindingSource.Item(TruckId.SelectedIndex)("TRUCK_DRIVER").ToString)
                EDDriver.SelectedIndex = TDRIVERBindingSource.Position
            Catch ex As Exception
                TDRIVERBindingSource.Position = -1
                EDDriver.SelectedIndex = -1
            End Try

            Try
                TCOMPANYBindingSource.Position = TCOMPANYBindingSource.Find("COMPANY_ID", TTRUCKBindingSource.Item(TruckId.SelectedIndex)("TRUCK_COMPANY").ToString)
                EDForword.SelectedIndex = TCOMPANYBindingSource.Position
            Catch ex As Exception
                TCOMPANYBindingSource.Position = -1
                EDForword.SelectedIndex = -1
            End Try

        End If

    End Sub

    Private Sub EDForword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDForword.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If EDForword.Text = "" Then
            Company_sapcode.SelectedIndex = -1
            TruckCompanyId.SelectedIndex = -1
        Else
            TruckCompanyId.SelectedIndex = EDForword.SelectedIndex
            Company_sapcode.SelectedIndex = EDForword.SelectedIndex
        End If
    End Sub

    Private Sub EDDriver_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EDDriver.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If EDDriver.Text = "" Then
            DriverID.SelectedIndex = -1
        Else
            DriverID.SelectedIndex = EDDriver.SelectedIndex
        End If
        d_Exdate_ValueChanged(Nothing, Nothing)
    End Sub

    Private Sub Status_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Status.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        If Status.Text = "" Then
            StatusId.SelectedIndex = -1
        Else
            StatusId.SelectedIndex = Status.SelectedIndex
        End If

    End Sub

    Private Sub BtUnload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtUnload.Click
        Me.AddOwnedForm(Unloading)
        Unloading.Show()
        Unloading.BringToFront()
    End Sub

    Private Sub Density_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Density.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        Try
            If Convert.ToDouble(Density.Text) > 0 Then
                Calculate.Text = (Convert.ToDouble(WeightTotal.Text) / Convert.ToDouble(Density.Text)).ToString("###")
            Else
                Calculate.Text = 0
            End If

        Catch ex As Exception
            Calculate.Text = 0
        End Try
    End Sub


    Private Sub WeightTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WeightTotal.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        ' TextWeightTotal.Text = WeightTotal.Text
    End Sub

    Private Sub WeightScal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WeightScal.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        Try

            If WeightScal.Text > 100 Then
                Try
                    If LawWeightIn.Text > 0 Then
                        TextBox2.Text = WeightScal.Text - LawWeightIn.Text
                    End If
                Catch ex As Exception

                End Try


                If InG.Visible Then
                    ImagetruckIn.Visible = False
                    ImagetruckOut.Visible = True
                Else
                    ImagetruckOut.Visible = False
                    ImagetruckIn.Visible = True
                End If

                '       TextWeightTotal.Visible = True
            Else
                TextBox2.Text = 0
                ImagetruckIn.Visible = False
                ImagetruckOut.Visible = False
                '       TextWeightTotal.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub V_LoadingnoteBindingSource_PositionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles V_LoadingnoteBindingSource.PositionChanged
        If zDoNotDoEvent = True Then Exit Sub
        Try

            '  TextWeightTotal.Text = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("Raw_Weight_out").ToString - V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("Raw_Weight_in").ToString


        Catch ex As Exception

        End Try

        Try
            If V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("STATUS_NAME").ToString = "Finished" Then
                BTEDIT_SAP.Enabled = True
            Else
                BTEDIT_SAP.Enabled = False
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BtVehicleEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtVehicleEdit.Click
        If zDoNotDoEvent = True Then Exit Sub
        Dim SelectText As String
        '  Dim Name As String
        Try
            SelectText = EDTruck.Text


            '     sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (EDTruck.SelectedIndex >= 0) And (EDTruck.Text <> "") Then
                f04_01_Truck.Close()
                Me.AddOwnedForm(f04_01_Truck)
                f04_01_Truck.ShowType = 2
                f04_01_Truck.TRUCK_NO = SelectText
                f04_01_Truck.ShowDialog()
                Me.RemoveOwnedForm(f04_01_Truck)
                Me.T_TRUCKTableAdapter.Fill(Me.FPTDataSet.T_TRUCK)
                EDTruck.SelectedIndex = EDTruck.FindStringExact(SelectText)
                EDTruck_Leave(sender, e)
                Order_Click(sender, e)

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Packing_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Packing.Leave
        If zDoNotDoEvent = True Then Exit Sub
        Dim SelectText As String
        '  Dim Name As String
        Try
            SelectText = Me.Packing.Text


            '     sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (Me.Packing.SelectedIndex < 0) And (Me.Packing.Text <> "") Then
                Fpacking.Close()
                Me.AddOwnedForm(Fpacking)
                Fpacking.ShowType = 1
                Fpacking.PCode = SelectText
                Fpacking.ShowDialog()
                Me.RemoveOwnedForm(Fpacking)
                Me.T_PACKINGTableAdapter.Fill(Me.FPTDataSet.T_PACKING)
                Me.Packing.SelectedIndex = Me.Packing.FindStringExact(SelectText)

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Packing_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Packing.KeyUp
        If zDoNotDoEvent = True Then Exit Sub
        If e.KeyCode = 13 Then
            Packing_Leave(sender, Nothing)
        End If
    End Sub


    Public Sub SaveTankfarm(ByVal ds As DataSet)
        'Update a dataset representing T_batchMeter
        Dim conn As SqlClient.SqlConnection = GetConnection()
        conn.Open()
        Try
            Dim sql As String = "Select * from T_TANKFARM ORDER BY ID"
            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql, conn)

            Try
                Dim cb As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(da)
                sql = "UPDATE T_USERLOGIN SET Update_date=getdate(),USERNAME='" & Main.U_NAME & "'" _
               & ",USERGROUP='" & Main.U_GROUP & "'"
                Dim da1 As New SqlClient.SqlDataAdapter
                da1.UpdateCommand = New SqlClient.SqlCommand(sql, conn)
                da1.UpdateCommand.ExecuteNonQuery()
                If ds.HasChanges Then
                    da.Update(ds, "T_TANKFARM")
                    ds.AcceptChanges()
                End If
            Finally

                da.Dispose()
            End Try

        Finally
            conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub CBTankfarm_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBTankfarm.Leave

        Dim SelectText As String
        '  Dim Name As String
        Try
            SelectText = CBTankfarm.Text
            '     sender.SelectedIndex = sender.FindStringExact(SelectText)
            If (CBTankfarm.SelectedIndex < 0) And (CBTankfarm.Text <> "") Then

                Dim conn As SqlClient.SqlConnection = GetConnection()

                conn.Open()
                '  If MyErrorProvider.CheckAndShowSummaryErrorMessage = True Then

                Try

                    '
                    TTANKFARMBindingSource.CancelEdit()
                    TTANKFARMBindingSource.AddNew()
                    TTANKFARMBindingSource.Item(TTANKFARMBindingSource.Position)("CDATE") = Now
                    TTANKFARMBindingSource.Item(TTANKFARMBindingSource.Position)("CODE") = SelectText
                    TTANKFARMBindingSource.Item(TTANKFARMBindingSource.Position)("NAME") = SelectText
                    'TTANKFARMBindingSource.Item(TTANKFARMBindingSource.Position)("CUS_ID") = CustomerID.Text
                    TTANKFARMBindingSource.Item(TTANKFARMBindingSource.Position)("UPDATE_DATE") = Now
                    TTANKFARMBindingSource.EndEdit()
                    BindingContext(FPTDataSet, "T_TANKFARM").EndCurrentEdit()
                    SaveTankfarm(FPTDataSet)
                    'Driver_Load(sender, e)
                    Me.BringToFront()
                    Me.T_TANKFARMTableAdapter.Fill(Me.FPTDataSet.T_TANKFARM)
                    CBTankfarm.SelectedIndex = CBTankfarm.FindStringExact(SelectText)
                Finally

                    conn.Close()
                    conn.Dispose()
                End Try
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Missing Information", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Stop)

        End Try

    End Sub

    Private Sub CBTankfarm_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CBTankfarm.KeyUp
        If zDoNotDoEvent = True Then Exit Sub
        If e.KeyCode = 13 Then
            CBTankfarm_Leave(sender, Nothing)
        End If
    End Sub


    Private Sub BtWorkPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtWorkPrint.Click
        Try
            If TSHIPPERBindingSource(0)("LOAD_Workorder").ToString() = "TOCWorkOrder.rpt" Then

                Try
                    Dim conn1 As SqlClient.SqlConnection = GetConnection()
                    conn1.Open()
                    Dim ref1 As String

                    If EDREf.Text = "" Then
                        ref1 = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("reference").ToString()
                    Else
                        ref1 = EDREf.Text
                    End If

                    Dim Myreport1 As New ReportDocument
                    Myreport1 = New ReportDocument
                    Dim sql1 As String
                    sql1 = "Select * from V_LOADINGNOTE where reference =" + ref1 + ""
                    Dim da1 As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql1, conn1)
                    Dim ds1 As New DataSet()
                    Dim dt1 As New DataTable
                    da1.Fill(dt1)

                    If dt1.Rows.Count = 0 Then
                        MessageBox.Show("Load Id Status can not Weight-out", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    Else
                        'Dim bmpHold As StdPicture

                        da1.Fill(ds1, "V_LOADINGNOTE")
                        Myreport1.Load(TSHIPPERBindingSource(0)("LOAD_Workorder").ToString())


                        'Myreport.Load("weightticket.rpt")
                        Try
                            Myreport1.PrintOptions.PrinterName = "GCCOA"
                        Catch ex As Exception

                        End Try

                        Myreport1.SetDataSource(ds1.Tables("V_LOADINGNOTE"))
                        'Myreport.ParameterFields.
                        ReportPrint.Close()
                        ReportPrint.CrystalReportViewer1.ReportSource = Myreport1
                        ReportPrint.CrystalReportViewer1.Refresh()
                        'ReportPrint.CrystalReportViewer1.Show()
                        'Threading.Thread.Sleep(1000)
                        Try


                            ReportPrint.ShowDialog()
                        Catch ex As Exception
                            '   ReportPrint.ShowDialog()
                        End Try

                        Myreport1.Dispose()
                        ds1.Dispose()
                        dt1.Dispose()
                        da1.Dispose()
                        conn1.Close()
                        conn1.Dispose()

                    End If
                Catch ex As Exception
                End Try
                Exit Sub


            End If


            'If TSETTINGBindingSource(0)("PLANT").ToString = "1151" Then
            '    weightoutprint_Click(sender, e)
            '    Exit Sub
            'End If
            Dim conn As SqlClient.SqlConnection = GetConnection()
            Dim Myreport As New ReportDocument
            Dim sql As String
            Dim cmd As New SqlCommand
            Dim ds As New DataSet()
            Dim ref As String = ""
            Dim DO_no As String = ""
            conn.Open()

            If EDDONo.Text = "" Then
                DO_no = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_DOfull").ToString()
            Else
                DO_no = EDDONo.Text
            End If


            If EDREf.Text = "" Then
                ref = V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("reference").ToString()
            Else
                ref = EDREf.Text
            End If

            sql = "Select * from V_LOADINGNOTE where LOAD_DOfull ='" + DO_no + "'"
            If Trim(DO_no) = "" Or DO_no.Length < 9 Then
                sql = "Select * from V_LOADINGNOTE where reference ='" + ref + "'"
            End If

            If UCase(TSHIPPERBindingSource(0)("LOAD_Workorder").ToString()) = UCase("TOLadvisenotereport.rpt") Then
                sql = "Select * from V_LOADINGNOTE2 where LOAD_DOfull ='" + DO_no + "'"
                If Trim(DO_no) = "" Or DO_no.Length < 9 Then
                    sql = "Select * from V_LOADINGNOTE2 where reference ='" + ref + "'"
                End If
            End If

            Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(sql & " and LC_STATUS<>99", conn)

            cmd.Connection = conn
            ' Dim dt As New DataTable
            da.Fill(ds, "V_LOADINGNOTE")
            'da.Fill(dt)


            If ds.Tables("V_LOADINGNOTE").Rows.Count = 0 Then
                MessageBox.Show("Load Id Status can not Weight-out", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                'Dim bmpHold As StdPicture
                '_LOAD_DATE =

                Dim IMAGECODE As Image
                Dim location = Assembly.GetExecutingAssembly().Location
                Dim appPath = Path.GetDirectoryName(location)
                IMAGECODE = (GENCODE1D(ds.Tables("V_LOADINGNOTE").Rows(0)("LOAD_DATE"), ds.Tables("V_LOADINGNOTE").Rows(0)("LOAD_DID")))
                IMAGECODE.Save(appPath + "\BARCODE1D.png", System.Drawing.Imaging.ImageFormat.Png)

                IMAGECODE = (GENCODE1D(ds.Tables("V_LOADINGNOTE").Rows(0)("LOAD_DATE"), ds.Tables("V_LOADINGNOTE").Rows(0)("LOAD_DID")))
                IMAGECODE.Save(appPath + "\BARCODE2D.png", System.Drawing.Imaging.ImageFormat.Png)
                IMAGECODE.Dispose()
                Try

                    cmd.CommandText = " Update T_BARCODE set BarCode1D=(select * from openrowset (bulk '" & appPath & "\BARCODE1D.png" & "',single_blob) as x)" &
                        " where(ID = 1)"
                    cmd.ExecuteNonQuery()
                    cmd.CommandText = " Update T_BARCODE set BarCode2D=(select * from openrowset (bulk '" & appPath & "\BARCODE2D.png" & "',single_blob) as x)" &
                        " where(ID = 1)"
                    cmd.ExecuteNonQuery()
                Catch ex As Exception

                End Try

                sql = "Select * from T_BARCODE"
                da.SelectCommand.CommandText = sql
                da.Fill(ds, "T_BARCODE")

                sql = "Select * from T_USERLOGIN"
                da.SelectCommand.CommandText = sql
                da.Fill(ds, "T_USERLOGIN")
                '  End If
                Myreport.Load(TSHIPPERBindingSource(0)("LOAD_Workorder").ToString())
                ' Myreport.Load("I1WorkOrder.rpt")

                'Select Case Main.SALE_ORG
                '    Case 1100
                '        Myreport.Load("TOCWorkOrder.rpt")
                '    Case 1200
                '        Myreport.Load("TEAWorkOrder.rpt")
                '    Case 1500
                '        Myreport.Load("TOLWorkOrder.rpt")
                '    Case 1600
                '        Myreport.Load("TOLWorkOrder.rpt")
                '    Case Else
                '        Myreport.Load("WorkOrder.rpt")

                'End Select
                'Myreport.Load("weightticket.rpt")
                ' Myreport.PrintOptions.PrinterName = "GCCOA"
                Myreport.SetDataSource(ds)
                'Myreport.ParameterFields.
                Try
                    Myreport.PrintOptions.PrinterName = My.Settings.WORK_P
                Catch ex As Exception

                End Try

                Try
                    Myreport.PrintOptions.PaperOrientation = My.Settings.WORK_PaperOrientation
                    Myreport.PrintOptions.PaperSize = My.Settings.WORK_PaperSize
                Catch ex As Exception
                End Try


                If CShowP.Checked Then
                    Dim frm1 As New ReportPrint
                    frm1.CrystalReportViewer1.ReportSource = Nothing
                    frm1.CrystalReportViewer1.ReportSource = Myreport
                    frm1.CrystalReportViewer1.Refresh()
                    frm1.CrystalReportViewer1.RefreshReport()
                    frm1.ShowDialog(Me)
                    frm1.Dispose()

                    'RemoveOwnedForm(ReportPrint)
                    'ReportPrint.Close()
                    'ReportPrint.CrystalReportViewer1.ReportSource = Nothing
                    'ReportPrint.CrystalReportViewer1.ReportSource = Myreport
                    'ReportPrint.CrystalReportViewer1.Refresh()
                    'ReportPrint.CrystalReportViewer1.RefreshReport()
                    'ReportPrint.Show()


                Else
                    Myreport.PrintToPrinter(My.Settings.WORK_COPY, False, 0, 0)
                End If
                'Myreport.PrintToPrinter(My.Settings.WORK_COPY, False, 0, 0)

            End If
            Try
                Myreport.Dispose()
                conn.Close()
                conn.Dispose()
                da.Dispose()
                ds.Dispose()
                dt.Dispose()
            Catch ex As Exception

            End Try


        Catch ex As Exception
            '  MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub BTDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTDownload.Click

        If UCase(V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("STATUS_NAME").ToString()) = UCase("Finished") Or
            UCase(V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("STATUS_NAME").ToString()) = UCase("Cancel") Then
            MessageBox.Show("Loading " & V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("STATUS_NAME").ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Try

            Dim q As String
            Dim SqlCon As New SqlClient.SqlConnection
            SqlCon.ConnectionString = My.Settings.FPTConnectionString
            SqlCon.Open()
            Dim da As New SqlClient.SqlDataAdapter
            'q = "delete T_PLCDOWNLOAD"
            'da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            'da.UpdateCommand.ExecuteNonQuery()

            q = "insert into T_PLCDOWNLOAD (DRIVER_PRESET,DRIVER_PASSWORD,BAY) Values ("
            q = q & " " & V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_PRESET").ToString() & ","
            q = q & " " & V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LOAD_CARD").ToString() & ","
            q = q & " " & V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("LC_BAY").ToString() & ")"
            da = New SqlClient.SqlDataAdapter
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()
            q = "update T_loadingnote set load_status=2 where Reference='" & V_LoadingnoteBindingSource.Item(V_LoadingnoteBindingSource.Position)("Reference").ToString() & "'"
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()
            da.Dispose()
            SqlCon.Close()
            SqlCon.Dispose()
            SelectVLoadingNote()

        Catch ex As Exception
        End Try
    End Sub

    Private Sub Advisenote_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            ThrLIGHTCONTROL.CancelAsync()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Seal_No_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Seal_No.TextChanged
        If zDoNotDoEvent = True Then Exit Sub
        'Dim a, b, c, d, Seal1, Seal2 As String
        Dim ee, f, g, h As Integer
        Dim NumSealCount As Integer = 0

        Try

            Dim SealSplit() As String
            SealSplit = Split(Seal_No.Text, ",")
            For i = 0 To SealSplit.Length - 1
                Dim SealSp() As String
                SealSp = Split(SealSplit(i), "-")

                If SealSp.Length > 1 Then


                    ee = SealSp(0) + SealSp(1)
                    f = Strings.Right(SealSp(0).ToString, SealSp(1).ToString.Length)
                    g = (SealSp(1))
                    NumSealCount += g - f
                    NumSealCount += 1
                    SealCount.Text = NumSealCount

                Else
                    NumSealCount += 1
                    SealCount.Text = NumSealCount
                End If

            Next

        Catch ex As Exception

        End Try




        'Seal1 =GetFirstName(Seal_no.Text,',',1);
        'Seal2 =GetFirstName(Seal_no.Text,',',2);

        'a:=GetFirstName(Seal1,'-',1);
        'b:=GetFirstName(Seal1,'-',2);
        '//if b ='' then sealsum.text := '1';


        'c:=GetFirstName(Seal2,'-',1);
        'd:=GetFirstName(Seal2,'-',2);
        '  Try
        '      e:=strtoint(Copy(a,(length(a)-Length(b)+1),length(b)));
        '      g:=(strtoint(b)-e)+1;
        '      f:=strtoint(Copy(c,(length(c)-Length(d)+1),length(d)));
        '      h:=(strtoint(d)-f)+1;
        '      Sealsum.Text := inttostr(g+h);
        '      if b=''then
        '      Sealsum.Text:=inttostr(Strtoint(Sealsum.Text)+1);
        '          except()

        '          Try
        '        f:=strtoint(Copy(c,(length(c)-Length(d)+1),length(d)));
        '        h:=(strtoint(d)-f)+1;
        '              except()
        '      end;
        '    if c<>'' then Sealsum.Text := inttostr(g+1)
        '    else   Sealsum.Text := inttostr(g);
        '    //if b ='' then sealsum.text := '1';
        '    if b ='' then sealsum.text := inttostr(h+1);

        '    if (a<>'') and (b='') and (c<>'') and (d='') then sealsum.text:='2';


        '    //if a ='' then

        'end;


    End Sub


    Private Sub Bay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bay.SelectedIndexChanged
        If zDoNotDoEvent = True Then Exit Sub
        OrderBut.PerformClick()
    End Sub
    Private Sub Order_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Order.Leave
        If zDoNotDoEvent = True Then Exit Sub
        OrderBut.PerformClick()
    End Sub

    Private Sub PlantShow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlantShow.CheckedChanged
        If zDoNotDoEvent = True Then Exit Sub
        EDFillter.Text = ""
        If PlantShow.Checked Then
            PlantFilter = ""

        Else
            PlantFilter = " (PLANT='" & TSETTINGBindingSource(0)("PLANT").ToString & "' or PLANT='') "

        End If
        V_LoadingnoteBindingSource.Filter = PlantFilter
        EDFillter.Text = ""

    End Sub

    Private Sub BTG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTG.Click
        Try

            Dim q As String
            Dim SqlCon As New SqlClient.SqlConnection
            SqlCon.ConnectionString = My.Settings.FPTConnectionString
            SqlCon.Open()
            q = "Update T_LIGHTCONTROL set TAG_CMD_VAL= ~ cast(TAG_CMD_VAL as bit) WHERE CODE='LIGHTIN'"
            Dim da As New SqlClient.SqlDataAdapter
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BTR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTR.Click
        Try

            Dim q As String
            Dim SqlCon As New SqlClient.SqlConnection
            SqlCon.ConnectionString = My.Settings.FPTConnectionString
            SqlCon.Open()
            q = "Update T_LIGHTCONTROL set TAG_CMD_VAL= ~ cast(TAG_CMD_VAL as bit) WHERE CODE='LIGHTOUT'"
            Dim da As New SqlClient.SqlDataAdapter
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()

            q = "Update T_LIGHTCONTROL set TAG_CMD_VAL= 0 WHERE CODE='LIGHTIN'"
            '  Dim da As New SqlClient.SqlDataAdapter
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub StatusShow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusShow.CheckedChanged
        If zDoNotDoEvent = True Then Exit Sub
        EDFillter.Text = ""
        If StatusShow.Checked Then
            StatusFilter = ""
        Else
            StatusFilter = " (((LOAD_DOFULL<>'') and (Do_status=0) and (load_status in(1,2,3))) or  ((LOAD_DOFULL='') and (load_status in(1,2)))) "
            '  StatusFilter = " (PLANT='" & TSETTINGBindingSource(0)("PLANT").ToString & "' or PLANT='') "

        End If
        V_LoadingnoteBindingSource.Filter = StatusFilter
    End Sub

    Private Sub WAck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAck.CheckedChanged
        If zDoNotDoEvent = True Then Exit Sub
        If WAck.Checked Then
            loginId = 1
            f02_Login.ShowDialog()
            If loginId = 1 Then
                WAck.Checked = False
            End If
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Try
            'DD = "Loaddate='" + String.Format("{0:yyyy-M-d}", DatetimeDBGrid1.Value) + "'"
            'V_LoadingnoteBindingSource.Filter = DD
            SelectVLoadingNote()
        Catch ex As Exception
        End Try
    End Sub


    Private Sub BTOverrideWight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTOverrideWight.Click
        WeightScal.Text = 0
        WeightIn_Click(sender, e)
        WeightOut_Click(sender, e)
        editweightout_Click(sender, e)
    End Sub

    Private Sub f05_01_Advisenote_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        RegisterHotKey(Me.Handle, 100, MOD_CONTROL, Keys.N)
        RegisterHotKey(Me.Handle, 101, MOD_CONTROL, Keys.E)
        RegisterHotKey(Me.Handle, 102, MOD_CONTROL, Keys.P)
        RegisterHotKey(Me.Handle, 200, MOD_ALT, Keys.S)
        RegisterHotKey(Me.Handle, 201, MOD_CONTROL, Keys.I)
        RegisterHotKey(Me.Handle, 202, MOD_CONTROL, Keys.O)
        RegisterHotKey(Me.Handle, 203, MOD_CONTROL, Keys.S)
        RegisterHotKey(Me.Handle, 204, MOD_CONTROL, Keys.U)
        RegisterHotKey(Me.Handle, 205, MOD_CONTROL, Keys.G)
    End Sub

    Private Sub f05_01_Advisenote_Deactivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Deactivate
        UnregisterHotKey(Me.Handle, 100)
        UnregisterHotKey(Me.Handle, 101)
        UnregisterHotKey(Me.Handle, 102)
        UnregisterHotKey(Me.Handle, 200)
        UnregisterHotKey(Me.Handle, 201)
        UnregisterHotKey(Me.Handle, 202)
        UnregisterHotKey(Me.Handle, 203)
        UnregisterHotKey(Me.Handle, 204)
        UnregisterHotKey(Me.Handle, 205)
    End Sub


    Private Sub BTGetTemp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTGetTemp.Click
        Try
            Dim con As SqlConnection = GetConnection()
            Dim cmd As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Try
                con.Open()
                cmd.Connection = con
                cmd.CommandText = "select BATCH_TEMP1 from T_BATCHMETER where BATCH_NUMBER= (select top 1 LC_METER from T_LOADINGNOTECOMPARTMENT where LC_LOAD='" & EDLoadID.Text & "')"
                da.SelectCommand = cmd
                da.Fill(ds, "T_BATCHMETER")
                Temp.Text = String.Format("{0:#.##}", ds.Tables("T_BATCHMETER").Rows(0)("BATCH_TEMP1"))
            Catch ex As Exception

            End Try
            con.Close()
            con.Dispose()
            cmd.Dispose()
            da.Dispose()
            ds.Dispose()

        Catch ex As Exception

        End Try



    End Sub

    Private Sub BTEsso_Click(sender As System.Object, e As System.EventArgs) Handles BTEsso.Click

        Dim YME As New YME_DATA
        Dim REMARK As String
        Try
            YME = CALL_YME_ESSO(Temp.Text, WeightTotal.Text)


            REMARK = "Density: " & String.Format("{0:0.0000}", YME.LOAD_DENSITY) &
                " อุณหภูมิ: " & String.Format("{0:00.00}", YME.LOAD_TEMP) &
                " .C น้ำหนักต้นทาง: " & String.Format("{0:0,0}", YME.LOAD_WEIGHT) &
                " kgs. " & String.Format("{0:0,0}", YME.LOAD_VOULME) & " Liters."
            Edremark2.Text = REMARK
        Catch ex As Exception

        End Try
    End Sub
    Function CALL_YME_ESSO(ByVal LOAD_TEMP As Single, ByVal LOAD_WEIGHT As Single) As YME_DATA
        Dim con As SqlConnection = GetConnection()
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Try
            con.Open()
            cmd.Connection = con
            cmd.CommandText = "select * from T_YME_ESSO where TANK_NO in(select TANK_NO from T_LOADTANK) order by TEMP"
            da.SelectCommand = cmd
            da.Fill(ds, "T_YME_ESSO")



            Dim sTEMP() As Single
            Dim sDEN() As Single
            ReDim sTEMP(ds.Tables("T_YME_ESSO").Rows.Count - 1)
            ReDim sDEN(ds.Tables("T_YME_ESSO").Rows.Count - 1)
            For i As Integer = 0 To ds.Tables("T_YME_ESSO").Rows.Count - 1
                sTEMP(i) = ds.Tables("T_YME_ESSO").Rows(i)("TEMP")
                sDEN(i) = ds.Tables("T_YME_ESSO").Rows(i)("DENSITY")
            Next

            Dim xlApp As New Microsoft.Office.Interop.Excel.Application
            Dim Results As Array
            CALL_YME_ESSO.LOAD_TEMP = LOAD_TEMP
            CALL_YME_ESSO.LOAD_WEIGHT = LOAD_WEIGHT
            Results = xlApp.WorksheetFunction.LinEst(sDEN, sTEMP)

            CALL_YME_ESSO.LOAD_DENSITY = (Results(1) * CALL_YME_ESSO.LOAD_TEMP) + Results(2)
            CALL_YME_ESSO.LOAD_VOULME = Int(CALL_YME_ESSO.LOAD_WEIGHT / CALL_YME_ESSO.LOAD_DENSITY)

        Catch ex As Exception

        End Try
        con.Close()
        con.Dispose()
        cmd.Dispose()
        da.Dispose()
        ds.Dispose()
    End Function

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick

        RadPanel7.Visible = Not RadPanel7.Visible
        RadPanel7.Enabled = True

        RadPanel8.Visible = False
        RadPanel8.Enabled = True
    End Sub

    Private Sub d_Exdate_ValueChanged(sender As System.Object, e As System.EventArgs) Handles d_Exdate.ValueChanged
        If d_Exdate.Value.AddDays(-30) >= Now Then
            Timer1.Enabled = False
            RadPanel7.Visible = False
            RadPanel7.Enabled = True
            RadPanel8.Visible = True
            RadPanel8.Enabled = True

        Else
            Timer1.Enabled = True
            RadPanel7.Visible = True
            RadPanel7.Enabled = True
            RadPanel8.Visible = False
            RadPanel8.Enabled = True
        End If
    End Sub
End Class
