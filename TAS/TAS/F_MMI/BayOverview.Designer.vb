﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BayOverview
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BayOverview))
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.H_panel = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.P_Weight = New System.Windows.Forms.Panel()
        Me.W_DATETIME = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.weight_Unit = New System.Windows.Forms.Label()
        Me.weight = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.M_NAME = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.M_TIME = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.M_GROUP = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.M_DATE = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel200 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.VBATCHSTATUSBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Panel201 = New System.Windows.Forms.Panel()
        Me.LLoadStatus3 = New System.Windows.Forms.Label()
        Me.LComm3 = New System.Windows.Forms.Label()
        Me.PComparment_bay3 = New System.Windows.Forms.Panel()
        Me.Bay3_Comp10 = New Gauge.TGauge()
        Me.Bay3_Comp9 = New Gauge.TGauge()
        Me.Bay3_Comp8 = New Gauge.TGauge()
        Me.Bay3_Comp7 = New Gauge.TGauge()
        Me.Bay3_Comp6 = New Gauge.TGauge()
        Me.Bay3_Comp5 = New Gauge.TGauge()
        Me.Bay3_Comp4 = New Gauge.TGauge()
        Me.Bay3_Comp3 = New Gauge.TGauge()
        Me.Bay3_Comp2 = New Gauge.TGauge()
        Me.Bay3_Comp1 = New Gauge.TGauge()
        Me.pVehicle3 = New System.Windows.Forms.PictureBox()
        Me.Panel155 = New System.Windows.Forms.Panel()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.VBATCHSTATUSBindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.Panel156 = New System.Windows.Forms.Panel()
        Me.LLoadStatus4 = New System.Windows.Forms.Label()
        Me.LComm4 = New System.Windows.Forms.Label()
        Me.PComparment_bay4 = New System.Windows.Forms.Panel()
        Me.Bay4_Comp10 = New Gauge.TGauge()
        Me.Bay4_Comp9 = New Gauge.TGauge()
        Me.Bay4_Comp8 = New Gauge.TGauge()
        Me.Bay4_Comp7 = New Gauge.TGauge()
        Me.Bay4_Comp6 = New Gauge.TGauge()
        Me.Bay4_Comp5 = New Gauge.TGauge()
        Me.Bay4_Comp4 = New Gauge.TGauge()
        Me.Bay4_Comp3 = New Gauge.TGauge()
        Me.Bay4_Comp2 = New Gauge.TGauge()
        Me.Bay4_Comp1 = New Gauge.TGauge()
        Me.pVehicle4 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.VBATCHSTATUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel98 = New System.Windows.Forms.Panel()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.pIsland1_batch1 = New System.Windows.Forms.Panel()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Panel96 = New System.Windows.Forms.Panel()
        Me.LLoadStatus1 = New System.Windows.Forms.Label()
        Me.LComm1 = New System.Windows.Forms.Label()
        Me.PComparment_bay1 = New System.Windows.Forms.Panel()
        Me.Bay1_Comp10 = New Gauge.TGauge()
        Me.Bay1_Comp9 = New Gauge.TGauge()
        Me.Bay1_Comp8 = New Gauge.TGauge()
        Me.Bay1_Comp7 = New Gauge.TGauge()
        Me.Bay1_Comp6 = New Gauge.TGauge()
        Me.Bay1_Comp5 = New Gauge.TGauge()
        Me.Bay1_Comp4 = New Gauge.TGauge()
        Me.Bay1_Comp3 = New Gauge.TGauge()
        Me.Bay1_Comp2 = New Gauge.TGauge()
        Me.Bay1_Comp1 = New Gauge.TGauge()
        Me.pVehicle1 = New System.Windows.Forms.PictureBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.VBATCHSTATUSBindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label126 = New System.Windows.Forms.Label()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.Label137 = New System.Windows.Forms.Label()
        Me.Label138 = New System.Windows.Forms.Label()
        Me.Label139 = New System.Windows.Forms.Label()
        Me.Label140 = New System.Windows.Forms.Label()
        Me.Label141 = New System.Windows.Forms.Label()
        Me.Label142 = New System.Windows.Forms.Label()
        Me.Label143 = New System.Windows.Forms.Label()
        Me.Label144 = New System.Windows.Forms.Label()
        Me.Label145 = New System.Windows.Forms.Label()
        Me.Label146 = New System.Windows.Forms.Label()
        Me.Label147 = New System.Windows.Forms.Label()
        Me.Label148 = New System.Windows.Forms.Label()
        Me.Label149 = New System.Windows.Forms.Label()
        Me.Label150 = New System.Windows.Forms.Label()
        Me.Label151 = New System.Windows.Forms.Label()
        Me.Label152 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LLoadStatus5 = New System.Windows.Forms.Label()
        Me.LComm5 = New System.Windows.Forms.Label()
        Me.PComparment_bay5 = New System.Windows.Forms.Panel()
        Me.Bay5_Comp10 = New Gauge.TGauge()
        Me.Bay5_Comp9 = New Gauge.TGauge()
        Me.Bay5_Comp8 = New Gauge.TGauge()
        Me.Bay5_Comp7 = New Gauge.TGauge()
        Me.Bay5_Comp6 = New Gauge.TGauge()
        Me.Bay5_Comp5 = New Gauge.TGauge()
        Me.Bay5_Comp4 = New Gauge.TGauge()
        Me.Bay5_Comp3 = New Gauge.TGauge()
        Me.Bay5_Comp2 = New Gauge.TGauge()
        Me.Bay5_Comp1 = New Gauge.TGauge()
        Me.pVehicle5 = New System.Windows.Forms.PictureBox()
        Me.PISLAND1 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.VBATCHSTATUSBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Panel97 = New System.Windows.Forms.Panel()
        Me.LLoadStatus2 = New System.Windows.Forms.Label()
        Me.LComm2 = New System.Windows.Forms.Label()
        Me.PComparment_bay2 = New System.Windows.Forms.Panel()
        Me.Bay2_Comp10 = New Gauge.TGauge()
        Me.Bay2_Comp9 = New Gauge.TGauge()
        Me.Bay2_Comp8 = New Gauge.TGauge()
        Me.Bay2_Comp7 = New Gauge.TGauge()
        Me.Bay2_Comp6 = New Gauge.TGauge()
        Me.Bay2_Comp5 = New Gauge.TGauge()
        Me.Bay2_Comp4 = New Gauge.TGauge()
        Me.Bay2_Comp3 = New Gauge.TGauge()
        Me.Bay2_Comp2 = New Gauge.TGauge()
        Me.Bay2_Comp1 = New Gauge.TGauge()
        Me.pVehicle2 = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.Label154 = New System.Windows.Forms.Label()
        Me.VBATCHSTATUSBindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label155 = New System.Windows.Forms.Label()
        Me.Label156 = New System.Windows.Forms.Label()
        Me.Label157 = New System.Windows.Forms.Label()
        Me.Label158 = New System.Windows.Forms.Label()
        Me.Label159 = New System.Windows.Forms.Label()
        Me.Label160 = New System.Windows.Forms.Label()
        Me.Label161 = New System.Windows.Forms.Label()
        Me.Label153 = New System.Windows.Forms.Label()
        Me.Label162 = New System.Windows.Forms.Label()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.Label163 = New System.Windows.Forms.Label()
        Me.Label164 = New System.Windows.Forms.Label()
        Me.Label165 = New System.Windows.Forms.Label()
        Me.Label166 = New System.Windows.Forms.Label()
        Me.Label167 = New System.Windows.Forms.Label()
        Me.Label168 = New System.Windows.Forms.Label()
        Me.Label169 = New System.Windows.Forms.Label()
        Me.Label170 = New System.Windows.Forms.Label()
        Me.Label171 = New System.Windows.Forms.Label()
        Me.Label172 = New System.Windows.Forms.Label()
        Me.Label173 = New System.Windows.Forms.Label()
        Me.Label174 = New System.Windows.Forms.Label()
        Me.Label175 = New System.Windows.Forms.Label()
        Me.Label176 = New System.Windows.Forms.Label()
        Me.Label177 = New System.Windows.Forms.Label()
        Me.Label178 = New System.Windows.Forms.Label()
        Me.Label179 = New System.Windows.Forms.Label()
        Me.Label180 = New System.Windows.Forms.Label()
        Me.Label181 = New System.Windows.Forms.Label()
        Me.Panel117 = New System.Windows.Forms.Panel()
        Me.LLoadStatus6 = New System.Windows.Forms.Label()
        Me.LComm6 = New System.Windows.Forms.Label()
        Me.PComparment_bay6 = New System.Windows.Forms.Panel()
        Me.Bay6_Comp10 = New Gauge.TGauge()
        Me.Bay6_Comp9 = New Gauge.TGauge()
        Me.Bay6_Comp8 = New Gauge.TGauge()
        Me.Bay6_Comp7 = New Gauge.TGauge()
        Me.Bay6_Comp6 = New Gauge.TGauge()
        Me.Bay6_Comp5 = New Gauge.TGauge()
        Me.Bay6_Comp4 = New Gauge.TGauge()
        Me.Bay6_Comp3 = New Gauge.TGauge()
        Me.Bay6_Comp2 = New Gauge.TGauge()
        Me.Bay6_Comp1 = New Gauge.TGauge()
        Me.pVehicle6 = New System.Windows.Forms.PictureBox()
        Me.V_BATCH_STATUSTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_BATCH_STATUSTableAdapter()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.H_panel.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.P_Weight.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel200.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel15.SuspendLayout()
        CType(Me.VBATCHSTATUSBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        Me.Panel201.SuspendLayout()
        Me.PComparment_bay3.SuspendLayout()
        CType(Me.pVehicle3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel155.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel18.SuspendLayout()
        CType(Me.VBATCHSTATUSBindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel19.SuspendLayout()
        Me.Panel156.SuspendLayout()
        Me.PComparment_bay4.SuspendLayout()
        CType(Me.pVehicle4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.VBATCHSTATUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel98.SuspendLayout()
        Me.pIsland1_batch1.SuspendLayout()
        Me.Panel96.SuspendLayout()
        Me.PComparment_bay1.SuspendLayout()
        CType(Me.pVehicle1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.Panel21.SuspendLayout()
        CType(Me.VBATCHSTATUSBindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel22.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PComparment_bay5.SuspendLayout()
        CType(Me.pVehicle5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PISLAND1.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel12.SuspendLayout()
        CType(Me.VBATCHSTATUSBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel13.SuspendLayout()
        Me.Panel97.SuspendLayout()
        Me.PComparment_bay2.SuspendLayout()
        CType(Me.pVehicle2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.Panel24.SuspendLayout()
        CType(Me.VBATCHSTATUSBindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel25.SuspendLayout()
        Me.Panel117.SuspendLayout()
        Me.PComparment_bay6.SuspendLayout()
        CType(Me.pVehicle6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.H_panel, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 137.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1354, 733)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'H_panel
        '
        Me.H_panel.Controls.Add(Me.TableLayoutPanel4)
        Me.H_panel.Controls.Add(Me.TableLayoutPanel3)
        Me.H_panel.Controls.Add(Me.Label27)
        Me.H_panel.Controls.Add(Me.PictureBox8)
        Me.H_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.H_panel.Location = New System.Drawing.Point(4, 4)
        Me.H_panel.Name = "H_panel"
        Me.H_panel.Size = New System.Drawing.Size(1346, 131)
        Me.H_panel.TabIndex = 19
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.P_Weight, 0, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(441, 58)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(464, 70)
        Me.TableLayoutPanel4.TabIndex = 29
        '
        'P_Weight
        '
        Me.P_Weight.Controls.Add(Me.W_DATETIME)
        Me.P_Weight.Controls.Add(Me.Label6)
        Me.P_Weight.Controls.Add(Me.weight_Unit)
        Me.P_Weight.Controls.Add(Me.weight)
        Me.P_Weight.Controls.Add(Me.Label7)
        Me.P_Weight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.P_Weight.Location = New System.Drawing.Point(4, 4)
        Me.P_Weight.Name = "P_Weight"
        Me.P_Weight.Size = New System.Drawing.Size(456, 62)
        Me.P_Weight.TabIndex = 27
        '
        'W_DATETIME
        '
        Me.W_DATETIME.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.W_DATETIME.AutoSize = True
        Me.W_DATETIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.W_DATETIME.ForeColor = System.Drawing.Color.White
        Me.W_DATETIME.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.W_DATETIME.Location = New System.Drawing.Point(63, 48)
        Me.W_DATETIME.Name = "W_DATETIME"
        Me.W_DATETIME.Size = New System.Drawing.Size(114, 13)
        Me.W_DATETIME.TabIndex = 32
        Me.W_DATETIME.Text = "dd/MM/yyyy hh:mm:ss"
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(10, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 13)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Updated : "
        '
        'weight_Unit
        '
        Me.weight_Unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weight_Unit.AutoSize = True
        Me.weight_Unit.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight_Unit.ForeColor = System.Drawing.Color.Lime
        Me.weight_Unit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight_Unit.Location = New System.Drawing.Point(376, 2)
        Me.weight_Unit.Name = "weight_Unit"
        Me.weight_Unit.Size = New System.Drawing.Size(77, 52)
        Me.weight_Unit.TabIndex = 31
        Me.weight_Unit.Text = "KG"
        '
        'weight
        '
        Me.weight.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight.ForeColor = System.Drawing.Color.Lime
        Me.weight.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight.Location = New System.Drawing.Point(167, 2)
        Me.weight.Name = "weight"
        Me.weight.Size = New System.Drawing.Size(210, 52)
        Me.weight.TabIndex = 30
        Me.weight.Text = "0"
        Me.weight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 2)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(173, 52)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "WEIGHT"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel7, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel8, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel9, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel10, 0, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(1099, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(247, 129)
        Me.TableLayoutPanel3.TabIndex = 28
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.Controls.Add(Me.Label15)
        Me.Panel7.Controls.Add(Me.M_NAME)
        Me.Panel7.Location = New System.Drawing.Point(4, 68)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(239, 25)
        Me.Panel7.TabIndex = 42
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(17, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(63, 20)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "NAME :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_NAME
        '
        Me.M_NAME.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_NAME.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_NAME.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_NAME.Location = New System.Drawing.Point(76, 1)
        Me.M_NAME.Name = "M_NAME"
        Me.M_NAME.Size = New System.Drawing.Size(159, 24)
        Me.M_NAME.TabIndex = 36
        Me.M_NAME.Text = "NONE"
        Me.M_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.Controls.Add(Me.Label21)
        Me.Panel8.Controls.Add(Me.M_TIME)
        Me.Panel8.Location = New System.Drawing.Point(4, 4)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(239, 25)
        Me.Panel8.TabIndex = 41
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(21, 4)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(59, 20)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "TIME  :"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_TIME
        '
        Me.M_TIME.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_TIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_TIME.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_TIME.Location = New System.Drawing.Point(76, 1)
        Me.M_TIME.Name = "M_TIME"
        Me.M_TIME.Size = New System.Drawing.Size(159, 26)
        Me.M_TIME.TabIndex = 36
        Me.M_TIME.Text = "HH:MM:SS"
        Me.M_TIME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel9
        '
        Me.Panel9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel9.Controls.Add(Me.Label22)
        Me.Panel9.Controls.Add(Me.M_GROUP)
        Me.Panel9.Location = New System.Drawing.Point(4, 100)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(239, 25)
        Me.Panel9.TabIndex = 43
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(4, 4)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(76, 20)
        Me.Label22.TabIndex = 37
        Me.Label22.Text = "GROUP :"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_GROUP
        '
        Me.M_GROUP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_GROUP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_GROUP.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_GROUP.Location = New System.Drawing.Point(76, 1)
        Me.M_GROUP.Name = "M_GROUP"
        Me.M_GROUP.Size = New System.Drawing.Size(159, 25)
        Me.M_GROUP.TabIndex = 36
        Me.M_GROUP.Text = "NONE"
        Me.M_GROUP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.Controls.Add(Me.Label24)
        Me.Panel10.Controls.Add(Me.M_DATE)
        Me.Panel10.Location = New System.Drawing.Point(4, 36)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(239, 25)
        Me.Panel10.TabIndex = 40
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(20, 4)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(60, 20)
        Me.Label24.TabIndex = 37
        Me.Label24.Text = "DATE :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_DATE
        '
        Me.M_DATE.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_DATE.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_DATE.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_DATE.Location = New System.Drawing.Point(76, 1)
        Me.M_DATE.Name = "M_DATE"
        Me.M_DATE.Size = New System.Drawing.Size(159, 26)
        Me.M_DATE.TabIndex = 36
        Me.M_DATE.Text = "dd/mm/yyyy"
        Me.M_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(590, 16)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(263, 37)
        Me.Label27.TabIndex = 25
        Me.Label27.Text = "BAY OVERVIEW"
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.InitialImage = Nothing
        Me.PictureBox8.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(323, 129)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 15
        Me.PictureBox8.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(4, 142)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1346, 587)
        Me.Panel2.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 6
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.6675!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.6675!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.6675!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66583!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66583!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66583!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel200, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel155, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel5, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PISLAND1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel6, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1346, 587)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel200
        '
        Me.Panel200.Controls.Add(Me.Panel14)
        Me.Panel200.Controls.Add(Me.Panel201)
        Me.Panel200.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel200.Location = New System.Drawing.Point(452, 4)
        Me.Panel200.Name = "Panel200"
        Me.Panel200.Size = New System.Drawing.Size(217, 579)
        Me.Panel200.TabIndex = 39
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.Panel15)
        Me.Panel14.Controls.Add(Me.Label44)
        Me.Panel14.Controls.Add(Me.Label72)
        Me.Panel14.Controls.Add(Me.Panel16)
        Me.Panel14.Controls.Add(Me.Label94)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel14.Location = New System.Drawing.Point(0, 0)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(217, 345)
        Me.Panel14.TabIndex = 45
        '
        'Panel15
        '
        Me.Panel15.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel15.BackColor = System.Drawing.Color.Transparent
        Me.Panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel15.Controls.Add(Me.Label45)
        Me.Panel15.Controls.Add(Me.Label46)
        Me.Panel15.Controls.Add(Me.Label47)
        Me.Panel15.Controls.Add(Me.Label65)
        Me.Panel15.Controls.Add(Me.Label66)
        Me.Panel15.Controls.Add(Me.Label67)
        Me.Panel15.Controls.Add(Me.Label68)
        Me.Panel15.Controls.Add(Me.Label71)
        Me.Panel15.Location = New System.Drawing.Point(-4, 103)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(227, 72)
        Me.Panel15.TabIndex = 77
        '
        'Label45
        '
        Me.Label45.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "DRIVER_PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label45.Location = New System.Drawing.Point(154, 51)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(51, 16)
        Me.Label45.TabIndex = 45
        Me.Label45.Text = "Preset"
        '
        'VBATCHSTATUSBindingSource2
        '
        Me.VBATCHSTATUSBindingSource2.DataMember = "V_BATCH_STATUS"
        Me.VBATCHSTATUSBindingSource2.DataSource = Me.FPTDataSet
        Me.VBATCHSTATUSBindingSource2.Filter = "BATCH_NUMBER=3"
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label46
        '
        Me.Label46.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "DRIVER_PASSWORD", True))
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label46.Location = New System.Drawing.Point(154, 30)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(51, 16)
        Me.Label46.TabIndex = 44
        Me.Label46.Text = "Preset"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(154, 7)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(53, 16)
        Me.Label47.TabIndex = 43
        Me.Label47.Text = "Opertor"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(79, 7)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(42, 16)
        Me.Label65.TabIndex = 42
        Me.Label65.Text = "Batch"
        '
        'Label66
        '
        Me.Label66.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label66.Location = New System.Drawing.Point(79, 51)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(51, 16)
        Me.Label66.TabIndex = 41
        Me.Label66.Text = "Preset"
        '
        'Label67
        '
        Me.Label67.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "PASSWORD", True))
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label67.Location = New System.Drawing.Point(79, 30)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(51, 16)
        Me.Label67.TabIndex = 40
        Me.Label67.Text = "Preset"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.White
        Me.Label68.Location = New System.Drawing.Point(26, 50)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(53, 16)
        Me.Label68.TabIndex = 35
        Me.Label68.Text = "Preset :"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(5, 29)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(74, 16)
        Me.Label71.TabIndex = 34
        Me.Label71.Text = "Password :"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "Product_code", True))
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.Yellow
        Me.Label44.Location = New System.Drawing.Point(106, 72)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(54, 24)
        Me.Label44.TabIndex = 72
        Me.Label44.Text = " TEG"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "BATCH_NAME", True))
        Me.Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label72.ForeColor = System.Drawing.Color.Yellow
        Me.Label72.Location = New System.Drawing.Point(85, 44)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(100, 24)
        Me.Label72.TabIndex = 71
        Me.Label72.Text = "FQIC-1513"
        '
        'Panel16
        '
        Me.Panel16.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel16.BackColor = System.Drawing.Color.Transparent
        Me.Panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel16.Controls.Add(Me.Label73)
        Me.Panel16.Controls.Add(Me.Label74)
        Me.Panel16.Controls.Add(Me.Label78)
        Me.Panel16.Controls.Add(Me.Label79)
        Me.Panel16.Controls.Add(Me.Label80)
        Me.Panel16.Controls.Add(Me.Label81)
        Me.Panel16.Controls.Add(Me.Label82)
        Me.Panel16.Controls.Add(Me.Label83)
        Me.Panel16.Controls.Add(Me.Label84)
        Me.Panel16.Controls.Add(Me.Label85)
        Me.Panel16.Controls.Add(Me.Label86)
        Me.Panel16.Controls.Add(Me.Label87)
        Me.Panel16.Controls.Add(Me.Label88)
        Me.Panel16.Controls.Add(Me.Label89)
        Me.Panel16.Controls.Add(Me.Label90)
        Me.Panel16.Controls.Add(Me.Label91)
        Me.Panel16.Controls.Add(Me.Label92)
        Me.Panel16.Controls.Add(Me.Label93)
        Me.Panel16.Location = New System.Drawing.Point(-4, 181)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(227, 158)
        Me.Panel16.TabIndex = 74
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.ForeColor = System.Drawing.Color.White
        Me.Label73.Location = New System.Drawing.Point(156, 131)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(24, 16)
        Me.Label73.TabIndex = 51
        Me.Label73.Text = "Kg"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.White
        Me.Label74.Location = New System.Drawing.Point(156, 107)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(24, 16)
        Me.Label74.TabIndex = 50
        Me.Label74.Text = "Kg"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label78.ForeColor = System.Drawing.Color.White
        Me.Label78.Location = New System.Drawing.Point(156, 84)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(46, 16)
        Me.Label78.TabIndex = 49
        Me.Label78.Text = "Kg/m3"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.ForeColor = System.Drawing.Color.White
        Me.Label79.Location = New System.Drawing.Point(156, 59)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(39, 16)
        Me.Label79.TabIndex = 48
        Me.Label79.Text = "Kg/hr"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.ForeColor = System.Drawing.Color.White
        Me.Label80.Location = New System.Drawing.Point(156, 36)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(24, 16)
        Me.Label80.TabIndex = 47
        Me.Label80.Text = "Kg"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label81.ForeColor = System.Drawing.Color.White
        Me.Label81.Location = New System.Drawing.Point(156, 14)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(24, 16)
        Me.Label81.TabIndex = 46
        Me.Label81.Text = "Kg"
        '
        'Label82
        '
        Me.Label82.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "BASE_ACCUM_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label82.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label82.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label82.Location = New System.Drawing.Point(79, 132)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(78, 16)
        Me.Label82.TabIndex = 45
        Me.Label82.Text = "Preset"
        '
        'Label83
        '
        Me.Label83.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "BASE_T", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label83.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label83.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label83.Location = New System.Drawing.Point(79, 108)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(78, 16)
        Me.Label83.TabIndex = 44
        Me.Label83.Text = "Preset"
        '
        'Label84
        '
        Me.Label84.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "DENSITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label84.Location = New System.Drawing.Point(79, 84)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(78, 16)
        Me.Label84.TabIndex = 43
        Me.Label84.Text = "Preset"
        '
        'Label85
        '
        Me.Label85.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "BASE_F", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label85.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label85.Location = New System.Drawing.Point(79, 60)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(78, 16)
        Me.Label85.TabIndex = 42
        Me.Label85.Text = "Preset"
        '
        'Label86
        '
        Me.Label86.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "BASE_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label86.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label86.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label86.Location = New System.Drawing.Point(79, 36)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(78, 16)
        Me.Label86.TabIndex = 41
        Me.Label86.Text = "Preset"
        '
        'Label87
        '
        Me.Label87.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource2, "BASE_P", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label87.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label87.Location = New System.Drawing.Point(79, 12)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(78, 16)
        Me.Label87.TabIndex = 40
        Me.Label87.Text = "Preset"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.ForeColor = System.Drawing.Color.White
        Me.Label88.Location = New System.Drawing.Point(21, 131)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(58, 16)
        Me.Label88.TabIndex = 39
        Me.Label88.Text = "Accum. :"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label89.ForeColor = System.Drawing.Color.White
        Me.Label89.Location = New System.Drawing.Point(26, 107)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(53, 16)
        Me.Label89.TabIndex = 38
        Me.Label89.Text = "Temp. :"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label90.ForeColor = System.Drawing.Color.White
        Me.Label90.Location = New System.Drawing.Point(20, 83)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(59, 16)
        Me.Label90.TabIndex = 37
        Me.Label90.Text = "Density :"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label91.ForeColor = System.Drawing.Color.White
        Me.Label91.Location = New System.Drawing.Point(11, 59)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(68, 16)
        Me.Label91.TabIndex = 36
        Me.Label91.Text = "Flow rate :"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label92.ForeColor = System.Drawing.Color.White
        Me.Label92.Location = New System.Drawing.Point(23, 35)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(56, 16)
        Me.Label92.TabIndex = 35
        Me.Label92.Text = "Current :"
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.ForeColor = System.Drawing.Color.White
        Me.Label93.Location = New System.Drawing.Point(26, 11)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(53, 16)
        Me.Label93.TabIndex = 34
        Me.Label93.Text = "Preset :"
        '
        'Label94
        '
        Me.Label94.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Label94.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label94.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label94.ForeColor = System.Drawing.Color.White
        Me.Label94.Location = New System.Drawing.Point(0, 0)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(217, 36)
        Me.Label94.TabIndex = 73
        Me.Label94.Text = "BAY 3"
        Me.Label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel201
        '
        Me.Panel201.Controls.Add(Me.LLoadStatus3)
        Me.Panel201.Controls.Add(Me.LComm3)
        Me.Panel201.Controls.Add(Me.PComparment_bay3)
        Me.Panel201.Controls.Add(Me.pVehicle3)
        Me.Panel201.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel201.Location = New System.Drawing.Point(0, 54)
        Me.Panel201.Name = "Panel201"
        Me.Panel201.Size = New System.Drawing.Size(217, 525)
        Me.Panel201.TabIndex = 43
        '
        'LLoadStatus3
        '
        Me.LLoadStatus3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLoadStatus3.ForeColor = System.Drawing.Color.Magenta
        Me.LLoadStatus3.Location = New System.Drawing.Point(52, 69)
        Me.LLoadStatus3.Name = "LLoadStatus3"
        Me.LLoadStatus3.Size = New System.Drawing.Size(171, 20)
        Me.LLoadStatus3.TabIndex = 76
        Me.LLoadStatus3.Text = "Loading"
        Me.LLoadStatus3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LComm3
        '
        Me.LComm3.AutoSize = True
        Me.LComm3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LComm3.ForeColor = System.Drawing.Color.Lime
        Me.LComm3.Location = New System.Drawing.Point(53, 48)
        Me.LComm3.Name = "LComm3"
        Me.LComm3.Size = New System.Drawing.Size(171, 20)
        Me.LComm3.TabIndex = 75
        Me.LComm3.Text = "Good Communications"
        '
        'PComparment_bay3
        '
        Me.PComparment_bay3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PComparment_bay3.BackColor = System.Drawing.Color.Silver
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp10)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp9)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp8)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp7)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp6)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp5)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp4)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp3)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp2)
        Me.PComparment_bay3.Controls.Add(Me.Bay3_Comp1)
        Me.PComparment_bay3.Location = New System.Drawing.Point(83, 221)
        Me.PComparment_bay3.Name = "PComparment_bay3"
        Me.PComparment_bay3.Size = New System.Drawing.Size(50, 218)
        Me.PComparment_bay3.TabIndex = 66
        '
        'Bay3_Comp10
        '
        Me.Bay3_Comp10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp10.Location = New System.Drawing.Point(0, 198)
        Me.Bay3_Comp10.MaxValue = 10000.0R
        Me.Bay3_Comp10.MinValue = 0.0R
        Me.Bay3_Comp10.Name = "Bay3_Comp10"
        Me.Bay3_Comp10.Progress = 30.0R
        Me.Bay3_Comp10.Size = New System.Drawing.Size(50, 20)
        Me.Bay3_Comp10.TabIndex = 16
        '
        'Bay3_Comp9
        '
        Me.Bay3_Comp9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp9.Location = New System.Drawing.Point(0, 176)
        Me.Bay3_Comp9.MaxValue = 10000.0R
        Me.Bay3_Comp9.MinValue = 0.0R
        Me.Bay3_Comp9.Name = "Bay3_Comp9"
        Me.Bay3_Comp9.Progress = 30.0R
        Me.Bay3_Comp9.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp9.TabIndex = 15
        '
        'Bay3_Comp8
        '
        Me.Bay3_Comp8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp8.Location = New System.Drawing.Point(0, 154)
        Me.Bay3_Comp8.MaxValue = 10000.0R
        Me.Bay3_Comp8.MinValue = 0.0R
        Me.Bay3_Comp8.Name = "Bay3_Comp8"
        Me.Bay3_Comp8.Progress = 30.0R
        Me.Bay3_Comp8.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp8.TabIndex = 14
        '
        'Bay3_Comp7
        '
        Me.Bay3_Comp7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp7.Location = New System.Drawing.Point(0, 132)
        Me.Bay3_Comp7.MaxValue = 10000.0R
        Me.Bay3_Comp7.MinValue = 0.0R
        Me.Bay3_Comp7.Name = "Bay3_Comp7"
        Me.Bay3_Comp7.Progress = 30.0R
        Me.Bay3_Comp7.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp7.TabIndex = 13
        '
        'Bay3_Comp6
        '
        Me.Bay3_Comp6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp6.Location = New System.Drawing.Point(0, 110)
        Me.Bay3_Comp6.MaxValue = 10000.0R
        Me.Bay3_Comp6.MinValue = 0.0R
        Me.Bay3_Comp6.Name = "Bay3_Comp6"
        Me.Bay3_Comp6.Progress = 30.0R
        Me.Bay3_Comp6.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp6.TabIndex = 12
        '
        'Bay3_Comp5
        '
        Me.Bay3_Comp5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp5.Location = New System.Drawing.Point(0, 88)
        Me.Bay3_Comp5.MaxValue = 10000.0R
        Me.Bay3_Comp5.MinValue = 0.0R
        Me.Bay3_Comp5.Name = "Bay3_Comp5"
        Me.Bay3_Comp5.Progress = 30.0R
        Me.Bay3_Comp5.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp5.TabIndex = 11
        '
        'Bay3_Comp4
        '
        Me.Bay3_Comp4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp4.Location = New System.Drawing.Point(0, 66)
        Me.Bay3_Comp4.MaxValue = 10000.0R
        Me.Bay3_Comp4.MinValue = 0.0R
        Me.Bay3_Comp4.Name = "Bay3_Comp4"
        Me.Bay3_Comp4.Progress = 30.0R
        Me.Bay3_Comp4.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp4.TabIndex = 10
        '
        'Bay3_Comp3
        '
        Me.Bay3_Comp3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp3.Location = New System.Drawing.Point(0, 44)
        Me.Bay3_Comp3.MaxValue = 10000.0R
        Me.Bay3_Comp3.MinValue = 0.0R
        Me.Bay3_Comp3.Name = "Bay3_Comp3"
        Me.Bay3_Comp3.Progress = 30.0R
        Me.Bay3_Comp3.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp3.TabIndex = 9
        '
        'Bay3_Comp2
        '
        Me.Bay3_Comp2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay3_Comp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp2.Location = New System.Drawing.Point(0, 22)
        Me.Bay3_Comp2.MaxValue = 10000.0R
        Me.Bay3_Comp2.MinValue = 0.0R
        Me.Bay3_Comp2.Name = "Bay3_Comp2"
        Me.Bay3_Comp2.Progress = 30.0R
        Me.Bay3_Comp2.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp2.TabIndex = 8
        '
        'Bay3_Comp1
        '
        Me.Bay3_Comp1.BackColor = System.Drawing.Color.DarkRed
        Me.Bay3_Comp1.Color = System.Drawing.Color.DarkRed
        Me.Bay3_Comp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay3_Comp1.Location = New System.Drawing.Point(0, 0)
        Me.Bay3_Comp1.MaxValue = 100.0R
        Me.Bay3_Comp1.MinValue = 0.0R
        Me.Bay3_Comp1.Name = "Bay3_Comp1"
        Me.Bay3_Comp1.Progress = 80.0R
        Me.Bay3_Comp1.Size = New System.Drawing.Size(50, 22)
        Me.Bay3_Comp1.TabIndex = 7
        '
        'pVehicle3
        '
        Me.pVehicle3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pVehicle3.Image = Global.TAS_TOL.My.Resources.Resources.Truck_Lpg_Topview
        Me.pVehicle3.Location = New System.Drawing.Point(58, 87)
        Me.pVehicle3.Name = "pVehicle3"
        Me.pVehicle3.Size = New System.Drawing.Size(100, 397)
        Me.pVehicle3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pVehicle3.TabIndex = 44
        Me.pVehicle3.TabStop = False
        '
        'Panel155
        '
        Me.Panel155.Controls.Add(Me.Panel17)
        Me.Panel155.Controls.Add(Me.Panel156)
        Me.Panel155.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel155.Location = New System.Drawing.Point(676, 4)
        Me.Panel155.Name = "Panel155"
        Me.Panel155.Size = New System.Drawing.Size(217, 579)
        Me.Panel155.TabIndex = 38
        '
        'Panel17
        '
        Me.Panel17.Controls.Add(Me.Panel18)
        Me.Panel17.Controls.Add(Me.Label95)
        Me.Panel17.Controls.Add(Me.Label104)
        Me.Panel17.Controls.Add(Me.Panel19)
        Me.Panel17.Controls.Add(Me.Label123)
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel17.Location = New System.Drawing.Point(0, 0)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(217, 345)
        Me.Panel17.TabIndex = 45
        '
        'Panel18
        '
        Me.Panel18.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel18.BackColor = System.Drawing.Color.Transparent
        Me.Panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel18.Controls.Add(Me.Label96)
        Me.Panel18.Controls.Add(Me.Label97)
        Me.Panel18.Controls.Add(Me.Label98)
        Me.Panel18.Controls.Add(Me.Label99)
        Me.Panel18.Controls.Add(Me.Label100)
        Me.Panel18.Controls.Add(Me.Label101)
        Me.Panel18.Controls.Add(Me.Label102)
        Me.Panel18.Controls.Add(Me.Label103)
        Me.Panel18.Location = New System.Drawing.Point(-4, 103)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(227, 72)
        Me.Panel18.TabIndex = 77
        '
        'Label96
        '
        Me.Label96.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "DRIVER_PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label96.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label96.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label96.Location = New System.Drawing.Point(154, 51)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(51, 16)
        Me.Label96.TabIndex = 45
        Me.Label96.Text = "Preset"
        '
        'VBATCHSTATUSBindingSource3
        '
        Me.VBATCHSTATUSBindingSource3.DataMember = "V_BATCH_STATUS"
        Me.VBATCHSTATUSBindingSource3.DataSource = Me.FPTDataSet
        Me.VBATCHSTATUSBindingSource3.Filter = "BATCH_NUMBER=4"
        '
        'Label97
        '
        Me.Label97.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "DRIVER_PASSWORD", True))
        Me.Label97.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label97.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label97.Location = New System.Drawing.Point(154, 30)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(51, 16)
        Me.Label97.TabIndex = 44
        Me.Label97.Text = "Preset"
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label98.ForeColor = System.Drawing.Color.White
        Me.Label98.Location = New System.Drawing.Point(154, 7)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(53, 16)
        Me.Label98.TabIndex = 43
        Me.Label98.Text = "Opertor"
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label99.ForeColor = System.Drawing.Color.White
        Me.Label99.Location = New System.Drawing.Point(79, 7)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(42, 16)
        Me.Label99.TabIndex = 42
        Me.Label99.Text = "Batch"
        '
        'Label100
        '
        Me.Label100.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label100.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label100.Location = New System.Drawing.Point(79, 51)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(51, 16)
        Me.Label100.TabIndex = 41
        Me.Label100.Text = "Preset"
        '
        'Label101
        '
        Me.Label101.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "PASSWORD", True))
        Me.Label101.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label101.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label101.Location = New System.Drawing.Point(79, 30)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(51, 16)
        Me.Label101.TabIndex = 40
        Me.Label101.Text = "Preset"
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label102.ForeColor = System.Drawing.Color.White
        Me.Label102.Location = New System.Drawing.Point(26, 50)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(53, 16)
        Me.Label102.TabIndex = 35
        Me.Label102.Text = "Preset :"
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label103.ForeColor = System.Drawing.Color.White
        Me.Label103.Location = New System.Drawing.Point(5, 29)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(74, 16)
        Me.Label103.TabIndex = 34
        Me.Label103.Text = "Password :"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "Product_code", True))
        Me.Label95.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label95.ForeColor = System.Drawing.Color.Yellow
        Me.Label95.Location = New System.Drawing.Point(106, 72)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(54, 24)
        Me.Label95.TabIndex = 72
        Me.Label95.Text = " TEG"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "BATCH_NAME", True))
        Me.Label104.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label104.ForeColor = System.Drawing.Color.Yellow
        Me.Label104.Location = New System.Drawing.Point(85, 44)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(100, 24)
        Me.Label104.TabIndex = 71
        Me.Label104.Text = "FQIC-1513"
        '
        'Panel19
        '
        Me.Panel19.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel19.BackColor = System.Drawing.Color.Transparent
        Me.Panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel19.Controls.Add(Me.Label105)
        Me.Panel19.Controls.Add(Me.Label106)
        Me.Panel19.Controls.Add(Me.Label107)
        Me.Panel19.Controls.Add(Me.Label108)
        Me.Panel19.Controls.Add(Me.Label109)
        Me.Panel19.Controls.Add(Me.Label110)
        Me.Panel19.Controls.Add(Me.Label111)
        Me.Panel19.Controls.Add(Me.Label112)
        Me.Panel19.Controls.Add(Me.Label113)
        Me.Panel19.Controls.Add(Me.Label114)
        Me.Panel19.Controls.Add(Me.Label115)
        Me.Panel19.Controls.Add(Me.Label116)
        Me.Panel19.Controls.Add(Me.Label117)
        Me.Panel19.Controls.Add(Me.Label118)
        Me.Panel19.Controls.Add(Me.Label119)
        Me.Panel19.Controls.Add(Me.Label120)
        Me.Panel19.Controls.Add(Me.Label121)
        Me.Panel19.Controls.Add(Me.Label122)
        Me.Panel19.Location = New System.Drawing.Point(-4, 181)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(227, 158)
        Me.Panel19.TabIndex = 74
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.ForeColor = System.Drawing.Color.White
        Me.Label105.Location = New System.Drawing.Point(156, 131)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(24, 16)
        Me.Label105.TabIndex = 51
        Me.Label105.Text = "Kg"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label106.ForeColor = System.Drawing.Color.White
        Me.Label106.Location = New System.Drawing.Point(156, 107)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(24, 16)
        Me.Label106.TabIndex = 50
        Me.Label106.Text = "Kg"
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.ForeColor = System.Drawing.Color.White
        Me.Label107.Location = New System.Drawing.Point(156, 84)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(46, 16)
        Me.Label107.TabIndex = 49
        Me.Label107.Text = "Kg/m3"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label108.ForeColor = System.Drawing.Color.White
        Me.Label108.Location = New System.Drawing.Point(156, 59)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(39, 16)
        Me.Label108.TabIndex = 48
        Me.Label108.Text = "Kg/hr"
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.ForeColor = System.Drawing.Color.White
        Me.Label109.Location = New System.Drawing.Point(156, 36)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(24, 16)
        Me.Label109.TabIndex = 47
        Me.Label109.Text = "Kg"
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label110.ForeColor = System.Drawing.Color.White
        Me.Label110.Location = New System.Drawing.Point(156, 14)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(24, 16)
        Me.Label110.TabIndex = 46
        Me.Label110.Text = "Kg"
        '
        'Label111
        '
        Me.Label111.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "BASE_ACCUM_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label111.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label111.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label111.Location = New System.Drawing.Point(79, 132)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(78, 16)
        Me.Label111.TabIndex = 45
        Me.Label111.Text = "Preset"
        '
        'Label112
        '
        Me.Label112.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "BASE_T", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label112.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label112.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label112.Location = New System.Drawing.Point(79, 108)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(78, 16)
        Me.Label112.TabIndex = 44
        Me.Label112.Text = "Preset"
        '
        'Label113
        '
        Me.Label113.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "DENSITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label113.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label113.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label113.Location = New System.Drawing.Point(79, 84)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(78, 16)
        Me.Label113.TabIndex = 43
        Me.Label113.Text = "Preset"
        '
        'Label114
        '
        Me.Label114.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "BASE_F", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label114.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label114.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label114.Location = New System.Drawing.Point(79, 60)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(78, 16)
        Me.Label114.TabIndex = 42
        Me.Label114.Text = "Preset"
        '
        'Label115
        '
        Me.Label115.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "BASE_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label115.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label115.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label115.Location = New System.Drawing.Point(79, 36)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(78, 16)
        Me.Label115.TabIndex = 41
        Me.Label115.Text = "Preset"
        '
        'Label116
        '
        Me.Label116.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource3, "BASE_P", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label116.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label116.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label116.Location = New System.Drawing.Point(79, 12)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(78, 16)
        Me.Label116.TabIndex = 40
        Me.Label116.Text = "Preset"
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label117.ForeColor = System.Drawing.Color.White
        Me.Label117.Location = New System.Drawing.Point(21, 131)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(58, 16)
        Me.Label117.TabIndex = 39
        Me.Label117.Text = "Accum. :"
        '
        'Label118
        '
        Me.Label118.AutoSize = True
        Me.Label118.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label118.ForeColor = System.Drawing.Color.White
        Me.Label118.Location = New System.Drawing.Point(26, 107)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(53, 16)
        Me.Label118.TabIndex = 38
        Me.Label118.Text = "Temp. :"
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label119.ForeColor = System.Drawing.Color.White
        Me.Label119.Location = New System.Drawing.Point(20, 83)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(59, 16)
        Me.Label119.TabIndex = 37
        Me.Label119.Text = "Density :"
        '
        'Label120
        '
        Me.Label120.AutoSize = True
        Me.Label120.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label120.ForeColor = System.Drawing.Color.White
        Me.Label120.Location = New System.Drawing.Point(11, 59)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(68, 16)
        Me.Label120.TabIndex = 36
        Me.Label120.Text = "Flow rate :"
        '
        'Label121
        '
        Me.Label121.AutoSize = True
        Me.Label121.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label121.ForeColor = System.Drawing.Color.White
        Me.Label121.Location = New System.Drawing.Point(23, 35)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(56, 16)
        Me.Label121.TabIndex = 35
        Me.Label121.Text = "Current :"
        '
        'Label122
        '
        Me.Label122.AutoSize = True
        Me.Label122.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label122.ForeColor = System.Drawing.Color.White
        Me.Label122.Location = New System.Drawing.Point(26, 11)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(53, 16)
        Me.Label122.TabIndex = 34
        Me.Label122.Text = "Preset :"
        '
        'Label123
        '
        Me.Label123.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Label123.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label123.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label123.ForeColor = System.Drawing.Color.White
        Me.Label123.Location = New System.Drawing.Point(0, 0)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(217, 36)
        Me.Label123.TabIndex = 73
        Me.Label123.Text = "BAY 4"
        Me.Label123.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel156
        '
        Me.Panel156.Controls.Add(Me.LLoadStatus4)
        Me.Panel156.Controls.Add(Me.LComm4)
        Me.Panel156.Controls.Add(Me.PComparment_bay4)
        Me.Panel156.Controls.Add(Me.pVehicle4)
        Me.Panel156.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel156.Location = New System.Drawing.Point(0, 54)
        Me.Panel156.Name = "Panel156"
        Me.Panel156.Size = New System.Drawing.Size(217, 525)
        Me.Panel156.TabIndex = 43
        '
        'LLoadStatus4
        '
        Me.LLoadStatus4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLoadStatus4.ForeColor = System.Drawing.Color.Magenta
        Me.LLoadStatus4.Location = New System.Drawing.Point(44, 69)
        Me.LLoadStatus4.Name = "LLoadStatus4"
        Me.LLoadStatus4.Size = New System.Drawing.Size(171, 20)
        Me.LLoadStatus4.TabIndex = 76
        Me.LLoadStatus4.Text = "Loading"
        Me.LLoadStatus4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LComm4
        '
        Me.LComm4.AutoSize = True
        Me.LComm4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LComm4.ForeColor = System.Drawing.Color.Lime
        Me.LComm4.Location = New System.Drawing.Point(51, 48)
        Me.LComm4.Name = "LComm4"
        Me.LComm4.Size = New System.Drawing.Size(171, 20)
        Me.LComm4.TabIndex = 75
        Me.LComm4.Text = "Good Communications"
        '
        'PComparment_bay4
        '
        Me.PComparment_bay4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PComparment_bay4.BackColor = System.Drawing.Color.Silver
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp10)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp9)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp8)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp7)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp6)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp5)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp4)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp3)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp2)
        Me.PComparment_bay4.Controls.Add(Me.Bay4_Comp1)
        Me.PComparment_bay4.Location = New System.Drawing.Point(77, 221)
        Me.PComparment_bay4.Name = "PComparment_bay4"
        Me.PComparment_bay4.Size = New System.Drawing.Size(50, 218)
        Me.PComparment_bay4.TabIndex = 67
        '
        'Bay4_Comp10
        '
        Me.Bay4_Comp10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp10.Location = New System.Drawing.Point(0, 198)
        Me.Bay4_Comp10.MaxValue = 10000.0R
        Me.Bay4_Comp10.MinValue = 0.0R
        Me.Bay4_Comp10.Name = "Bay4_Comp10"
        Me.Bay4_Comp10.Progress = 30.0R
        Me.Bay4_Comp10.Size = New System.Drawing.Size(50, 20)
        Me.Bay4_Comp10.TabIndex = 16
        '
        'Bay4_Comp9
        '
        Me.Bay4_Comp9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp9.Location = New System.Drawing.Point(0, 176)
        Me.Bay4_Comp9.MaxValue = 10000.0R
        Me.Bay4_Comp9.MinValue = 0.0R
        Me.Bay4_Comp9.Name = "Bay4_Comp9"
        Me.Bay4_Comp9.Progress = 30.0R
        Me.Bay4_Comp9.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp9.TabIndex = 15
        '
        'Bay4_Comp8
        '
        Me.Bay4_Comp8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp8.Location = New System.Drawing.Point(0, 154)
        Me.Bay4_Comp8.MaxValue = 10000.0R
        Me.Bay4_Comp8.MinValue = 0.0R
        Me.Bay4_Comp8.Name = "Bay4_Comp8"
        Me.Bay4_Comp8.Progress = 30.0R
        Me.Bay4_Comp8.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp8.TabIndex = 14
        '
        'Bay4_Comp7
        '
        Me.Bay4_Comp7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp7.Location = New System.Drawing.Point(0, 132)
        Me.Bay4_Comp7.MaxValue = 10000.0R
        Me.Bay4_Comp7.MinValue = 0.0R
        Me.Bay4_Comp7.Name = "Bay4_Comp7"
        Me.Bay4_Comp7.Progress = 30.0R
        Me.Bay4_Comp7.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp7.TabIndex = 13
        '
        'Bay4_Comp6
        '
        Me.Bay4_Comp6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp6.Location = New System.Drawing.Point(0, 110)
        Me.Bay4_Comp6.MaxValue = 10000.0R
        Me.Bay4_Comp6.MinValue = 0.0R
        Me.Bay4_Comp6.Name = "Bay4_Comp6"
        Me.Bay4_Comp6.Progress = 30.0R
        Me.Bay4_Comp6.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp6.TabIndex = 12
        '
        'Bay4_Comp5
        '
        Me.Bay4_Comp5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp5.Location = New System.Drawing.Point(0, 88)
        Me.Bay4_Comp5.MaxValue = 10000.0R
        Me.Bay4_Comp5.MinValue = 0.0R
        Me.Bay4_Comp5.Name = "Bay4_Comp5"
        Me.Bay4_Comp5.Progress = 30.0R
        Me.Bay4_Comp5.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp5.TabIndex = 11
        '
        'Bay4_Comp4
        '
        Me.Bay4_Comp4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp4.Location = New System.Drawing.Point(0, 66)
        Me.Bay4_Comp4.MaxValue = 10000.0R
        Me.Bay4_Comp4.MinValue = 0.0R
        Me.Bay4_Comp4.Name = "Bay4_Comp4"
        Me.Bay4_Comp4.Progress = 30.0R
        Me.Bay4_Comp4.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp4.TabIndex = 10
        '
        'Bay4_Comp3
        '
        Me.Bay4_Comp3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp3.Location = New System.Drawing.Point(0, 44)
        Me.Bay4_Comp3.MaxValue = 10000.0R
        Me.Bay4_Comp3.MinValue = 0.0R
        Me.Bay4_Comp3.Name = "Bay4_Comp3"
        Me.Bay4_Comp3.Progress = 30.0R
        Me.Bay4_Comp3.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp3.TabIndex = 9
        '
        'Bay4_Comp2
        '
        Me.Bay4_Comp2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay4_Comp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp2.Location = New System.Drawing.Point(0, 22)
        Me.Bay4_Comp2.MaxValue = 10000.0R
        Me.Bay4_Comp2.MinValue = 0.0R
        Me.Bay4_Comp2.Name = "Bay4_Comp2"
        Me.Bay4_Comp2.Progress = 30.0R
        Me.Bay4_Comp2.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp2.TabIndex = 8
        '
        'Bay4_Comp1
        '
        Me.Bay4_Comp1.BackColor = System.Drawing.Color.DarkRed
        Me.Bay4_Comp1.Color = System.Drawing.Color.DarkRed
        Me.Bay4_Comp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay4_Comp1.Location = New System.Drawing.Point(0, 0)
        Me.Bay4_Comp1.MaxValue = 100.0R
        Me.Bay4_Comp1.MinValue = 0.0R
        Me.Bay4_Comp1.Name = "Bay4_Comp1"
        Me.Bay4_Comp1.Progress = 80.0R
        Me.Bay4_Comp1.Size = New System.Drawing.Size(50, 22)
        Me.Bay4_Comp1.TabIndex = 7
        '
        'pVehicle4
        '
        Me.pVehicle4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pVehicle4.Image = Global.TAS_TOL.My.Resources.Resources.Truck_Lpg_Topview
        Me.pVehicle4.Location = New System.Drawing.Point(52, 88)
        Me.pVehicle4.Name = "pVehicle4"
        Me.pVehicle4.Size = New System.Drawing.Size(100, 396)
        Me.pVehicle4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pVehicle4.TabIndex = 43
        Me.pVehicle4.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.Panel96)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(217, 579)
        Me.Panel3.TabIndex = 37
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label64)
        Me.Panel4.Controls.Add(Me.Panel98)
        Me.Panel4.Controls.Add(Me.Label63)
        Me.Panel4.Controls.Add(Me.pIsland1_batch1)
        Me.Panel4.Controls.Add(Me.Label77)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(217, 345)
        Me.Panel4.TabIndex = 44
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "Product_code", True))
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.Yellow
        Me.Label64.Location = New System.Drawing.Point(106, 72)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(54, 24)
        Me.Label64.TabIndex = 72
        Me.Label64.Text = " TEG"
        '
        'VBATCHSTATUSBindingSource
        '
        Me.VBATCHSTATUSBindingSource.DataMember = "V_BATCH_STATUS"
        Me.VBATCHSTATUSBindingSource.DataSource = Me.FPTDataSet
        Me.VBATCHSTATUSBindingSource.Filter = "BATCH_NUMBER=1"
        '
        'Panel98
        '
        Me.Panel98.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel98.BackColor = System.Drawing.Color.Transparent
        Me.Panel98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel98.Controls.Add(Me.Label61)
        Me.Panel98.Controls.Add(Me.Label62)
        Me.Panel98.Controls.Add(Me.Label60)
        Me.Panel98.Controls.Add(Me.Label59)
        Me.Panel98.Controls.Add(Me.Label69)
        Me.Panel98.Controls.Add(Me.Label70)
        Me.Panel98.Controls.Add(Me.Label75)
        Me.Panel98.Controls.Add(Me.Label76)
        Me.Panel98.Location = New System.Drawing.Point(-4, 103)
        Me.Panel98.Name = "Panel98"
        Me.Panel98.Size = New System.Drawing.Size(227, 72)
        Me.Panel98.TabIndex = 75
        '
        'Label61
        '
        Me.Label61.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "DRIVER_PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label61.Location = New System.Drawing.Point(154, 51)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(51, 16)
        Me.Label61.TabIndex = 45
        Me.Label61.Text = "Preset"
        '
        'Label62
        '
        Me.Label62.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "DRIVER_PASSWORD", True))
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label62.Location = New System.Drawing.Point(154, 30)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(51, 16)
        Me.Label62.TabIndex = 44
        Me.Label62.Text = "Preset"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.White
        Me.Label60.Location = New System.Drawing.Point(154, 7)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(53, 16)
        Me.Label60.TabIndex = 43
        Me.Label60.Text = "Opertor"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(79, 7)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(42, 16)
        Me.Label59.TabIndex = 42
        Me.Label59.Text = "Batch"
        '
        'Label69
        '
        Me.Label69.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label69.Location = New System.Drawing.Point(79, 51)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(51, 16)
        Me.Label69.TabIndex = 41
        Me.Label69.Text = "Preset"
        '
        'Label70
        '
        Me.Label70.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "PASSWORD", True))
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label70.Location = New System.Drawing.Point(79, 30)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(51, 16)
        Me.Label70.TabIndex = 40
        Me.Label70.Text = "Preset"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.ForeColor = System.Drawing.Color.White
        Me.Label75.Location = New System.Drawing.Point(26, 50)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(53, 16)
        Me.Label75.TabIndex = 35
        Me.Label75.Text = "Preset :"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label76.ForeColor = System.Drawing.Color.White
        Me.Label76.Location = New System.Drawing.Point(5, 29)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(74, 16)
        Me.Label76.TabIndex = 34
        Me.Label76.Text = "Password :"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BATCH_NAME", True))
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.ForeColor = System.Drawing.Color.Yellow
        Me.Label63.Location = New System.Drawing.Point(85, 44)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(100, 24)
        Me.Label63.TabIndex = 71
        Me.Label63.Text = "FQIC-1513"
        '
        'pIsland1_batch1
        '
        Me.pIsland1_batch1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.pIsland1_batch1.BackColor = System.Drawing.Color.Transparent
        Me.pIsland1_batch1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pIsland1_batch1.Controls.Add(Me.Label58)
        Me.pIsland1_batch1.Controls.Add(Me.Label57)
        Me.pIsland1_batch1.Controls.Add(Me.Label56)
        Me.pIsland1_batch1.Controls.Add(Me.Label55)
        Me.pIsland1_batch1.Controls.Add(Me.Label54)
        Me.pIsland1_batch1.Controls.Add(Me.Label53)
        Me.pIsland1_batch1.Controls.Add(Me.Label52)
        Me.pIsland1_batch1.Controls.Add(Me.Label51)
        Me.pIsland1_batch1.Controls.Add(Me.Label50)
        Me.pIsland1_batch1.Controls.Add(Me.Label49)
        Me.pIsland1_batch1.Controls.Add(Me.Label48)
        Me.pIsland1_batch1.Controls.Add(Me.Label33)
        Me.pIsland1_batch1.Controls.Add(Me.Label32)
        Me.pIsland1_batch1.Controls.Add(Me.Label26)
        Me.pIsland1_batch1.Controls.Add(Me.Label23)
        Me.pIsland1_batch1.Controls.Add(Me.Label20)
        Me.pIsland1_batch1.Controls.Add(Me.Label17)
        Me.pIsland1_batch1.Controls.Add(Me.Label16)
        Me.pIsland1_batch1.Location = New System.Drawing.Point(-4, 181)
        Me.pIsland1_batch1.Name = "pIsland1_batch1"
        Me.pIsland1_batch1.Size = New System.Drawing.Size(227, 158)
        Me.pIsland1_batch1.TabIndex = 74
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(156, 131)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(24, 16)
        Me.Label58.TabIndex = 51
        Me.Label58.Text = "Kg"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(156, 107)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(24, 16)
        Me.Label57.TabIndex = 50
        Me.Label57.Text = "Kg"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(156, 84)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(46, 16)
        Me.Label56.TabIndex = 49
        Me.Label56.Text = "Kg/m3"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(156, 59)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(39, 16)
        Me.Label55.TabIndex = 48
        Me.Label55.Text = "Kg/hr"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(156, 36)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(24, 16)
        Me.Label54.TabIndex = 47
        Me.Label54.Text = "Kg"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.ForeColor = System.Drawing.Color.White
        Me.Label53.Location = New System.Drawing.Point(156, 14)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(24, 16)
        Me.Label53.TabIndex = 46
        Me.Label53.Text = "Kg"
        '
        'Label52
        '
        Me.Label52.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BASE_ACCUM_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label52.Location = New System.Drawing.Point(79, 132)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(78, 16)
        Me.Label52.TabIndex = 45
        Me.Label52.Text = "Preset"
        '
        'Label51
        '
        Me.Label51.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BASE_T", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label51.Location = New System.Drawing.Point(79, 108)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(78, 16)
        Me.Label51.TabIndex = 44
        Me.Label51.Text = "Preset"
        '
        'Label50
        '
        Me.Label50.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "DENSITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label50.Location = New System.Drawing.Point(79, 84)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(78, 16)
        Me.Label50.TabIndex = 43
        Me.Label50.Text = "Preset"
        '
        'Label49
        '
        Me.Label49.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BASE_F", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label49.Location = New System.Drawing.Point(79, 60)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(78, 16)
        Me.Label49.TabIndex = 42
        Me.Label49.Text = "Preset"
        '
        'Label48
        '
        Me.Label48.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BASE_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label48.Location = New System.Drawing.Point(79, 36)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(78, 16)
        Me.Label48.TabIndex = 41
        Me.Label48.Text = "Preset"
        '
        'Label33
        '
        Me.Label33.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BASE_P", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label33.Location = New System.Drawing.Point(79, 12)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(78, 16)
        Me.Label33.TabIndex = 40
        Me.Label33.Text = "Preset"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(21, 131)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(58, 16)
        Me.Label32.TabIndex = 39
        Me.Label32.Text = "Accum. :"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(26, 107)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(53, 16)
        Me.Label26.TabIndex = 38
        Me.Label26.Text = "Temp. :"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(20, 83)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(59, 16)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "Density :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(11, 59)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(68, 16)
        Me.Label20.TabIndex = 36
        Me.Label20.Text = "Flow rate :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(23, 35)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 16)
        Me.Label17.TabIndex = 35
        Me.Label17.Text = "Current :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(26, 11)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(53, 16)
        Me.Label16.TabIndex = 34
        Me.Label16.Text = "Preset :"
        '
        'Label77
        '
        Me.Label77.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Label77.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label77.ForeColor = System.Drawing.Color.White
        Me.Label77.Location = New System.Drawing.Point(0, 0)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(217, 36)
        Me.Label77.TabIndex = 73
        Me.Label77.Text = "BAY 1"
        Me.Label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel96
        '
        Me.Panel96.Controls.Add(Me.LLoadStatus1)
        Me.Panel96.Controls.Add(Me.LComm1)
        Me.Panel96.Controls.Add(Me.PComparment_bay1)
        Me.Panel96.Controls.Add(Me.pVehicle1)
        Me.Panel96.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel96.Location = New System.Drawing.Point(0, 54)
        Me.Panel96.Name = "Panel96"
        Me.Panel96.Size = New System.Drawing.Size(217, 525)
        Me.Panel96.TabIndex = 43
        '
        'LLoadStatus1
        '
        Me.LLoadStatus1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLoadStatus1.ForeColor = System.Drawing.Color.Magenta
        Me.LLoadStatus1.Location = New System.Drawing.Point(49, 69)
        Me.LLoadStatus1.Name = "LLoadStatus1"
        Me.LLoadStatus1.Size = New System.Drawing.Size(171, 20)
        Me.LLoadStatus1.TabIndex = 74
        Me.LLoadStatus1.Text = "Loading"
        Me.LLoadStatus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LComm1
        '
        Me.LComm1.AutoSize = True
        Me.LComm1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LComm1.ForeColor = System.Drawing.Color.Lime
        Me.LComm1.Location = New System.Drawing.Point(48, 48)
        Me.LComm1.Name = "LComm1"
        Me.LComm1.Size = New System.Drawing.Size(171, 20)
        Me.LComm1.TabIndex = 73
        Me.LComm1.Text = "Good Communications"
        '
        'PComparment_bay1
        '
        Me.PComparment_bay1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PComparment_bay1.BackColor = System.Drawing.Color.Silver
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp10)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp9)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp8)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp7)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp6)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp5)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp4)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp3)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp2)
        Me.PComparment_bay1.Controls.Add(Me.Bay1_Comp1)
        Me.PComparment_bay1.Location = New System.Drawing.Point(83, 222)
        Me.PComparment_bay1.Name = "PComparment_bay1"
        Me.PComparment_bay1.Size = New System.Drawing.Size(50, 218)
        Me.PComparment_bay1.TabIndex = 67
        '
        'Bay1_Comp10
        '
        Me.Bay1_Comp10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp10.Location = New System.Drawing.Point(0, 198)
        Me.Bay1_Comp10.MaxValue = 10000.0R
        Me.Bay1_Comp10.MinValue = 0.0R
        Me.Bay1_Comp10.Name = "Bay1_Comp10"
        Me.Bay1_Comp10.Progress = 30.0R
        Me.Bay1_Comp10.Size = New System.Drawing.Size(50, 20)
        Me.Bay1_Comp10.TabIndex = 16
        '
        'Bay1_Comp9
        '
        Me.Bay1_Comp9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp9.Location = New System.Drawing.Point(0, 176)
        Me.Bay1_Comp9.MaxValue = 10000.0R
        Me.Bay1_Comp9.MinValue = 0.0R
        Me.Bay1_Comp9.Name = "Bay1_Comp9"
        Me.Bay1_Comp9.Progress = 30.0R
        Me.Bay1_Comp9.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp9.TabIndex = 15
        '
        'Bay1_Comp8
        '
        Me.Bay1_Comp8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp8.Location = New System.Drawing.Point(0, 154)
        Me.Bay1_Comp8.MaxValue = 10000.0R
        Me.Bay1_Comp8.MinValue = 0.0R
        Me.Bay1_Comp8.Name = "Bay1_Comp8"
        Me.Bay1_Comp8.Progress = 30.0R
        Me.Bay1_Comp8.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp8.TabIndex = 14
        '
        'Bay1_Comp7
        '
        Me.Bay1_Comp7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp7.Location = New System.Drawing.Point(0, 132)
        Me.Bay1_Comp7.MaxValue = 10000.0R
        Me.Bay1_Comp7.MinValue = 0.0R
        Me.Bay1_Comp7.Name = "Bay1_Comp7"
        Me.Bay1_Comp7.Progress = 30.0R
        Me.Bay1_Comp7.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp7.TabIndex = 13
        '
        'Bay1_Comp6
        '
        Me.Bay1_Comp6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp6.Location = New System.Drawing.Point(0, 110)
        Me.Bay1_Comp6.MaxValue = 10000.0R
        Me.Bay1_Comp6.MinValue = 0.0R
        Me.Bay1_Comp6.Name = "Bay1_Comp6"
        Me.Bay1_Comp6.Progress = 30.0R
        Me.Bay1_Comp6.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp6.TabIndex = 12
        '
        'Bay1_Comp5
        '
        Me.Bay1_Comp5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp5.Location = New System.Drawing.Point(0, 88)
        Me.Bay1_Comp5.MaxValue = 10000.0R
        Me.Bay1_Comp5.MinValue = 0.0R
        Me.Bay1_Comp5.Name = "Bay1_Comp5"
        Me.Bay1_Comp5.Progress = 30.0R
        Me.Bay1_Comp5.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp5.TabIndex = 11
        '
        'Bay1_Comp4
        '
        Me.Bay1_Comp4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp4.Location = New System.Drawing.Point(0, 66)
        Me.Bay1_Comp4.MaxValue = 10000.0R
        Me.Bay1_Comp4.MinValue = 0.0R
        Me.Bay1_Comp4.Name = "Bay1_Comp4"
        Me.Bay1_Comp4.Progress = 30.0R
        Me.Bay1_Comp4.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp4.TabIndex = 10
        '
        'Bay1_Comp3
        '
        Me.Bay1_Comp3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp3.Location = New System.Drawing.Point(0, 44)
        Me.Bay1_Comp3.MaxValue = 10000.0R
        Me.Bay1_Comp3.MinValue = 0.0R
        Me.Bay1_Comp3.Name = "Bay1_Comp3"
        Me.Bay1_Comp3.Progress = 30.0R
        Me.Bay1_Comp3.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp3.TabIndex = 9
        '
        'Bay1_Comp2
        '
        Me.Bay1_Comp2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay1_Comp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp2.Location = New System.Drawing.Point(0, 22)
        Me.Bay1_Comp2.MaxValue = 10000.0R
        Me.Bay1_Comp2.MinValue = 0.0R
        Me.Bay1_Comp2.Name = "Bay1_Comp2"
        Me.Bay1_Comp2.Progress = 30.0R
        Me.Bay1_Comp2.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp2.TabIndex = 8
        '
        'Bay1_Comp1
        '
        Me.Bay1_Comp1.BackColor = System.Drawing.Color.DarkRed
        Me.Bay1_Comp1.Color = System.Drawing.Color.DarkRed
        Me.Bay1_Comp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay1_Comp1.Location = New System.Drawing.Point(0, 0)
        Me.Bay1_Comp1.MaxValue = 100.0R
        Me.Bay1_Comp1.MinValue = 0.0R
        Me.Bay1_Comp1.Name = "Bay1_Comp1"
        Me.Bay1_Comp1.Progress = 80.0R
        Me.Bay1_Comp1.Size = New System.Drawing.Size(50, 22)
        Me.Bay1_Comp1.TabIndex = 7
        '
        'pVehicle1
        '
        Me.pVehicle1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pVehicle1.Image = Global.TAS_TOL.My.Resources.Resources.Truck_Lpg_Topview
        Me.pVehicle1.Location = New System.Drawing.Point(58, 86)
        Me.pVehicle1.Name = "pVehicle1"
        Me.pVehicle1.Size = New System.Drawing.Size(100, 398)
        Me.pVehicle1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pVehicle1.TabIndex = 66
        Me.pVehicle1.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Panel20)
        Me.Panel5.Controls.Add(Me.Panel1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(900, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(217, 579)
        Me.Panel5.TabIndex = 36
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.Panel21)
        Me.Panel20.Controls.Add(Me.Label124)
        Me.Panel20.Controls.Add(Me.Label133)
        Me.Panel20.Controls.Add(Me.Panel22)
        Me.Panel20.Controls.Add(Me.Label152)
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel20.Location = New System.Drawing.Point(0, 0)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(217, 345)
        Me.Panel20.TabIndex = 45
        '
        'Panel21
        '
        Me.Panel21.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel21.BackColor = System.Drawing.Color.Transparent
        Me.Panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel21.Controls.Add(Me.Label125)
        Me.Panel21.Controls.Add(Me.Label126)
        Me.Panel21.Controls.Add(Me.Label127)
        Me.Panel21.Controls.Add(Me.Label128)
        Me.Panel21.Controls.Add(Me.Label129)
        Me.Panel21.Controls.Add(Me.Label130)
        Me.Panel21.Controls.Add(Me.Label131)
        Me.Panel21.Controls.Add(Me.Label132)
        Me.Panel21.Location = New System.Drawing.Point(-4, 103)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(227, 72)
        Me.Panel21.TabIndex = 77
        '
        'Label125
        '
        Me.Label125.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "DRIVER_PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label125.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label125.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label125.Location = New System.Drawing.Point(154, 51)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(51, 16)
        Me.Label125.TabIndex = 45
        Me.Label125.Text = "Preset"
        '
        'VBATCHSTATUSBindingSource4
        '
        Me.VBATCHSTATUSBindingSource4.DataMember = "V_BATCH_STATUS"
        Me.VBATCHSTATUSBindingSource4.DataSource = Me.FPTDataSet
        Me.VBATCHSTATUSBindingSource4.Filter = "BATCH_NUMBER=5"
        '
        'Label126
        '
        Me.Label126.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "DRIVER_PASSWORD", True))
        Me.Label126.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label126.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label126.Location = New System.Drawing.Point(154, 30)
        Me.Label126.Name = "Label126"
        Me.Label126.Size = New System.Drawing.Size(51, 16)
        Me.Label126.TabIndex = 44
        Me.Label126.Text = "Preset"
        '
        'Label127
        '
        Me.Label127.AutoSize = True
        Me.Label127.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label127.ForeColor = System.Drawing.Color.White
        Me.Label127.Location = New System.Drawing.Point(154, 7)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(53, 16)
        Me.Label127.TabIndex = 43
        Me.Label127.Text = "Opertor"
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label128.ForeColor = System.Drawing.Color.White
        Me.Label128.Location = New System.Drawing.Point(79, 7)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(42, 16)
        Me.Label128.TabIndex = 42
        Me.Label128.Text = "Batch"
        '
        'Label129
        '
        Me.Label129.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label129.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label129.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label129.Location = New System.Drawing.Point(79, 51)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(51, 16)
        Me.Label129.TabIndex = 41
        Me.Label129.Text = "Preset"
        '
        'Label130
        '
        Me.Label130.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "PASSWORD", True))
        Me.Label130.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label130.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label130.Location = New System.Drawing.Point(79, 30)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(51, 16)
        Me.Label130.TabIndex = 40
        Me.Label130.Text = "Preset"
        '
        'Label131
        '
        Me.Label131.AutoSize = True
        Me.Label131.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label131.ForeColor = System.Drawing.Color.White
        Me.Label131.Location = New System.Drawing.Point(26, 50)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(53, 16)
        Me.Label131.TabIndex = 35
        Me.Label131.Text = "Preset :"
        '
        'Label132
        '
        Me.Label132.AutoSize = True
        Me.Label132.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label132.ForeColor = System.Drawing.Color.White
        Me.Label132.Location = New System.Drawing.Point(5, 29)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(74, 16)
        Me.Label132.TabIndex = 34
        Me.Label132.Text = "Password :"
        '
        'Label124
        '
        Me.Label124.AutoSize = True
        Me.Label124.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "Product_code", True))
        Me.Label124.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label124.ForeColor = System.Drawing.Color.Yellow
        Me.Label124.Location = New System.Drawing.Point(106, 72)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(54, 24)
        Me.Label124.TabIndex = 72
        Me.Label124.Text = " TEG"
        '
        'Label133
        '
        Me.Label133.AutoSize = True
        Me.Label133.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "BATCH_NAME", True))
        Me.Label133.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label133.ForeColor = System.Drawing.Color.Yellow
        Me.Label133.Location = New System.Drawing.Point(85, 44)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(100, 24)
        Me.Label133.TabIndex = 71
        Me.Label133.Text = "FQIC-1513"
        '
        'Panel22
        '
        Me.Panel22.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel22.BackColor = System.Drawing.Color.Transparent
        Me.Panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel22.Controls.Add(Me.Label134)
        Me.Panel22.Controls.Add(Me.Label135)
        Me.Panel22.Controls.Add(Me.Label136)
        Me.Panel22.Controls.Add(Me.Label137)
        Me.Panel22.Controls.Add(Me.Label138)
        Me.Panel22.Controls.Add(Me.Label139)
        Me.Panel22.Controls.Add(Me.Label140)
        Me.Panel22.Controls.Add(Me.Label141)
        Me.Panel22.Controls.Add(Me.Label142)
        Me.Panel22.Controls.Add(Me.Label143)
        Me.Panel22.Controls.Add(Me.Label144)
        Me.Panel22.Controls.Add(Me.Label145)
        Me.Panel22.Controls.Add(Me.Label146)
        Me.Panel22.Controls.Add(Me.Label147)
        Me.Panel22.Controls.Add(Me.Label148)
        Me.Panel22.Controls.Add(Me.Label149)
        Me.Panel22.Controls.Add(Me.Label150)
        Me.Panel22.Controls.Add(Me.Label151)
        Me.Panel22.Location = New System.Drawing.Point(-4, 181)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(227, 158)
        Me.Panel22.TabIndex = 74
        '
        'Label134
        '
        Me.Label134.AutoSize = True
        Me.Label134.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label134.ForeColor = System.Drawing.Color.White
        Me.Label134.Location = New System.Drawing.Point(156, 131)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(24, 16)
        Me.Label134.TabIndex = 51
        Me.Label134.Text = "Kg"
        '
        'Label135
        '
        Me.Label135.AutoSize = True
        Me.Label135.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label135.ForeColor = System.Drawing.Color.White
        Me.Label135.Location = New System.Drawing.Point(156, 107)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(24, 16)
        Me.Label135.TabIndex = 50
        Me.Label135.Text = "Kg"
        '
        'Label136
        '
        Me.Label136.AutoSize = True
        Me.Label136.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label136.ForeColor = System.Drawing.Color.White
        Me.Label136.Location = New System.Drawing.Point(156, 84)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(46, 16)
        Me.Label136.TabIndex = 49
        Me.Label136.Text = "Kg/m3"
        '
        'Label137
        '
        Me.Label137.AutoSize = True
        Me.Label137.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label137.ForeColor = System.Drawing.Color.White
        Me.Label137.Location = New System.Drawing.Point(156, 59)
        Me.Label137.Name = "Label137"
        Me.Label137.Size = New System.Drawing.Size(39, 16)
        Me.Label137.TabIndex = 48
        Me.Label137.Text = "Kg/hr"
        '
        'Label138
        '
        Me.Label138.AutoSize = True
        Me.Label138.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label138.ForeColor = System.Drawing.Color.White
        Me.Label138.Location = New System.Drawing.Point(156, 36)
        Me.Label138.Name = "Label138"
        Me.Label138.Size = New System.Drawing.Size(24, 16)
        Me.Label138.TabIndex = 47
        Me.Label138.Text = "Kg"
        '
        'Label139
        '
        Me.Label139.AutoSize = True
        Me.Label139.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label139.ForeColor = System.Drawing.Color.White
        Me.Label139.Location = New System.Drawing.Point(156, 14)
        Me.Label139.Name = "Label139"
        Me.Label139.Size = New System.Drawing.Size(24, 16)
        Me.Label139.TabIndex = 46
        Me.Label139.Text = "Kg"
        '
        'Label140
        '
        Me.Label140.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "BASE_ACCUM_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label140.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label140.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label140.Location = New System.Drawing.Point(79, 132)
        Me.Label140.Name = "Label140"
        Me.Label140.Size = New System.Drawing.Size(78, 16)
        Me.Label140.TabIndex = 45
        Me.Label140.Text = "Preset"
        '
        'Label141
        '
        Me.Label141.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "BASE_T", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label141.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label141.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label141.Location = New System.Drawing.Point(79, 108)
        Me.Label141.Name = "Label141"
        Me.Label141.Size = New System.Drawing.Size(78, 16)
        Me.Label141.TabIndex = 44
        Me.Label141.Text = "Preset"
        '
        'Label142
        '
        Me.Label142.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "DENSITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label142.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label142.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label142.Location = New System.Drawing.Point(79, 84)
        Me.Label142.Name = "Label142"
        Me.Label142.Size = New System.Drawing.Size(78, 16)
        Me.Label142.TabIndex = 43
        Me.Label142.Text = "Preset"
        '
        'Label143
        '
        Me.Label143.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "BASE_F", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label143.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label143.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label143.Location = New System.Drawing.Point(79, 60)
        Me.Label143.Name = "Label143"
        Me.Label143.Size = New System.Drawing.Size(78, 16)
        Me.Label143.TabIndex = 42
        Me.Label143.Text = "Preset"
        '
        'Label144
        '
        Me.Label144.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "BASE_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label144.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label144.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label144.Location = New System.Drawing.Point(79, 36)
        Me.Label144.Name = "Label144"
        Me.Label144.Size = New System.Drawing.Size(78, 16)
        Me.Label144.TabIndex = 41
        Me.Label144.Text = "Preset"
        '
        'Label145
        '
        Me.Label145.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource4, "BASE_P", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label145.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label145.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label145.Location = New System.Drawing.Point(79, 12)
        Me.Label145.Name = "Label145"
        Me.Label145.Size = New System.Drawing.Size(78, 16)
        Me.Label145.TabIndex = 40
        Me.Label145.Text = "Preset"
        '
        'Label146
        '
        Me.Label146.AutoSize = True
        Me.Label146.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label146.ForeColor = System.Drawing.Color.White
        Me.Label146.Location = New System.Drawing.Point(21, 131)
        Me.Label146.Name = "Label146"
        Me.Label146.Size = New System.Drawing.Size(58, 16)
        Me.Label146.TabIndex = 39
        Me.Label146.Text = "Accum. :"
        '
        'Label147
        '
        Me.Label147.AutoSize = True
        Me.Label147.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label147.ForeColor = System.Drawing.Color.White
        Me.Label147.Location = New System.Drawing.Point(26, 107)
        Me.Label147.Name = "Label147"
        Me.Label147.Size = New System.Drawing.Size(53, 16)
        Me.Label147.TabIndex = 38
        Me.Label147.Text = "Temp. :"
        '
        'Label148
        '
        Me.Label148.AutoSize = True
        Me.Label148.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label148.ForeColor = System.Drawing.Color.White
        Me.Label148.Location = New System.Drawing.Point(20, 83)
        Me.Label148.Name = "Label148"
        Me.Label148.Size = New System.Drawing.Size(59, 16)
        Me.Label148.TabIndex = 37
        Me.Label148.Text = "Density :"
        '
        'Label149
        '
        Me.Label149.AutoSize = True
        Me.Label149.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label149.ForeColor = System.Drawing.Color.White
        Me.Label149.Location = New System.Drawing.Point(11, 59)
        Me.Label149.Name = "Label149"
        Me.Label149.Size = New System.Drawing.Size(68, 16)
        Me.Label149.TabIndex = 36
        Me.Label149.Text = "Flow rate :"
        '
        'Label150
        '
        Me.Label150.AutoSize = True
        Me.Label150.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label150.ForeColor = System.Drawing.Color.White
        Me.Label150.Location = New System.Drawing.Point(23, 35)
        Me.Label150.Name = "Label150"
        Me.Label150.Size = New System.Drawing.Size(56, 16)
        Me.Label150.TabIndex = 35
        Me.Label150.Text = "Current :"
        '
        'Label151
        '
        Me.Label151.AutoSize = True
        Me.Label151.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label151.ForeColor = System.Drawing.Color.White
        Me.Label151.Location = New System.Drawing.Point(26, 11)
        Me.Label151.Name = "Label151"
        Me.Label151.Size = New System.Drawing.Size(53, 16)
        Me.Label151.TabIndex = 34
        Me.Label151.Text = "Preset :"
        '
        'Label152
        '
        Me.Label152.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Label152.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label152.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label152.ForeColor = System.Drawing.Color.White
        Me.Label152.Location = New System.Drawing.Point(0, 0)
        Me.Label152.Name = "Label152"
        Me.Label152.Size = New System.Drawing.Size(217, 36)
        Me.Label152.TabIndex = 73
        Me.Label152.Text = "BAY 5"
        Me.Label152.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LLoadStatus5)
        Me.Panel1.Controls.Add(Me.LComm5)
        Me.Panel1.Controls.Add(Me.PComparment_bay5)
        Me.Panel1.Controls.Add(Me.pVehicle5)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 54)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(217, 525)
        Me.Panel1.TabIndex = 43
        '
        'LLoadStatus5
        '
        Me.LLoadStatus5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLoadStatus5.ForeColor = System.Drawing.Color.Magenta
        Me.LLoadStatus5.Location = New System.Drawing.Point(48, 69)
        Me.LLoadStatus5.Name = "LLoadStatus5"
        Me.LLoadStatus5.Size = New System.Drawing.Size(171, 20)
        Me.LLoadStatus5.TabIndex = 76
        Me.LLoadStatus5.Text = "Loading"
        Me.LLoadStatus5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LComm5
        '
        Me.LComm5.AutoSize = True
        Me.LComm5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LComm5.ForeColor = System.Drawing.Color.Lime
        Me.LComm5.Location = New System.Drawing.Point(50, 48)
        Me.LComm5.Name = "LComm5"
        Me.LComm5.Size = New System.Drawing.Size(171, 20)
        Me.LComm5.TabIndex = 75
        Me.LComm5.Text = "Good Communications"
        '
        'PComparment_bay5
        '
        Me.PComparment_bay5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PComparment_bay5.BackColor = System.Drawing.Color.Silver
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp10)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp9)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp8)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp7)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp6)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp5)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp4)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp3)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp2)
        Me.PComparment_bay5.Controls.Add(Me.Bay5_Comp1)
        Me.PComparment_bay5.Location = New System.Drawing.Point(83, 221)
        Me.PComparment_bay5.Name = "PComparment_bay5"
        Me.PComparment_bay5.Size = New System.Drawing.Size(50, 218)
        Me.PComparment_bay5.TabIndex = 66
        '
        'Bay5_Comp10
        '
        Me.Bay5_Comp10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp10.Location = New System.Drawing.Point(0, 198)
        Me.Bay5_Comp10.MaxValue = 10000.0R
        Me.Bay5_Comp10.MinValue = 0.0R
        Me.Bay5_Comp10.Name = "Bay5_Comp10"
        Me.Bay5_Comp10.Progress = 30.0R
        Me.Bay5_Comp10.Size = New System.Drawing.Size(50, 20)
        Me.Bay5_Comp10.TabIndex = 16
        '
        'Bay5_Comp9
        '
        Me.Bay5_Comp9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp9.Location = New System.Drawing.Point(0, 176)
        Me.Bay5_Comp9.MaxValue = 10000.0R
        Me.Bay5_Comp9.MinValue = 0.0R
        Me.Bay5_Comp9.Name = "Bay5_Comp9"
        Me.Bay5_Comp9.Progress = 30.0R
        Me.Bay5_Comp9.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp9.TabIndex = 15
        '
        'Bay5_Comp8
        '
        Me.Bay5_Comp8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp8.Location = New System.Drawing.Point(0, 154)
        Me.Bay5_Comp8.MaxValue = 10000.0R
        Me.Bay5_Comp8.MinValue = 0.0R
        Me.Bay5_Comp8.Name = "Bay5_Comp8"
        Me.Bay5_Comp8.Progress = 30.0R
        Me.Bay5_Comp8.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp8.TabIndex = 14
        '
        'Bay5_Comp7
        '
        Me.Bay5_Comp7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp7.Location = New System.Drawing.Point(0, 132)
        Me.Bay5_Comp7.MaxValue = 10000.0R
        Me.Bay5_Comp7.MinValue = 0.0R
        Me.Bay5_Comp7.Name = "Bay5_Comp7"
        Me.Bay5_Comp7.Progress = 30.0R
        Me.Bay5_Comp7.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp7.TabIndex = 13
        '
        'Bay5_Comp6
        '
        Me.Bay5_Comp6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp6.Location = New System.Drawing.Point(0, 110)
        Me.Bay5_Comp6.MaxValue = 10000.0R
        Me.Bay5_Comp6.MinValue = 0.0R
        Me.Bay5_Comp6.Name = "Bay5_Comp6"
        Me.Bay5_Comp6.Progress = 30.0R
        Me.Bay5_Comp6.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp6.TabIndex = 12
        '
        'Bay5_Comp5
        '
        Me.Bay5_Comp5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp5.Location = New System.Drawing.Point(0, 88)
        Me.Bay5_Comp5.MaxValue = 10000.0R
        Me.Bay5_Comp5.MinValue = 0.0R
        Me.Bay5_Comp5.Name = "Bay5_Comp5"
        Me.Bay5_Comp5.Progress = 30.0R
        Me.Bay5_Comp5.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp5.TabIndex = 11
        '
        'Bay5_Comp4
        '
        Me.Bay5_Comp4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp4.Location = New System.Drawing.Point(0, 66)
        Me.Bay5_Comp4.MaxValue = 10000.0R
        Me.Bay5_Comp4.MinValue = 0.0R
        Me.Bay5_Comp4.Name = "Bay5_Comp4"
        Me.Bay5_Comp4.Progress = 30.0R
        Me.Bay5_Comp4.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp4.TabIndex = 10
        '
        'Bay5_Comp3
        '
        Me.Bay5_Comp3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp3.Location = New System.Drawing.Point(0, 44)
        Me.Bay5_Comp3.MaxValue = 10000.0R
        Me.Bay5_Comp3.MinValue = 0.0R
        Me.Bay5_Comp3.Name = "Bay5_Comp3"
        Me.Bay5_Comp3.Progress = 30.0R
        Me.Bay5_Comp3.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp3.TabIndex = 9
        '
        'Bay5_Comp2
        '
        Me.Bay5_Comp2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay5_Comp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp2.Location = New System.Drawing.Point(0, 22)
        Me.Bay5_Comp2.MaxValue = 10000.0R
        Me.Bay5_Comp2.MinValue = 0.0R
        Me.Bay5_Comp2.Name = "Bay5_Comp2"
        Me.Bay5_Comp2.Progress = 30.0R
        Me.Bay5_Comp2.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp2.TabIndex = 8
        '
        'Bay5_Comp1
        '
        Me.Bay5_Comp1.BackColor = System.Drawing.Color.DarkRed
        Me.Bay5_Comp1.Color = System.Drawing.Color.DarkRed
        Me.Bay5_Comp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay5_Comp1.Location = New System.Drawing.Point(0, 0)
        Me.Bay5_Comp1.MaxValue = 100.0R
        Me.Bay5_Comp1.MinValue = 0.0R
        Me.Bay5_Comp1.Name = "Bay5_Comp1"
        Me.Bay5_Comp1.Progress = 80.0R
        Me.Bay5_Comp1.Size = New System.Drawing.Size(50, 22)
        Me.Bay5_Comp1.TabIndex = 7
        '
        'pVehicle5
        '
        Me.pVehicle5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pVehicle5.Image = Global.TAS_TOL.My.Resources.Resources.Truck_Lpg_Topview
        Me.pVehicle5.Location = New System.Drawing.Point(58, 87)
        Me.pVehicle5.Name = "pVehicle5"
        Me.pVehicle5.Size = New System.Drawing.Size(100, 397)
        Me.pVehicle5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pVehicle5.TabIndex = 44
        Me.pVehicle5.TabStop = False
        '
        'PISLAND1
        '
        Me.PISLAND1.Controls.Add(Me.Panel11)
        Me.PISLAND1.Controls.Add(Me.Panel97)
        Me.PISLAND1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PISLAND1.Location = New System.Drawing.Point(228, 4)
        Me.PISLAND1.Name = "PISLAND1"
        Me.PISLAND1.Size = New System.Drawing.Size(217, 579)
        Me.PISLAND1.TabIndex = 6
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.Panel12)
        Me.Panel11.Controls.Add(Me.Label1)
        Me.Panel11.Controls.Add(Me.Label12)
        Me.Panel11.Controls.Add(Me.Panel13)
        Me.Panel11.Controls.Add(Me.Label43)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel11.Location = New System.Drawing.Point(0, 0)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(217, 345)
        Me.Panel11.TabIndex = 45
        '
        'Panel12
        '
        Me.Panel12.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel12.Controls.Add(Me.Label2)
        Me.Panel12.Controls.Add(Me.Label3)
        Me.Panel12.Controls.Add(Me.Label4)
        Me.Panel12.Controls.Add(Me.Label5)
        Me.Panel12.Controls.Add(Me.Label8)
        Me.Panel12.Controls.Add(Me.Label9)
        Me.Panel12.Controls.Add(Me.Label10)
        Me.Panel12.Controls.Add(Me.Label11)
        Me.Panel12.Location = New System.Drawing.Point(-4, 103)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(227, 72)
        Me.Panel12.TabIndex = 76
        '
        'Label2
        '
        Me.Label2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "DRIVER_PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(154, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Preset"
        '
        'VBATCHSTATUSBindingSource1
        '
        Me.VBATCHSTATUSBindingSource1.DataMember = "V_BATCH_STATUS"
        Me.VBATCHSTATUSBindingSource1.DataSource = Me.FPTDataSet
        Me.VBATCHSTATUSBindingSource1.Filter = "BATCH_NUMBER=2"
        '
        'Label3
        '
        Me.Label3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "DRIVER_PASSWORD", True))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(154, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 16)
        Me.Label3.TabIndex = 44
        Me.Label3.Text = "Preset"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(154, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 16)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "Opertor"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(79, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 16)
        Me.Label5.TabIndex = 42
        Me.Label5.Text = "Batch"
        '
        'Label8
        '
        Me.Label8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(79, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 16)
        Me.Label8.TabIndex = 41
        Me.Label8.Text = "Preset"
        '
        'Label9
        '
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "PASSWORD", True))
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(79, 30)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 16)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Preset"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(26, 50)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 16)
        Me.Label10.TabIndex = 35
        Me.Label10.Text = "Preset :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(5, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 16)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Password :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "Product_code", True))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Yellow
        Me.Label1.Location = New System.Drawing.Point(106, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 24)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = " MEG"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "BATCH_NAME", True))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Yellow
        Me.Label12.Location = New System.Drawing.Point(85, 44)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 24)
        Me.Label12.TabIndex = 71
        Me.Label12.Text = "FQIC-1513"
        '
        'Panel13
        '
        Me.Panel13.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel13.BackColor = System.Drawing.Color.Transparent
        Me.Panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel13.Controls.Add(Me.Label13)
        Me.Panel13.Controls.Add(Me.Label14)
        Me.Panel13.Controls.Add(Me.Label18)
        Me.Panel13.Controls.Add(Me.Label19)
        Me.Panel13.Controls.Add(Me.Label25)
        Me.Panel13.Controls.Add(Me.Label28)
        Me.Panel13.Controls.Add(Me.Label29)
        Me.Panel13.Controls.Add(Me.Label30)
        Me.Panel13.Controls.Add(Me.Label31)
        Me.Panel13.Controls.Add(Me.Label34)
        Me.Panel13.Controls.Add(Me.Label35)
        Me.Panel13.Controls.Add(Me.Label36)
        Me.Panel13.Controls.Add(Me.Label37)
        Me.Panel13.Controls.Add(Me.Label38)
        Me.Panel13.Controls.Add(Me.Label39)
        Me.Panel13.Controls.Add(Me.Label40)
        Me.Panel13.Controls.Add(Me.Label41)
        Me.Panel13.Controls.Add(Me.Label42)
        Me.Panel13.Location = New System.Drawing.Point(-4, 181)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(227, 158)
        Me.Panel13.TabIndex = 74
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(156, 131)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(24, 16)
        Me.Label13.TabIndex = 51
        Me.Label13.Text = "Kg"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(156, 107)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(24, 16)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Kg"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(156, 84)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(46, 16)
        Me.Label18.TabIndex = 49
        Me.Label18.Text = "Kg/m3"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(156, 59)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(39, 16)
        Me.Label19.TabIndex = 48
        Me.Label19.Text = "Kg/hr"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(156, 36)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(24, 16)
        Me.Label25.TabIndex = 47
        Me.Label25.Text = "Kg"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(156, 14)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(24, 16)
        Me.Label28.TabIndex = 46
        Me.Label28.Text = "Kg"
        '
        'Label29
        '
        Me.Label29.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "BASE_ACCUM_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label29.Location = New System.Drawing.Point(79, 132)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(78, 16)
        Me.Label29.TabIndex = 45
        Me.Label29.Text = "Preset"
        '
        'Label30
        '
        Me.Label30.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "BASE_T", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label30.Location = New System.Drawing.Point(79, 108)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(78, 16)
        Me.Label30.TabIndex = 44
        Me.Label30.Text = "Preset"
        '
        'Label31
        '
        Me.Label31.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "DENSITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label31.Location = New System.Drawing.Point(79, 84)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(78, 16)
        Me.Label31.TabIndex = 43
        Me.Label31.Text = "Preset"
        '
        'Label34
        '
        Me.Label34.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource, "BASE_F", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label34.Location = New System.Drawing.Point(79, 60)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(78, 16)
        Me.Label34.TabIndex = 42
        Me.Label34.Text = "Preset"
        '
        'Label35
        '
        Me.Label35.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "BASE_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label35.Location = New System.Drawing.Point(79, 36)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(78, 16)
        Me.Label35.TabIndex = 41
        Me.Label35.Text = "Preset"
        '
        'Label36
        '
        Me.Label36.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource1, "BASE_P", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label36.Location = New System.Drawing.Point(79, 12)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(78, 16)
        Me.Label36.TabIndex = 40
        Me.Label36.Text = "Preset"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(21, 131)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(58, 16)
        Me.Label37.TabIndex = 39
        Me.Label37.Text = "Accum. :"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(26, 107)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(53, 16)
        Me.Label38.TabIndex = 38
        Me.Label38.Text = "Temp. :"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.Color.White
        Me.Label39.Location = New System.Drawing.Point(20, 83)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(59, 16)
        Me.Label39.TabIndex = 37
        Me.Label39.Text = "Density :"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(11, 59)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(68, 16)
        Me.Label40.TabIndex = 36
        Me.Label40.Text = "Flow rate :"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(23, 35)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(56, 16)
        Me.Label41.TabIndex = 35
        Me.Label41.Text = "Current :"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(26, 11)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(53, 16)
        Me.Label42.TabIndex = 34
        Me.Label42.Text = "Preset :"
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Label43.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(0, 0)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(217, 36)
        Me.Label43.TabIndex = 73
        Me.Label43.Text = "BAY 2"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel97
        '
        Me.Panel97.Controls.Add(Me.LLoadStatus2)
        Me.Panel97.Controls.Add(Me.LComm2)
        Me.Panel97.Controls.Add(Me.PComparment_bay2)
        Me.Panel97.Controls.Add(Me.pVehicle2)
        Me.Panel97.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel97.Location = New System.Drawing.Point(0, 54)
        Me.Panel97.Name = "Panel97"
        Me.Panel97.Size = New System.Drawing.Size(217, 525)
        Me.Panel97.TabIndex = 42
        '
        'LLoadStatus2
        '
        Me.LLoadStatus2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLoadStatus2.ForeColor = System.Drawing.Color.Magenta
        Me.LLoadStatus2.Location = New System.Drawing.Point(41, 69)
        Me.LLoadStatus2.Name = "LLoadStatus2"
        Me.LLoadStatus2.Size = New System.Drawing.Size(171, 20)
        Me.LLoadStatus2.TabIndex = 75
        Me.LLoadStatus2.Text = "Loading"
        Me.LLoadStatus2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LComm2
        '
        Me.LComm2.AutoSize = True
        Me.LComm2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LComm2.ForeColor = System.Drawing.Color.Lime
        Me.LComm2.Location = New System.Drawing.Point(43, 48)
        Me.LComm2.Name = "LComm2"
        Me.LComm2.Size = New System.Drawing.Size(171, 20)
        Me.LComm2.TabIndex = 74
        Me.LComm2.Text = "Good Communications"
        '
        'PComparment_bay2
        '
        Me.PComparment_bay2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PComparment_bay2.BackColor = System.Drawing.Color.Silver
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp10)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp9)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp8)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp7)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp6)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp5)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp4)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp3)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp2)
        Me.PComparment_bay2.Controls.Add(Me.Bay2_Comp1)
        Me.PComparment_bay2.Location = New System.Drawing.Point(74, 220)
        Me.PComparment_bay2.Name = "PComparment_bay2"
        Me.PComparment_bay2.Size = New System.Drawing.Size(50, 218)
        Me.PComparment_bay2.TabIndex = 67
        '
        'Bay2_Comp10
        '
        Me.Bay2_Comp10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp10.Location = New System.Drawing.Point(0, 198)
        Me.Bay2_Comp10.MaxValue = 10000.0R
        Me.Bay2_Comp10.MinValue = 0.0R
        Me.Bay2_Comp10.Name = "Bay2_Comp10"
        Me.Bay2_Comp10.Progress = 30.0R
        Me.Bay2_Comp10.Size = New System.Drawing.Size(50, 20)
        Me.Bay2_Comp10.TabIndex = 16
        '
        'Bay2_Comp9
        '
        Me.Bay2_Comp9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp9.Location = New System.Drawing.Point(0, 176)
        Me.Bay2_Comp9.MaxValue = 10000.0R
        Me.Bay2_Comp9.MinValue = 0.0R
        Me.Bay2_Comp9.Name = "Bay2_Comp9"
        Me.Bay2_Comp9.Progress = 30.0R
        Me.Bay2_Comp9.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp9.TabIndex = 15
        '
        'Bay2_Comp8
        '
        Me.Bay2_Comp8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp8.Location = New System.Drawing.Point(0, 154)
        Me.Bay2_Comp8.MaxValue = 10000.0R
        Me.Bay2_Comp8.MinValue = 0.0R
        Me.Bay2_Comp8.Name = "Bay2_Comp8"
        Me.Bay2_Comp8.Progress = 30.0R
        Me.Bay2_Comp8.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp8.TabIndex = 14
        '
        'Bay2_Comp7
        '
        Me.Bay2_Comp7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp7.Location = New System.Drawing.Point(0, 132)
        Me.Bay2_Comp7.MaxValue = 10000.0R
        Me.Bay2_Comp7.MinValue = 0.0R
        Me.Bay2_Comp7.Name = "Bay2_Comp7"
        Me.Bay2_Comp7.Progress = 30.0R
        Me.Bay2_Comp7.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp7.TabIndex = 13
        '
        'Bay2_Comp6
        '
        Me.Bay2_Comp6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp6.Location = New System.Drawing.Point(0, 110)
        Me.Bay2_Comp6.MaxValue = 10000.0R
        Me.Bay2_Comp6.MinValue = 0.0R
        Me.Bay2_Comp6.Name = "Bay2_Comp6"
        Me.Bay2_Comp6.Progress = 30.0R
        Me.Bay2_Comp6.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp6.TabIndex = 12
        '
        'Bay2_Comp5
        '
        Me.Bay2_Comp5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp5.Location = New System.Drawing.Point(0, 88)
        Me.Bay2_Comp5.MaxValue = 10000.0R
        Me.Bay2_Comp5.MinValue = 0.0R
        Me.Bay2_Comp5.Name = "Bay2_Comp5"
        Me.Bay2_Comp5.Progress = 30.0R
        Me.Bay2_Comp5.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp5.TabIndex = 11
        '
        'Bay2_Comp4
        '
        Me.Bay2_Comp4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp4.Location = New System.Drawing.Point(0, 66)
        Me.Bay2_Comp4.MaxValue = 10000.0R
        Me.Bay2_Comp4.MinValue = 0.0R
        Me.Bay2_Comp4.Name = "Bay2_Comp4"
        Me.Bay2_Comp4.Progress = 30.0R
        Me.Bay2_Comp4.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp4.TabIndex = 10
        '
        'Bay2_Comp3
        '
        Me.Bay2_Comp3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp3.Location = New System.Drawing.Point(0, 44)
        Me.Bay2_Comp3.MaxValue = 10000.0R
        Me.Bay2_Comp3.MinValue = 0.0R
        Me.Bay2_Comp3.Name = "Bay2_Comp3"
        Me.Bay2_Comp3.Progress = 30.0R
        Me.Bay2_Comp3.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp3.TabIndex = 9
        '
        'Bay2_Comp2
        '
        Me.Bay2_Comp2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay2_Comp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp2.Location = New System.Drawing.Point(0, 22)
        Me.Bay2_Comp2.MaxValue = 10000.0R
        Me.Bay2_Comp2.MinValue = 0.0R
        Me.Bay2_Comp2.Name = "Bay2_Comp2"
        Me.Bay2_Comp2.Progress = 30.0R
        Me.Bay2_Comp2.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp2.TabIndex = 8
        '
        'Bay2_Comp1
        '
        Me.Bay2_Comp1.BackColor = System.Drawing.Color.DarkRed
        Me.Bay2_Comp1.Color = System.Drawing.Color.DarkRed
        Me.Bay2_Comp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay2_Comp1.Location = New System.Drawing.Point(0, 0)
        Me.Bay2_Comp1.MaxValue = 100.0R
        Me.Bay2_Comp1.MinValue = 0.0R
        Me.Bay2_Comp1.Name = "Bay2_Comp1"
        Me.Bay2_Comp1.Progress = 80.0R
        Me.Bay2_Comp1.Size = New System.Drawing.Size(50, 22)
        Me.Bay2_Comp1.TabIndex = 7
        '
        'pVehicle2
        '
        Me.pVehicle2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pVehicle2.Image = Global.TAS_TOL.My.Resources.Resources.Truck_Lpg_Topview
        Me.pVehicle2.Location = New System.Drawing.Point(47, 87)
        Me.pVehicle2.Name = "pVehicle2"
        Me.pVehicle2.Size = New System.Drawing.Size(100, 397)
        Me.pVehicle2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pVehicle2.TabIndex = 43
        Me.pVehicle2.TabStop = False
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Panel23)
        Me.Panel6.Controls.Add(Me.Panel117)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(1124, 4)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(218, 579)
        Me.Panel6.TabIndex = 36
        '
        'Panel23
        '
        Me.Panel23.Controls.Add(Me.Panel24)
        Me.Panel23.Controls.Add(Me.Label153)
        Me.Panel23.Controls.Add(Me.Label162)
        Me.Panel23.Controls.Add(Me.Panel25)
        Me.Panel23.Controls.Add(Me.Label181)
        Me.Panel23.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel23.Location = New System.Drawing.Point(0, 0)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(218, 345)
        Me.Panel23.TabIndex = 45
        '
        'Panel24
        '
        Me.Panel24.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel24.BackColor = System.Drawing.Color.Transparent
        Me.Panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel24.Controls.Add(Me.Label154)
        Me.Panel24.Controls.Add(Me.Label155)
        Me.Panel24.Controls.Add(Me.Label156)
        Me.Panel24.Controls.Add(Me.Label157)
        Me.Panel24.Controls.Add(Me.Label158)
        Me.Panel24.Controls.Add(Me.Label159)
        Me.Panel24.Controls.Add(Me.Label160)
        Me.Panel24.Controls.Add(Me.Label161)
        Me.Panel24.Location = New System.Drawing.Point(-5, 103)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(227, 72)
        Me.Panel24.TabIndex = 77
        '
        'Label154
        '
        Me.Label154.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "DRIVER_PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label154.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label154.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label154.Location = New System.Drawing.Point(154, 51)
        Me.Label154.Name = "Label154"
        Me.Label154.Size = New System.Drawing.Size(51, 16)
        Me.Label154.TabIndex = 45
        Me.Label154.Text = "Preset"
        '
        'VBATCHSTATUSBindingSource5
        '
        Me.VBATCHSTATUSBindingSource5.DataMember = "V_BATCH_STATUS"
        Me.VBATCHSTATUSBindingSource5.DataSource = Me.FPTDataSet
        Me.VBATCHSTATUSBindingSource5.Filter = "BATCH_NUMBER=6"
        '
        'Label155
        '
        Me.Label155.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "DRIVER_PASSWORD", True))
        Me.Label155.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label155.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label155.Location = New System.Drawing.Point(154, 30)
        Me.Label155.Name = "Label155"
        Me.Label155.Size = New System.Drawing.Size(51, 16)
        Me.Label155.TabIndex = 44
        Me.Label155.Text = "Preset"
        '
        'Label156
        '
        Me.Label156.AutoSize = True
        Me.Label156.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label156.ForeColor = System.Drawing.Color.White
        Me.Label156.Location = New System.Drawing.Point(154, 7)
        Me.Label156.Name = "Label156"
        Me.Label156.Size = New System.Drawing.Size(53, 16)
        Me.Label156.TabIndex = 43
        Me.Label156.Text = "Opertor"
        '
        'Label157
        '
        Me.Label157.AutoSize = True
        Me.Label157.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label157.ForeColor = System.Drawing.Color.White
        Me.Label157.Location = New System.Drawing.Point(79, 7)
        Me.Label157.Name = "Label157"
        Me.Label157.Size = New System.Drawing.Size(42, 16)
        Me.Label157.TabIndex = 42
        Me.Label157.Text = "Batch"
        '
        'Label158
        '
        Me.Label158.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "PRESET", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label158.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label158.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label158.Location = New System.Drawing.Point(79, 51)
        Me.Label158.Name = "Label158"
        Me.Label158.Size = New System.Drawing.Size(51, 16)
        Me.Label158.TabIndex = 41
        Me.Label158.Text = "Preset"
        '
        'Label159
        '
        Me.Label159.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "PASSWORD", True))
        Me.Label159.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label159.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label159.Location = New System.Drawing.Point(79, 30)
        Me.Label159.Name = "Label159"
        Me.Label159.Size = New System.Drawing.Size(51, 16)
        Me.Label159.TabIndex = 40
        Me.Label159.Text = "Preset"
        '
        'Label160
        '
        Me.Label160.AutoSize = True
        Me.Label160.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label160.ForeColor = System.Drawing.Color.White
        Me.Label160.Location = New System.Drawing.Point(26, 50)
        Me.Label160.Name = "Label160"
        Me.Label160.Size = New System.Drawing.Size(53, 16)
        Me.Label160.TabIndex = 35
        Me.Label160.Text = "Preset :"
        '
        'Label161
        '
        Me.Label161.AutoSize = True
        Me.Label161.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label161.ForeColor = System.Drawing.Color.White
        Me.Label161.Location = New System.Drawing.Point(5, 29)
        Me.Label161.Name = "Label161"
        Me.Label161.Size = New System.Drawing.Size(74, 16)
        Me.Label161.TabIndex = 34
        Me.Label161.Text = "Password :"
        '
        'Label153
        '
        Me.Label153.AutoSize = True
        Me.Label153.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "Product_code", True))
        Me.Label153.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label153.ForeColor = System.Drawing.Color.Yellow
        Me.Label153.Location = New System.Drawing.Point(106, 72)
        Me.Label153.Name = "Label153"
        Me.Label153.Size = New System.Drawing.Size(54, 24)
        Me.Label153.TabIndex = 72
        Me.Label153.Text = " TEG"
        '
        'Label162
        '
        Me.Label162.AutoSize = True
        Me.Label162.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "BATCH_NAME", True))
        Me.Label162.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label162.ForeColor = System.Drawing.Color.Yellow
        Me.Label162.Location = New System.Drawing.Point(85, 44)
        Me.Label162.Name = "Label162"
        Me.Label162.Size = New System.Drawing.Size(100, 24)
        Me.Label162.TabIndex = 71
        Me.Label162.Text = "FQIC-1513"
        '
        'Panel25
        '
        Me.Panel25.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel25.BackColor = System.Drawing.Color.Transparent
        Me.Panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel25.Controls.Add(Me.Label163)
        Me.Panel25.Controls.Add(Me.Label164)
        Me.Panel25.Controls.Add(Me.Label165)
        Me.Panel25.Controls.Add(Me.Label166)
        Me.Panel25.Controls.Add(Me.Label167)
        Me.Panel25.Controls.Add(Me.Label168)
        Me.Panel25.Controls.Add(Me.Label169)
        Me.Panel25.Controls.Add(Me.Label170)
        Me.Panel25.Controls.Add(Me.Label171)
        Me.Panel25.Controls.Add(Me.Label172)
        Me.Panel25.Controls.Add(Me.Label173)
        Me.Panel25.Controls.Add(Me.Label174)
        Me.Panel25.Controls.Add(Me.Label175)
        Me.Panel25.Controls.Add(Me.Label176)
        Me.Panel25.Controls.Add(Me.Label177)
        Me.Panel25.Controls.Add(Me.Label178)
        Me.Panel25.Controls.Add(Me.Label179)
        Me.Panel25.Controls.Add(Me.Label180)
        Me.Panel25.Location = New System.Drawing.Point(-5, 181)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(227, 158)
        Me.Panel25.TabIndex = 74
        '
        'Label163
        '
        Me.Label163.AutoSize = True
        Me.Label163.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label163.ForeColor = System.Drawing.Color.White
        Me.Label163.Location = New System.Drawing.Point(156, 131)
        Me.Label163.Name = "Label163"
        Me.Label163.Size = New System.Drawing.Size(24, 16)
        Me.Label163.TabIndex = 51
        Me.Label163.Text = "Kg"
        '
        'Label164
        '
        Me.Label164.AutoSize = True
        Me.Label164.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label164.ForeColor = System.Drawing.Color.White
        Me.Label164.Location = New System.Drawing.Point(156, 107)
        Me.Label164.Name = "Label164"
        Me.Label164.Size = New System.Drawing.Size(24, 16)
        Me.Label164.TabIndex = 50
        Me.Label164.Text = "Kg"
        '
        'Label165
        '
        Me.Label165.AutoSize = True
        Me.Label165.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label165.ForeColor = System.Drawing.Color.White
        Me.Label165.Location = New System.Drawing.Point(156, 84)
        Me.Label165.Name = "Label165"
        Me.Label165.Size = New System.Drawing.Size(46, 16)
        Me.Label165.TabIndex = 49
        Me.Label165.Text = "Kg/m3"
        '
        'Label166
        '
        Me.Label166.AutoSize = True
        Me.Label166.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label166.ForeColor = System.Drawing.Color.White
        Me.Label166.Location = New System.Drawing.Point(156, 59)
        Me.Label166.Name = "Label166"
        Me.Label166.Size = New System.Drawing.Size(39, 16)
        Me.Label166.TabIndex = 48
        Me.Label166.Text = "Kg/hr"
        '
        'Label167
        '
        Me.Label167.AutoSize = True
        Me.Label167.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label167.ForeColor = System.Drawing.Color.White
        Me.Label167.Location = New System.Drawing.Point(156, 36)
        Me.Label167.Name = "Label167"
        Me.Label167.Size = New System.Drawing.Size(24, 16)
        Me.Label167.TabIndex = 47
        Me.Label167.Text = "Kg"
        '
        'Label168
        '
        Me.Label168.AutoSize = True
        Me.Label168.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label168.ForeColor = System.Drawing.Color.White
        Me.Label168.Location = New System.Drawing.Point(156, 14)
        Me.Label168.Name = "Label168"
        Me.Label168.Size = New System.Drawing.Size(24, 16)
        Me.Label168.TabIndex = 46
        Me.Label168.Text = "Kg"
        '
        'Label169
        '
        Me.Label169.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "BASE_ACCUM_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label169.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label169.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label169.Location = New System.Drawing.Point(79, 132)
        Me.Label169.Name = "Label169"
        Me.Label169.Size = New System.Drawing.Size(78, 16)
        Me.Label169.TabIndex = 45
        Me.Label169.Text = "Preset"
        '
        'Label170
        '
        Me.Label170.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "BASE_T", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label170.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label170.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label170.Location = New System.Drawing.Point(79, 108)
        Me.Label170.Name = "Label170"
        Me.Label170.Size = New System.Drawing.Size(78, 16)
        Me.Label170.TabIndex = 44
        Me.Label170.Text = "Preset"
        '
        'Label171
        '
        Me.Label171.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "DENSITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label171.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label171.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label171.Location = New System.Drawing.Point(79, 84)
        Me.Label171.Name = "Label171"
        Me.Label171.Size = New System.Drawing.Size(78, 16)
        Me.Label171.TabIndex = 43
        Me.Label171.Text = "Preset"
        '
        'Label172
        '
        Me.Label172.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "BASE_F", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.Label172.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label172.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label172.Location = New System.Drawing.Point(79, 60)
        Me.Label172.Name = "Label172"
        Me.Label172.Size = New System.Drawing.Size(78, 16)
        Me.Label172.TabIndex = 42
        Me.Label172.Text = "Preset"
        '
        'Label173
        '
        Me.Label173.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "BASE_G", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label173.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label173.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label173.Location = New System.Drawing.Point(79, 36)
        Me.Label173.Name = "Label173"
        Me.Label173.Size = New System.Drawing.Size(78, 16)
        Me.Label173.TabIndex = 41
        Me.Label173.Text = "Preset"
        '
        'Label174
        '
        Me.Label174.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VBATCHSTATUSBindingSource5, "BASE_P", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.Label174.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label174.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label174.Location = New System.Drawing.Point(79, 12)
        Me.Label174.Name = "Label174"
        Me.Label174.Size = New System.Drawing.Size(78, 16)
        Me.Label174.TabIndex = 40
        Me.Label174.Text = "Preset"
        '
        'Label175
        '
        Me.Label175.AutoSize = True
        Me.Label175.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label175.ForeColor = System.Drawing.Color.White
        Me.Label175.Location = New System.Drawing.Point(21, 131)
        Me.Label175.Name = "Label175"
        Me.Label175.Size = New System.Drawing.Size(58, 16)
        Me.Label175.TabIndex = 39
        Me.Label175.Text = "Accum. :"
        '
        'Label176
        '
        Me.Label176.AutoSize = True
        Me.Label176.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label176.ForeColor = System.Drawing.Color.White
        Me.Label176.Location = New System.Drawing.Point(26, 107)
        Me.Label176.Name = "Label176"
        Me.Label176.Size = New System.Drawing.Size(53, 16)
        Me.Label176.TabIndex = 38
        Me.Label176.Text = "Temp. :"
        '
        'Label177
        '
        Me.Label177.AutoSize = True
        Me.Label177.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label177.ForeColor = System.Drawing.Color.White
        Me.Label177.Location = New System.Drawing.Point(20, 83)
        Me.Label177.Name = "Label177"
        Me.Label177.Size = New System.Drawing.Size(59, 16)
        Me.Label177.TabIndex = 37
        Me.Label177.Text = "Density :"
        '
        'Label178
        '
        Me.Label178.AutoSize = True
        Me.Label178.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label178.ForeColor = System.Drawing.Color.White
        Me.Label178.Location = New System.Drawing.Point(11, 59)
        Me.Label178.Name = "Label178"
        Me.Label178.Size = New System.Drawing.Size(68, 16)
        Me.Label178.TabIndex = 36
        Me.Label178.Text = "Flow rate :"
        '
        'Label179
        '
        Me.Label179.AutoSize = True
        Me.Label179.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label179.ForeColor = System.Drawing.Color.White
        Me.Label179.Location = New System.Drawing.Point(23, 35)
        Me.Label179.Name = "Label179"
        Me.Label179.Size = New System.Drawing.Size(56, 16)
        Me.Label179.TabIndex = 35
        Me.Label179.Text = "Current :"
        '
        'Label180
        '
        Me.Label180.AutoSize = True
        Me.Label180.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label180.ForeColor = System.Drawing.Color.White
        Me.Label180.Location = New System.Drawing.Point(26, 11)
        Me.Label180.Name = "Label180"
        Me.Label180.Size = New System.Drawing.Size(53, 16)
        Me.Label180.TabIndex = 34
        Me.Label180.Text = "Preset :"
        '
        'Label181
        '
        Me.Label181.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Label181.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label181.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label181.ForeColor = System.Drawing.Color.White
        Me.Label181.Location = New System.Drawing.Point(0, 0)
        Me.Label181.Name = "Label181"
        Me.Label181.Size = New System.Drawing.Size(218, 36)
        Me.Label181.TabIndex = 73
        Me.Label181.Text = "BAY 6"
        Me.Label181.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel117
        '
        Me.Panel117.Controls.Add(Me.LLoadStatus6)
        Me.Panel117.Controls.Add(Me.LComm6)
        Me.Panel117.Controls.Add(Me.PComparment_bay6)
        Me.Panel117.Controls.Add(Me.pVehicle6)
        Me.Panel117.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel117.Location = New System.Drawing.Point(0, 54)
        Me.Panel117.Name = "Panel117"
        Me.Panel117.Size = New System.Drawing.Size(218, 525)
        Me.Panel117.TabIndex = 43
        '
        'LLoadStatus6
        '
        Me.LLoadStatus6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLoadStatus6.ForeColor = System.Drawing.Color.Magenta
        Me.LLoadStatus6.Location = New System.Drawing.Point(41, 69)
        Me.LLoadStatus6.Name = "LLoadStatus6"
        Me.LLoadStatus6.Size = New System.Drawing.Size(171, 20)
        Me.LLoadStatus6.TabIndex = 76
        Me.LLoadStatus6.Text = "Loading"
        Me.LLoadStatus6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LComm6
        '
        Me.LComm6.AutoSize = True
        Me.LComm6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LComm6.ForeColor = System.Drawing.Color.Magenta
        Me.LComm6.Location = New System.Drawing.Point(47, 48)
        Me.LComm6.Name = "LComm6"
        Me.LComm6.Size = New System.Drawing.Size(171, 20)
        Me.LComm6.TabIndex = 75
        Me.LComm6.Text = "Good Communications"
        '
        'PComparment_bay6
        '
        Me.PComparment_bay6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PComparment_bay6.BackColor = System.Drawing.Color.Silver
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp10)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp9)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp8)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp7)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp6)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp5)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp4)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp3)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp2)
        Me.PComparment_bay6.Controls.Add(Me.Bay6_Comp1)
        Me.PComparment_bay6.Location = New System.Drawing.Point(71, 221)
        Me.PComparment_bay6.Name = "PComparment_bay6"
        Me.PComparment_bay6.Size = New System.Drawing.Size(50, 218)
        Me.PComparment_bay6.TabIndex = 67
        '
        'Bay6_Comp10
        '
        Me.Bay6_Comp10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp10.Location = New System.Drawing.Point(0, 198)
        Me.Bay6_Comp10.MaxValue = 10000.0R
        Me.Bay6_Comp10.MinValue = 0.0R
        Me.Bay6_Comp10.Name = "Bay6_Comp10"
        Me.Bay6_Comp10.Progress = 30.0R
        Me.Bay6_Comp10.Size = New System.Drawing.Size(50, 20)
        Me.Bay6_Comp10.TabIndex = 16
        '
        'Bay6_Comp9
        '
        Me.Bay6_Comp9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp9.Location = New System.Drawing.Point(0, 176)
        Me.Bay6_Comp9.MaxValue = 10000.0R
        Me.Bay6_Comp9.MinValue = 0.0R
        Me.Bay6_Comp9.Name = "Bay6_Comp9"
        Me.Bay6_Comp9.Progress = 30.0R
        Me.Bay6_Comp9.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp9.TabIndex = 15
        '
        'Bay6_Comp8
        '
        Me.Bay6_Comp8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp8.Location = New System.Drawing.Point(0, 154)
        Me.Bay6_Comp8.MaxValue = 10000.0R
        Me.Bay6_Comp8.MinValue = 0.0R
        Me.Bay6_Comp8.Name = "Bay6_Comp8"
        Me.Bay6_Comp8.Progress = 30.0R
        Me.Bay6_Comp8.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp8.TabIndex = 14
        '
        'Bay6_Comp7
        '
        Me.Bay6_Comp7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp7.Location = New System.Drawing.Point(0, 132)
        Me.Bay6_Comp7.MaxValue = 10000.0R
        Me.Bay6_Comp7.MinValue = 0.0R
        Me.Bay6_Comp7.Name = "Bay6_Comp7"
        Me.Bay6_Comp7.Progress = 30.0R
        Me.Bay6_Comp7.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp7.TabIndex = 13
        '
        'Bay6_Comp6
        '
        Me.Bay6_Comp6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp6.Location = New System.Drawing.Point(0, 110)
        Me.Bay6_Comp6.MaxValue = 10000.0R
        Me.Bay6_Comp6.MinValue = 0.0R
        Me.Bay6_Comp6.Name = "Bay6_Comp6"
        Me.Bay6_Comp6.Progress = 30.0R
        Me.Bay6_Comp6.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp6.TabIndex = 12
        '
        'Bay6_Comp5
        '
        Me.Bay6_Comp5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp5.Location = New System.Drawing.Point(0, 88)
        Me.Bay6_Comp5.MaxValue = 10000.0R
        Me.Bay6_Comp5.MinValue = 0.0R
        Me.Bay6_Comp5.Name = "Bay6_Comp5"
        Me.Bay6_Comp5.Progress = 30.0R
        Me.Bay6_Comp5.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp5.TabIndex = 11
        '
        'Bay6_Comp4
        '
        Me.Bay6_Comp4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp4.Location = New System.Drawing.Point(0, 66)
        Me.Bay6_Comp4.MaxValue = 10000.0R
        Me.Bay6_Comp4.MinValue = 0.0R
        Me.Bay6_Comp4.Name = "Bay6_Comp4"
        Me.Bay6_Comp4.Progress = 30.0R
        Me.Bay6_Comp4.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp4.TabIndex = 10
        '
        'Bay6_Comp3
        '
        Me.Bay6_Comp3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp3.Location = New System.Drawing.Point(0, 44)
        Me.Bay6_Comp3.MaxValue = 10000.0R
        Me.Bay6_Comp3.MinValue = 0.0R
        Me.Bay6_Comp3.Name = "Bay6_Comp3"
        Me.Bay6_Comp3.Progress = 30.0R
        Me.Bay6_Comp3.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp3.TabIndex = 9
        '
        'Bay6_Comp2
        '
        Me.Bay6_Comp2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bay6_Comp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp2.Location = New System.Drawing.Point(0, 22)
        Me.Bay6_Comp2.MaxValue = 10000.0R
        Me.Bay6_Comp2.MinValue = 0.0R
        Me.Bay6_Comp2.Name = "Bay6_Comp2"
        Me.Bay6_Comp2.Progress = 30.0R
        Me.Bay6_Comp2.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp2.TabIndex = 8
        '
        'Bay6_Comp1
        '
        Me.Bay6_Comp1.BackColor = System.Drawing.Color.DarkRed
        Me.Bay6_Comp1.Color = System.Drawing.Color.DarkRed
        Me.Bay6_Comp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bay6_Comp1.Location = New System.Drawing.Point(0, 0)
        Me.Bay6_Comp1.MaxValue = 100.0R
        Me.Bay6_Comp1.MinValue = 0.0R
        Me.Bay6_Comp1.Name = "Bay6_Comp1"
        Me.Bay6_Comp1.Progress = 80.0R
        Me.Bay6_Comp1.Size = New System.Drawing.Size(50, 22)
        Me.Bay6_Comp1.TabIndex = 7
        '
        'pVehicle6
        '
        Me.pVehicle6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pVehicle6.Image = Global.TAS_TOL.My.Resources.Resources.Truck_Lpg_Topview
        Me.pVehicle6.Location = New System.Drawing.Point(46, 88)
        Me.pVehicle6.Name = "pVehicle6"
        Me.pVehicle6.Size = New System.Drawing.Size(100, 396)
        Me.pVehicle6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pVehicle6.TabIndex = 43
        Me.pVehicle6.TabStop = False
        '
        'V_BATCH_STATUSTableAdapter
        '
        Me.V_BATCH_STATUSTableAdapter.ClearBeforeFill = True
        '
        'BayOverview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1354, 733)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Name = "BayOverview"
        Me.Text = "BayOverview"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.H_panel.ResumeLayout(False)
        Me.H_panel.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.P_Weight.ResumeLayout(False)
        Me.P_Weight.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel200.ResumeLayout(False)
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        CType(Me.VBATCHSTATUSBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.Panel201.ResumeLayout(False)
        Me.Panel201.PerformLayout()
        Me.PComparment_bay3.ResumeLayout(False)
        CType(Me.pVehicle3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel155.ResumeLayout(False)
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.VBATCHSTATUSBindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel156.ResumeLayout(False)
        Me.Panel156.PerformLayout()
        Me.PComparment_bay4.ResumeLayout(False)
        CType(Me.pVehicle4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.VBATCHSTATUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel98.ResumeLayout(False)
        Me.Panel98.PerformLayout()
        Me.pIsland1_batch1.ResumeLayout(False)
        Me.pIsland1_batch1.PerformLayout()
        Me.Panel96.ResumeLayout(False)
        Me.Panel96.PerformLayout()
        Me.PComparment_bay1.ResumeLayout(False)
        CType(Me.pVehicle1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        CType(Me.VBATCHSTATUSBindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PComparment_bay5.ResumeLayout(False)
        CType(Me.pVehicle5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PISLAND1.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.VBATCHSTATUSBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel97.ResumeLayout(False)
        Me.Panel97.PerformLayout()
        Me.PComparment_bay2.ResumeLayout(False)
        CType(Me.pVehicle2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel24.ResumeLayout(False)
        Me.Panel24.PerformLayout()
        CType(Me.VBATCHSTATUSBindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel25.ResumeLayout(False)
        Me.Panel25.PerformLayout()
        Me.Panel117.ResumeLayout(False)
        Me.Panel117.PerformLayout()
        Me.PComparment_bay6.ResumeLayout(False)
        CType(Me.pVehicle6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents H_panel As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents M_NAME As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents M_TIME As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents M_GROUP As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents M_DATE As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PComparment_bay4 As System.Windows.Forms.Panel
    Friend WithEvents Bay4_Comp10 As Gauge.TGauge
    Friend WithEvents Bay4_Comp9 As Gauge.TGauge
    Friend WithEvents Bay4_Comp8 As Gauge.TGauge
    Friend WithEvents Bay4_Comp7 As Gauge.TGauge
    Friend WithEvents Bay4_Comp6 As Gauge.TGauge
    Friend WithEvents Bay4_Comp5 As Gauge.TGauge
    Friend WithEvents Bay4_Comp4 As Gauge.TGauge
    Friend WithEvents Bay4_Comp3 As Gauge.TGauge
    Friend WithEvents Bay4_Comp2 As Gauge.TGauge
    Friend WithEvents Bay4_Comp1 As Gauge.TGauge
    Friend WithEvents PComparment_bay3 As System.Windows.Forms.Panel
    Friend WithEvents Bay3_Comp10 As Gauge.TGauge
    Friend WithEvents Bay3_Comp9 As Gauge.TGauge
    Friend WithEvents Bay3_Comp8 As Gauge.TGauge
    Friend WithEvents Bay3_Comp7 As Gauge.TGauge
    Friend WithEvents Bay3_Comp6 As Gauge.TGauge
    Friend WithEvents Bay3_Comp5 As Gauge.TGauge
    Friend WithEvents Bay3_Comp4 As Gauge.TGauge
    Friend WithEvents Bay3_Comp3 As Gauge.TGauge
    Friend WithEvents Bay3_Comp2 As Gauge.TGauge
    Friend WithEvents Bay3_Comp1 As Gauge.TGauge
    Friend WithEvents pVehicle3 As System.Windows.Forms.PictureBox
    Friend WithEvents pVehicle4 As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents P_Weight As System.Windows.Forms.Panel
    Friend WithEvents W_DATETIME As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents weight_Unit As System.Windows.Forms.Label
    Friend WithEvents weight As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel200 As System.Windows.Forms.Panel
    Friend WithEvents Panel201 As System.Windows.Forms.Panel
    Friend WithEvents Panel155 As System.Windows.Forms.Panel
    Friend WithEvents Panel156 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel96 As System.Windows.Forms.Panel
    Friend WithEvents PComparment_bay1 As System.Windows.Forms.Panel
    Friend WithEvents Bay1_Comp10 As Gauge.TGauge
    Friend WithEvents Bay1_Comp9 As Gauge.TGauge
    Friend WithEvents Bay1_Comp8 As Gauge.TGauge
    Friend WithEvents Bay1_Comp7 As Gauge.TGauge
    Friend WithEvents Bay1_Comp6 As Gauge.TGauge
    Friend WithEvents Bay1_Comp5 As Gauge.TGauge
    Friend WithEvents Bay1_Comp4 As Gauge.TGauge
    Friend WithEvents Bay1_Comp3 As Gauge.TGauge
    Friend WithEvents Bay1_Comp2 As Gauge.TGauge
    Friend WithEvents Bay1_Comp1 As Gauge.TGauge
    Friend WithEvents pVehicle1 As System.Windows.Forms.PictureBox
    Friend WithEvents PComparment_bay5 As System.Windows.Forms.Panel
    Friend WithEvents Bay5_Comp10 As Gauge.TGauge
    Friend WithEvents Bay5_Comp9 As Gauge.TGauge
    Friend WithEvents Bay5_Comp8 As Gauge.TGauge
    Friend WithEvents Bay5_Comp7 As Gauge.TGauge
    Friend WithEvents Bay5_Comp6 As Gauge.TGauge
    Friend WithEvents Bay5_Comp5 As Gauge.TGauge
    Friend WithEvents Bay5_Comp4 As Gauge.TGauge
    Friend WithEvents Bay5_Comp3 As Gauge.TGauge
    Friend WithEvents Bay5_Comp2 As Gauge.TGauge
    Friend WithEvents Bay5_Comp1 As Gauge.TGauge
    Friend WithEvents pVehicle5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel117 As System.Windows.Forms.Panel
    Friend WithEvents PComparment_bay6 As System.Windows.Forms.Panel
    Friend WithEvents Bay6_Comp10 As Gauge.TGauge
    Friend WithEvents Bay6_Comp9 As Gauge.TGauge
    Friend WithEvents Bay6_Comp8 As Gauge.TGauge
    Friend WithEvents Bay6_Comp7 As Gauge.TGauge
    Friend WithEvents Bay6_Comp6 As Gauge.TGauge
    Friend WithEvents Bay6_Comp5 As Gauge.TGauge
    Friend WithEvents Bay6_Comp4 As Gauge.TGauge
    Friend WithEvents Bay6_Comp3 As Gauge.TGauge
    Friend WithEvents Bay6_Comp2 As Gauge.TGauge
    Friend WithEvents Bay6_Comp1 As Gauge.TGauge
    Friend WithEvents pVehicle6 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Panel98 As System.Windows.Forms.Panel
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents pIsland1_batch1 As System.Windows.Forms.Panel
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents PISLAND1 As System.Windows.Forms.Panel
    Friend WithEvents Panel97 As System.Windows.Forms.Panel
    Friend WithEvents PComparment_bay2 As System.Windows.Forms.Panel
    Friend WithEvents Bay2_Comp10 As Gauge.TGauge
    Friend WithEvents Bay2_Comp9 As Gauge.TGauge
    Friend WithEvents Bay2_Comp8 As Gauge.TGauge
    Friend WithEvents Bay2_Comp7 As Gauge.TGauge
    Friend WithEvents Bay2_Comp6 As Gauge.TGauge
    Friend WithEvents Bay2_Comp5 As Gauge.TGauge
    Friend WithEvents Bay2_Comp4 As Gauge.TGauge
    Friend WithEvents Bay2_Comp3 As Gauge.TGauge
    Friend WithEvents Bay2_Comp2 As Gauge.TGauge
    Friend WithEvents Bay2_Comp1 As Gauge.TGauge
    Friend WithEvents pVehicle2 As System.Windows.Forms.PictureBox
    Friend WithEvents LComm1 As System.Windows.Forms.Label
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents VBATCHSTATUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_BATCH_STATUSTableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_BATCH_STATUSTableAdapter
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents VBATCHSTATUSBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Panel19 As System.Windows.Forms.Panel
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents Label115 As System.Windows.Forms.Label
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents Label117 As System.Windows.Forms.Label
    Friend WithEvents Label118 As System.Windows.Forms.Label
    Friend WithEvents Label119 As System.Windows.Forms.Label
    Friend WithEvents Label120 As System.Windows.Forms.Label
    Friend WithEvents Label121 As System.Windows.Forms.Label
    Friend WithEvents Label122 As System.Windows.Forms.Label
    Friend WithEvents Label123 As System.Windows.Forms.Label
    Friend WithEvents Panel20 As System.Windows.Forms.Panel
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents Label133 As System.Windows.Forms.Label
    Friend WithEvents Panel22 As System.Windows.Forms.Panel
    Friend WithEvents Label134 As System.Windows.Forms.Label
    Friend WithEvents Label135 As System.Windows.Forms.Label
    Friend WithEvents Label136 As System.Windows.Forms.Label
    Friend WithEvents Label137 As System.Windows.Forms.Label
    Friend WithEvents Label138 As System.Windows.Forms.Label
    Friend WithEvents Label139 As System.Windows.Forms.Label
    Friend WithEvents Label140 As System.Windows.Forms.Label
    Friend WithEvents Label141 As System.Windows.Forms.Label
    Friend WithEvents Label142 As System.Windows.Forms.Label
    Friend WithEvents Label143 As System.Windows.Forms.Label
    Friend WithEvents Label144 As System.Windows.Forms.Label
    Friend WithEvents Label145 As System.Windows.Forms.Label
    Friend WithEvents Label146 As System.Windows.Forms.Label
    Friend WithEvents Label147 As System.Windows.Forms.Label
    Friend WithEvents Label148 As System.Windows.Forms.Label
    Friend WithEvents Label149 As System.Windows.Forms.Label
    Friend WithEvents Label150 As System.Windows.Forms.Label
    Friend WithEvents Label151 As System.Windows.Forms.Label
    Friend WithEvents Label152 As System.Windows.Forms.Label
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VBATCHSTATUSBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Panel23 As System.Windows.Forms.Panel
    Friend WithEvents Label153 As System.Windows.Forms.Label
    Friend WithEvents Label162 As System.Windows.Forms.Label
    Friend WithEvents Panel25 As System.Windows.Forms.Panel
    Friend WithEvents Label163 As System.Windows.Forms.Label
    Friend WithEvents Label164 As System.Windows.Forms.Label
    Friend WithEvents Label165 As System.Windows.Forms.Label
    Friend WithEvents Label166 As System.Windows.Forms.Label
    Friend WithEvents Label167 As System.Windows.Forms.Label
    Friend WithEvents Label168 As System.Windows.Forms.Label
    Friend WithEvents Label169 As System.Windows.Forms.Label
    Friend WithEvents Label170 As System.Windows.Forms.Label
    Friend WithEvents Label171 As System.Windows.Forms.Label
    Friend WithEvents Label172 As System.Windows.Forms.Label
    Friend WithEvents Label173 As System.Windows.Forms.Label
    Friend WithEvents Label174 As System.Windows.Forms.Label
    Friend WithEvents Label175 As System.Windows.Forms.Label
    Friend WithEvents Label176 As System.Windows.Forms.Label
    Friend WithEvents Label177 As System.Windows.Forms.Label
    Friend WithEvents Label178 As System.Windows.Forms.Label
    Friend WithEvents Label179 As System.Windows.Forms.Label
    Friend WithEvents Label180 As System.Windows.Forms.Label
    Friend WithEvents Label181 As System.Windows.Forms.Label
    Friend WithEvents VBATCHSTATUSBindingSource3 As System.Windows.Forms.BindingSource
    Friend WithEvents VBATCHSTATUSBindingSource4 As System.Windows.Forms.BindingSource
    Friend WithEvents VBATCHSTATUSBindingSource5 As System.Windows.Forms.BindingSource
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Panel21 As System.Windows.Forms.Panel
    Friend WithEvents Label125 As System.Windows.Forms.Label
    Friend WithEvents Label126 As System.Windows.Forms.Label
    Friend WithEvents Label127 As System.Windows.Forms.Label
    Friend WithEvents Label128 As System.Windows.Forms.Label
    Friend WithEvents Label129 As System.Windows.Forms.Label
    Friend WithEvents Label130 As System.Windows.Forms.Label
    Friend WithEvents Label131 As System.Windows.Forms.Label
    Friend WithEvents Label132 As System.Windows.Forms.Label
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Panel24 As System.Windows.Forms.Panel
    Friend WithEvents Label154 As System.Windows.Forms.Label
    Friend WithEvents Label155 As System.Windows.Forms.Label
    Friend WithEvents Label156 As System.Windows.Forms.Label
    Friend WithEvents Label157 As System.Windows.Forms.Label
    Friend WithEvents Label158 As System.Windows.Forms.Label
    Friend WithEvents Label159 As System.Windows.Forms.Label
    Friend WithEvents Label160 As System.Windows.Forms.Label
    Friend WithEvents Label161 As System.Windows.Forms.Label
    Friend WithEvents LLoadStatus3 As System.Windows.Forms.Label
    Friend WithEvents LComm3 As System.Windows.Forms.Label
    Friend WithEvents LLoadStatus4 As System.Windows.Forms.Label
    Friend WithEvents LComm4 As System.Windows.Forms.Label
    Friend WithEvents LLoadStatus1 As System.Windows.Forms.Label
    Friend WithEvents LLoadStatus5 As System.Windows.Forms.Label
    Friend WithEvents LComm5 As System.Windows.Forms.Label
    Friend WithEvents LLoadStatus2 As System.Windows.Forms.Label
    Friend WithEvents LComm2 As System.Windows.Forms.Label
    Friend WithEvents LLoadStatus6 As System.Windows.Forms.Label
    Friend WithEvents LComm6 As System.Windows.Forms.Label
End Class
