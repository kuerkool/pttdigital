﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Bay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Bay))
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape3 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.VLOADINGLOCALMODEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.VISLANDBAYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.V_BATCH_STATUSTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_BATCH_STATUSTableAdapter()
        Me.V_ISLANDBAYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_ISLANDBAYTableAdapter()
        Me.V_LOADING_LOCALMODETableAdapter = New TAS_TOL.FPTDataSetTableAdapters.V_LOADING_LOCALMODETableAdapter()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.EDTIMEUSE = New System.Windows.Forms.TextBox()
        Me.DEREMAIN = New System.Windows.Forms.TextBox()
        Me.EDGROSS = New System.Windows.Forms.TextBox()
        Me.EDPRESET = New System.Windows.Forms.TextBox()
        Me.EdCountComp = New System.Windows.Forms.TextBox()
        Me.EDPIN = New System.Windows.Forms.TextBox()
        Me.EDREF = New System.Windows.Forms.TextBox()
        Me.EDLOADVEH = New System.Windows.Forms.TextBox()
        Me.EdLoadID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LoadDetail = New System.Windows.Forms.DataGridView()
        Me.CSFDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REFERENCE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ORDERNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CVHDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TIMESTARTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LTIMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRESETSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GROSSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REMAINDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CVm1accum_START = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CVD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CVF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.PBay = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.CBBay = New System.Windows.Forms.ComboBox()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Pbatch8 = New System.Windows.Forms.Panel()
        Me.Accum_B8 = New System.Windows.Forms.Label()
        Me.PRO_B8 = New System.Windows.Forms.TextBox()
        Me.Name_B8 = New System.Windows.Forms.TextBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Pbatch7 = New System.Windows.Forms.Panel()
        Me.Accum_B7 = New System.Windows.Forms.Label()
        Me.PRO_B7 = New System.Windows.Forms.TextBox()
        Me.Name_B7 = New System.Windows.Forms.TextBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Pbatch6 = New System.Windows.Forms.Panel()
        Me.Accum_B6 = New System.Windows.Forms.Label()
        Me.PRO_B6 = New System.Windows.Forms.TextBox()
        Me.Name_B6 = New System.Windows.Forms.TextBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Pbatch5 = New System.Windows.Forms.Panel()
        Me.Accum_B5 = New System.Windows.Forms.Label()
        Me.PRO_B5 = New System.Windows.Forms.TextBox()
        Me.Name_B5 = New System.Windows.Forms.TextBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.Pbatch4 = New System.Windows.Forms.Panel()
        Me.Accum_B4 = New System.Windows.Forms.Label()
        Me.PRO_B4 = New System.Windows.Forms.TextBox()
        Me.Name_B4 = New System.Windows.Forms.TextBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Pbatch3 = New System.Windows.Forms.Panel()
        Me.Accum_B3 = New System.Windows.Forms.Label()
        Me.PRO_B3 = New System.Windows.Forms.TextBox()
        Me.Name_B3 = New System.Windows.Forms.TextBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Pbatch2 = New System.Windows.Forms.Panel()
        Me.Accum_B2 = New System.Windows.Forms.Label()
        Me.PRO_B2 = New System.Windows.Forms.TextBox()
        Me.Name_B2 = New System.Windows.Forms.TextBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Pbatch1 = New System.Windows.Forms.Panel()
        Me.Accum_B1 = New System.Windows.Forms.Label()
        Me.PRO_B1 = New System.Windows.Forms.TextBox()
        Me.Name_B1 = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.H_panel = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.P_Weight = New System.Windows.Forms.Panel()
        Me.W_DATETIME = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.weight_Unit = New System.Windows.Forms.Label()
        Me.weight = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.M_NAME = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.M_TIME = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.M_GROUP = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.M_DATE = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.MeterDetail = New System.Windows.Forms.DataGridView()
        Me.BATCHNUMBERDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BATCH_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BATCH_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MODENAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASECPMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASEPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASEGDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REMAIN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASETDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASEFDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BASEACCUMGDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.METER_STSTUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ShapeContainer3 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape6 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape5 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape4 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.PCompartment = New System.Windows.Forms.Panel()
        Me.TGauge20 = New Gauge.TGauge()
        Me.TGauge19 = New Gauge.TGauge()
        Me.TGauge18 = New Gauge.TGauge()
        Me.TGauge17 = New Gauge.TGauge()
        Me.TGauge16 = New Gauge.TGauge()
        Me.TGauge15 = New Gauge.TGauge()
        Me.TGauge14 = New Gauge.TGauge()
        Me.TGauge13 = New Gauge.TGauge()
        Me.TGauge12 = New Gauge.TGauge()
        Me.TGauge11 = New Gauge.TGauge()
        Me.TGauge10 = New Gauge.TGauge()
        Me.TGauge9 = New Gauge.TGauge()
        Me.TGauge8 = New Gauge.TGauge()
        Me.TGauge7 = New Gauge.TGauge()
        Me.TGauge6 = New Gauge.TGauge()
        Me.TGauge5 = New Gauge.TGauge()
        Me.TGauge4 = New Gauge.TGauge()
        Me.TGauge3 = New Gauge.TGauge()
        Me.TGauge2 = New Gauge.TGauge()
        Me.TGauge1 = New Gauge.TGauge()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.VLOADINGLOCALMODEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VISLANDBAYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.LoadDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PBay.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Pbatch8.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch7.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch6.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch5.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch4.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch3.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch2.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pbatch1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.H_panel.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.P_Weight.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel16.SuspendLayout()
        CType(Me.MeterDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel15.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PCompartment.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BackColor = System.Drawing.Color.Red
        Me.RectangleShape1.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape1.BorderColor = System.Drawing.Color.White
        Me.RectangleShape1.Location = New System.Drawing.Point(94, 6)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(45, 23)
        '
        'RectangleShape2
        '
        Me.RectangleShape2.BackColor = System.Drawing.Color.Lime
        Me.RectangleShape2.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape2.BorderColor = System.Drawing.Color.White
        Me.RectangleShape2.Location = New System.Drawing.Point(244, 6)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(45, 23)
        '
        'RectangleShape3
        '
        Me.RectangleShape3.BackColor = System.Drawing.Color.Lime
        Me.RectangleShape3.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape3.BorderColor = System.Drawing.Color.White
        Me.RectangleShape3.Location = New System.Drawing.Point(374, 6)
        Me.RectangleShape3.Name = "RectangleShape3"
        Me.RectangleShape3.Size = New System.Drawing.Size(45, 23)
        '
        'VLOADINGLOCALMODEBindingSource
        '
        Me.VLOADINGLOCALMODEBindingSource.DataMember = "V_LOADING_LOCALMODE"
        Me.VLOADINGLOCALMODEBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VISLANDBAYBindingSource
        '
        Me.VISLANDBAYBindingSource.DataMember = "V_ISLANDBAY"
        Me.VISLANDBAYBindingSource.DataSource = Me.FPTDataSet
        '
        'FPTDataSetBindingSource
        '
        Me.FPTDataSetBindingSource.DataMember = "V_BATCH_STATUS"
        Me.FPTDataSetBindingSource.DataSource = Me.FPTDataSet
        '
        'V_BATCH_STATUSTableAdapter
        '
        Me.V_BATCH_STATUSTableAdapter.ClearBeforeFill = True
        '
        'V_ISLANDBAYTableAdapter
        '
        Me.V_ISLANDBAYTableAdapter.ClearBeforeFill = True
        '
        'V_LOADING_LOCALMODETableAdapter
        '
        Me.V_LOADING_LOCALMODETableAdapter.ClearBeforeFill = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel5, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PBay, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.H_panel, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel11, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 136.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 252.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1348, 733)
        Me.TableLayoutPanel1.TabIndex = 30
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.TableLayoutPanel7)
        Me.Panel5.Controls.Add(Me.LoadDetail)
        Me.Panel5.Controls.Add(Me.Label25)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(4, 525)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1340, 204)
        Me.Panel5.TabIndex = 22
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel7.ColumnCount = 1
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.Panel6, 0, 0)
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(0, 30)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(1339, 39)
        Me.TableLayoutPanel7.TabIndex = 29
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label9)
        Me.Panel6.Controls.Add(Me.Label8)
        Me.Panel6.Controls.Add(Me.Label7)
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Controls.Add(Me.Label4)
        Me.Panel6.Controls.Add(Me.Label3)
        Me.Panel6.Controls.Add(Me.Label2)
        Me.Panel6.Controls.Add(Me.EDTIMEUSE)
        Me.Panel6.Controls.Add(Me.DEREMAIN)
        Me.Panel6.Controls.Add(Me.EDGROSS)
        Me.Panel6.Controls.Add(Me.EDPRESET)
        Me.Panel6.Controls.Add(Me.EdCountComp)
        Me.Panel6.Controls.Add(Me.EDPIN)
        Me.Panel6.Controls.Add(Me.EDREF)
        Me.Panel6.Controls.Add(Me.EDLOADVEH)
        Me.Panel6.Controls.Add(Me.EdLoadID)
        Me.Panel6.Controls.Add(Me.Label1)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(4, 4)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1331, 31)
        Me.Panel6.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(1139, 10)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(73, 16)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Load Time"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(998, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 16)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Remain"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(866, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 16)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "Gross"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(725, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 16)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Presets"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(605, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 16)
        Me.Label5.TabIndex = 31
        Me.Label5.Text = "Comp. Count"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(473, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 16)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Pin. No."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(323, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 16)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Ref. No."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(138, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 16)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Vehicle No."
        '
        'EDTIMEUSE
        '
        Me.EDTIMEUSE.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EDTIMEUSE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDTIMEUSE.Location = New System.Drawing.Point(1218, 5)
        Me.EDTIMEUSE.Name = "EDTIMEUSE"
        Me.EDTIMEUSE.Size = New System.Drawing.Size(80, 22)
        Me.EDTIMEUSE.TabIndex = 27
        '
        'DEREMAIN
        '
        Me.DEREMAIN.BackColor = System.Drawing.SystemColors.ControlDark
        Me.DEREMAIN.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DEREMAIN.Location = New System.Drawing.Point(1053, 5)
        Me.DEREMAIN.Name = "DEREMAIN"
        Me.DEREMAIN.Size = New System.Drawing.Size(80, 22)
        Me.DEREMAIN.TabIndex = 26
        '
        'EDGROSS
        '
        Me.EDGROSS.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EDGROSS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDGROSS.Location = New System.Drawing.Point(911, 5)
        Me.EDGROSS.Name = "EDGROSS"
        Me.EDGROSS.ReadOnly = True
        Me.EDGROSS.Size = New System.Drawing.Size(80, 22)
        Me.EDGROSS.TabIndex = 25
        '
        'EDPRESET
        '
        Me.EDPRESET.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EDPRESET.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDPRESET.Location = New System.Drawing.Point(780, 5)
        Me.EDPRESET.Name = "EDPRESET"
        Me.EDPRESET.ReadOnly = True
        Me.EDPRESET.Size = New System.Drawing.Size(80, 22)
        Me.EDPRESET.TabIndex = 24
        '
        'EdCountComp
        '
        Me.EdCountComp.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EdCountComp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EdCountComp.Location = New System.Drawing.Point(690, 5)
        Me.EdCountComp.Name = "EdCountComp"
        Me.EdCountComp.ReadOnly = True
        Me.EdCountComp.Size = New System.Drawing.Size(29, 22)
        Me.EdCountComp.TabIndex = 23
        '
        'EDPIN
        '
        Me.EDPIN.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EDPIN.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDPIN.Location = New System.Drawing.Point(532, 5)
        Me.EDPIN.Name = "EDPIN"
        Me.EDPIN.ReadOnly = True
        Me.EDPIN.Size = New System.Drawing.Size(67, 22)
        Me.EDPIN.TabIndex = 22
        '
        'EDREF
        '
        Me.EDREF.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EDREF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDREF.Location = New System.Drawing.Point(384, 5)
        Me.EDREF.Name = "EDREF"
        Me.EDREF.ReadOnly = True
        Me.EDREF.Size = New System.Drawing.Size(83, 22)
        Me.EDREF.TabIndex = 21
        '
        'EDLOADVEH
        '
        Me.EDLOADVEH.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EDLOADVEH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDLOADVEH.Location = New System.Drawing.Point(217, 5)
        Me.EDLOADVEH.Name = "EDLOADVEH"
        Me.EDLOADVEH.ReadOnly = True
        Me.EDLOADVEH.Size = New System.Drawing.Size(100, 22)
        Me.EDLOADVEH.TabIndex = 20
        '
        'EdLoadID
        '
        Me.EdLoadID.BackColor = System.Drawing.SystemColors.ControlDark
        Me.EdLoadID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EdLoadID.Location = New System.Drawing.Point(81, 5)
        Me.EdLoadID.Name = "EdLoadID"
        Me.EdLoadID.ReadOnly = True
        Me.EdLoadID.Size = New System.Drawing.Size(51, 22)
        Me.EdLoadID.TabIndex = 19
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(7, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Load No."
        '
        'LoadDetail
        '
        Me.LoadDetail.AllowUserToAddRows = False
        Me.LoadDetail.AllowUserToDeleteRows = False
        Me.LoadDetail.AllowUserToResizeRows = False
        Me.LoadDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LoadDetail.AutoGenerateColumns = False
        Me.LoadDetail.BackgroundColor = System.Drawing.Color.Black
        Me.LoadDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LoadDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LoadDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.LoadDetail.ColumnHeadersHeight = 50
        Me.LoadDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.LoadDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CSFDataGridViewTextBoxColumn, Me.REFERENCE, Me.ORDERNODataGridViewTextBoxColumn, Me.CVHDataGridViewTextBoxColumn, Me.TIMESTARTDataGridViewTextBoxColumn, Me.LTIMEDataGridViewTextBoxColumn, Me.ProductcodeDataGridViewTextBoxColumn, Me.PRESETSDataGridViewTextBoxColumn, Me.GROSSDataGridViewTextBoxColumn, Me.REMAINDataGridViewTextBoxColumn, Me.CVm1accum_START, Me.CVD, Me.CVF, Me.STATUS_NAME, Me.Column2})
        Me.LoadDetail.DataSource = Me.VLOADINGLOCALMODEBindingSource
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.LoadDetail.DefaultCellStyle = DataGridViewCellStyle16
        Me.LoadDetail.Location = New System.Drawing.Point(0, 75)
        Me.LoadDetail.MultiSelect = False
        Me.LoadDetail.Name = "LoadDetail"
        Me.LoadDetail.ReadOnly = True
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LoadDetail.RowHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.LoadDetail.RowHeadersVisible = False
        Me.LoadDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.LoadDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.LoadDetail.Size = New System.Drawing.Size(1340, 129)
        Me.LoadDetail.TabIndex = 21
        '
        'CSFDataGridViewTextBoxColumn
        '
        Me.CSFDataGridViewTextBoxColumn.DataPropertyName = "CSF"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = "-"
        Me.CSFDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.CSFDataGridViewTextBoxColumn.HeaderText = "Transection No."
        Me.CSFDataGridViewTextBoxColumn.Name = "CSFDataGridViewTextBoxColumn"
        Me.CSFDataGridViewTextBoxColumn.ReadOnly = True
        '
        'REFERENCE
        '
        Me.REFERENCE.DataPropertyName = "REFERENCE"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.NullValue = "-"
        Me.REFERENCE.DefaultCellStyle = DataGridViewCellStyle3
        Me.REFERENCE.HeaderText = "Reference"
        Me.REFERENCE.Name = "REFERENCE"
        Me.REFERENCE.ReadOnly = True
        '
        'ORDERNODataGridViewTextBoxColumn
        '
        Me.ORDERNODataGridViewTextBoxColumn.DataPropertyName = "ORDER_NO"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.NullValue = "-"
        Me.ORDERNODataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.ORDERNODataGridViewTextBoxColumn.HeaderText = "Order No."
        Me.ORDERNODataGridViewTextBoxColumn.Name = "ORDERNODataGridViewTextBoxColumn"
        Me.ORDERNODataGridViewTextBoxColumn.ReadOnly = True
        Me.ORDERNODataGridViewTextBoxColumn.Width = 80
        '
        'CVHDataGridViewTextBoxColumn
        '
        Me.CVHDataGridViewTextBoxColumn.DataPropertyName = "CVH"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = "-"
        Me.CVHDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.CVHDataGridViewTextBoxColumn.HeaderText = "Compartment No."
        Me.CVHDataGridViewTextBoxColumn.Name = "CVHDataGridViewTextBoxColumn"
        Me.CVHDataGridViewTextBoxColumn.ReadOnly = True
        Me.CVHDataGridViewTextBoxColumn.Width = 120
        '
        'TIMESTARTDataGridViewTextBoxColumn
        '
        Me.TIMESTARTDataGridViewTextBoxColumn.DataPropertyName = "TIME_START"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "T"
        DataGridViewCellStyle6.NullValue = "--:--"
        Me.TIMESTARTDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.TIMESTARTDataGridViewTextBoxColumn.HeaderText = "Time Start"
        Me.TIMESTARTDataGridViewTextBoxColumn.Name = "TIMESTARTDataGridViewTextBoxColumn"
        Me.TIMESTARTDataGridViewTextBoxColumn.ReadOnly = True
        Me.TIMESTARTDataGridViewTextBoxColumn.Width = 80
        '
        'LTIMEDataGridViewTextBoxColumn
        '
        Me.LTIMEDataGridViewTextBoxColumn.DataPropertyName = "LTIME"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Format = "T"
        DataGridViewCellStyle7.NullValue = "--:--"
        Me.LTIMEDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.LTIMEDataGridViewTextBoxColumn.HeaderText = "Time End"
        Me.LTIMEDataGridViewTextBoxColumn.Name = "LTIMEDataGridViewTextBoxColumn"
        Me.LTIMEDataGridViewTextBoxColumn.ReadOnly = True
        Me.LTIMEDataGridViewTextBoxColumn.Width = 80
        '
        'ProductcodeDataGridViewTextBoxColumn
        '
        Me.ProductcodeDataGridViewTextBoxColumn.DataPropertyName = "Product_code"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.NullValue = "-"
        Me.ProductcodeDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.ProductcodeDataGridViewTextBoxColumn.HeaderText = "Product"
        Me.ProductcodeDataGridViewTextBoxColumn.Name = "ProductcodeDataGridViewTextBoxColumn"
        Me.ProductcodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PRESETSDataGridViewTextBoxColumn
        '
        Me.PRESETSDataGridViewTextBoxColumn.DataPropertyName = "PRESETS"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N0"
        DataGridViewCellStyle9.NullValue = "0"
        Me.PRESETSDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.PRESETSDataGridViewTextBoxColumn.HeaderText = "Presets (kg)"
        Me.PRESETSDataGridViewTextBoxColumn.Name = "PRESETSDataGridViewTextBoxColumn"
        Me.PRESETSDataGridViewTextBoxColumn.ReadOnly = True
        '
        'GROSSDataGridViewTextBoxColumn
        '
        Me.GROSSDataGridViewTextBoxColumn.DataPropertyName = "GROSS"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = "0"
        Me.GROSSDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.GROSSDataGridViewTextBoxColumn.HeaderText = "Gross (kg)"
        Me.GROSSDataGridViewTextBoxColumn.Name = "GROSSDataGridViewTextBoxColumn"
        Me.GROSSDataGridViewTextBoxColumn.ReadOnly = True
        '
        'REMAINDataGridViewTextBoxColumn
        '
        Me.REMAINDataGridViewTextBoxColumn.DataPropertyName = "REMAIN"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = "0"
        Me.REMAINDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.REMAINDataGridViewTextBoxColumn.HeaderText = "Remain (kg)"
        Me.REMAINDataGridViewTextBoxColumn.Name = "REMAINDataGridViewTextBoxColumn"
        Me.REMAINDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CVm1accum_START
        '
        Me.CVm1accum_START.DataPropertyName = "CVm1accum_START"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = "0"
        Me.CVm1accum_START.DefaultCellStyle = DataGridViewCellStyle12
        Me.CVm1accum_START.HeaderText = "Accum. Start  (kg)"
        Me.CVm1accum_START.Name = "CVm1accum_START"
        Me.CVm1accum_START.ReadOnly = True
        Me.CVm1accum_START.Width = 150
        '
        'CVD
        '
        Me.CVD.DataPropertyName = "CVD"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N0"
        DataGridViewCellStyle13.NullValue = "0"
        Me.CVD.DefaultCellStyle = DataGridViewCellStyle13
        Me.CVD.HeaderText = "Accum. Stop (kg)"
        Me.CVD.Name = "CVD"
        Me.CVD.ReadOnly = True
        Me.CVD.Width = 150
        '
        'CVF
        '
        Me.CVF.DataPropertyName = "CVF"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = "0.00"
        Me.CVF.DefaultCellStyle = DataGridViewCellStyle14
        Me.CVF.HeaderText = "Temp.  (ํF)"
        Me.CVF.Name = "CVF"
        Me.CVF.ReadOnly = True
        Me.CVF.Width = 80
        '
        'STATUS_NAME
        '
        Me.STATUS_NAME.DataPropertyName = "STATUS_NAME"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.STATUS_NAME.DefaultCellStyle = DataGridViewCellStyle15
        Me.STATUS_NAME.HeaderText = "Load Status"
        Me.STATUS_NAME.Name = "STATUS_NAME"
        Me.STATUS_NAME.ReadOnly = True
        Me.STATUS_NAME.Width = 150
        '
        'Column2
        '
        Me.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column2.HeaderText = ""
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Label25
        '
        Me.Label25.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label25.Location = New System.Drawing.Point(0, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(1340, 24)
        Me.Label25.TabIndex = 20
        Me.Label25.Text = "LOADING DETAIL"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PBay
        '
        Me.PBay.Controls.Add(Me.TableLayoutPanel6)
        Me.PBay.Controls.Add(Me.Pbatch8)
        Me.PBay.Controls.Add(Me.Pbatch7)
        Me.PBay.Controls.Add(Me.Pbatch6)
        Me.PBay.Controls.Add(Me.Pbatch5)
        Me.PBay.Controls.Add(Me.Pbatch4)
        Me.PBay.Controls.Add(Me.Pbatch3)
        Me.PBay.Controls.Add(Me.Pbatch2)
        Me.PBay.Controls.Add(Me.Pbatch1)
        Me.PBay.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PBay.Location = New System.Drawing.Point(4, 141)
        Me.PBay.Name = "PBay"
        Me.PBay.Size = New System.Drawing.Size(1340, 124)
        Me.PBay.TabIndex = 20
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 213.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.Panel22, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.Panel13, 1, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Right
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(965, 0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(375, 124)
        Me.TableLayoutPanel6.TabIndex = 31
        '
        'Panel22
        '
        Me.Panel22.Controls.Add(Me.Label16)
        Me.Panel22.Controls.Add(Me.CBBay)
        Me.Panel22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel22.Location = New System.Drawing.Point(4, 4)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(153, 116)
        Me.Panel22.TabIndex = 30
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label16.Location = New System.Drawing.Point(14, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(128, 24)
        Me.Label16.TabIndex = 23
        Me.Label16.Text = "BAY/ISLAND"
        '
        'CBBay
        '
        Me.CBBay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CBBay.BackColor = System.Drawing.SystemColors.WindowText
        Me.CBBay.DataSource = Me.VISLANDBAYBindingSource
        Me.CBBay.DisplayMember = "BAY_ISLAND"
        Me.CBBay.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CBBay.ForeColor = System.Drawing.SystemColors.Window
        Me.CBBay.FormattingEnabled = True
        Me.CBBay.Location = New System.Drawing.Point(15, 57)
        Me.CBBay.Name = "CBBay"
        Me.CBBay.Size = New System.Drawing.Size(124, 28)
        Me.CBBay.TabIndex = 22
        Me.CBBay.ValueMember = "BAY_ISLAND"
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Panel14)
        Me.Panel13.Controls.Add(Me.Panel3)
        Me.Panel13.Controls.Add(Me.Panel2)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel13.Location = New System.Drawing.Point(164, 4)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(207, 116)
        Me.Panel13.TabIndex = 22
        '
        'Panel14
        '
        Me.Panel14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel14.BackColor = System.Drawing.Color.Silver
        Me.Panel14.Controls.Add(Me.Label18)
        Me.Panel14.Location = New System.Drawing.Point(2, 0)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(204, 31)
        Me.Panel14.TabIndex = 8
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Label18.Location = New System.Drawing.Point(58, 5)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(97, 20)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "Bay Status"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Red
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.ForeColor = System.Drawing.SystemColors.Window
        Me.Panel3.Location = New System.Drawing.Point(2, 75)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(202, 37)
        Me.Panel3.TabIndex = 6
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(83, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 18)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "ESD"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Green
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Location = New System.Drawing.Point(2, 34)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(202, 38)
        Me.Panel2.TabIndex = 5
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Window
        Me.Label13.Location = New System.Drawing.Point(64, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(83, 18)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Bay Mode"
        '
        'Pbatch8
        '
        Me.Pbatch8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch8.Controls.Add(Me.Accum_B8)
        Me.Pbatch8.Controls.Add(Me.PRO_B8)
        Me.Pbatch8.Controls.Add(Me.Name_B8)
        Me.Pbatch8.Controls.Add(Me.PictureBox10)
        Me.Pbatch8.Location = New System.Drawing.Point(835, 4)
        Me.Pbatch8.Name = "Pbatch8"
        Me.Pbatch8.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch8.TabIndex = 30
        '
        'Accum_B8
        '
        Me.Accum_B8.BackColor = System.Drawing.Color.Lime
        Me.Accum_B8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B8.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B8.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B8.Name = "Accum_B8"
        Me.Accum_B8.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B8.TabIndex = 4
        Me.Accum_B8.Text = "1234567890"
        Me.Accum_B8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B8
        '
        Me.PRO_B8.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B8.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B8.Name = "PRO_B8"
        Me.PRO_B8.ReadOnly = True
        Me.PRO_B8.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B8.TabIndex = 2
        Me.PRO_B8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B8
        '
        Me.Name_B8.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B8.Location = New System.Drawing.Point(1, 1)
        Me.Name_B8.Name = "Name_B8"
        Me.Name_B8.ReadOnly = True
        Me.Name_B8.Size = New System.Drawing.Size(54, 20)
        Me.Name_B8.TabIndex = 1
        Me.Name_B8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox10
        '
        Me.PictureBox10.ErrorImage = Nothing
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox10.TabIndex = 0
        Me.PictureBox10.TabStop = False
        '
        'Pbatch7
        '
        Me.Pbatch7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch7.Controls.Add(Me.Accum_B7)
        Me.Pbatch7.Controls.Add(Me.PRO_B7)
        Me.Pbatch7.Controls.Add(Me.Name_B7)
        Me.Pbatch7.Controls.Add(Me.PictureBox9)
        Me.Pbatch7.Location = New System.Drawing.Point(718, 4)
        Me.Pbatch7.Name = "Pbatch7"
        Me.Pbatch7.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch7.TabIndex = 28
        '
        'Accum_B7
        '
        Me.Accum_B7.BackColor = System.Drawing.Color.Lime
        Me.Accum_B7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B7.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B7.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B7.Name = "Accum_B7"
        Me.Accum_B7.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B7.TabIndex = 4
        Me.Accum_B7.Text = "1234567890"
        Me.Accum_B7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B7
        '
        Me.PRO_B7.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B7.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B7.Name = "PRO_B7"
        Me.PRO_B7.ReadOnly = True
        Me.PRO_B7.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B7.TabIndex = 2
        Me.PRO_B7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B7
        '
        Me.Name_B7.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B7.Location = New System.Drawing.Point(1, 1)
        Me.Name_B7.Name = "Name_B7"
        Me.Name_B7.ReadOnly = True
        Me.Name_B7.Size = New System.Drawing.Size(54, 20)
        Me.Name_B7.TabIndex = 1
        Me.Name_B7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox9
        '
        Me.PictureBox9.ErrorImage = Nothing
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox9.TabIndex = 0
        Me.PictureBox9.TabStop = False
        '
        'Pbatch6
        '
        Me.Pbatch6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch6.Controls.Add(Me.Accum_B6)
        Me.Pbatch6.Controls.Add(Me.PRO_B6)
        Me.Pbatch6.Controls.Add(Me.Name_B6)
        Me.Pbatch6.Controls.Add(Me.PictureBox7)
        Me.Pbatch6.Location = New System.Drawing.Point(599, 4)
        Me.Pbatch6.Name = "Pbatch6"
        Me.Pbatch6.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch6.TabIndex = 27
        '
        'Accum_B6
        '
        Me.Accum_B6.BackColor = System.Drawing.Color.Lime
        Me.Accum_B6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B6.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B6.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B6.Name = "Accum_B6"
        Me.Accum_B6.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B6.TabIndex = 4
        Me.Accum_B6.Text = "1234567890"
        Me.Accum_B6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B6
        '
        Me.PRO_B6.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B6.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B6.Name = "PRO_B6"
        Me.PRO_B6.ReadOnly = True
        Me.PRO_B6.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B6.TabIndex = 2
        Me.PRO_B6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B6
        '
        Me.Name_B6.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B6.Location = New System.Drawing.Point(1, 1)
        Me.Name_B6.Name = "Name_B6"
        Me.Name_B6.ReadOnly = True
        Me.Name_B6.Size = New System.Drawing.Size(54, 20)
        Me.Name_B6.TabIndex = 1
        Me.Name_B6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox7
        '
        Me.PictureBox7.ErrorImage = Nothing
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'Pbatch5
        '
        Me.Pbatch5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch5.Controls.Add(Me.Accum_B5)
        Me.Pbatch5.Controls.Add(Me.PRO_B5)
        Me.Pbatch5.Controls.Add(Me.Name_B5)
        Me.Pbatch5.Controls.Add(Me.PictureBox6)
        Me.Pbatch5.Location = New System.Drawing.Point(480, 4)
        Me.Pbatch5.Name = "Pbatch5"
        Me.Pbatch5.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch5.TabIndex = 26
        '
        'Accum_B5
        '
        Me.Accum_B5.BackColor = System.Drawing.Color.Lime
        Me.Accum_B5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B5.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B5.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B5.Name = "Accum_B5"
        Me.Accum_B5.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B5.TabIndex = 4
        Me.Accum_B5.Text = "1234567890"
        Me.Accum_B5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B5
        '
        Me.PRO_B5.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B5.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B5.Name = "PRO_B5"
        Me.PRO_B5.ReadOnly = True
        Me.PRO_B5.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B5.TabIndex = 2
        Me.PRO_B5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B5
        '
        Me.Name_B5.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B5.Location = New System.Drawing.Point(1, 1)
        Me.Name_B5.Name = "Name_B5"
        Me.Name_B5.ReadOnly = True
        Me.Name_B5.Size = New System.Drawing.Size(54, 20)
        Me.Name_B5.TabIndex = 1
        Me.Name_B5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox6
        '
        Me.PictureBox6.ErrorImage = Nothing
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 0
        Me.PictureBox6.TabStop = False
        '
        'Pbatch4
        '
        Me.Pbatch4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch4.Controls.Add(Me.Accum_B4)
        Me.Pbatch4.Controls.Add(Me.PRO_B4)
        Me.Pbatch4.Controls.Add(Me.Name_B4)
        Me.Pbatch4.Controls.Add(Me.PictureBox5)
        Me.Pbatch4.Location = New System.Drawing.Point(363, 4)
        Me.Pbatch4.Name = "Pbatch4"
        Me.Pbatch4.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch4.TabIndex = 25
        '
        'Accum_B4
        '
        Me.Accum_B4.BackColor = System.Drawing.Color.Lime
        Me.Accum_B4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B4.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B4.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B4.Name = "Accum_B4"
        Me.Accum_B4.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B4.TabIndex = 4
        Me.Accum_B4.Text = "1234567890"
        Me.Accum_B4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B4
        '
        Me.PRO_B4.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B4.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B4.Name = "PRO_B4"
        Me.PRO_B4.ReadOnly = True
        Me.PRO_B4.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B4.TabIndex = 2
        Me.PRO_B4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B4
        '
        Me.Name_B4.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B4.Location = New System.Drawing.Point(1, 1)
        Me.Name_B4.Name = "Name_B4"
        Me.Name_B4.ReadOnly = True
        Me.Name_B4.Size = New System.Drawing.Size(54, 20)
        Me.Name_B4.TabIndex = 1
        Me.Name_B4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox5
        '
        Me.PictureBox5.ErrorImage = Nothing
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 0
        Me.PictureBox5.TabStop = False
        '
        'Pbatch3
        '
        Me.Pbatch3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch3.Controls.Add(Me.Accum_B3)
        Me.Pbatch3.Controls.Add(Me.PRO_B3)
        Me.Pbatch3.Controls.Add(Me.Name_B3)
        Me.Pbatch3.Controls.Add(Me.PictureBox4)
        Me.Pbatch3.Location = New System.Drawing.Point(244, 4)
        Me.Pbatch3.Name = "Pbatch3"
        Me.Pbatch3.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch3.TabIndex = 24
        '
        'Accum_B3
        '
        Me.Accum_B3.BackColor = System.Drawing.Color.Lime
        Me.Accum_B3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B3.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B3.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B3.Name = "Accum_B3"
        Me.Accum_B3.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B3.TabIndex = 4
        Me.Accum_B3.Text = "1234567890"
        Me.Accum_B3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B3
        '
        Me.PRO_B3.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B3.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B3.Name = "PRO_B3"
        Me.PRO_B3.ReadOnly = True
        Me.PRO_B3.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B3.TabIndex = 2
        Me.PRO_B3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B3
        '
        Me.Name_B3.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B3.Location = New System.Drawing.Point(1, 1)
        Me.Name_B3.Name = "Name_B3"
        Me.Name_B3.ReadOnly = True
        Me.Name_B3.Size = New System.Drawing.Size(54, 20)
        Me.Name_B3.TabIndex = 1
        Me.Name_B3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox4
        '
        Me.PictureBox4.ErrorImage = Nothing
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'Pbatch2
        '
        Me.Pbatch2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch2.Controls.Add(Me.Accum_B2)
        Me.Pbatch2.Controls.Add(Me.PRO_B2)
        Me.Pbatch2.Controls.Add(Me.Name_B2)
        Me.Pbatch2.Controls.Add(Me.PictureBox3)
        Me.Pbatch2.Location = New System.Drawing.Point(125, 4)
        Me.Pbatch2.Name = "Pbatch2"
        Me.Pbatch2.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch2.TabIndex = 23
        '
        'Accum_B2
        '
        Me.Accum_B2.BackColor = System.Drawing.Color.Lime
        Me.Accum_B2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B2.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B2.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B2.Name = "Accum_B2"
        Me.Accum_B2.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B2.TabIndex = 4
        Me.Accum_B2.Text = "1234567890"
        Me.Accum_B2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B2
        '
        Me.PRO_B2.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B2.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B2.Name = "PRO_B2"
        Me.PRO_B2.ReadOnly = True
        Me.PRO_B2.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B2.TabIndex = 2
        Me.PRO_B2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B2
        '
        Me.Name_B2.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B2.Location = New System.Drawing.Point(1, 1)
        Me.Name_B2.Name = "Name_B2"
        Me.Name_B2.ReadOnly = True
        Me.Name_B2.Size = New System.Drawing.Size(54, 20)
        Me.Name_B2.TabIndex = 1
        Me.Name_B2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox3
        '
        Me.PictureBox3.ErrorImage = Nothing
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 0
        Me.PictureBox3.TabStop = False
        '
        'Pbatch1
        '
        Me.Pbatch1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pbatch1.Controls.Add(Me.Accum_B1)
        Me.Pbatch1.Controls.Add(Me.PRO_B1)
        Me.Pbatch1.Controls.Add(Me.Name_B1)
        Me.Pbatch1.Controls.Add(Me.PictureBox2)
        Me.Pbatch1.Location = New System.Drawing.Point(6, 3)
        Me.Pbatch1.Name = "Pbatch1"
        Me.Pbatch1.Size = New System.Drawing.Size(113, 117)
        Me.Pbatch1.TabIndex = 14
        '
        'Accum_B1
        '
        Me.Accum_B1.BackColor = System.Drawing.Color.Lime
        Me.Accum_B1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Accum_B1.Font = New System.Drawing.Font("DS-Digital", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accum_B1.ForeColor = System.Drawing.Color.Black
        Me.Accum_B1.Location = New System.Drawing.Point(26, 43)
        Me.Accum_B1.Name = "Accum_B1"
        Me.Accum_B1.Size = New System.Drawing.Size(62, 15)
        Me.Accum_B1.TabIndex = 3
        Me.Accum_B1.Text = "1234567890"
        Me.Accum_B1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PRO_B1
        '
        Me.PRO_B1.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.PRO_B1.Location = New System.Drawing.Point(56, 1)
        Me.PRO_B1.Name = "PRO_B1"
        Me.PRO_B1.ReadOnly = True
        Me.PRO_B1.Size = New System.Drawing.Size(54, 20)
        Me.PRO_B1.TabIndex = 2
        Me.PRO_B1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Name_B1
        '
        Me.Name_B1.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Name_B1.Location = New System.Drawing.Point(1, 1)
        Me.Name_B1.Name = "Name_B1"
        Me.Name_B1.ReadOnly = True
        Me.Name_B1.Size = New System.Drawing.Size(54, 20)
        Me.Name_B1.TabIndex = 1
        Me.Name_B1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PictureBox2
        '
        Me.PictureBox2.ErrorImage = Nothing
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-1, 20)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(111, 94)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'H_panel
        '
        Me.H_panel.Controls.Add(Me.TableLayoutPanel2)
        Me.H_panel.Controls.Add(Me.TableLayoutPanel3)
        Me.H_panel.Controls.Add(Me.Label27)
        Me.H_panel.Controls.Add(Me.PictureBox8)
        Me.H_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.H_panel.Location = New System.Drawing.Point(4, 4)
        Me.H_panel.Name = "H_panel"
        Me.H_panel.Size = New System.Drawing.Size(1340, 130)
        Me.H_panel.TabIndex = 18
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.P_Weight, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(438, 57)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(464, 70)
        Me.TableLayoutPanel2.TabIndex = 29
        '
        'P_Weight
        '
        Me.P_Weight.Controls.Add(Me.W_DATETIME)
        Me.P_Weight.Controls.Add(Me.Label20)
        Me.P_Weight.Controls.Add(Me.weight_Unit)
        Me.P_Weight.Controls.Add(Me.weight)
        Me.P_Weight.Controls.Add(Me.Label23)
        Me.P_Weight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.P_Weight.Location = New System.Drawing.Point(4, 4)
        Me.P_Weight.Name = "P_Weight"
        Me.P_Weight.Size = New System.Drawing.Size(456, 62)
        Me.P_Weight.TabIndex = 27
        '
        'W_DATETIME
        '
        Me.W_DATETIME.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.W_DATETIME.AutoSize = True
        Me.W_DATETIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.W_DATETIME.ForeColor = System.Drawing.Color.White
        Me.W_DATETIME.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.W_DATETIME.Location = New System.Drawing.Point(63, 48)
        Me.W_DATETIME.Name = "W_DATETIME"
        Me.W_DATETIME.Size = New System.Drawing.Size(114, 13)
        Me.W_DATETIME.TabIndex = 32
        Me.W_DATETIME.Text = "dd/MM/yyyy hh:mm:ss"
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(10, 48)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(57, 13)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Updated : "
        '
        'weight_Unit
        '
        Me.weight_Unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weight_Unit.AutoSize = True
        Me.weight_Unit.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight_Unit.ForeColor = System.Drawing.Color.Lime
        Me.weight_Unit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight_Unit.Location = New System.Drawing.Point(376, 2)
        Me.weight_Unit.Name = "weight_Unit"
        Me.weight_Unit.Size = New System.Drawing.Size(77, 52)
        Me.weight_Unit.TabIndex = 31
        Me.weight_Unit.Text = "KG"
        '
        'weight
        '
        Me.weight.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight.ForeColor = System.Drawing.Color.Lime
        Me.weight.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight.Location = New System.Drawing.Point(167, 2)
        Me.weight.Name = "weight"
        Me.weight.Size = New System.Drawing.Size(210, 52)
        Me.weight.TabIndex = 30
        Me.weight.Text = "0"
        Me.weight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(3, 2)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(173, 52)
        Me.Label23.TabIndex = 29
        Me.Label23.Text = "WEIGHT"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel7, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel8, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel9, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel10, 0, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(1093, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(247, 129)
        Me.TableLayoutPanel3.TabIndex = 28
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.Controls.Add(Me.Label15)
        Me.Panel7.Controls.Add(Me.M_NAME)
        Me.Panel7.Location = New System.Drawing.Point(4, 68)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(239, 25)
        Me.Panel7.TabIndex = 42
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(17, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(63, 20)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "NAME :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_NAME
        '
        Me.M_NAME.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_NAME.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_NAME.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_NAME.Location = New System.Drawing.Point(76, 1)
        Me.M_NAME.Name = "M_NAME"
        Me.M_NAME.Size = New System.Drawing.Size(159, 24)
        Me.M_NAME.TabIndex = 36
        Me.M_NAME.Text = "NONE"
        Me.M_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.Controls.Add(Me.Label21)
        Me.Panel8.Controls.Add(Me.M_TIME)
        Me.Panel8.Location = New System.Drawing.Point(4, 4)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(239, 25)
        Me.Panel8.TabIndex = 41
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(21, 4)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(59, 20)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "TIME  :"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_TIME
        '
        Me.M_TIME.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_TIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_TIME.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_TIME.Location = New System.Drawing.Point(76, 1)
        Me.M_TIME.Name = "M_TIME"
        Me.M_TIME.Size = New System.Drawing.Size(159, 26)
        Me.M_TIME.TabIndex = 36
        Me.M_TIME.Text = "HH:MM:SS"
        Me.M_TIME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel9
        '
        Me.Panel9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel9.Controls.Add(Me.Label22)
        Me.Panel9.Controls.Add(Me.M_GROUP)
        Me.Panel9.Location = New System.Drawing.Point(4, 100)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(239, 25)
        Me.Panel9.TabIndex = 43
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(4, 4)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(76, 20)
        Me.Label22.TabIndex = 37
        Me.Label22.Text = "GROUP :"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_GROUP
        '
        Me.M_GROUP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_GROUP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_GROUP.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_GROUP.Location = New System.Drawing.Point(76, 1)
        Me.M_GROUP.Name = "M_GROUP"
        Me.M_GROUP.Size = New System.Drawing.Size(159, 25)
        Me.M_GROUP.TabIndex = 36
        Me.M_GROUP.Text = "NONE"
        Me.M_GROUP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.Controls.Add(Me.Label24)
        Me.Panel10.Controls.Add(Me.M_DATE)
        Me.Panel10.Location = New System.Drawing.Point(4, 36)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(239, 25)
        Me.Panel10.TabIndex = 40
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(20, 4)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(60, 20)
        Me.Label24.TabIndex = 37
        Me.Label24.Text = "DATE :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_DATE
        '
        Me.M_DATE.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_DATE.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_DATE.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_DATE.Location = New System.Drawing.Point(76, 1)
        Me.M_DATE.Name = "M_DATE"
        Me.M_DATE.Size = New System.Drawing.Size(159, 26)
        Me.M_DATE.TabIndex = 36
        Me.M_DATE.Text = "dd/mm/yyyy"
        Me.M_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(587, 16)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(203, 37)
        Me.Label27.TabIndex = 25
        Me.Label27.Text = "BAY DETAIL"
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.InitialImage = Nothing
        Me.PictureBox8.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(316, 129)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 15
        Me.PictureBox8.TabStop = False
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.TableLayoutPanel4)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel11.Location = New System.Drawing.Point(4, 272)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(1340, 246)
        Me.Panel11.TabIndex = 21
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 473.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel16, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel15, 0, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1340, 246)
        Me.TableLayoutPanel4.TabIndex = 24
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.Label19)
        Me.Panel16.Controls.Add(Me.MeterDetail)
        Me.Panel16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel16.Location = New System.Drawing.Point(478, 4)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(858, 238)
        Me.Panel16.TabIndex = 23
        '
        'Label19
        '
        Me.Label19.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label19.Location = New System.Drawing.Point(0, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(858, 24)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "METER DETAIL"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MeterDetail
        '
        Me.MeterDetail.AllowUserToAddRows = False
        Me.MeterDetail.AllowUserToDeleteRows = False
        Me.MeterDetail.AllowUserToResizeRows = False
        Me.MeterDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MeterDetail.AutoGenerateColumns = False
        Me.MeterDetail.BackgroundColor = System.Drawing.Color.Black
        Me.MeterDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.MeterDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.MeterDetail.ColumnHeadersHeight = 50
        Me.MeterDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.MeterDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BATCHNUMBERDataGridViewTextBoxColumn, Me.BATCH_DATE, Me.BATCH_NAME, Me.MODENAMEDataGridViewTextBoxColumn, Me.BASECPMDataGridViewTextBoxColumn, Me.BASEPDataGridViewTextBoxColumn, Me.BASEGDataGridViewTextBoxColumn, Me.REMAIN, Me.BASETDataGridViewTextBoxColumn, Me.BASEFDataGridViewTextBoxColumn, Me.BASEACCUMGDataGridViewTextBoxColumn, Me.METER_STSTUS_NAME})
        Me.MeterDetail.DataSource = Me.FPTDataSetBindingSource
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.Black
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MeterDetail.DefaultCellStyle = DataGridViewCellStyle28
        Me.MeterDetail.GridColor = System.Drawing.SystemColors.Control
        Me.MeterDetail.Location = New System.Drawing.Point(2, 25)
        Me.MeterDetail.MultiSelect = False
        Me.MeterDetail.Name = "MeterDetail"
        Me.MeterDetail.ReadOnly = True
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MeterDetail.RowHeadersDefaultCellStyle = DataGridViewCellStyle29
        Me.MeterDetail.RowHeadersVisible = False
        Me.MeterDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MeterDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.MeterDetail.Size = New System.Drawing.Size(856, 213)
        Me.MeterDetail.TabIndex = 19
        '
        'BATCHNUMBERDataGridViewTextBoxColumn
        '
        Me.BATCHNUMBERDataGridViewTextBoxColumn.DataPropertyName = "BATCH_NUMBER"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.BATCHNUMBERDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle19
        Me.BATCHNUMBERDataGridViewTextBoxColumn.Frozen = True
        Me.BATCHNUMBERDataGridViewTextBoxColumn.HeaderText = "No."
        Me.BATCHNUMBERDataGridViewTextBoxColumn.Name = "BATCHNUMBERDataGridViewTextBoxColumn"
        Me.BATCHNUMBERDataGridViewTextBoxColumn.ReadOnly = True
        Me.BATCHNUMBERDataGridViewTextBoxColumn.Width = 40
        '
        'BATCH_DATE
        '
        Me.BATCH_DATE.DataPropertyName = "BATCH_DATE"
        DataGridViewCellStyle20.Format = "G"
        DataGridViewCellStyle20.NullValue = Nothing
        Me.BATCH_DATE.DefaultCellStyle = DataGridViewCellStyle20
        Me.BATCH_DATE.Frozen = True
        Me.BATCH_DATE.HeaderText = "Date & Time"
        Me.BATCH_DATE.Name = "BATCH_DATE"
        Me.BATCH_DATE.ReadOnly = True
        Me.BATCH_DATE.Width = 170
        '
        'BATCH_NAME
        '
        Me.BATCH_NAME.DataPropertyName = "BATCH_NAME"
        Me.BATCH_NAME.Frozen = True
        Me.BATCH_NAME.HeaderText = "Name"
        Me.BATCH_NAME.Name = "BATCH_NAME"
        Me.BATCH_NAME.ReadOnly = True
        '
        'MODENAMEDataGridViewTextBoxColumn
        '
        Me.MODENAMEDataGridViewTextBoxColumn.DataPropertyName = "MODE_NAME"
        Me.MODENAMEDataGridViewTextBoxColumn.Frozen = True
        Me.MODENAMEDataGridViewTextBoxColumn.HeaderText = "Mode"
        Me.MODENAMEDataGridViewTextBoxColumn.Name = "MODENAMEDataGridViewTextBoxColumn"
        Me.MODENAMEDataGridViewTextBoxColumn.ReadOnly = True
        Me.MODENAMEDataGridViewTextBoxColumn.Width = 70
        '
        'BASECPMDataGridViewTextBoxColumn
        '
        Me.BASECPMDataGridViewTextBoxColumn.DataPropertyName = "BASE_CPM"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.Format = "N0"
        DataGridViewCellStyle21.NullValue = Nothing
        Me.BASECPMDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle21
        Me.BASECPMDataGridViewTextBoxColumn.Frozen = True
        Me.BASECPMDataGridViewTextBoxColumn.HeaderText = "Comp."
        Me.BASECPMDataGridViewTextBoxColumn.Name = "BASECPMDataGridViewTextBoxColumn"
        Me.BASECPMDataGridViewTextBoxColumn.ReadOnly = True
        Me.BASECPMDataGridViewTextBoxColumn.Width = 60
        '
        'BASEPDataGridViewTextBoxColumn
        '
        Me.BASEPDataGridViewTextBoxColumn.DataPropertyName = "BASE_P"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle22.Format = "N0"
        DataGridViewCellStyle22.NullValue = "0"
        Me.BASEPDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle22
        Me.BASEPDataGridViewTextBoxColumn.Frozen = True
        Me.BASEPDataGridViewTextBoxColumn.HeaderText = "Preset (kg)"
        Me.BASEPDataGridViewTextBoxColumn.Name = "BASEPDataGridViewTextBoxColumn"
        Me.BASEPDataGridViewTextBoxColumn.ReadOnly = True
        Me.BASEPDataGridViewTextBoxColumn.Width = 90
        '
        'BASEGDataGridViewTextBoxColumn
        '
        Me.BASEGDataGridViewTextBoxColumn.DataPropertyName = "BASE_G"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle23.Format = "N0"
        DataGridViewCellStyle23.NullValue = "0"
        Me.BASEGDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle23
        Me.BASEGDataGridViewTextBoxColumn.Frozen = True
        Me.BASEGDataGridViewTextBoxColumn.HeaderText = "Gross (kg)"
        Me.BASEGDataGridViewTextBoxColumn.Name = "BASEGDataGridViewTextBoxColumn"
        Me.BASEGDataGridViewTextBoxColumn.ReadOnly = True
        Me.BASEGDataGridViewTextBoxColumn.Width = 90
        '
        'REMAIN
        '
        Me.REMAIN.DataPropertyName = "REMAIN"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle24.Format = "N0"
        DataGridViewCellStyle24.NullValue = "0"
        Me.REMAIN.DefaultCellStyle = DataGridViewCellStyle24
        Me.REMAIN.Frozen = True
        Me.REMAIN.HeaderText = "Remain (kg)"
        Me.REMAIN.Name = "REMAIN"
        Me.REMAIN.ReadOnly = True
        '
        'BASETDataGridViewTextBoxColumn
        '
        Me.BASETDataGridViewTextBoxColumn.DataPropertyName = "BASE_T"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle25.Format = "N2"
        DataGridViewCellStyle25.NullValue = "0.00"
        Me.BASETDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle25
        Me.BASETDataGridViewTextBoxColumn.Frozen = True
        Me.BASETDataGridViewTextBoxColumn.HeaderText = "Temp.  (ํF)"
        Me.BASETDataGridViewTextBoxColumn.Name = "BASETDataGridViewTextBoxColumn"
        Me.BASETDataGridViewTextBoxColumn.ReadOnly = True
        Me.BASETDataGridViewTextBoxColumn.Width = 86
        '
        'BASEFDataGridViewTextBoxColumn
        '
        Me.BASEFDataGridViewTextBoxColumn.DataPropertyName = "BASE_F"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "N0"
        DataGridViewCellStyle26.NullValue = Nothing
        Me.BASEFDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle26
        Me.BASEFDataGridViewTextBoxColumn.Frozen = True
        Me.BASEFDataGridViewTextBoxColumn.HeaderText = "Flow (Kg/min)"
        Me.BASEFDataGridViewTextBoxColumn.Name = "BASEFDataGridViewTextBoxColumn"
        Me.BASEFDataGridViewTextBoxColumn.ReadOnly = True
        Me.BASEFDataGridViewTextBoxColumn.Width = 80
        '
        'BASEACCUMGDataGridViewTextBoxColumn
        '
        Me.BASEACCUMGDataGridViewTextBoxColumn.DataPropertyName = "BASE_ACCUM_G"
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle27.Format = "N0"
        DataGridViewCellStyle27.NullValue = Nothing
        Me.BASEACCUMGDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle27
        Me.BASEACCUMGDataGridViewTextBoxColumn.Frozen = True
        Me.BASEACCUMGDataGridViewTextBoxColumn.HeaderText = "Accum (kg)"
        Me.BASEACCUMGDataGridViewTextBoxColumn.Name = "BASEACCUMGDataGridViewTextBoxColumn"
        Me.BASEACCUMGDataGridViewTextBoxColumn.ReadOnly = True
        Me.BASEACCUMGDataGridViewTextBoxColumn.Width = 114
        '
        'METER_STSTUS_NAME
        '
        Me.METER_STSTUS_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.METER_STSTUS_NAME.DataPropertyName = "METER_STSTUS_NAME"
        Me.METER_STSTUS_NAME.HeaderText = "Status"
        Me.METER_STSTUS_NAME.Name = "METER_STSTUS_NAME"
        Me.METER_STSTUS_NAME.ReadOnly = True
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.TableLayoutPanel5)
        Me.Panel15.Controls.Add(Me.Label17)
        Me.Panel15.Controls.Add(Me.PCompartment)
        Me.Panel15.Controls.Add(Me.PictureBox1)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel15.Location = New System.Drawing.Point(4, 4)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(467, 238)
        Me.Panel15.TabIndex = 22
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 27)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(464, 49)
        Me.TableLayoutPanel5.TabIndex = 28
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.ShapeContainer3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.ForeColor = System.Drawing.SystemColors.Window
        Me.Panel1.Location = New System.Drawing.Point(4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(456, 41)
        Me.Panel1.TabIndex = 32
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(324, 15)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Earth :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(142, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(97, 13)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Overfill Detect :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(10, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(78, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Arm Detect :"
        '
        'ShapeContainer3
        '
        Me.ShapeContainer3.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer3.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer3.Name = "ShapeContainer3"
        Me.ShapeContainer3.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape6, Me.RectangleShape5, Me.RectangleShape4})
        Me.ShapeContainer3.Size = New System.Drawing.Size(456, 41)
        Me.ShapeContainer3.TabIndex = 5
        Me.ShapeContainer3.TabStop = False
        '
        'RectangleShape6
        '
        Me.RectangleShape6.BackColor = System.Drawing.Color.Red
        Me.RectangleShape6.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape6.BorderColor = System.Drawing.Color.White
        Me.RectangleShape6.Location = New System.Drawing.Point(373, 9)
        Me.RectangleShape6.Name = "RectangleShape6"
        Me.RectangleShape6.Size = New System.Drawing.Size(37, 23)
        '
        'RectangleShape5
        '
        Me.RectangleShape5.BackColor = System.Drawing.Color.Red
        Me.RectangleShape5.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape5.BorderColor = System.Drawing.Color.White
        Me.RectangleShape5.Location = New System.Drawing.Point(242, 9)
        Me.RectangleShape5.Name = "RectangleShape5"
        Me.RectangleShape5.Size = New System.Drawing.Size(37, 23)
        '
        'RectangleShape4
        '
        Me.RectangleShape4.BackColor = System.Drawing.Color.Lime
        Me.RectangleShape4.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape4.BorderColor = System.Drawing.Color.White
        Me.RectangleShape4.Location = New System.Drawing.Point(91, 9)
        Me.RectangleShape4.Name = "RectangleShape4"
        Me.RectangleShape4.Size = New System.Drawing.Size(37, 23)
        '
        'Label17
        '
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.GreenYellow
        Me.Label17.Location = New System.Drawing.Point(0, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(467, 24)
        Me.Label17.TabIndex = 18
        Me.Label17.Text = " VEHICLE STATUS"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PCompartment
        '
        Me.PCompartment.BackColor = System.Drawing.Color.Silver
        Me.PCompartment.Controls.Add(Me.TGauge20)
        Me.PCompartment.Controls.Add(Me.TGauge19)
        Me.PCompartment.Controls.Add(Me.TGauge18)
        Me.PCompartment.Controls.Add(Me.TGauge17)
        Me.PCompartment.Controls.Add(Me.TGauge16)
        Me.PCompartment.Controls.Add(Me.TGauge15)
        Me.PCompartment.Controls.Add(Me.TGauge14)
        Me.PCompartment.Controls.Add(Me.TGauge13)
        Me.PCompartment.Controls.Add(Me.TGauge12)
        Me.PCompartment.Controls.Add(Me.TGauge11)
        Me.PCompartment.Controls.Add(Me.TGauge10)
        Me.PCompartment.Controls.Add(Me.TGauge9)
        Me.PCompartment.Controls.Add(Me.TGauge8)
        Me.PCompartment.Controls.Add(Me.TGauge7)
        Me.PCompartment.Controls.Add(Me.TGauge6)
        Me.PCompartment.Controls.Add(Me.TGauge5)
        Me.PCompartment.Controls.Add(Me.TGauge4)
        Me.PCompartment.Controls.Add(Me.TGauge3)
        Me.PCompartment.Controls.Add(Me.TGauge2)
        Me.PCompartment.Controls.Add(Me.TGauge1)
        Me.PCompartment.Location = New System.Drawing.Point(117, 108)
        Me.PCompartment.Name = "PCompartment"
        Me.PCompartment.Size = New System.Drawing.Size(315, 50)
        Me.PCompartment.TabIndex = 17
        '
        'TGauge20
        '
        Me.TGauge20.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge20.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge20.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge20.Location = New System.Drawing.Point(285, 0)
        Me.TGauge20.MaxValue = 100.0R
        Me.TGauge20.MinValue = 0.0R
        Me.TGauge20.Name = "TGauge20"
        Me.TGauge20.Progress = 30.0R
        Me.TGauge20.Size = New System.Drawing.Size(15, 50)
        Me.TGauge20.TabIndex = 25
        '
        'TGauge19
        '
        Me.TGauge19.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge19.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge19.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge19.Location = New System.Drawing.Point(270, 0)
        Me.TGauge19.MaxValue = 100.0R
        Me.TGauge19.MinValue = 0.0R
        Me.TGauge19.Name = "TGauge19"
        Me.TGauge19.Progress = 30.0R
        Me.TGauge19.Size = New System.Drawing.Size(15, 50)
        Me.TGauge19.TabIndex = 24
        '
        'TGauge18
        '
        Me.TGauge18.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge18.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge18.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge18.Location = New System.Drawing.Point(255, 0)
        Me.TGauge18.MaxValue = 100.0R
        Me.TGauge18.MinValue = 0.0R
        Me.TGauge18.Name = "TGauge18"
        Me.TGauge18.Progress = 30.0R
        Me.TGauge18.Size = New System.Drawing.Size(15, 50)
        Me.TGauge18.TabIndex = 23
        '
        'TGauge17
        '
        Me.TGauge17.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge17.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge17.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge17.Location = New System.Drawing.Point(240, 0)
        Me.TGauge17.MaxValue = 100.0R
        Me.TGauge17.MinValue = 0.0R
        Me.TGauge17.Name = "TGauge17"
        Me.TGauge17.Progress = 30.0R
        Me.TGauge17.Size = New System.Drawing.Size(15, 50)
        Me.TGauge17.TabIndex = 22
        '
        'TGauge16
        '
        Me.TGauge16.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge16.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge16.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge16.Location = New System.Drawing.Point(225, 0)
        Me.TGauge16.MaxValue = 100.0R
        Me.TGauge16.MinValue = 0.0R
        Me.TGauge16.Name = "TGauge16"
        Me.TGauge16.Progress = 30.0R
        Me.TGauge16.Size = New System.Drawing.Size(15, 50)
        Me.TGauge16.TabIndex = 21
        '
        'TGauge15
        '
        Me.TGauge15.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge15.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge15.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge15.Location = New System.Drawing.Point(210, 0)
        Me.TGauge15.MaxValue = 100.0R
        Me.TGauge15.MinValue = 0.0R
        Me.TGauge15.Name = "TGauge15"
        Me.TGauge15.Progress = 30.0R
        Me.TGauge15.Size = New System.Drawing.Size(15, 50)
        Me.TGauge15.TabIndex = 20
        '
        'TGauge14
        '
        Me.TGauge14.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge14.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge14.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge14.Location = New System.Drawing.Point(195, 0)
        Me.TGauge14.MaxValue = 100.0R
        Me.TGauge14.MinValue = 0.0R
        Me.TGauge14.Name = "TGauge14"
        Me.TGauge14.Progress = 30.0R
        Me.TGauge14.Size = New System.Drawing.Size(15, 50)
        Me.TGauge14.TabIndex = 19
        '
        'TGauge13
        '
        Me.TGauge13.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge13.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge13.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge13.Location = New System.Drawing.Point(180, 0)
        Me.TGauge13.MaxValue = 100.0R
        Me.TGauge13.MinValue = 0.0R
        Me.TGauge13.Name = "TGauge13"
        Me.TGauge13.Progress = 30.0R
        Me.TGauge13.Size = New System.Drawing.Size(15, 50)
        Me.TGauge13.TabIndex = 18
        '
        'TGauge12
        '
        Me.TGauge12.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge12.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge12.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge12.Location = New System.Drawing.Point(165, 0)
        Me.TGauge12.MaxValue = 100.0R
        Me.TGauge12.MinValue = 0.0R
        Me.TGauge12.Name = "TGauge12"
        Me.TGauge12.Progress = 30.0R
        Me.TGauge12.Size = New System.Drawing.Size(15, 50)
        Me.TGauge12.TabIndex = 17
        '
        'TGauge11
        '
        Me.TGauge11.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge11.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge11.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge11.Location = New System.Drawing.Point(150, 0)
        Me.TGauge11.MaxValue = 100.0R
        Me.TGauge11.MinValue = 0.0R
        Me.TGauge11.Name = "TGauge11"
        Me.TGauge11.Progress = 30.0R
        Me.TGauge11.Size = New System.Drawing.Size(15, 50)
        Me.TGauge11.TabIndex = 16
        '
        'TGauge10
        '
        Me.TGauge10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge10.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge10.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge10.Location = New System.Drawing.Point(135, 0)
        Me.TGauge10.MaxValue = 100.0R
        Me.TGauge10.MinValue = 0.0R
        Me.TGauge10.Name = "TGauge10"
        Me.TGauge10.Progress = 30.0R
        Me.TGauge10.Size = New System.Drawing.Size(15, 50)
        Me.TGauge10.TabIndex = 15
        '
        'TGauge9
        '
        Me.TGauge9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge9.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge9.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge9.Location = New System.Drawing.Point(120, 0)
        Me.TGauge9.MaxValue = 100.0R
        Me.TGauge9.MinValue = 0.0R
        Me.TGauge9.Name = "TGauge9"
        Me.TGauge9.Progress = 30.0R
        Me.TGauge9.Size = New System.Drawing.Size(15, 50)
        Me.TGauge9.TabIndex = 14
        '
        'TGauge8
        '
        Me.TGauge8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge8.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge8.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge8.Location = New System.Drawing.Point(105, 0)
        Me.TGauge8.MaxValue = 100.0R
        Me.TGauge8.MinValue = 0.0R
        Me.TGauge8.Name = "TGauge8"
        Me.TGauge8.Progress = 30.0R
        Me.TGauge8.Size = New System.Drawing.Size(15, 50)
        Me.TGauge8.TabIndex = 13
        '
        'TGauge7
        '
        Me.TGauge7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge7.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge7.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge7.Location = New System.Drawing.Point(90, 0)
        Me.TGauge7.MaxValue = 100.0R
        Me.TGauge7.MinValue = 0.0R
        Me.TGauge7.Name = "TGauge7"
        Me.TGauge7.Progress = 30.0R
        Me.TGauge7.Size = New System.Drawing.Size(15, 50)
        Me.TGauge7.TabIndex = 12
        '
        'TGauge6
        '
        Me.TGauge6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge6.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge6.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge6.Location = New System.Drawing.Point(75, 0)
        Me.TGauge6.MaxValue = 10000.0R
        Me.TGauge6.MinValue = 0.0R
        Me.TGauge6.Name = "TGauge6"
        Me.TGauge6.Progress = 30.0R
        Me.TGauge6.Size = New System.Drawing.Size(15, 50)
        Me.TGauge6.TabIndex = 11
        '
        'TGauge5
        '
        Me.TGauge5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge5.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge5.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge5.Location = New System.Drawing.Point(60, 0)
        Me.TGauge5.MaxValue = 10000.0R
        Me.TGauge5.MinValue = 0.0R
        Me.TGauge5.Name = "TGauge5"
        Me.TGauge5.Progress = 30.0R
        Me.TGauge5.Size = New System.Drawing.Size(15, 50)
        Me.TGauge5.TabIndex = 10
        '
        'TGauge4
        '
        Me.TGauge4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge4.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge4.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge4.Location = New System.Drawing.Point(45, 0)
        Me.TGauge4.MaxValue = 10000.0R
        Me.TGauge4.MinValue = 0.0R
        Me.TGauge4.Name = "TGauge4"
        Me.TGauge4.Progress = 30.0R
        Me.TGauge4.Size = New System.Drawing.Size(15, 50)
        Me.TGauge4.TabIndex = 9
        '
        'TGauge3
        '
        Me.TGauge3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge3.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge3.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge3.Location = New System.Drawing.Point(30, 0)
        Me.TGauge3.MaxValue = 100.0R
        Me.TGauge3.MinValue = 0.0R
        Me.TGauge3.Name = "TGauge3"
        Me.TGauge3.Progress = 30.0R
        Me.TGauge3.Size = New System.Drawing.Size(15, 50)
        Me.TGauge3.TabIndex = 8
        '
        'TGauge2
        '
        Me.TGauge2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge2.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge2.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge2.Location = New System.Drawing.Point(15, 0)
        Me.TGauge2.MaxValue = 100.0R
        Me.TGauge2.MinValue = 0.0R
        Me.TGauge2.Name = "TGauge2"
        Me.TGauge2.Progress = 30.0R
        Me.TGauge2.Size = New System.Drawing.Size(15, 50)
        Me.TGauge2.TabIndex = 7
        '
        'TGauge1
        '
        Me.TGauge1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge1.Color = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TGauge1.Dock = System.Windows.Forms.DockStyle.Left
        Me.TGauge1.Location = New System.Drawing.Point(0, 0)
        Me.TGauge1.MaxValue = 10000.0R
        Me.TGauge1.MinValue = 0.0R
        Me.TGauge1.Name = "TGauge1"
        Me.TGauge1.Progress = 30.0R
        Me.TGauge1.Size = New System.Drawing.Size(15, 50)
        Me.TGauge1.TabIndex = 6
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(10, 85)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(436, 140)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'Bay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1348, 733)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "Bay"
        Me.Text = "Bay Detail"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.VLOADINGLOCALMODEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VISLANDBAYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.LoadDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PBay.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Pbatch8.ResumeLayout(False)
        Me.Pbatch8.PerformLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch7.ResumeLayout(False)
        Me.Pbatch7.PerformLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch6.ResumeLayout(False)
        Me.Pbatch6.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch5.ResumeLayout(False)
        Me.Pbatch5.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch4.ResumeLayout(False)
        Me.Pbatch4.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch3.ResumeLayout(False)
        Me.Pbatch3.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch2.ResumeLayout(False)
        Me.Pbatch2.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pbatch1.ResumeLayout(False)
        Me.Pbatch1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.H_panel.ResumeLayout(False)
        Me.H_panel.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.P_Weight.ResumeLayout(False)
        Me.P_Weight.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel16.ResumeLayout(False)
        CType(Me.MeterDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel15.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PCompartment.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape3 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents FPTDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents V_BATCH_STATUSTableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_BATCH_STATUSTableAdapter
    Friend WithEvents VISLANDBAYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_ISLANDBAYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_ISLANDBAYTableAdapter
    Friend WithEvents VLOADINGLOCALMODEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents V_LOADING_LOCALMODETableAdapter As TAS_TOL.FPTDataSetTableAdapters.V_LOADING_LOCALMODETableAdapter
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents LoadDetail As System.Windows.Forms.DataGridView
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents PBay As System.Windows.Forms.Panel
    Friend WithEvents Pbatch8 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B8 As System.Windows.Forms.Label
    Friend WithEvents PRO_B8 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B8 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Pbatch7 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B7 As System.Windows.Forms.Label
    Friend WithEvents PRO_B7 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B7 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Pbatch6 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B6 As System.Windows.Forms.Label
    Friend WithEvents PRO_B6 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B6 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Pbatch5 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B5 As System.Windows.Forms.Label
    Friend WithEvents PRO_B5 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B5 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents Pbatch4 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B4 As System.Windows.Forms.Label
    Friend WithEvents PRO_B4 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B4 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Pbatch3 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B3 As System.Windows.Forms.Label
    Friend WithEvents PRO_B3 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B3 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Pbatch2 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B2 As System.Windows.Forms.Label
    Friend WithEvents PRO_B2 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B2 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Pbatch1 As System.Windows.Forms.Panel
    Friend WithEvents Accum_B1 As System.Windows.Forms.Label
    Friend WithEvents PRO_B1 As System.Windows.Forms.TextBox
    Friend WithEvents Name_B1 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents H_panel As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents M_NAME As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents M_TIME As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents M_GROUP As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents M_DATE As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents MeterDetail As System.Windows.Forms.DataGridView
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer3 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape6 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape5 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape4 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel22 As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents CBBay As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents EDTIMEUSE As System.Windows.Forms.TextBox
    Friend WithEvents DEREMAIN As System.Windows.Forms.TextBox
    Friend WithEvents EDGROSS As System.Windows.Forms.TextBox
    Friend WithEvents EDPRESET As System.Windows.Forms.TextBox
    Friend WithEvents EdCountComp As System.Windows.Forms.TextBox
    Friend WithEvents EDPIN As System.Windows.Forms.TextBox
    Friend WithEvents EDREF As System.Windows.Forms.TextBox
    Friend WithEvents EDLOADVEH As System.Windows.Forms.TextBox
    Friend WithEvents EdLoadID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CSFDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REFERENCE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ORDERNODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CVHDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TIMESTARTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LTIMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRESETSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GROSSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REMAINDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CVm1accum_START As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CVD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CVF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PCompartment As System.Windows.Forms.Panel
    Friend WithEvents TGauge20 As Gauge.TGauge
    Friend WithEvents TGauge19 As Gauge.TGauge
    Friend WithEvents TGauge18 As Gauge.TGauge
    Friend WithEvents TGauge17 As Gauge.TGauge
    Friend WithEvents TGauge16 As Gauge.TGauge
    Friend WithEvents TGauge15 As Gauge.TGauge
    Friend WithEvents TGauge14 As Gauge.TGauge
    Friend WithEvents TGauge13 As Gauge.TGauge
    Friend WithEvents TGauge12 As Gauge.TGauge
    Friend WithEvents TGauge11 As Gauge.TGauge
    Friend WithEvents TGauge10 As Gauge.TGauge
    Friend WithEvents TGauge9 As Gauge.TGauge
    Friend WithEvents TGauge8 As Gauge.TGauge
    Friend WithEvents TGauge7 As Gauge.TGauge
    Friend WithEvents TGauge6 As Gauge.TGauge
    Friend WithEvents TGauge5 As Gauge.TGauge
    Friend WithEvents TGauge4 As Gauge.TGauge
    Friend WithEvents TGauge3 As Gauge.TGauge
    Friend WithEvents TGauge2 As Gauge.TGauge
    Friend WithEvents TGauge1 As Gauge.TGauge
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents P_Weight As System.Windows.Forms.Panel
    Friend WithEvents W_DATETIME As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents weight_Unit As System.Windows.Forms.Label
    Friend WithEvents weight As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents BATCHNUMBERDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCH_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BATCH_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MODENAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASECPMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASEPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASEGDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REMAIN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASETDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASEFDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BASEACCUMGDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents METER_STSTUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
