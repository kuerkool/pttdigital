﻿Imports System.ComponentModel
Imports System.IO
Imports System.Data.SqlClient
Imports System.Threading
Imports Telerik.WinControls.UI

Public Class BayLoading
    Friend WithEvents ThrShipment As BackgroundWorker
    Dim CallBlackShipment As New MethodInvoker(AddressOf Me.DataBlackShipment)
    Dim TasDataset As DataSet
    Dim TasAdapter As New SqlDataAdapter

    Private Sub FReShipment()
        ThrShipment = New BackgroundWorker
        ThrShipment.WorkerSupportsCancellation = True
        AddHandler ThrShipment.DoWork, AddressOf RefreshShipment
        ThrShipment.RunWorkerAsync()
    End Sub
    Private Sub RefreshShipment()
        Dim conn As SqlConnection
        Dim cmd As New SqlCommand

        While Not ThrShipment.CancellationPending
            conn = New SqlConnection(My.Settings.FPTConnectionString)
            conn.Open()
            cmd.Connection = conn
            cmd.CommandText = "Select convert(Varchar(8),Max(LOAD_DATE),112)+  Right('000'+cast(Max(LOAD_DID) as varchar(3)),3) as LOAD_DID, " &
                " Max(Customer_name) as Customer_name,max(LOAD_VEHICLE) as LOAD_VEHICLE ,max(LOAD_DRIVER) as LOAD_DRIVER," &
                " Max(PRODUCT_CODE) as PRODUCT_CODE ,max(LOAD_PRESET) as LOAD_PRESET, max(Raw_Weight_in) as Raw_Weight_in ,max(Weightin_time) as Weightin_time, " &
                "  max(Raw_Weight_out) as Raw_Weight_out ,max(Weightout_time) as Weightout_time, " &
                " max(LOAD_STATUS) as LOAD_STATUS,CAST( MAX(lc_status)-1 as bit) as lc_status,max(LC_BAY) as LC_BAY  " &
                " from V_LOADINGNOTE where (LOAD_STATUS=2)  or (LOAD_STATUS=3 and DATEADD(MINUTE,1, cast(convert(varchar(10), (Weightout_Date),121)+' '+convert(varchar(8), (Weightout_time),121) as Datetime))> GETDATE())" &
                " Group by LOAD_ID "

            TasAdapter.SelectCommand = cmd
            TasDataset = Nothing
            TasDataset = New DataSet
            'If ds.Tables.Count > 0 Then
            '    ds.Tables("V_LOADINGNOTE").Rows.Clear()
            'End If
            Try
                TasDataset.Tables("V_LOADINGNOTE").Rows.Clear()
            Catch ex As Exception

            End Try
            TasAdapter.Fill(TasDataset, "V_LOADINGNOTE")
            Me.BeginInvoke(CallBlackShipment)
            Thread.Sleep(5000)
        End While

    End Sub

    Private Sub DataBlackShipment()


        Try

            For i = 1 To 15


                Dim drarray() As DataRow
                Dim Dt As New DataTable
                Dim Ds As New DataSet
                Try

                    Dt = TasDataset.Tables("V_LOADINGNOTE").Clone
                    drarray = TasDataset.Tables("V_LOADINGNOTE").Select("LC_BAY=" & i)

                Catch ex As Exception

                End Try

                If drarray.Length > 0 Then

                    For Each row As DataRow In drarray
                        Dt.ImportRow(row)
                    Next

                End If
                Dt.TableName = "V_LOADINGNOTE" & i.ToString
                Ds.Tables.Add(Dt)
                Select Case i
                    Case 1
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P1.Visible = True
                        Else
                            P1.Visible = False
                        End If
                        Try

                            BindingSource1.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource1.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 2
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P2.Visible = True
                        Else
                            P2.Visible = False
                        End If
                        Try
                            BindingSource2.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource2.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 3
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P3.Visible = True
                        Else
                            P3.Visible = False
                        End If
                        Try
                            BindingSource3.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource3.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 4
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P4.Visible = True
                        Else
                            P4.Visible = False
                        End If
                        Try
                            BindingSource4.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource4.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 5
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P5.Visible = True
                        Else
                            P5.Visible = False
                        End If
                        Try
                            BindingSource5.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource5.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 6
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P6.Visible = True
                        Else
                            P6.Visible = False
                        End If
                        Try
                            BindingSource6.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource6.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 7
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P7.Visible = True
                        Else
                            P7.Visible = False
                        End If
                        Try
                            BindingSource7.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource7.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 8
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P8.Visible = True
                        Else
                            P8.Visible = False
                        End If
                        Try
                            BindingSource8.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource8.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 9
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P9.Visible = True
                        Else
                            P9.Visible = False
                        End If
                        Try
                            BindingSource9.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource9.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 10
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P10.Visible = True
                        Else
                            P10.Visible = False
                        End If
                        Try
                            BindingSource10.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource10.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 11
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P11.Visible = True
                        Else
                            P11.Visible = False
                        End If
                        Try
                            BindingSource11.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource11.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 12
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P12.Visible = True
                        Else
                            P12.Visible = False
                        End If
                        Try
                            BindingSource12.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource12.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 13
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P13.Visible = True
                        Else
                            P13.Visible = False
                        End If
                        Try
                            BindingSource13.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource13.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 14
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P14.Visible = True
                        Else
                            P14.Visible = False
                        End If
                        Try
                            BindingSource14.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource14.DataMember = "V_LOADINGNOTE" & i.ToString


                    Case 15
                        If Ds.Tables("V_LOADINGNOTE" & i.ToString).Rows.Count > 0 Then
                            P15.Visible = True
                        Else
                            P15.Visible = False
                        End If
                        Try
                            BindingSource15.DataSource = Ds

                        Catch ex As Exception

                        End Try
                        BindingSource15.DataMember = "V_LOADINGNOTE" & i.ToString



                End Select
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Sub MMI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.LOADBAY' table. You can move, or remove it, as needed.
        '  Me.LOADBAYTableAdapter.Fill(Me.FPTDataSet.LOADBAY)
    End Sub


    Private Sub BayOverview_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ThrShipment.CancelAsync()
    End Sub

    Private Sub BayOverview_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Try
            ThrShipment.CancelAsync()
        Catch ex As Exception

        End Try

        FReShipment()
    End Sub
End Class
