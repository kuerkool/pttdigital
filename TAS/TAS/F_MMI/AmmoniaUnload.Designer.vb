﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AmmoniaUnload
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AmmoniaUnload))
        Me.H_panel = New System.Windows.Forms.Panel()
        Me.GroupEMERGENSY = New System.Windows.Forms.GroupBox()
        Me.BTESD = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.P_Weight = New System.Windows.Forms.Panel()
        Me.W_DATETIME = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.weight_Unit = New System.Windows.Forms.Label()
        Me.weight = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.M_NAME = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.M_TIME = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.M_GROUP = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.M_DATE = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LStatus2 = New System.Windows.Forms.Label()
        Me.LMode2 = New System.Windows.Forms.Label()
        Me.LStatus3 = New System.Windows.Forms.Label()
        Me.LMode3 = New System.Windows.Forms.Label()
        Me.XVPB3 = New System.Windows.Forms.PictureBox()
        Me.XVPG3 = New System.Windows.Forms.PictureBox()
        Me.XVPR3 = New System.Windows.Forms.PictureBox()
        Me.XVPL3 = New System.Windows.Forms.PictureBox()
        Me.XVPB2 = New System.Windows.Forms.PictureBox()
        Me.XVPG2 = New System.Windows.Forms.PictureBox()
        Me.XVPR2 = New System.Windows.Forms.PictureBox()
        Me.XVPL2 = New System.Windows.Forms.PictureBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.ShapeContainer5 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape7 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape6 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape5 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape4 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape3 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.BtPstop = New System.Windows.Forms.Button()
        Me.btPstart = New System.Windows.Forms.Button()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.ShapeContainer4 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape13 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.ShapeContainer3 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LEDPOWER = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LEDLOADING = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LEDESD = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LEDGND = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.EDSTOP = New System.Windows.Forms.TextBox()
        Me.EDTSTART = New System.Windows.Forms.TextBox()
        Me.EDWOUT = New System.Windows.Forms.TextBox()
        Me.EDWIN = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.EDCUS = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.EDVEH = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.EDREF = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.EVEH = New System.Windows.Forms.PictureBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BtReset = New System.Windows.Forms.Button()
        Me.BtRemove = New System.Windows.Forms.Button()
        Me.BtGrant = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LEDDEP = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LEDPERM = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LEDESD2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LEDOP = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.H_panel.SuspendLayout()
        Me.GroupEMERGENSY.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.P_Weight.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.XVPB3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPG3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPR3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPL3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPB2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPG2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPR2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPL2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.EVEH, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'H_panel
        '
        Me.H_panel.Controls.Add(Me.GroupEMERGENSY)
        Me.H_panel.Controls.Add(Me.TableLayoutPanel2)
        Me.H_panel.Controls.Add(Me.TableLayoutPanel3)
        Me.H_panel.Controls.Add(Me.Label27)
        Me.H_panel.Controls.Add(Me.PictureBox8)
        Me.H_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.H_panel.Location = New System.Drawing.Point(4, 4)
        Me.H_panel.Name = "H_panel"
        Me.H_panel.Size = New System.Drawing.Size(1645, 130)
        Me.H_panel.TabIndex = 18
        '
        'GroupEMERGENSY
        '
        Me.GroupEMERGENSY.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupEMERGENSY.Controls.Add(Me.BTESD)
        Me.GroupEMERGENSY.ForeColor = System.Drawing.Color.Yellow
        Me.GroupEMERGENSY.Location = New System.Drawing.Point(1270, 5)
        Me.GroupEMERGENSY.Name = "GroupEMERGENSY"
        Me.GroupEMERGENSY.Size = New System.Drawing.Size(122, 123)
        Me.GroupEMERGENSY.TabIndex = 31
        Me.GroupEMERGENSY.TabStop = False
        Me.GroupEMERGENSY.Text = "EMERGENSY STOP"
        '
        'BTESD
        '
        Me.BTESD.BackColor = System.Drawing.Color.Lime
        Me.BTESD.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BTESD.ForeColor = System.Drawing.Color.Fuchsia
        Me.BTESD.Location = New System.Drawing.Point(8, 19)
        Me.BTESD.Name = "BTESD"
        Me.BTESD.Size = New System.Drawing.Size(108, 96)
        Me.BTESD.TabIndex = 31
        Me.BTESD.Text = "ESD"
        Me.BTESD.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.P_Weight, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(590, 57)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(464, 70)
        Me.TableLayoutPanel2.TabIndex = 29
        '
        'P_Weight
        '
        Me.P_Weight.Controls.Add(Me.W_DATETIME)
        Me.P_Weight.Controls.Add(Me.Label20)
        Me.P_Weight.Controls.Add(Me.weight_Unit)
        Me.P_Weight.Controls.Add(Me.weight)
        Me.P_Weight.Controls.Add(Me.Label23)
        Me.P_Weight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.P_Weight.Location = New System.Drawing.Point(4, 4)
        Me.P_Weight.Name = "P_Weight"
        Me.P_Weight.Size = New System.Drawing.Size(456, 62)
        Me.P_Weight.TabIndex = 27
        '
        'W_DATETIME
        '
        Me.W_DATETIME.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.W_DATETIME.AutoSize = True
        Me.W_DATETIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.W_DATETIME.ForeColor = System.Drawing.Color.White
        Me.W_DATETIME.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.W_DATETIME.Location = New System.Drawing.Point(63, 48)
        Me.W_DATETIME.Name = "W_DATETIME"
        Me.W_DATETIME.Size = New System.Drawing.Size(114, 13)
        Me.W_DATETIME.TabIndex = 32
        Me.W_DATETIME.Text = "dd/MM/yyyy hh:mm:ss"
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(10, 48)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(57, 13)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Updated : "
        '
        'weight_Unit
        '
        Me.weight_Unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weight_Unit.AutoSize = True
        Me.weight_Unit.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight_Unit.ForeColor = System.Drawing.Color.Lime
        Me.weight_Unit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight_Unit.Location = New System.Drawing.Point(376, 2)
        Me.weight_Unit.Name = "weight_Unit"
        Me.weight_Unit.Size = New System.Drawing.Size(77, 52)
        Me.weight_Unit.TabIndex = 31
        Me.weight_Unit.Text = "KG"
        '
        'weight
        '
        Me.weight.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight.ForeColor = System.Drawing.Color.Lime
        Me.weight.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight.Location = New System.Drawing.Point(167, 2)
        Me.weight.Name = "weight"
        Me.weight.Size = New System.Drawing.Size(210, 52)
        Me.weight.TabIndex = 30
        Me.weight.Text = "0"
        Me.weight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(3, 2)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(173, 52)
        Me.Label23.TabIndex = 29
        Me.Label23.Text = "WEIGHT"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel7, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel8, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel9, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel10, 0, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(1398, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(247, 129)
        Me.TableLayoutPanel3.TabIndex = 28
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.Controls.Add(Me.Label15)
        Me.Panel7.Controls.Add(Me.M_NAME)
        Me.Panel7.Location = New System.Drawing.Point(4, 68)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(239, 25)
        Me.Panel7.TabIndex = 42
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(17, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(63, 20)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "NAME :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_NAME
        '
        Me.M_NAME.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_NAME.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_NAME.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_NAME.Location = New System.Drawing.Point(76, 1)
        Me.M_NAME.Name = "M_NAME"
        Me.M_NAME.Size = New System.Drawing.Size(159, 24)
        Me.M_NAME.TabIndex = 36
        Me.M_NAME.Text = "NONE"
        Me.M_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.Controls.Add(Me.Label21)
        Me.Panel8.Controls.Add(Me.M_TIME)
        Me.Panel8.Location = New System.Drawing.Point(4, 4)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(239, 25)
        Me.Panel8.TabIndex = 41
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(21, 4)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(59, 20)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "TIME  :"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_TIME
        '
        Me.M_TIME.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_TIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_TIME.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_TIME.Location = New System.Drawing.Point(76, 1)
        Me.M_TIME.Name = "M_TIME"
        Me.M_TIME.Size = New System.Drawing.Size(159, 26)
        Me.M_TIME.TabIndex = 36
        Me.M_TIME.Text = "HH:MM:SS"
        Me.M_TIME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel9
        '
        Me.Panel9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel9.Controls.Add(Me.Label22)
        Me.Panel9.Controls.Add(Me.M_GROUP)
        Me.Panel9.Location = New System.Drawing.Point(4, 100)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(239, 25)
        Me.Panel9.TabIndex = 43
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(4, 4)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(76, 20)
        Me.Label22.TabIndex = 37
        Me.Label22.Text = "GROUP :"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_GROUP
        '
        Me.M_GROUP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_GROUP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_GROUP.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_GROUP.Location = New System.Drawing.Point(76, 1)
        Me.M_GROUP.Name = "M_GROUP"
        Me.M_GROUP.Size = New System.Drawing.Size(159, 25)
        Me.M_GROUP.TabIndex = 36
        Me.M_GROUP.Text = "NONE"
        Me.M_GROUP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.Controls.Add(Me.Label24)
        Me.Panel10.Controls.Add(Me.M_DATE)
        Me.Panel10.Location = New System.Drawing.Point(4, 36)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(239, 25)
        Me.Panel10.TabIndex = 40
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(20, 4)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(60, 20)
        Me.Label24.TabIndex = 37
        Me.Label24.Text = "DATE :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_DATE
        '
        Me.M_DATE.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_DATE.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M_DATE.ForeColor = System.Drawing.Color.DodgerBlue
        Me.M_DATE.Location = New System.Drawing.Point(76, 1)
        Me.M_DATE.Name = "M_DATE"
        Me.M_DATE.Size = New System.Drawing.Size(159, 26)
        Me.M_DATE.TabIndex = 36
        Me.M_DATE.Text = "dd/mm/yyyy"
        Me.M_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(645, 16)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(371, 37)
        Me.Label27.TabIndex = 25
        Me.Label27.Text = "AMMONIA UNLOADING"
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.InitialImage = Nothing
        Me.PictureBox8.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(247, 129)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 15
        Me.PictureBox8.TabStop = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.H_panel, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 136.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1653, 874)
        Me.TableLayoutPanel1.TabIndex = 31
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(4, 141)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1645, 729)
        Me.Panel2.TabIndex = 21
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.Controls.Add(Me.LStatus2)
        Me.Panel1.Controls.Add(Me.LMode2)
        Me.Panel1.Controls.Add(Me.LStatus3)
        Me.Panel1.Controls.Add(Me.LMode3)
        Me.Panel1.Controls.Add(Me.XVPB3)
        Me.Panel1.Controls.Add(Me.XVPG3)
        Me.Panel1.Controls.Add(Me.XVPR3)
        Me.Panel1.Controls.Add(Me.XVPL3)
        Me.Panel1.Controls.Add(Me.XVPB2)
        Me.Panel1.Controls.Add(Me.XVPG2)
        Me.Panel1.Controls.Add(Me.XVPR2)
        Me.Panel1.Controls.Add(Me.XVPL2)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.PictureBox15)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.ShapeContainer5)
        Me.Panel1.Location = New System.Drawing.Point(142, 267)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(885, 454)
        Me.Panel1.TabIndex = 29
        '
        'LStatus2
        '
        Me.LStatus2.AutoSize = True
        Me.LStatus2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LStatus2.ForeColor = System.Drawing.Color.Lime
        Me.LStatus2.Location = New System.Drawing.Point(751, 230)
        Me.LStatus2.Name = "LStatus2"
        Me.LStatus2.Size = New System.Drawing.Size(72, 16)
        Me.LStatus2.TabIndex = 42
        Me.LStatus2.Text = "Fully Open"
        '
        'LMode2
        '
        Me.LMode2.AutoSize = True
        Me.LMode2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LMode2.ForeColor = System.Drawing.Color.Aqua
        Me.LMode2.Location = New System.Drawing.Point(753, 158)
        Me.LMode2.Name = "LMode2"
        Me.LMode2.Size = New System.Drawing.Size(73, 16)
        Me.LMode2.TabIndex = 41
        Me.LMode2.Text = "Auto Mode"
        '
        'LStatus3
        '
        Me.LStatus3.AutoSize = True
        Me.LStatus3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LStatus3.ForeColor = System.Drawing.Color.Lime
        Me.LStatus3.Location = New System.Drawing.Point(751, 116)
        Me.LStatus3.Name = "LStatus3"
        Me.LStatus3.Size = New System.Drawing.Size(72, 16)
        Me.LStatus3.TabIndex = 40
        Me.LStatus3.Text = "Fully Open"
        '
        'LMode3
        '
        Me.LMode3.AutoSize = True
        Me.LMode3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LMode3.ForeColor = System.Drawing.Color.Aqua
        Me.LMode3.Location = New System.Drawing.Point(753, 45)
        Me.LMode3.Name = "LMode3"
        Me.LMode3.Size = New System.Drawing.Size(73, 16)
        Me.LMode3.TabIndex = 39
        Me.LMode3.Text = "Auto Mode"
        '
        'XVPB3
        '
        Me.XVPB3.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Bul1
        Me.XVPB3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPB3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPB3.Location = New System.Drawing.Point(763, 64)
        Me.XVPB3.Name = "XVPB3"
        Me.XVPB3.Size = New System.Drawing.Size(50, 49)
        Me.XVPB3.TabIndex = 38
        Me.XVPB3.TabStop = False
        '
        'XVPG3
        '
        Me.XVPG3.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.gre
        Me.XVPG3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPG3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPG3.Location = New System.Drawing.Point(763, 64)
        Me.XVPG3.Name = "XVPG3"
        Me.XVPG3.Size = New System.Drawing.Size(50, 49)
        Me.XVPG3.TabIndex = 37
        Me.XVPG3.TabStop = False
        '
        'XVPR3
        '
        Me.XVPR3.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.red
        Me.XVPR3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPR3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPR3.Location = New System.Drawing.Point(763, 64)
        Me.XVPR3.Name = "XVPR3"
        Me.XVPR3.Size = New System.Drawing.Size(50, 49)
        Me.XVPR3.TabIndex = 36
        Me.XVPR3.TabStop = False
        '
        'XVPL3
        '
        Me.XVPL3.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Yen
        Me.XVPL3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPL3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPL3.Location = New System.Drawing.Point(763, 64)
        Me.XVPL3.Name = "XVPL3"
        Me.XVPL3.Size = New System.Drawing.Size(50, 49)
        Me.XVPL3.TabIndex = 35
        Me.XVPL3.TabStop = False
        '
        'XVPB2
        '
        Me.XVPB2.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Bul1
        Me.XVPB2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPB2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPB2.Location = New System.Drawing.Point(763, 177)
        Me.XVPB2.Name = "XVPB2"
        Me.XVPB2.Size = New System.Drawing.Size(50, 49)
        Me.XVPB2.TabIndex = 34
        Me.XVPB2.TabStop = False
        '
        'XVPG2
        '
        Me.XVPG2.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.gre
        Me.XVPG2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPG2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPG2.Location = New System.Drawing.Point(763, 177)
        Me.XVPG2.Name = "XVPG2"
        Me.XVPG2.Size = New System.Drawing.Size(50, 49)
        Me.XVPG2.TabIndex = 33
        Me.XVPG2.TabStop = False
        '
        'XVPR2
        '
        Me.XVPR2.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.red
        Me.XVPR2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPR2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPR2.Location = New System.Drawing.Point(763, 177)
        Me.XVPR2.Name = "XVPR2"
        Me.XVPR2.Size = New System.Drawing.Size(50, 49)
        Me.XVPR2.TabIndex = 32
        Me.XVPR2.TabStop = False
        '
        'XVPL2
        '
        Me.XVPL2.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Yen
        Me.XVPL2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPL2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XVPL2.Location = New System.Drawing.Point(763, 177)
        Me.XVPL2.Name = "XVPL2"
        Me.XVPL2.Size = New System.Drawing.Size(50, 49)
        Me.XVPL2.TabIndex = 31
        Me.XVPL2.TabStop = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Aqua
        Me.Label30.Location = New System.Drawing.Point(34, 46)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(142, 20)
        Me.Label30.TabIndex = 27
        Me.Label30.Text = "AMMONIA TANK"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.DimGray
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.Aqua
        Me.Label29.Location = New System.Drawing.Point(550, 87)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(134, 20)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "VAPER SYSTEM"
        '
        'PictureBox15
        '
        Me.PictureBox15.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Tank1
        Me.PictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox15.Location = New System.Drawing.Point(40, 70)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(463, 313)
        Me.PictureBox15.TabIndex = 30
        Me.PictureBox15.TabStop = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.Yellow
        Me.Label26.Location = New System.Drawing.Point(757, 32)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(66, 13)
        Me.Label26.TabIndex = 27
        Me.Label26.Text = "XV-M2014-3"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.ForeColor = System.Drawing.Color.Yellow
        Me.Label28.Location = New System.Drawing.Point(757, 145)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(66, 13)
        Me.Label28.TabIndex = 28
        Me.Label28.Text = "XV-M2014-2"
        '
        'ShapeContainer5
        '
        Me.ShapeContainer5.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer5.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer5.Name = "ShapeContainer5"
        Me.ShapeContainer5.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape7, Me.LineShape6, Me.LineShape5, Me.LineShape4, Me.LineShape3, Me.LineShape2, Me.LineShape1})
        Me.ShapeContainer5.Size = New System.Drawing.Size(885, 454)
        Me.ShapeContainer5.TabIndex = 0
        Me.ShapeContainer5.TabStop = False
        '
        'LineShape7
        '
        Me.LineShape7.BorderColor = System.Drawing.Color.Lime
        Me.LineShape7.Name = "LineShape7"
        Me.LineShape7.X1 = 181
        Me.LineShape7.X2 = 181
        Me.LineShape7.Y1 = 47
        Me.LineShape7.Y2 = 72
        '
        'LineShape6
        '
        Me.LineShape6.BorderColor = System.Drawing.Color.Lime
        Me.LineShape6.Name = "LineShape6"
        Me.LineShape6.X1 = 181
        Me.LineShape6.X2 = 513
        Me.LineShape6.Y1 = 47
        Me.LineShape6.Y2 = 47
        '
        'LineShape5
        '
        Me.LineShape5.BorderColor = System.Drawing.Color.Lime
        Me.LineShape5.Name = "LineShape5"
        Me.LineShape5.X1 = 513
        Me.LineShape5.X2 = 513
        Me.LineShape5.Y1 = 47
        Me.LineShape5.Y2 = 215
        '
        'LineShape4
        '
        Me.LineShape4.BorderColor = System.Drawing.Color.Fuchsia
        Me.LineShape4.Name = "LineShape4"
        Me.LineShape4.X1 = 606
        Me.LineShape4.X2 = 860
        Me.LineShape4.Y1 = 102
        Me.LineShape4.Y2 = 102
        '
        'LineShape3
        '
        Me.LineShape3.BorderColor = System.Drawing.Color.Fuchsia
        Me.LineShape3.Name = "LineShape3"
        Me.LineShape3.X1 = 860
        Me.LineShape3.X2 = 860
        Me.LineShape3.Y1 = 202
        Me.LineShape3.Y2 = 102
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.Color.Fuchsia
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 861
        Me.LineShape2.X2 = 886
        Me.LineShape2.Y1 = 202
        Me.LineShape2.Y2 = 202
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.Color.Lime
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 513
        Me.LineShape1.X2 = 887
        Me.LineShape1.Y1 = 215
        Me.LineShape1.Y2 = 215
        '
        'Panel4
        '
        Me.Panel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel4.Controls.Add(Me.GroupBox5)
        Me.Panel4.Controls.Add(Me.PictureBox9)
        Me.Panel4.Controls.Add(Me.PictureBox11)
        Me.Panel4.Controls.Add(Me.PictureBox12)
        Me.Panel4.Controls.Add(Me.Panel3)
        Me.Panel4.Controls.Add(Me.GroupBox4)
        Me.Panel4.Controls.Add(Me.GroupBox3)
        Me.Panel4.Controls.Add(Me.EVEH)
        Me.Panel4.Location = New System.Drawing.Point(1026, 263)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(482, 436)
        Me.Panel4.TabIndex = 21
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.BtPstop)
        Me.GroupBox5.Controls.Add(Me.btPstart)
        Me.GroupBox5.ForeColor = System.Drawing.Color.Yellow
        Me.GroupBox5.Location = New System.Drawing.Point(309, 4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(159, 74)
        Me.GroupBox5.TabIndex = 28
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "POWER PUMP"
        '
        'BtPstop
        '
        Me.BtPstop.BackColor = System.Drawing.Color.Red
        Me.BtPstop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtPstop.ForeColor = System.Drawing.Color.Black
        Me.BtPstop.Location = New System.Drawing.Point(82, 17)
        Me.BtPstop.Name = "BtPstop"
        Me.BtPstop.Size = New System.Drawing.Size(70, 51)
        Me.BtPstop.TabIndex = 33
        Me.BtPstop.Text = "STOP"
        Me.BtPstop.UseVisualStyleBackColor = False
        '
        'btPstart
        '
        Me.btPstart.BackColor = System.Drawing.SystemColors.Control
        Me.btPstart.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btPstart.ForeColor = System.Drawing.Color.Black
        Me.btPstart.Location = New System.Drawing.Point(6, 17)
        Me.btPstart.Name = "btPstart"
        Me.btPstart.Size = New System.Drawing.Size(70, 51)
        Me.btPstart.TabIndex = 32
        Me.btPstart.Text = "START"
        Me.btPstart.UseVisualStyleBackColor = False
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.Black
        Me.PictureBox9.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox9.ErrorImage = Nothing
        Me.PictureBox9.Location = New System.Drawing.Point(42, 216)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox9.TabIndex = 27
        Me.PictureBox9.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Black
        Me.PictureBox11.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox11.ErrorImage = Nothing
        Me.PictureBox11.Location = New System.Drawing.Point(431, 216)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox11.TabIndex = 26
        Me.PictureBox11.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Black
        Me.PictureBox12.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Line
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox12.ErrorImage = Nothing
        Me.PictureBox12.Location = New System.Drawing.Point(244, 216)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(12, 47)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox12.TabIndex = 25
        Me.PictureBox12.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.DimGray
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.ShapeContainer4)
        Me.Panel3.Location = New System.Drawing.Point(4, 247)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(467, 18)
        Me.Panel3.TabIndex = 24
        '
        'ShapeContainer4
        '
        Me.ShapeContainer4.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer4.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer4.Name = "ShapeContainer4"
        Me.ShapeContainer4.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape13})
        Me.ShapeContainer4.Size = New System.Drawing.Size(465, 16)
        Me.ShapeContainer4.TabIndex = 0
        Me.ShapeContainer4.TabStop = False
        '
        'RectangleShape13
        '
        Me.RectangleShape13.BackColor = System.Drawing.Color.Gray
        Me.RectangleShape13.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape13.BorderColor = System.Drawing.Color.White
        Me.RectangleShape13.Location = New System.Drawing.Point(0, 0)
        Me.RectangleShape13.Name = "RectangleShape6"
        Me.RectangleShape13.Size = New System.Drawing.Size(644, 15)
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.Label25)
        Me.GroupBox4.Controls.Add(Me.ShapeContainer3)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.Yellow
        Me.GroupBox4.Location = New System.Drawing.Point(14, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(282, 74)
        Me.GroupBox4.TabIndex = 23
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = " STATUS"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Aqua
        Me.Label17.Location = New System.Drawing.Point(217, 51)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(59, 16)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "POWER"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Aqua
        Me.Label18.Location = New System.Drawing.Point(19, 51)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(38, 16)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "GND"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Aqua
        Me.Label19.Location = New System.Drawing.Point(144, 51)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(67, 16)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "LOADING"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Aqua
        Me.Label25.Location = New System.Drawing.Point(89, 51)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 16)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "ESD"
        '
        'ShapeContainer3
        '
        Me.ShapeContainer3.Location = New System.Drawing.Point(3, 18)
        Me.ShapeContainer3.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer3.Name = "ShapeContainer3"
        Me.ShapeContainer3.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LEDPOWER, Me.LEDLOADING, Me.LEDESD, Me.LEDGND})
        Me.ShapeContainer3.Size = New System.Drawing.Size(276, 53)
        Me.ShapeContainer3.TabIndex = 4
        Me.ShapeContainer3.TabStop = False
        '
        'LEDPOWER
        '
        Me.LEDPOWER.BackColor = System.Drawing.Color.Red
        Me.LEDPOWER.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDPOWER.BorderColor = System.Drawing.Color.White
        Me.LEDPOWER.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEDPOWER.Location = New System.Drawing.Point(224, 3)
        Me.LEDPOWER.Name = "LEDPOWER"
        Me.LEDPOWER.Size = New System.Drawing.Size(37, 23)
        '
        'LEDLOADING
        '
        Me.LEDLOADING.BackColor = System.Drawing.Color.Red
        Me.LEDLOADING.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDLOADING.BorderColor = System.Drawing.Color.White
        Me.LEDLOADING.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEDLOADING.Location = New System.Drawing.Point(155, 3)
        Me.LEDLOADING.Name = "LEDLOADING"
        Me.LEDLOADING.Size = New System.Drawing.Size(37, 23)
        '
        'LEDESD
        '
        Me.LEDESD.BackColor = System.Drawing.Color.Red
        Me.LEDESD.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDESD.BorderColor = System.Drawing.Color.White
        Me.LEDESD.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEDESD.Location = New System.Drawing.Point(86, 3)
        Me.LEDESD.Name = "LEDESD"
        Me.LEDESD.Size = New System.Drawing.Size(37, 23)
        '
        'LEDGND
        '
        Me.LEDGND.BackColor = System.Drawing.Color.Gray
        Me.LEDGND.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDGND.BorderColor = System.Drawing.Color.White
        Me.LEDGND.Location = New System.Drawing.Point(17, 3)
        Me.LEDGND.Name = "LEDGND"
        Me.LEDGND.Size = New System.Drawing.Size(37, 23)
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.EDSTOP)
        Me.GroupBox3.Controls.Add(Me.EDTSTART)
        Me.GroupBox3.Controls.Add(Me.EDWOUT)
        Me.GroupBox3.Controls.Add(Me.EDWIN)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.EDCUS)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.EDVEH)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.EDREF)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Yellow
        Me.GroupBox3.Location = New System.Drawing.Point(29, 271)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(412, 139)
        Me.GroupBox3.TabIndex = 22
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "TRUCK DETAIL"
        '
        'EDSTOP
        '
        Me.EDSTOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDSTOP.Location = New System.Drawing.Point(298, 109)
        Me.EDSTOP.Name = "EDSTOP"
        Me.EDSTOP.Size = New System.Drawing.Size(100, 22)
        Me.EDSTOP.TabIndex = 4
        Me.EDSTOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EDTSTART
        '
        Me.EDTSTART.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDTSTART.Location = New System.Drawing.Point(102, 109)
        Me.EDTSTART.Name = "EDTSTART"
        Me.EDTSTART.Size = New System.Drawing.Size(100, 22)
        Me.EDTSTART.TabIndex = 4
        Me.EDTSTART.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EDWOUT
        '
        Me.EDWOUT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDWOUT.Location = New System.Drawing.Point(298, 81)
        Me.EDWOUT.Name = "EDWOUT"
        Me.EDWOUT.Size = New System.Drawing.Size(100, 22)
        Me.EDWOUT.TabIndex = 4
        Me.EDWOUT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EDWIN
        '
        Me.EDWIN.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDWIN.Location = New System.Drawing.Point(102, 81)
        Me.EDWIN.Name = "EDWIN"
        Me.EDWIN.Size = New System.Drawing.Size(100, 22)
        Me.EDWIN.TabIndex = 4
        Me.EDWIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Aqua
        Me.Label16.Location = New System.Drawing.Point(226, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 16)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Time out :"
        '
        'EDCUS
        '
        Me.EDCUS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDCUS.Location = New System.Drawing.Point(102, 53)
        Me.EDCUS.Name = "EDCUS"
        Me.EDCUS.Size = New System.Drawing.Size(296, 22)
        Me.EDCUS.TabIndex = 4
        Me.EDCUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Aqua
        Me.Label13.Location = New System.Drawing.Point(215, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(77, 16)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Weight out :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Aqua
        Me.Label14.Location = New System.Drawing.Point(38, 112)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Time in :"
        '
        'EDVEH
        '
        Me.EDVEH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDVEH.Location = New System.Drawing.Point(298, 25)
        Me.EDVEH.Name = "EDVEH"
        Me.EDVEH.Size = New System.Drawing.Size(100, 22)
        Me.EDVEH.TabIndex = 4
        Me.EDVEH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Aqua
        Me.Label12.Location = New System.Drawing.Point(27, 83)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 16)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Weight in :"
        '
        'EDREF
        '
        Me.EDREF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.EDREF.Location = New System.Drawing.Point(102, 25)
        Me.EDREF.Name = "EDREF"
        Me.EDREF.Size = New System.Drawing.Size(100, 22)
        Me.EDREF.TabIndex = 4
        Me.EDREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Aqua
        Me.Label11.Location = New System.Drawing.Point(22, 56)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Customer. :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Aqua
        Me.Label9.Location = New System.Drawing.Point(207, 28)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Vehicle NO. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Aqua
        Me.Label10.Location = New System.Drawing.Point(27, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Load No. :"
        '
        'EVEH
        '
        Me.EVEH.ErrorImage = Nothing
        Me.EVEH.Image = Global.TAS_TOL.My.Resources.Resources.Truck2
        Me.EVEH.Location = New System.Drawing.Point(4, 84)
        Me.EVEH.Name = "EVEH"
        Me.EVEH.Size = New System.Drawing.Size(467, 171)
        Me.EVEH.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.EVEH.TabIndex = 21
        Me.EVEH.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox2.Controls.Add(Me.BtReset)
        Me.GroupBox2.Controls.Add(Me.BtRemove)
        Me.GroupBox2.Controls.Add(Me.BtGrant)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.ShapeContainer1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Yellow
        Me.GroupBox2.Location = New System.Drawing.Point(621, 17)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(401, 220)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "UNLOADING STATUS AND CONTROL "
        '
        'BtReset
        '
        Me.BtReset.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtReset.ForeColor = System.Drawing.Color.Black
        Me.BtReset.Location = New System.Drawing.Point(22, 178)
        Me.BtReset.Name = "BtReset"
        Me.BtReset.Size = New System.Drawing.Size(116, 36)
        Me.BtReset.TabIndex = 0
        Me.BtReset.Text = "&Reset"
        Me.BtReset.UseVisualStyleBackColor = True
        '
        'BtRemove
        '
        Me.BtRemove.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtRemove.ForeColor = System.Drawing.Color.Black
        Me.BtRemove.Location = New System.Drawing.Point(266, 178)
        Me.BtRemove.Name = "BtRemove"
        Me.BtRemove.Size = New System.Drawing.Size(116, 36)
        Me.BtRemove.TabIndex = 1
        Me.BtRemove.Text = "&Remove"
        Me.BtRemove.UseVisualStyleBackColor = True
        '
        'BtGrant
        '
        Me.BtGrant.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BtGrant.ForeColor = System.Drawing.Color.Black
        Me.BtGrant.Location = New System.Drawing.Point(144, 178)
        Me.BtGrant.Name = "BtGrant"
        Me.BtGrant.Size = New System.Drawing.Size(116, 36)
        Me.BtGrant.TabIndex = 0
        Me.BtGrant.Text = "&Grant"
        Me.BtGrant.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Aqua
        Me.Label1.Location = New System.Drawing.Point(69, 153)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(271, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "TRUCK DEPARTURE PERMISSIVE"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Aqua
        Me.Label6.Location = New System.Drawing.Point(72, 71)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(159, 20)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "EMERGENSY STOP"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Aqua
        Me.Label7.Location = New System.Drawing.Point(72, 114)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(209, 20)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "UNLOADING PERMISSIVE"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Aqua
        Me.Label8.Location = New System.Drawing.Point(72, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(173, 20)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "OPERATION ALARMS"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(3, 22)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LEDDEP, Me.LEDPERM, Me.LEDESD2, Me.LEDOP})
        Me.ShapeContainer1.Size = New System.Drawing.Size(395, 195)
        Me.ShapeContainer1.TabIndex = 4
        Me.ShapeContainer1.TabStop = False
        '
        'LEDDEP
        '
        Me.LEDDEP.BackColor = System.Drawing.Color.Red
        Me.LEDDEP.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDDEP.BorderColor = System.Drawing.Color.White
        Me.LEDDEP.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEDDEP.Location = New System.Drawing.Point(22, 127)
        Me.LEDDEP.Name = "LEDDEP"
        Me.LEDDEP.Size = New System.Drawing.Size(37, 23)
        '
        'LEDPERM
        '
        Me.LEDPERM.BackColor = System.Drawing.Color.Red
        Me.LEDPERM.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDPERM.BorderColor = System.Drawing.Color.White
        Me.LEDPERM.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEDPERM.Location = New System.Drawing.Point(22, 88)
        Me.LEDPERM.Name = "LEDPERM"
        Me.LEDPERM.Size = New System.Drawing.Size(37, 23)
        '
        'LEDESD2
        '
        Me.LEDESD2.BackColor = System.Drawing.Color.Red
        Me.LEDESD2.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDESD2.BorderColor = System.Drawing.Color.White
        Me.LEDESD2.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEDESD2.Location = New System.Drawing.Point(22, 49)
        Me.LEDESD2.Name = "LEDESD2"
        Me.LEDESD2.Size = New System.Drawing.Size(37, 23)
        '
        'LEDOP
        '
        Me.LEDOP.BackColor = System.Drawing.Color.Red
        Me.LEDOP.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.LEDOP.BorderColor = System.Drawing.Color.White
        Me.LEDOP.Location = New System.Drawing.Point(22, 10)
        Me.LEDOP.Name = "LEDOP"
        Me.LEDOP.Size = New System.Drawing.Size(37, 23)
        '
        'AmmoniaUnload
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1653, 874)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "AmmoniaUnload"
        Me.ShowInTaskbar = False
        Me.Text = "AmmoniaUnload"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.H_panel.ResumeLayout(False)
        Me.H_panel.PerformLayout()
        Me.GroupEMERGENSY.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.P_Weight.ResumeLayout(False)
        Me.P_Weight.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.XVPB3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPG3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPR3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPL3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPB2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPG2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPR2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPL2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.EVEH, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents H_panel As System.Windows.Forms.Panel
    Friend WithEvents GroupEMERGENSY As System.Windows.Forms.GroupBox
    Friend WithEvents BTESD As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents P_Weight As System.Windows.Forms.Panel
    Friend WithEvents W_DATETIME As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents weight_Unit As System.Windows.Forms.Label
    Friend WithEvents weight As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents M_NAME As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents M_TIME As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents M_GROUP As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents M_DATE As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents BtPstop As System.Windows.Forms.Button
    Friend WithEvents btPstart As System.Windows.Forms.Button
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ShapeContainer4 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape13 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer3 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LEDPOWER As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LEDLOADING As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LEDESD As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LEDGND As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents EDSTOP As System.Windows.Forms.TextBox
    Friend WithEvents EDTSTART As System.Windows.Forms.TextBox
    Friend WithEvents EDWOUT As System.Windows.Forms.TextBox
    Friend WithEvents EDWIN As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents EDCUS As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents EDVEH As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents EDREF As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents EVEH As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BtReset As System.Windows.Forms.Button
    Friend WithEvents BtRemove As System.Windows.Forms.Button
    Friend WithEvents BtGrant As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LEDDEP As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LEDPERM As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LEDESD2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LEDOP As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer5 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape4 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape3 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents LineShape7 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape6 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape5 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents XVPB3 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPG3 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPR3 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPL3 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPB2 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPG2 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPR2 As System.Windows.Forms.PictureBox
    Friend WithEvents XVPL2 As System.Windows.Forms.PictureBox
    Friend WithEvents LStatus2 As System.Windows.Forms.Label
    Friend WithEvents LMode2 As System.Windows.Forms.Label
    Friend WithEvents LStatus3 As System.Windows.Forms.Label
    Friend WithEvents LMode3 As System.Windows.Forms.Label
End Class
