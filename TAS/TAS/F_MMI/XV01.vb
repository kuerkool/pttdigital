﻿Imports System.ComponentModel

Public Class XV01
    Friend WithEvents ThrRefStstus As BackgroundWorker
    Private CallDataBindToStatus As New MethodInvoker(AddressOf Me.DataBindToStatus)
    Private BIT_UnStatus As String

    Private Sub RunThrRefStstus()

        ThrRefStstus = New BackgroundWorker
        ThrRefStstus.WorkerSupportsCancellation = True
        AddHandler ThrRefStstus.DoWork, AddressOf AsyncThrRefStstus
        ThrRefStstus.RunWorkerAsync()
    End Sub

    Private Sub SetImageXV(ByVal Index As Int16)
        Select Case Index
            Case 0
                XVPR.Visible = True
                XVPG.Visible = False
                XVPB.Visible = False
                XVPL.Visible = False
            Case 1
                XVPR.Visible = False
                XVPG.Visible = True
                XVPB.Visible = False
                XVPL.Visible = False
            Case 2
                XVPR.Visible = False
                XVPG.Visible = False
                XVPB.Visible = True
                XVPL.Visible = False
            Case 3
                XVPR.Visible = False
                XVPG.Visible = False
                XVPB.Visible = False
                XVPL.Visible = True
        End Select
    End Sub
    Private Sub AsyncThrRefStstus(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        Dim da As New SqlClient.SqlDataAdapter
        Dim ds As New DataSet
        Dim SqlCon As New SqlClient.SqlConnection
        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        SqlCon.Open()
        ' Dim i, j As Integer
        Dim q As String
        Dim T_REALTIME, T_MBR As New DataTable
        Dim REG_UnStatus As Integer



        Do
            If ThrRefStstus.CancellationPending = True Then
                e.Cancel = True
                Exit Do
            Else
                Try
                    BIT_UnStatus = ""
                    q = "select * from T_MBR where mbr_reg='406501'"
                    da = New SqlClient.SqlDataAdapter(q, SqlCon)
                    'ds.Reset()
                    ds.Dispose()
                    ds = New DataSet
                    da.Fill(ds, "T_MBR")
                    REG_UnStatus = ds.Tables("T_MBR").Rows(0).Item("MBR_VAL").ToString
                    BIT_UnStatus = Strings.Right("0000000000000000" + Convert.ToString(REG_UnStatus, 2), 16)
                    ' GNDSTATUS = BIT_UnStatus(12).ToString


                    Me.BeginInvoke(CallDataBindToStatus)
                Catch ex As Exception

                End Try

            End If
            Threading.Thread.Sleep(1500)
        Loop While True
    End Sub


    Private Sub DataBindToStatus()
        Try



            If BIT_UnStatus(2) = "1" Then
                LMode.Text = "Manual Mode"
                BtOpen.Enabled = True
                BtClose.Enabled = True
                BtReset.Enabled = True
            Else

                LMode.Text = "Auto Mode"
                BtOpen.Enabled = False
                BtClose.Enabled = False
                '      BtReset.Enabled =false
            End If


            If BIT_UnStatus(14) = "1" Then

                LStatus.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus.ForeColor = Color.Blue
                LStatus.Text = "Moving..."
                'LStatus.BehaviorOptions.Active :=False;
                BtOpen.Enabled = True
                BtClose.Enabled = True
                SetImageXV(2)
            End If




            If BIT_UnStatus(4) = "1" Then

                LStatus.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus.ForeColor = Color.Red
                LStatus.Text = "Fully Close"
                'LStatus.BehaviorOptions.Active :=False;
                BtClose.Enabled = False
                SetImageXV(0)
            End If

            If BIT_UnStatus(5) = "1" Then

                LStatus.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus.ForeColor = Color.Green
                LStatus.Text = "Fully Open"
                ' LStatus.BehaviorOptions.Active :=False;
                BtOpen.Enabled = False
                SetImageXV(1)
            End If


            If BIT_UnStatus(9) = "1" Then

                BtOpen.Enabled = False
                BtClose.Enabled = False
                LStatus.Visible = True
                'LStatus.Behavior:='Blinking';
                LStatus.ForeColor = Color.Yellow
                LStatus.Text = "Fail..."
                'LStatus.BiDiMode:=bdLeftToRight;
                'LStatus.BehaviorOptions.Active :=True;
                SetImageXV(3)
            End If


        Catch ex As Exception

        End Try
    End Sub



    Private Sub BtOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtOpen.Click
        If MessageBox.Show("Open XV-M2014-3 ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 0 of 15 PCB 8421
            UpdateSql("406001", 1)
        End If
    End Sub

    Private Sub UpdateSql(ByVal Reg As String, ByVal Val As String)
        Dim q As String
        Dim SqlCon As New SqlClient.SqlConnection
        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        Dim da As New SqlClient.SqlDataAdapter
        SqlCon.Open()
        Try
            q = ""
            q = q + "update T_MBR "
            q = q + " set MBR_DATE = getdate() "
            q = q + ",MBR_VAL = '" + (Val) + "'"
            q = q + " where MBR_REG = '" + (Reg) + "'"
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()
        Catch ex As Exception
        End Try
        da.Dispose()
        SqlCon.Dispose()
    End Sub

    Private Sub BtClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtClose.Click
        If MessageBox.Show("Close XV-M2014-3 ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 1 of 15 PCB 8421
            UpdateSql("406001", 2)
        End If
    End Sub

    Private Sub BtAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtAuto.Click
        If MessageBox.Show("Select Mode XV-M2014-3 ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 2 of 15 PCB 8421
            UpdateSql("406001", 4)
        End If
    End Sub

    Private Sub BtReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtReset.Click
        If MessageBox.Show("Reset XV-M2014-3 ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 3 of 15 PCB 8421
            UpdateSql("406001", 8)
        End If
    End Sub


    Private Sub XV01_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RunThrRefStstus()
    End Sub

    Private Sub XV01_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ThrRefStstus.CancelAsync()
    End Sub
End Class