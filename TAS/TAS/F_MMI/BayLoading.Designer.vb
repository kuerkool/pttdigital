﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BayLoading
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.RadGroupBox9 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadPanel2 = New Telerik.WinControls.UI.RadPanel()
        Me.W_DATETIME = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.weight = New System.Windows.Forms.Label()
        Me.weight_Unit = New System.Windows.Forms.Label()
        Me.IMCus = New System.Windows.Forms.PictureBox()
        Me.RadClock1 = New Telerik.WinControls.UI.RadClock()
        Me.RadCalendar1 = New Telerik.WinControls.UI.RadCalendar()
        Me.P1 = New System.Windows.Forms.PictureBox()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.FPTDataSet = New TAS_TOL.FPTDataSet()
        Me.LOADBAYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomShape1 = New Telerik.WinControls.CustomShape(Me.components)
        Me.CustomShape2 = New Telerik.WinControls.CustomShape(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel3 = New Telerik.WinControls.UI.RadPanel()
        Me.QaShape1 = New Telerik.WinControls.Tests.QAShape()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.RadPanel1 = New Telerik.WinControls.UI.RadPanel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RadSeparator46 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.RoundRectShape1 = New Telerik.WinControls.RoundRectShape(Me.components)
        Me.Label12 = New System.Windows.Forms.Label()
        Me.EllipseShape1 = New Telerik.WinControls.EllipseShape()
        Me.TabOffice12Shape1 = New Telerik.WinControls.UI.TabOffice12Shape()
        Me.TabVsShape1 = New Telerik.WinControls.UI.TabVsShape()
        Me.TrackBarDThumbShape1 = New Telerik.WinControls.UI.TrackBarDThumbShape()
        Me.TrackBarLThumbShape1 = New Telerik.WinControls.UI.TrackBarLThumbShape()
        Me.TrackBarRThumbShape1 = New Telerik.WinControls.UI.TrackBarRThumbShape()
        Me.TrackBarUThumbShape1 = New Telerik.WinControls.UI.TrackBarUThumbShape()
        Me.MediaShape1 = New Telerik.WinControls.Tests.MediaShape()
        Me.LOADBAYTableAdapter = New TAS_TOL.FPTDataSetTableAdapters.LOADBAYTableAdapter()
        Me.RadGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel4 = New Telerik.WinControls.UI.RadPanel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.BindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.RadPanel5 = New Telerik.WinControls.UI.RadPanel()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.RadSeparator1 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.P2 = New System.Windows.Forms.PictureBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.BindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource6 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource7 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource8 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource9 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource10 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource11 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource12 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource13 = New System.Windows.Forms.BindingSource(Me.components)
        Me.RadGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel6 = New Telerik.WinControls.UI.RadPanel()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.RadPanel7 = New Telerik.WinControls.UI.RadPanel()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.RadSeparator2 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.P3 = New System.Windows.Forms.PictureBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.RadGroupBox4 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel8 = New Telerik.WinControls.UI.RadPanel()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.RadPanel9 = New Telerik.WinControls.UI.RadPanel()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.RadSeparator3 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.P4 = New System.Windows.Forms.PictureBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.RadGroupBox5 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel10 = New Telerik.WinControls.UI.RadPanel()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.RadPanel11 = New Telerik.WinControls.UI.RadPanel()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.RadSeparator4 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.P5 = New System.Windows.Forms.PictureBox()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.RadGroupBox6 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel12 = New Telerik.WinControls.UI.RadPanel()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.RadPanel13 = New Telerik.WinControls.UI.RadPanel()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.RadSeparator5 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.P6 = New System.Windows.Forms.PictureBox()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.RadGroupBox7 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel14 = New Telerik.WinControls.UI.RadPanel()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.Label126 = New System.Windows.Forms.Label()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.RadPanel15 = New Telerik.WinControls.UI.RadPanel()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.RadSeparator6 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.Label137 = New System.Windows.Forms.Label()
        Me.Label138 = New System.Windows.Forms.Label()
        Me.P7 = New System.Windows.Forms.PictureBox()
        Me.Label139 = New System.Windows.Forms.Label()
        Me.Label140 = New System.Windows.Forms.Label()
        Me.Label141 = New System.Windows.Forms.Label()
        Me.Label142 = New System.Windows.Forms.Label()
        Me.RadGroupBox8 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel16 = New Telerik.WinControls.UI.RadPanel()
        Me.Label143 = New System.Windows.Forms.Label()
        Me.Label144 = New System.Windows.Forms.Label()
        Me.Label145 = New System.Windows.Forms.Label()
        Me.Label146 = New System.Windows.Forms.Label()
        Me.Label147 = New System.Windows.Forms.Label()
        Me.Label148 = New System.Windows.Forms.Label()
        Me.RadPanel17 = New Telerik.WinControls.UI.RadPanel()
        Me.Label149 = New System.Windows.Forms.Label()
        Me.Label150 = New System.Windows.Forms.Label()
        Me.Label151 = New System.Windows.Forms.Label()
        Me.Label152 = New System.Windows.Forms.Label()
        Me.RadSeparator7 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label153 = New System.Windows.Forms.Label()
        Me.Label154 = New System.Windows.Forms.Label()
        Me.Label155 = New System.Windows.Forms.Label()
        Me.Label156 = New System.Windows.Forms.Label()
        Me.Label157 = New System.Windows.Forms.Label()
        Me.Label158 = New System.Windows.Forms.Label()
        Me.P8 = New System.Windows.Forms.PictureBox()
        Me.Label159 = New System.Windows.Forms.Label()
        Me.Label160 = New System.Windows.Forms.Label()
        Me.Label161 = New System.Windows.Forms.Label()
        Me.Label162 = New System.Windows.Forms.Label()
        Me.RadGroupBox10 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel18 = New Telerik.WinControls.UI.RadPanel()
        Me.Label163 = New System.Windows.Forms.Label()
        Me.Label164 = New System.Windows.Forms.Label()
        Me.Label165 = New System.Windows.Forms.Label()
        Me.Label166 = New System.Windows.Forms.Label()
        Me.Label167 = New System.Windows.Forms.Label()
        Me.Label168 = New System.Windows.Forms.Label()
        Me.RadPanel19 = New Telerik.WinControls.UI.RadPanel()
        Me.Label169 = New System.Windows.Forms.Label()
        Me.Label170 = New System.Windows.Forms.Label()
        Me.Label171 = New System.Windows.Forms.Label()
        Me.Label172 = New System.Windows.Forms.Label()
        Me.RadSeparator8 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label173 = New System.Windows.Forms.Label()
        Me.Label174 = New System.Windows.Forms.Label()
        Me.Label175 = New System.Windows.Forms.Label()
        Me.Label176 = New System.Windows.Forms.Label()
        Me.Label177 = New System.Windows.Forms.Label()
        Me.Label178 = New System.Windows.Forms.Label()
        Me.P9 = New System.Windows.Forms.PictureBox()
        Me.Label179 = New System.Windows.Forms.Label()
        Me.Label180 = New System.Windows.Forms.Label()
        Me.Label181 = New System.Windows.Forms.Label()
        Me.Label182 = New System.Windows.Forms.Label()
        Me.RadGroupBox11 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel20 = New Telerik.WinControls.UI.RadPanel()
        Me.Label183 = New System.Windows.Forms.Label()
        Me.Label184 = New System.Windows.Forms.Label()
        Me.Label185 = New System.Windows.Forms.Label()
        Me.Label186 = New System.Windows.Forms.Label()
        Me.Label187 = New System.Windows.Forms.Label()
        Me.Label188 = New System.Windows.Forms.Label()
        Me.RadPanel21 = New Telerik.WinControls.UI.RadPanel()
        Me.Label189 = New System.Windows.Forms.Label()
        Me.Label190 = New System.Windows.Forms.Label()
        Me.Label191 = New System.Windows.Forms.Label()
        Me.Label192 = New System.Windows.Forms.Label()
        Me.RadSeparator9 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label193 = New System.Windows.Forms.Label()
        Me.Label194 = New System.Windows.Forms.Label()
        Me.Label195 = New System.Windows.Forms.Label()
        Me.Label196 = New System.Windows.Forms.Label()
        Me.Label197 = New System.Windows.Forms.Label()
        Me.Label198 = New System.Windows.Forms.Label()
        Me.P10 = New System.Windows.Forms.PictureBox()
        Me.Label199 = New System.Windows.Forms.Label()
        Me.Label200 = New System.Windows.Forms.Label()
        Me.Label201 = New System.Windows.Forms.Label()
        Me.Label202 = New System.Windows.Forms.Label()
        Me.RadGroupBox12 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel22 = New Telerik.WinControls.UI.RadPanel()
        Me.Label203 = New System.Windows.Forms.Label()
        Me.Label204 = New System.Windows.Forms.Label()
        Me.Label205 = New System.Windows.Forms.Label()
        Me.Label206 = New System.Windows.Forms.Label()
        Me.Label207 = New System.Windows.Forms.Label()
        Me.Label208 = New System.Windows.Forms.Label()
        Me.RadPanel23 = New Telerik.WinControls.UI.RadPanel()
        Me.Label209 = New System.Windows.Forms.Label()
        Me.Label210 = New System.Windows.Forms.Label()
        Me.Label211 = New System.Windows.Forms.Label()
        Me.Label212 = New System.Windows.Forms.Label()
        Me.RadSeparator10 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label213 = New System.Windows.Forms.Label()
        Me.Label214 = New System.Windows.Forms.Label()
        Me.Label215 = New System.Windows.Forms.Label()
        Me.Label216 = New System.Windows.Forms.Label()
        Me.Label217 = New System.Windows.Forms.Label()
        Me.Label218 = New System.Windows.Forms.Label()
        Me.P11 = New System.Windows.Forms.PictureBox()
        Me.Label219 = New System.Windows.Forms.Label()
        Me.Label220 = New System.Windows.Forms.Label()
        Me.Label221 = New System.Windows.Forms.Label()
        Me.Label222 = New System.Windows.Forms.Label()
        Me.RadGroupBox13 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel24 = New Telerik.WinControls.UI.RadPanel()
        Me.Label223 = New System.Windows.Forms.Label()
        Me.Label224 = New System.Windows.Forms.Label()
        Me.Label225 = New System.Windows.Forms.Label()
        Me.Label226 = New System.Windows.Forms.Label()
        Me.Label227 = New System.Windows.Forms.Label()
        Me.Label228 = New System.Windows.Forms.Label()
        Me.RadPanel25 = New Telerik.WinControls.UI.RadPanel()
        Me.Label229 = New System.Windows.Forms.Label()
        Me.Label230 = New System.Windows.Forms.Label()
        Me.Label231 = New System.Windows.Forms.Label()
        Me.Label232 = New System.Windows.Forms.Label()
        Me.RadSeparator11 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label233 = New System.Windows.Forms.Label()
        Me.Label234 = New System.Windows.Forms.Label()
        Me.Label235 = New System.Windows.Forms.Label()
        Me.Label236 = New System.Windows.Forms.Label()
        Me.Label237 = New System.Windows.Forms.Label()
        Me.Label238 = New System.Windows.Forms.Label()
        Me.P12 = New System.Windows.Forms.PictureBox()
        Me.Label239 = New System.Windows.Forms.Label()
        Me.Label240 = New System.Windows.Forms.Label()
        Me.Label241 = New System.Windows.Forms.Label()
        Me.Label242 = New System.Windows.Forms.Label()
        Me.RadGroupBox14 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel26 = New Telerik.WinControls.UI.RadPanel()
        Me.Label243 = New System.Windows.Forms.Label()
        Me.Label244 = New System.Windows.Forms.Label()
        Me.Label245 = New System.Windows.Forms.Label()
        Me.Label246 = New System.Windows.Forms.Label()
        Me.Label247 = New System.Windows.Forms.Label()
        Me.Label248 = New System.Windows.Forms.Label()
        Me.RadPanel27 = New Telerik.WinControls.UI.RadPanel()
        Me.Label249 = New System.Windows.Forms.Label()
        Me.Label250 = New System.Windows.Forms.Label()
        Me.Label251 = New System.Windows.Forms.Label()
        Me.Label252 = New System.Windows.Forms.Label()
        Me.RadSeparator12 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label253 = New System.Windows.Forms.Label()
        Me.Label254 = New System.Windows.Forms.Label()
        Me.Label255 = New System.Windows.Forms.Label()
        Me.Label256 = New System.Windows.Forms.Label()
        Me.Label257 = New System.Windows.Forms.Label()
        Me.Label258 = New System.Windows.Forms.Label()
        Me.P13 = New System.Windows.Forms.PictureBox()
        Me.Label259 = New System.Windows.Forms.Label()
        Me.Label260 = New System.Windows.Forms.Label()
        Me.Label261 = New System.Windows.Forms.Label()
        Me.Label262 = New System.Windows.Forms.Label()
        Me.RadGroupBox15 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel28 = New Telerik.WinControls.UI.RadPanel()
        Me.Label263 = New System.Windows.Forms.Label()
        Me.BindingSource14 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label264 = New System.Windows.Forms.Label()
        Me.Label265 = New System.Windows.Forms.Label()
        Me.Label266 = New System.Windows.Forms.Label()
        Me.Label267 = New System.Windows.Forms.Label()
        Me.Label268 = New System.Windows.Forms.Label()
        Me.RadPanel29 = New Telerik.WinControls.UI.RadPanel()
        Me.Label269 = New System.Windows.Forms.Label()
        Me.Label270 = New System.Windows.Forms.Label()
        Me.Label271 = New System.Windows.Forms.Label()
        Me.Label272 = New System.Windows.Forms.Label()
        Me.RadSeparator13 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label273 = New System.Windows.Forms.Label()
        Me.Label274 = New System.Windows.Forms.Label()
        Me.Label275 = New System.Windows.Forms.Label()
        Me.Label276 = New System.Windows.Forms.Label()
        Me.Label277 = New System.Windows.Forms.Label()
        Me.Label278 = New System.Windows.Forms.Label()
        Me.P14 = New System.Windows.Forms.PictureBox()
        Me.Label279 = New System.Windows.Forms.Label()
        Me.Label280 = New System.Windows.Forms.Label()
        Me.Label281 = New System.Windows.Forms.Label()
        Me.Label282 = New System.Windows.Forms.Label()
        Me.RadGroupBox16 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadPanel30 = New Telerik.WinControls.UI.RadPanel()
        Me.Label283 = New System.Windows.Forms.Label()
        Me.BindingSource15 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label284 = New System.Windows.Forms.Label()
        Me.Label285 = New System.Windows.Forms.Label()
        Me.Label286 = New System.Windows.Forms.Label()
        Me.Label287 = New System.Windows.Forms.Label()
        Me.Label288 = New System.Windows.Forms.Label()
        Me.RadPanel31 = New Telerik.WinControls.UI.RadPanel()
        Me.Label289 = New System.Windows.Forms.Label()
        Me.Label290 = New System.Windows.Forms.Label()
        Me.Label291 = New System.Windows.Forms.Label()
        Me.Label292 = New System.Windows.Forms.Label()
        Me.RadSeparator14 = New Telerik.WinControls.UI.RadSeparator()
        Me.Label293 = New System.Windows.Forms.Label()
        Me.Label294 = New System.Windows.Forms.Label()
        Me.Label295 = New System.Windows.Forms.Label()
        Me.Label296 = New System.Windows.Forms.Label()
        Me.Label297 = New System.Windows.Forms.Label()
        Me.Label298 = New System.Windows.Forms.Label()
        Me.P15 = New System.Windows.Forms.PictureBox()
        Me.Label299 = New System.Windows.Forms.Label()
        Me.Label300 = New System.Windows.Forms.Label()
        Me.Label301 = New System.Windows.Forms.Label()
        Me.Label302 = New System.Windows.Forms.Label()
        CType(Me.RadGroupBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox9.SuspendLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel2.SuspendLayout()
        CType(Me.IMCus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadClock1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadCalendar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LOADBAYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.RadPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel1.SuspendLayout()
        CType(Me.RadSeparator46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox2.SuspendLayout()
        CType(Me.RadPanel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel5.SuspendLayout()
        CType(Me.RadSeparator1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox3.SuspendLayout()
        CType(Me.RadPanel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel7.SuspendLayout()
        CType(Me.RadSeparator2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox4.SuspendLayout()
        CType(Me.RadPanel8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel9.SuspendLayout()
        CType(Me.RadSeparator3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox5.SuspendLayout()
        CType(Me.RadPanel10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel11.SuspendLayout()
        CType(Me.RadSeparator4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox6.SuspendLayout()
        CType(Me.RadPanel12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel13.SuspendLayout()
        CType(Me.RadSeparator5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox7.SuspendLayout()
        CType(Me.RadPanel14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel15.SuspendLayout()
        CType(Me.RadSeparator6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox8.SuspendLayout()
        CType(Me.RadPanel16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel17.SuspendLayout()
        CType(Me.RadSeparator7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox10.SuspendLayout()
        CType(Me.RadPanel18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel19.SuspendLayout()
        CType(Me.RadSeparator8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox11.SuspendLayout()
        CType(Me.RadPanel20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel21.SuspendLayout()
        CType(Me.RadSeparator9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox12.SuspendLayout()
        CType(Me.RadPanel22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel23.SuspendLayout()
        CType(Me.RadSeparator10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox13.SuspendLayout()
        CType(Me.RadPanel24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel25.SuspendLayout()
        CType(Me.RadSeparator11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox14.SuspendLayout()
        CType(Me.RadPanel26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel27, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel27.SuspendLayout()
        CType(Me.RadSeparator12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox15.SuspendLayout()
        CType(Me.RadPanel28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel29, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel29.SuspendLayout()
        CType(Me.RadSeparator13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox16.SuspendLayout()
        CType(Me.RadPanel30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadPanel31, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPanel31.SuspendLayout()
        CType(Me.RadSeparator14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.P15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadGroupBox9
        '
        Me.RadGroupBox9.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox9.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RadGroupBox9.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox9.Controls.Add(Me.RadPanel2)
        Me.RadGroupBox9.Controls.Add(Me.IMCus)
        Me.RadGroupBox9.Controls.Add(Me.RadClock1)
        Me.RadGroupBox9.Controls.Add(Me.RadCalendar1)
        Me.RadGroupBox9.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadGroupBox9.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office
        Me.RadGroupBox9.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox9.HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Bottom
        Me.RadGroupBox9.HeaderText = ""
        Me.RadGroupBox9.Location = New System.Drawing.Point(0, 0)
        Me.RadGroupBox9.Name = "RadGroupBox9"
        Me.RadGroupBox9.Size = New System.Drawing.Size(1358, 147)
        Me.RadGroupBox9.TabIndex = 37
        CType(Me.RadGroupBox9.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox9.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).InvalidateMeasureInMainLayout = 1
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(199, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(215, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).NumberOfColors = 4
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).GradientStyle = Telerik.WinControls.GradientStyles.Linear
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor3 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor4 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor3 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).InnerColor4 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).ForeColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.GroupBoxHeader).HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Bottom
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.GroupBoxHeader).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadGroupBox9.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
        '
        'RadLabel1
        '
        Me.RadLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.RadLabel1.BackColor = System.Drawing.Color.Transparent
        Me.RadLabel1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.ForeColor = System.Drawing.Color.White
        Me.RadLabel1.Location = New System.Drawing.Point(425, 18)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(509, 34)
        Me.RadLabel1.TabIndex = 31
        Me.RadLabel1.TabStop = True
        Me.RadLabel1.Text = "TERMINAL AUTOMATION SYSTEM (TAS)"
        Me.RadLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RadPanel2
        '
        Me.RadPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.RadPanel2.Controls.Add(Me.W_DATETIME)
        Me.RadPanel2.Controls.Add(Me.Label15)
        Me.RadPanel2.Controls.Add(Me.Label14)
        Me.RadPanel2.Controls.Add(Me.weight)
        Me.RadPanel2.Controls.Add(Me.weight_Unit)
        Me.RadPanel2.Location = New System.Drawing.Point(425, 58)
        Me.RadPanel2.Name = "RadPanel2"
        Me.RadPanel2.Size = New System.Drawing.Size(489, 66)
        Me.RadPanel2.TabIndex = 37
        Me.RadPanel2.ThemeName = "Breeze"
        CType(Me.RadPanel2.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        '
        'W_DATETIME
        '
        Me.W_DATETIME.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.W_DATETIME.AutoSize = True
        Me.W_DATETIME.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.W_DATETIME.ForeColor = System.Drawing.Color.White
        Me.W_DATETIME.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.W_DATETIME.Location = New System.Drawing.Point(75, 50)
        Me.W_DATETIME.Name = "W_DATETIME"
        Me.W_DATETIME.Size = New System.Drawing.Size(114, 13)
        Me.W_DATETIME.TabIndex = 32
        Me.W_DATETIME.Text = "dd/MM/yyyy hh:mm:ss"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(15, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(173, 52)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "WEIGHT"
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(22, 50)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(57, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Updated : "
        '
        'weight
        '
        Me.weight.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight.ForeColor = System.Drawing.Color.Lime
        Me.weight.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight.Location = New System.Drawing.Point(186, 6)
        Me.weight.Name = "weight"
        Me.weight.Size = New System.Drawing.Size(210, 52)
        Me.weight.TabIndex = 30
        Me.weight.Text = "0"
        Me.weight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'weight_Unit
        '
        Me.weight_Unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.weight_Unit.AutoSize = True
        Me.weight_Unit.Font = New System.Drawing.Font("DS-Digital", 39.75!, System.Drawing.FontStyle.Bold)
        Me.weight_Unit.ForeColor = System.Drawing.Color.Lime
        Me.weight_Unit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.weight_Unit.Location = New System.Drawing.Point(395, 6)
        Me.weight_Unit.Name = "weight_Unit"
        Me.weight_Unit.Size = New System.Drawing.Size(77, 52)
        Me.weight_Unit.TabIndex = 31
        Me.weight_Unit.Text = "KG"
        '
        'IMCus
        '
        Me.IMCus.BackColor = System.Drawing.Color.Transparent
        Me.IMCus.Image = Global.TAS_TOL.My.Resources.Resources.PTT2
        Me.IMCus.Location = New System.Drawing.Point(6, 3)
        Me.IMCus.Name = "IMCus"
        Me.IMCus.Size = New System.Drawing.Size(260, 133)
        Me.IMCus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.IMCus.TabIndex = 14
        Me.IMCus.TabStop = False
        '
        'RadClock1
        '
        Me.RadClock1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadClock1.BackColor = System.Drawing.Color.Transparent
        Me.RadClock1.Location = New System.Drawing.Point(1037, 1)
        Me.RadClock1.Name = "RadClock1"
        Me.RadClock1.Size = New System.Drawing.Size(134, 135)
        Me.RadClock1.TabIndex = 13
        Me.RadClock1.Text = "RadClock1"
        Me.RadClock1.ThemeName = "Office2010Blue"
        '
        'RadCalendar1
        '
        Me.RadCalendar1.AllowSelect = False
        Me.RadCalendar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadCalendar1.Location = New System.Drawing.Point(1177, 3)
        Me.RadCalendar1.Name = "RadCalendar1"
        Me.RadCalendar1.ReadOnly = True
        Me.RadCalendar1.ShowHeader = False
        Me.RadCalendar1.ShowNavigationButtons = False
        Me.RadCalendar1.ShowViewHeader = True
        Me.RadCalendar1.Size = New System.Drawing.Size(176, 133)
        Me.RadCalendar1.TabIndex = 12
        Me.RadCalendar1.Text = "RadCalendar1"
        Me.RadCalendar1.ThemeName = "Office2010Blue"
        '
        'P1
        '
        Me.P1.ErrorImage = Nothing
        Me.P1.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P1.Location = New System.Drawing.Point(4, -6)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(275, 127)
        Me.P1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P1.TabIndex = 38
        Me.P1.TabStop = False
        Me.P1.Visible = False
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "LOADBAY"
        Me.BindingSource1.DataSource = Me.FPTDataSet
        '
        'FPTDataSet
        '
        Me.FPTDataSet.DataSetName = "FPTDataSet"
        Me.FPTDataSet.Locale = New System.Globalization.CultureInfo("th-TH")
        Me.FPTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LOADBAYBindingSource
        '
        Me.LOADBAYBindingSource.DataMember = "LOADBAY"
        Me.LOADBAYBindingSource.DataSource = Me.FPTDataSet
        '
        'CustomShape1
        '
        Me.CustomShape1.AsString = "20,20,200,100:20,20,False,0,0,0,0,0:220,20,False,0,0,0,0,0:220,120,False,0,0,0,0," & _
    "0:20,120,False,0,0,0,0,0:"
        '
        'CustomShape2
        '
        Me.CustomShape2.AsString = "20,20,200,100:20,20,False,0,0,0,0,0:220,20,False,0,0,0,0,0:220,120,False,0,0,0,0," & _
    "0:20,120,False,0,0,0,0,0:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(280, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "Truck No. :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(285, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 16)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Load No. :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(301, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 16)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Driver :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(288, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 16)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Quantity :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(289, 117)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 16)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "Material :"
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox1.Controls.Add(Me.RadPanel3)
        Me.RadGroupBox1.Controls.Add(Me.Label7)
        Me.RadGroupBox1.Controls.Add(Me.Label8)
        Me.RadGroupBox1.Controls.Add(Me.Label9)
        Me.RadGroupBox1.Controls.Add(Me.Label10)
        Me.RadGroupBox1.Controls.Add(Me.Label11)
        Me.RadGroupBox1.Controls.Add(Me.Label22)
        Me.RadGroupBox1.Controls.Add(Me.RadPanel1)
        Me.RadGroupBox1.Controls.Add(Me.Label12)
        Me.RadGroupBox1.Controls.Add(Me.Label5)
        Me.RadGroupBox1.Controls.Add(Me.Label4)
        Me.RadGroupBox1.Controls.Add(Me.Label2)
        Me.RadGroupBox1.Controls.Add(Me.Label3)
        Me.RadGroupBox1.Controls.Add(Me.Label1)
        Me.RadGroupBox1.Controls.Add(Me.P1)
        Me.RadGroupBox1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox1.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox1.HeaderText = ""
        Me.RadGroupBox1.Location = New System.Drawing.Point(114, 157)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox1.TabIndex = 43
        CType(Me.RadGroupBox1.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox1.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel3
        '
        Me.RadPanel3.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel3.Name = "RadPanel3"
        '
        '
        '
        Me.RadPanel3.RootElement.Shape = Me.QaShape1
        Me.RadPanel3.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel3.TabIndex = 44
        Me.RadPanel3.Text = "BAY 01"
        Me.RadPanel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 01"
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel3.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "Customer_name", True))
        Me.Label7.ForeColor = System.Drawing.Color.Navy
        Me.Label7.Location = New System.Drawing.Point(358, 54)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 16)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Customer :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "Product_code", True))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(358, 117)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 16)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Material :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "LOAD_PRESET", True))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(358, 138)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 16)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Quantity :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "LOAD_DID", True))
        Me.Label10.ForeColor = System.Drawing.Color.Navy
        Me.Label10.Location = New System.Drawing.Point(358, 33)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 16)
        Me.Label10.TabIndex = 68
        Me.Label10.Text = "Load No. :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "LOAD_DRIVER", True))
        Me.Label11.ForeColor = System.Drawing.Color.Navy
        Me.Label11.Location = New System.Drawing.Point(358, 96)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 16)
        Me.Label11.TabIndex = 69
        Me.Label11.Text = "Driver :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "LOAD_VEHICLE", True))
        Me.Label22.ForeColor = System.Drawing.Color.Navy
        Me.Label22.Location = New System.Drawing.Point(358, 75)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(72, 16)
        Me.Label22.TabIndex = 67
        Me.Label22.Text = "Truck No. :"
        '
        'RadPanel1
        '
        Me.RadPanel1.Controls.Add(Me.Label13)
        Me.RadPanel1.Controls.Add(Me.Label16)
        Me.RadPanel1.Controls.Add(Me.Label21)
        Me.RadPanel1.Controls.Add(Me.Label6)
        Me.RadPanel1.Controls.Add(Me.RadSeparator46)
        Me.RadPanel1.Controls.Add(Me.Label18)
        Me.RadPanel1.Controls.Add(Me.Label17)
        Me.RadPanel1.Controls.Add(Me.Label19)
        Me.RadPanel1.Controls.Add(Me.Label20)
        Me.RadPanel1.Location = New System.Drawing.Point(5, 119)
        Me.RadPanel1.Name = "RadPanel1"
        '
        '
        '
        Me.RadPanel1.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel1.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel1.TabIndex = 66
        CType(Me.RadPanel1.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel1.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel1.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(200, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(23, 13)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Kg."
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "Raw_Weight_Out", True))
        Me.Label16.ForeColor = System.Drawing.Color.Navy
        Me.Label16.Location = New System.Drawing.Point(150, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(45, 13)
        Me.Label16.TabIndex = 68
        Me.Label16.Text = "Weight"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "WeightOut_time", True))
        Me.Label21.ForeColor = System.Drawing.Color.Navy
        Me.Label21.Location = New System.Drawing.Point(226, 20)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(30, 13)
        Me.Label21.TabIndex = 69
        Me.Label21.Text = "Time"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(65, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(23, 13)
        Me.Label6.TabIndex = 67
        Me.Label6.Text = "Kg."
        '
        'RadSeparator46
        '
        Me.RadSeparator46.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator46.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator46.Name = "RadSeparator46"
        Me.RadSeparator46.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator46.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator46.TabIndex = 66
        Me.RadSeparator46.Text = "RadSeparator46"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(66, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(21, 16)
        Me.Label18.TabIndex = 61
        Me.Label18.Text = "In"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "Raw_Weight_in", True))
        Me.Label17.ForeColor = System.Drawing.Color.Navy
        Me.Label17.Location = New System.Drawing.Point(15, 20)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 60
        Me.Label17.Text = "Weight"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(195, 1)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 16)
        Me.Label19.TabIndex = 62
        Me.Label19.Text = "Out"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "Weightin_time", True))
        Me.Label20.ForeColor = System.Drawing.Color.Navy
        Me.Label20.Location = New System.Drawing.Point(91, 20)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(30, 13)
        Me.Label20.TabIndex = 63
        Me.Label20.Text = "Time"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(280, 54)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 16)
        Me.Label12.TabIndex = 51
        Me.Label12.Text = "Customer :"
        '
        'LOADBAYTableAdapter
        '
        Me.LOADBAYTableAdapter.ClearBeforeFill = True
        '
        'RadGroupBox2
        '
        Me.RadGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox2.Controls.Add(Me.RadPanel4)
        Me.RadGroupBox2.Controls.Add(Me.Label23)
        Me.RadGroupBox2.Controls.Add(Me.Label24)
        Me.RadGroupBox2.Controls.Add(Me.Label25)
        Me.RadGroupBox2.Controls.Add(Me.Label26)
        Me.RadGroupBox2.Controls.Add(Me.Label27)
        Me.RadGroupBox2.Controls.Add(Me.Label28)
        Me.RadGroupBox2.Controls.Add(Me.RadPanel5)
        Me.RadGroupBox2.Controls.Add(Me.Label37)
        Me.RadGroupBox2.Controls.Add(Me.Label38)
        Me.RadGroupBox2.Controls.Add(Me.P2)
        Me.RadGroupBox2.Controls.Add(Me.Label39)
        Me.RadGroupBox2.Controls.Add(Me.Label40)
        Me.RadGroupBox2.Controls.Add(Me.Label41)
        Me.RadGroupBox2.Controls.Add(Me.Label42)
        Me.RadGroupBox2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox2.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox2.HeaderText = ""
        Me.RadGroupBox2.Location = New System.Drawing.Point(114, 326)
        Me.RadGroupBox2.Name = "RadGroupBox2"
        Me.RadGroupBox2.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox2.TabIndex = 44
        CType(Me.RadGroupBox2.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox2.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox2.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel4
        '
        Me.RadPanel4.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel4.Name = "RadPanel4"
        '
        '
        '
        Me.RadPanel4.RootElement.Shape = Me.QaShape1
        Me.RadPanel4.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel4.TabIndex = 44
        Me.RadPanel4.Text = "BAY 02"
        Me.RadPanel4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 02"
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel4.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "Customer_name", True))
        Me.Label23.ForeColor = System.Drawing.Color.Navy
        Me.Label23.Location = New System.Drawing.Point(358, 54)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(72, 16)
        Me.Label23.TabIndex = 72
        Me.Label23.Text = "Customer :"
        '
        'BindingSource2
        '
        Me.BindingSource2.DataMember = "LOADBAY"
        Me.BindingSource2.DataSource = Me.FPTDataSet
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "Product_code", True))
        Me.Label24.ForeColor = System.Drawing.Color.Navy
        Me.Label24.Location = New System.Drawing.Point(358, 117)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(63, 16)
        Me.Label24.TabIndex = 71
        Me.Label24.Text = "Material :"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "LOAD_PRESET", True))
        Me.Label25.ForeColor = System.Drawing.Color.Navy
        Me.Label25.Location = New System.Drawing.Point(358, 138)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(64, 16)
        Me.Label25.TabIndex = 70
        Me.Label25.Text = "Quantity :"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "LOAD_DID", True))
        Me.Label26.ForeColor = System.Drawing.Color.Navy
        Me.Label26.Location = New System.Drawing.Point(358, 33)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(67, 16)
        Me.Label26.TabIndex = 68
        Me.Label26.Text = "Load No. :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "LOAD_DRIVER", True))
        Me.Label27.ForeColor = System.Drawing.Color.Navy
        Me.Label27.Location = New System.Drawing.Point(358, 96)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(51, 16)
        Me.Label27.TabIndex = 69
        Me.Label27.Text = "Driver :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "LOAD_VEHICLE", True))
        Me.Label28.ForeColor = System.Drawing.Color.Navy
        Me.Label28.Location = New System.Drawing.Point(358, 75)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(72, 16)
        Me.Label28.TabIndex = 67
        Me.Label28.Text = "Truck No. :"
        '
        'RadPanel5
        '
        Me.RadPanel5.Controls.Add(Me.Label29)
        Me.RadPanel5.Controls.Add(Me.Label30)
        Me.RadPanel5.Controls.Add(Me.Label31)
        Me.RadPanel5.Controls.Add(Me.Label32)
        Me.RadPanel5.Controls.Add(Me.RadSeparator1)
        Me.RadPanel5.Controls.Add(Me.Label33)
        Me.RadPanel5.Controls.Add(Me.Label34)
        Me.RadPanel5.Controls.Add(Me.Label35)
        Me.RadPanel5.Controls.Add(Me.Label36)
        Me.RadPanel5.Location = New System.Drawing.Point(5, 118)
        Me.RadPanel5.Name = "RadPanel5"
        '
        '
        '
        Me.RadPanel5.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel5.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel5.TabIndex = 66
        CType(Me.RadPanel5.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel5.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.Color.Black
        Me.Label29.Location = New System.Drawing.Point(200, 20)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(23, 13)
        Me.Label29.TabIndex = 70
        Me.Label29.Text = "Kg."
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "Raw_Weight_Out", True))
        Me.Label30.ForeColor = System.Drawing.Color.Navy
        Me.Label30.Location = New System.Drawing.Point(150, 20)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(45, 13)
        Me.Label30.TabIndex = 68
        Me.Label30.Text = "Weight"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "WeightOut_time", True))
        Me.Label31.ForeColor = System.Drawing.Color.Navy
        Me.Label31.Location = New System.Drawing.Point(226, 20)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(30, 13)
        Me.Label31.TabIndex = 69
        Me.Label31.Text = "Time"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ForeColor = System.Drawing.Color.Black
        Me.Label32.Location = New System.Drawing.Point(65, 20)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(23, 13)
        Me.Label32.TabIndex = 67
        Me.Label32.Text = "Kg."
        '
        'RadSeparator1
        '
        Me.RadSeparator1.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator1.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator1.Name = "RadSeparator1"
        Me.RadSeparator1.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator1.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator1.TabIndex = 66
        Me.RadSeparator1.Text = "RadSeparator1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(66, 0)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(21, 16)
        Me.Label33.TabIndex = 61
        Me.Label33.Text = "In"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "Raw_Weight_in", True))
        Me.Label34.ForeColor = System.Drawing.Color.Navy
        Me.Label34.Location = New System.Drawing.Point(15, 20)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(45, 13)
        Me.Label34.TabIndex = 60
        Me.Label34.Text = "Weight"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(195, 1)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(31, 16)
        Me.Label35.TabIndex = 62
        Me.Label35.Text = "Out"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource2, "Weightin_time", True))
        Me.Label36.ForeColor = System.Drawing.Color.Navy
        Me.Label36.Location = New System.Drawing.Point(91, 20)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(30, 13)
        Me.Label36.TabIndex = 63
        Me.Label36.Text = "Time"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(280, 54)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(72, 16)
        Me.Label37.TabIndex = 51
        Me.Label37.Text = "Customer :"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(289, 117)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(63, 16)
        Me.Label38.TabIndex = 45
        Me.Label38.Text = "Material :"
        '
        'P2
        '
        Me.P2.ErrorImage = Nothing
        Me.P2.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P2.Location = New System.Drawing.Point(5, -8)
        Me.P2.Name = "P2"
        Me.P2.Size = New System.Drawing.Size(275, 127)
        Me.P2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P2.TabIndex = 38
        Me.P2.TabStop = False
        Me.P2.Visible = False
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(288, 138)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(64, 16)
        Me.Label39.TabIndex = 44
        Me.Label39.Text = "Quantity :"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(285, 33)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(67, 16)
        Me.Label40.TabIndex = 42
        Me.Label40.Text = "Load No. :"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(301, 96)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(51, 16)
        Me.Label41.TabIndex = 43
        Me.Label41.Text = "Driver :"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(280, 75)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(72, 16)
        Me.Label42.TabIndex = 41
        Me.Label42.Text = "Truck No. :"
        '
        'BindingSource3
        '
        Me.BindingSource3.DataMember = "LOADBAY"
        Me.BindingSource3.DataSource = Me.FPTDataSet
        '
        'BindingSource4
        '
        Me.BindingSource4.DataMember = "LOADBAY"
        Me.BindingSource4.DataSource = Me.FPTDataSet
        '
        'BindingSource5
        '
        Me.BindingSource5.DataMember = "LOADBAY"
        Me.BindingSource5.DataSource = Me.FPTDataSet
        '
        'BindingSource6
        '
        Me.BindingSource6.DataMember = "LOADBAY"
        Me.BindingSource6.DataSource = Me.FPTDataSet
        '
        'BindingSource7
        '
        Me.BindingSource7.DataMember = "LOADBAY"
        Me.BindingSource7.DataSource = Me.FPTDataSet
        '
        'BindingSource8
        '
        Me.BindingSource8.DataMember = "LOADBAY"
        Me.BindingSource8.DataSource = Me.FPTDataSet
        '
        'BindingSource9
        '
        Me.BindingSource9.DataMember = "LOADBAY"
        Me.BindingSource9.DataSource = Me.FPTDataSet
        '
        'BindingSource10
        '
        Me.BindingSource10.DataMember = "LOADBAY"
        Me.BindingSource10.DataSource = Me.FPTDataSet
        '
        'BindingSource11
        '
        Me.BindingSource11.DataMember = "LOADBAY"
        Me.BindingSource11.DataSource = Me.FPTDataSet
        '
        'BindingSource12
        '
        Me.BindingSource12.DataMember = "LOADBAY"
        Me.BindingSource12.DataSource = Me.FPTDataSet
        '
        'BindingSource13
        '
        Me.BindingSource13.DataMember = "LOADBAY"
        Me.BindingSource13.DataSource = Me.FPTDataSet
        '
        'RadGroupBox3
        '
        Me.RadGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox3.Controls.Add(Me.RadPanel6)
        Me.RadGroupBox3.Controls.Add(Me.Label43)
        Me.RadGroupBox3.Controls.Add(Me.Label44)
        Me.RadGroupBox3.Controls.Add(Me.Label45)
        Me.RadGroupBox3.Controls.Add(Me.Label46)
        Me.RadGroupBox3.Controls.Add(Me.Label47)
        Me.RadGroupBox3.Controls.Add(Me.Label48)
        Me.RadGroupBox3.Controls.Add(Me.RadPanel7)
        Me.RadGroupBox3.Controls.Add(Me.Label57)
        Me.RadGroupBox3.Controls.Add(Me.Label58)
        Me.RadGroupBox3.Controls.Add(Me.P3)
        Me.RadGroupBox3.Controls.Add(Me.Label59)
        Me.RadGroupBox3.Controls.Add(Me.Label60)
        Me.RadGroupBox3.Controls.Add(Me.Label61)
        Me.RadGroupBox3.Controls.Add(Me.Label62)
        Me.RadGroupBox3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox3.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox3.HeaderText = ""
        Me.RadGroupBox3.Location = New System.Drawing.Point(114, 495)
        Me.RadGroupBox3.Name = "RadGroupBox3"
        Me.RadGroupBox3.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox3.TabIndex = 45
        CType(Me.RadGroupBox3.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox3.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox3.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel6
        '
        Me.RadPanel6.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel6.Name = "RadPanel6"
        '
        '
        '
        Me.RadPanel6.RootElement.Shape = Me.QaShape1
        Me.RadPanel6.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel6.TabIndex = 44
        Me.RadPanel6.Text = "BAY 03"
        Me.RadPanel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 03"
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel6.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "Customer_name", True))
        Me.Label43.ForeColor = System.Drawing.Color.Navy
        Me.Label43.Location = New System.Drawing.Point(358, 54)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(72, 16)
        Me.Label43.TabIndex = 72
        Me.Label43.Text = "Customer :"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "Product_code", True))
        Me.Label44.ForeColor = System.Drawing.Color.Navy
        Me.Label44.Location = New System.Drawing.Point(358, 117)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(63, 16)
        Me.Label44.TabIndex = 71
        Me.Label44.Text = "Material :"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "LOAD_PRESET", True))
        Me.Label45.ForeColor = System.Drawing.Color.Navy
        Me.Label45.Location = New System.Drawing.Point(358, 138)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(64, 16)
        Me.Label45.TabIndex = 70
        Me.Label45.Text = "Quantity :"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "LOAD_DID", True))
        Me.Label46.ForeColor = System.Drawing.Color.Navy
        Me.Label46.Location = New System.Drawing.Point(358, 33)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(67, 16)
        Me.Label46.TabIndex = 68
        Me.Label46.Text = "Load No. :"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "LOAD_DRIVER", True))
        Me.Label47.ForeColor = System.Drawing.Color.Navy
        Me.Label47.Location = New System.Drawing.Point(358, 96)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(51, 16)
        Me.Label47.TabIndex = 69
        Me.Label47.Text = "Driver :"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "LOAD_VEHICLE", True))
        Me.Label48.ForeColor = System.Drawing.Color.Navy
        Me.Label48.Location = New System.Drawing.Point(358, 75)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(72, 16)
        Me.Label48.TabIndex = 67
        Me.Label48.Text = "Truck No. :"
        '
        'RadPanel7
        '
        Me.RadPanel7.Controls.Add(Me.Label49)
        Me.RadPanel7.Controls.Add(Me.Label50)
        Me.RadPanel7.Controls.Add(Me.Label51)
        Me.RadPanel7.Controls.Add(Me.Label52)
        Me.RadPanel7.Controls.Add(Me.RadSeparator2)
        Me.RadPanel7.Controls.Add(Me.Label53)
        Me.RadPanel7.Controls.Add(Me.Label54)
        Me.RadPanel7.Controls.Add(Me.Label55)
        Me.RadPanel7.Controls.Add(Me.Label56)
        Me.RadPanel7.Location = New System.Drawing.Point(5, 118)
        Me.RadPanel7.Name = "RadPanel7"
        '
        '
        '
        Me.RadPanel7.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel7.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel7.TabIndex = 66
        CType(Me.RadPanel7.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel7.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel7.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel7.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel7.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel7.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.ForeColor = System.Drawing.Color.Black
        Me.Label49.Location = New System.Drawing.Point(200, 20)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(23, 13)
        Me.Label49.TabIndex = 70
        Me.Label49.Text = "Kg."
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "Raw_Weight_Out", True))
        Me.Label50.ForeColor = System.Drawing.Color.Navy
        Me.Label50.Location = New System.Drawing.Point(150, 20)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(45, 13)
        Me.Label50.TabIndex = 68
        Me.Label50.Text = "Weight"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "WeightOut_time", True))
        Me.Label51.ForeColor = System.Drawing.Color.Navy
        Me.Label51.Location = New System.Drawing.Point(226, 20)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(30, 13)
        Me.Label51.TabIndex = 69
        Me.Label51.Text = "Time"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.ForeColor = System.Drawing.Color.Black
        Me.Label52.Location = New System.Drawing.Point(65, 20)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(23, 13)
        Me.Label52.TabIndex = 67
        Me.Label52.Text = "Kg."
        '
        'RadSeparator2
        '
        Me.RadSeparator2.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator2.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator2.Name = "RadSeparator2"
        Me.RadSeparator2.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator2.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator2.TabIndex = 66
        Me.RadSeparator2.Text = "RadSeparator2"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(66, 0)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(21, 16)
        Me.Label53.TabIndex = 61
        Me.Label53.Text = "In"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "Raw_Weight_in", True))
        Me.Label54.ForeColor = System.Drawing.Color.Navy
        Me.Label54.Location = New System.Drawing.Point(15, 20)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(45, 13)
        Me.Label54.TabIndex = 60
        Me.Label54.Text = "Weight"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(195, 1)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(31, 16)
        Me.Label55.TabIndex = 62
        Me.Label55.Text = "Out"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource3, "Weightin_time", True))
        Me.Label56.ForeColor = System.Drawing.Color.Navy
        Me.Label56.Location = New System.Drawing.Point(91, 20)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(30, 13)
        Me.Label56.TabIndex = 63
        Me.Label56.Text = "Time"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(280, 54)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(72, 16)
        Me.Label57.TabIndex = 51
        Me.Label57.Text = "Customer :"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(289, 117)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(63, 16)
        Me.Label58.TabIndex = 45
        Me.Label58.Text = "Material :"
        '
        'P3
        '
        Me.P3.ErrorImage = Nothing
        Me.P3.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P3.Location = New System.Drawing.Point(5, -8)
        Me.P3.Name = "P3"
        Me.P3.Size = New System.Drawing.Size(275, 127)
        Me.P3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P3.TabIndex = 38
        Me.P3.TabStop = False
        Me.P3.Visible = False
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(288, 138)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(64, 16)
        Me.Label59.TabIndex = 44
        Me.Label59.Text = "Quantity :"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(285, 33)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(67, 16)
        Me.Label60.TabIndex = 42
        Me.Label60.Text = "Load No. :"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(301, 96)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(51, 16)
        Me.Label61.TabIndex = 43
        Me.Label61.Text = "Driver :"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(280, 75)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(72, 16)
        Me.Label62.TabIndex = 41
        Me.Label62.Text = "Truck No. :"
        '
        'RadGroupBox4
        '
        Me.RadGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox4.Controls.Add(Me.RadPanel8)
        Me.RadGroupBox4.Controls.Add(Me.Label63)
        Me.RadGroupBox4.Controls.Add(Me.Label64)
        Me.RadGroupBox4.Controls.Add(Me.Label65)
        Me.RadGroupBox4.Controls.Add(Me.Label66)
        Me.RadGroupBox4.Controls.Add(Me.Label67)
        Me.RadGroupBox4.Controls.Add(Me.Label68)
        Me.RadGroupBox4.Controls.Add(Me.RadPanel9)
        Me.RadGroupBox4.Controls.Add(Me.Label77)
        Me.RadGroupBox4.Controls.Add(Me.Label78)
        Me.RadGroupBox4.Controls.Add(Me.P4)
        Me.RadGroupBox4.Controls.Add(Me.Label79)
        Me.RadGroupBox4.Controls.Add(Me.Label80)
        Me.RadGroupBox4.Controls.Add(Me.Label81)
        Me.RadGroupBox4.Controls.Add(Me.Label82)
        Me.RadGroupBox4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox4.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox4.HeaderText = ""
        Me.RadGroupBox4.Location = New System.Drawing.Point(114, 664)
        Me.RadGroupBox4.Name = "RadGroupBox4"
        Me.RadGroupBox4.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox4.TabIndex = 46
        CType(Me.RadGroupBox4.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox4.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox4.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel8
        '
        Me.RadPanel8.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel8.Name = "RadPanel8"
        '
        '
        '
        Me.RadPanel8.RootElement.Shape = Me.QaShape1
        Me.RadPanel8.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel8.TabIndex = 44
        Me.RadPanel8.Text = "BAY 04"
        Me.RadPanel8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel8.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 04"
        CType(Me.RadPanel8.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel8.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel8.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel8.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel8.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel8.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel8.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel8.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "Customer_name", True))
        Me.Label63.ForeColor = System.Drawing.Color.Navy
        Me.Label63.Location = New System.Drawing.Point(358, 54)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(72, 16)
        Me.Label63.TabIndex = 72
        Me.Label63.Text = "Customer :"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "Product_code", True))
        Me.Label64.ForeColor = System.Drawing.Color.Navy
        Me.Label64.Location = New System.Drawing.Point(358, 117)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(63, 16)
        Me.Label64.TabIndex = 71
        Me.Label64.Text = "Material :"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "LOAD_PRESET", True))
        Me.Label65.ForeColor = System.Drawing.Color.Navy
        Me.Label65.Location = New System.Drawing.Point(358, 138)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(64, 16)
        Me.Label65.TabIndex = 70
        Me.Label65.Text = "Quantity :"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "LOAD_DID", True))
        Me.Label66.ForeColor = System.Drawing.Color.Navy
        Me.Label66.Location = New System.Drawing.Point(358, 33)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(67, 16)
        Me.Label66.TabIndex = 68
        Me.Label66.Text = "Load No. :"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "LOAD_DRIVER", True))
        Me.Label67.ForeColor = System.Drawing.Color.Navy
        Me.Label67.Location = New System.Drawing.Point(358, 96)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(51, 16)
        Me.Label67.TabIndex = 69
        Me.Label67.Text = "Driver :"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "LOAD_VEHICLE", True))
        Me.Label68.ForeColor = System.Drawing.Color.Navy
        Me.Label68.Location = New System.Drawing.Point(358, 75)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(72, 16)
        Me.Label68.TabIndex = 67
        Me.Label68.Text = "Truck No. :"
        '
        'RadPanel9
        '
        Me.RadPanel9.Controls.Add(Me.Label69)
        Me.RadPanel9.Controls.Add(Me.Label70)
        Me.RadPanel9.Controls.Add(Me.Label71)
        Me.RadPanel9.Controls.Add(Me.Label72)
        Me.RadPanel9.Controls.Add(Me.RadSeparator3)
        Me.RadPanel9.Controls.Add(Me.Label73)
        Me.RadPanel9.Controls.Add(Me.Label74)
        Me.RadPanel9.Controls.Add(Me.Label75)
        Me.RadPanel9.Controls.Add(Me.Label76)
        Me.RadPanel9.Location = New System.Drawing.Point(5, 117)
        Me.RadPanel9.Name = "RadPanel9"
        '
        '
        '
        Me.RadPanel9.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel9.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel9.TabIndex = 66
        CType(Me.RadPanel9.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel9.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel9.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel9.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel9.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel9.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.ForeColor = System.Drawing.Color.Black
        Me.Label69.Location = New System.Drawing.Point(200, 20)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(23, 13)
        Me.Label69.TabIndex = 70
        Me.Label69.Text = "Kg."
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "Raw_Weight_Out", True))
        Me.Label70.ForeColor = System.Drawing.Color.Navy
        Me.Label70.Location = New System.Drawing.Point(150, 20)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(45, 13)
        Me.Label70.TabIndex = 68
        Me.Label70.Text = "Weight"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "WeightOut_time", True))
        Me.Label71.ForeColor = System.Drawing.Color.Navy
        Me.Label71.Location = New System.Drawing.Point(226, 20)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(30, 13)
        Me.Label71.TabIndex = 69
        Me.Label71.Text = "Time"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.ForeColor = System.Drawing.Color.Black
        Me.Label72.Location = New System.Drawing.Point(65, 20)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(23, 13)
        Me.Label72.TabIndex = 67
        Me.Label72.Text = "Kg."
        '
        'RadSeparator3
        '
        Me.RadSeparator3.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator3.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator3.Name = "RadSeparator3"
        Me.RadSeparator3.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator3.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator3.TabIndex = 66
        Me.RadSeparator3.Text = "RadSeparator3"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.Location = New System.Drawing.Point(66, 0)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(21, 16)
        Me.Label73.TabIndex = 61
        Me.Label73.Text = "In"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "Raw_Weight_in", True))
        Me.Label74.ForeColor = System.Drawing.Color.Navy
        Me.Label74.Location = New System.Drawing.Point(15, 20)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(45, 13)
        Me.Label74.TabIndex = 60
        Me.Label74.Text = "Weight"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.Location = New System.Drawing.Point(195, 1)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(31, 16)
        Me.Label75.TabIndex = 62
        Me.Label75.Text = "Out"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource4, "Weightin_time", True))
        Me.Label76.ForeColor = System.Drawing.Color.Navy
        Me.Label76.Location = New System.Drawing.Point(91, 20)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(30, 13)
        Me.Label76.TabIndex = 63
        Me.Label76.Text = "Time"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(280, 54)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(72, 16)
        Me.Label77.TabIndex = 51
        Me.Label77.Text = "Customer :"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(289, 117)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(63, 16)
        Me.Label78.TabIndex = 45
        Me.Label78.Text = "Material :"
        '
        'P4
        '
        Me.P4.ErrorImage = Nothing
        Me.P4.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P4.Location = New System.Drawing.Point(5, -8)
        Me.P4.Name = "P4"
        Me.P4.Size = New System.Drawing.Size(275, 127)
        Me.P4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P4.TabIndex = 38
        Me.P4.TabStop = False
        Me.P4.Visible = False
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(288, 138)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(64, 16)
        Me.Label79.TabIndex = 44
        Me.Label79.Text = "Quantity :"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(285, 33)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(67, 16)
        Me.Label80.TabIndex = 42
        Me.Label80.Text = "Load No. :"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(301, 96)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(51, 16)
        Me.Label81.TabIndex = 43
        Me.Label81.Text = "Driver :"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(280, 75)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(72, 16)
        Me.Label82.TabIndex = 41
        Me.Label82.Text = "Truck No. :"
        '
        'RadGroupBox5
        '
        Me.RadGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox5.Controls.Add(Me.RadPanel10)
        Me.RadGroupBox5.Controls.Add(Me.Label83)
        Me.RadGroupBox5.Controls.Add(Me.Label84)
        Me.RadGroupBox5.Controls.Add(Me.Label85)
        Me.RadGroupBox5.Controls.Add(Me.Label86)
        Me.RadGroupBox5.Controls.Add(Me.Label87)
        Me.RadGroupBox5.Controls.Add(Me.Label88)
        Me.RadGroupBox5.Controls.Add(Me.RadPanel11)
        Me.RadGroupBox5.Controls.Add(Me.Label97)
        Me.RadGroupBox5.Controls.Add(Me.Label98)
        Me.RadGroupBox5.Controls.Add(Me.P5)
        Me.RadGroupBox5.Controls.Add(Me.Label99)
        Me.RadGroupBox5.Controls.Add(Me.Label100)
        Me.RadGroupBox5.Controls.Add(Me.Label101)
        Me.RadGroupBox5.Controls.Add(Me.Label102)
        Me.RadGroupBox5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox5.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox5.HeaderText = ""
        Me.RadGroupBox5.Location = New System.Drawing.Point(114, 833)
        Me.RadGroupBox5.Name = "RadGroupBox5"
        Me.RadGroupBox5.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox5.TabIndex = 47
        CType(Me.RadGroupBox5.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox5.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox5.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel10
        '
        Me.RadPanel10.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel10.Name = "RadPanel10"
        '
        '
        '
        Me.RadPanel10.RootElement.Shape = Me.QaShape1
        Me.RadPanel10.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel10.TabIndex = 44
        Me.RadPanel10.Text = "BAY 05"
        Me.RadPanel10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel10.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 05"
        CType(Me.RadPanel10.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel10.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel10.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel10.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel10.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel10.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel10.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel10.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "Customer_name", True))
        Me.Label83.ForeColor = System.Drawing.Color.Navy
        Me.Label83.Location = New System.Drawing.Point(358, 54)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(72, 16)
        Me.Label83.TabIndex = 72
        Me.Label83.Text = "Customer :"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "Product_code", True))
        Me.Label84.ForeColor = System.Drawing.Color.Navy
        Me.Label84.Location = New System.Drawing.Point(358, 117)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(63, 16)
        Me.Label84.TabIndex = 71
        Me.Label84.Text = "Material :"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "LOAD_PRESET", True))
        Me.Label85.ForeColor = System.Drawing.Color.Navy
        Me.Label85.Location = New System.Drawing.Point(358, 138)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(64, 16)
        Me.Label85.TabIndex = 70
        Me.Label85.Text = "Quantity :"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "LOAD_DID", True))
        Me.Label86.ForeColor = System.Drawing.Color.Navy
        Me.Label86.Location = New System.Drawing.Point(358, 33)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(67, 16)
        Me.Label86.TabIndex = 68
        Me.Label86.Text = "Load No. :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "LOAD_DRIVER", True))
        Me.Label87.ForeColor = System.Drawing.Color.Navy
        Me.Label87.Location = New System.Drawing.Point(358, 96)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(51, 16)
        Me.Label87.TabIndex = 69
        Me.Label87.Text = "Driver :"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "LOAD_VEHICLE", True))
        Me.Label88.ForeColor = System.Drawing.Color.Navy
        Me.Label88.Location = New System.Drawing.Point(358, 75)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(72, 16)
        Me.Label88.TabIndex = 67
        Me.Label88.Text = "Truck No. :"
        '
        'RadPanel11
        '
        Me.RadPanel11.Controls.Add(Me.Label89)
        Me.RadPanel11.Controls.Add(Me.Label90)
        Me.RadPanel11.Controls.Add(Me.Label91)
        Me.RadPanel11.Controls.Add(Me.Label92)
        Me.RadPanel11.Controls.Add(Me.RadSeparator4)
        Me.RadPanel11.Controls.Add(Me.Label93)
        Me.RadPanel11.Controls.Add(Me.Label94)
        Me.RadPanel11.Controls.Add(Me.Label95)
        Me.RadPanel11.Controls.Add(Me.Label96)
        Me.RadPanel11.Location = New System.Drawing.Point(5, 116)
        Me.RadPanel11.Name = "RadPanel11"
        '
        '
        '
        Me.RadPanel11.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel11.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel11.TabIndex = 66
        CType(Me.RadPanel11.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel11.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel11.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel11.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel11.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel11.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.ForeColor = System.Drawing.Color.Black
        Me.Label89.Location = New System.Drawing.Point(200, 20)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(23, 13)
        Me.Label89.TabIndex = 70
        Me.Label89.Text = "Kg."
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "Raw_Weight_Out", True))
        Me.Label90.ForeColor = System.Drawing.Color.Navy
        Me.Label90.Location = New System.Drawing.Point(150, 20)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(45, 13)
        Me.Label90.TabIndex = 68
        Me.Label90.Text = "Weight"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "WeightOut_time", True))
        Me.Label91.ForeColor = System.Drawing.Color.Navy
        Me.Label91.Location = New System.Drawing.Point(226, 20)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(30, 13)
        Me.Label91.TabIndex = 69
        Me.Label91.Text = "Time"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.ForeColor = System.Drawing.Color.Black
        Me.Label92.Location = New System.Drawing.Point(65, 20)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(23, 13)
        Me.Label92.TabIndex = 67
        Me.Label92.Text = "Kg."
        '
        'RadSeparator4
        '
        Me.RadSeparator4.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator4.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator4.Name = "RadSeparator4"
        Me.RadSeparator4.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator4.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator4.TabIndex = 66
        Me.RadSeparator4.Text = "RadSeparator4"
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.Location = New System.Drawing.Point(66, 0)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(21, 16)
        Me.Label93.TabIndex = 61
        Me.Label93.Text = "In"
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "Raw_Weight_in", True))
        Me.Label94.ForeColor = System.Drawing.Color.Navy
        Me.Label94.Location = New System.Drawing.Point(15, 20)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(45, 13)
        Me.Label94.TabIndex = 60
        Me.Label94.Text = "Weight"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label95.Location = New System.Drawing.Point(195, 1)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(31, 16)
        Me.Label95.TabIndex = 62
        Me.Label95.Text = "Out"
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource5, "Weightin_time", True))
        Me.Label96.ForeColor = System.Drawing.Color.Navy
        Me.Label96.Location = New System.Drawing.Point(91, 20)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(30, 13)
        Me.Label96.TabIndex = 63
        Me.Label96.Text = "Time"
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(280, 54)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(72, 16)
        Me.Label97.TabIndex = 51
        Me.Label97.Text = "Customer :"
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(289, 117)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(63, 16)
        Me.Label98.TabIndex = 45
        Me.Label98.Text = "Material :"
        '
        'P5
        '
        Me.P5.ErrorImage = Nothing
        Me.P5.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P5.Location = New System.Drawing.Point(5, -8)
        Me.P5.Name = "P5"
        Me.P5.Size = New System.Drawing.Size(275, 127)
        Me.P5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P5.TabIndex = 38
        Me.P5.TabStop = False
        Me.P5.Visible = False
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(288, 138)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(64, 16)
        Me.Label99.TabIndex = 44
        Me.Label99.Text = "Quantity :"
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(285, 33)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(67, 16)
        Me.Label100.TabIndex = 42
        Me.Label100.Text = "Load No. :"
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Location = New System.Drawing.Point(301, 96)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(51, 16)
        Me.Label101.TabIndex = 43
        Me.Label101.Text = "Driver :"
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Location = New System.Drawing.Point(280, 75)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(72, 16)
        Me.Label102.TabIndex = 41
        Me.Label102.Text = "Truck No. :"
        '
        'RadGroupBox6
        '
        Me.RadGroupBox6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox6.Controls.Add(Me.RadPanel12)
        Me.RadGroupBox6.Controls.Add(Me.Label103)
        Me.RadGroupBox6.Controls.Add(Me.Label104)
        Me.RadGroupBox6.Controls.Add(Me.Label105)
        Me.RadGroupBox6.Controls.Add(Me.Label106)
        Me.RadGroupBox6.Controls.Add(Me.Label107)
        Me.RadGroupBox6.Controls.Add(Me.Label108)
        Me.RadGroupBox6.Controls.Add(Me.RadPanel13)
        Me.RadGroupBox6.Controls.Add(Me.Label117)
        Me.RadGroupBox6.Controls.Add(Me.Label118)
        Me.RadGroupBox6.Controls.Add(Me.P6)
        Me.RadGroupBox6.Controls.Add(Me.Label119)
        Me.RadGroupBox6.Controls.Add(Me.Label120)
        Me.RadGroupBox6.Controls.Add(Me.Label121)
        Me.RadGroupBox6.Controls.Add(Me.Label122)
        Me.RadGroupBox6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox6.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox6.HeaderText = ""
        Me.RadGroupBox6.Location = New System.Drawing.Point(685, 157)
        Me.RadGroupBox6.Name = "RadGroupBox6"
        Me.RadGroupBox6.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox6.TabIndex = 48
        CType(Me.RadGroupBox6.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox6.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox6.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel12
        '
        Me.RadPanel12.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel12.Name = "RadPanel12"
        '
        '
        '
        Me.RadPanel12.RootElement.Shape = Me.QaShape1
        Me.RadPanel12.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel12.TabIndex = 44
        Me.RadPanel12.Text = "BAY 06"
        Me.RadPanel12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel12.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 06"
        CType(Me.RadPanel12.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel12.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel12.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel12.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel12.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel12.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel12.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel12.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "Customer_name", True))
        Me.Label103.ForeColor = System.Drawing.Color.Navy
        Me.Label103.Location = New System.Drawing.Point(358, 54)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(72, 16)
        Me.Label103.TabIndex = 72
        Me.Label103.Text = "Customer :"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "Product_code", True))
        Me.Label104.ForeColor = System.Drawing.Color.Navy
        Me.Label104.Location = New System.Drawing.Point(358, 117)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(63, 16)
        Me.Label104.TabIndex = 71
        Me.Label104.Text = "Material :"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "LOAD_PRESET", True))
        Me.Label105.ForeColor = System.Drawing.Color.Navy
        Me.Label105.Location = New System.Drawing.Point(358, 138)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(64, 16)
        Me.Label105.TabIndex = 70
        Me.Label105.Text = "Quantity :"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "LOAD_DID", True))
        Me.Label106.ForeColor = System.Drawing.Color.Navy
        Me.Label106.Location = New System.Drawing.Point(358, 33)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(67, 16)
        Me.Label106.TabIndex = 68
        Me.Label106.Text = "Load No. :"
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "LOAD_DRIVER", True))
        Me.Label107.ForeColor = System.Drawing.Color.Navy
        Me.Label107.Location = New System.Drawing.Point(358, 96)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(51, 16)
        Me.Label107.TabIndex = 69
        Me.Label107.Text = "Driver :"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "LOAD_VEHICLE", True))
        Me.Label108.ForeColor = System.Drawing.Color.Navy
        Me.Label108.Location = New System.Drawing.Point(358, 75)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(72, 16)
        Me.Label108.TabIndex = 67
        Me.Label108.Text = "Truck No. :"
        '
        'RadPanel13
        '
        Me.RadPanel13.Controls.Add(Me.Label109)
        Me.RadPanel13.Controls.Add(Me.Label110)
        Me.RadPanel13.Controls.Add(Me.Label111)
        Me.RadPanel13.Controls.Add(Me.Label112)
        Me.RadPanel13.Controls.Add(Me.RadSeparator5)
        Me.RadPanel13.Controls.Add(Me.Label113)
        Me.RadPanel13.Controls.Add(Me.Label114)
        Me.RadPanel13.Controls.Add(Me.Label115)
        Me.RadPanel13.Controls.Add(Me.Label116)
        Me.RadPanel13.Location = New System.Drawing.Point(5, 117)
        Me.RadPanel13.Name = "RadPanel13"
        '
        '
        '
        Me.RadPanel13.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel13.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel13.TabIndex = 66
        CType(Me.RadPanel13.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel13.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel13.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel13.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel13.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel13.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.ForeColor = System.Drawing.Color.Black
        Me.Label109.Location = New System.Drawing.Point(200, 20)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(23, 13)
        Me.Label109.TabIndex = 70
        Me.Label109.Text = "Kg."
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "Raw_Weight_Out", True))
        Me.Label110.ForeColor = System.Drawing.Color.Navy
        Me.Label110.Location = New System.Drawing.Point(150, 20)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(45, 13)
        Me.Label110.TabIndex = 68
        Me.Label110.Text = "Weight"
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "WeightOut_time", True))
        Me.Label111.ForeColor = System.Drawing.Color.Navy
        Me.Label111.Location = New System.Drawing.Point(226, 20)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(30, 13)
        Me.Label111.TabIndex = 69
        Me.Label111.Text = "Time"
        '
        'Label112
        '
        Me.Label112.AutoSize = True
        Me.Label112.ForeColor = System.Drawing.Color.Black
        Me.Label112.Location = New System.Drawing.Point(65, 20)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(23, 13)
        Me.Label112.TabIndex = 67
        Me.Label112.Text = "Kg."
        '
        'RadSeparator5
        '
        Me.RadSeparator5.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator5.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator5.Name = "RadSeparator5"
        Me.RadSeparator5.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator5.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator5.TabIndex = 66
        Me.RadSeparator5.Text = "RadSeparator5"
        '
        'Label113
        '
        Me.Label113.AutoSize = True
        Me.Label113.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label113.Location = New System.Drawing.Point(66, 0)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(21, 16)
        Me.Label113.TabIndex = 61
        Me.Label113.Text = "In"
        '
        'Label114
        '
        Me.Label114.AutoSize = True
        Me.Label114.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "Raw_Weight_in", True))
        Me.Label114.ForeColor = System.Drawing.Color.Navy
        Me.Label114.Location = New System.Drawing.Point(15, 20)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(45, 13)
        Me.Label114.TabIndex = 60
        Me.Label114.Text = "Weight"
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label115.Location = New System.Drawing.Point(195, 1)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(31, 16)
        Me.Label115.TabIndex = 62
        Me.Label115.Text = "Out"
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource6, "Weightin_time", True))
        Me.Label116.ForeColor = System.Drawing.Color.Navy
        Me.Label116.Location = New System.Drawing.Point(91, 20)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(30, 13)
        Me.Label116.TabIndex = 63
        Me.Label116.Text = "Time"
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(280, 54)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(72, 16)
        Me.Label117.TabIndex = 51
        Me.Label117.Text = "Customer :"
        '
        'Label118
        '
        Me.Label118.AutoSize = True
        Me.Label118.Location = New System.Drawing.Point(289, 117)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(63, 16)
        Me.Label118.TabIndex = 45
        Me.Label118.Text = "Material :"
        '
        'P6
        '
        Me.P6.ErrorImage = Nothing
        Me.P6.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P6.Location = New System.Drawing.Point(5, -8)
        Me.P6.Name = "P6"
        Me.P6.Size = New System.Drawing.Size(275, 127)
        Me.P6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P6.TabIndex = 38
        Me.P6.TabStop = False
        Me.P6.Visible = False
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(288, 138)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(64, 16)
        Me.Label119.TabIndex = 44
        Me.Label119.Text = "Quantity :"
        '
        'Label120
        '
        Me.Label120.AutoSize = True
        Me.Label120.Location = New System.Drawing.Point(285, 33)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(67, 16)
        Me.Label120.TabIndex = 42
        Me.Label120.Text = "Load No. :"
        '
        'Label121
        '
        Me.Label121.AutoSize = True
        Me.Label121.Location = New System.Drawing.Point(301, 96)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(51, 16)
        Me.Label121.TabIndex = 43
        Me.Label121.Text = "Driver :"
        '
        'Label122
        '
        Me.Label122.AutoSize = True
        Me.Label122.Location = New System.Drawing.Point(280, 75)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(72, 16)
        Me.Label122.TabIndex = 41
        Me.Label122.Text = "Truck No. :"
        '
        'RadGroupBox7
        '
        Me.RadGroupBox7.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox7.Controls.Add(Me.RadPanel14)
        Me.RadGroupBox7.Controls.Add(Me.Label123)
        Me.RadGroupBox7.Controls.Add(Me.Label124)
        Me.RadGroupBox7.Controls.Add(Me.Label125)
        Me.RadGroupBox7.Controls.Add(Me.Label126)
        Me.RadGroupBox7.Controls.Add(Me.Label127)
        Me.RadGroupBox7.Controls.Add(Me.Label128)
        Me.RadGroupBox7.Controls.Add(Me.RadPanel15)
        Me.RadGroupBox7.Controls.Add(Me.Label137)
        Me.RadGroupBox7.Controls.Add(Me.Label138)
        Me.RadGroupBox7.Controls.Add(Me.P7)
        Me.RadGroupBox7.Controls.Add(Me.Label139)
        Me.RadGroupBox7.Controls.Add(Me.Label140)
        Me.RadGroupBox7.Controls.Add(Me.Label141)
        Me.RadGroupBox7.Controls.Add(Me.Label142)
        Me.RadGroupBox7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox7.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox7.HeaderText = ""
        Me.RadGroupBox7.Location = New System.Drawing.Point(685, 326)
        Me.RadGroupBox7.Name = "RadGroupBox7"
        Me.RadGroupBox7.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox7.TabIndex = 49
        CType(Me.RadGroupBox7.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox7.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox7.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel14
        '
        Me.RadPanel14.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel14.Name = "RadPanel14"
        '
        '
        '
        Me.RadPanel14.RootElement.Shape = Me.QaShape1
        Me.RadPanel14.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel14.TabIndex = 44
        Me.RadPanel14.Text = "BAY 07"
        Me.RadPanel14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel14.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 07"
        CType(Me.RadPanel14.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel14.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel14.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel14.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel14.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel14.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel14.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel14.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label123
        '
        Me.Label123.AutoSize = True
        Me.Label123.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "Customer_name", True))
        Me.Label123.ForeColor = System.Drawing.Color.Navy
        Me.Label123.Location = New System.Drawing.Point(358, 54)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(72, 16)
        Me.Label123.TabIndex = 72
        Me.Label123.Text = "Customer :"
        '
        'Label124
        '
        Me.Label124.AutoSize = True
        Me.Label124.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "Product_code", True))
        Me.Label124.ForeColor = System.Drawing.Color.Navy
        Me.Label124.Location = New System.Drawing.Point(358, 117)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(63, 16)
        Me.Label124.TabIndex = 71
        Me.Label124.Text = "Material :"
        '
        'Label125
        '
        Me.Label125.AutoSize = True
        Me.Label125.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "LOAD_PRESET", True))
        Me.Label125.ForeColor = System.Drawing.Color.Navy
        Me.Label125.Location = New System.Drawing.Point(358, 138)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(64, 16)
        Me.Label125.TabIndex = 70
        Me.Label125.Text = "Quantity :"
        '
        'Label126
        '
        Me.Label126.AutoSize = True
        Me.Label126.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "LOAD_DID", True))
        Me.Label126.ForeColor = System.Drawing.Color.Navy
        Me.Label126.Location = New System.Drawing.Point(358, 33)
        Me.Label126.Name = "Label126"
        Me.Label126.Size = New System.Drawing.Size(67, 16)
        Me.Label126.TabIndex = 68
        Me.Label126.Text = "Load No. :"
        '
        'Label127
        '
        Me.Label127.AutoSize = True
        Me.Label127.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "LOAD_DRIVER", True))
        Me.Label127.ForeColor = System.Drawing.Color.Navy
        Me.Label127.Location = New System.Drawing.Point(358, 96)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(51, 16)
        Me.Label127.TabIndex = 69
        Me.Label127.Text = "Driver :"
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "LOAD_VEHICLE", True))
        Me.Label128.ForeColor = System.Drawing.Color.Navy
        Me.Label128.Location = New System.Drawing.Point(358, 75)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(72, 16)
        Me.Label128.TabIndex = 67
        Me.Label128.Text = "Truck No. :"
        '
        'RadPanel15
        '
        Me.RadPanel15.Controls.Add(Me.Label129)
        Me.RadPanel15.Controls.Add(Me.Label130)
        Me.RadPanel15.Controls.Add(Me.Label131)
        Me.RadPanel15.Controls.Add(Me.Label132)
        Me.RadPanel15.Controls.Add(Me.RadSeparator6)
        Me.RadPanel15.Controls.Add(Me.Label133)
        Me.RadPanel15.Controls.Add(Me.Label134)
        Me.RadPanel15.Controls.Add(Me.Label135)
        Me.RadPanel15.Controls.Add(Me.Label136)
        Me.RadPanel15.Location = New System.Drawing.Point(5, 117)
        Me.RadPanel15.Name = "RadPanel15"
        '
        '
        '
        Me.RadPanel15.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel15.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel15.TabIndex = 66
        CType(Me.RadPanel15.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel15.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel15.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel15.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel15.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel15.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label129
        '
        Me.Label129.AutoSize = True
        Me.Label129.ForeColor = System.Drawing.Color.Black
        Me.Label129.Location = New System.Drawing.Point(200, 20)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(23, 13)
        Me.Label129.TabIndex = 70
        Me.Label129.Text = "Kg."
        '
        'Label130
        '
        Me.Label130.AutoSize = True
        Me.Label130.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "Raw_Weight_Out", True))
        Me.Label130.ForeColor = System.Drawing.Color.Navy
        Me.Label130.Location = New System.Drawing.Point(150, 20)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(45, 13)
        Me.Label130.TabIndex = 68
        Me.Label130.Text = "Weight"
        '
        'Label131
        '
        Me.Label131.AutoSize = True
        Me.Label131.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "WeightOut_time", True))
        Me.Label131.ForeColor = System.Drawing.Color.Navy
        Me.Label131.Location = New System.Drawing.Point(226, 20)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(30, 13)
        Me.Label131.TabIndex = 69
        Me.Label131.Text = "Time"
        '
        'Label132
        '
        Me.Label132.AutoSize = True
        Me.Label132.ForeColor = System.Drawing.Color.Black
        Me.Label132.Location = New System.Drawing.Point(65, 20)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(23, 13)
        Me.Label132.TabIndex = 67
        Me.Label132.Text = "Kg."
        '
        'RadSeparator6
        '
        Me.RadSeparator6.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator6.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator6.Name = "RadSeparator6"
        Me.RadSeparator6.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator6.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator6.TabIndex = 66
        Me.RadSeparator6.Text = "RadSeparator6"
        '
        'Label133
        '
        Me.Label133.AutoSize = True
        Me.Label133.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label133.Location = New System.Drawing.Point(66, 0)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(21, 16)
        Me.Label133.TabIndex = 61
        Me.Label133.Text = "In"
        '
        'Label134
        '
        Me.Label134.AutoSize = True
        Me.Label134.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "Raw_Weight_in", True))
        Me.Label134.ForeColor = System.Drawing.Color.Navy
        Me.Label134.Location = New System.Drawing.Point(15, 20)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(45, 13)
        Me.Label134.TabIndex = 60
        Me.Label134.Text = "Weight"
        '
        'Label135
        '
        Me.Label135.AutoSize = True
        Me.Label135.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label135.Location = New System.Drawing.Point(195, 1)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(31, 16)
        Me.Label135.TabIndex = 62
        Me.Label135.Text = "Out"
        '
        'Label136
        '
        Me.Label136.AutoSize = True
        Me.Label136.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource7, "Weightin_time", True))
        Me.Label136.ForeColor = System.Drawing.Color.Navy
        Me.Label136.Location = New System.Drawing.Point(91, 20)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(30, 13)
        Me.Label136.TabIndex = 63
        Me.Label136.Text = "Time"
        '
        'Label137
        '
        Me.Label137.AutoSize = True
        Me.Label137.Location = New System.Drawing.Point(280, 54)
        Me.Label137.Name = "Label137"
        Me.Label137.Size = New System.Drawing.Size(72, 16)
        Me.Label137.TabIndex = 51
        Me.Label137.Text = "Customer :"
        '
        'Label138
        '
        Me.Label138.AutoSize = True
        Me.Label138.Location = New System.Drawing.Point(289, 117)
        Me.Label138.Name = "Label138"
        Me.Label138.Size = New System.Drawing.Size(63, 16)
        Me.Label138.TabIndex = 45
        Me.Label138.Text = "Material :"
        '
        'P7
        '
        Me.P7.ErrorImage = Nothing
        Me.P7.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P7.Location = New System.Drawing.Point(5, -8)
        Me.P7.Name = "P7"
        Me.P7.Size = New System.Drawing.Size(275, 127)
        Me.P7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P7.TabIndex = 38
        Me.P7.TabStop = False
        Me.P7.Visible = False
        '
        'Label139
        '
        Me.Label139.AutoSize = True
        Me.Label139.Location = New System.Drawing.Point(288, 138)
        Me.Label139.Name = "Label139"
        Me.Label139.Size = New System.Drawing.Size(64, 16)
        Me.Label139.TabIndex = 44
        Me.Label139.Text = "Quantity :"
        '
        'Label140
        '
        Me.Label140.AutoSize = True
        Me.Label140.Location = New System.Drawing.Point(285, 33)
        Me.Label140.Name = "Label140"
        Me.Label140.Size = New System.Drawing.Size(67, 16)
        Me.Label140.TabIndex = 42
        Me.Label140.Text = "Load No. :"
        '
        'Label141
        '
        Me.Label141.AutoSize = True
        Me.Label141.Location = New System.Drawing.Point(301, 96)
        Me.Label141.Name = "Label141"
        Me.Label141.Size = New System.Drawing.Size(51, 16)
        Me.Label141.TabIndex = 43
        Me.Label141.Text = "Driver :"
        '
        'Label142
        '
        Me.Label142.AutoSize = True
        Me.Label142.Location = New System.Drawing.Point(280, 75)
        Me.Label142.Name = "Label142"
        Me.Label142.Size = New System.Drawing.Size(72, 16)
        Me.Label142.TabIndex = 41
        Me.Label142.Text = "Truck No. :"
        '
        'RadGroupBox8
        '
        Me.RadGroupBox8.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox8.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox8.Controls.Add(Me.RadPanel16)
        Me.RadGroupBox8.Controls.Add(Me.Label143)
        Me.RadGroupBox8.Controls.Add(Me.Label144)
        Me.RadGroupBox8.Controls.Add(Me.Label145)
        Me.RadGroupBox8.Controls.Add(Me.Label146)
        Me.RadGroupBox8.Controls.Add(Me.Label147)
        Me.RadGroupBox8.Controls.Add(Me.Label148)
        Me.RadGroupBox8.Controls.Add(Me.RadPanel17)
        Me.RadGroupBox8.Controls.Add(Me.Label157)
        Me.RadGroupBox8.Controls.Add(Me.Label158)
        Me.RadGroupBox8.Controls.Add(Me.P8)
        Me.RadGroupBox8.Controls.Add(Me.Label159)
        Me.RadGroupBox8.Controls.Add(Me.Label160)
        Me.RadGroupBox8.Controls.Add(Me.Label161)
        Me.RadGroupBox8.Controls.Add(Me.Label162)
        Me.RadGroupBox8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox8.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox8.HeaderText = ""
        Me.RadGroupBox8.Location = New System.Drawing.Point(685, 495)
        Me.RadGroupBox8.Name = "RadGroupBox8"
        Me.RadGroupBox8.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox8.TabIndex = 73
        CType(Me.RadGroupBox8.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox8.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox8.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel16
        '
        Me.RadPanel16.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel16.Name = "RadPanel16"
        '
        '
        '
        Me.RadPanel16.RootElement.Shape = Me.QaShape1
        Me.RadPanel16.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel16.TabIndex = 44
        Me.RadPanel16.Text = "BAY 08"
        Me.RadPanel16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel16.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 08"
        CType(Me.RadPanel16.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel16.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel16.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel16.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel16.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel16.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel16.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel16.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label143
        '
        Me.Label143.AutoSize = True
        Me.Label143.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "Customer_name", True))
        Me.Label143.ForeColor = System.Drawing.Color.Navy
        Me.Label143.Location = New System.Drawing.Point(358, 54)
        Me.Label143.Name = "Label143"
        Me.Label143.Size = New System.Drawing.Size(72, 16)
        Me.Label143.TabIndex = 72
        Me.Label143.Text = "Customer :"
        '
        'Label144
        '
        Me.Label144.AutoSize = True
        Me.Label144.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "Product_code", True))
        Me.Label144.ForeColor = System.Drawing.Color.Navy
        Me.Label144.Location = New System.Drawing.Point(358, 117)
        Me.Label144.Name = "Label144"
        Me.Label144.Size = New System.Drawing.Size(63, 16)
        Me.Label144.TabIndex = 71
        Me.Label144.Text = "Material :"
        '
        'Label145
        '
        Me.Label145.AutoSize = True
        Me.Label145.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "LOAD_PRESET", True))
        Me.Label145.ForeColor = System.Drawing.Color.Navy
        Me.Label145.Location = New System.Drawing.Point(358, 138)
        Me.Label145.Name = "Label145"
        Me.Label145.Size = New System.Drawing.Size(64, 16)
        Me.Label145.TabIndex = 70
        Me.Label145.Text = "Quantity :"
        '
        'Label146
        '
        Me.Label146.AutoSize = True
        Me.Label146.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "LOAD_DID", True))
        Me.Label146.ForeColor = System.Drawing.Color.Navy
        Me.Label146.Location = New System.Drawing.Point(358, 33)
        Me.Label146.Name = "Label146"
        Me.Label146.Size = New System.Drawing.Size(67, 16)
        Me.Label146.TabIndex = 68
        Me.Label146.Text = "Load No. :"
        '
        'Label147
        '
        Me.Label147.AutoSize = True
        Me.Label147.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "LOAD_DRIVER", True))
        Me.Label147.ForeColor = System.Drawing.Color.Navy
        Me.Label147.Location = New System.Drawing.Point(358, 96)
        Me.Label147.Name = "Label147"
        Me.Label147.Size = New System.Drawing.Size(51, 16)
        Me.Label147.TabIndex = 69
        Me.Label147.Text = "Driver :"
        '
        'Label148
        '
        Me.Label148.AutoSize = True
        Me.Label148.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "LOAD_VEHICLE", True))
        Me.Label148.ForeColor = System.Drawing.Color.Navy
        Me.Label148.Location = New System.Drawing.Point(358, 75)
        Me.Label148.Name = "Label148"
        Me.Label148.Size = New System.Drawing.Size(72, 16)
        Me.Label148.TabIndex = 67
        Me.Label148.Text = "Truck No. :"
        '
        'RadPanel17
        '
        Me.RadPanel17.Controls.Add(Me.Label149)
        Me.RadPanel17.Controls.Add(Me.Label150)
        Me.RadPanel17.Controls.Add(Me.Label151)
        Me.RadPanel17.Controls.Add(Me.Label152)
        Me.RadPanel17.Controls.Add(Me.RadSeparator7)
        Me.RadPanel17.Controls.Add(Me.Label153)
        Me.RadPanel17.Controls.Add(Me.Label154)
        Me.RadPanel17.Controls.Add(Me.Label155)
        Me.RadPanel17.Controls.Add(Me.Label156)
        Me.RadPanel17.Location = New System.Drawing.Point(5, 117)
        Me.RadPanel17.Name = "RadPanel17"
        '
        '
        '
        Me.RadPanel17.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel17.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel17.TabIndex = 66
        CType(Me.RadPanel17.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel17.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel17.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel17.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel17.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel17.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label149
        '
        Me.Label149.AutoSize = True
        Me.Label149.ForeColor = System.Drawing.Color.Black
        Me.Label149.Location = New System.Drawing.Point(200, 20)
        Me.Label149.Name = "Label149"
        Me.Label149.Size = New System.Drawing.Size(23, 13)
        Me.Label149.TabIndex = 70
        Me.Label149.Text = "Kg."
        '
        'Label150
        '
        Me.Label150.AutoSize = True
        Me.Label150.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "Raw_Weight_Out", True))
        Me.Label150.ForeColor = System.Drawing.Color.Navy
        Me.Label150.Location = New System.Drawing.Point(150, 20)
        Me.Label150.Name = "Label150"
        Me.Label150.Size = New System.Drawing.Size(45, 13)
        Me.Label150.TabIndex = 68
        Me.Label150.Text = "Weight"
        '
        'Label151
        '
        Me.Label151.AutoSize = True
        Me.Label151.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "WeightOut_time", True))
        Me.Label151.ForeColor = System.Drawing.Color.Navy
        Me.Label151.Location = New System.Drawing.Point(226, 20)
        Me.Label151.Name = "Label151"
        Me.Label151.Size = New System.Drawing.Size(30, 13)
        Me.Label151.TabIndex = 69
        Me.Label151.Text = "Time"
        '
        'Label152
        '
        Me.Label152.AutoSize = True
        Me.Label152.ForeColor = System.Drawing.Color.Black
        Me.Label152.Location = New System.Drawing.Point(65, 20)
        Me.Label152.Name = "Label152"
        Me.Label152.Size = New System.Drawing.Size(23, 13)
        Me.Label152.TabIndex = 67
        Me.Label152.Text = "Kg."
        '
        'RadSeparator7
        '
        Me.RadSeparator7.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator7.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator7.Name = "RadSeparator7"
        Me.RadSeparator7.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator7.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator7.TabIndex = 66
        Me.RadSeparator7.Text = "RadSeparator7"
        '
        'Label153
        '
        Me.Label153.AutoSize = True
        Me.Label153.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label153.Location = New System.Drawing.Point(66, 0)
        Me.Label153.Name = "Label153"
        Me.Label153.Size = New System.Drawing.Size(21, 16)
        Me.Label153.TabIndex = 61
        Me.Label153.Text = "In"
        '
        'Label154
        '
        Me.Label154.AutoSize = True
        Me.Label154.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "Raw_Weight_in", True))
        Me.Label154.ForeColor = System.Drawing.Color.Navy
        Me.Label154.Location = New System.Drawing.Point(15, 20)
        Me.Label154.Name = "Label154"
        Me.Label154.Size = New System.Drawing.Size(45, 13)
        Me.Label154.TabIndex = 60
        Me.Label154.Text = "Weight"
        '
        'Label155
        '
        Me.Label155.AutoSize = True
        Me.Label155.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label155.Location = New System.Drawing.Point(195, 1)
        Me.Label155.Name = "Label155"
        Me.Label155.Size = New System.Drawing.Size(31, 16)
        Me.Label155.TabIndex = 62
        Me.Label155.Text = "Out"
        '
        'Label156
        '
        Me.Label156.AutoSize = True
        Me.Label156.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource8, "Weightin_time", True))
        Me.Label156.ForeColor = System.Drawing.Color.Navy
        Me.Label156.Location = New System.Drawing.Point(91, 20)
        Me.Label156.Name = "Label156"
        Me.Label156.Size = New System.Drawing.Size(30, 13)
        Me.Label156.TabIndex = 63
        Me.Label156.Text = "Time"
        '
        'Label157
        '
        Me.Label157.AutoSize = True
        Me.Label157.Location = New System.Drawing.Point(280, 54)
        Me.Label157.Name = "Label157"
        Me.Label157.Size = New System.Drawing.Size(72, 16)
        Me.Label157.TabIndex = 51
        Me.Label157.Text = "Customer :"
        '
        'Label158
        '
        Me.Label158.AutoSize = True
        Me.Label158.Location = New System.Drawing.Point(289, 117)
        Me.Label158.Name = "Label158"
        Me.Label158.Size = New System.Drawing.Size(63, 16)
        Me.Label158.TabIndex = 45
        Me.Label158.Text = "Material :"
        '
        'P8
        '
        Me.P8.ErrorImage = Nothing
        Me.P8.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P8.Location = New System.Drawing.Point(5, -8)
        Me.P8.Name = "P8"
        Me.P8.Size = New System.Drawing.Size(275, 127)
        Me.P8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P8.TabIndex = 38
        Me.P8.TabStop = False
        Me.P8.Visible = False
        '
        'Label159
        '
        Me.Label159.AutoSize = True
        Me.Label159.Location = New System.Drawing.Point(288, 138)
        Me.Label159.Name = "Label159"
        Me.Label159.Size = New System.Drawing.Size(64, 16)
        Me.Label159.TabIndex = 44
        Me.Label159.Text = "Quantity :"
        '
        'Label160
        '
        Me.Label160.AutoSize = True
        Me.Label160.Location = New System.Drawing.Point(285, 33)
        Me.Label160.Name = "Label160"
        Me.Label160.Size = New System.Drawing.Size(67, 16)
        Me.Label160.TabIndex = 42
        Me.Label160.Text = "Load No. :"
        '
        'Label161
        '
        Me.Label161.AutoSize = True
        Me.Label161.Location = New System.Drawing.Point(301, 96)
        Me.Label161.Name = "Label161"
        Me.Label161.Size = New System.Drawing.Size(51, 16)
        Me.Label161.TabIndex = 43
        Me.Label161.Text = "Driver :"
        '
        'Label162
        '
        Me.Label162.AutoSize = True
        Me.Label162.Location = New System.Drawing.Point(280, 75)
        Me.Label162.Name = "Label162"
        Me.Label162.Size = New System.Drawing.Size(72, 16)
        Me.Label162.TabIndex = 41
        Me.Label162.Text = "Truck No. :"
        '
        'RadGroupBox10
        '
        Me.RadGroupBox10.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox10.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox10.Controls.Add(Me.RadPanel18)
        Me.RadGroupBox10.Controls.Add(Me.Label163)
        Me.RadGroupBox10.Controls.Add(Me.Label164)
        Me.RadGroupBox10.Controls.Add(Me.Label165)
        Me.RadGroupBox10.Controls.Add(Me.Label166)
        Me.RadGroupBox10.Controls.Add(Me.Label167)
        Me.RadGroupBox10.Controls.Add(Me.Label168)
        Me.RadGroupBox10.Controls.Add(Me.RadPanel19)
        Me.RadGroupBox10.Controls.Add(Me.Label177)
        Me.RadGroupBox10.Controls.Add(Me.Label178)
        Me.RadGroupBox10.Controls.Add(Me.P9)
        Me.RadGroupBox10.Controls.Add(Me.Label179)
        Me.RadGroupBox10.Controls.Add(Me.Label180)
        Me.RadGroupBox10.Controls.Add(Me.Label181)
        Me.RadGroupBox10.Controls.Add(Me.Label182)
        Me.RadGroupBox10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox10.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox10.HeaderText = ""
        Me.RadGroupBox10.Location = New System.Drawing.Point(685, 664)
        Me.RadGroupBox10.Name = "RadGroupBox10"
        Me.RadGroupBox10.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox10.TabIndex = 74
        CType(Me.RadGroupBox10.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox10.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox10.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel18
        '
        Me.RadPanel18.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel18.Name = "RadPanel18"
        '
        '
        '
        Me.RadPanel18.RootElement.Shape = Me.QaShape1
        Me.RadPanel18.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel18.TabIndex = 44
        Me.RadPanel18.Text = "BAY 09"
        Me.RadPanel18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel18.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 09"
        CType(Me.RadPanel18.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel18.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel18.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel18.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel18.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel18.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel18.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel18.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label163
        '
        Me.Label163.AutoSize = True
        Me.Label163.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "Customer_name", True))
        Me.Label163.ForeColor = System.Drawing.Color.Navy
        Me.Label163.Location = New System.Drawing.Point(358, 54)
        Me.Label163.Name = "Label163"
        Me.Label163.Size = New System.Drawing.Size(72, 16)
        Me.Label163.TabIndex = 72
        Me.Label163.Text = "Customer :"
        '
        'Label164
        '
        Me.Label164.AutoSize = True
        Me.Label164.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "Product_code", True))
        Me.Label164.ForeColor = System.Drawing.Color.Navy
        Me.Label164.Location = New System.Drawing.Point(358, 117)
        Me.Label164.Name = "Label164"
        Me.Label164.Size = New System.Drawing.Size(63, 16)
        Me.Label164.TabIndex = 71
        Me.Label164.Text = "Material :"
        '
        'Label165
        '
        Me.Label165.AutoSize = True
        Me.Label165.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "LOAD_PRESET", True))
        Me.Label165.ForeColor = System.Drawing.Color.Navy
        Me.Label165.Location = New System.Drawing.Point(358, 138)
        Me.Label165.Name = "Label165"
        Me.Label165.Size = New System.Drawing.Size(64, 16)
        Me.Label165.TabIndex = 70
        Me.Label165.Text = "Quantity :"
        '
        'Label166
        '
        Me.Label166.AutoSize = True
        Me.Label166.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "LOAD_DID", True))
        Me.Label166.ForeColor = System.Drawing.Color.Navy
        Me.Label166.Location = New System.Drawing.Point(358, 33)
        Me.Label166.Name = "Label166"
        Me.Label166.Size = New System.Drawing.Size(67, 16)
        Me.Label166.TabIndex = 68
        Me.Label166.Text = "Load No. :"
        '
        'Label167
        '
        Me.Label167.AutoSize = True
        Me.Label167.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "LOAD_DRIVER", True))
        Me.Label167.ForeColor = System.Drawing.Color.Navy
        Me.Label167.Location = New System.Drawing.Point(358, 96)
        Me.Label167.Name = "Label167"
        Me.Label167.Size = New System.Drawing.Size(51, 16)
        Me.Label167.TabIndex = 69
        Me.Label167.Text = "Driver :"
        '
        'Label168
        '
        Me.Label168.AutoSize = True
        Me.Label168.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "LOAD_VEHICLE", True))
        Me.Label168.ForeColor = System.Drawing.Color.Navy
        Me.Label168.Location = New System.Drawing.Point(358, 75)
        Me.Label168.Name = "Label168"
        Me.Label168.Size = New System.Drawing.Size(72, 16)
        Me.Label168.TabIndex = 67
        Me.Label168.Text = "Truck No. :"
        '
        'RadPanel19
        '
        Me.RadPanel19.Controls.Add(Me.Label169)
        Me.RadPanel19.Controls.Add(Me.Label170)
        Me.RadPanel19.Controls.Add(Me.Label171)
        Me.RadPanel19.Controls.Add(Me.Label172)
        Me.RadPanel19.Controls.Add(Me.RadSeparator8)
        Me.RadPanel19.Controls.Add(Me.Label173)
        Me.RadPanel19.Controls.Add(Me.Label174)
        Me.RadPanel19.Controls.Add(Me.Label175)
        Me.RadPanel19.Controls.Add(Me.Label176)
        Me.RadPanel19.Location = New System.Drawing.Point(5, 117)
        Me.RadPanel19.Name = "RadPanel19"
        '
        '
        '
        Me.RadPanel19.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel19.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel19.TabIndex = 66
        CType(Me.RadPanel19.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel19.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel19.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel19.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel19.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel19.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label169
        '
        Me.Label169.AutoSize = True
        Me.Label169.ForeColor = System.Drawing.Color.Black
        Me.Label169.Location = New System.Drawing.Point(200, 20)
        Me.Label169.Name = "Label169"
        Me.Label169.Size = New System.Drawing.Size(23, 13)
        Me.Label169.TabIndex = 70
        Me.Label169.Text = "Kg."
        '
        'Label170
        '
        Me.Label170.AutoSize = True
        Me.Label170.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "Raw_Weight_Out", True))
        Me.Label170.ForeColor = System.Drawing.Color.Navy
        Me.Label170.Location = New System.Drawing.Point(150, 20)
        Me.Label170.Name = "Label170"
        Me.Label170.Size = New System.Drawing.Size(45, 13)
        Me.Label170.TabIndex = 68
        Me.Label170.Text = "Weight"
        '
        'Label171
        '
        Me.Label171.AutoSize = True
        Me.Label171.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "WeightOut_time", True))
        Me.Label171.ForeColor = System.Drawing.Color.Navy
        Me.Label171.Location = New System.Drawing.Point(226, 20)
        Me.Label171.Name = "Label171"
        Me.Label171.Size = New System.Drawing.Size(30, 13)
        Me.Label171.TabIndex = 69
        Me.Label171.Text = "Time"
        '
        'Label172
        '
        Me.Label172.AutoSize = True
        Me.Label172.ForeColor = System.Drawing.Color.Black
        Me.Label172.Location = New System.Drawing.Point(65, 20)
        Me.Label172.Name = "Label172"
        Me.Label172.Size = New System.Drawing.Size(23, 13)
        Me.Label172.TabIndex = 67
        Me.Label172.Text = "Kg."
        '
        'RadSeparator8
        '
        Me.RadSeparator8.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator8.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator8.Name = "RadSeparator8"
        Me.RadSeparator8.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator8.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator8.TabIndex = 66
        Me.RadSeparator8.Text = "RadSeparator8"
        '
        'Label173
        '
        Me.Label173.AutoSize = True
        Me.Label173.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label173.Location = New System.Drawing.Point(66, 0)
        Me.Label173.Name = "Label173"
        Me.Label173.Size = New System.Drawing.Size(21, 16)
        Me.Label173.TabIndex = 61
        Me.Label173.Text = "In"
        '
        'Label174
        '
        Me.Label174.AutoSize = True
        Me.Label174.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "Raw_Weight_in", True))
        Me.Label174.ForeColor = System.Drawing.Color.Navy
        Me.Label174.Location = New System.Drawing.Point(15, 20)
        Me.Label174.Name = "Label174"
        Me.Label174.Size = New System.Drawing.Size(45, 13)
        Me.Label174.TabIndex = 60
        Me.Label174.Text = "Weight"
        '
        'Label175
        '
        Me.Label175.AutoSize = True
        Me.Label175.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label175.Location = New System.Drawing.Point(195, 1)
        Me.Label175.Name = "Label175"
        Me.Label175.Size = New System.Drawing.Size(31, 16)
        Me.Label175.TabIndex = 62
        Me.Label175.Text = "Out"
        '
        'Label176
        '
        Me.Label176.AutoSize = True
        Me.Label176.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource9, "Weightin_time", True))
        Me.Label176.ForeColor = System.Drawing.Color.Navy
        Me.Label176.Location = New System.Drawing.Point(91, 20)
        Me.Label176.Name = "Label176"
        Me.Label176.Size = New System.Drawing.Size(30, 13)
        Me.Label176.TabIndex = 63
        Me.Label176.Text = "Time"
        '
        'Label177
        '
        Me.Label177.AutoSize = True
        Me.Label177.Location = New System.Drawing.Point(280, 54)
        Me.Label177.Name = "Label177"
        Me.Label177.Size = New System.Drawing.Size(72, 16)
        Me.Label177.TabIndex = 51
        Me.Label177.Text = "Customer :"
        '
        'Label178
        '
        Me.Label178.AutoSize = True
        Me.Label178.Location = New System.Drawing.Point(289, 117)
        Me.Label178.Name = "Label178"
        Me.Label178.Size = New System.Drawing.Size(63, 16)
        Me.Label178.TabIndex = 45
        Me.Label178.Text = "Material :"
        '
        'P9
        '
        Me.P9.ErrorImage = Nothing
        Me.P9.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P9.Location = New System.Drawing.Point(5, -8)
        Me.P9.Name = "P9"
        Me.P9.Size = New System.Drawing.Size(275, 127)
        Me.P9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P9.TabIndex = 38
        Me.P9.TabStop = False
        Me.P9.Visible = False
        '
        'Label179
        '
        Me.Label179.AutoSize = True
        Me.Label179.Location = New System.Drawing.Point(288, 138)
        Me.Label179.Name = "Label179"
        Me.Label179.Size = New System.Drawing.Size(64, 16)
        Me.Label179.TabIndex = 44
        Me.Label179.Text = "Quantity :"
        '
        'Label180
        '
        Me.Label180.AutoSize = True
        Me.Label180.Location = New System.Drawing.Point(285, 33)
        Me.Label180.Name = "Label180"
        Me.Label180.Size = New System.Drawing.Size(67, 16)
        Me.Label180.TabIndex = 42
        Me.Label180.Text = "Load No. :"
        '
        'Label181
        '
        Me.Label181.AutoSize = True
        Me.Label181.Location = New System.Drawing.Point(301, 96)
        Me.Label181.Name = "Label181"
        Me.Label181.Size = New System.Drawing.Size(51, 16)
        Me.Label181.TabIndex = 43
        Me.Label181.Text = "Driver :"
        '
        'Label182
        '
        Me.Label182.AutoSize = True
        Me.Label182.Location = New System.Drawing.Point(280, 75)
        Me.Label182.Name = "Label182"
        Me.Label182.Size = New System.Drawing.Size(72, 16)
        Me.Label182.TabIndex = 41
        Me.Label182.Text = "Truck No. :"
        '
        'RadGroupBox11
        '
        Me.RadGroupBox11.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox11.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox11.Controls.Add(Me.RadPanel20)
        Me.RadGroupBox11.Controls.Add(Me.Label183)
        Me.RadGroupBox11.Controls.Add(Me.Label184)
        Me.RadGroupBox11.Controls.Add(Me.Label185)
        Me.RadGroupBox11.Controls.Add(Me.Label186)
        Me.RadGroupBox11.Controls.Add(Me.Label187)
        Me.RadGroupBox11.Controls.Add(Me.Label188)
        Me.RadGroupBox11.Controls.Add(Me.RadPanel21)
        Me.RadGroupBox11.Controls.Add(Me.Label197)
        Me.RadGroupBox11.Controls.Add(Me.Label198)
        Me.RadGroupBox11.Controls.Add(Me.P10)
        Me.RadGroupBox11.Controls.Add(Me.Label199)
        Me.RadGroupBox11.Controls.Add(Me.Label200)
        Me.RadGroupBox11.Controls.Add(Me.Label201)
        Me.RadGroupBox11.Controls.Add(Me.Label202)
        Me.RadGroupBox11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox11.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox11.HeaderText = ""
        Me.RadGroupBox11.Location = New System.Drawing.Point(685, 833)
        Me.RadGroupBox11.Name = "RadGroupBox11"
        Me.RadGroupBox11.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox11.TabIndex = 75
        CType(Me.RadGroupBox11.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox11.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox11.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel20
        '
        Me.RadPanel20.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel20.Name = "RadPanel20"
        '
        '
        '
        Me.RadPanel20.RootElement.Shape = Me.QaShape1
        Me.RadPanel20.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel20.TabIndex = 44
        Me.RadPanel20.Text = "BAY 10"
        Me.RadPanel20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel20.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 10"
        CType(Me.RadPanel20.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel20.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel20.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel20.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel20.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel20.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel20.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel20.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label183
        '
        Me.Label183.AutoSize = True
        Me.Label183.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "Customer_name", True))
        Me.Label183.ForeColor = System.Drawing.Color.Navy
        Me.Label183.Location = New System.Drawing.Point(358, 54)
        Me.Label183.Name = "Label183"
        Me.Label183.Size = New System.Drawing.Size(72, 16)
        Me.Label183.TabIndex = 72
        Me.Label183.Text = "Customer :"
        '
        'Label184
        '
        Me.Label184.AutoSize = True
        Me.Label184.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "Product_code", True))
        Me.Label184.ForeColor = System.Drawing.Color.Navy
        Me.Label184.Location = New System.Drawing.Point(358, 117)
        Me.Label184.Name = "Label184"
        Me.Label184.Size = New System.Drawing.Size(63, 16)
        Me.Label184.TabIndex = 71
        Me.Label184.Text = "Material :"
        '
        'Label185
        '
        Me.Label185.AutoSize = True
        Me.Label185.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "LOAD_PRESET", True))
        Me.Label185.ForeColor = System.Drawing.Color.Navy
        Me.Label185.Location = New System.Drawing.Point(358, 138)
        Me.Label185.Name = "Label185"
        Me.Label185.Size = New System.Drawing.Size(64, 16)
        Me.Label185.TabIndex = 70
        Me.Label185.Text = "Quantity :"
        '
        'Label186
        '
        Me.Label186.AutoSize = True
        Me.Label186.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "LOAD_DID", True))
        Me.Label186.ForeColor = System.Drawing.Color.Navy
        Me.Label186.Location = New System.Drawing.Point(358, 33)
        Me.Label186.Name = "Label186"
        Me.Label186.Size = New System.Drawing.Size(67, 16)
        Me.Label186.TabIndex = 68
        Me.Label186.Text = "Load No. :"
        '
        'Label187
        '
        Me.Label187.AutoSize = True
        Me.Label187.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "LOAD_DRIVER", True))
        Me.Label187.ForeColor = System.Drawing.Color.Navy
        Me.Label187.Location = New System.Drawing.Point(358, 96)
        Me.Label187.Name = "Label187"
        Me.Label187.Size = New System.Drawing.Size(51, 16)
        Me.Label187.TabIndex = 69
        Me.Label187.Text = "Driver :"
        '
        'Label188
        '
        Me.Label188.AutoSize = True
        Me.Label188.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "LOAD_VEHICLE", True))
        Me.Label188.ForeColor = System.Drawing.Color.Navy
        Me.Label188.Location = New System.Drawing.Point(358, 75)
        Me.Label188.Name = "Label188"
        Me.Label188.Size = New System.Drawing.Size(72, 16)
        Me.Label188.TabIndex = 67
        Me.Label188.Text = "Truck No. :"
        '
        'RadPanel21
        '
        Me.RadPanel21.Controls.Add(Me.Label189)
        Me.RadPanel21.Controls.Add(Me.Label190)
        Me.RadPanel21.Controls.Add(Me.Label191)
        Me.RadPanel21.Controls.Add(Me.Label192)
        Me.RadPanel21.Controls.Add(Me.RadSeparator9)
        Me.RadPanel21.Controls.Add(Me.Label193)
        Me.RadPanel21.Controls.Add(Me.Label194)
        Me.RadPanel21.Controls.Add(Me.Label195)
        Me.RadPanel21.Controls.Add(Me.Label196)
        Me.RadPanel21.Location = New System.Drawing.Point(5, 117)
        Me.RadPanel21.Name = "RadPanel21"
        '
        '
        '
        Me.RadPanel21.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel21.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel21.TabIndex = 66
        CType(Me.RadPanel21.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel21.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel21.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel21.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel21.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel21.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label189
        '
        Me.Label189.AutoSize = True
        Me.Label189.ForeColor = System.Drawing.Color.Black
        Me.Label189.Location = New System.Drawing.Point(200, 20)
        Me.Label189.Name = "Label189"
        Me.Label189.Size = New System.Drawing.Size(23, 13)
        Me.Label189.TabIndex = 70
        Me.Label189.Text = "Kg."
        '
        'Label190
        '
        Me.Label190.AutoSize = True
        Me.Label190.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "Raw_Weight_Out", True))
        Me.Label190.ForeColor = System.Drawing.Color.Navy
        Me.Label190.Location = New System.Drawing.Point(150, 20)
        Me.Label190.Name = "Label190"
        Me.Label190.Size = New System.Drawing.Size(45, 13)
        Me.Label190.TabIndex = 68
        Me.Label190.Text = "Weight"
        '
        'Label191
        '
        Me.Label191.AutoSize = True
        Me.Label191.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "WeightOut_time", True))
        Me.Label191.ForeColor = System.Drawing.Color.Navy
        Me.Label191.Location = New System.Drawing.Point(226, 20)
        Me.Label191.Name = "Label191"
        Me.Label191.Size = New System.Drawing.Size(30, 13)
        Me.Label191.TabIndex = 69
        Me.Label191.Text = "Time"
        '
        'Label192
        '
        Me.Label192.AutoSize = True
        Me.Label192.ForeColor = System.Drawing.Color.Black
        Me.Label192.Location = New System.Drawing.Point(65, 20)
        Me.Label192.Name = "Label192"
        Me.Label192.Size = New System.Drawing.Size(23, 13)
        Me.Label192.TabIndex = 67
        Me.Label192.Text = "Kg."
        '
        'RadSeparator9
        '
        Me.RadSeparator9.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator9.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator9.Name = "RadSeparator9"
        Me.RadSeparator9.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator9.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator9.TabIndex = 66
        Me.RadSeparator9.Text = "RadSeparator9"
        '
        'Label193
        '
        Me.Label193.AutoSize = True
        Me.Label193.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label193.Location = New System.Drawing.Point(66, 0)
        Me.Label193.Name = "Label193"
        Me.Label193.Size = New System.Drawing.Size(21, 16)
        Me.Label193.TabIndex = 61
        Me.Label193.Text = "In"
        '
        'Label194
        '
        Me.Label194.AutoSize = True
        Me.Label194.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "Raw_Weight_in", True))
        Me.Label194.ForeColor = System.Drawing.Color.Navy
        Me.Label194.Location = New System.Drawing.Point(15, 20)
        Me.Label194.Name = "Label194"
        Me.Label194.Size = New System.Drawing.Size(45, 13)
        Me.Label194.TabIndex = 60
        Me.Label194.Text = "Weight"
        '
        'Label195
        '
        Me.Label195.AutoSize = True
        Me.Label195.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label195.Location = New System.Drawing.Point(195, 1)
        Me.Label195.Name = "Label195"
        Me.Label195.Size = New System.Drawing.Size(31, 16)
        Me.Label195.TabIndex = 62
        Me.Label195.Text = "Out"
        '
        'Label196
        '
        Me.Label196.AutoSize = True
        Me.Label196.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource10, "Weightin_time", True))
        Me.Label196.ForeColor = System.Drawing.Color.Navy
        Me.Label196.Location = New System.Drawing.Point(91, 20)
        Me.Label196.Name = "Label196"
        Me.Label196.Size = New System.Drawing.Size(30, 13)
        Me.Label196.TabIndex = 63
        Me.Label196.Text = "Time"
        '
        'Label197
        '
        Me.Label197.AutoSize = True
        Me.Label197.Location = New System.Drawing.Point(280, 54)
        Me.Label197.Name = "Label197"
        Me.Label197.Size = New System.Drawing.Size(72, 16)
        Me.Label197.TabIndex = 51
        Me.Label197.Text = "Customer :"
        '
        'Label198
        '
        Me.Label198.AutoSize = True
        Me.Label198.Location = New System.Drawing.Point(289, 117)
        Me.Label198.Name = "Label198"
        Me.Label198.Size = New System.Drawing.Size(63, 16)
        Me.Label198.TabIndex = 45
        Me.Label198.Text = "Material :"
        '
        'P10
        '
        Me.P10.ErrorImage = Nothing
        Me.P10.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P10.Location = New System.Drawing.Point(5, -8)
        Me.P10.Name = "P10"
        Me.P10.Size = New System.Drawing.Size(275, 127)
        Me.P10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P10.TabIndex = 38
        Me.P10.TabStop = False
        Me.P10.Visible = False
        '
        'Label199
        '
        Me.Label199.AutoSize = True
        Me.Label199.Location = New System.Drawing.Point(288, 138)
        Me.Label199.Name = "Label199"
        Me.Label199.Size = New System.Drawing.Size(64, 16)
        Me.Label199.TabIndex = 44
        Me.Label199.Text = "Quantity :"
        '
        'Label200
        '
        Me.Label200.AutoSize = True
        Me.Label200.Location = New System.Drawing.Point(285, 33)
        Me.Label200.Name = "Label200"
        Me.Label200.Size = New System.Drawing.Size(67, 16)
        Me.Label200.TabIndex = 42
        Me.Label200.Text = "Load No. :"
        '
        'Label201
        '
        Me.Label201.AutoSize = True
        Me.Label201.Location = New System.Drawing.Point(301, 96)
        Me.Label201.Name = "Label201"
        Me.Label201.Size = New System.Drawing.Size(51, 16)
        Me.Label201.TabIndex = 43
        Me.Label201.Text = "Driver :"
        '
        'Label202
        '
        Me.Label202.AutoSize = True
        Me.Label202.Location = New System.Drawing.Point(280, 75)
        Me.Label202.Name = "Label202"
        Me.Label202.Size = New System.Drawing.Size(72, 16)
        Me.Label202.TabIndex = 41
        Me.Label202.Text = "Truck No. :"
        '
        'RadGroupBox12
        '
        Me.RadGroupBox12.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox12.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox12.Controls.Add(Me.RadPanel22)
        Me.RadGroupBox12.Controls.Add(Me.Label203)
        Me.RadGroupBox12.Controls.Add(Me.Label204)
        Me.RadGroupBox12.Controls.Add(Me.Label205)
        Me.RadGroupBox12.Controls.Add(Me.Label206)
        Me.RadGroupBox12.Controls.Add(Me.Label207)
        Me.RadGroupBox12.Controls.Add(Me.Label208)
        Me.RadGroupBox12.Controls.Add(Me.RadPanel23)
        Me.RadGroupBox12.Controls.Add(Me.Label217)
        Me.RadGroupBox12.Controls.Add(Me.Label218)
        Me.RadGroupBox12.Controls.Add(Me.P11)
        Me.RadGroupBox12.Controls.Add(Me.Label219)
        Me.RadGroupBox12.Controls.Add(Me.Label220)
        Me.RadGroupBox12.Controls.Add(Me.Label221)
        Me.RadGroupBox12.Controls.Add(Me.Label222)
        Me.RadGroupBox12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox12.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox12.HeaderText = ""
        Me.RadGroupBox12.Location = New System.Drawing.Point(1259, 157)
        Me.RadGroupBox12.Name = "RadGroupBox12"
        Me.RadGroupBox12.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox12.TabIndex = 76
        CType(Me.RadGroupBox12.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox12.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox12.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel22
        '
        Me.RadPanel22.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel22.Name = "RadPanel22"
        '
        '
        '
        Me.RadPanel22.RootElement.Shape = Me.QaShape1
        Me.RadPanel22.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel22.TabIndex = 44
        Me.RadPanel22.Text = "BAY 11"
        Me.RadPanel22.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel22.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 11"
        CType(Me.RadPanel22.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel22.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel22.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel22.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel22.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel22.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel22.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel22.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label203
        '
        Me.Label203.AutoSize = True
        Me.Label203.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "Customer_name", True))
        Me.Label203.ForeColor = System.Drawing.Color.Navy
        Me.Label203.Location = New System.Drawing.Point(358, 54)
        Me.Label203.Name = "Label203"
        Me.Label203.Size = New System.Drawing.Size(72, 16)
        Me.Label203.TabIndex = 72
        Me.Label203.Text = "Customer :"
        '
        'Label204
        '
        Me.Label204.AutoSize = True
        Me.Label204.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "Product_code", True))
        Me.Label204.ForeColor = System.Drawing.Color.Navy
        Me.Label204.Location = New System.Drawing.Point(358, 117)
        Me.Label204.Name = "Label204"
        Me.Label204.Size = New System.Drawing.Size(63, 16)
        Me.Label204.TabIndex = 71
        Me.Label204.Text = "Material :"
        '
        'Label205
        '
        Me.Label205.AutoSize = True
        Me.Label205.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "LOAD_PRESET", True))
        Me.Label205.ForeColor = System.Drawing.Color.Navy
        Me.Label205.Location = New System.Drawing.Point(358, 138)
        Me.Label205.Name = "Label205"
        Me.Label205.Size = New System.Drawing.Size(64, 16)
        Me.Label205.TabIndex = 70
        Me.Label205.Text = "Quantity :"
        '
        'Label206
        '
        Me.Label206.AutoSize = True
        Me.Label206.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "LOAD_DID", True))
        Me.Label206.ForeColor = System.Drawing.Color.Navy
        Me.Label206.Location = New System.Drawing.Point(358, 33)
        Me.Label206.Name = "Label206"
        Me.Label206.Size = New System.Drawing.Size(67, 16)
        Me.Label206.TabIndex = 68
        Me.Label206.Text = "Load No. :"
        '
        'Label207
        '
        Me.Label207.AutoSize = True
        Me.Label207.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "LOAD_DRIVER", True))
        Me.Label207.ForeColor = System.Drawing.Color.Navy
        Me.Label207.Location = New System.Drawing.Point(358, 96)
        Me.Label207.Name = "Label207"
        Me.Label207.Size = New System.Drawing.Size(51, 16)
        Me.Label207.TabIndex = 69
        Me.Label207.Text = "Driver :"
        '
        'Label208
        '
        Me.Label208.AutoSize = True
        Me.Label208.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "LOAD_VEHICLE", True))
        Me.Label208.ForeColor = System.Drawing.Color.Navy
        Me.Label208.Location = New System.Drawing.Point(358, 75)
        Me.Label208.Name = "Label208"
        Me.Label208.Size = New System.Drawing.Size(72, 16)
        Me.Label208.TabIndex = 67
        Me.Label208.Text = "Truck No. :"
        '
        'RadPanel23
        '
        Me.RadPanel23.Controls.Add(Me.Label209)
        Me.RadPanel23.Controls.Add(Me.Label210)
        Me.RadPanel23.Controls.Add(Me.Label211)
        Me.RadPanel23.Controls.Add(Me.Label212)
        Me.RadPanel23.Controls.Add(Me.RadSeparator10)
        Me.RadPanel23.Controls.Add(Me.Label213)
        Me.RadPanel23.Controls.Add(Me.Label214)
        Me.RadPanel23.Controls.Add(Me.Label215)
        Me.RadPanel23.Controls.Add(Me.Label216)
        Me.RadPanel23.Location = New System.Drawing.Point(5, 119)
        Me.RadPanel23.Name = "RadPanel23"
        '
        '
        '
        Me.RadPanel23.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel23.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel23.TabIndex = 66
        CType(Me.RadPanel23.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel23.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel23.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel23.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel23.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel23.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label209
        '
        Me.Label209.AutoSize = True
        Me.Label209.ForeColor = System.Drawing.Color.Black
        Me.Label209.Location = New System.Drawing.Point(200, 20)
        Me.Label209.Name = "Label209"
        Me.Label209.Size = New System.Drawing.Size(23, 13)
        Me.Label209.TabIndex = 70
        Me.Label209.Text = "Kg."
        '
        'Label210
        '
        Me.Label210.AutoSize = True
        Me.Label210.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "Raw_Weight_Out", True))
        Me.Label210.ForeColor = System.Drawing.Color.Navy
        Me.Label210.Location = New System.Drawing.Point(150, 20)
        Me.Label210.Name = "Label210"
        Me.Label210.Size = New System.Drawing.Size(45, 13)
        Me.Label210.TabIndex = 68
        Me.Label210.Text = "Weight"
        '
        'Label211
        '
        Me.Label211.AutoSize = True
        Me.Label211.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "WeightOut_time", True))
        Me.Label211.ForeColor = System.Drawing.Color.Navy
        Me.Label211.Location = New System.Drawing.Point(226, 20)
        Me.Label211.Name = "Label211"
        Me.Label211.Size = New System.Drawing.Size(30, 13)
        Me.Label211.TabIndex = 69
        Me.Label211.Text = "Time"
        '
        'Label212
        '
        Me.Label212.AutoSize = True
        Me.Label212.ForeColor = System.Drawing.Color.Black
        Me.Label212.Location = New System.Drawing.Point(65, 20)
        Me.Label212.Name = "Label212"
        Me.Label212.Size = New System.Drawing.Size(23, 13)
        Me.Label212.TabIndex = 67
        Me.Label212.Text = "Kg."
        '
        'RadSeparator10
        '
        Me.RadSeparator10.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator10.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator10.Name = "RadSeparator10"
        Me.RadSeparator10.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator10.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator10.TabIndex = 66
        Me.RadSeparator10.Text = "RadSeparator10"
        '
        'Label213
        '
        Me.Label213.AutoSize = True
        Me.Label213.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label213.Location = New System.Drawing.Point(66, 0)
        Me.Label213.Name = "Label213"
        Me.Label213.Size = New System.Drawing.Size(21, 16)
        Me.Label213.TabIndex = 61
        Me.Label213.Text = "In"
        '
        'Label214
        '
        Me.Label214.AutoSize = True
        Me.Label214.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "Raw_Weight_in", True))
        Me.Label214.ForeColor = System.Drawing.Color.Navy
        Me.Label214.Location = New System.Drawing.Point(15, 20)
        Me.Label214.Name = "Label214"
        Me.Label214.Size = New System.Drawing.Size(45, 13)
        Me.Label214.TabIndex = 60
        Me.Label214.Text = "Weight"
        '
        'Label215
        '
        Me.Label215.AutoSize = True
        Me.Label215.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label215.Location = New System.Drawing.Point(195, 1)
        Me.Label215.Name = "Label215"
        Me.Label215.Size = New System.Drawing.Size(31, 16)
        Me.Label215.TabIndex = 62
        Me.Label215.Text = "Out"
        '
        'Label216
        '
        Me.Label216.AutoSize = True
        Me.Label216.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource11, "Weightin_time", True))
        Me.Label216.ForeColor = System.Drawing.Color.Navy
        Me.Label216.Location = New System.Drawing.Point(91, 20)
        Me.Label216.Name = "Label216"
        Me.Label216.Size = New System.Drawing.Size(30, 13)
        Me.Label216.TabIndex = 63
        Me.Label216.Text = "Time"
        '
        'Label217
        '
        Me.Label217.AutoSize = True
        Me.Label217.Location = New System.Drawing.Point(280, 54)
        Me.Label217.Name = "Label217"
        Me.Label217.Size = New System.Drawing.Size(72, 16)
        Me.Label217.TabIndex = 51
        Me.Label217.Text = "Customer :"
        '
        'Label218
        '
        Me.Label218.AutoSize = True
        Me.Label218.Location = New System.Drawing.Point(289, 117)
        Me.Label218.Name = "Label218"
        Me.Label218.Size = New System.Drawing.Size(63, 16)
        Me.Label218.TabIndex = 45
        Me.Label218.Text = "Material :"
        '
        'P11
        '
        Me.P11.ErrorImage = Nothing
        Me.P11.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P11.Location = New System.Drawing.Point(5, -8)
        Me.P11.Name = "P11"
        Me.P11.Size = New System.Drawing.Size(275, 127)
        Me.P11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P11.TabIndex = 38
        Me.P11.TabStop = False
        Me.P11.Visible = False
        '
        'Label219
        '
        Me.Label219.AutoSize = True
        Me.Label219.Location = New System.Drawing.Point(288, 138)
        Me.Label219.Name = "Label219"
        Me.Label219.Size = New System.Drawing.Size(64, 16)
        Me.Label219.TabIndex = 44
        Me.Label219.Text = "Quantity :"
        '
        'Label220
        '
        Me.Label220.AutoSize = True
        Me.Label220.Location = New System.Drawing.Point(285, 33)
        Me.Label220.Name = "Label220"
        Me.Label220.Size = New System.Drawing.Size(67, 16)
        Me.Label220.TabIndex = 42
        Me.Label220.Text = "Load No. :"
        '
        'Label221
        '
        Me.Label221.AutoSize = True
        Me.Label221.Location = New System.Drawing.Point(301, 96)
        Me.Label221.Name = "Label221"
        Me.Label221.Size = New System.Drawing.Size(51, 16)
        Me.Label221.TabIndex = 43
        Me.Label221.Text = "Driver :"
        '
        'Label222
        '
        Me.Label222.AutoSize = True
        Me.Label222.Location = New System.Drawing.Point(280, 75)
        Me.Label222.Name = "Label222"
        Me.Label222.Size = New System.Drawing.Size(72, 16)
        Me.Label222.TabIndex = 41
        Me.Label222.Text = "Truck No. :"
        '
        'RadGroupBox13
        '
        Me.RadGroupBox13.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox13.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox13.Controls.Add(Me.RadPanel24)
        Me.RadGroupBox13.Controls.Add(Me.Label223)
        Me.RadGroupBox13.Controls.Add(Me.Label224)
        Me.RadGroupBox13.Controls.Add(Me.Label225)
        Me.RadGroupBox13.Controls.Add(Me.Label226)
        Me.RadGroupBox13.Controls.Add(Me.Label227)
        Me.RadGroupBox13.Controls.Add(Me.Label228)
        Me.RadGroupBox13.Controls.Add(Me.RadPanel25)
        Me.RadGroupBox13.Controls.Add(Me.Label237)
        Me.RadGroupBox13.Controls.Add(Me.Label238)
        Me.RadGroupBox13.Controls.Add(Me.P12)
        Me.RadGroupBox13.Controls.Add(Me.Label239)
        Me.RadGroupBox13.Controls.Add(Me.Label240)
        Me.RadGroupBox13.Controls.Add(Me.Label241)
        Me.RadGroupBox13.Controls.Add(Me.Label242)
        Me.RadGroupBox13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox13.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox13.HeaderText = ""
        Me.RadGroupBox13.Location = New System.Drawing.Point(1259, 326)
        Me.RadGroupBox13.Name = "RadGroupBox13"
        Me.RadGroupBox13.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox13.TabIndex = 77
        CType(Me.RadGroupBox13.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox13.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox13.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel24
        '
        Me.RadPanel24.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel24.Name = "RadPanel24"
        '
        '
        '
        Me.RadPanel24.RootElement.Shape = Me.QaShape1
        Me.RadPanel24.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel24.TabIndex = 44
        Me.RadPanel24.Text = "BAY 12"
        Me.RadPanel24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel24.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 12"
        CType(Me.RadPanel24.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel24.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel24.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel24.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel24.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel24.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel24.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel24.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label223
        '
        Me.Label223.AutoSize = True
        Me.Label223.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "Customer_name", True))
        Me.Label223.ForeColor = System.Drawing.Color.Navy
        Me.Label223.Location = New System.Drawing.Point(358, 54)
        Me.Label223.Name = "Label223"
        Me.Label223.Size = New System.Drawing.Size(72, 16)
        Me.Label223.TabIndex = 72
        Me.Label223.Text = "Customer :"
        '
        'Label224
        '
        Me.Label224.AutoSize = True
        Me.Label224.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "Product_code", True))
        Me.Label224.ForeColor = System.Drawing.Color.Navy
        Me.Label224.Location = New System.Drawing.Point(358, 117)
        Me.Label224.Name = "Label224"
        Me.Label224.Size = New System.Drawing.Size(63, 16)
        Me.Label224.TabIndex = 71
        Me.Label224.Text = "Material :"
        '
        'Label225
        '
        Me.Label225.AutoSize = True
        Me.Label225.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "LOAD_PRESET", True))
        Me.Label225.ForeColor = System.Drawing.Color.Navy
        Me.Label225.Location = New System.Drawing.Point(358, 138)
        Me.Label225.Name = "Label225"
        Me.Label225.Size = New System.Drawing.Size(64, 16)
        Me.Label225.TabIndex = 70
        Me.Label225.Text = "Quantity :"
        '
        'Label226
        '
        Me.Label226.AutoSize = True
        Me.Label226.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "LOAD_DID", True))
        Me.Label226.ForeColor = System.Drawing.Color.Navy
        Me.Label226.Location = New System.Drawing.Point(358, 33)
        Me.Label226.Name = "Label226"
        Me.Label226.Size = New System.Drawing.Size(67, 16)
        Me.Label226.TabIndex = 68
        Me.Label226.Text = "Load No. :"
        '
        'Label227
        '
        Me.Label227.AutoSize = True
        Me.Label227.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "LOAD_DRIVER", True))
        Me.Label227.ForeColor = System.Drawing.Color.Navy
        Me.Label227.Location = New System.Drawing.Point(358, 96)
        Me.Label227.Name = "Label227"
        Me.Label227.Size = New System.Drawing.Size(51, 16)
        Me.Label227.TabIndex = 69
        Me.Label227.Text = "Driver :"
        '
        'Label228
        '
        Me.Label228.AutoSize = True
        Me.Label228.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "LOAD_VEHICLE", True))
        Me.Label228.ForeColor = System.Drawing.Color.Navy
        Me.Label228.Location = New System.Drawing.Point(358, 75)
        Me.Label228.Name = "Label228"
        Me.Label228.Size = New System.Drawing.Size(72, 16)
        Me.Label228.TabIndex = 67
        Me.Label228.Text = "Truck No. :"
        '
        'RadPanel25
        '
        Me.RadPanel25.Controls.Add(Me.Label229)
        Me.RadPanel25.Controls.Add(Me.Label230)
        Me.RadPanel25.Controls.Add(Me.Label231)
        Me.RadPanel25.Controls.Add(Me.Label232)
        Me.RadPanel25.Controls.Add(Me.RadSeparator11)
        Me.RadPanel25.Controls.Add(Me.Label233)
        Me.RadPanel25.Controls.Add(Me.Label234)
        Me.RadPanel25.Controls.Add(Me.Label235)
        Me.RadPanel25.Controls.Add(Me.Label236)
        Me.RadPanel25.Location = New System.Drawing.Point(5, 118)
        Me.RadPanel25.Name = "RadPanel25"
        '
        '
        '
        Me.RadPanel25.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel25.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel25.TabIndex = 66
        CType(Me.RadPanel25.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel25.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel25.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel25.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel25.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel25.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label229
        '
        Me.Label229.AutoSize = True
        Me.Label229.ForeColor = System.Drawing.Color.Black
        Me.Label229.Location = New System.Drawing.Point(200, 20)
        Me.Label229.Name = "Label229"
        Me.Label229.Size = New System.Drawing.Size(23, 13)
        Me.Label229.TabIndex = 70
        Me.Label229.Text = "Kg."
        '
        'Label230
        '
        Me.Label230.AutoSize = True
        Me.Label230.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "Raw_Weight_Out", True))
        Me.Label230.ForeColor = System.Drawing.Color.Navy
        Me.Label230.Location = New System.Drawing.Point(150, 20)
        Me.Label230.Name = "Label230"
        Me.Label230.Size = New System.Drawing.Size(45, 13)
        Me.Label230.TabIndex = 68
        Me.Label230.Text = "Weight"
        '
        'Label231
        '
        Me.Label231.AutoSize = True
        Me.Label231.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "WeightOut_time", True))
        Me.Label231.ForeColor = System.Drawing.Color.Navy
        Me.Label231.Location = New System.Drawing.Point(226, 20)
        Me.Label231.Name = "Label231"
        Me.Label231.Size = New System.Drawing.Size(30, 13)
        Me.Label231.TabIndex = 69
        Me.Label231.Text = "Time"
        '
        'Label232
        '
        Me.Label232.AutoSize = True
        Me.Label232.ForeColor = System.Drawing.Color.Black
        Me.Label232.Location = New System.Drawing.Point(65, 20)
        Me.Label232.Name = "Label232"
        Me.Label232.Size = New System.Drawing.Size(23, 13)
        Me.Label232.TabIndex = 67
        Me.Label232.Text = "Kg."
        '
        'RadSeparator11
        '
        Me.RadSeparator11.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator11.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator11.Name = "RadSeparator11"
        Me.RadSeparator11.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator11.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator11.TabIndex = 66
        Me.RadSeparator11.Text = "RadSeparator11"
        '
        'Label233
        '
        Me.Label233.AutoSize = True
        Me.Label233.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label233.Location = New System.Drawing.Point(66, 0)
        Me.Label233.Name = "Label233"
        Me.Label233.Size = New System.Drawing.Size(21, 16)
        Me.Label233.TabIndex = 61
        Me.Label233.Text = "In"
        '
        'Label234
        '
        Me.Label234.AutoSize = True
        Me.Label234.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "Raw_Weight_in", True))
        Me.Label234.ForeColor = System.Drawing.Color.Navy
        Me.Label234.Location = New System.Drawing.Point(15, 20)
        Me.Label234.Name = "Label234"
        Me.Label234.Size = New System.Drawing.Size(45, 13)
        Me.Label234.TabIndex = 60
        Me.Label234.Text = "Weight"
        '
        'Label235
        '
        Me.Label235.AutoSize = True
        Me.Label235.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label235.Location = New System.Drawing.Point(195, 1)
        Me.Label235.Name = "Label235"
        Me.Label235.Size = New System.Drawing.Size(31, 16)
        Me.Label235.TabIndex = 62
        Me.Label235.Text = "Out"
        '
        'Label236
        '
        Me.Label236.AutoSize = True
        Me.Label236.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource12, "Weightin_time", True))
        Me.Label236.ForeColor = System.Drawing.Color.Navy
        Me.Label236.Location = New System.Drawing.Point(91, 20)
        Me.Label236.Name = "Label236"
        Me.Label236.Size = New System.Drawing.Size(30, 13)
        Me.Label236.TabIndex = 63
        Me.Label236.Text = "Time"
        '
        'Label237
        '
        Me.Label237.AutoSize = True
        Me.Label237.Location = New System.Drawing.Point(280, 54)
        Me.Label237.Name = "Label237"
        Me.Label237.Size = New System.Drawing.Size(72, 16)
        Me.Label237.TabIndex = 51
        Me.Label237.Text = "Customer :"
        '
        'Label238
        '
        Me.Label238.AutoSize = True
        Me.Label238.Location = New System.Drawing.Point(289, 117)
        Me.Label238.Name = "Label238"
        Me.Label238.Size = New System.Drawing.Size(63, 16)
        Me.Label238.TabIndex = 45
        Me.Label238.Text = "Material :"
        '
        'P12
        '
        Me.P12.ErrorImage = Nothing
        Me.P12.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P12.Location = New System.Drawing.Point(5, -8)
        Me.P12.Name = "P12"
        Me.P12.Size = New System.Drawing.Size(275, 127)
        Me.P12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P12.TabIndex = 38
        Me.P12.TabStop = False
        Me.P12.Visible = False
        '
        'Label239
        '
        Me.Label239.AutoSize = True
        Me.Label239.Location = New System.Drawing.Point(288, 138)
        Me.Label239.Name = "Label239"
        Me.Label239.Size = New System.Drawing.Size(64, 16)
        Me.Label239.TabIndex = 44
        Me.Label239.Text = "Quantity :"
        '
        'Label240
        '
        Me.Label240.AutoSize = True
        Me.Label240.Location = New System.Drawing.Point(285, 33)
        Me.Label240.Name = "Label240"
        Me.Label240.Size = New System.Drawing.Size(67, 16)
        Me.Label240.TabIndex = 42
        Me.Label240.Text = "Load No. :"
        '
        'Label241
        '
        Me.Label241.AutoSize = True
        Me.Label241.Location = New System.Drawing.Point(301, 96)
        Me.Label241.Name = "Label241"
        Me.Label241.Size = New System.Drawing.Size(51, 16)
        Me.Label241.TabIndex = 43
        Me.Label241.Text = "Driver :"
        '
        'Label242
        '
        Me.Label242.AutoSize = True
        Me.Label242.Location = New System.Drawing.Point(280, 75)
        Me.Label242.Name = "Label242"
        Me.Label242.Size = New System.Drawing.Size(72, 16)
        Me.Label242.TabIndex = 41
        Me.Label242.Text = "Truck No. :"
        '
        'RadGroupBox14
        '
        Me.RadGroupBox14.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox14.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox14.Controls.Add(Me.RadPanel26)
        Me.RadGroupBox14.Controls.Add(Me.Label243)
        Me.RadGroupBox14.Controls.Add(Me.Label244)
        Me.RadGroupBox14.Controls.Add(Me.Label245)
        Me.RadGroupBox14.Controls.Add(Me.Label246)
        Me.RadGroupBox14.Controls.Add(Me.Label247)
        Me.RadGroupBox14.Controls.Add(Me.Label248)
        Me.RadGroupBox14.Controls.Add(Me.RadPanel27)
        Me.RadGroupBox14.Controls.Add(Me.Label257)
        Me.RadGroupBox14.Controls.Add(Me.Label258)
        Me.RadGroupBox14.Controls.Add(Me.P13)
        Me.RadGroupBox14.Controls.Add(Me.Label259)
        Me.RadGroupBox14.Controls.Add(Me.Label260)
        Me.RadGroupBox14.Controls.Add(Me.Label261)
        Me.RadGroupBox14.Controls.Add(Me.Label262)
        Me.RadGroupBox14.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox14.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox14.HeaderText = ""
        Me.RadGroupBox14.Location = New System.Drawing.Point(1259, 495)
        Me.RadGroupBox14.Name = "RadGroupBox14"
        Me.RadGroupBox14.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox14.TabIndex = 78
        CType(Me.RadGroupBox14.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox14.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox14.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel26
        '
        Me.RadPanel26.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel26.Name = "RadPanel26"
        '
        '
        '
        Me.RadPanel26.RootElement.Shape = Me.QaShape1
        Me.RadPanel26.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel26.TabIndex = 44
        Me.RadPanel26.Text = "BAY 13"
        Me.RadPanel26.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel26.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 13"
        CType(Me.RadPanel26.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel26.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel26.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel26.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel26.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel26.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel26.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel26.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label243
        '
        Me.Label243.AutoSize = True
        Me.Label243.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "Customer_name", True))
        Me.Label243.ForeColor = System.Drawing.Color.Navy
        Me.Label243.Location = New System.Drawing.Point(358, 54)
        Me.Label243.Name = "Label243"
        Me.Label243.Size = New System.Drawing.Size(72, 16)
        Me.Label243.TabIndex = 72
        Me.Label243.Text = "Customer :"
        '
        'Label244
        '
        Me.Label244.AutoSize = True
        Me.Label244.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "Product_code", True))
        Me.Label244.ForeColor = System.Drawing.Color.Navy
        Me.Label244.Location = New System.Drawing.Point(358, 117)
        Me.Label244.Name = "Label244"
        Me.Label244.Size = New System.Drawing.Size(63, 16)
        Me.Label244.TabIndex = 71
        Me.Label244.Text = "Material :"
        '
        'Label245
        '
        Me.Label245.AutoSize = True
        Me.Label245.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "LOAD_PRESET", True))
        Me.Label245.ForeColor = System.Drawing.Color.Navy
        Me.Label245.Location = New System.Drawing.Point(358, 138)
        Me.Label245.Name = "Label245"
        Me.Label245.Size = New System.Drawing.Size(64, 16)
        Me.Label245.TabIndex = 70
        Me.Label245.Text = "Quantity :"
        '
        'Label246
        '
        Me.Label246.AutoSize = True
        Me.Label246.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "LOAD_DID", True))
        Me.Label246.ForeColor = System.Drawing.Color.Navy
        Me.Label246.Location = New System.Drawing.Point(358, 33)
        Me.Label246.Name = "Label246"
        Me.Label246.Size = New System.Drawing.Size(67, 16)
        Me.Label246.TabIndex = 68
        Me.Label246.Text = "Load No. :"
        '
        'Label247
        '
        Me.Label247.AutoSize = True
        Me.Label247.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "LOAD_DRIVER", True))
        Me.Label247.ForeColor = System.Drawing.Color.Navy
        Me.Label247.Location = New System.Drawing.Point(358, 96)
        Me.Label247.Name = "Label247"
        Me.Label247.Size = New System.Drawing.Size(51, 16)
        Me.Label247.TabIndex = 69
        Me.Label247.Text = "Driver :"
        '
        'Label248
        '
        Me.Label248.AutoSize = True
        Me.Label248.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "LOAD_VEHICLE", True))
        Me.Label248.ForeColor = System.Drawing.Color.Navy
        Me.Label248.Location = New System.Drawing.Point(358, 75)
        Me.Label248.Name = "Label248"
        Me.Label248.Size = New System.Drawing.Size(72, 16)
        Me.Label248.TabIndex = 67
        Me.Label248.Text = "Truck No. :"
        '
        'RadPanel27
        '
        Me.RadPanel27.Controls.Add(Me.Label249)
        Me.RadPanel27.Controls.Add(Me.Label250)
        Me.RadPanel27.Controls.Add(Me.Label251)
        Me.RadPanel27.Controls.Add(Me.Label252)
        Me.RadPanel27.Controls.Add(Me.RadSeparator12)
        Me.RadPanel27.Controls.Add(Me.Label253)
        Me.RadPanel27.Controls.Add(Me.Label254)
        Me.RadPanel27.Controls.Add(Me.Label255)
        Me.RadPanel27.Controls.Add(Me.Label256)
        Me.RadPanel27.Location = New System.Drawing.Point(5, 119)
        Me.RadPanel27.Name = "RadPanel27"
        '
        '
        '
        Me.RadPanel27.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel27.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel27.TabIndex = 66
        CType(Me.RadPanel27.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel27.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel27.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel27.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel27.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel27.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label249
        '
        Me.Label249.AutoSize = True
        Me.Label249.ForeColor = System.Drawing.Color.Black
        Me.Label249.Location = New System.Drawing.Point(200, 20)
        Me.Label249.Name = "Label249"
        Me.Label249.Size = New System.Drawing.Size(23, 13)
        Me.Label249.TabIndex = 70
        Me.Label249.Text = "Kg."
        '
        'Label250
        '
        Me.Label250.AutoSize = True
        Me.Label250.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "Raw_Weight_Out", True))
        Me.Label250.ForeColor = System.Drawing.Color.Navy
        Me.Label250.Location = New System.Drawing.Point(150, 20)
        Me.Label250.Name = "Label250"
        Me.Label250.Size = New System.Drawing.Size(45, 13)
        Me.Label250.TabIndex = 68
        Me.Label250.Text = "Weight"
        '
        'Label251
        '
        Me.Label251.AutoSize = True
        Me.Label251.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "WeightOut_time", True))
        Me.Label251.ForeColor = System.Drawing.Color.Navy
        Me.Label251.Location = New System.Drawing.Point(226, 20)
        Me.Label251.Name = "Label251"
        Me.Label251.Size = New System.Drawing.Size(30, 13)
        Me.Label251.TabIndex = 69
        Me.Label251.Text = "Time"
        '
        'Label252
        '
        Me.Label252.AutoSize = True
        Me.Label252.ForeColor = System.Drawing.Color.Black
        Me.Label252.Location = New System.Drawing.Point(65, 20)
        Me.Label252.Name = "Label252"
        Me.Label252.Size = New System.Drawing.Size(23, 13)
        Me.Label252.TabIndex = 67
        Me.Label252.Text = "Kg."
        '
        'RadSeparator12
        '
        Me.RadSeparator12.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator12.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator12.Name = "RadSeparator12"
        Me.RadSeparator12.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator12.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator12.TabIndex = 66
        Me.RadSeparator12.Text = "RadSeparator12"
        '
        'Label253
        '
        Me.Label253.AutoSize = True
        Me.Label253.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label253.Location = New System.Drawing.Point(66, 0)
        Me.Label253.Name = "Label253"
        Me.Label253.Size = New System.Drawing.Size(21, 16)
        Me.Label253.TabIndex = 61
        Me.Label253.Text = "In"
        '
        'Label254
        '
        Me.Label254.AutoSize = True
        Me.Label254.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "Raw_Weight_in", True))
        Me.Label254.ForeColor = System.Drawing.Color.Navy
        Me.Label254.Location = New System.Drawing.Point(15, 20)
        Me.Label254.Name = "Label254"
        Me.Label254.Size = New System.Drawing.Size(45, 13)
        Me.Label254.TabIndex = 60
        Me.Label254.Text = "Weight"
        '
        'Label255
        '
        Me.Label255.AutoSize = True
        Me.Label255.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label255.Location = New System.Drawing.Point(195, 1)
        Me.Label255.Name = "Label255"
        Me.Label255.Size = New System.Drawing.Size(31, 16)
        Me.Label255.TabIndex = 62
        Me.Label255.Text = "Out"
        '
        'Label256
        '
        Me.Label256.AutoSize = True
        Me.Label256.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource13, "Weightin_time", True))
        Me.Label256.ForeColor = System.Drawing.Color.Navy
        Me.Label256.Location = New System.Drawing.Point(91, 20)
        Me.Label256.Name = "Label256"
        Me.Label256.Size = New System.Drawing.Size(30, 13)
        Me.Label256.TabIndex = 63
        Me.Label256.Text = "Time"
        '
        'Label257
        '
        Me.Label257.AutoSize = True
        Me.Label257.Location = New System.Drawing.Point(280, 54)
        Me.Label257.Name = "Label257"
        Me.Label257.Size = New System.Drawing.Size(72, 16)
        Me.Label257.TabIndex = 51
        Me.Label257.Text = "Customer :"
        '
        'Label258
        '
        Me.Label258.AutoSize = True
        Me.Label258.Location = New System.Drawing.Point(289, 117)
        Me.Label258.Name = "Label258"
        Me.Label258.Size = New System.Drawing.Size(63, 16)
        Me.Label258.TabIndex = 45
        Me.Label258.Text = "Material :"
        '
        'P13
        '
        Me.P13.ErrorImage = Nothing
        Me.P13.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P13.Location = New System.Drawing.Point(5, -8)
        Me.P13.Name = "P13"
        Me.P13.Size = New System.Drawing.Size(275, 127)
        Me.P13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P13.TabIndex = 38
        Me.P13.TabStop = False
        Me.P13.Visible = False
        '
        'Label259
        '
        Me.Label259.AutoSize = True
        Me.Label259.Location = New System.Drawing.Point(288, 138)
        Me.Label259.Name = "Label259"
        Me.Label259.Size = New System.Drawing.Size(64, 16)
        Me.Label259.TabIndex = 44
        Me.Label259.Text = "Quantity :"
        '
        'Label260
        '
        Me.Label260.AutoSize = True
        Me.Label260.Location = New System.Drawing.Point(285, 33)
        Me.Label260.Name = "Label260"
        Me.Label260.Size = New System.Drawing.Size(67, 16)
        Me.Label260.TabIndex = 42
        Me.Label260.Text = "Load No. :"
        '
        'Label261
        '
        Me.Label261.AutoSize = True
        Me.Label261.Location = New System.Drawing.Point(301, 96)
        Me.Label261.Name = "Label261"
        Me.Label261.Size = New System.Drawing.Size(51, 16)
        Me.Label261.TabIndex = 43
        Me.Label261.Text = "Driver :"
        '
        'Label262
        '
        Me.Label262.AutoSize = True
        Me.Label262.Location = New System.Drawing.Point(280, 75)
        Me.Label262.Name = "Label262"
        Me.Label262.Size = New System.Drawing.Size(72, 16)
        Me.Label262.TabIndex = 41
        Me.Label262.Text = "Truck No. :"
        '
        'RadGroupBox15
        '
        Me.RadGroupBox15.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox15.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox15.Controls.Add(Me.RadPanel28)
        Me.RadGroupBox15.Controls.Add(Me.Label263)
        Me.RadGroupBox15.Controls.Add(Me.Label264)
        Me.RadGroupBox15.Controls.Add(Me.Label265)
        Me.RadGroupBox15.Controls.Add(Me.Label266)
        Me.RadGroupBox15.Controls.Add(Me.Label267)
        Me.RadGroupBox15.Controls.Add(Me.Label268)
        Me.RadGroupBox15.Controls.Add(Me.RadPanel29)
        Me.RadGroupBox15.Controls.Add(Me.Label277)
        Me.RadGroupBox15.Controls.Add(Me.Label278)
        Me.RadGroupBox15.Controls.Add(Me.P14)
        Me.RadGroupBox15.Controls.Add(Me.Label279)
        Me.RadGroupBox15.Controls.Add(Me.Label280)
        Me.RadGroupBox15.Controls.Add(Me.Label281)
        Me.RadGroupBox15.Controls.Add(Me.Label282)
        Me.RadGroupBox15.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox15.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox15.HeaderText = ""
        Me.RadGroupBox15.Location = New System.Drawing.Point(1259, 664)
        Me.RadGroupBox15.Name = "RadGroupBox15"
        Me.RadGroupBox15.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox15.TabIndex = 78
        CType(Me.RadGroupBox15.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox15.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox15.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel28
        '
        Me.RadPanel28.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel28.Name = "RadPanel28"
        '
        '
        '
        Me.RadPanel28.RootElement.Shape = Me.QaShape1
        Me.RadPanel28.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel28.TabIndex = 44
        Me.RadPanel28.Text = "BAY 14"
        Me.RadPanel28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel28.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 14"
        CType(Me.RadPanel28.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel28.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel28.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel28.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel28.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel28.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel28.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel28.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label263
        '
        Me.Label263.AutoSize = True
        Me.Label263.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "Customer_name", True))
        Me.Label263.ForeColor = System.Drawing.Color.Navy
        Me.Label263.Location = New System.Drawing.Point(358, 54)
        Me.Label263.Name = "Label263"
        Me.Label263.Size = New System.Drawing.Size(72, 16)
        Me.Label263.TabIndex = 72
        Me.Label263.Text = "Customer :"
        '
        'BindingSource14
        '
        Me.BindingSource14.DataMember = "LOADBAY"
        Me.BindingSource14.DataSource = Me.FPTDataSet
        '
        'Label264
        '
        Me.Label264.AutoSize = True
        Me.Label264.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "Product_code", True))
        Me.Label264.ForeColor = System.Drawing.Color.Navy
        Me.Label264.Location = New System.Drawing.Point(358, 117)
        Me.Label264.Name = "Label264"
        Me.Label264.Size = New System.Drawing.Size(63, 16)
        Me.Label264.TabIndex = 71
        Me.Label264.Text = "Material :"
        '
        'Label265
        '
        Me.Label265.AutoSize = True
        Me.Label265.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "LOAD_PRESET", True))
        Me.Label265.ForeColor = System.Drawing.Color.Navy
        Me.Label265.Location = New System.Drawing.Point(358, 138)
        Me.Label265.Name = "Label265"
        Me.Label265.Size = New System.Drawing.Size(64, 16)
        Me.Label265.TabIndex = 70
        Me.Label265.Text = "Quantity :"
        '
        'Label266
        '
        Me.Label266.AutoSize = True
        Me.Label266.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "LOAD_DID", True))
        Me.Label266.ForeColor = System.Drawing.Color.Navy
        Me.Label266.Location = New System.Drawing.Point(358, 33)
        Me.Label266.Name = "Label266"
        Me.Label266.Size = New System.Drawing.Size(67, 16)
        Me.Label266.TabIndex = 68
        Me.Label266.Text = "Load No. :"
        '
        'Label267
        '
        Me.Label267.AutoSize = True
        Me.Label267.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "LOAD_DRIVER", True))
        Me.Label267.ForeColor = System.Drawing.Color.Navy
        Me.Label267.Location = New System.Drawing.Point(358, 96)
        Me.Label267.Name = "Label267"
        Me.Label267.Size = New System.Drawing.Size(51, 16)
        Me.Label267.TabIndex = 69
        Me.Label267.Text = "Driver :"
        '
        'Label268
        '
        Me.Label268.AutoSize = True
        Me.Label268.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "LOAD_VEHICLE", True))
        Me.Label268.ForeColor = System.Drawing.Color.Navy
        Me.Label268.Location = New System.Drawing.Point(358, 75)
        Me.Label268.Name = "Label268"
        Me.Label268.Size = New System.Drawing.Size(72, 16)
        Me.Label268.TabIndex = 67
        Me.Label268.Text = "Truck No. :"
        '
        'RadPanel29
        '
        Me.RadPanel29.Controls.Add(Me.Label269)
        Me.RadPanel29.Controls.Add(Me.Label270)
        Me.RadPanel29.Controls.Add(Me.Label271)
        Me.RadPanel29.Controls.Add(Me.Label272)
        Me.RadPanel29.Controls.Add(Me.RadSeparator13)
        Me.RadPanel29.Controls.Add(Me.Label273)
        Me.RadPanel29.Controls.Add(Me.Label274)
        Me.RadPanel29.Controls.Add(Me.Label275)
        Me.RadPanel29.Controls.Add(Me.Label276)
        Me.RadPanel29.Location = New System.Drawing.Point(5, 118)
        Me.RadPanel29.Name = "RadPanel29"
        '
        '
        '
        Me.RadPanel29.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel29.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel29.TabIndex = 66
        CType(Me.RadPanel29.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel29.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel29.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel29.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel29.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel29.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label269
        '
        Me.Label269.AutoSize = True
        Me.Label269.ForeColor = System.Drawing.Color.Black
        Me.Label269.Location = New System.Drawing.Point(200, 20)
        Me.Label269.Name = "Label269"
        Me.Label269.Size = New System.Drawing.Size(23, 13)
        Me.Label269.TabIndex = 70
        Me.Label269.Text = "Kg."
        '
        'Label270
        '
        Me.Label270.AutoSize = True
        Me.Label270.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "Raw_Weight_Out", True))
        Me.Label270.ForeColor = System.Drawing.Color.Navy
        Me.Label270.Location = New System.Drawing.Point(150, 20)
        Me.Label270.Name = "Label270"
        Me.Label270.Size = New System.Drawing.Size(45, 13)
        Me.Label270.TabIndex = 68
        Me.Label270.Text = "Weight"
        '
        'Label271
        '
        Me.Label271.AutoSize = True
        Me.Label271.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "WeightOut_time", True))
        Me.Label271.ForeColor = System.Drawing.Color.Navy
        Me.Label271.Location = New System.Drawing.Point(226, 20)
        Me.Label271.Name = "Label271"
        Me.Label271.Size = New System.Drawing.Size(30, 13)
        Me.Label271.TabIndex = 69
        Me.Label271.Text = "Time"
        '
        'Label272
        '
        Me.Label272.AutoSize = True
        Me.Label272.ForeColor = System.Drawing.Color.Black
        Me.Label272.Location = New System.Drawing.Point(65, 20)
        Me.Label272.Name = "Label272"
        Me.Label272.Size = New System.Drawing.Size(23, 13)
        Me.Label272.TabIndex = 67
        Me.Label272.Text = "Kg."
        '
        'RadSeparator13
        '
        Me.RadSeparator13.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator13.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator13.Name = "RadSeparator13"
        Me.RadSeparator13.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator13.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator13.TabIndex = 66
        Me.RadSeparator13.Text = "RadSeparator13"
        '
        'Label273
        '
        Me.Label273.AutoSize = True
        Me.Label273.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label273.Location = New System.Drawing.Point(66, 0)
        Me.Label273.Name = "Label273"
        Me.Label273.Size = New System.Drawing.Size(21, 16)
        Me.Label273.TabIndex = 61
        Me.Label273.Text = "In"
        '
        'Label274
        '
        Me.Label274.AutoSize = True
        Me.Label274.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "Raw_Weight_in", True))
        Me.Label274.ForeColor = System.Drawing.Color.Navy
        Me.Label274.Location = New System.Drawing.Point(15, 20)
        Me.Label274.Name = "Label274"
        Me.Label274.Size = New System.Drawing.Size(45, 13)
        Me.Label274.TabIndex = 60
        Me.Label274.Text = "Weight"
        '
        'Label275
        '
        Me.Label275.AutoSize = True
        Me.Label275.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label275.Location = New System.Drawing.Point(195, 1)
        Me.Label275.Name = "Label275"
        Me.Label275.Size = New System.Drawing.Size(31, 16)
        Me.Label275.TabIndex = 62
        Me.Label275.Text = "Out"
        '
        'Label276
        '
        Me.Label276.AutoSize = True
        Me.Label276.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource14, "Weightin_time", True))
        Me.Label276.ForeColor = System.Drawing.Color.Navy
        Me.Label276.Location = New System.Drawing.Point(91, 20)
        Me.Label276.Name = "Label276"
        Me.Label276.Size = New System.Drawing.Size(30, 13)
        Me.Label276.TabIndex = 63
        Me.Label276.Text = "Time"
        '
        'Label277
        '
        Me.Label277.AutoSize = True
        Me.Label277.Location = New System.Drawing.Point(280, 54)
        Me.Label277.Name = "Label277"
        Me.Label277.Size = New System.Drawing.Size(72, 16)
        Me.Label277.TabIndex = 51
        Me.Label277.Text = "Customer :"
        '
        'Label278
        '
        Me.Label278.AutoSize = True
        Me.Label278.Location = New System.Drawing.Point(289, 117)
        Me.Label278.Name = "Label278"
        Me.Label278.Size = New System.Drawing.Size(63, 16)
        Me.Label278.TabIndex = 45
        Me.Label278.Text = "Material :"
        '
        'P14
        '
        Me.P14.ErrorImage = Nothing
        Me.P14.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P14.Location = New System.Drawing.Point(5, -8)
        Me.P14.Name = "P14"
        Me.P14.Size = New System.Drawing.Size(275, 127)
        Me.P14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P14.TabIndex = 38
        Me.P14.TabStop = False
        Me.P14.Visible = False
        '
        'Label279
        '
        Me.Label279.AutoSize = True
        Me.Label279.Location = New System.Drawing.Point(288, 138)
        Me.Label279.Name = "Label279"
        Me.Label279.Size = New System.Drawing.Size(64, 16)
        Me.Label279.TabIndex = 44
        Me.Label279.Text = "Quantity :"
        '
        'Label280
        '
        Me.Label280.AutoSize = True
        Me.Label280.Location = New System.Drawing.Point(285, 33)
        Me.Label280.Name = "Label280"
        Me.Label280.Size = New System.Drawing.Size(67, 16)
        Me.Label280.TabIndex = 42
        Me.Label280.Text = "Load No. :"
        '
        'Label281
        '
        Me.Label281.AutoSize = True
        Me.Label281.Location = New System.Drawing.Point(301, 96)
        Me.Label281.Name = "Label281"
        Me.Label281.Size = New System.Drawing.Size(51, 16)
        Me.Label281.TabIndex = 43
        Me.Label281.Text = "Driver :"
        '
        'Label282
        '
        Me.Label282.AutoSize = True
        Me.Label282.Location = New System.Drawing.Point(280, 75)
        Me.Label282.Name = "Label282"
        Me.Label282.Size = New System.Drawing.Size(72, 16)
        Me.Label282.TabIndex = 41
        Me.Label282.Text = "Truck No. :"
        '
        'RadGroupBox16
        '
        Me.RadGroupBox16.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox16.BackColor = System.Drawing.Color.Transparent
        Me.RadGroupBox16.Controls.Add(Me.RadPanel30)
        Me.RadGroupBox16.Controls.Add(Me.Label283)
        Me.RadGroupBox16.Controls.Add(Me.Label284)
        Me.RadGroupBox16.Controls.Add(Me.Label285)
        Me.RadGroupBox16.Controls.Add(Me.Label286)
        Me.RadGroupBox16.Controls.Add(Me.Label287)
        Me.RadGroupBox16.Controls.Add(Me.Label288)
        Me.RadGroupBox16.Controls.Add(Me.RadPanel31)
        Me.RadGroupBox16.Controls.Add(Me.Label297)
        Me.RadGroupBox16.Controls.Add(Me.Label298)
        Me.RadGroupBox16.Controls.Add(Me.P15)
        Me.RadGroupBox16.Controls.Add(Me.Label299)
        Me.RadGroupBox16.Controls.Add(Me.Label300)
        Me.RadGroupBox16.Controls.Add(Me.Label301)
        Me.RadGroupBox16.Controls.Add(Me.Label302)
        Me.RadGroupBox16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox16.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        Me.RadGroupBox16.HeaderText = ""
        Me.RadGroupBox16.Location = New System.Drawing.Point(1259, 833)
        Me.RadGroupBox16.Name = "RadGroupBox16"
        Me.RadGroupBox16.Size = New System.Drawing.Size(526, 158)
        Me.RadGroupBox16.TabIndex = 79
        CType(Me.RadGroupBox16.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center
        CType(Me.RadGroupBox16.GetChildAt(0), Telerik.WinControls.UI.RadGroupBoxElement).Shape = Nothing
        CType(Me.RadGroupBox16.GetChildAt(0).GetChildAt(0), Telerik.WinControls.UI.GroupBoxContent).Shape = Me.RoundRectShape1
        '
        'RadPanel30
        '
        Me.RadPanel30.Location = New System.Drawing.Point(272, 3)
        Me.RadPanel30.Name = "RadPanel30"
        '
        '
        '
        Me.RadPanel30.RootElement.Shape = Me.QaShape1
        Me.RadPanel30.Size = New System.Drawing.Size(251, 26)
        Me.RadPanel30.TabIndex = 44
        Me.RadPanel30.Text = "BAY 15"
        Me.RadPanel30.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        CType(Me.RadPanel30.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = "BAY 15"
        CType(Me.RadPanel30.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).ForeColor = System.Drawing.SystemColors.Window
        CType(Me.RadPanel30.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).BackColor = System.Drawing.Color.Transparent
        CType(Me.RadPanel30.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CType(Me.RadPanel30.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.QaShape1
        CType(Me.RadPanel30.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel30.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel30.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(171, Byte), Integer))
        CType(Me.RadPanel30.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(245, Byte), Integer))
        '
        'Label283
        '
        Me.Label283.AutoSize = True
        Me.Label283.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "Customer_name", True))
        Me.Label283.ForeColor = System.Drawing.Color.Navy
        Me.Label283.Location = New System.Drawing.Point(358, 54)
        Me.Label283.Name = "Label283"
        Me.Label283.Size = New System.Drawing.Size(72, 16)
        Me.Label283.TabIndex = 72
        Me.Label283.Text = "Customer :"
        '
        'BindingSource15
        '
        Me.BindingSource15.DataMember = "LOADBAY"
        Me.BindingSource15.DataSource = Me.FPTDataSet
        '
        'Label284
        '
        Me.Label284.AutoSize = True
        Me.Label284.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "Product_code", True))
        Me.Label284.ForeColor = System.Drawing.Color.Navy
        Me.Label284.Location = New System.Drawing.Point(358, 117)
        Me.Label284.Name = "Label284"
        Me.Label284.Size = New System.Drawing.Size(63, 16)
        Me.Label284.TabIndex = 71
        Me.Label284.Text = "Material :"
        '
        'Label285
        '
        Me.Label285.AutoSize = True
        Me.Label285.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "LOAD_PRESET", True))
        Me.Label285.ForeColor = System.Drawing.Color.Navy
        Me.Label285.Location = New System.Drawing.Point(358, 138)
        Me.Label285.Name = "Label285"
        Me.Label285.Size = New System.Drawing.Size(64, 16)
        Me.Label285.TabIndex = 70
        Me.Label285.Text = "Quantity :"
        '
        'Label286
        '
        Me.Label286.AutoSize = True
        Me.Label286.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "LOAD_DID", True))
        Me.Label286.ForeColor = System.Drawing.Color.Navy
        Me.Label286.Location = New System.Drawing.Point(358, 33)
        Me.Label286.Name = "Label286"
        Me.Label286.Size = New System.Drawing.Size(67, 16)
        Me.Label286.TabIndex = 68
        Me.Label286.Text = "Load No. :"
        '
        'Label287
        '
        Me.Label287.AutoSize = True
        Me.Label287.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "LOAD_DRIVER", True))
        Me.Label287.ForeColor = System.Drawing.Color.Navy
        Me.Label287.Location = New System.Drawing.Point(358, 96)
        Me.Label287.Name = "Label287"
        Me.Label287.Size = New System.Drawing.Size(51, 16)
        Me.Label287.TabIndex = 69
        Me.Label287.Text = "Driver :"
        '
        'Label288
        '
        Me.Label288.AutoSize = True
        Me.Label288.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "LOAD_VEHICLE", True))
        Me.Label288.ForeColor = System.Drawing.Color.Navy
        Me.Label288.Location = New System.Drawing.Point(358, 75)
        Me.Label288.Name = "Label288"
        Me.Label288.Size = New System.Drawing.Size(72, 16)
        Me.Label288.TabIndex = 67
        Me.Label288.Text = "Truck No. :"
        '
        'RadPanel31
        '
        Me.RadPanel31.Controls.Add(Me.Label289)
        Me.RadPanel31.Controls.Add(Me.Label290)
        Me.RadPanel31.Controls.Add(Me.Label291)
        Me.RadPanel31.Controls.Add(Me.Label292)
        Me.RadPanel31.Controls.Add(Me.RadSeparator14)
        Me.RadPanel31.Controls.Add(Me.Label293)
        Me.RadPanel31.Controls.Add(Me.Label294)
        Me.RadPanel31.Controls.Add(Me.Label295)
        Me.RadPanel31.Controls.Add(Me.Label296)
        Me.RadPanel31.Location = New System.Drawing.Point(5, 118)
        Me.RadPanel31.Name = "RadPanel31"
        '
        '
        '
        Me.RadPanel31.RootElement.Shape = Me.RoundRectShape1
        Me.RadPanel31.Size = New System.Drawing.Size(275, 36)
        Me.RadPanel31.TabIndex = 66
        CType(Me.RadPanel31.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Text = ""
        CType(Me.RadPanel31.GetChildAt(0), Telerik.WinControls.UI.RadPanelElement).Shape = Me.RoundRectShape1
        CType(Me.RadPanel31.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor2 = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel31.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor3 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel31.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor4 = System.Drawing.Color.FromArgb(CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        CType(Me.RadPanel31.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).BackColor = System.Drawing.Color.White
        '
        'Label289
        '
        Me.Label289.AutoSize = True
        Me.Label289.ForeColor = System.Drawing.Color.Black
        Me.Label289.Location = New System.Drawing.Point(200, 20)
        Me.Label289.Name = "Label289"
        Me.Label289.Size = New System.Drawing.Size(23, 13)
        Me.Label289.TabIndex = 70
        Me.Label289.Text = "Kg."
        '
        'Label290
        '
        Me.Label290.AutoSize = True
        Me.Label290.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "Raw_Weight_Out", True))
        Me.Label290.ForeColor = System.Drawing.Color.Navy
        Me.Label290.Location = New System.Drawing.Point(150, 20)
        Me.Label290.Name = "Label290"
        Me.Label290.Size = New System.Drawing.Size(45, 13)
        Me.Label290.TabIndex = 68
        Me.Label290.Text = "Weight"
        '
        'Label291
        '
        Me.Label291.AutoSize = True
        Me.Label291.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "WeightOut_time", True))
        Me.Label291.ForeColor = System.Drawing.Color.Navy
        Me.Label291.Location = New System.Drawing.Point(226, 20)
        Me.Label291.Name = "Label291"
        Me.Label291.Size = New System.Drawing.Size(30, 13)
        Me.Label291.TabIndex = 69
        Me.Label291.Text = "Time"
        '
        'Label292
        '
        Me.Label292.AutoSize = True
        Me.Label292.ForeColor = System.Drawing.Color.Black
        Me.Label292.Location = New System.Drawing.Point(65, 20)
        Me.Label292.Name = "Label292"
        Me.Label292.Size = New System.Drawing.Size(23, 13)
        Me.Label292.TabIndex = 67
        Me.Label292.Text = "Kg."
        '
        'RadSeparator14
        '
        Me.RadSeparator14.BackColor = System.Drawing.Color.Transparent
        Me.RadSeparator14.Location = New System.Drawing.Point(135, 1)
        Me.RadSeparator14.Name = "RadSeparator14"
        Me.RadSeparator14.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RadSeparator14.Size = New System.Drawing.Size(4, 33)
        Me.RadSeparator14.TabIndex = 66
        Me.RadSeparator14.Text = "RadSeparator14"
        '
        'Label293
        '
        Me.Label293.AutoSize = True
        Me.Label293.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label293.Location = New System.Drawing.Point(66, 0)
        Me.Label293.Name = "Label293"
        Me.Label293.Size = New System.Drawing.Size(21, 16)
        Me.Label293.TabIndex = 61
        Me.Label293.Text = "In"
        '
        'Label294
        '
        Me.Label294.AutoSize = True
        Me.Label294.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "Raw_Weight_in", True))
        Me.Label294.ForeColor = System.Drawing.Color.Navy
        Me.Label294.Location = New System.Drawing.Point(15, 20)
        Me.Label294.Name = "Label294"
        Me.Label294.Size = New System.Drawing.Size(45, 13)
        Me.Label294.TabIndex = 60
        Me.Label294.Text = "Weight"
        '
        'Label295
        '
        Me.Label295.AutoSize = True
        Me.Label295.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label295.Location = New System.Drawing.Point(195, 1)
        Me.Label295.Name = "Label295"
        Me.Label295.Size = New System.Drawing.Size(31, 16)
        Me.Label295.TabIndex = 62
        Me.Label295.Text = "Out"
        '
        'Label296
        '
        Me.Label296.AutoSize = True
        Me.Label296.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource15, "Weightin_time", True))
        Me.Label296.ForeColor = System.Drawing.Color.Navy
        Me.Label296.Location = New System.Drawing.Point(91, 20)
        Me.Label296.Name = "Label296"
        Me.Label296.Size = New System.Drawing.Size(30, 13)
        Me.Label296.TabIndex = 63
        Me.Label296.Text = "Time"
        '
        'Label297
        '
        Me.Label297.AutoSize = True
        Me.Label297.Location = New System.Drawing.Point(280, 54)
        Me.Label297.Name = "Label297"
        Me.Label297.Size = New System.Drawing.Size(72, 16)
        Me.Label297.TabIndex = 51
        Me.Label297.Text = "Customer :"
        '
        'Label298
        '
        Me.Label298.AutoSize = True
        Me.Label298.Location = New System.Drawing.Point(289, 117)
        Me.Label298.Name = "Label298"
        Me.Label298.Size = New System.Drawing.Size(63, 16)
        Me.Label298.TabIndex = 45
        Me.Label298.Text = "Material :"
        '
        'P15
        '
        Me.P15.ErrorImage = Nothing
        Me.P15.Image = Global.TAS_TOL.My.Resources.Resources.Social_Truck_facebook3
        Me.P15.Location = New System.Drawing.Point(5, -8)
        Me.P15.Name = "P15"
        Me.P15.Size = New System.Drawing.Size(275, 127)
        Me.P15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.P15.TabIndex = 38
        Me.P15.TabStop = False
        Me.P15.Visible = False
        '
        'Label299
        '
        Me.Label299.AutoSize = True
        Me.Label299.Location = New System.Drawing.Point(288, 138)
        Me.Label299.Name = "Label299"
        Me.Label299.Size = New System.Drawing.Size(64, 16)
        Me.Label299.TabIndex = 44
        Me.Label299.Text = "Quantity :"
        '
        'Label300
        '
        Me.Label300.AutoSize = True
        Me.Label300.Location = New System.Drawing.Point(285, 33)
        Me.Label300.Name = "Label300"
        Me.Label300.Size = New System.Drawing.Size(67, 16)
        Me.Label300.TabIndex = 42
        Me.Label300.Text = "Load No. :"
        '
        'Label301
        '
        Me.Label301.AutoSize = True
        Me.Label301.Location = New System.Drawing.Point(301, 96)
        Me.Label301.Name = "Label301"
        Me.Label301.Size = New System.Drawing.Size(51, 16)
        Me.Label301.TabIndex = 43
        Me.Label301.Text = "Driver :"
        '
        'Label302
        '
        Me.Label302.AutoSize = True
        Me.Label302.Location = New System.Drawing.Point(280, 75)
        Me.Label302.Name = "Label302"
        Me.Label302.Size = New System.Drawing.Size(72, 16)
        Me.Label302.TabIndex = 41
        Me.Label302.Text = "Truck No. :"
        '
        'BayOverview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1358, 695)
        Me.Controls.Add(Me.RadGroupBox16)
        Me.Controls.Add(Me.RadGroupBox15)
        Me.Controls.Add(Me.RadGroupBox14)
        Me.Controls.Add(Me.RadGroupBox13)
        Me.Controls.Add(Me.RadGroupBox12)
        Me.Controls.Add(Me.RadGroupBox11)
        Me.Controls.Add(Me.RadGroupBox10)
        Me.Controls.Add(Me.RadGroupBox8)
        Me.Controls.Add(Me.RadGroupBox7)
        Me.Controls.Add(Me.RadGroupBox6)
        Me.Controls.Add(Me.RadGroupBox5)
        Me.Controls.Add(Me.RadGroupBox4)
        Me.Controls.Add(Me.RadGroupBox3)
        Me.Controls.Add(Me.RadGroupBox2)
        Me.Controls.Add(Me.RadGroupBox1)
        Me.Controls.Add(Me.RadGroupBox9)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "BayOverview"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.Tag = "22"
        Me.Text = "MMI"
        Me.ThemeName = "Office2010Blue"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RadGroupBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox9.ResumeLayout(False)
        Me.RadGroupBox9.PerformLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel2.ResumeLayout(False)
        Me.RadPanel2.PerformLayout()
        CType(Me.IMCus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadClock1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadCalendar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FPTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LOADBAYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        Me.RadGroupBox1.PerformLayout()
        CType(Me.RadPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel1.ResumeLayout(False)
        Me.RadPanel1.PerformLayout()
        CType(Me.RadSeparator46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox2.ResumeLayout(False)
        Me.RadGroupBox2.PerformLayout()
        CType(Me.RadPanel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel5.ResumeLayout(False)
        Me.RadPanel5.PerformLayout()
        CType(Me.RadSeparator1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox3.ResumeLayout(False)
        Me.RadGroupBox3.PerformLayout()
        CType(Me.RadPanel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel7.ResumeLayout(False)
        Me.RadPanel7.PerformLayout()
        CType(Me.RadSeparator2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox4.ResumeLayout(False)
        Me.RadGroupBox4.PerformLayout()
        CType(Me.RadPanel8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel9.ResumeLayout(False)
        Me.RadPanel9.PerformLayout()
        CType(Me.RadSeparator3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox5.ResumeLayout(False)
        Me.RadGroupBox5.PerformLayout()
        CType(Me.RadPanel10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel11.ResumeLayout(False)
        Me.RadPanel11.PerformLayout()
        CType(Me.RadSeparator4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox6.ResumeLayout(False)
        Me.RadGroupBox6.PerformLayout()
        CType(Me.RadPanel12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel13.ResumeLayout(False)
        Me.RadPanel13.PerformLayout()
        CType(Me.RadSeparator5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox7.ResumeLayout(False)
        Me.RadGroupBox7.PerformLayout()
        CType(Me.RadPanel14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel15.ResumeLayout(False)
        Me.RadPanel15.PerformLayout()
        CType(Me.RadSeparator6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox8.ResumeLayout(False)
        Me.RadGroupBox8.PerformLayout()
        CType(Me.RadPanel16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel17.ResumeLayout(False)
        Me.RadPanel17.PerformLayout()
        CType(Me.RadSeparator7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox10.ResumeLayout(False)
        Me.RadGroupBox10.PerformLayout()
        CType(Me.RadPanel18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel19.ResumeLayout(False)
        Me.RadPanel19.PerformLayout()
        CType(Me.RadSeparator8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox11.ResumeLayout(False)
        Me.RadGroupBox11.PerformLayout()
        CType(Me.RadPanel20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel21.ResumeLayout(False)
        Me.RadPanel21.PerformLayout()
        CType(Me.RadSeparator9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox12.ResumeLayout(False)
        Me.RadGroupBox12.PerformLayout()
        CType(Me.RadPanel22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel23.ResumeLayout(False)
        Me.RadPanel23.PerformLayout()
        CType(Me.RadSeparator10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox13.ResumeLayout(False)
        Me.RadGroupBox13.PerformLayout()
        CType(Me.RadPanel24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel25.ResumeLayout(False)
        Me.RadPanel25.PerformLayout()
        CType(Me.RadSeparator11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox14.ResumeLayout(False)
        Me.RadGroupBox14.PerformLayout()
        CType(Me.RadPanel26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel27, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel27.ResumeLayout(False)
        Me.RadPanel27.PerformLayout()
        CType(Me.RadSeparator12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox15.ResumeLayout(False)
        Me.RadGroupBox15.PerformLayout()
        CType(Me.RadPanel28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel29, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel29.ResumeLayout(False)
        Me.RadPanel29.PerformLayout()
        CType(Me.RadSeparator13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox16.ResumeLayout(False)
        Me.RadGroupBox16.PerformLayout()
        CType(Me.RadPanel30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadPanel31, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPanel31.ResumeLayout(False)
        Me.RadPanel31.PerformLayout()
        CType(Me.RadSeparator14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.P15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadGroupBox9 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadPanel2 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents W_DATETIME As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents weight As System.Windows.Forms.Label
    Friend WithEvents weight_Unit As System.Windows.Forms.Label
    Friend WithEvents IMCus As System.Windows.Forms.PictureBox
    Friend WithEvents RadClock1 As Telerik.WinControls.UI.RadClock
    Friend WithEvents RadCalendar1 As Telerik.WinControls.UI.RadCalendar
    Friend WithEvents P1 As System.Windows.Forms.PictureBox
    Private WithEvents CustomShape1 As Telerik.WinControls.CustomShape
    Private WithEvents CustomShape2 As Telerik.WinControls.CustomShape
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents RadPanel1 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents RadPanel3 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents EllipseShape1 As Telerik.WinControls.EllipseShape
    Friend WithEvents TabOffice12Shape1 As Telerik.WinControls.UI.TabOffice12Shape
    Friend WithEvents TabVsShape1 As Telerik.WinControls.UI.TabVsShape
    Friend WithEvents TrackBarDThumbShape1 As Telerik.WinControls.UI.TrackBarDThumbShape
    Friend WithEvents TrackBarLThumbShape1 As Telerik.WinControls.UI.TrackBarLThumbShape
    Friend WithEvents TrackBarRThumbShape1 As Telerik.WinControls.UI.TrackBarRThumbShape
    Friend WithEvents TrackBarUThumbShape1 As Telerik.WinControls.UI.TrackBarUThumbShape
    Friend WithEvents RadSeparator46 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents QaShape1 As Telerik.WinControls.Tests.QAShape
    Friend WithEvents RoundRectShape1 As Telerik.WinControls.RoundRectShape
    Friend WithEvents MediaShape1 As Telerik.WinControls.Tests.MediaShape
    Friend WithEvents FPTDataSet As TAS_TOL.FPTDataSet
    Friend WithEvents LOADBAYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LOADBAYTableAdapter As TAS_TOL.FPTDataSetTableAdapters.LOADBAYTableAdapter
    Friend WithEvents RadGroupBox2 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel4 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents RadPanel5 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator1 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents P2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource3 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource4 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource5 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource6 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource7 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource8 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource9 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource10 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource11 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource12 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource13 As System.Windows.Forms.BindingSource
    Friend WithEvents RadGroupBox3 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel6 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents RadPanel7 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator2 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents P3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox4 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel8 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents RadPanel9 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator3 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents P4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox5 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel10 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents RadPanel11 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator4 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents P5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox6 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel12 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents RadPanel13 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator5 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents Label115 As System.Windows.Forms.Label
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents Label117 As System.Windows.Forms.Label
    Friend WithEvents Label118 As System.Windows.Forms.Label
    Friend WithEvents P6 As System.Windows.Forms.PictureBox
    Friend WithEvents Label119 As System.Windows.Forms.Label
    Friend WithEvents Label120 As System.Windows.Forms.Label
    Friend WithEvents Label121 As System.Windows.Forms.Label
    Friend WithEvents Label122 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox7 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel14 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label123 As System.Windows.Forms.Label
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents Label125 As System.Windows.Forms.Label
    Friend WithEvents Label126 As System.Windows.Forms.Label
    Friend WithEvents Label127 As System.Windows.Forms.Label
    Friend WithEvents Label128 As System.Windows.Forms.Label
    Friend WithEvents RadPanel15 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label129 As System.Windows.Forms.Label
    Friend WithEvents Label130 As System.Windows.Forms.Label
    Friend WithEvents Label131 As System.Windows.Forms.Label
    Friend WithEvents Label132 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator6 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label133 As System.Windows.Forms.Label
    Friend WithEvents Label134 As System.Windows.Forms.Label
    Friend WithEvents Label135 As System.Windows.Forms.Label
    Friend WithEvents Label136 As System.Windows.Forms.Label
    Friend WithEvents Label137 As System.Windows.Forms.Label
    Friend WithEvents Label138 As System.Windows.Forms.Label
    Friend WithEvents P7 As System.Windows.Forms.PictureBox
    Friend WithEvents Label139 As System.Windows.Forms.Label
    Friend WithEvents Label140 As System.Windows.Forms.Label
    Friend WithEvents Label141 As System.Windows.Forms.Label
    Friend WithEvents Label142 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox8 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel16 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label143 As System.Windows.Forms.Label
    Friend WithEvents Label144 As System.Windows.Forms.Label
    Friend WithEvents Label145 As System.Windows.Forms.Label
    Friend WithEvents Label146 As System.Windows.Forms.Label
    Friend WithEvents Label147 As System.Windows.Forms.Label
    Friend WithEvents Label148 As System.Windows.Forms.Label
    Friend WithEvents RadPanel17 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label149 As System.Windows.Forms.Label
    Friend WithEvents Label150 As System.Windows.Forms.Label
    Friend WithEvents Label151 As System.Windows.Forms.Label
    Friend WithEvents Label152 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator7 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label153 As System.Windows.Forms.Label
    Friend WithEvents Label154 As System.Windows.Forms.Label
    Friend WithEvents Label155 As System.Windows.Forms.Label
    Friend WithEvents Label156 As System.Windows.Forms.Label
    Friend WithEvents Label157 As System.Windows.Forms.Label
    Friend WithEvents Label158 As System.Windows.Forms.Label
    Friend WithEvents P8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label159 As System.Windows.Forms.Label
    Friend WithEvents Label160 As System.Windows.Forms.Label
    Friend WithEvents Label161 As System.Windows.Forms.Label
    Friend WithEvents Label162 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox10 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel18 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label163 As System.Windows.Forms.Label
    Friend WithEvents Label164 As System.Windows.Forms.Label
    Friend WithEvents Label165 As System.Windows.Forms.Label
    Friend WithEvents Label166 As System.Windows.Forms.Label
    Friend WithEvents Label167 As System.Windows.Forms.Label
    Friend WithEvents Label168 As System.Windows.Forms.Label
    Friend WithEvents RadPanel19 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label169 As System.Windows.Forms.Label
    Friend WithEvents Label170 As System.Windows.Forms.Label
    Friend WithEvents Label171 As System.Windows.Forms.Label
    Friend WithEvents Label172 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator8 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label173 As System.Windows.Forms.Label
    Friend WithEvents Label174 As System.Windows.Forms.Label
    Friend WithEvents Label175 As System.Windows.Forms.Label
    Friend WithEvents Label176 As System.Windows.Forms.Label
    Friend WithEvents Label177 As System.Windows.Forms.Label
    Friend WithEvents Label178 As System.Windows.Forms.Label
    Friend WithEvents P9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label179 As System.Windows.Forms.Label
    Friend WithEvents Label180 As System.Windows.Forms.Label
    Friend WithEvents Label181 As System.Windows.Forms.Label
    Friend WithEvents Label182 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox11 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel20 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label183 As System.Windows.Forms.Label
    Friend WithEvents Label184 As System.Windows.Forms.Label
    Friend WithEvents Label185 As System.Windows.Forms.Label
    Friend WithEvents Label186 As System.Windows.Forms.Label
    Friend WithEvents Label187 As System.Windows.Forms.Label
    Friend WithEvents Label188 As System.Windows.Forms.Label
    Friend WithEvents RadPanel21 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label189 As System.Windows.Forms.Label
    Friend WithEvents Label190 As System.Windows.Forms.Label
    Friend WithEvents Label191 As System.Windows.Forms.Label
    Friend WithEvents Label192 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator9 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label193 As System.Windows.Forms.Label
    Friend WithEvents Label194 As System.Windows.Forms.Label
    Friend WithEvents Label195 As System.Windows.Forms.Label
    Friend WithEvents Label196 As System.Windows.Forms.Label
    Friend WithEvents Label197 As System.Windows.Forms.Label
    Friend WithEvents Label198 As System.Windows.Forms.Label
    Friend WithEvents P10 As System.Windows.Forms.PictureBox
    Friend WithEvents Label199 As System.Windows.Forms.Label
    Friend WithEvents Label200 As System.Windows.Forms.Label
    Friend WithEvents Label201 As System.Windows.Forms.Label
    Friend WithEvents Label202 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox12 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel22 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label203 As System.Windows.Forms.Label
    Friend WithEvents Label204 As System.Windows.Forms.Label
    Friend WithEvents Label205 As System.Windows.Forms.Label
    Friend WithEvents Label206 As System.Windows.Forms.Label
    Friend WithEvents Label207 As System.Windows.Forms.Label
    Friend WithEvents Label208 As System.Windows.Forms.Label
    Friend WithEvents RadPanel23 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label209 As System.Windows.Forms.Label
    Friend WithEvents Label210 As System.Windows.Forms.Label
    Friend WithEvents Label211 As System.Windows.Forms.Label
    Friend WithEvents Label212 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator10 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label213 As System.Windows.Forms.Label
    Friend WithEvents Label214 As System.Windows.Forms.Label
    Friend WithEvents Label215 As System.Windows.Forms.Label
    Friend WithEvents Label216 As System.Windows.Forms.Label
    Friend WithEvents Label217 As System.Windows.Forms.Label
    Friend WithEvents Label218 As System.Windows.Forms.Label
    Friend WithEvents P11 As System.Windows.Forms.PictureBox
    Friend WithEvents Label219 As System.Windows.Forms.Label
    Friend WithEvents Label220 As System.Windows.Forms.Label
    Friend WithEvents Label221 As System.Windows.Forms.Label
    Friend WithEvents Label222 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox13 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel24 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label223 As System.Windows.Forms.Label
    Friend WithEvents Label224 As System.Windows.Forms.Label
    Friend WithEvents Label225 As System.Windows.Forms.Label
    Friend WithEvents Label226 As System.Windows.Forms.Label
    Friend WithEvents Label227 As System.Windows.Forms.Label
    Friend WithEvents Label228 As System.Windows.Forms.Label
    Friend WithEvents RadPanel25 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label229 As System.Windows.Forms.Label
    Friend WithEvents Label230 As System.Windows.Forms.Label
    Friend WithEvents Label231 As System.Windows.Forms.Label
    Friend WithEvents Label232 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator11 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label233 As System.Windows.Forms.Label
    Friend WithEvents Label234 As System.Windows.Forms.Label
    Friend WithEvents Label235 As System.Windows.Forms.Label
    Friend WithEvents Label236 As System.Windows.Forms.Label
    Friend WithEvents Label237 As System.Windows.Forms.Label
    Friend WithEvents Label238 As System.Windows.Forms.Label
    Friend WithEvents P12 As System.Windows.Forms.PictureBox
    Friend WithEvents Label239 As System.Windows.Forms.Label
    Friend WithEvents Label240 As System.Windows.Forms.Label
    Friend WithEvents Label241 As System.Windows.Forms.Label
    Friend WithEvents Label242 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox14 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel26 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label243 As System.Windows.Forms.Label
    Friend WithEvents Label244 As System.Windows.Forms.Label
    Friend WithEvents Label245 As System.Windows.Forms.Label
    Friend WithEvents Label246 As System.Windows.Forms.Label
    Friend WithEvents Label247 As System.Windows.Forms.Label
    Friend WithEvents Label248 As System.Windows.Forms.Label
    Friend WithEvents RadPanel27 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label249 As System.Windows.Forms.Label
    Friend WithEvents Label250 As System.Windows.Forms.Label
    Friend WithEvents Label251 As System.Windows.Forms.Label
    Friend WithEvents Label252 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator12 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label253 As System.Windows.Forms.Label
    Friend WithEvents Label254 As System.Windows.Forms.Label
    Friend WithEvents Label255 As System.Windows.Forms.Label
    Friend WithEvents Label256 As System.Windows.Forms.Label
    Friend WithEvents Label257 As System.Windows.Forms.Label
    Friend WithEvents Label258 As System.Windows.Forms.Label
    Friend WithEvents P13 As System.Windows.Forms.PictureBox
    Friend WithEvents Label259 As System.Windows.Forms.Label
    Friend WithEvents Label260 As System.Windows.Forms.Label
    Friend WithEvents Label261 As System.Windows.Forms.Label
    Friend WithEvents Label262 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox15 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel28 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label263 As System.Windows.Forms.Label
    Friend WithEvents Label264 As System.Windows.Forms.Label
    Friend WithEvents Label265 As System.Windows.Forms.Label
    Friend WithEvents Label266 As System.Windows.Forms.Label
    Friend WithEvents Label267 As System.Windows.Forms.Label
    Friend WithEvents Label268 As System.Windows.Forms.Label
    Friend WithEvents RadPanel29 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label269 As System.Windows.Forms.Label
    Friend WithEvents Label270 As System.Windows.Forms.Label
    Friend WithEvents Label271 As System.Windows.Forms.Label
    Friend WithEvents Label272 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator13 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label273 As System.Windows.Forms.Label
    Friend WithEvents Label274 As System.Windows.Forms.Label
    Friend WithEvents Label275 As System.Windows.Forms.Label
    Friend WithEvents Label276 As System.Windows.Forms.Label
    Friend WithEvents Label277 As System.Windows.Forms.Label
    Friend WithEvents Label278 As System.Windows.Forms.Label
    Friend WithEvents P14 As System.Windows.Forms.PictureBox
    Friend WithEvents Label279 As System.Windows.Forms.Label
    Friend WithEvents Label280 As System.Windows.Forms.Label
    Friend WithEvents Label281 As System.Windows.Forms.Label
    Friend WithEvents Label282 As System.Windows.Forms.Label
    Friend WithEvents RadGroupBox16 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadPanel30 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label283 As System.Windows.Forms.Label
    Friend WithEvents Label284 As System.Windows.Forms.Label
    Friend WithEvents Label285 As System.Windows.Forms.Label
    Friend WithEvents Label286 As System.Windows.Forms.Label
    Friend WithEvents Label287 As System.Windows.Forms.Label
    Friend WithEvents Label288 As System.Windows.Forms.Label
    Friend WithEvents RadPanel31 As Telerik.WinControls.UI.RadPanel
    Friend WithEvents Label289 As System.Windows.Forms.Label
    Friend WithEvents Label290 As System.Windows.Forms.Label
    Friend WithEvents Label291 As System.Windows.Forms.Label
    Friend WithEvents Label292 As System.Windows.Forms.Label
    Friend WithEvents RadSeparator14 As Telerik.WinControls.UI.RadSeparator
    Friend WithEvents Label293 As System.Windows.Forms.Label
    Friend WithEvents Label294 As System.Windows.Forms.Label
    Friend WithEvents Label295 As System.Windows.Forms.Label
    Friend WithEvents Label296 As System.Windows.Forms.Label
    Friend WithEvents Label297 As System.Windows.Forms.Label
    Friend WithEvents Label298 As System.Windows.Forms.Label
    Friend WithEvents P15 As System.Windows.Forms.PictureBox
    Friend WithEvents Label299 As System.Windows.Forms.Label
    Friend WithEvents Label300 As System.Windows.Forms.Label
    Friend WithEvents Label301 As System.Windows.Forms.Label
    Friend WithEvents Label302 As System.Windows.Forms.Label
    Friend WithEvents BindingSource14 As System.Windows.Forms.BindingSource
    Friend WithEvents BindingSource15 As System.Windows.Forms.BindingSource
End Class

