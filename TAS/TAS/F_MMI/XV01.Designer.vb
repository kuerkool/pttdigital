﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XV01
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtReset = New System.Windows.Forms.Button()
        Me.BtClose = New System.Windows.Forms.Button()
        Me.BtAuto = New System.Windows.Forms.Button()
        Me.BtOpen = New System.Windows.Forms.Button()
        Me.LStatus = New System.Windows.Forms.Label()
        Me.LMode = New System.Windows.Forms.Label()
        Me.XVPB = New System.Windows.Forms.PictureBox()
        Me.XVPG = New System.Windows.Forms.PictureBox()
        Me.XVPR = New System.Windows.Forms.PictureBox()
        Me.XVPL = New System.Windows.Forms.PictureBox()
        CType(Me.XVPB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XVPL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtReset
        '
        Me.BtReset.ForeColor = System.Drawing.Color.Black
        Me.BtReset.Location = New System.Drawing.Point(109, 148)
        Me.BtReset.Name = "BtReset"
        Me.BtReset.Size = New System.Drawing.Size(92, 27)
        Me.BtReset.TabIndex = 13
        Me.BtReset.Text = "Reset Fail"
        Me.BtReset.UseVisualStyleBackColor = True
        '
        'BtClose
        '
        Me.BtClose.ForeColor = System.Drawing.Color.Red
        Me.BtClose.Location = New System.Drawing.Point(11, 148)
        Me.BtClose.Name = "BtClose"
        Me.BtClose.Size = New System.Drawing.Size(92, 27)
        Me.BtClose.TabIndex = 12
        Me.BtClose.Text = "Close"
        Me.BtClose.UseVisualStyleBackColor = True
        '
        'BtAuto
        '
        Me.BtAuto.ForeColor = System.Drawing.Color.Black
        Me.BtAuto.Location = New System.Drawing.Point(109, 115)
        Me.BtAuto.Name = "BtAuto"
        Me.BtAuto.Size = New System.Drawing.Size(92, 27)
        Me.BtAuto.TabIndex = 15
        Me.BtAuto.Text = "Auto/Manual"
        Me.BtAuto.UseVisualStyleBackColor = True
        '
        'BtOpen
        '
        Me.BtOpen.ForeColor = System.Drawing.Color.Green
        Me.BtOpen.Location = New System.Drawing.Point(11, 115)
        Me.BtOpen.Name = "BtOpen"
        Me.BtOpen.Size = New System.Drawing.Size(92, 27)
        Me.BtOpen.TabIndex = 14
        Me.BtOpen.Text = "Open"
        Me.BtOpen.UseVisualStyleBackColor = True
        '
        'LStatus
        '
        Me.LStatus.AutoSize = True
        Me.LStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LStatus.ForeColor = System.Drawing.Color.Lime
        Me.LStatus.Location = New System.Drawing.Point(77, 90)
        Me.LStatus.Name = "LStatus"
        Me.LStatus.Size = New System.Drawing.Size(72, 16)
        Me.LStatus.TabIndex = 11
        Me.LStatus.Text = "Fully Open"
        '
        'LMode
        '
        Me.LMode.AutoSize = True
        Me.LMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LMode.ForeColor = System.Drawing.Color.Aqua
        Me.LMode.Location = New System.Drawing.Point(77, 11)
        Me.LMode.Name = "LMode"
        Me.LMode.Size = New System.Drawing.Size(73, 16)
        Me.LMode.TabIndex = 10
        Me.LMode.Text = "Auto Mode"
        '
        'XVPB
        '
        Me.XVPB.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Bul1
        Me.XVPB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPB.Location = New System.Drawing.Point(89, 35)
        Me.XVPB.Name = "XVPB"
        Me.XVPB.Size = New System.Drawing.Size(50, 49)
        Me.XVPB.TabIndex = 18
        Me.XVPB.TabStop = False
        '
        'XVPG
        '
        Me.XVPG.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.gre
        Me.XVPG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPG.Location = New System.Drawing.Point(89, 35)
        Me.XVPG.Name = "XVPG"
        Me.XVPG.Size = New System.Drawing.Size(50, 49)
        Me.XVPG.TabIndex = 17
        Me.XVPG.TabStop = False
        '
        'XVPR
        '
        Me.XVPR.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.red
        Me.XVPR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPR.Location = New System.Drawing.Point(89, 35)
        Me.XVPR.Name = "XVPR"
        Me.XVPR.Size = New System.Drawing.Size(50, 49)
        Me.XVPR.TabIndex = 16
        Me.XVPR.TabStop = False
        '
        'XVPL
        '
        Me.XVPL.BackgroundImage = Global.TAS_TOL.My.Resources.Resources.Yen
        Me.XVPL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.XVPL.Location = New System.Drawing.Point(89, 35)
        Me.XVPL.Name = "XVPL"
        Me.XVPL.Size = New System.Drawing.Size(50, 49)
        Me.XVPL.TabIndex = 7
        Me.XVPL.TabStop = False
        '
        'XV01
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(214, 186)
        Me.Controls.Add(Me.XVPB)
        Me.Controls.Add(Me.XVPG)
        Me.Controls.Add(Me.XVPR)
        Me.Controls.Add(Me.BtReset)
        Me.Controls.Add(Me.BtClose)
        Me.Controls.Add(Me.BtAuto)
        Me.Controls.Add(Me.BtOpen)
        Me.Controls.Add(Me.LStatus)
        Me.Controls.Add(Me.LMode)
        Me.Controls.Add(Me.XVPL)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "XV01"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " XV-M2014-3 Control "
        CType(Me.XVPB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XVPL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BtReset As System.Windows.Forms.Button
    Friend WithEvents BtClose As System.Windows.Forms.Button
    Friend WithEvents BtAuto As System.Windows.Forms.Button
    Friend WithEvents BtOpen As System.Windows.Forms.Button
    Friend WithEvents LStatus As System.Windows.Forms.Label
    Friend WithEvents LMode As System.Windows.Forms.Label
    Friend WithEvents XVPL As System.Windows.Forms.PictureBox
    Friend WithEvents XVPR As System.Windows.Forms.PictureBox
    Friend WithEvents XVPG As System.Windows.Forms.PictureBox
    Friend WithEvents XVPB As System.Windows.Forms.PictureBox
End Class
