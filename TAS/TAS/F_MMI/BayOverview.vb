﻿Imports Gauge
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports System.Threading

Public Class BayOverview
    ' Dim MyDataSet As DataSet
    Dim MyDataAdapter As SqlDataAdapter
    '  Dim MyQueryString As String
    Dim MyConnection As New SqlConnection(My.Settings.FPTConnectionString)
    Dim StrBay As String
    Friend WithEvents ThrRefresh As BackgroundWorker
    Dim CallDataBindToDataGrid As New MethodInvoker(AddressOf Me.DataBindToDataGrid)

    Private Sub FRefresh()
        ThrRefresh = New BackgroundWorker
        ThrRefresh.WorkerSupportsCancellation = True
        AddHandler ThrRefresh.DoWork, AddressOf RefreshGrig
        ThrRefresh.RunWorkerAsync()
    End Sub

    Private Sub RefreshGrig()
        While True
            If ThrRefresh.CancellationPending Then
                ' StartConnect()
                '   ConnectTCP.Enabled = True
                '   ConnectTCP_Tick(sender, e)
                Exit While
            End If
            Try
                ' Me.V_BATCH_STATUSTableAdapter.Fill(Me.FPTDataSet.V_BATCH_STATUS)
                '   LockWindow(Me.Handle, True)
                TableLayoutPanel1.SuspendLayout()
                Panel4.SuspendLayout()
                Panel11.SuspendLayout()
                Panel17.SuspendLayout()
                Panel20.SuspendLayout()
                Panel23.SuspendLayout()
                Me.BeginInvoke(CallDataBindToDataGrid)

                Thread.Sleep(1000)
            Catch ex As Exception
                MyConnection.Close()
                '     MessageBox.Show(ex.Message)
            End Try
        End While

    End Sub

    Declare Function LockWindowUpdate Lib "user32" _
  (ByVal hWnd As Long) As Long

    Public Sub LockWindow(ByVal hWnd As Long, ByVal blnValue As Boolean)
        If blnValue Then
            LockWindowUpdate(hWnd)
        Else
            LockWindowUpdate(0)
        End If
    End Sub
    Public Sub DataBindToDataGrid()
        Dim Gauge As TGauge
        Try
            ' Set Meter Grid'
            'Me.BayOverview.SuspendLayout()
            LockWindow(Me.Handle, True)
            TableLayoutPanel1.SuspendLayout()
            Panel4.SuspendLayout()
            Panel11.SuspendLayout()
            Panel17.SuspendLayout()
            Panel20.SuspendLayout()
            Panel23.SuspendLayout()
            Me.V_BATCH_STATUSTableAdapter.Fill(Me.FPTDataSet.V_BATCH_STATUS)
            ''MeterDetail.SuspendLayout()
            ''FPTDataSetBindingSource.DataSource = MyDataSet
            ''FPTDataSetBindingSource.DataMember = "V_BATCH_STATUS"
            ''SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))


            Dim MaxCompartment As Int16 = 1
            Dim r As Int16 = 0
            For j = 1 To 6

                Select Case j

                    Case 1

                        'Connected/Idle'


                        Dim METER_STSTUS_BAY1() As String = Split(VBATCHSTATUSBindingSource.Item(VBATCHSTATUSBindingSource.Position)("METER_STSTUS_NAME").ToString(), "/")

                        If UCase(METER_STSTUS_BAY1(0)) = UCase("Connected") Then
                            LComm1.Text = "Good Communications"
                            LComm1.ForeColor = Color.Lime
                        Else
                            LComm1.Text = "Bad Communications"
                            LComm1.ForeColor = Color.Red
                        End If

                        If UCase(METER_STSTUS_BAY1(1)) = UCase("Loading") Then
                            LLoadStatus1.Text = "Loading"
                            LLoadStatus1.Visible = Not LLoadStatus1.Visible
                            PComparment_bay1.Visible = True
                            pVehicle1.Visible = True
                        Else
                            LLoadStatus1.Text = "Idle"
                            LLoadStatus1.Visible = Not LLoadStatus1.Visible
                            PComparment_bay1.Visible = False
                            pVehicle1.Visible = False
                        End If

                        For I As Int16 = 1 To 10
                            Dim BNane() As Control = Me.Controls.Find("Bay" & j.ToString & "_Comp" & I.ToString, True)
                            If BNane.Length = 1 Then
                                Gauge = Nothing
                                Gauge = DirectCast(BNane(0), TGauge)
                                '      Gauge.SuspendLayout()
                                If I <= MaxCompartment Then
                                    Gauge.Dock = DockStyle.Top
                                    Gauge.Height = Int(PComparment_bay1.Height / MaxCompartment)

                                    'If Truck.Rows(r).Item("CVH").ToString = I Then
                                    Gauge.MaxValue = VBATCHSTATUSBindingSource.Item(VBATCHSTATUSBindingSource.Position)("BASE_P").ToString()
                                    Gauge.Progress = VBATCHSTATUSBindingSource.Item(VBATCHSTATUSBindingSource.Position)("BASE_G").ToString()
                                    '  MessageBox.Show(VBATCHSTATUSBindingSource.Item(VBATCHSTATUSBindingSource.Position)("PRODUCT_COLORM").ToString())
                                    Gauge.Color = Color.FromName(VBATCHSTATUSBindingSource.Item(VBATCHSTATUSBindingSource.Position)("PRODUCT_COLORM3").ToString())
                                    'r += 1
                                    'Else
                                    '  Gauge.Progress = 0
                                    '   If Truck.Rows(r).Item("CVH").ToString <= I Then
                                    '      r += 1
                                    '  End If
                                    'End If
                                    Gauge.Visible = True
                                Else : Gauge.Visible = False
                                End If
                            End If
                            '    Gauge.ResumeLayout()
                        Next

                    Case 2
                        Dim METER_STSTUS_BAY2() As String = Split(VBATCHSTATUSBindingSource1.Item(VBATCHSTATUSBindingSource1.Position)("METER_STSTUS_NAME").ToString(), "/")

                        If UCase(METER_STSTUS_BAY2(0)) = UCase("Connected") Then
                            LComm2.Text = "Good Communications"
                            LComm2.ForeColor = Color.Lime
                        Else
                            LComm2.Text = "Bad Communications"
                            LComm2.ForeColor = Color.Red
                        End If

                        If UCase(METER_STSTUS_BAY2(1)) = UCase("Loading") Then
                            LLoadStatus2.Text = "Loading"
                            LLoadStatus2.Visible = Not LLoadStatus2.Visible
                            PComparment_bay2.Visible = True
                            pVehicle2.Visible = True
                        Else
                            LLoadStatus2.Text = "Idle"
                            LLoadStatus2.Visible = Not LLoadStatus2.Visible
                            PComparment_bay2.Visible = False
                            pVehicle2.Visible = False
                        End If
                        For I As Int16 = 1 To 10
                            Dim BNane() As Control = Me.Controls.Find("Bay" & j.ToString & "_Comp" & I.ToString, True)
                            If BNane.Length = 1 Then
                                Gauge = Nothing
                                Gauge = DirectCast(BNane(0), TGauge)
                                '      Gauge.SuspendLayout()
                                If I <= MaxCompartment Then
                                    Gauge.Dock = DockStyle.Top
                                    Gauge.Height = Int(PComparment_bay2.Height / MaxCompartment)

                                    'If Truck.Rows(r).Item("CVH").ToString = I Then
                                    Gauge.MaxValue = VBATCHSTATUSBindingSource1.Item(VBATCHSTATUSBindingSource1.Position)("BASE_P").ToString()
                                    Gauge.Progress = VBATCHSTATUSBindingSource1.Item(VBATCHSTATUSBindingSource1.Position)("BASE_G").ToString()
                                    Gauge.Color = Color.FromName(VBATCHSTATUSBindingSource1.Item(VBATCHSTATUSBindingSource1.Position)("PRODUCT_COLORM3").ToString())
                                    'r += 1
                                    'Else
                                    '  Gauge.Progress = 0
                                    '   If Truck.Rows(r).Item("CVH").ToString <= I Then
                                    '      r += 1
                                    '  End If
                                    'End If
                                    Gauge.Visible = True
                                Else : Gauge.Visible = False
                                End If
                            End If
                            '    Gauge.ResumeLayout()
                        Next

                    Case 3

                        Dim METER_STSTUS_BAY3() As String = Split(VBATCHSTATUSBindingSource2.Item(VBATCHSTATUSBindingSource2.Position)("METER_STSTUS_NAME").ToString(), "/")

                        If UCase(METER_STSTUS_BAY3(0)) = UCase("Connected") Then
                            LComm3.Text = "Good Communications"
                            LComm3.ForeColor = Color.Lime
                        Else
                            LComm3.Text = "Bad Communications"
                            LComm3.ForeColor = Color.Red
                        End If

                        If UCase(METER_STSTUS_BAY3(1)) = UCase("Loading") Then
                            LLoadStatus3.Text = "Loading"
                            LLoadStatus3.Visible = Not LLoadStatus3.Visible
                            PComparment_bay3.Visible = True
                            pVehicle3.Visible = True
                        Else
                            LLoadStatus3.Text = "Idle"
                            LLoadStatus3.Visible = Not LLoadStatus3.Visible
                            PComparment_bay3.Visible = False
                            pVehicle3.Visible = False
                        End If

                        For I As Int16 = 1 To 10
                            Dim BNane() As Control = Me.Controls.Find("Bay" & j.ToString & "_Comp" & I.ToString, True)
                            If BNane.Length = 1 Then
                                Gauge = Nothing
                                Gauge = DirectCast(BNane(0), TGauge)
                                '      Gauge.SuspendLayout()
                                If I <= MaxCompartment Then
                                    Gauge.Dock = DockStyle.Top
                                    Gauge.Height = Int(PComparment_bay3.Height / MaxCompartment)

                                    'If Truck.Rows(r).Item("CVH").ToString = I Then
                                    Gauge.MaxValue = VBATCHSTATUSBindingSource2.Item(VBATCHSTATUSBindingSource2.Position)("BASE_P").ToString()
                                    Gauge.Progress = VBATCHSTATUSBindingSource2.Item(VBATCHSTATUSBindingSource2.Position)("BASE_G").ToString()
                                    Gauge.Color = Color.FromName(VBATCHSTATUSBindingSource2.Item(VBATCHSTATUSBindingSource2.Position)("PRODUCT_COLORM3").ToString())
                                    'r += 1
                                    'Else
                                    '  Gauge.Progress = 0
                                    '   If Truck.Rows(r).Item("CVH").ToString <= I Then
                                    '      r += 1
                                    '  End If
                                    'End If
                                    Gauge.Visible = True
                                Else : Gauge.Visible = False
                                End If
                            End If
                            '    Gauge.ResumeLayout()
                        Next

                    Case 4
                        Dim METER_STSTUS_BAY4() As String = Split(VBATCHSTATUSBindingSource3.Item(VBATCHSTATUSBindingSource3.Position)("METER_STSTUS_NAME").ToString(), "/")

                        If UCase(METER_STSTUS_BAY4(0)) = UCase("Connected") Then
                            LComm4.Text = "Good Communications"
                            LComm4.ForeColor = Color.Lime
                        Else
                            LComm4.Text = "Bad Communications"
                            LComm4.ForeColor = Color.Red
                        End If

                        If UCase(METER_STSTUS_BAY4(1)) = UCase("Loading") Then
                            LLoadStatus4.Text = "Loading"
                            LLoadStatus4.Visible = Not LLoadStatus4.Visible
                            PComparment_bay4.Visible = True
                            pVehicle4.Visible = True
                        Else
                            LLoadStatus4.Text = "Idle"
                            LLoadStatus4.Visible = Not LLoadStatus4.Visible
                            PComparment_bay4.Visible = False
                            pVehicle4.Visible = False
                        End If
                        For I As Int16 = 1 To 10
                            Dim BNane() As Control = Me.Controls.Find("Bay" & j.ToString & "_Comp" & I.ToString, True)
                            If BNane.Length = 1 Then
                                Gauge = Nothing
                                Gauge = DirectCast(BNane(0), TGauge)
                                '      Gauge.SuspendLayout()
                                If I <= MaxCompartment Then
                                    Gauge.Dock = DockStyle.Top
                                    Gauge.Height = Int(PComparment_bay4.Height / MaxCompartment)

                                    'If Truck.Rows(r).Item("CVH").ToString = I Then
                                    Gauge.MaxValue = VBATCHSTATUSBindingSource3.Item(VBATCHSTATUSBindingSource3.Position)("BASE_P").ToString()
                                    Gauge.Progress = VBATCHSTATUSBindingSource3.Item(VBATCHSTATUSBindingSource3.Position)("BASE_G").ToString()
                                    Gauge.Color = Color.FromName(VBATCHSTATUSBindingSource3.Item(VBATCHSTATUSBindingSource3.Position)("PRODUCT_COLORM3").ToString())
                                    'r += 1
                                    'Else
                                    '  Gauge.Progress = 0
                                    '   If Truck.Rows(r).Item("CVH").ToString <= I Then
                                    '      r += 1
                                    '  End If
                                    'End If
                                    Gauge.Visible = True
                                Else : Gauge.Visible = False
                                End If
                            End If
                            '    Gauge.ResumeLayout()
                        Next


                    Case 5

                        Dim METER_STSTUS_BAY5() As String = Split(VBATCHSTATUSBindingSource4.Item(VBATCHSTATUSBindingSource4.Position)("METER_STSTUS_NAME").ToString(), "/")

                        If UCase(METER_STSTUS_BAY5(0)) = UCase("Connected") Then
                            LComm5.Text = "Good Communications"
                            LComm5.ForeColor = Color.Lime
                        Else
                            LComm5.Text = "Bad Communications"
                            LComm5.ForeColor = Color.Red
                        End If

                        If UCase(METER_STSTUS_BAY5(1)) = UCase("Loading") Then
                            LLoadStatus5.Text = "Loading"
                            LLoadStatus5.Visible = Not LLoadStatus5.Visible
                            PComparment_bay5.Visible = True
                            pVehicle5.Visible = True
                        Else
                            LLoadStatus5.Text = "Idle"
                            LLoadStatus5.Visible = Not LLoadStatus5.Visible
                            PComparment_bay5.Visible = False
                            pVehicle5.Visible = False
                        End If
                        For I As Int16 = 1 To 10
                            Dim BNane() As Control = Me.Controls.Find("Bay" & j.ToString & "_Comp" & I.ToString, True)
                            If BNane.Length = 1 Then
                                Gauge = Nothing
                                Gauge = DirectCast(BNane(0), TGauge)
                                '      Gauge.SuspendLayout()
                                If I <= MaxCompartment Then
                                    Gauge.Dock = DockStyle.Top
                                    Gauge.Height = Int(PComparment_bay5.Height / MaxCompartment)

                                    'If Truck.Rows(r).Item("CVH").ToString = I Then
                                    Gauge.MaxValue = VBATCHSTATUSBindingSource4.Item(VBATCHSTATUSBindingSource4.Position)("BASE_P").ToString()
                                    Gauge.Progress = VBATCHSTATUSBindingSource4.Item(VBATCHSTATUSBindingSource4.Position)("BASE_G").ToString()
                                    Gauge.Color = Color.FromName(VBATCHSTATUSBindingSource4.Item(VBATCHSTATUSBindingSource4.Position)("PRODUCT_COLORM3").ToString())
                                    'r += 1
                                    'Else
                                    '  Gauge.Progress = 0
                                    '   If Truck.Rows(r).Item("CVH").ToString <= I Then
                                    '      r += 1
                                    '  End If
                                    'End If
                                    Gauge.Visible = True
                                Else : Gauge.Visible = False
                                End If
                            End If
                            '    Gauge.ResumeLayout()
                        Next


                    Case 6

                        Dim METER_STSTUS_BAY6() As String = Split(VBATCHSTATUSBindingSource5.Item(VBATCHSTATUSBindingSource5.Position)("METER_STSTUS_NAME").ToString(), "/")

                        If UCase(METER_STSTUS_BAY6(0)) = UCase("Connected") Then
                            LComm6.Text = "Good Communications"
                            LComm6.ForeColor = Color.Lime
                        Else
                            LComm6.Text = "Bad Communications"
                            LComm6.ForeColor = Color.Red
                        End If

                        If UCase(METER_STSTUS_BAY6(1)) = UCase("Loading") Then
                            LLoadStatus6.Text = "Loading"
                            LLoadStatus6.Visible = Not LLoadStatus6.Visible
                            PComparment_bay6.Visible = True
                            pVehicle6.Visible = True
                        Else
                            LLoadStatus6.Text = "Idle"
                            LLoadStatus6.Visible = Not LLoadStatus6.Visible
                            PComparment_bay6.Visible = False
                            pVehicle6.Visible = False
                        End If
                        For I As Int16 = 1 To 10
                            Dim BNane() As Control = Me.Controls.Find("Bay" & j.ToString & "_Comp" & I.ToString, True)
                            If BNane.Length = 1 Then
                                Gauge = Nothing
                                Gauge = DirectCast(BNane(0), TGauge)
                                '      Gauge.SuspendLayout()
                                If I <= MaxCompartment Then
                                    Gauge.Dock = DockStyle.Top
                                    Gauge.Height = Int(PComparment_bay6.Height / MaxCompartment)

                                    'If Truck.Rows(r).Item("CVH").ToString = I Then
                                    Gauge.MaxValue = VBATCHSTATUSBindingSource5.Item(VBATCHSTATUSBindingSource5.Position)("BASE_P").ToString()
                                    Gauge.Progress = VBATCHSTATUSBindingSource5.Item(VBATCHSTATUSBindingSource5.Position)("BASE_G").ToString()
                                    Gauge.Color = Color.FromName(VBATCHSTATUSBindingSource5.Item(VBATCHSTATUSBindingSource5.Position)("PRODUCT_COLORM3").ToString())
                                    'r += 1
                                    'Else
                                    '  Gauge.Progress = 0
                                    '   If Truck.Rows(r).Item("CVH").ToString <= I Then
                                    '      r += 1
                                    '  End If
                                    'End If
                                    Gauge.Visible = True
                                Else : Gauge.Visible = False
                                End If
                            End If
                            '    Gauge.ResumeLayout()
                        Next
                End Select
            Next




            Panel4.ResumeLayout()
            Panel11.ResumeLayout()
            Panel17.ResumeLayout()
            Panel20.ResumeLayout()
            Panel23.ResumeLayout()
            TableLayoutPanel1.ResumeLayout()
            LockWindow(Me.Handle, False)
            ' '' SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))
            ''MeterDetail.ResumeLayout()


            ''MeterDetail.SuspendLayout()
            ''MeterDetail.DataSource = MyDataSet
            ''MeterDetail.DataMember = "V_BATCH_STATUS"
            ''SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))
            ''MeterDetail.ResumeLayout()
            '' End Set Meter Grid


            ''LoadDetail.SuspendLayout()
            ''VLOADINGLOCALMODEBindingSource.DataSource = MyDataSet
            ''VLOADINGLOCALMODEBindingSource.DataMember = "LOADING"
            ''SetLoadDetail(MyDataSet.Tables("LOADING"), MyDataSet.Tables("TRUCK_LOADING"))

            ' '' SetbatchEn(MyDataSet.Tables("V_BATCH_STATUS"))
            ''LoadDetail.ResumeLayout()


            ''Set Loading Grid
            'LoadDetail.SuspendLayout()
            'LoadDetail.DataSource = MyDataSet
            'LoadDetail.DataMember = "LOADING"
            'SetLoadDetail(MyDataSet.Tables("LOADING"), MyDataSet.Tables("TRUCK_LOADING"))
            'LoadDetail.ResumeLayout()
            '  MyDataAdapter = Nothing
            '  MyDataSet = Nothing
        Catch ex As Exception

        End Try

    End Sub


    Private Sub PComparment_bay_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PComparment_bay6.Resize, PComparment_bay5.Resize, PComparment_bay4.Resize, PComparment_bay3.Resize, PComparment_bay2.Resize, PComparment_bay1.Resize

        If sender.name = "PComparment_bay1" Then '''Bay 1'''''''''''''''''''''''''''''''''
            Dim MaxCompartment As Integer = 0
            For i = 1 To 10
                Dim Bay1_Comp() As Control = Me.Controls.Find("Bay1_Comp" & i, True)
                If Bay1_Comp.Length = 1 Then
                    '''''''''''''Max Compant///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay1_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        MaxCompartment += 1
                    End If
                End If
            Next


            For i = 1 To 10
                Dim Bay1_Comp() As Control = Me.Controls.Find("Bay1_Comp" & i, True)
                If Bay1_Comp.Length = 1 Then
                    '''''''''''''Size Compartment///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay1_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        Comp.Height = Int(PComparment_bay1.Height / MaxCompartment)
                    End If
                End If
            Next

        ElseIf sender.name = "PComparment_bay2" Then '''Bay 2'''''''''''''''''''''''''''''''''
            Dim MaxCompartment As Integer = 0
            For i = 1 To 10
                Dim Bay2_Comp() As Control = Me.Controls.Find("Bay2_Comp" & i, True)
                If Bay2_Comp.Length = 1 Then
                    '''''''''''''Max Compant///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay2_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        MaxCompartment += 1
                    End If
                End If
            Next


            For i = 1 To 10
                Dim Bay2_Comp() As Control = Me.Controls.Find("Bay2_Comp" & i, True)
                If Bay2_Comp.Length = 1 Then
                    '''''''''''''Size Compartment///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay2_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        Comp.Height = Int(PComparment_bay2.Height / MaxCompartment)
                    End If
                End If
            Next
        ElseIf sender.name = "PComparment_bay3" Then '''Bay 3'''''''''''''''''''''''''''''''''
            Dim MaxCompartment As Integer = 0
            For i = 1 To 10
                Dim Bay3_Comp() As Control = Me.Controls.Find("Bay3_Comp" & i, True)
                If Bay3_Comp.Length = 1 Then
                    '''''''''''''Max Compant///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay3_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        MaxCompartment += 1
                    End If
                End If
            Next


            For i = 1 To 10
                Dim Bay3_Comp() As Control = Me.Controls.Find("Bay3_Comp" & i, True)
                If Bay3_Comp.Length = 1 Then
                    '''''''''''''Size Compartment///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay3_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        Comp.Height = Int(PComparment_bay3.Height / MaxCompartment)
                    End If
                End If
            Next

        ElseIf sender.name = "PComparment_bay4" Then '''Bay 4'''''''''''''''''''''''''''''''''
            Dim MaxCompartment As Integer = 0
            For i = 1 To 10
                Dim Bay4_Comp() As Control = Me.Controls.Find("Bay4_Comp" & i, True)
                If Bay4_Comp.Length = 1 Then
                    '''''''''''''Max Compant///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay4_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        MaxCompartment += 1
                    End If
                End If
            Next


            For i = 1 To 10
                Dim Bay4_Comp() As Control = Me.Controls.Find("Bay4_Comp" & i, True)
                If Bay4_Comp.Length = 1 Then
                    '''''''''''''Size Compartment///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay4_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        Comp.Height = Int(PComparment_bay4.Height / MaxCompartment)
                    End If
                End If
            Next

        ElseIf sender.name = "PComparment_bay5" Then '''Bay 4'''''''''''''''''''''''''''''''''
            Dim MaxCompartment As Integer = 0
            For i = 1 To 10
                Dim Bay5_Comp() As Control = Me.Controls.Find("Bay5_Comp" & i, True)
                If Bay5_Comp.Length = 1 Then
                    '''''''''''''Max Compant///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay5_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        MaxCompartment += 1
                    End If
                End If
            Next


            For i = 1 To 10
                Dim Bay5_Comp() As Control = Me.Controls.Find("Bay5_Comp" & i, True)
                If Bay5_Comp.Length = 1 Then
                    '''''''''''''Size Compartment///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay5_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        Comp.Height = Int(PComparment_bay5.Height / MaxCompartment)
                    End If
                End If
            Next

        ElseIf sender.name = "PComparment_bay6" Then '''Bay 6'''''''''''''''''''''''''''''''''
            Dim MaxCompartment As Integer = 0
            For i = 1 To 10
                Dim Bay6_Comp() As Control = Me.Controls.Find("Bay6_Comp" & i, True)
                If Bay6_Comp.Length = 1 Then
                    '''''''''''''Max Compant///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay6_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        MaxCompartment += 1
                    End If
                End If
            Next


            For i = 1 To 10
                Dim Bay6_Comp() As Control = Me.Controls.Find("Bay6_Comp" & i, True)
                If Bay6_Comp.Length = 1 Then
                    '''''''''''''Size Compartment///////////////////////////////////////
                    Dim Comp As TGauge = DirectCast(Bay6_Comp(0), TGauge)
                    If Comp.Visible = True Then
                        Comp.Height = Int(PComparment_bay6.Height / MaxCompartment)
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub BayOverview_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FPTDataSet.V_BATCH_STATUS' table. You can move, or remove it, as needed.
        Me.V_BATCH_STATUSTableAdapter.Fill(Me.FPTDataSet.V_BATCH_STATUS)
        Me.PictureBox8.Image = New System.Drawing.Bitmap("Company.png")
        '  Me.PictureBox2.BackgroundImage = New System.Drawing.Bitmap("PTT.png")
        ' Gen_BayOverView()
        FRefresh()
    End Sub

    Private Sub GEN_Compartment()
        Dim batch_bay As String
        Dim MyDataTableDetail As New DataTable
        Dim MyDataTableComp As New DataTable
        batch_bay = "1,2,3"

        MyDataTableDetail = FirstDataLoadDetail("1,2,3", 0)
        MyDataTableComp = FirstDataLoadComp("1,2,3", 0)

    End Sub

    Function FirstDataLoadDetail(ByVal BATCH_BAY As String, ByRef Mode As Int16) As DataTable
        Dim MyDataSet As DataSet
        Dim MyQueryString As String
        MyDataSet = New DataSet()
        MyConnection.Open()
        If Mode = 0 Then
            MyQueryString = "select * from V_LOADING_LOCALMODE"
            MyQueryString = MyQueryString + " where LOAD_DATE=(select LOAD_DATE from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + BATCH_BAY + ")))"
            MyQueryString = MyQueryString + " and CSF=(select CSF from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + BATCH_BAY + "))) "
            MyQueryString = MyQueryString + " order by CVH,DAY_LOAD_ID "
            Dim cmd As New SqlCommand(MyQueryString, MyConnection)
            MyDataAdapter = New SqlDataAdapter(cmd)
            MyDataAdapter.Fill(MyDataSet, "V_LOADING_LOCALMODE")
        ElseIf Mode = 1 Then
            MyQueryString = " select * from V_LOADING_REMOTEMODE "
            MyQueryString = MyQueryString + " where Reference =(select top 1 (cast(Reference as float))as Reference from V_LOADING_REMOTEMODE where (ID IS NOT NULL) and  BATCH_NUMBER in (" + BATCH_BAY + ") order by ldate desc ) "
            MyQueryString = MyQueryString + " order by CVH,DAY_LOAD_ID "
            Dim cmd As New SqlCommand(MyQueryString, MyConnection)
            MyDataAdapter = New SqlDataAdapter(cmd)
            MyDataAdapter.Fill(MyDataSet, "V_LOADING_REMOTEMODE")
        End If
        Return MyDataSet.Tables(0)
    End Function

    Function FirstDataLoadComp(ByVal BATCH_BAY As String, ByRef Mode As Int16) As DataTable
        Dim MyDataSet As DataSet
        Dim MyQueryString As String
        MyDataSet = New DataSet()
        MyConnection.Open()
        If Mode = 0 Then
            MyQueryString = " select CVH,MAX(CVA) as CVA,MAX(CVI) as CVI, SUM(CVB) as CVB,SUM(CVJ) as CVJ ,MAX(Product_color) as Product_color,MAX(LOAD_STATUS) as LOAD_STATUS from V_LOADING_LOCALMODE"
            MyQueryString = MyQueryString + " where LOAD_DATE=(select LOAD_DATE from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + BATCH_BAY + ")))"
            MyQueryString = MyQueryString + " and CSF=(select CSF from V_LOADING_LOCALMODE where LDATE=(select MAX(LDATE) from V_LOADING_LOCALMODE where BATCH_NUMBER in (" + BATCH_BAY + "))) "
            MyQueryString = MyQueryString + " Group by CVH"
            MyQueryString = MyQueryString + " order by CVH"
            Dim cmd As New SqlCommand(MyQueryString, MyConnection)
            MyDataAdapter = New SqlDataAdapter(cmd)
            MyDataAdapter.Fill(MyDataSet, "V_LOADING_LOCALMODE")
        ElseIf Mode = 1 Then
            MyQueryString = "select CVH,MAX(CVA) as CVA,MAX(CVI) as CVI, SUM(CVB) as CVB,SUM(CVJ) as CVJ,MAX(Product_color) as Product_color,MAX(LOAD_STATUS) as LOAD_STATUS from V_LOADING_REMOTEMODE"
            MyQueryString = MyQueryString + " where  Reference =(select top 1 (cast(Reference as float))as Reference from V_LOADING_REMOTEMODE where (ID IS NOT NULL) and BATCH_NUMBER in (" + BATCH_BAY + ") order by ldate desc )"
            MyQueryString = MyQueryString + " Group by CVH"
            MyQueryString = MyQueryString + " order by CVH"
            Dim cmd As New SqlCommand(MyQueryString, MyConnection)
            MyDataAdapter = New SqlDataAdapter(cmd)
            MyDataAdapter.Fill(MyDataSet, "V_LOADING_REMOTEMODE")
        End If
        Return MyDataSet.Tables(0)
    End Function

    Private Sub Gen_BayOverView()
        Dim MyDataSet As DataSet
        Dim MyQueryString As String
        MyDataSet = New DataSet()
        MyConnection.Open()
        '  Dim Bay_island() As String = Split(StrBay, "/")
        MyQueryString = " SELECT* FROM T_ISLAND ORDER BY ISLAND_NUMBER"
        Dim cmd As New SqlCommand(MyQueryString, MyConnection)
        MyDataAdapter = New SqlDataAdapter(cmd)
        MyDataAdapter.Fill(MyDataSet, "T_ISLAND")
        Dim Numbay As Integer = 0
        Dim NumLbay As Integer = 0
        For i = 0 To MyDataSet.Tables(0).Rows.Count - 1





            '''''''''''''''''ISLAND_BAYRIGHT_STATUS
            Numbay += 1
            Dim LbayStatus As String = UCase(MyDataSet.Tables(0)(i).Item("ISLAND_BAYLEFT_STATUS").ToString())
            Dim LPComparment_bay() As Control = Me.Controls.Find("PComparment_bay" & Numbay, True)
            Dim LVehicle() As Control = Me.Controls.Find("pVehicle" & Numbay, True)
            Dim Llbay() As Control = Me.Controls.Find("Lbay" & Numbay, True)
            If LbayStatus = "EN" Then
                If LPComparment_bay.Length = 1 Then
                    '''''''''''''Visible PComparment_bay   True///////////////////////////////////////
                    Dim Comp As Panel = DirectCast(LPComparment_bay(0), Panel)
                    Comp.Visible = True
                End If
                If LVehicle.Length = 1 Then
                    '''''''''''''Visible Vehicle   True///////////////////////////////////////
                    Dim Vehicle As PictureBox = DirectCast(LVehicle(0), PictureBox)
                    Vehicle.Visible = True
                End If
                If Llbay.Length = 1 Then
                    ''''''''''''' Label bay  ///////////////////////////////////////
                    Dim lbay As Label = DirectCast(Llbay(0), Label)
                    NumLbay += 1
                    lbay.Text = NumLbay
                End If
            Else
                If LPComparment_bay.Length = 1 Then
                    '''''''''''''Visible PComparment_bay   False///////////////////////////////////////
                    Dim Comp As Panel = DirectCast(LPComparment_bay(0), Panel)
                    Comp.Visible = False
                End If
                If LVehicle.Length = 1 Then
                    '''''''''''''Visible Vehicle   False///////////////////////////////////////
                    Dim Vehicle As PictureBox = DirectCast(LVehicle(0), PictureBox)
                    Vehicle.Visible = False
                End If
                If Llbay.Length = 1 Then
                    ''''''''''''' Label bay  ///////////////////////////////////////
                    Dim lbay As Label = DirectCast(Llbay(0), Label)
                    lbay.Text = "-"
                End If
            End If
            Numbay += 1
            Dim RbayStatus As String = UCase(MyDataSet.Tables(0)(i).Item("ISLAND_BAYRIGHT_STATUS").ToString())
            Dim RPComparment_bay() As Control = Me.Controls.Find("PComparment_bay" & Numbay, True)
            Dim RVehicle() As Control = Me.Controls.Find("pVehicle" & Numbay, True)
            Dim Rlbay() As Control = Me.Controls.Find("Lbay" & Numbay, True)
            If RbayStatus = "EN" Then
                If RPComparment_bay.Length = 1 Then
                    '''''''''''''Visible PComparment_bay   True///////////////////////////////////////
                    Dim Comp As Panel = DirectCast(RPComparment_bay(0), Panel)
                    Comp.Visible = True
                End If
                If RVehicle.Length = 1 Then
                    '''''''''''''Visible Vehicle   True///////////////////////////////////////
                    Dim Vehicle As PictureBox = DirectCast(RVehicle(0), PictureBox)
                    Vehicle.Visible = True
                End If
                If Rlbay.Length = 1 Then
                    ''''''''''''' Label bay  ///////////////////////////////////////
                    Dim Rbay As Label = DirectCast(Rlbay(0), Label)
                    NumLbay += 1
                    Rbay.Text = NumLbay
                End If
            Else
                If RPComparment_bay.Length = 1 Then
                    '''''''''''''Visible PComparment_bay   False///////////////////////////////////////
                    Dim Comp As Panel = DirectCast(RPComparment_bay(0), Panel)
                    Comp.Visible = False
                End If
                If RVehicle.Length = 1 Then
                    '''''''''''''Visible Vehicle   False///////////////////////////////////////
                    Dim Vehicle As PictureBox = DirectCast(RVehicle(0), PictureBox)
                    Vehicle.Visible = False
                End If
                If Rlbay.Length = 1 Then
                    ''''''''''''' Label bay  ///////////////////////////////////////
                    Dim Rbay As Label = DirectCast(Rlbay(0), Label)
                    Rbay.Text = "-"
                End If
            End If




            '''''''''''pIsland1_batch1'''''''


            Dim Batchsum_island As Integer = UCase(MyDataSet.Tables(0)(i).Item("ISLAND_METER_SUM"))
            Dim island_NO As Integer = UCase(MyDataSet.Tables(0)(i).Item("ISLAND_NUMBER"))

            For pbatch = 1 To 8
                Dim pIsland_batch() As Control = Me.Controls.Find("pIsland" & island_NO & "_batch" & pbatch, True)
                Dim LPanel As Panel = DirectCast(pIsland_batch(0), Panel)
                ' Rbay.Text = "-"
                If pbatch <= Batchsum_island Then
                    LPanel.Visible = True
                Else
                    LPanel.Visible = False
                End If
            Next




        Next


    End Sub


    Private Sub BayOverview_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ThrRefresh.CancelAsync()
    End Sub

  
   
End Class