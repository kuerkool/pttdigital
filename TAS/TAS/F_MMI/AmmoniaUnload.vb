﻿Imports System.ComponentModel

Public Class AmmoniaUnload


    Friend WithEvents ThrRefStstus As BackgroundWorker
    Private CallDataBindToStatus As New MethodInvoker(AddressOf Me.DataBindToStatus)
    Private BIT_UnStatus2, BIT_UnStatus3, BIT_TRUCKSTATUS As String
    Private V_UNLOAD_NEW As New DataSet

    Private Sub RunThrRefStstus()
        ThrRefStstus = New BackgroundWorker
        ThrRefStstus.WorkerSupportsCancellation = True
        AddHandler ThrRefStstus.DoWork, AddressOf AsyncThrRefStstus
        ThrRefStstus.RunWorkerAsync()
    End Sub

    Private Sub SetImageXV2(ByVal Index As Int16)
        Select Case Index
            Case 0
                XVPR2.Visible = True
                XVPG2.Visible = False
                XVPB2.Visible = False
                XVPL2.Visible = False
            Case 1
                XVPR2.Visible = False
                XVPG2.Visible = True
                XVPB2.Visible = False
                XVPL2.Visible = False
            Case 2
                XVPR2.Visible = False
                XVPG2.Visible = False
                XVPB2.Visible = True
                XVPL2.Visible = False
            Case 3
                XVPR2.Visible = False
                XVPG2.Visible = False
                XVPB2.Visible = False
                XVPL2.Visible = True
        End Select
    End Sub

    Private Sub SetImageXV3(ByVal Index As Int16)
        Select Case Index
            Case 0
                XVPR3.Visible = True
                XVPG3.Visible = False
                XVPB3.Visible = False
                XVPL3.Visible = False
            Case 1
                XVPR3.Visible = False
                XVPG3.Visible = True
                XVPB3.Visible = False
                XVPL3.Visible = False
            Case 2
                XVPR3.Visible = False
                XVPG3.Visible = False
                XVPB3.Visible = True
                XVPL3.Visible = False
            Case 3
                XVPR3.Visible = False
                XVPG3.Visible = False
                XVPB3.Visible = False
                XVPL3.Visible = True
        End Select
    End Sub

    Private Sub AsyncThrRefStstus(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        Dim da As New SqlClient.SqlDataAdapter
        Dim ds As New DataSet
        Dim SqlCon As New SqlClient.SqlConnection
        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        SqlCon.Open()
        ' Dim i, j As Integer
        Dim q As String
        Dim T_REALTIME, T_MBR As New DataTable
        Dim REG_UnStatus As Integer



        Do
            If ThrRefStstus.CancellationPending = True Then
                e.Cancel = True
                Exit Do
            Else
                Try
                    BIT_UnStatus2 = ""
                    BIT_UnStatus3 = ""
                    BIT_TRUCKSTATUS = ""
                    'VX-3
                    q = "select * from T_MBR where mbr_reg='406501'"
                    da = New SqlClient.SqlDataAdapter(q, SqlCon)
                    'ds.Reset()
                    ds.Dispose()
                    ds = New DataSet
                    da.Fill(ds, "T_MBR")
                    REG_UnStatus = ds.Tables("T_MBR").Rows(0).Item("MBR_VAL").ToString
                    BIT_UnStatus3 = Strings.Right("0000000000000000" + Convert.ToString(REG_UnStatus, 2), 16)


                    ' XV01-2
                    q = "select * from T_MBR where mbr_reg='406502'"
                    da = New SqlClient.SqlDataAdapter(q, SqlCon)
                    'ds.Reset()
                    ds.Dispose()
                    ds = New DataSet
                    da.Fill(ds, "T_MBR")
                    REG_UnStatus = ds.Tables("T_MBR").Rows(0).Item("MBR_VAL").ToString
                    BIT_UnStatus2 = Strings.Right("0000000000000000" + Convert.ToString(REG_UnStatus, 2), 16)
                    ' GNDSTATUS = BIT_UnStatus(12).ToString

                    ' TRUCK STATUS
                    q = "select * from T_MBR where mbr_reg='406503'"
                    da = New SqlClient.SqlDataAdapter(q, SqlCon)
                    'ds.Reset()
                    ds.Dispose()
                    ds = New DataSet
                    da.Fill(ds, "T_MBR")
                    REG_UnStatus = ds.Tables("T_MBR").Rows(0).Item("MBR_VAL").ToString
                    BIT_TRUCKSTATUS = Strings.Right("0000000000000000" + Convert.ToString(REG_UnStatus, 2), 16)
                    ' GNDSTATUS = BIT_UnStatus(12).ToString


                    ' TRUCK LOADING
                    q = "select  * from V_UNLOAD_NEW  ORDER BY LOAD_ID DESC"
                    da = New SqlClient.SqlDataAdapter(q, SqlCon)
                    'ds.Reset()
                    V_UNLOAD_NEW.Dispose()
                    V_UNLOAD_NEW = New DataSet
                    da.Fill(V_UNLOAD_NEW, "V_UNLOAD_NEW")
                    ' REG_UnStatus = ds.Tables("T_MBR").Rows(0).Item("MBR_VAL").ToString
                    ' BIT_TRUCKSTATUS = Strings.Right("0000000000000000" + Convert.ToString(REG_UnStatus, 2), 16)
                    ' GNDSTATUS = BIT_UnStatus(12).ToString


                    Me.BeginInvoke(CallDataBindToStatus)
                Catch ex As Exception

                End Try

            End If
            Threading.Thread.Sleep(1500)
        Loop While True
    End Sub


    Private Sub DataBindToStatus()
        '' XV STATUS'''''''''''''''''''''''''''''''''''''
        Try

            If BIT_UnStatus2(2) = "1" Then
                LMode2.Text = "Manual Mode"
                'BtOpen.Enabled = True
                ' BtClose.Enabled = True
                ' BtReset.Enabled = True
            Else

                LMode2.Text = "Auto Mode"
                ' BtOpen.Enabled = False
                ' BtClose.Enabled = False
                '      BtReset.Enabled =false
            End If

            If BIT_UnStatus3(2) = "1" Then
                LMode3.Text = "Manual Mode"
                'BtOpen.Enabled = True
                ' BtClose.Enabled = True
                'BtReset.Enabled = True
            Else

                LMode3.Text = "Auto Mode"
                ' BtOpen.Enabled = False
                ' BtClose.Enabled = False
                '      BtReset.Enabled =false
            End If


            If BIT_UnStatus2(14) = "1" Then

                LStatus2.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus2.ForeColor = Color.Blue
                LStatus2.Text = "Moving..."
                'LStatus.BehaviorOptions.Active :=False;
                ' BtOpen.Enabled = True
                'BtClose.Enabled = True
                SetImageXV2(2)
            End If

            If BIT_UnStatus3(14) = "1" Then

                LStatus3.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus3.ForeColor = Color.Blue
                LStatus3.Text = "Moving..."
                'LStatus.BehaviorOptions.Active :=False;
                ' BtOpen.Enabled = True
                'BtClose.Enabled = True
                SetImageXV3(2)
            End If




            If BIT_UnStatus2(4) = "1" Then

                LStatus2.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus2.ForeColor = Color.Red
                LStatus2.Text = "Fully Close"
                'LStatus.BehaviorOptions.Active :=False;
                'BtClose.Enabled = False
                SetImageXV2(0)
            End If


            If BIT_UnStatus3(4) = "1" Then

                LStatus3.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus3.ForeColor = Color.Red
                LStatus3.Text = "Fully Close"
                'LStatus.BehaviorOptions.Active :=False;
                'BtClose.Enabled = False
                SetImageXV3(0)
            End If


            If BIT_UnStatus2(5) = "1" Then

                LStatus2.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus2.ForeColor = Color.Green
                LStatus2.Text = "Fully Open"
                ' LStatus.BehaviorOptions.Active :=False;
                'BtOpen.Enabled = False
                SetImageXV2(1)
            End If


            If BIT_UnStatus3(5) = "1" Then

                LStatus3.Visible = True
                'LStatus.Behavior:='(None)';
                LStatus3.ForeColor = Color.Green
                LStatus3.Text = "Fully Open"
                ' LStatus.BehaviorOptions.Active :=False;
                'BtOpen.Enabled = False
                SetImageXV3(1)
            End If

            If BIT_UnStatus2(9) = "1" Then

                ' BtOpen.Enabled = False
                ' BtClose.Enabled = False
                LStatus2.Visible = True
                'LStatus.Behavior:='Blinking';
                LStatus2.ForeColor = Color.Yellow
                LStatus2.Text = "Fail..."
                'LStatus.BiDiMode:=bdLeftToRight;
                'LStatus.BehaviorOptions.Active :=True;
                SetImageXV2(3)
            End If

            If BIT_UnStatus3(9) = "1" Then

                ' BtOpen.Enabled = False
                ' BtClose.Enabled = False
                LStatus3.Visible = True
                'LStatus.Behavior:='Blinking';
                LStatus3.ForeColor = Color.Yellow
                LStatus3.Text = "Fail..."
                'LStatus.BiDiMode:=bdLeftToRight;
                'LStatus.BehaviorOptions.Active :=True;
                SetImageXV3(3)
            End If

        Catch ex As Exception

        End Try
        '' TRUCK STATUS'''''''''''''''''''''''''''''''''''''
        Try


            If BIT_TRUCKSTATUS(12).ToString = 1 Then
                EVEH.Visible = True
                LEDGND.BackColor = Color.Lime
            Else
                EVEH.Visible = False
                LEDGND.BackColor = Color.Red
            End If

            If BIT_TRUCKSTATUS(15).ToString = 1 Then
                'EVEH.Visible = True
                LEDESD.BackColor = Color.Lime
                LEDESD2.BackColor = Color.Lime
            Else
                'EVEH.Visible = False
                LEDESD.BackColor = Color.Red
                LEDESD2.BackColor = Color.Red
            End If



            'LEDESD.Checked  := StrtoBool(StrBin[16]);
            '//LEDSTART.Checked  := StrtoBool(StrBin[12]);
            '//LEDSTOP.Checked  := StrtoBool(StrBin[11]);
            'LEDESD2.Checked  := StrtoBool(StrBin[16]);

            If BIT_TRUCKSTATUS(14).ToString = 1 Then
                'EVEH.Visible = True
                LEDPERM.BackColor = Color.Lime
            Else
                'EVEH.Visible = False
                LEDPERM.BackColor = Color.Red
            End If

            ' LEDPERM.Checked := StrtoBool(StrBin[15]);

            If BIT_TRUCKSTATUS(13).ToString = 1 Then
                'EVEH.Visible = True
                LEDDEP.BackColor = Color.Lime
            Else
                'EVEH.Visible = False
                LEDDEP.BackColor = Color.Red
            End If
            'LEDDEP.Checked := StrtoBool(StrBin[14]);

            If BIT_TRUCKSTATUS(11).ToString = 1 Then
                'EVEH.Visible = True
                LEDOP.BackColor = Color.Red
            Else
                'EVEH.Visible = False
                LEDOP.BackColor = Color.Lime
            End If


            'LEDOP .Checked :=  StrtoBool(StrBin[12]);

            If BIT_TRUCKSTATUS(10).ToString = 1 Then
                'EVEH.Visible = True
                LEDPOWER.BackColor = Color.Lime
                btPstart.BackColor = Color.Lime
                BtPstop.BackColor = Color.Gray
            Else
                'EVEH.Visible = False
                LEDPOWER.BackColor = Color.Red
                btPstart.BackColor = Color.Gray
                BtPstop.BackColor = Color.Red
            End If

            'LED_POWER .Checked :=  StrtoBool(StrBin[11]);
            '         If LED_POWER.Checked Then
            '             begin()
            '   PUMP_START.color:=clLime;
            '   PUMP_STOP.color:=clBtnFace;
            'end else begin
            '    PUMP_START.color:=clBtnFace;
            '    PUMP_STOP.color:=clred;
            'end;

            If BIT_TRUCKSTATUS(9).ToString = 1 Then
                'EVEH.Visible = True
                LEDLOADING.BackColor = Color.Lime
            Else
                'EVEH.Visible = False
                LEDLOADING.BackColor = Color.Gray
            End If
            '  LEDLOADING.Checked :=  StrtoBool(StrBin[10]);
        Catch ex As Exception
        End Try
        Try
            If EVEH.Visible Then
                If V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows.Count > 0 Then
                    EDREF.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("Reference").ToString
                    EDVEH.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("LOAD_VEHICLE").ToString
                    EDCUS.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("LOAD_CUSTOMER").ToString
                    EDTSTART.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("LOAD_STARTTIME").ToString
                    EDSTOP.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("LOAD_ENDTIME").ToString
                    EDWIN.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("LOAD_WEIGHT_IN").ToString + " Kg"
                    EDWOUT.Text = V_UNLOAD_NEW.Tables("V_UNLOAD_NEW").Rows(0).Item("LOAD_WEIGHT_OUT").ToString + " Kg"
                Else
                    EDREF.Text = "-"
                    EDVEH.Text = "-"
                    EDCUS.Text = "-"
                    EDTSTART.Text = "--:--"
                    EDSTOP.Text = "--:--"
                    EDWIN.Text = "- Kg"
                    EDWOUT.Text = "- Kg"
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub



    Private Sub PXV2014_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles XVPR3.Click, XVPL3.Click, XVPG3.Click, XVPB3.Click
        Me.AddOwnedForm(XV01)
        XV01.ShowDialog()
        XV01.BringToFront()
    End Sub

    Private Sub PXV2014_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles XVPR2.Click, XVPL2.Click, XVPG2.Click, XVPB2.Click
        Me.AddOwnedForm(XV02)
        XV02.ShowDialog()
        XV02.BringToFront()
    End Sub

    Private Sub UpdateSql(ByVal Reg As String, ByVal Val As String)
        Dim q As String
        Dim SqlCon As New SqlClient.SqlConnection
        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        Dim da As New SqlClient.SqlDataAdapter
        SqlCon.Open()
        Try
            q = ""
            q = q + "update T_MBR "
            q = q + " set MBR_DATE = getdate() "
            q = q + ",MBR_VAL = '" + (Val) + "'"
            q = q + " where MBR_REG = '" + (Reg) + "'"
            da.UpdateCommand = New SqlClient.SqlCommand(q, SqlCon)
            da.UpdateCommand.ExecuteNonQuery()
        Catch ex As Exception
        End Try
        da.Dispose()
        SqlCon.Close()
        SqlCon.Dispose()
    End Sub

    Private Sub BTESD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTESD.Click

        Dim da As New SqlClient.SqlDataAdapter
        Dim ds As New DataSet
        Dim SqlCon As New SqlClient.SqlConnection
        SqlCon.ConnectionString = My.Settings.FPTConnectionString
        SqlCon.Open()
        ' Dim i, j As Integer
        Dim q As String
        Dim T_REALTIME, T_MBR As New DataTable
        Dim REG_UnStatus As Integer
        Dim BIT_UnStatus As String

        If MessageBox.Show("EMERGENCY CLEAR ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            Try
                BIT_UnStatus = ""
                q = "select * from T_MBR where mbr_reg='406004'"
                da = New SqlClient.SqlDataAdapter(q, SqlCon)
                'ds.Reset()
                ds.Dispose()
                ds = New DataSet
                da.Fill(ds, "T_MBR")
                REG_UnStatus = ds.Tables("T_MBR").Rows(0).Item("MBR_VAL").ToString
                BIT_UnStatus = Strings.Right("0000000000000000" + Convert.ToString(REG_UnStatus, 2), 16)
                ' GNDSTATUS = BIT_UnStatus(12).ToString
                REG_UnStatus = (REG_UnStatus Or 1)


                'Bit 1 of 15 PCB 8421
                UpdateSql("406004", REG_UnStatus)

            Catch ex As Exception

            End Try
        End If
        ds.Dispose()
        da.Dispose()
        SqlCon.Dispose()
    End Sub

    Private Sub btPstart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btPstart.Click
        If MessageBox.Show("START POWER PUMP ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 0 of 15 PCB 8421
            UpdateSql("406003", 8)
        End If

    End Sub

    Private Sub BtPstop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtPstop.Click
        If MessageBox.Show("STOP POWER PUMP ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 0 of 15 PCB 8421
            UpdateSql("406003", 16)
        End If
    End Sub


    Private Sub BtReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtReset.Click
        If MessageBox.Show("RESET ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 0 of 15 PCB 8421
            UpdateSql("406003", 4)
        End If
    End Sub

    Private Sub BtGrant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtGrant.Click
        If MessageBox.Show("GRANT ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 0 of 15 PCB 8421
            UpdateSql("406003", 1)
        End If
    End Sub

    Private Sub BtRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtRemove.Click
        If MessageBox.Show("REMOVE ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = vbYes Then
            'Bit 0 of 15 PCB 8421
            UpdateSql("406003", 2)
        End If
    End Sub

    Private Sub AmmoniaUnload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.PictureBox8.Image = New System.Drawing.Bitmap("Company.png")
        RunThrRefStstus()
    End Sub

    Private Sub AmmoniaUnload_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ThrRefStstus.CancelAsync()
    End Sub
End Class